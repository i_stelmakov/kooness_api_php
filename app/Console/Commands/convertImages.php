<?php

namespace App\Console\Commands;

use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\Post;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class convertImages extends Command
{
    protected $_path = 'https://s3-eu-west-1.amazonaws.com/kooness-bucket';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:images {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('type') == 'artworks') {
            $this->convertImagesArtworks();
        } else if ($this->argument('type') == 'artists') {
            $this->convertImagesArtists();
        } else if ($this->argument('type') == 'galleries') {
            $this->convertImagesGalleries();
        } else if ($this->argument('type') == 'fairs') {
            $this->convertImagesFairs();
        } else if ($this->argument('type') == 'posts') {
            $this->convertImagesPost();
        } else {
            $this->convertImagesArtworks();
            $this->convertImagesArtists();
            $this->convertImagesGalleries();
            $this->convertImagesFairs();
            $this->convertImagesPost();
        }
    }

    public function convertImagesArtworks()
    {
        $artworks = Artwork::all();
        foreach ($artworks as $artwork) {
            $images = $artwork->images()->first();
            if ($images) {
                $fileName = $this->cpImage('artworks', 'artworks', $artwork->id, $images->url);
                $images->url = $fileName;
                $images->save();
            }
        }
    }

    public function convertImagesArtists()
    {
        $artists = Artist::all();
        foreach ($artists as $artist) {
            if ($artist->url_picture) {
                $fileName = $this->cpImage('artists', 'artists', $artist->id, $artist->url_picture);
                $artist->url_picture = $fileName;
            }
            if ($artist->url_cover) {
                $fileName = $this->cpImage('artists', 'artists', $artist->id, $artist->url_cover);
                $artist->url_cover = $fileName;
            }
            $artist->save();
        }
    }

    public function convertImagesGalleries()
    {
        $galleries = Gallery::all();
        foreach ($galleries as $gallery) {
            if ($gallery->url_cover) {
                $fileName = $this->cpImage('galleries', 'galleries', $gallery->id, $gallery->url_cover);
                $gallery->url_cover = $fileName;
                $gallery->save();
            }
        }
    }

    public function convertImagesFairs()
    {
        $fairs = Fair::all();
        foreach ($fairs as $fair) {
            if ($fair->url_picture) {
                $fileName = $this->cpImage('fairs', 'fairs', $fair->id, $fair->url_picture);
                $fair->url_picture = $fileName;
            }
            if ($fair->url_cover) {
                $fileName = $this->cpImage('fairs', 'fairs', $fair->id, $fair->url_cover);
                $fair->url_cover = $fileName;
            }
            $fair->save();
        }
    }

    public function convertImagesPost()
    {
        $posts = Post::all();
        foreach ($posts as $post) {
//            if ($post->url_picture) {
//                $fileName = $this->cpImage('posts', 'posts', $post->id, $post->url_picture);
//                $post->url_picture = $fileName;
//            }
//            if ($post->url_cover) {
//                $fileName = $this->cpImage('posts', 'posts', $post->id, $post->url_cover);
//                $post->url_cover = $fileName;
//            }
            preg_match_all('/src="([^"]*)"/i', $post->content, $matches);
            if (isset($matches[1]) && count($matches[1])) {
                $content = $post->content;
                foreach ($matches[1] as $match) {
                    if (strpos($match, 'https://www.kooness.com/public/uploads/') > -1) {
                        $new_match = str_replace('https://www.kooness.com/public/uploads/', '', $match);

                        $fileName = $this->cpImage('', 'archive', $post->id, $new_match, true);
                        $content = str_replace($match, $fileName, $content);
                    }
                }
                $post->content = $content;
            }
            $post->save();
        }
    }

    private function cpImage($old_directory, $directory, $id, $fileName, $ignore_id = false)
    {
        $fileName = preg_replace('/.[^.]+$/i', '.' . 'jpg', $fileName);
        $fileName_new = preg_replace('/-trim\((.*)\)/i', '', $fileName);
        $old_path = "/uploads_old/$old_directory/" . $fileName;
        if (!$ignore_id) {
            $new_path = "/uploads/$directory/$id/" . sanitize_file_name_chars(basename($fileName_new));
        } else {
            $new_path = "/uploads/$directory/" . sanitize_file_name_chars(basename($fileName_new));
        }
        if (!$ignore_id) {
            if (Storage::disk('local')->exists($old_path)) {
                if (Storage::disk('s3')->exists("/uploads/$directory") || Storage::disk()->makeDirectory("/uploads/$directory")) {
                    if (!$ignore_id) {
                        if (Storage::disk('s3')->exists("/uploads/$directory/$id") || Storage::disk()->makeDirectory("/uploads/$directory/$id")) {
                            if (!Storage::disk('s3')->exists($new_path)) {
                                $contents = Storage::disk('local')->get($old_path);
                                Storage::disk('s3')->put($new_path, $contents);
                            }
                            return $this->_path . $new_path;
                        }
                    } else {
                        if (!Storage::disk('s3')->exists($new_path)) {
                            $contents = Storage::disk('local')->get($old_path);
                            Storage::disk('s3')->put($new_path, $contents);
                        }
                        return $this->_path . $new_path;
                    }
                }
            } else {
                Log::channel('images')->error("Old images not exists: $old_path");
                return '/images/default-picture.png';
            }
        } else {
            if (Storage::disk('local')->exists($old_path)) {
                if (Storage::disk('local')->exists("/uploads/$directory") || Storage::disk('local')->makeDirectory("/uploads/$directory")) {
                    if (!$ignore_id) {
                        if (Storage::disk('local')->exists("/uploads/$directory/$id") || Storage::disk('local')->makeDirectory("/uploads/$directory/$id")) {
                            if (!Storage::disk('local')->exists($new_path)) {
                                Storage::disk('local')->copy($old_path, $new_path);
                            }
                            return $new_path;
                        }
                    } else {
                        if (!Storage::disk('local')->exists($new_path)) {
                            Storage::disk('local')->copy($old_path, $new_path);
                        }
                        return $new_path;
                    }
                }
            } else {
                Log::channel('images')->error("Old images not exists: $old_path");
                return '/images/default-picture.png';
            }
        }
    }
}
