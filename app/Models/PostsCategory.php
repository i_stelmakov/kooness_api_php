<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsCategory extends Model
{
    protected $guarded = [];

    public function posts(){
        return $this->belongsToMany('App\Models\Post', 'posts_posts_categories', 'category_id', 'post_id');
    }

    public function randomImage(){
        $post = $this->posts()->whereStatus(1)->orderByRaw("RAND()")->first();
        if($post && !$post->url_full_image){
            return $this->randomImage();
        }else{
            if(!$post)
                return null;
            return $post->url_full_image;
        }
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
