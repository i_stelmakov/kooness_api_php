<div class="slider-item one-third-col-with-margin">
    <i title="Remove" class="fa fa-times delete-item" data-id="{{ $gallery->id }}" data-section="galleries"></i>
    <div class="slider-item-img wide-box">
        <a href="{{ route('galleries.single', [$gallery->slug]) }}">
            <div class="background-cover gallery lazy" data-src="{{ $gallery->url_wide_box }}"></div>
        </a>
    </div>
    <div class="slider-item-txt">
        <div class="default-sheet-row slider-item-row">
            <div class="default-sheet-row-cell">
                <h2><a class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a></h2>
            </div>
        </div>
        <div class="default-sheet-row slider-item-row">
            <div class="default-sheet-row-cell">
                <p class="gallery-adress">{{ $gallery->city }}, {{ $gallery->address }}</p>
            </div>
        </div>
        <a class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">read more</a>
    </div>
</div>