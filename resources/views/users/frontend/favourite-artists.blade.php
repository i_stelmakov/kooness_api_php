{{-- User - Favourite Artists --}}

@extends('layouts.app')

@section('title')
    User - Favourite Artists
@endsection

@section('title', 'Favourite Artists')

@section('body-class')
    user-favourite-artists
@endsection

@section('content')
    <section id="artist-slider" class="default-slider-section slider-border">
        <div id="saved-artists" class="container boxed-container">
            <div class="default-slider-header">
                <h1>Saved Artists</h1>
            </div>
            <div class="col-container">
                @if(Auth::user()->collectionArtworks()->count())
                    @foreach(Auth::user()->collectionArtists()->get() as $artist)
                        @include('users.frontend.partials.artist', ['artist' => $artist])
                    @endforeach
                @else
                    No favourite
                @endif
            </div>
        </div>
    </section>
@endsection