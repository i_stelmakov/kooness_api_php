@extends('layouts.admin')
@section('title', "Admin | Edit Artwork `{$artwork->title}`")
@section('page-header', "Admin - Edit Artwork `{$artwork->title}`")

@section('content')
    @include('artworks.admin.form', ['action' => route('admin.artworks.update', [$artwork->id]), 'method' => 'PATCH'])
@endsection