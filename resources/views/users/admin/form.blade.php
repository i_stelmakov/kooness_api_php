<!-- ☢️ jQuery Validation test -->
<style>
    form .error {
        color: #ff0000;
    }
</style>
<div class="row">
    <form class="ajax-form" role="form" method="POST" action="{{ $action }}" name="insert-user" data-attachment="validate" data-id="{{ $user->id }}">
        {{ method_field($method) }}
        <div class="col-md-6 col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    User information
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="first_name">First name (*)</label>
                                <input id="first_name"
                                       class="form-control"
                                       type="text"
                                       placeholder="First Name" name="first_name"
                                       value="{{ old('first_name', $user->{'first_name'}) }}"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="last_name">Last name (*)</label>
                                <input id="last_name"
                                       class="form-control"
                                       type="text"
                                       placeholder="Last Name" name="last_name"
                                       value="{{ old('last_name', $user->{'last_name'}) }}"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="username">Username (*)</label>
                                <input id="username"
                                       class="form-control"
                                       type="text"
                                       placeholder="Username" name="username"
                                       value="{{ old('username', $user->{'username'}) }}" required>
                            </div>
                        </div>
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="email">Email (*)</label>
                                <input id="email"
                                       class="form-control"
                                       type="email"
                                       placeholder="Email" name="email"
                                       value="{{ old('email', $user->{'email'}) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="password">Password
                                    @if(Request::is('admin/users/*/edit'))
                                        <small> (Leave password blank if dont want to change)</small>
                                    @else
                                       (*)
                                    @endif
                                </label>
                                <input id="password"
                                       class="form-control"
                                       type="password"
                                       placeholder="Password" name="password"
                                       value=""
                                       @if(!Request::is('admin/users/*/edit'))
                                       required
                                        @endif>

                            </div>
                        </div>
                        <div class="col-md-6  col-xs-12">
                            <div class="form-group">
                                <label for="password_confirm">Reinsert password(*)</label>
                                <input id="password_confirm"
                                       class="form-control"
                                       type="password"
                                       placeholder="Reinsert password" name="password_confirm"
                                       value=""
                                       @if(!Request::is('admin/users/*/edit'))
                                       required
                                        @endif>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="select2 form-control" name="role">
                                    @foreach($roles as $role)
                                        @if(!roleCheck('superadmin') && $role->name == 'superadmin' )
                                            @continue
                                        @endif
                                        <option value="{{ $role->name }}" {{ $role->name == $user->role? 'selected="selected"' : '' }}>{{ get_user_role_name($role->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    @php($fair_user = $user->fair()->first())
                    @php($gallery_user = $user->gallery()->first())
                    @php($artist_user = $user->artist()->first())
                    <div class="row {{ (!$fair_user) ? 'hide' : 'default-assignment' }}">
                        <div class="col-md-12 col-xs-12 ">
                            <div class="form-group">
                                <label for="fair">Associate to fair</label>
                                @if($fair_user)
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <button class="btn btn-warning change-role-association" type="button" data-text="Click here for change<">Click here for change</button>
                                        </span>
                                        <select class="select2 form-control " name="fair" disabled>
                                            <option value="">Not assigned</option>
                                            @foreach($fairs as $fair)
                                                <option value="{{ $fair->id }}" {{ ($fair_user && $fair_user->id ==  $fair->id)?  'selected="selected" data-default="true"' : '' }}>{{ $fair->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <select class="select2 form-control " name="fair" disabled>
                                        <option value="">Not assigned</option>
                                        @foreach($fairs as $fair)
                                            <option value="{{ $fair->id }}" {{ ($fair_user && $fair_user->id ==  $fair->id)?  'selected="selected"  data-default="true"' : '' }}>{{ $fair->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row {{ (!$gallery_user) ? 'hide' : 'default-assignment' }}">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="gallery">Associate to gallery</label>
                                @if($gallery_user)
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <button class="btn btn-warning change-role-association" type="button" data-text="Click here for change<">Click here for change</button>
                                        </span>
                                        <select class="select2 form-control" name="gallery" disabled>
                                            <option value="">Not assigned</option>
                                            @foreach($galleries as $gallery)
                                                <option value="{{ $gallery->id }}" {{ ($gallery_user && $gallery_user->id ==  $gallery->id)?  'selected="selected" data-default="true"' : '' }}>{{ $gallery->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <select class="select2 form-control" name="gallery" disabled>
                                        <option value="">Not assigned</option>
                                        @foreach($galleries as $gallery)
                                            <option value="{{ $gallery->id }}" {{ ($gallery_user && $gallery_user->id ==  $gallery->id)?  'selected="selected" data-default="true"' : '' }}>{{ $gallery->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="email">Membership expired on (*)</label>
                                <input
                                        id="membership_expiration"
                                        class="form-control datepicker date-mask"
                                        type="text"
                                        name="membership_expiration"
                                        placeholder="DD/MM/AAAA"
                                        data-for-user="true"
                                        value="{{ ($user->trial_activation_date)?  date('d/m/Y',strtotime(date("Y-m-d", strtotime($user->{'trial_activation_date'})) . " +1 month")) : date('d/m/Y', strtotime(date("Y-m-d", time()) . " +1 month")) }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row {{ (!$artist_user) ? 'hide' : 'default-assignment' }}">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="artist">Associate to artist</label>
                                @if($artist_user)
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <button class="btn btn-warning change-role-association" type="button" data-text="Click here for change<">Click here for change</button>
                                        </span>
                                        <select class="select2 form-control" name="artist" disabled>
                                            <option value="">Not assigned</option>
                                            @foreach($artists as $artist)
                                                <option value="{{ $artist->id }}" {{ ($artist_user && $artist_user->id ==  $artist->id)?  'selected="selected" data-default="true"' : '' }}>{{ $artist->full_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <select class="select2 form-control" name="artist" disabled>
                                        <option value="">Not assigned</option>
                                        @foreach($artists as $artist)
                                            <option value="{{ $artist->id }}" {{ ($artist_user && $artist_user->id ==  $artist->id)?  'selected="selected" data-default="true"' : '' }}>{{ $artist->full_name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="status">Enable</label>
                                <select class="select2 form-control" name="status">
                                    <option value="1" {{ $user->{'status'} ? 'selected="selected"' : '' }}>Yes</option>
                                    <option value="0" {{ !$user->{'status'} ? 'selected="selected"' : '' }}>No</option>
                                </select>
                            </div>
                        </div>
                        @if(!Request::is('admin/users/*/edit'))
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email_activation">Send email with login data to user</label>
                                    <select class="select2 form-control" name="email_activation">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>