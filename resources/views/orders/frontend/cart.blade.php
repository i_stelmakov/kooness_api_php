@extends('layouts.app')

@section('title')
    Cart
@endsection

@section('body-class')
    cart
@endsection

@section('seo-customs-scripts')
    <script>
        window.addEventListener('load', function() {
            // Initiate Checkout button
            var initiateCheckoutButton = document.getElementById('initiateCheckoutButton');
            if(initiateCheckoutButton)
                initiateCheckoutButton.addEventListener(
                    'click',
                    function() {

                        // For facebook pixel
                        fbq('track', 'InitiateCheckout');
                        console.log('Facebook Pixel "Intiate Checkout" triggered!');

                        // For Google Analytics
                        ga('send', 'event', 'Funnel', 'Intiate Checkout');
                        console.log('Google Analytics "Intiate Checkout" triggered!');

                    },
                    false
                );
        });
    </script>
@endsection

@section('content')
    <section class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>Purchase</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="cart">
            <div class="boxed-container">
                <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                    @if($cart->items()->count())
                        @foreach($cart->items()->get() as $item)
                            <?php
                            $product = $item->product;
                            $casual_transfer[] = $item->product->title;
                            ?>
                            <div class="full-col-with-margin cart-item">
                                <div class="col-container">
                                    <div class="one-third-col-with-margin">
                                        <div class="cart-product-img">
                                            <a href="{{ route('artworks.single', [$product->slug]) }}" class="cart-thumb">
                                                <img data-src="{{ $product->main_image->url }}" src="" class="lazy"> </a>
                                        </div>
                                    </div>
                                    <div class="two-third-col-with-margin">
                                        <form method="POST" action="{{ route('orders.remove.from.cart') }}">
                                            @csrf
                                            <input type="hidden" value="{{ $product->id }}" name="id">
                                            <a onclick="this.parentNode.submit()" title="Remove" class="red-text remove">Remove</a>
                                        </form>
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <h3 class="no-margin">
                                                        <a title="{{ $product->artist_name }}" class="dark-text" href="{{ route('artists.single', [$product->artist->slug]) }}">{{ $product->artist_name }}</a>
                                                    </h3>
                                                </div>

                                                @if(Auth::user())
                                                <a class="follow secondary-link {{ (Auth::user()->collectionArtworks->contains($product->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$product->id}}">
                                                    @if(Auth::user()->collectionArtworks->contains($product->id))
                                                        <i class="fa fa-heart"> </i>
                                                        <span class="follow-unfollow">
                                                            Unfollow
                                                        </span>
                                                    @else
                                                        <i class="fa fa-heart-o"> </i>
                                                        <span class="follow-unfollow">
                                                            Follow
                                                        </span>
                                                    @endif
                                                </a>
                                                @endif

                                            </div>
                                            <div class="default-sheet-row">
                                                <span class="h1" class="no-margin">{{ $product->title }}</span>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Reference</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->reference }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Year</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->year }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Medium</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->medium_string }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Size</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->measure_cm }} cm<br>{{ $product->measure_inc }} in
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box default-sheet-price">
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Price</p>
                                                <div class="default-sheet-row flex-align-center">
                                                    <div class="default-sheet-row-cell">
                                                        <h4 data-price="{{ $product->price }}" data-currency="EUR">{{ $product->pretty_price }}</h4>
                                                    </div>
                                                    <div class="default-sheet-row-cell">
                                                        <select class="currency-change no-margin">
                                                            <option value="EUR" selected="selected">€</option>
                                                            <option value="USD">$</option>
                                                            <option value="GBP">£</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Artwork offered by</p>
                                                <div class="default-sheet-row">
                                                    <div class="default-sheet-row-cell">
                                                        <h4>
                                                            <a href="{{ route('galleries.single', [$product->gallery->slug]) }}">{{ $product->gallery->name }}</a>, {{ $product->gallery->city }}
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="full-col-with-margin cart-item">
                            <div class="col-container">
                                <p>Your cart is empty.</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        @if($cart->items()->count())
            <section class="cart-payment">
                <div class="boxed-container">
                    <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                        {{--Billing Address Section--}}
                        <div class="one-half-col-with-margin">
                            <!-- Cart Summary !-->
                            <div class="default-sheet-box cart-box">
                                <h4>Summary</h4>
                                <p>To obtain final shipping costs check out and complete your order</p>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Subtot</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell cart-subtotal">
                                        @if($promo)
                                            <p>{{ number_format($cart->total_price * (1 - ($promo->value / 100)), 2, ',', '') }} €</p>
                                        @else
                                            <p>{{ number_format($cart->total_price, 2, ',', '') }} €</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Tot</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text cart-price cart-total">
                                            @if($promo)
                                                <strong>{{ number_format($cart->total_price * (1 - ($promo->value / 100)), 2, ',', '') }} €</strong>
                                            @else
                                                <strong>{{ number_format($cart->total_price, 2, ',', '') }} €</strong>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- Payment Method !-->
                            <a href="{{ route('orders.view.checkout') }}">
                                <button id="initiateCheckoutButton" class="black-button" type="submit">Checkout</button>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </section>
@endsection