<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\Gallery;
use App\Models\PromoCode;
use App\Models\User;
use App\Services\Api4Dem\Api4Dem;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FrontendController extends FrontendExtendedController
{
    public function search(Request $request)
    {
        $result = [
            'artists' => [],
            'galleries' => [],
            'categories' => []
        ];
        if ($request->ajax() && $request->get('query')) {
            $q = $request->get('query');
            $words = explode(' ', $q);
            $artists_query = Artist::whereStatus(1)->distinct();
            $galleries_query = Gallery::whereStatus(1)->distinct();
            $categories_query = ArtworksCategory::distinct();
            foreach ($words as $word) {
                $artists_query->whereRaw("(first_name LIKE '%$word%' OR last_name LIKE '%$word%')");
                $galleries_query->whereRaw("name LIKE '%$word%'");
                $categories_query->whereRaw("name LIKE '%$word%'");
            }
            $artists = $artists_query->get();
            $galleries = $galleries_query->get();
            $categories = $categories_query->get();


            foreach ($artists as $artist) {
                $result['artists'][] = [
                    "image_url" => $artist->url_full_image,
                    "text" => $artist->first_name . " " . $artist->last_name,
                    "link" => route('artists.single', [$artist->slug])
                ];
            }

            foreach ($galleries as $gallery) {
                $result['galleries'][] = [
                    "image_url" => $gallery->url_full_image,
                    "text" => $gallery->name,
                    "link" => route('galleries.single', [$gallery->slug])
                ];
            }

            foreach ($categories as $category) {
                $result['categories'][] = [
                    "image_url" => $category->url_full_image,
                    "text" => $category->name,
                    "link" => (!$category->is_medium) ? route('artworks.single', [$category->slug]) : route('artworks.single', [$category->slug])
                ];
            }

        }
        return response()->json($result);
    }

    public function getCurrency($currency)
    {
        $xml = simplexml_load_file('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');
        $elements = $xml->xpath('//*[@currency="' . $currency . '"]');
        if (isset($elements[0])) {
            return response()->json([
                "result" => "success",
                "value" => (float)$elements[0]->attributes()->rate
            ]);
        }
        return response()->json([
            "result" => "error"
        ]);
    }

    public function applyPromoCode(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $code = $request->get('code');
            $promoCode = PromoCode::whereName($code)->first();
            if ($promoCode) {
                $response = [
                    'status' => 'success',
                    'coupon' => $promoCode
                ];
            }
        }
        return response()->json($response);
    }

    public function subscribeToNewsletter(Request $request){
        if ($request->method() == 'POST') {
            $validator = Validator::make($request->all(), [
                'honey_name' => 'honeypot',
                'honey_time' => 'required|honeytime:5'
            ]);

            if ($validator->fails()) {
                return redirect()->back();
            }
            $firstname = $request->get('firstname');
            $lastname = $request->get('lastname');
            $email = $request->get('email');

            // 4DEM -
            $user = new User();
            $user->first_name = $firstname;
            $user->last_name = $lastname;
            $user->email = $email;
            $api4dem = new Api4Dem();
            $result = $api4dem->userSubscribe($user);

            if($result){
                return redirect()->route('subscription.newsletter.thank.you');
            } else{
                return redirect()->back()->with('error', 'newsletter');
            }

        }
    }

    public function subscriptionNewsletterThankYou(){
        return view('newsletter-thank-you');
    }

    public function sendContactMessage(Request $request){
        if ($request->method() == 'POST') {
            $validator = Validator::make($request->all(), [
                'honey_name' => 'honeypot',
                'honey_time' => 'required|honeytime:5'
            ]);

            if ($validator->fails()) {
                return redirect()->back();
            }
            $data = $request->all(['name','email','subject','body','redirect_to']);

            $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');
            $name = 'Kooness Request Contact';
//            $subject = "New request from kooness.com";
            $subject = SUBJECT_MAIL_REGISTRATION_INFO_REQUEST;

            Mail::send('emails.contact-forms.info-request', $data, function ($message) use ($to, $name, $subject) {
                $message->to($to, $name)->subject($subject);
            });

//            dd($data);
            if($request->has('redirect_to')) {
                return redirect($request->get('redirect_to'))->with('send', 'success');
//            return redirect()->route('pages.intro', ['contact']).'#contact-us';
            }
            return redirect()->back()->with('send', 'success');
        }
    }

    public function googleFeed(){

        $xml = new DOMDocument("1.0","UTF-8");

        $rss = $xml->createElement('rss');
        $rss->setAttribute("version", "2.0");
        $rss->setAttribute("xmlns:g", "http://base.google.com/ns/1.0");

        $channel = $xml->createElement('channel');

        $title = $xml->createElement('title', 'Kooness Google Shopping Feed');

        $link = $xml->createElement('link', url(''));

        $description = $xml->createElement('description', 'Feed for google shopping');

        $channel->appendChild($description);
        $channel->appendChild($link);
        $channel->appendChild($title);

        $artworks = Artwork::whereRaw('artworks.status IN (1,2)')->with([
            'artist',
            'categories' => function ($query) {
                $query->whereIsMedium(0);
//                }])->whereReference('507235e8')->get();
//                }])->offset('5')->limit('5')->get();
            }])->limit(100)->get();
        foreach ($artworks as $artwork){
            try {
                $item = $xml->createElement('item');

                $id = $xml->createElement('g:id', $artwork->reference);
                $item->appendChild($id);

                $title = $xml->createElement('g:title', htmlspecialchars(($artwork->artist->full_name . ' - ' . $artwork->title . ' - ' . $artwork->year), ENT_XML1, 'UTF-8'));
                $item->appendChild($title);

                $link = $xml->createElement('g:link', route('artworks.single', [htmlspecialchars($artwork->slug, ENT_XML1, 'UTF-8')]));
                $item->appendChild($link);

                $description = $xml->createElement('g:description', htmlspecialchars(strip_tags($artwork->about_the_work), ENT_XML1, 'UTF-8'));
                $item->appendChild($description);

                $image = $xml->createElement('g:image_link', $artwork->main_image->url);
                $item->appendChild($image);

                $type = $xml->createElement('g:product_type', htmlspecialchars(join(', ', $artwork->medium->pluck('name')->toArray()), ENT_XML1, 'UTF-8'));
                $item->appendChild($type);

                $availability = $xml->createElement('g:availability', ($artwork->status == 1) ? 'in stock' : 'out of stock');
                $item->appendChild($availability);

                $price = $xml->createElement('g:price', number_format($artwork->price, 2, '.', '') . ' EUR');
                $item->appendChild($price);

                $brand = $xml->createElement('g:brand', htmlspecialchars($artwork->artist->full_name, ENT_XML1, 'UTF-8'));
                $item->appendChild($brand);

                $size = $xml->createElement('g:size', $artwork->measure_inc . ' in');
                $item->appendChild($size);

                $ident = $xml->createElement('g:identifier_exists', 'no');
                $item->appendChild($ident);

                $category = $xml->createElement('g:google_product_category', "500044");
                $item->appendChild($category);

                $category = $xml->createElement('g:condition', "New");
                $item->appendChild($category);

                $custom_0 = $xml->createElement('g:custom_label_0', htmlspecialchars($artwork->gallery_name, ENT_XML1, 'UTF-8'));
                $item->appendChild($custom_0);

                $custom_1 = $xml->createElement('g:custom_label_1', $artwork->year);
                $item->appendChild($custom_1);

                $channel->appendChild($item);
            } catch(\Exception $e){
                dump($artwork);
                dump($e);exit;
            }
        }

        $rss->appendChild($channel);

        $xml->appendChild($rss);

        return response($xml->saveXML(), 200, [
            'Content-Type' => 'application/xml; charset=utf-8'
        ]);
    }
}
