<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\ArtworksImage;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\LayoutPage;
use App\Models\Post;
use App\Models\PostsCategory;
use App\Models\User;
use App\Models\WidgetSlider;
use App\Models\WidgetTemplate;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Models\Config;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function userCheckEmail(Request $request)
    {
        if ($request->method() == 'POST') {
            $email = $request->get('email');
            $id = $request->get('id');
            if ($id) {
                if (User::whereId($id)->whereEmail($email)->count()) {
                    return response()->json(true);
                }
            }
            if (User::whereEmail($email)->count()) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        }
    }

    public function userCheckUsername(Request $request)
    {
        if ($request->method() == 'POST') {
            $username = $request->get('username');
            $id = $request->get('id');
            if ($id) {
                if (User::whereId($id)->whereUsername($username)->count()) {
                    return response()->json(true);
                }
            }
            if (User::whereUsername($username)->count()) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        }
    }

    public function checkSlugAvailability(Request $request)
    {
        if ($request->method() == 'POST' && $request->ajax()) {
            $slug = $request->get('slug');
            $id = $request->get('id');
            $referer = $request->get('referer');
            $class = null;
            $other_class = null;
            if ($referer == 'category-posts') {
                $class = 'App\Models\PostsCategory';
                $other_class = 'App\Models\Post';
            }
            if ($referer == 'posts') {
                $class = 'App\Models\Post';
                $other_class = 'App\Models\PostsCategory';
            }
            if ($referer == 'tag') {
                $class = 'App\Models\Tag';
            }
            if ($referer == 'category-gallery') {
                $class = 'App\Models\GalleriesCategory';
                $other_class = 'App\Models\Gallery';
            }
            if ($referer == 'gallery') {
                $class = 'App\Models\Gallery';
                $other_class = 'App\Models\GalleriesCategory';
            }
            if ($referer == 'fair') {
                $class = 'App\Models\Fair';
            }
            if ($referer == 'category-artist') {
                $class = 'App\Models\ArtistsCategory';
                $other_class = 'App\Models\Artist';
            }
            if ($referer == 'artist') {
                $class = 'App\Models\Artist';
                $other_class = 'App\Models\ArtistsCategory';
            }
            if ($referer == 'category-artwork') {
                $class = 'App\Models\ArtworksCategory';
                $other_class = 'App\Models\Artwork';
            }
            if ($referer == 'artwork') {
                $class = 'App\Models\Artwork';
                $other_class = 'App\Models\ArtworksCategory';
            }
            if ($referer == 'exhibition') {
                $class = 'App\Models\Exhibition';
            }
            if ($referer == 'page') {
                $class = 'App\Models\Page';
            }

            if ($class) {
                if ($id) {
                    if ($class::whereId($id)->whereSlug($slug)->count()) {
                        return response()->json(true);
                    } else{
                        if ($class::whereSlug($slug)->count()) {
                            return response()->json(false);
                        } else {
                            if($other_class){
                                if ($other_class::whereSlug($slug)->count()) {
                                    return response()->json(false);
                                }
                            }
                            return response()->json(true);
                        }
                    }
                }
                if ($class::whereSlug($slug)->count()) {
                    return response()->json(false);
                } else {
                    if($other_class){
                        if ($other_class::whereSlug($slug)->count()) {
                            return response()->json(false);
                        }
                    }
                    return response()->json(true);
                }
            }
        }
    }

    public function uploadImages(Request $request)
    {
        $response = [
            'status' => 'failed'
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $data = Input::all();
            $directory = $data['path'];
            $id = $data['id_row'];
            $fileName = $data['name'];
            $label = $data['label'];
            $stream = $data['stream'];

            if (Storage::disk()->exists("/uploads/$directory") || Storage::disk()->makeDirectory("/uploads/$directory")) {
                if (Storage::disk()->exists("/uploads/$directory/$id") || Storage::disk()->makeDirectory("/uploads/$directory/$id")) {
                    $path = "/uploads/$directory/$id/" . substr(sha1(time()), 0, 8) . "_" . sanitize_file_name_chars($fileName);
                    //Log::channel('images')->error($stream);
                    $uri = substr($stream, strpos($stream, ",") + 1);
                    Storage::disk()->put($path, base64_decode($uri), 'public');
                    if (env('FILESYSTEM_DRIVER', 'local') == 's3') {
                        $path = env('CLOUD_FRONT_URL', '') . $path;
                    }
                    $response = $this->saveImageOnDB($directory, $id, $path, $label);
                }
            }
        }
        return response()->json($response);
    }

    private
    function saveImageOnDB($directory, $id, $path, $label)
    {
        if ($directory == 'artworks') {
            $artwork = Artwork::find($id);
            if ($label == 'square_box') {
                $artwork->url_square_box = $path;
            } else if ($label == 'wide_box') {
                $artwork->url_wide_box = $path;
            } else if ($label == 'vertical_box') {
                $artwork->url_vertical_box = $path;
            } else {
                $artworkImage = ArtworksImage::whereArtworkId($id)->whereLabel($label)->first();
                if ($artworkImage) {
                    $file = public_path($artworkImage->url);
                    if (\File::exists($file))
                        \File::delete($file);
                    $artworkImage->url = $path;
                    $artworkImage->save();
                    return [
                        'status' => 'success'
                    ];
                } else {
                    ArtworksImage::create([
                        'artwork_id' => $id,
                        'url' => $path,
                        'label' => $label
                    ]);
                    return [
                        'status' => 'success'
                    ];
                }
            }
            return [
                'status' => 'success'
            ];
        } else if ($directory == 'artists') {
            $artist = Artist::find($id);
            if ($artist) {
                if ($label == 'square_box') {
                    $artist->url_square_box = $path;
                } else if ($label == 'wide_box') {
                    $artist->url_wide_box = $path;
                } else if ($label == 'vertical_box') {
                    $artist->url_vertical_box = $path;
                } else {
                    $artist->url_full_image = $path;
                }
                $artist->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'exhibitions') {
            $exhibition = Exhibition::find($id);
            if ($exhibition) {
                if ($label == 'square_box') {
                    $exhibition->url_square_box = $path;
                } else if ($label == 'wide_box') {
                    $exhibition->url_wide_box = $path;
                } else if ($label == 'vertical_box') {
                    $exhibition->url_vertical_box = $path;
                } else {
                    $exhibition->url_full_image = $path;
                }
                $exhibition->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'galleries') {
            $gallery = Gallery::find($id);
            if ($gallery) {
                if ($label == 'square_box') {
                    $gallery->url_square_box = $path;
                } else if ($label == 'wide_box') {
                    $gallery->url_wide_box = $path;
                } else if ($label == 'vertical_box') {
                    $gallery->url_vertical_box = $path;
                } else {
                    $gallery->url_full_image = $path;
                }
                $gallery->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'fairs') {
            $fair = Fair::find($id);
            if ($fair) {
                if ($label == 'square_box') {
                    $fair->url_square_box = $path;
                } else if ($label == 'wide_box') {
                    $fair->url_wide_box = $path;
                } else if ($label == 'vertical_box') {
                    $fair->url_vertical_box = $path;
                } else {
                    $fair->url_full_image = $path;
                }
                $fair->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'posts') {
            $post = Post::find($id);
            if ($post) {
                if ($label == 'square_box') {
                    $post->url_square_box = $path;
                } else if ($label == 'wide_box') {
                    $post->url_wide_box = $path;
                } else if ($label == 'vertical_box') {
                    $post->url_vertical_box = $path;
                } else {
                    $post->url_full_image = $path;
                }
                $post->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'artists/categories') {
            $category = ArtistsCategory::find($id);
            if ($category) {
                $category->url_cover = $path;
                $category->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'artworks/categories') {
            $category = ArtworksCategory::find($id);
            if ($category) {
                $category->url_cover = $path;
                $category->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'galleries/categories') {
            $category = GalleriesCategory::find($id);
            if ($category) {
                $category->url_cover = $path;
                $category->save();
                return [
                    'status' => 'success'
                ];
            }
        } else if ($directory == 'posts/categories') {
            $category = PostsCategory::find($id);
            if ($category) {
                $category->url_cover = $path;
                $category->save();
                return [
                    'status' => 'success'
                ];
            }
        }
    }

    public
    function mainPage($section)
    {
        if ($section == 'home') {
            return redirect(route('admin.homepage.layout.edit'));
        }
        $layoutPage = LayoutPage::whereLabel($section)->first();
        $result = WidgetTemplate::whereLayoutPageId($layoutPage->id)->orderBy('order', 'ASC')->get();
        $widgetSlider = WidgetSlider::whereLayoutPageId($layoutPage->id)->first();

        $widgets = [];
        $data = [];
        $contents = [];
        if ($widgetSlider) {
            $contents = json_decode($widgetSlider->contents);
        }

        if ($section == 'artworks') {
            $data = Artwork::selectRaw('id as id, title as text')->get();
        } else if ($section == 'artists') {
            $data = Artist::selectRaw('id as id, CONCAT_WS(" ", last_name, first_name) as text')->get();
        } else if ($section == 'galleries') {
            $data = Gallery::selectRaw('id as id, name as text')->get();
        } else if ($section == 'fairs') {
            $data = Fair::selectRaw('id as id, name as text')->get();
        } else if ($section == 'exhibitions') {
            $data = Exhibition::selectRaw('id as id, name as text')->get();
        }

        foreach ($result as $row) {
            $row->items = json_decode($row->items);
            $items = $row->items;
            foreach ($items as $key => $item) {
                $element = null;
                if ($row->section == 'artists') {
                    $element = Artist::selectRaw('id as id, CONCAT_WS(" ", last_name, first_name) as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'artworks') {
                    $element = Artwork::selectRaw('id as id, title as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'galleries') {
                    $element = Gallery::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'fairs') {
                    $element = Fair::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'exhibitions') {
                    $element = Exhibition::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                }
                if ($element) {
                    $items[$key] = (object)[
                        "id" => $element->id,
                        "text" => $element->text
                    ];
                }
                $row->items = $items;
            }
            $widgets[] = $row;
        }

        if (!count($widgets)) {
            $widgets[] = new WidgetTemplate();
        }

        return view('main-page', compact('section', 'contents', 'data', 'widgets'));
    }

    public
    function saveMainPage(Request $request, $section)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];

        if ($request->method() == 'POST') {
            $data = Input::all();
            $toRemove = [];
            $layoutPage = LayoutPage::whereLabel($section)->first();
            if ($layoutPage) {
                if (isset($data['contents'])) {
                    $widgetSlider = WidgetSlider::whereLayoutPageId($layoutPage->id)->first();
                    if (!$widgetSlider) {
                        $widgetSlider = new WidgetSlider();
                        $widgetSlider->layout_page_id = $layoutPage->id;
                    }
                    $widgetSlider->contents = json_encode(array_filter($request->get('contents')));
                    $widgetSlider->save();

                    $response = [
                        'status' => 'success',
                        'redirectTo' => route('admin.main.page', [$section])
                    ];
                } else {
                    $toRemove = WidgetTemplate::whereLayoutPageId($layoutPage->id)->pluck('id')->toArray();
                }
                if (isset($data['widget']))
                    foreach ($data['widget'] as $widget) {
                        $response = [
                            'status' => 'failed',
                            'msg' => 'Ops! Something went wrong!',
                        ];
                        if (isset($widget['items']) && $widget['template'] != 'tplC') {
                            $widget['items'] = json_encode($widget['items']);
                            if ($widget['id']) {
                                if (in_array($widget['id'], $toRemove)) {
                                    $index = array_search($widget['id'], $toRemove);
                                    if ($index > -1) {
                                        unset($toRemove[$index]);
                                    }
                                }
                                $widgetTemplate = WidgetTemplate::find($widget['id']);
                                if ($widgetTemplate->update($widget)) {
                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.main.page', [$section])
                                    ];
                                }
                            } else {
                                $widgetTemplate = WidgetTemplate::create($widget);
                                if ($widgetTemplate) {
                                    $layoutPage->widgets()->save($widgetTemplate);

                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.main.page', [$section])
                                    ];
                                }
                            }
                        } else {
                            if ($widget['id']) {
                                if (in_array($widget['id'], $toRemove)) {
                                    $index = array_search($widget['id'], $toRemove);
                                    if ($index > -1) {
                                        unset($toRemove[$index]);
                                    }
                                }
                                $widgetTemplate = WidgetTemplate::find($widget['id']);
                            } else {
                                $widgetTemplate = new WidgetTemplate();
                            }
                            if ($widget['template'] == 'tplC') {
                                $widgetTemplate->title = $widget['title'];
                                $widgetTemplate->link = $widget['link'];
                                $widgetTemplate->subtitle = null;
                                $widgetTemplate->description = null;
                                $widgetTemplate->template = 'tplC';
                                $widgetTemplate->section = 'exhibitions';
                                $widgetTemplate->order = $widget['order'];
                                $items = [];
                                if (isset($widget['item_past'])) {
                                    $items['past'] = $widget['item_past'];
                                }
                                if (isset($widget['item_current'])) {
                                    $items['current'] = $widget['item_current'];
                                }
                                if (isset($widget['item_upcoming'])) {
                                    $items['upcoming'] = $widget['item_upcoming'];
                                }
                                $widgetTemplate->items = json_encode($items);
                                if ($widgetTemplate->save()) {
                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.main.page', [$section])
                                    ];
                                }
                            }
                        }
                    }
                if (count($toRemove)) {
                    WidgetTemplate::destroy($toRemove);
                    $response = [
                        'status' => 'success',
                        'redirectTo' => route('admin.main.page', [$section])
                    ];
                }

                $request->session()->flash("success", "Intro is successful updated!");
            }
        }
        return response()->json($response);
    }

    // funzione per visualizzare e salvare i campi
    public function configFreeShipping(Request $request){
        // recupero dal db ogni singola row riferita ad una key necessaria
        $free_shipping_start_date = Config::where('key', '=', 'free_shipping_start_date')->first(); // recupero dal DB la chiave indicata (o NULL oppure intera Row)
        $free_shipping_end_date = Config::where('key', '=', 'free_shipping_end_date')->first();
        $free_shipping_enabled = Config::where('key', '=', 'free_shipping_enabled')->first();

        // dd($free_shipping_start_date );

        // verifico se la richiesta � una submit del form
        if( $request->method() == 'POST' ) {
            // dd($request);
            // SE non esiste gi� una riga nel DB con quella chiave la creo
            if( !$free_shipping_start_date ) {
                $free_shipping_start_date = new Config(); // creo una nuova riga nel DB (insert into Config table...)
                $free_shipping_start_date->key = 'free_shipping_start_date'; // gli assegno il valore chiave nel DB (value 'free_shipping_start_date')
            }
            $free_shipping_start_date->value = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('free_shipping_start_date')))); // e poi inserisco
            $free_shipping_start_date->save(); // esegue il salvataggio effettivo sul DB OPPURE aggiorno il valore

            // recupero secondo valore
            if( !$free_shipping_end_date ) {
                $free_shipping_end_date = new Config();
                $free_shipping_end_date->key = 'free_shipping_end_date';
            }
            $free_shipping_end_date->value = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('free_shipping_end_date'))));
            $free_shipping_end_date->save();

            // recupero terzo valore (variante perch� gli input checkbox se disattivati non passano nessun valore)
            $checkbox_value_retrieve = 0; // setto come valore iniziale zero (non checcato)
            // se trovo il valore  (checcato)
            if ( $request->has('free_shipping_enabled') ) {
                $checkbox_value_retrieve = 1;
            }
            if( !$free_shipping_enabled ) {
                $free_shipping_enabled = new Config();
                $free_shipping_enabled->key = 'free_shipping_enabled';
            }
            $free_shipping_enabled->value = $checkbox_value_retrieve;
            $free_shipping_enabled->save();

        }

        // creo oggetto per passare i valori al blade
        $data = (object)[
            'free_shipping_enabled' => ($free_shipping_enabled) ? $free_shipping_enabled->value : false,
            'free_shipping_start_date' => ($free_shipping_start_date) ? date('d/m/Y', strtotime($free_shipping_start_date->value)) : '',
            'free_shipping_end_date' => ($free_shipping_end_date) ? date('d/m/Y', strtotime($free_shipping_end_date->value)) : '',
        ];
        // al ricaricamento della pagina passo i valori ricavati nell'oggetto data
        return view('configs.admin.global-configurations', compact('data'));
    }

    public function galleristGuide(Request $request){
        $destinationPath = storage_path('/uploads/guide');
        if($request->method() == 'POST'){
            $this->validate($request, [
                'pdf_guide' => 'required|mimes:pdf|max:200000',
            ]);

            // upload PDF
            $pdf = $request->file('pdf_guide');
            $input['name'] = 'gallerist_guide.'.$pdf->getClientOriginalExtension();
            $pdf->move($destinationPath, $input['name']);

            // upload PDF after base64 conversion
            $data = $request['pdf_guide_preview'];
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            file_put_contents($destinationPath.'/gallerist_guide_preview.png', $data);

            return back()->with('success','Pdf Upload successful');

        }
        $pdf_preview = false;
        if(\File::exists($destinationPath.'/gallerist_guide_preview.png')){
            $pdf_preview = file_get_contents($destinationPath.'/gallerist_guide_preview.png');

        }
        return view('configs.admin.gallerist-guide', compact('pdf_preview'));
    }

} // chiusura classe
