<?php

namespace App\Http\Controllers\Artists;

use App\Http\Controllers\Controller;
use App\Models\ArtistsCategory;
use App\Models\SiteMap;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use View;

class CategoriesController extends Controller
{

    private $_directory_images = 'artists/categories';

    private $_type_url = 'ArtistsCategory';

    private $_url_prefix = '/artists/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('artists.categories.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'slug',
            3 => 'created_at',
            4 => 'options',
        );

        $totalData = ArtistsCategory::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $categories = ArtistsCategory::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $categories = ArtistsCategory::where('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = ArtistsCategory::where('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $edit = route('admin.categories.artists.edit', $category->id);
                $delete = route('admin.categories.artists.destroy', $category->id);

                $nestedData['id'] = $category->id;
                $nestedData['name'] = $category->name;
                $nestedData['slug'] = $category->name;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($category->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $category = new ArtistsCategory();
        return view('artists.categories.create', compact('category', 'parentCategories'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $category = ArtistsCategory::create(Input::all());
            if ($category) {
                SiteMap::create([
                    "url" => $this->_url_prefix . $category->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $category->id
                ]);
                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $category->id,
                    'redirectTo' => route('admin.categories.artists.index')
                ];
                $request->session()->flash("success", "Category with name `{$category->name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($category)
    {
        $category = ArtistsCategory::find($category);
        return view('artists.categories.edit', compact('category'));
    }

    public function update(Request $request, $category)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $category = ArtistsCategory::find($category);
        if ($request->method() == 'PATCH' && $category) {
            if ($category->update(Input::all())) {
                
                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($category->id)->first();
                $site_map->url = $this->_url_prefix . $category->slug;
                $site_map->save();
                
                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $category->id,
                    'redirectTo' => route('admin.categories.artists.index')
                ];
                $request->session()->flash("success", "Category with name `{$category->name}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public
    function destroy($category)
    {
        $category = ArtistsCategory::find($category);
        if (Storage::disk()->exists(public_path("uploads/artists/categories/{$category->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/artists/categories/{$category->id}"));
        }
        $category->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($category->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('artists.single', [$category->slug]), PHP_URL_PATH), parse_url(route('artists.intro'), PHP_URL_PATH));

        // redirect
        Session::flash('success', "Category with name `{$category->name}` is successful removed!");
        return redirect(route('admin.categories.artists.index'));
    }

    public
    function checkTagIfExist(Request $request)
    {
        if ($request->method() == 'POST') {
            $name = strtolower($request->get('name'));
            if (Tag::whereRaw("LOWER(name) like '{$name}'")->count()) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        }
    }
}
