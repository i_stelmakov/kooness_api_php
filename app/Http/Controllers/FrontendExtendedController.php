<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtworksCategory;
use App\Models\Gallery;
use App\Models\GenericSeoDatum;
use App\Models\HeaderLink;
use App\Models\PromoCode;
use App\Services\Api4Dem\Api4Dem;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;

class FrontendExtendedController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $_cart_instance = null;
    public $_seo_data = null;
    // Controller => $this->_envKind;
    // View => $envKind:
    public $_envKind = '';


    public function __construct()
    {
        $this->_envKind = env('APP_ENVIRONMENT_KIND');
        $request = Request::capture();
        if ($request->path() == '/')
            $this->_seo_data = $seo_data = GenericSeoDatum::whereUrl("/")->first();
        else
            $this->_seo_data = $seo_data = GenericSeoDatum::whereUrl("/" . $request->path())->first();
        $medium = ArtworksCategory::whereIsMedium(1)->orderBy('order', "ASC")->get();
        $galleries = Gallery::whereStatus(1)->orderBy('created_at', "DESC")->limit(5)->get();
        $artists = [];

        $artists1 = Artist::whereStatus(1)->whereRaw('id IN (112)')->first(); // passo al menu artisti selettivamente tramite ID ('id IN ...')
        $artists2 = Artist::whereStatus(1)->whereRaw('id IN (177)')->first();
        $artists3 = Artist::whereStatus(1)->whereRaw('id IN (119)')->first();
        $artists4 = Artist::whereStatus(1)->whereRaw('id IN (588)')->first();

        if($artists1) { $artists[] = $artists1; }
        if($artists2) { $artists[] = $artists2; }
        if($artists3) { $artists[] = $artists3; }
        if($artists4) { $artists[] = $artists4; }

        $headerLink = HeaderLink::orderBy('order', 'ASC')->get();
        $recentViewed = Cookie::get("kooness_recent");
        if ($recentViewed) {
            $recentViewed = json_decode(Crypt::decrypt($recentViewed));
        }
        $this->_cart_instance = app('cart');
        if($this->_cart_instance->promo_code){
            $promo = PromoCode::find($this->_cart_instance->promo_code);
            if($promo && $promo->status == 3){
                $this->_cart_instance->promo_code = null;
                $this->_cart_instance->save();
            }
        }
        if(Auth::user() && $this->_cart_instance->user_id == Auth::user()->id && $this->_cart_instance->id && $this->_cart_instance->items()->count()) {
            if (!$this->_cart_instance->api4dem_referer_id) {
                // creo istanza 4dem
                $api4Dem = new Api4Dem();
                $api4Dem->cartCreate($this->_cart_instance, Auth::user());
            }
            foreach ($this->_cart_instance->items()->get() as $item) {
                if (!$item->api4dem_success_send) {
                    $api4Dem = new Api4Dem();
                    $api4Dem->cartArtworkAdd($item, $this->_cart_instance);
                }
            }
        }
        View::share("envKind", $this->_envKind);
        View::share("cart", $this->_cart_instance);
        View::share("recentViewed", $recentViewed);
        View::share("medium_menu", $medium);
        View::share("galleries_menu", $galleries);
        View::share("artists_menu", $artists);
        View::share("header_link", $headerLink);
        View::share("seo_data", $seo_data);
    }
}
