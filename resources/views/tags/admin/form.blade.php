@if(roleCheck('superadmin|admin|seo'))
    <form class="ajax-form row no-margin" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $tag->id }}" data-referer="tag">
        {{ method_field($method) }}
        @csrf

        {{-- Section intro --}}
        <div class="">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        {{-- Details --}}
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Details
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input 
                                id="generated_url" 
                                class="form-control" 
                                type="text" 
                                data-prefix="{{ url('') }}/tags/" 
                                value="{{ ($tag->{'slug'})? url('') . '/tags/' . $tag->{'slug'} : '' }}" 
                                disabled
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Name (*)</label>
                            <input 
                                id="name" 
                                class="form-control" 
                                type="text" 
                                data-slug="#slug" 
                                placeholder="Name" 
                                name="name" 
                                value="{{ old('name', $tag->{'name'}) }}" 
                                required
                                {{ ( roleCheck('seo') ) ? 'disabled' : '' }}
                            >
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="slug">Slug (*)</label>
                            <input 
                                id="slug" 
                                class="form-control" 
                                type="text" 
                                placeholder="Slug" 
                                name="slug" 
                                value="{{ old('slug', $tag->{'slug'}) }}" 
                                data-url="#generated_url" 
                                required
                                {{ ( roleCheck('seo') ) ? 'disabled' : '' }}
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- SEO Management --}}
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                SEO Management
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="meta_title">SEO - title</label>
                            <input 
                                class="form-control" 
                                name="meta_title" 
                                type="text" 
                                placeholder="SEO - title" 
                                id="meta_title" 
                                value="{{ $tag->{'meta_title'} }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="meta_keywords">SEO - keywords</label>
                            <input 
                                class="form-control" 
                                name="meta_keywords" 
                                type="text" 
                                placeholder="SEO - keywords" 
                                id="meta_keywords" 
                                value="{{ $tag->{'meta_keywords'} }}"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="h1">SEO - H1</label>
                            <input 
                                class="form-control"
                                name="h1" type="text"
                                placeholder="SEO - H1"
                                id="h1"
                                value="{{ $tag->{'h1'} }}"
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="canonical">SEO - Canonical Url</label>
                            <input 
                                class="form-control"
                                name="canonical" type="text"
                                placeholder="SEO - Canonical Url"
                                id="canonical"
                                value="{{ $tag->{'canonical'} }}"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="meta_description">SEO - Description</label>
                            <textarea 
                            id="meta_description" 
                            class="form-control" 
                            placeholder="SEO Description" 
                            name="meta_description" 
                            rows="5"
                        >
                            {{ $tag->{'meta_description'} }}
                        </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Form Footer & Submit -->
        <div class="box box-primary box-solid">
            <div class="box-footer">
                <div class="col-md-12">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endif