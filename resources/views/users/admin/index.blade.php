@extends('layouts.admin')
@section('title', 'Admin | Users')
@section('page-header', 'Users')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="pull-right">
                        <a href="{{ route('admin.users.create') }}">
                            <button class="btn btn-primary">Add new user</button>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table id="table-galleries" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Enabled</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function () {
            $('#table-galleries').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [[ 6, "desc" ]],
                "ajax":{
                    "url": "{{ route('admin.users.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "id" },
                    { "data": "first_name" },
                    { "data": "last_name" },
                    { "data": "email" },
                    { "data": "role" },
                    { "data": "status" },
                    { "data": "created_at" },
                    { "data": "options" ,"bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                        // esempio di attivazione di attributi CSS personalizzati per una singola cella 
                        // if ( cellData < 1 ) {
                        //     $(td).css('color', 'red')
                        // }
                    }
                ]
            });
        });
    </script>
@endsection