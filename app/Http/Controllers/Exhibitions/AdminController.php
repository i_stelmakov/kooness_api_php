<?php

namespace App\Http\Controllers\Exhibitions;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Country;
use App\Models\Exhibition;
use App\Models\Gallery;
use App\Models\SiteMap;
use App\Models\User;
use App\Services\Notification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use View;

class AdminController extends Controller
{

    private $_directory_images = 'exhibitions';

    private $_type_url = 'Exhibition';

    private $_url_prefix = '/exhibitions/';

    // variabili private dell'istanza della classe
    private $gallery_id = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
        // recupero eventuale gallerist ID che sta accedendo al dato
        $gallery = auth()->user()->gallery()->first();
        if($gallery){
            $this->gallery_id = $gallery->id;
        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('exhibitions.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'slug',
            3 => 'start_date',
            4 => 'end_date',
            5 => 'status',
            6 => 'created_at',
            7 => 'options',
        );

        // creazione di una nuova query basata su model Artist
        $totalData_query = new Exhibition();

        // creazione di una nuova query partendo dall'istanza di Artist, per integrare le condizioni sotto
        $totalData_query = $totalData_query->newQuery();

        // se non è admin evita l'estrazione delle righe senza slug
        if( roleCheck('gallerist') ){
            $totalData_query->whereRaw("gallery_id = '$this->gallery_id'");
        }

        // conteggia gli elementi trovati
        $totalData = $totalData_query->count();

        $totalFiltered = $totalData;


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        // se non c'è una ricerca
        if (empty($request->input('search.value'))) {
            $exhibitions_query = Exhibition::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $exhibitions_query->whereRaw("gallery_id = '$this->gallery_id'");
            }

            $exhibitions =  $exhibitions_query->get();

        }
        // se c'è una ricerca
        else {
            $search = $request->input('search.value');

            $exhibitions_query = Exhibition::where('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $exhibitions_query->whereRaw("gallery_id = '$this->gallery_id'");
            }

            $exhibitions =  $exhibitions_query->get();

            $totalFiltered_query = Exhibition::where('name', 'LIKE', "%{$search}%");

            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $totalFiltered_query->whereRaw("gallery_id = '$this->gallery_id'");
            }

            $totalFiltered =  $totalFiltered_query->count();
        }

        $data = array();
        if (!empty($exhibitions)) {
            foreach ($exhibitions as $exhibition) {
                $edit = route('admin.exhibitions.edit', $exhibition->id);
                $delete = route('admin.exhibitions.destroy', $exhibition->id);

                $nestedData['id'] = $exhibition->id;
                $nestedData['name'] = $exhibition->name;
                $nestedData['slug'] = $exhibition->slug;
                $nestedData['start_date'] = date('d/m/Y', strtotime($exhibition->start_date));
                $nestedData['end_date'] = date('d/m/Y', strtotime($exhibition->end_date));
                $nestedData['status'] = get_state_status($exhibition->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($exhibition->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $exhibition = new Exhibition();
        $artists = Artist::all();
        if( roleCheck('gallerist') ) {
            $galleries = Gallery::select("id", "name")->whereId($this->gallery_id)->get();
        } else {
            $galleries = Gallery::select("id", "name")->get();
        }
        $countries = Country::all();
        return view('exhibitions.admin.create', compact('exhibition', 'galleries', 'countries', 'artists'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['created_by'] = Auth::user()->id;
            $data['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
            $data['end_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
            $exhibition = Exhibition::create($data);
            if ($exhibition) {

                manyToManyAssociation((isset($data['artists'])) ? $data['artists'] : null, $exhibition, '\App\Models\Artist', 'artists', 'artist_ids');

                SiteMap::create([
                    "url" => $this->_url_prefix . $exhibition->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $exhibition->id
                ]);

                if (roleCheck('gallerist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_EXHIBITION, 'exhibition', $exhibition);
                    Notification::addUsers($notify, $users);
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $exhibition->id,
                    'redirectTo' => route('admin.exhibitions.index')
                ];
                $request->session()->flash("success", "Exhibition with name `{$exhibition->name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public
    function edit($exhibition)
    {
        $exhibition = Exhibition::find($exhibition);
        $exhibition->start_date = date('d/m/Y', strtotime($exhibition->start_date));
        $exhibition->end_date = date('d/m/Y', strtotime($exhibition->end_date));
        if( roleCheck('gallerist') ) {
            $galleries = Gallery::select("id", "name")->whereId($this->gallery_id)->get();
        } else {
            $galleries = Gallery::select("id", "name")->get();
        }
        $artists = Artist::all();
        $countries = Country::all();

        return view('exhibitions.admin.edit', compact('exhibition', 'galleries', 'countries', 'artists'));
    }

    public
    function update(Request $request, $exhibition)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $exhibition = Exhibition::find($exhibition);
        if ($request->method() == 'PATCH' && $exhibition) {
            $data = Input::all();

            if(isset($data['start_date'])) {
                $data['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
            }
            if(isset($data['end_date'])) {
                $data['end_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
            }


            if ($exhibition->update($data)) {

                manyToManyAssociation((isset($data['artists'])) ? $data['artists'] : null, $exhibition, '\App\Models\Artist', 'artists', 'artist_ids');

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($exhibition->id)->first();
                $site_map->url = $this->_url_prefix . $exhibition->slug;
                $site_map->save();

                if (roleCheck('gallerist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_EXHIBITION, 'exhibition', $exhibition);
                    Notification::addUsers($notify, $users);
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $exhibition->id,
                    'redirectTo' => route('admin.exhibitions.index')
                ];
                $request->session()->flash("success", "Exhibition with name `{$exhibition->name}` is successful updated!");
            }
        }
        return response()->json($response);
    }

    public
    function destroy($exhibition)
    {
        $exhibition = Exhibition::find($exhibition);
        if (Storage::disk()->exists(public_path("uploads/exhibition/{$exhibition->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/exhibition/{$exhibition->id}"));
        }
        $exhibition->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($exhibition->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('exhibitions.single', [$exhibition->slug]), PHP_URL_PATH), parse_url(route('exhibitions.archive'), PHP_URL_PATH));

        Session::flash('success', "Exhibition with name `{$exhibition->name}` is successful removed!");
        return redirect(route('admin.exhibitions.index'));
    }

}
