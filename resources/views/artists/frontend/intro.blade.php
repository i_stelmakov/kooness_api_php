@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Artists')

@section('body-class', 'artists-intro')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'Artists' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="boxed-container container-vertical-padding-top">
            <section id="category-filter" class="show">
                <!-- Hiding Box Header !-->
                <div class="hiding-box-header">
                    <div class="default-sheet-row-cell">
                        <h3>Featured subjects</h3>
                    </div>
                    <div title="open/close" class="hiding-box-trigger"></div>
                </div>
                <div class="col-container-with-offset">
                    <div class="category-filter-container">
                        @foreach($categories as $category)
                            <div class="slider-item-img wide-box category-filter-item">
                                <a href="{{route('artists.single', [$category->slug])}}">
                                    <div class="category-filter-item-img background-cover lazy" data-src="{{($category->url_cover)? $category->url_cover : $category->randomImage()}}">
                                        <h4>{{ $category->name }}</h4>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>

        @foreach($widgets as $widget)
            @if($widget['template'] == 'tplA')
                @include('partials.widget-template-a', ['data' => $widget])
            @endif
            @if($widget['template'] == 'tplB')
                @include('partials.widget-template-b', ['data' => $widget])
            @endif
        @endforeach
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span class="h1">Browse Artists</span>
                        </div>
                    </div>
                    <div class="page-header-filter">
                        <div class="default-sheet-row-cell">
                            <p>Nationality</p>
                            <form>
                                <select class="search-by location">
                                    <option value="">All</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->code }}" {{ (app('request')->input('location') == $city->code )? 'selected="selected"' : ''}}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="header-secondary-menu">
            <div class="container boxed-container">
                <a href="{{ route('artists.archive.all') }}" >All</a><a class="search-by" data-value="number">0-9</a>
                <a class="search-by" data-value="a">A</a> <a class="search-by" data-value="b">B</a>
                <a class="search-by" data-value="c">C</a> <a class="search-by" data-value="d">D</a>
                <a class="search-by" data-value="e">E</a> <a class="search-by" data-value="f">F</a>
                <a class="search-by" data-value="g">G</a> <a class="search-by" data-value="h">H</a>
                <a class="search-by" data-value="i">I</a> <a class="search-by" data-value="j">J</a>
                <a class="search-by" data-value="k">K</a> <a class="search-by" data-value="l">L</a>
                <a class="search-by" data-value="m">M</a> <a class="search-by" data-value="n">N</a>
                <a class="search-by" data-value="o">O</a> <a class="search-by" data-value="p">P</a>
                <a class="search-by" data-value="q">Q</a> <a class="search-by" data-value="r">R</a>
                <a class="search-by" data-value="s">S</a> <a class="search-by" data-value="t">T</a>
                <a class="search-by" data-value="u">U</a> <a class="search-by" data-value="v">V</a>
                <a class="search-by" data-value="w">W</a> <a class="search-by" data-value="x">X</a>
                <a class="search-by" data-value="y">Y</a> <a class="search-by" data-value="z">Z</a>
            </div>
        </div>
        <section id="archive" class="artists-page">
            <div class="container boxed-container">
                <div class="container ">
                    <section id="product-masonry" class="default-slider-section">
                        <div class="container col-container-with-offset artists-container">
                            @foreach($artists as $artist)
                                <div data-id="{{ $artist->id }}" class="slider-item one-fourth-col-with-margin half-width-mobile">
                                    <i title="Remove" class="fa fa-times delete-item"></i>
                                    @if(Auth::user())
                                        <div class="add-item follow {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                            @if(Auth::user()->collectionArtists->contains($artist->id))
                                                <i class="fa fa-heart"></i>
                                            @else
                                                <i class="fa fa-heart-o"></i>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="slider-item-img square-box">
                                        <a title="{{  $artist->first_name . " " . $artist->last_name }}" href="{{  route('artists.single', [$artist->slug]) }}">
                                            <div class="contain-image gallery lazy" data-src="{{ $artist->url_full_image }}"></div>
                                        </a>
                                    </div>
                                    {{--<div class="slider-item-img">--}}
                                        {{--<a title="{{  $artist->first_name . " " . $artist->last_name }}" href="{{  route('artists.single', [$artist->slug]) }}">--}}
                                            {{--<img src="{{ $artist->url_full_image }}"/>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="slider-item-txt">
                                        <div class="default-sheet-row slider-item-row">
                                            <div class="default-sheet-row-cell">
                                                <h2><a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->last_name }} {{ $artist->first_name }}</a></h2>
                                            </div>
                                        </div>
                                        @if($artist->countryBirth()->count())
                                            <p class="dark-text provenance">{{ $artist->countryBirth()->first()->name }}</p>
                                        @endif
                                        <p class="exposure">{{ $artist->artworks()->count() }} Works exhibited</p>
                                        <a class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="view-all-box">
                            <span data-alpha-filter="" class="btn_view_all" ><a style="color: white" href="{{ route('artists.archive.all') }}">View All</a></span>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection