<div class="col-md-12 col-xs-12">
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            Default Images
        </div>
        <div class="box-body">
            <div class="form-group">
                {{-- Image box 1 --}}
                <div class="col-md-12">
                    <label>Picture <br>
                        <small>&nbsp;</small>
                    </label>
                    @if(!$item->url_full_image)
                        <img src="/images/placeholder-wide-box.jpg" class="img-responsive img-preview-1" style="max-height: 300px;">
                    @else
                        <img src="{{ asset($item->url_full_image) }}" class="img-responsive img-preview-1">
                    @endif
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    Upload new image…
                                    <input type="file"
                                           class="img-uploader"
                                           data-label="full_image"
                                           data-preview=".img-preview-1"
                                           data-maxWidth="2048"
                                           data-maxHeight="2048"
                                           data-cropper="false">
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>