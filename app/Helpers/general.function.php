<?php
/**
 * @param $status
 * @return string
 */

// GALLERIST
use App\Models\Redirect301;
const SUBJECT_MAIL_GALLERIST_ACTIVATION = 'Request for registration of a gallery';

// MEMBERSHIP
const SUBJECT_MAIL_MEMBERSHIP_ACTIVATION = 'Membership Activated';
const SUBJECT_MAIL_MEMBERSHIP_CANCELLATION = 'Membership Cancelled';
const SUBJECT_MAIL_MEMBERSHIP_CARD_EXPIRATION = 'Membership Card Expiration';
const SUBJECT_MAIL_MEMBERSHIP_AUTOMATIC_RENEWAL = 'Automatic Membership Renewal';
const SUBJECT_MAIL_MEMBERSHIP_UNSUCCESSFUL_RENEWAL = 'Unsuccessful Memebership Renewal';
const SUBJECT_MAIL_MEMBERSHIP_SUCCESSFUL_RENEWAL = 'Successful Membership Renewal';
const SUBJECT_MAIL_MEMBERSHIP_TRIAL_DEADLINE = 'Trial Membership Deadline';
const SUBJECT_MAIL_MEMBERSHIP_CHANGE_PLAN = 'Membership Plan Changed';

// ORDER
const SUBJECT_MAIL_ORDER_RECEIVED_ADMIN = 'You have received a new order';
const SUBJECT_MAIL_ORDER_RECEIVED = 'Thanks for your order';
const SUBJECT_MAIL_ORDER_COMPLETED = 'Your order has been completed';
const SUBJECT_MAIL_ORDER_SHIPPED = 'Your order has been shipped';
const SUBJECT_MAIL_PAY_ORDER = 'Pay your order';

// OFFER
const SUBJECT_MAIL_OFFER_RECEIVED_ADMIN = 'You have received a new offer';
const SUBJECT_MAIL_OFFER_ACCEPTED = 'Offer Accepted';
const SUBJECT_MAIL_OFFER_DECLINED = 'Offer Declined';

// AVAILABLE
const SUBJECT_MAIL_AVAILABLE_RECEIVED_ADMIN = 'You have received a new request';

// REGISTRATION
const SUBJECT_MAIL_REGISTRATION_USER_REGISTERED = 'Welcome to Kooness!';
const SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED = 'Welcome to Kooness!';
const SUBJECT_MAIL_REGISTRATION_USER_RESET_PSW = 'Reset Password';

// INFO REQUEST
const SUBJECT_MAIL_REGISTRATION_INFO_REQUEST = 'Info request';


function get_state_status($status)
{
    return ($status) ? 'Yes' : 'No';
}

/**
 * @param $role
 * @return string
 */
function get_user_role_name($role)
{
    if ($role == 'superadmin') {
        return 'SuperAdmin';
    } else if ($role == 'admin') {
        return 'Administrator';
    } else if ($role == 'fair') {
        return 'Fair Director';
    } else if ($role == 'gallerist') {
        return 'Gallerist';
    } else if ($role == 'artist') {
        return 'Artist';
    } else if ($role == 'editor') {
        return 'Editor';
    } else if ($role == 'user') {
        return 'Subscriber';
    } else if ($role == 'seo') {
        return 'Seo';
    }

}

/**
 * @param $filename
 * @return mixed|null|string|string[]
 */
function sanitize_file_name_chars($filename)
{

    $sanitized_filename = remove_accents($filename); // Convert to ASCII

    // Standard replacements
    $invalid = array(
        ' ' => '-',
        '%20' => '-',
        '_' => '-'
    );
    $sanitized_filename = str_replace(array_keys($invalid), array_values($invalid), $sanitized_filename);

    $sanitized_filename = preg_replace('/[^A-Za-z0-9-\. ]/', '', $sanitized_filename); // Remove all non-alphanumeric except .
    $sanitized_filename = preg_replace('/\.(?=.*\.)/', '', $sanitized_filename); // Remove all but last .
    $sanitized_filename = preg_replace('/-+/', '-', $sanitized_filename); // Replace any more than one - in a row
    $sanitized_filename = str_replace('-.', '.', $sanitized_filename); // Remove last - if at the end
    $sanitized_filename = strtolower($sanitized_filename); // Lowercase

    return $sanitized_filename;
}

function remove_accents($string)
{
    if (!preg_match('/[\x80-\xff]/', $string))
        return $string;

    if (utf8_decode($string)) {
        $chars = array(
            // Decompositions for Latin-1 Supplement
            '' => 'a', '' => 'o',
            '' => 'A', '' => 'A',
            '' => 'A', '' => 'A',
            '' => 'A', '' => 'A',
            '' => 'AE', '' => 'C',
            '' => 'E', '' => 'E',
            '' => 'E', '' => 'E',
            '' => 'I', '' => 'I',
            '' => 'I', '' => 'I',
            '' => 'D', '' => 'N',
            '' => 'O', '' => 'O',
            '' => 'O', '' => 'O',
            '' => 'O', '' => 'U',
            '' => 'U', '' => 'U',
            '' => 'U', '' => 'Y',
            '' => 'TH', '' => 's',
            '' => 'a', '' => 'a',
            '' => 'a', '' => 'a',
            '' => 'a', '' => 'a',
            '' => 'ae', '' => 'c',
            '' => 'e', '' => 'e',
            '' => 'e', '' => 'e',
            '' => 'i', '' => 'i',
            '' => 'i', '' => 'i',
            '' => 'd', '' => 'n',
            '' => 'o', '' => 'o',
            '' => 'o', '' => 'o',
            '' => 'o', '' => 'o',
            '' => 'u', '' => 'u',
            '' => 'u', '' => 'u',
            '' => 'y', '' => 'th',
            '' => 'y', '' => 'O',
            // Decompositions for Latin Extended-A
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'C', '*' => 'c',
            '*' => 'C', '*' => 'c',
            '*' => 'C', '*' => 'c',
            '*' => 'C', '*' => 'c',
            '*' => 'D', '*' => 'd',
            '*' => 'D', '*' => 'd',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'G', '*' => 'g',
            '*' => 'G', '*' => 'g',
            '*' => 'G', '*' => 'g',
            '*' => 'G', '*' => 'g',
            '*' => 'H', '*' => 'h',
            '*' => 'H', '*' => 'h',
            '*' => 'I', '*' => 'i',
            '*' => 'I', '*' => 'i',
            '*' => 'I', '*' => 'i',
            '*' => 'I', '*' => 'i',
            '*' => 'I', '*' => 'i',
            '*' => 'IJ', '*' => 'ij',
            '*' => 'J', '*' => 'j',
            '*' => 'K', '*' => 'k',
            '*' => 'k', '*' => 'L',
            '*' => 'l', '*' => 'L',
            '*' => 'l', '*' => 'L',
            '*' => 'l', '*' => 'L',
            '*' => 'l', '*' => 'L',
            '*' => 'l', '*' => 'N',
            '*' => 'n', '*' => 'N',
            '*' => 'n', '*' => 'N',
            '*' => 'n', '*' => 'n',
            '*' => 'N', '*' => 'n',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'OE', '*' => 'oe',
            '*' => 'R', '*' => 'r',
            '*' => 'R', '*' => 'r',
            '*' => 'R', '*' => 'r',
            '*' => 'S', '*' => 's',
            '*' => 'S', '*' => 's',
            '*' => 'S', '*' => 's',
            '*' => 'S', '*' => 's',
            '*' => 'T', '*' => 't',
            '*' => 'T', '*' => 't',
            '*' => 'T', '*' => 't',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'W', '*' => 'w',
            '*' => 'Y', '*' => 'y',
            '*' => 'Y', '*' => 'Z',
            '*' => 'z', '*' => 'Z',
            '*' => 'z', '*' => 'Z',
            '*' => 'z', '*' => 's',
            // Decompositions for Latin Extended-B
            '*' => 'S', '*' => 's',
            '*' => 'T', '*' => 't',
            // Euro Sign
            '*' => 'E',
            // GBP (Pound) Sign
            '' => '',
            // Vowels with diacritic (Vietnamese)
            // unmarked
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            // grave accent
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'E', '*' => 'e',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            '*' => 'Y', '*' => 'y',
            // hook
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'I', '*' => 'i',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'Y', '*' => 'y',
            // tilde
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            '*' => 'Y', '*' => 'y',
            // acute accent
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'E', '*' => 'e',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            // dot below
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'A', '*' => 'a',
            '*' => 'E', '*' => 'e',
            '*' => 'E', '*' => 'e',
            '*' => 'I', '*' => 'i',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            '*' => 'Y', '*' => 'y',
            // Vowels with diacritic (Chinese, Hanyu Pinyin)
            '*' => 'a',
            // macron
            '*' => 'U', '*' => 'u',
            // acute accent
            '*' => 'U', '*' => 'u',
            // caron
            '*' => 'A', '*' => 'a',
            '*' => 'I', '*' => 'i',
            '*' => 'O', '*' => 'o',
            '*' => 'U', '*' => 'u',
            '*' => 'U', '*' => 'u',
            // grave accent
            '*' => 'U', '*' => 'u',
        );

        // Used for locale-specific rules

        $string = strtr($string, $chars);
    } else {
        $chars = array();
        // Assume ISO-8859-1 if not UTF-8
        $chars['in'] = "\x80\x83\x8a\x8e\x9a\x9e"
            . "\x9f\xa2\xa5\xb5\xc0\xc1\xc2"
            . "\xc3\xc4\xc5\xc7\xc8\xc9\xca"
            . "\xcb\xcc\xcd\xce\xcf\xd1\xd2"
            . "\xd3\xd4\xd5\xd6\xd8\xd9\xda"
            . "\xdb\xdc\xdd\xe0\xe1\xe2\xe3"
            . "\xe4\xe5\xe7\xe8\xe9\xea\xeb"
            . "\xec\xed\xee\xef\xf1\xf2\xf3"
            . "\xf4\xf5\xf6\xf8\xf9\xfa\xfb"
            . "\xfc\xfd\xff";

        $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

        $string = strtr($string, $chars['in'], $chars['out']);
        $double_chars = array();
        $double_chars['in'] = array("\x8c", "\x9c", "\xc6", "\xd0", "\xde", "\xdf", "\xe6", "\xf0", "\xfe");
        $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
        $string = str_replace($double_chars['in'], $double_chars['out'], $string);
    }

    return $string;
}

function manyToManyAssociation($data, $row, $elementClass, $relation, $field)
{
    // if user is SEO avoid associations
    if (auth()->user()->hasRole('seo') && $relation != 'categories') {
        return;
    }

    $existing_ids = [];
    if ($data) {
        foreach ($data as $single) {
            $element = $elementClass::find($single);
            if (!$row->{$relation}() || $row->{$relation}()->find($element->id) == null) {
                $row->{$relation}()->save($element);
            }
            $existing_ids[] = $element->id;
        }
    }
    $diff_tag = array_diff($row->{$field}, $existing_ids);
    $row->{$relation}()->detach($diff_tag);

}

function get_status_promo_code($status)
{
    if ($status == '1') {
        return "Activated for Newsletter subscription";
    } else if ($status == '2') {
        return "Active and applicable via MailChimp";
    } else {
        return "Not Active";
    }
}


function get_payment_method($payment_method)
{
    if ($payment_method == 'paypal') {
        return "PayPal";
    } else if ($payment_method == 'stripe') {
        return "Credit Card";
    } else if($payment_method == 'bank_transfer') {
        return "Bank Transfer";
    } else{
        return "Pending";
    }
}

function get_status_order($status)
{
    if ($status == '1') {
        return "Pending";
    } else if ($status == '2') {
        return "Completed";
    } else if ($status == '3') {
        return "Shipped";
    }
}

function get_day($pointer)
{
    $days = [
        'sun' => "Sunday",
        'mon' => "Monday",
        'tue' => "Tuesday",
        'wed' => "Wednesday",
        'thu' => "Thursday",
        'fri' => "Friday",
        'sat' => "Saturday",
    ];

    if (isset($days[$pointer])) {
        return $days[$pointer];
    }
    return '';
}

function get_offer_status($status)
{
    if ($status == 1) {
        return 'Pending';
    } else if ($status == 2) {
        return 'Accepted';
    } else {
        return 'Rejected';
    }
}

function conditionalViewer($args)
{
    return false;

}

function roleCheck($roles_args)
{
    $user = auth()->user();
    if (!$user) {
        return false;
    }
    $hasRole_check = false;
    $roles = explode('|', $roles_args);
    if (count($roles)) {
        foreach ($roles as $role) {
            if ($user->hasRole($role)) {
                $hasRole_check = true;
                break;
            }
        }
    }
    return $hasRole_check;
}


function isPremiumUser($gallery_id = null)
{
    $user = auth()->user();
    if (!$user) {
        return false;
    }
    $user_role = $user->role;
    $isPremium_check = false;
    if ($user_role == 'artist') {
        $artist = $user->artist()->first();
        if ($artist) {
            $galleries_query = $artist->galleries();
            if ($gallery_id) {
                $galleries_query->whereId($gallery_id);
            }
            $galleries = $galleries_query->get();
            foreach ($galleries as $gallery) {
                $gallery_user = $gallery->user()->first();
                if ($gallery_user && $gallery_user->is_premium) {
                    $isPremium_check = true;
                }
            }
        }
    } elseif ($user_role == 'gallerist') {
        if ($user->is_premium) {
            $isPremium_check = true;
        }
    }
    return $isPremium_check;
}

function isTrialUser($gallery_id = null)
{
    $user = auth()->user();
    if (!$user) {
        return false;
    }
    $user_role = $user->role;
    $isTrial_check = false;
    if ($user_role == 'artist') {
        $artist = $user->artist()->first();
        if ($artist) {
            $galleries_query = $artist->galleries();
            if ($gallery_id) {
                $galleries_query->whereId($gallery_id);
            }
            $galleries = $galleries_query->get();
            foreach ($galleries as $gallery) {
                $gallery_user = $gallery->user()->first();
                if ($gallery_user && $gallery_user->is_trial && date('Y-m-d') < date('Y-m-d', strtotime($gallery_user->trial_activation_date . " +30 Days"))) {
                    $isTrial_check = true;
                    break;
                }
            }
        }
    } elseif ($user_role == 'gallerist') {
        if ($user->is_trial && date('Y-m-d') <= date('Y-m-d', strtotime($user->trial_activation_date . " +30 Days"))) {
            $isTrial_check = true;
        }
    }
    return $isTrial_check;
}


function country_to_continent($country)
{
    $continent = '';
    if ($country == 'AF') $continent = 'Asia';
    if ($country == 'AX') $continent = 'Europe';
    if ($country == 'AL') $continent = 'Europe';
    if ($country == 'DZ') $continent = 'Africa';
    if ($country == 'AS') $continent = 'Oceania';
    if ($country == 'AD') $continent = 'Europe';
    if ($country == 'AO') $continent = 'Africa';
    if ($country == 'AI') $continent = 'North America';
    if ($country == 'AQ') $continent = 'Antarctica';
    if ($country == 'AG') $continent = 'North America';
    if ($country == 'AR') $continent = 'South America';
    if ($country == 'AM') $continent = 'Asia';
    if ($country == 'AW') $continent = 'North America';
    if ($country == 'AU') $continent = 'Oceania';
    if ($country == 'AT') $continent = 'Europe';
    if ($country == 'AZ') $continent = 'Asia';
    if ($country == 'BS') $continent = 'North America';
    if ($country == 'BH') $continent = 'Asia';
    if ($country == 'BD') $continent = 'Asia';
    if ($country == 'BB') $continent = 'North America';
    if ($country == 'BY') $continent = 'Europe';
    if ($country == 'BE') $continent = 'Europe';
    if ($country == 'BZ') $continent = 'North America';
    if ($country == 'BJ') $continent = 'Africa';
    if ($country == 'BM') $continent = 'North America';
    if ($country == 'BT') $continent = 'Asia';
    if ($country == 'BO') $continent = 'South America';
    if ($country == 'BA') $continent = 'Europe';
    if ($country == 'BW') $continent = 'Africa';
    if ($country == 'BV') $continent = 'Antarctica';
    if ($country == 'BR') $continent = 'South America';
    if ($country == 'IO') $continent = 'Asia';
    if ($country == 'VG') $continent = 'North America';
    if ($country == 'BN') $continent = 'Asia';
    if ($country == 'BG') $continent = 'Europe'; // Bulgaria - NOT euro
    if ($country == 'BF') $continent = 'Africa';
    if ($country == 'BI') $continent = 'Africa';
    if ($country == 'KH') $continent = 'Asia';
    if ($country == 'CM') $continent = 'Africa';
    if ($country == 'CA') $continent = 'North America';
    if ($country == 'CV') $continent = 'Africa';
    if ($country == 'KY') $continent = 'North America';
    if ($country == 'CF') $continent = 'Africa';
    if ($country == 'TD') $continent = 'Africa';
    if ($country == 'CL') $continent = 'South America';
    if ($country == 'CN') $continent = 'Asia';
    if ($country == 'CX') $continent = 'Asia';
    if ($country == 'CC') $continent = 'Asia';
    if ($country == 'CO') $continent = 'South America';
    if ($country == 'KM') $continent = 'Africa';
    if ($country == 'CD') $continent = 'Africa';
    if ($country == 'CG') $continent = 'Africa';
    if ($country == 'CK') $continent = 'Oceania';
    if ($country == 'CR') $continent = 'North America';
    if ($country == 'CI') $continent = 'Africa';
    if ($country == 'HR') $continent = 'Europe'; // Croatia - NOT euro
    if ($country == 'CU') $continent = 'North America';
    if ($country == 'CY') $continent = 'Asia';
    if ($country == 'CZ') $continent = 'Europe'; // Czech Republic - NOT euro
    if ($country == 'DK') $continent = 'Europe';  // Denmark - NOT euro
    if ($country == 'DJ') $continent = 'Africa';
    if ($country == 'DM') $continent = 'North America';
    if ($country == 'DO') $continent = 'North America';
    if ($country == 'EC') $continent = 'South America';
    if ($country == 'EG') $continent = 'Africa';
    if ($country == 'SV') $continent = 'North America';
    if ($country == 'GQ') $continent = 'Africa';
    if ($country == 'ER') $continent = 'Africa';
    if ($country == 'EE') $continent = 'Europe';
    if ($country == 'ET') $continent = 'Africa';
    if ($country == 'FO') $continent = 'Europe';
    if ($country == 'FK') $continent = 'South America';
    if ($country == 'FJ') $continent = 'Oceania';
    if ($country == 'FI') $continent = 'Europe';
    if ($country == 'FR') $continent = 'Europe';
    if ($country == 'GF') $continent = 'South America';
    if ($country == 'PF') $continent = 'Oceania';
    if ($country == 'TF') $continent = 'Antarctica';
    if ($country == 'GA') $continent = 'Africa';
    if ($country == 'GM') $continent = 'Africa';
    if ($country == 'GE') $continent = 'Asia';
    if ($country == 'DE') $continent = 'Europe';
    if ($country == 'GH') $continent = 'Africa';
    if ($country == 'GI') $continent = 'Europe';
    if ($country == 'GR') $continent = 'Europe';
    if ($country == 'GL') $continent = 'North America';
    if ($country == 'GD') $continent = 'North America';
    if ($country == 'GP') $continent = 'North America';
    if ($country == 'GU') $continent = 'Oceania';
    if ($country == 'GT') $continent = 'North America';
    if ($country == 'GG') $continent = 'Europe';
    if ($country == 'GN') $continent = 'Africa';
    if ($country == 'GW') $continent = 'Africa';
    if ($country == 'GY') $continent = 'South America';
    if ($country == 'HT') $continent = 'North America';
    if ($country == 'HM') $continent = 'Antarctica';
    if ($country == 'VA') $continent = 'Europe';
    if ($country == 'HN') $continent = 'North America';
    if ($country == 'HK') $continent = 'Asia';
    if ($country == 'HU') $continent = 'Europe'; // Hungary - NOT euro
    if ($country == 'IS') $continent = 'Europe';
    if ($country == 'IN') $continent = 'Asia';
    if ($country == 'ID') $continent = 'Asia';
    if ($country == 'IR') $continent = 'Asia';
    if ($country == 'IQ') $continent = 'Asia';
    if ($country == 'IE') $continent = 'Europe';
    if ($country == 'IM') $continent = 'Europe';
    if ($country == 'IL') $continent = 'Asia';
    if ($country == 'IT') $continent = 'Europe';
    if ($country == 'JM') $continent = 'North America';
    if ($country == 'JP') $continent = 'Asia';
    if ($country == 'JE') $continent = 'Europe';
    if ($country == 'JO') $continent = 'Asia';
    if ($country == 'KZ') $continent = 'Asia';
    if ($country == 'KE') $continent = 'Africa';
    if ($country == 'KI') $continent = 'Oceania';
    if ($country == 'KP') $continent = 'Asia';
    if ($country == 'KR') $continent = 'Asia';
    if ($country == 'KW') $continent = 'Asia';
    if ($country == 'KG') $continent = 'Asia';
    if ($country == 'LA') $continent = 'Asia';
    if ($country == 'LV') $continent = 'Europe';
    if ($country == 'LB') $continent = 'Asia';
    if ($country == 'LS') $continent = 'Africa';
    if ($country == 'LR') $continent = 'Africa';
    if ($country == 'LY') $continent = 'Africa';
    if ($country == 'LI') $continent = 'Europe';
    if ($country == 'LT') $continent = 'Europe';
    if ($country == 'LU') $continent = 'Europe';
    if ($country == 'MO') $continent = 'Asia';
    if ($country == 'MK') $continent = 'Europe';
    if ($country == 'MG') $continent = 'Africa';
    if ($country == 'MW') $continent = 'Africa';
    if ($country == 'MY') $continent = 'Asia';
    if ($country == 'MV') $continent = 'Asia';
    if ($country == 'ML') $continent = 'Africa';
    if ($country == 'MT') $continent = 'Europe';
    if ($country == 'MH') $continent = 'Oceania';
    if ($country == 'MQ') $continent = 'North America';
    if ($country == 'MR') $continent = 'Africa';
    if ($country == 'MU') $continent = 'Africa';
    if ($country == 'YT') $continent = 'Africa';
    if ($country == 'MX') $continent = 'North America';
    if ($country == 'FM') $continent = 'Oceania';
    if ($country == 'MD') $continent = 'Europe';
    if ($country == 'MC') $continent = 'Europe';
    if ($country == 'MN') $continent = 'Asia';
    if ($country == 'ME') $continent = 'Europe';
    if ($country == 'MS') $continent = 'North America';
    if ($country == 'MA') $continent = 'Africa';
    if ($country == 'MZ') $continent = 'Africa';
    if ($country == 'MM') $continent = 'Asia';
    if ($country == 'NA') $continent = 'Africa';
    if ($country == 'NR') $continent = 'Oceania';
    if ($country == 'NP') $continent = 'Asia';
    if ($country == 'AN') $continent = 'North America';
    if ($country == 'NL') $continent = 'Europe';
    if ($country == 'NC') $continent = 'Oceania';
    if ($country == 'NZ') $continent = 'Oceania';
    if ($country == 'NI') $continent = 'North America';
    if ($country == 'NE') $continent = 'Africa';
    if ($country == 'NG') $continent = 'Africa';
    if ($country == 'NU') $continent = 'Oceania';
    if ($country == 'NF') $continent = 'Oceania';
    if ($country == 'MP') $continent = 'Oceania';
    if ($country == 'NO') $continent = 'Europe';
    if ($country == 'OM') $continent = 'Asia';
    if ($country == 'PK') $continent = 'Asia';
    if ($country == 'PW') $continent = 'Oceania';
    if ($country == 'PS') $continent = 'Asia';
    if ($country == 'PA') $continent = 'North America';
    if ($country == 'PG') $continent = 'Oceania';
    if ($country == 'PY') $continent = 'South America';
    if ($country == 'PE') $continent = 'South America';
    if ($country == 'PH') $continent = 'Asia';
    if ($country == 'PN') $continent = 'Oceania';
    if ($country == 'PL') $continent = 'Europe'; // Poland - NOT euro
    if ($country == 'PT') $continent = 'Europe';
    if ($country == 'PR') $continent = 'North America';
    if ($country == 'QA') $continent = 'Asia';
    if ($country == 'RE') $continent = 'Africa';
    if ($country == 'RO') $continent = 'Europe'; // Romania - NOT euro
    if ($country == 'RU') $continent = 'Europe';
    if ($country == 'RW') $continent = 'Africa';
    if ($country == 'BL') $continent = 'North America';
    if ($country == 'SH') $continent = 'Africa';
    if ($country == 'KN') $continent = 'North America';
    if ($country == 'LC') $continent = 'North America';
    if ($country == 'MF') $continent = 'North America';
    if ($country == 'PM') $continent = 'North America';
    if ($country == 'VC') $continent = 'North America';
    if ($country == 'WS') $continent = 'Oceania';
    if ($country == 'SM') $continent = 'Europe';
    if ($country == 'ST') $continent = 'Africa';
    if ($country == 'SA') $continent = 'Asia';
    if ($country == 'SN') $continent = 'Africa';
    if ($country == 'RS') $continent = 'Europe';
    if ($country == 'SC') $continent = 'Africa';
    if ($country == 'SL') $continent = 'Africa';
    if ($country == 'SG') $continent = 'Asia';
    if ($country == 'SK') $continent = 'Europe';
    if ($country == 'SI') $continent = 'Europe';
    if ($country == 'SB') $continent = 'Oceania';
    if ($country == 'SO') $continent = 'Africa';
    if ($country == 'ZA') $continent = 'Africa';
    if ($country == 'GS') $continent = 'Antarctica';
    if ($country == 'ES') $continent = 'Europe';
    if ($country == 'LK') $continent = 'Asia';
    if ($country == 'SD') $continent = 'Africa';
    if ($country == 'SR') $continent = 'South America';
    if ($country == 'SJ') $continent = 'Europe';
    if ($country == 'SZ') $continent = 'Africa';
    if ($country == 'SE') $continent = 'Europe'; // Sweden - NOT euro
    if ($country == 'CH') $continent = 'Europe';
    if ($country == 'SY') $continent = 'Asia';
    if ($country == 'TW') $continent = 'Asia';
    if ($country == 'TJ') $continent = 'Asia';
    if ($country == 'TZ') $continent = 'Africa';
    if ($country == 'TH') $continent = 'Asia';
    if ($country == 'TL') $continent = 'Asia';
    if ($country == 'TG') $continent = 'Africa';
    if ($country == 'TK') $continent = 'Oceania';
    if ($country == 'TO') $continent = 'Oceania';
    if ($country == 'TT') $continent = 'North America';
    if ($country == 'TN') $continent = 'Africa';
    if ($country == 'TR') $continent = 'Asia';
    if ($country == 'TM') $continent = 'Asia';
    if ($country == 'TC') $continent = 'North America';
    if ($country == 'TV') $continent = 'Oceania';
    if ($country == 'UG') $continent = 'Africa';
    if ($country == 'UA') $continent = 'Europe';
    if ($country == 'AE') $continent = 'Asia';
    if ($country == 'GB') $continent = 'Europe'; // United Kingdom - NOT euro
    if ($country == 'US') $continent = 'North America';
    if ($country == 'UM') $continent = 'Oceania';
    if ($country == 'VI') $continent = 'North America';
    if ($country == 'UY') $continent = 'South America';
    if ($country == 'UZ') $continent = 'Asia';
    if ($country == 'VU') $continent = 'Oceania';
    if ($country == 'VE') $continent = 'South America';
    if ($country == 'VN') $continent = 'Asia';
    if ($country == 'WF') $continent = 'Oceania';
    if ($country == 'EH') $continent = 'Africa';
    if ($country == 'YE') $continent = 'Asia';
    if ($country == 'ZM') $continent = 'Africa';
    if ($country == 'ZW') $continent = 'Africa';
    return $continent;
}


function check_if_artist_has_multiple_gallery($artist_id){
    $artist = \App\Models\Artist::find($artist_id);
    if($artist){
        if($artist->galleries()->count() > 1){
            return true;
        }
    }
    return false;
}


function membershipGuideExist()
{
    $previousFile = storage_path('uploads/guide/gallerist_guide_preview.png');
    $previousFileExist = File::exists($previousFile);
    if($previousFileExist) {
        return true;
    } else {
        return false;
    }
}

function get_asset_rev(){
    $file = storage_path('config/asset.rev');
    if(File::exists($file)){
        return file_get_contents($file);
    }
    $time = time();
    file_put_contents($file, $time);
    return $time;
}

function create_redirect_301_on_delete($old_url, $new_url){
    $redirect = Redirect301::whereOldUrl($old_url)->count();
    if(!$redirect){
        Redirect301::create([
            'old_url' => $old_url,
            'new_url' => $new_url,
            'redirect_type' => 301
        ]);
    }
}