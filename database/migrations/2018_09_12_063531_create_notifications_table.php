<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('content');
            $table->integer('artwork_id')->unsigned()->nullable();
            $table->integer('artist_id')->unsigned()->nullable();
            $table->integer('gallery_id')->unsigned()->nullable();
            $table->integer('exhibition_id')->unsigned()->nullable();
            $table->integer('post_id')->unsigned()->nullable();
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('user_offers_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('artwork_id')
                ->references('id')
                ->on('artworks')
                ->onDelete('CASCADE');

            $table->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('CASCADE');

            $table->foreign('exhibition_id')
                ->references('id')
                ->on('exhibitions')
                ->onDelete('CASCADE');

            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('CASCADE');


            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('CASCADE');


            $table->foreign('user_offers_id')
                ->references('id')
                ->on('user_offers')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
