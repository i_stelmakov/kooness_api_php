{{-- Mail - Automatic Membership Renewal --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_AUTOMATIC_RENEWAL}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
            The plan you selected is about to be automatically renewed by the system in seven days.<br>
			We remind you that if you want to apply changes to your personal details or payment you can do it conveniently in your personal profile.
		</p>

	</div>
@stop
