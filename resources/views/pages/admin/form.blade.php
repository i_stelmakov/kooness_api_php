<style>
    form .error {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" name="form-tag"  data-attachment="validate" data-id="{{ $page->{'id'} }}" data-referer="page">

            {{ method_field($method) }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12  col-xs-12">
                            <div class="form-group">
                                <label for="first_name">Url generated</label>
                                <input id="generated_url" class="form-control" type="text" data-prefix="{{ url('') }}/pages/" value="{{ ($page->{'slug'})? url('') . '/pages/' . $page->{'slug'} : '' }}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title (*)</label>
                                <input id="title"
                                       class="form-control"
                                       type="text"
                                       placeholder="Title" name="title" value="{{ old('title', $page->{'title'}) }}"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Slug (*)</label>
                                <input id="slug"
                                       class="form-control"
                                       type="text"
                                       data-url="#generated_url"
                                       placeholder="Slug" name="slug" value="{{ old('name', $page->{'slug'}) }}"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="content">Content (*)</label>
                                <textarea
                                    id="content"
                                    class="form-control ck-editor"
                                    placeholder="Content"
                                    name="content"
                                    rows="5">{{ $page->content }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>



                {{--<div class="box-footer">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<div class="pull-right ">--}}
                            {{--<input class="btn btn-primary" type="submit" value="Save"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>

            {{-- Footer & Submit --}}
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
