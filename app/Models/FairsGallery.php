<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FairsGallery extends Model
{
    protected $guarded = [];

    public function parent(){
        return $this->hasOne("App\Models\FairsGallery",'id',"parent_id");
    }

    public function fairs(){
        return $this->belongsToMany('App\Models\Fair', 'fairs_fairs_galleries', 'gallery_id', 'fair_id');
    }
}
