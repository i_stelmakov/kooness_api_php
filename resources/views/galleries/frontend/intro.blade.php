@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Galleries')

@section('body-class', 'galleries-intro')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'Galleries' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="featured-section" class="container-vertical container-vertical-padding-bottom">
            <div class="container boxed-container">
                {{--Slider Hero--}}
                <div class="slick-carousel">
                    @foreach($slides as $slide)
                            {{--Slider Item--}}
                            <div class="slide-item">
                                <div class="col-container-with-offset featured-section-item">
                                    <div class="two-third-col featured-section-item-img">
                                        <div class="slider-item-img wide-box">
                                            <a title="{{ $slide->name }}" href="{{ route('galleries.single', [$slide->slug]) }}">
                                                <div class="background-cover lazy" data-src="{{ $slide->url_wide_box }}"></div>
                                            </a>
                                        </div>
                                        {{--<div class="slider-item-img">--}}
                                            {{--<a title="{{ $slide->name }}" href="{{ route('galleries.single', [$slide->slug]) }}">--}}
                                                {{--<img src="{{ $slide->url_wide_box }}"/>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="one-third-col featured-section-item-txt">
                                        <div class="slider-title-box-txt">
                                            <span class="h1">{{ $slide->name }}</span>
                                            <p>{!! strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($slide->about_gallery, 0, 201))); !!}...</p>
                                            <a title="{{ $slide->name }}" class="black-button" href="{{ route('galleries.single', [$slide->slug]) }}">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--Slider Item--}}
                    @endforeach

                </div>
                {{--Slider Hero--}}
            </div>
        </section>
        @foreach($widgets as $widget)
            @if($widget['template'] == 'tplA')
                @include('partials.widget-template-a', ['data' => $widget])
            @endif
            @if($widget['template'] == 'tplB')
                @include('partials.widget-template-b', ['data' => $widget])
            @endif
        @endforeach
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span class="h1">Browse Galleries</span>
                        </div>
                    </div>
                    <div class="page-header-filter">
                        <div class="default-sheet-row-cell">
                            <p>Location</p>
                            <form>
                                <select class="search-by location">
                                    <option value="">All</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->code }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        <div class="default-sheet-row-cell">
                            <p>Category</p>
                            <form>
                                <select class="search-by category">
                                    <option value="">All</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->slug }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="header-secondary-menu">
            <a class="search-by" href="{{ route('galleries.archive.all') }}">All</a><a class="search-by" data-value="number">0-9</a>
            <a class="search-by" data-value="a">A</a> <a class="search-by" data-value="b">B</a>
            <a class="search-by" data-value="c">C</a> <a class="search-by" data-value="d">D</a>
            <a class="search-by" data-value="e">E</a> <a class="search-by" data-value="f">F</a>
            <a class="search-by" data-value="g">G</a> <a class="search-by" data-value="h">H</a>
            <a class="search-by" data-value="i">I</a> <a class="search-by" data-value="j">J</a>
            <a class="search-by" data-value="k">K</a> <a class="search-by" data-value="l">L</a>
            <a class="search-by" data-value="m">M</a> <a class="search-by" data-value="n">N</a>
            <a class="search-by" data-value="o">O</a> <a class="search-by" data-value="p">P</a>
            <a class="search-by" data-value="q">Q</a> <a class="search-by" data-value="r">R</a>
            <a class="search-by" data-value="s">S</a> <a class="search-by" data-value="t">T</a>
            <a class="search-by" data-value="u">U</a> <a class="search-by" data-value="v">V</a>
            <a class="search-by" data-value="w">W</a> <a class="search-by" data-value="x">X</a>
            <a class="search-by" data-value="y">Y</a> <a class="search-by" data-value="z">Z</a>
        </div>

        <section id="archive" class="galleries-page">
            <div class="container boxed-container">
                <div class="container">
                    <section id="product-masonry" class="default-slider-section">
                        <div class="container col-container-with-offset-and-margin galleries-container">
                            @foreach($galleries as $gallery)
                                <div data-id="{{ $gallery->id }}" class="slider-item one-third-col-with-margin half-width-mobile">

                                    @if(Auth::user())
                                        <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($gallery->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$gallery->id}}">
                                            @if(Auth::user()->collectionArtworks->contains($gallery->id))
                                                <i class="fa fa-heart"></i>
                                            @else
                                                <i class="fa fa-heart-o"></i>
                                            @endif
                                        </div>
                                    @endif

                                    <div class="slider-item-img wide-box">
                                        <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">
                                            <div class="background-cover gallery lazy" data-src="{{ $gallery->url_wide_box }}"></div>
                                        </a>
                                    </div>
                                    <div class="slider-item-txt">
                                        <div class="default-sheet-row slider-item-row">
                                            <div class="default-sheet-row-cell">
                                                <h2>
                                                    <a title="{{ $gallery->name }}" class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="default-sheet-row slider-item-row">
                                            <div class="default-sheet-row-cell">
                                                <p class="gallery-adress">{{ $gallery->city }}, {{ $gallery->address }}</p>
                                            </div>
                                        </div>
                                        <a title="{{ $gallery->name }}" class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">read more</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="view-all-box">
                            <span data-alpha-filter="" data-location-filter="" data-category-filter="" class="btn_view_all" ><a style="color: white" href="{{ route('galleries.archive.all') }}">View All</a></span>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </div>
@endsection