@extends('layouts.admin')
@section('title', 'Admin | Create Artwork Media')
@section('page-header', 'Admin - Create Media')

@section('content')
    @include('artworks.medium.form', ['action' => route('admin.medium.artworks.store'), 'method' => 'POST'])
@endsection