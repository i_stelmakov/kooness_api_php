@extends('layouts.app')

@section('title')
    Thank you
@endsection

@section('body-class')
    thank-you
@endsection

@section('content')
    <div class="sections">

        <section id="page-header">
            <div class="container-vertical-padding-bottom boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <i class="fa fa-check main-color-txt thanks-icon"></i>
                            <h1 class="thanks-title">Thank You for your subscription</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


