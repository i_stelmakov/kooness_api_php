<style>
    form .error {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="" data-referer="">
            {{ method_field($method) }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    301 Redirect
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="old_url">Old Url (*)</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">{{ url('') }}</span>
                                <input id="old_url" class="form-control" type="text" placeholder="Old path" name="old_url" value="{{ $redirect->old_url }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="new_url">New Url (*)</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">{{ url('') }}</span>
                                <input id="new_url" class="form-control" type="text" placeholder="New path" name="new_url" value="{{ $redirect->new_url }}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>