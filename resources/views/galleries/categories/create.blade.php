@extends('layouts.admin')
@section('title', 'Admin | Create Gallery Category')
@section('page-header', 'Admin - Create Category')

@section('content')
    @include('galleries.categories.form', ['action' => route('admin.categories.galleries.store'), 'method' => 'POST'])
@endsection