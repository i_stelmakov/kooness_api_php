<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artwork;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class LoginController extends FrontendExtendedController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
//        dump(Session::has('postLogin'));
        $this->middleware('guest')->except('logout');

    }

    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['username' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('register');
    }

    protected function sendLoginResponse(Request $request)
    {
        $user = Auth::user();
        if($user->is_trial && !$user->trial_activation_date){
            $user->update([
                "trial_activation_date" => date('Y-m-d')
            ]);
        }

        if(Session::has('postLogin')){
            Session::remove('postLogin');
            $this->redirectTo = '/checkout';
        }

        if(Session::has('artworksIdReferer')){
            $id = Session::get('artworksIdReferer');
            Session::remove('artworksIdReferer');
            $artwork = Artwork::find($id);
            if($artwork){
                $this->redirectTo = route('artworks.single', [$artwork->slug]);
            }
        }

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    public function showLoginForm(){
        return redirect()->route('register');
    }
}
