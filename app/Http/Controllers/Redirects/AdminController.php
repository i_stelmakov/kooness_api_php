<?php

namespace App\Http\Controllers\Redirects;

use App\Http\Controllers\Controller;
use App\Models\Redirect301;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('redirects.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'url',
            1 => 'meta_title',
            2 => 'created_at',
            3 => 'options',
        );

        $totalData = Redirect301::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $redirects = Redirect301::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $redirects = Redirect301::where('old_url', 'LIKE', "%{$search}%")->orWhere('new_url', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Redirect301::where('old_url', 'LIKE', "%{$search}%")->orWhere('new_url', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($redirects)) {
            foreach ($redirects as $redirect) {
                $edit = route('admin.redirects.edit', $redirect->id);
                $delete = route('admin.redirects.destroy', $redirect->id);

                $nestedData['id'] = $redirect->id;
                $nestedData['old_url'] = url($redirect->old_url);
                $nestedData['new_url'] = url($redirect->new_url);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($redirect->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $redirect = new Redirect301();
        return view('redirects.admin.create', compact('redirect'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['redirect_type'] = 301;
            $redirect = Redirect301::create($data);
            if ($redirect) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.redirects.index')
                ];
                $request->session()->flash("success", "301 Redirect is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($redirect)
    {
        $redirect = Redirect301::find($redirect);
        return view('redirects.admin.edit', compact('redirect'));
    }

    public function update(Request $request, $redirect)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $redirect = Redirect301::find($redirect);
        if ($request->method() == 'PATCH' && $redirect) {
            if ($redirect->update(Input::all())) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.redirects.index')
                ];
                $request->session()->flash("success", "301 Redirect is successful saved!");
            }
        }

        return response()->json($response);
    }

    public function destroy($redirect)
    {
        $redirect = Redirect301::find($redirect);
        $redirect->delete();
        // redirect
        Session::flash('success', "301 Redirect is successful removed!");
        return redirect(route('admin.redirects.index'));
    }

    public function bulkUpload(){
        $type = 'redirects';
        return view('redirects.admin.bulk-upload', compact('type'));
    }
}
