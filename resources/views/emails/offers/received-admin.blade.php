{{-- Mail - Admin Offer Received --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_OFFER_RECEIVED_ADMIN}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: center;'>
		<b>You have received a new offer for:</b><br>
		"{{ $artwork->title }}"<br>
		<b>by the user:</b><br>
		{{ $user->first_name }} {{ $user->last_name }}<br>
		<b>with the message:</b><br>
		"{{ $msg}}"
	</p>
@stop