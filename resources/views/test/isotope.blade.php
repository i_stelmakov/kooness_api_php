{{-- Test - Intro --}}

@extends('layouts.app')

@section('title')
Test - Intro
@endsection

@section('body-class')
test-intro
@endsection

@section('scripts')
    {{--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.2.0/bootstrap-slider.js"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.2.0/css/bootstrap-slider.min.css" />
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />--}}
@endsection

@section('content')
<div class="container boxed-container">
<h1>Test Zone ☢</h1>️
{{-- ##### START TEST CONTENT ##### --}}
<style>
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    body { font-family: sans-serif; }
    .btn {
        font-weight: bold;
        text-transform: uppercase;
    }
    .btn:hover {
        background-color: ccc;
        text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
        color: #222;
    }
    .btn:active,
    .btn.is-checked {
        background-color: #2175b0;
        color: #fff;
    }

    /* --------------------------------------- */
    /* Isotope Grid */
    /* --------------------------------------- */
    .grid {
        background: #EEE;
        /*max-width: 1200px;*/
    }
    /* clear fix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }

    /* --------------------------------------- */
    /* Isotope item */
    /* --------------------------------------- */
    .item {
        text-align: center;
    }

    .item.red { background: #b0221b; }
    .item.blue { background: #2175b0; }
    .item.yellow { background: #e8ce3c; }
    .item.green { background: green; }
    .item.purple { background: purple; }

    .item .content {
        color: #fff;
        font-size: 10px;
        display: inline-block;
    }

    /* --------------------------------------- */
    /* Bootstrap Slider */
    /* --------------------------------------- */
    .sliders { padding: 15px 0 30px 0; }
    .filter-section .filter-label {
        display: block;
        font-weight: bold;
    }
    .bootstrap-slider .slider-selection { background: #2175b0; }


    /* --------------------------------------- */
    /* Extras */
    /* --------------------------------------- */
    btn-filter {
        cursor: pointer;
        display: inline-block;
        margin: 20px;
        font-size: 20px;
        text-transform: capitalize;
        tansition: ease-in-out .3s;
    }
    btn-filter.is-checked {
        color: darkorange;
    }
    .item {
        background-color: #333;
        text-align: center;
        color: #fff;
        height: 200px;
        width: 31.3%;
        overflow: hidden;
        display: block;
        margin: 1%;
        line-height: 200px;
    }
    .btn_load {
        display: inline-block;
        margin: auto;
        position: relative;
    }
    .tooltip-inner, .tooltip-arrow {
        display: none;
    }
</style>

    <select class="btn-filter" name="color">
        <option value="*">Any</option>
        <option value="red">Red</option>
        <option value="blue">Blue</option>
    </select>


    <div class="grid">
        <div id="001" class="item"><span class="content">001</span></div>
        <div id="002" class="item"><span class="content">002</span></div>
        <div id="003" class="item"><span class="content">003</span></div>
    </div>

    <p class="btn_load">Load More</p>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){

        // Variables
        var $ = jQuery;

        // Initialise Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.item',
            layout: 'masonry',
        });

        // Ajax call on filter click
        $('.btn-filter').on('change',function(){

            var actual_item_list = [];
            $(".item").each(function (index, element) {
                actual_item_list.push(element.id);
            });
            console.log("Actual Items list => " + actual_item_list);
            var actual_filter = $( this )[0].name;
            var color_filter_value = $( "select[name='color']" )[0].value;
            console.log("Actual filter => " + actual_filter + ': ' + color_filter_value);

            $.ajax({
                url: '/test/api',
                type: 'POST',
                dataType: 'json',
                data: {
                    actual_item_list: actual_item_list,
                    filter_color: color_filter_value
                },
                success: function(response) {
                    console.log('Success!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                complete: function (response) {
                    var $response = response.responseJSON.response;

                    console.log($response);

                    // Add new items
                    var $new_item_list = $response.new_item_list;
                    var $new_item_html = '';
                    // loop trough every item to Add and generate the relative html block
                    $new_item_list.forEach(function(element, index) {
                        $new_item_html += '<div id="' + element + '" class="item"><span class="content">' + element + '</span></div>';
                    });
                    console.log($new_item_html);
                    $grid.append($new_item_html).isotope('appended',$new_item_html).isotope( 'reloadItems' ).isotope({ sortBy: 'original-order' });

                    // Remove old items
                    var $old_item_list = $response.old_item_list;
                    // loop trough every item to Remove
                    $old_item_list.forEach(function(element, index) {
                        console.log( "#" + element);
                        $grid.isotope( 'remove', $( "#" + element ) )
                        // layout remaining item elements
                            .isotope('layout');

                    });

                }
            });
        });

    });
</script>
{{-- ##### END TEST CONTENT ##### --}}
</div>
@endsection