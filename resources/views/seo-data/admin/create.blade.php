@extends('layouts.admin')
@section('title', 'Admin | Create Seo Data for Url')
@section('page-header', 'Admin - Create Seo Data for Url')

@section('content')
    @include('seo-data.admin.form', ['action' => route('admin.seo-data.store'), 'method' => 'POST'])
@endsection