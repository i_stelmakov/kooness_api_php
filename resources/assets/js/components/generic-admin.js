jQuery.validator.addMethod("pwcheck", function(value) {
    // return /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/.test(value); // regex con caratteri speciali
    return /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).*$/.test(value); // regex senza caratteri speciali
}, 'The password must be at least 8 characters and contain a minimum of one lower case character, one upper case character and one digit.');

jQuery.validator.addMethod("exactlength", function(value, element, param) {
    return this.optional(element) || value.length == param;
}, $.validator.format("Please enter exactly {0} characters."));

$(function () {
    $('.select2').select2({
        width: 'resolve',
        containerCss: function (element) {
            var style = $(element)[0].style;
            if(style.display == 'none'){
                $(element).closest('select2').hide()
            }
            return {
                display: style.display
            };
        }
    });

    $('.iCheck-input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });

    $('.ajax-form').attachAjaxSubmit();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input[type=file].img-uploader').imageUploader();

    let options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    };

    CKEDITOR.editorConfig = function( config ) {
        config.toolbarGroups = [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'forms', groups: [ 'forms' ] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            '/',
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'colors', groups: [ 'colors' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'about', groups: [ 'about' ] }
        ];
    };

    CKEDITOR.config.allowedContent = true;

    $('.ck-editor').each(function (e) {
        CKEDITOR.config.extraPlugins = 'a11yhelp,about,basicstyles,bidi,blockquote,clipboard,colorbutton,colordialog,contextmenu,copyformatting,dialogadvtab,div,elementspath,enterkey,entities,filebrowser,find,flash,floatingspace,font,format,forms,horizontalrule,htmlwriter,iframe,image,indentblock,indentlist,justify,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastefromword,pastetext,preview,print,removeformat,resize,save,scayt,selectall,showblocks,showborders,smiley,sourcearea,specialchar,stylescombo,tab,table,tableselection,tabletools,templates,toolbar,undo,uploadimage,wsc,wysiwygarea,image2';
        CKEDITOR.config.contentsCss = '/css/ckeditor_content.css';
        CKEDITOR.replace(this.id, options);
    });

    $('.datepicker').datepicker({format: 'dd/mm/yyyy'}).inputmask('dd/mm/yyyy');

    $('.timepicker').timepicker({showMeridian: false}).inputmask('hh:mm');

    $('.date-year').datepicker({
        minViewMode: 2,
        format: 'yyyy'
    }).inputmask("9999", {
        postValidation: function (buffer, opts) {
            return parseInt(buffer.join('')) <= (new Date()).getFullYear();
        }
    });



    // Funzione di creazione Slug composito, specifica per artworks
    var artworksSlugTriggers = $('.artworks-slug-trigger');
    // Se esiste la classe target in pagina
    if ( artworksSlugTriggers[0] ) {
        // se il metodo è POST (creazione ex-novo)
        if($('input[name=_method]').val() === 'POST') {
            var generatedSlugInput = $("#slug");
            var previousSlug = '';
            var newSlug = '';
            artworksSlugTriggers.each(function() {
                var $that = $(this); // memorize $(this)
                $that.on('keyup keypress blur change', function() {

                    var artworkArtistFullName = $('select[name=artist_id]').find(":selected").text();
                    var artworkTitle = $('input[name=title]').val();
                    var artworkMedium = $('select[name="medium[]"]').find(":selected").first().text();

                    // console.clear();
                    // console.log(previousSlug + '?=' + newSlug);

                    newSlug = slugify(artworkArtistFullName + ' ' + artworkTitle + ' ' + artworkMedium);

                    // se slug è vuoto
                    if(generatedSlugInput.val() !== '') {
                        // se lo slug precedente è vuoto
                        if(previousSlug === '') {
                            previousSlug = newSlug;
                        }
                        // se i due valori non corrispondono
                        else if( previousSlug !== generatedSlugInput.val() ) {
                            return;
                        }
                    }
                    // imposto i valori
                    previousSlug = newSlug;
                    generatedSlugInput.val(slugify(newSlug)).trigger('change');
                });
            });
        }
    }


    // Funzione di creazione Slug composito, generica
    // seleziona ogni input con data-slug
    let $selector = $('input[data-slug]');
    // se il metodo è POST (creazione ex-novo)
    if($('input[name=_method]').val() === 'POST') {

        var generatedSlugInput = $("#slug");
        var previousSlug = '';
        var newSlug = '';

        // se ci sono più input...
        if ($selector.length > 1) {
            // per ognuno degli input
            $selector.each(function () {
                // alla variazione del campo
                $(this).on('keyup keypress blur change', function () {
                    let str = '';
                    // per ognuno dei valori
                    $selector.each(function () {
                        // lo concateno al precedente
                        str += " " + $(this).val();
                    });

                    newSlug = slugify(str);

                    // se slug è vuoto
                    if(generatedSlugInput.val() !== '') {
                        // se lo slug precedente è vuoto
                        if(previousSlug === '') {
                            previousSlug = newSlug;
                        }
                        // se i due valori non corrispondono
                        else if( previousSlug !== generatedSlugInput.val() ) {
                            return;
                        }
                    }
                    // imposto i valori
                    previousSlug = newSlug;
                    generatedSlugInput.val(slugify(newSlug)).trigger('change');

                    // ad ogni variazione recupero il valore, lo passo a "slugify" e lo inserisco nel campo indicato nell'attributo data-slug
                    // $("" + $(this).data('slug')).val(slugify(str)).trigger('change');

                });
            });
        }
        // ...altrimenti se c'è un solo input
        else {
            $selector.on('keyup keypress blur change', function () {
                // $("" + $(this).data('slug')).val(slugify($(this).val())).trigger('change');

                newSlug = slugify($(this).val());

                // se slug è vuoto
                if(generatedSlugInput.val() !== '') {
                    // se lo slug precedente è vuoto
                    if(previousSlug === '') {
                        previousSlug = newSlug;
                    }
                    // se i due valori non corrispondono
                    else if( previousSlug !== generatedSlugInput.val() ) {
                        return;
                    }
                }
                // imposto i valori
                previousSlug = newSlug;
                generatedSlugInput.val(slugify(newSlug)).trigger('change');

            });
        }
    }

    var input_slug = $('#slug');
    input_slug.on(' change', function () {
        var $url_input = $("" + $(this).data('url'));
        $url_input.val($url_input.data('prefix') + $(this).val());
    });
    input_slug.on('keyup keypress blur', function () {
        // $selector.unbind('keyup keypress blur change');

        var $url_input = $("" + $(this).data('url'));
        $url_input.val($url_input.data('prefix') + $(this).val());
    });

    var select_media_category = $('.media-category');
    if(select_media_category.length > 0){
        select_media_category.on('change', function () {
            var media = $('select[name=media] option:selected').data('slug');
            var category = $('select[name=category] option:selected').data('slug');
            var $url_input = $('#generated_url');
            $url_input.val($url_input.data('prefix') + media + "/" + category);
        });
        select_media_category.trigger('change')
    }

    $('.render-map').click(function () {
        var map = $('#map').val();
        if (map !== '')
            $('.render-container').html(map);
        return false;
    });

    $('.add-new-artist').click(function () {
        $('#new-artist-modal').modal('show');
        return false;
    });

    $("form[data-attachment='validate']").each(function () {
        $(this).attachValidation();
        $('select[multiple]').rules('add', {required: false});
    });

    $("form:not([data-attachment])").each(function () {
        if(!$(this).hasClass('form-upload') && !$(this).hasClass('delete-membership-button'))
            $(this).submit(function () {
                submit($(this));
            });
    });

    $('#created_user_yes').on('ifChecked ifUnchecked', function () {
        if (!$(this).is(':checked')) {
            $('[data-for-user]').attr('disabled', true);
            // console.log('disattiva campi collegati: ');
        } else {
            $('[data-for-user]').attr('disabled', false);
            // console.log('attiva campi collegati: ');
        }
    }).trigger('ifChecked');

    $('.select2-widget').each(function () {
        let length = 3;
        if ($(this).parents('.item-drop').find('select.select-tpl').val() === 'tplB') {
            length = 4;
        }
        $(this).select2widget(length);
    });

    $('.widget-time-repeater').repeater({
        show: function () {
            $(this).slideDown(400, function () {
                let th = $(this);
                th.find('input.order').val($('.item-time').length - 1);
                th.find('.datepicker').datepicker({format: 'dd/mm/yyyy'}).inputmask('dd/mm/yyyy');
                th.find('.timepicker').val('00:00').timepicker({showMeridian: false}).inputmask('hh:mm');
            });
        }
    });


    $('.widget-repeater-link').repeater({
        show: function () {
            $(this).slideDown(400, function () {
                let th = $(this);
                th.find('input.order').val($('.item-link').length - 1);
            });
        }
    });
    $('.widget-time-repeater-container').sortableWidgetTime();

    $('.widget-repeater-link-container').sortableWidgetLink();

    $('.widget-repeater').repeater({
        // isFirstItemUndeletable: true,
        show: function () {
            $(this).slideDown(400, function () {
                let th = $(this);
                $.when($(this).find('span.select2').remove()).then(function () {
                    th.find('select').removeClass('select2-hidden-accessible');
                    th.find('.select2').find('option:first-child').attr("selected", true);
                    th.find('.select2').select2();
                    th.find('.select2-widget').select2widget(3);
                    th.find('.select2-widget-exhibition').select2widgetExhibition();
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'artworks'){
                        th.find('.select-tpl').val('tplA');
                        th.find('.select-type').val('artworks');
                    }
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'artists'){
                        th.find('.select-tpl').val('tplA');
                        th.find('.select-type').val('artists');
                    }
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'galleries'){
                        th.find('.select-tpl').val('tplB');
                        th.find('.select-type').val('galleries');
                    }
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'fairs'){
                        th.find('.select-tpl').val('tplB');
                        th.find('.select-type').val('fairs');
                    }
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'exhibitions'){
                        th.find('.select-tpl').val('tplC');
                    }
                    if(th.parents('.widget-repeater').find('input[name=label]').val() === 'magazine'){
                        th.find('.select-tpl').val('tplCa');
                    }
                    th.find('.select-tpl').trigger('change');
                    th.find('select.select-type').selectType();
                    th.find('input.order').val($('.item-drop').length - 1);
                    $('.widget-repeater-container').sortableWidget();
                });

            });
        }
    });

    $('.widget-repeater-container').sortableWidget();

    $('select.select-type').selectType();

    $('.block-edit').each(function () {
        $(this).click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            let pointer = $(this).parents('.block').find('input.pointer').val();
            let $modal = $('#edit-section-modal');
            $modal.find('input.block-selector').val(pointer);
            $modal.modal('show');
            return false;
        });
    });

    let $modal = $('#edit-section-modal');
    let validator = null;
    $modal.on('shown.bs.modal', function () {
        let pointer = $modal.find('input.block-selector').val();
        let data = JSON.parse($('.' + pointer).find('.input-data').val());
        if (Object.keys(data).length) {
            $modal.find('select[name=sections]').val(data.sections);
            if (data.id_bg !== undefined)
                $modal.find('select[name=bg-image]').empty().append("<option value='" + data.id_bg + "'>" + data.text_bg + "</option>");
            $.each(data.link, function (index, value) {
                let $select = $modal.find('select[name=link]').get(index);
                $($select).empty().append("<option value='" + value.id + "'>" + value.name + "</option>")
            })
            $modal.find('#caption').val(data.caption)
        }
        $(this).find('.select2-modal-artwork').select2({
            dropdownParent: $('#edit-section-modal'),
        });
        $(this).find('.select2-modal-bg').select2Bg();
        $(this).find('.select2-modal-link').select2Link();
        validator = $('#form-modal-section').validate({
            highlight: function (element, errorClass, validClass) {
                let elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().addClass(errorClass);
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                let elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    $("#select2-" + elem.attr("id") + "-container").parent().removeClass(errorClass);
                } else {
                    elem.removeClass(errorClass);
                }
            },
            errorPlacement: function(error, element) {
                let elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent();
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });

    $('select.sections').on('change', function () {
        $modal.find('.select2-modal-bg').val('').empty();
        $modal.find('.select2-modal-link').val('').empty();
    });

    $modal.on('hidden.bs.modal', function () {
        $(this).find('.select2-modal').val('').find('option:first-child').attr('selected', true);
        $(this).find('.select2-modal-bg').val('');
        $(this).find('.select2-modal-link').val('');
        validator.destroy();
    });

    $('.submit-section').on('click', function () {
        if (!$('#form-modal-section').valid())
            return false;
        let data = $('#form-modal-section').serializeObject();
        let $modal = $('#edit-section-modal');
        let pointer = $modal.find('input.block-selector').val();
        $('.' + pointer).find('input.input-section').val(JSON.stringify(data));
        $('.section-form').initHomeSection(pointer);
        $modal.modal('hide');
    });

    $('.section-form').initHomeSection();

    $('.news-type').on('change', function () {
        if($(this).val() !== 'news'){
            $('.select-for-news').attr('disabled', true);
        } else{
            $('.select-for-news').attr('disabled', false);
        }
    }).trigger('change');

    $('select.order-status').on('change', function () {
        $(this).parents('form').find('input[type=submit]').removeAttr('disabled')
    })

    $('.widget-repeater-container').on('change', '.select-tpl', function () {
        console.log("here")
        if($(this).val() === 'tplC'){
            $(this).parents('.item-drop').find('.not-for-c').hide();
            $(this).parents('.item-drop').find('.widget-items').next('.select2').hide();
            $(this).parents('.item-drop').find('.select-type').next('.select2').hide();
            $(this).parents('.item-drop').find('.for-c').next('.select2').show();
            $(this).parents('.item-drop').find('label').show();
        } else{
            if($(this).val() === 'tplB' || $(this).val() === 'tplBf' || $(this).val() === 'tplCa'){
                $(this).parents('.item-drop').find('.not-for-b').hide();
            } else{
                $(this).parents('.item-drop').find('.not-for-b').show();
            }
            $(this).parents('.item-drop').find('.not-for-c:not(.not-for-c)').show();
            $(this).parents('.item-drop').find('.widget-items').next('.select2').show();
            $(this).parents('.item-drop').find('.select-type').next('.select2').show();
            $(this).parents('.item-drop').find('.for-c').next('.select2').hide();
            $(this).parents('.item-drop').find('label').hide();
        }
    });

    $('.select2-widget-exhibition').select2widgetExhibition();

    $('.select-tpl').each(function () {
        $(this).trigger('change');
    });

    $('select[name=available_in_fair]').on('change', function(){
        if($(this).val()=== '1'){
            $('select[name=show_price]').attr('disabled', false)
        }else{
            $('select[name=show_price]').attr('disabled', true)
        }
    });

    $('select[name=role]').on('change', function () {
        var $fairSelect = $('select[name=fair]');
        var $gallerySelect = $('select[name=gallery]');
        var $artistSelect = $('select[name=artist]');
        var $fairRow = $fairSelect.closest('.row');
        var $galleryRow = $gallerySelect.closest('.row');
        var $artistRow = $artistSelect.closest('.row');
        if($(this).val() === 'fair'){
            $gallerySelect.attr("disabled", true);
            $artistSelect.attr("disabled", true);
            $galleryRow.addClass('hide');
            $artistRow.addClass('hide');
            $fairSelect.attr("disabled", false);
            if($fairRow.hasClass('default-assignment')){
                $fairSelect.attr("readonly", true);
            }
            $fairRow.removeClass('hide');
        } else if($(this).val() === 'gallerist'){
            $fairSelect.attr("disabled", true);
            $artistSelect.attr("disabled", true);
            $fairRow.addClass('hide');
            $artistRow.addClass('hide');
            $gallerySelect.attr("disabled", false);
            if($galleryRow.hasClass('default-assignment')){
                $gallerySelect.attr("readonly", true);
            }
            $galleryRow.removeClass('hide');
        } else if($(this).val() === 'artist'){
            $fairSelect.attr("disabled", true);
            $gallerySelect.attr("disabled", true);
            $fairRow.addClass('hide');
            $galleryRow.addClass('hide');
            $artistSelect.attr("disabled", false);
            if($artistRow.hasClass('default-assignment')){
                $artistSelect.attr("readonly", true);
            }
            $artistRow.removeClass('hide');
        } else{
            $fairSelect.attr("disabled", true);
            $gallerySelect.attr("disabled", true);
            $artistSelect.attr("disabled", true);
            $fairRow.addClass('hide');
            $galleryRow.addClass('hide');
            $artistRow.addClass('hide');
        }
    }).trigger('change');

    $('.change-role-association').on('click', function () {
        if($(this).hasClass('btn-warning')) {
            $(this).closest('.input-group').find('select').removeAttr("readonly");
            $(this).html('<i class="fa fa-undo" aria-hidden="true"></i> Undo');
            $(this).removeClass('btn-warning');
            $(this).addClass('btn-danger');
        } else{
            $(this).closest('.input-group').find('select').attr("readonly", true);
            $(this).html($(this).data('text'));
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-warning');
            $(this).closest('.input-group').find('select').val($(this).closest('.input-group').find('select option[data-default]').attr('value')).trigger('change.select2');
        }
    })

    $('#not_in_kooness').on('ifUnchecked', function () {
        $(this).closest('.not-in-kooness-container').remove();
        var field = $('.artist-slug');
        field.attr('disabled', false);
        field.attr('readonly', false);
        field.val(slugify($('#first_name').val() + " " +  $('#last_name').val()));
        field.trigger('change');
        field.valid();
        $('.artist-slug-container').removeClass('hide');
    })
});

$.fn.initHomeSection = function (updateBlock) {
    $('.block').each(function () {
        if (updateBlock !== undefined && !$(this).hasClass(updateBlock)) {
            return;
        }
        let $block = $(this);
        let data = JSON.parse($block.find('input.input-section').val());

        if (!Object.keys(data).length) {
            return;
        }
        $block.find('.block-loader').show();

        $.ajax({
            url: laroute.route('admin.homepage.section.retrieve.data'),
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.result === 'success') {
                    $block.find('.input-data').val(JSON.stringify(response.data));
                    if (response.data.bg !== undefined) {
                        $block.attr("style", "background-image : url(" + response.data.bg + ") !important");
                    }
                    $block.find('h4.title').text(response.data.title);
                    $block.find('ul').empty();
                    $.each(response.data.link, function (index, value) {
                        let $li = $('<li> ' + value.name + '</li>');
                        $block.find('ul').append($li);
                    });
                    $block.find('.caption-home-sections').text(response.data.caption);
                    $block.find('.block-loader').hide();
                }
            }
        })
    });
};

$.fn.select2Bg = function () {
    $(this).select2({
        dropdownParent: $('#edit-section-modal'),
        width: 'resolve',
        minimumInputLength: 3,
        ajax: {
            url: laroute.route('admin.homepage.layout.search.items'),
            data: function (params) {
                return {
                    search: params.term,
                    type: $(this).parents('.modal-body').find('.sections').val(),
                    modal: true,
                    bg: true
                };
            },
            type: 'POST',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        templateResult: function (option) {
            if(option.image !== undefined){
                console.log(option.text);
                var imageWithText = '<div><img src="' + option.image + '" style="width:90px;margin-right:15px"> ' + option.text + '</div>';
                return $(imageWithText);
            } else
                return option.text
        }
    });
};

$.fn.select2Link = function () {
    $(this).select2({
        dropdownParent: $('#edit-section-modal'),
        width: 'resolve',
        minimumInputLength: 3,
        ajax: {
            url: laroute.route('admin.homepage.layout.search.items'),
            data: function (params) {
                return {
                    search: params.term,
                    type: $(this).parents('.modal-body').find('.sections').val(),
                    modal: true
                };
            },
            type: 'POST',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
};

$.fn.sortableWidget = function () {
    $(this).sortable({
        handle: 'i.fa-arrows',
        itemSelector: '.item-drop',
        helper: 'clone',
        onDrop: function ($item, container, _super, event) {
            $item.removeClass(container.group.options.draggedClass).removeAttr("style");
            $("body").removeClass(container.group.options.bodyClass);
            let $lis = $(container.el).find('li.item-drop');
            let index = 0;
            $lis.each(function () {
                $(this).children('.order').val(index);
                index++;
            });
        }
    });
};

$.fn.sortableWidgetLink = function () {
    $(this).sortable({
        handle: 'i.fa-arrows',
        itemSelector: '.item-link',
        helper: 'clone',
        onDrop: function ($item, container, _super, event) {
            $item.removeClass(container.group.options.draggedClass).removeAttr("style");
            $("body").removeClass(container.group.options.bodyClass);
            let $lis = $(container.el).find('li.item-link');
            console.log($lis);
            let index = 0;
            $lis.each(function () {
                $(this).children('.order').val(index);
                index++;
            });
        }
    });
};

$.fn.sortableWidgetTime = function () {
    $(this).sortable({
        handle: 'i.fa-arrows',
        itemSelector: '.item-time',
        helper: 'clone',
        onDrop: function ($item, container, _super, event) {
            $item.removeClass(container.group.options.draggedClass).removeAttr("style");
            $("body").removeClass(container.group.options.bodyClass);
            let $lis = $(container.el).find('li.item-time');
            let index = 0;
            $lis.each(function () {
                $(this).children('.order').val(index);
                index++;
            });
        }
    });
};


$.fn.selectType = function () {
    $(this).on('change', function () {
        $(this).parents('.item-drop').find('select.widget-items').val('').empty();
    });
};

$.fn.selectTemplate = function () {
    $(this).on('change', function () {
        if ($(this).val() === 'tplA')
            $(this).parents('.item-drop').find('select.select2-widget').select2widget(3);
        else
            $(this).parents('.item-drop').find('select.select2-widget').select2widget(4);
    });
};

$.fn.select2widget = function (maxLength) {
    $(this).select2({
        width: 'resolve',
        minimumInputLength: 3,
        maximumSelectionLength: maxLength,
        ajax: {
            url: laroute.route('admin.homepage.layout.search.items'),
            data: function (params) {
                return {
                    search: params.term,
                    type: $(this).parents('.item-drop').find('.select-type').val()
                };
            },
            type: 'POST',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
};

$.fn.select2widgetExhibition = function () {
    $(this).select2({
        width: 'resolve',
        minimumInputLength: 3,
        maximumSelectionLength: 1,
        ajax: {
            url: laroute.route('admin.homepage.layout.search.items'),
            data: function (params) {
                return {
                    search: params.term,
                    type: 'exhibitions'
                };
            },
            type: 'POST',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    })
};

$.fn.attachAjaxSubmit = function () {
    $(this).submit(function (e) {
        e.stopPropagation();
        e.preventDefault();
        return false;
    });
};

// REAL Validate Generic Function
$.fn.attachValidation = function () {
    console.log('check su contenuto presente in ckeditor');
    let $form = $(this);
    $(this).validate({
        debug: false,
        ignore: 'select[multiple]:not(.validatemultiple)',
        // ignore: [],
        rules: {
            username: {
                required: true,
                remote: {
                    url: laroute.route('admin.user.check.username'),
                    type: "post",
                    data: {
                        username: function () {
                            return $("#username").val();
                        },
                        id: $form.data('id')
                    },
                    dataType: 'json',
                }
            },
            content:{
                required: function()
                {
                    console.log('check su contenuto presente in ckeditor');
                    CKEDITOR.instances.content.updateElement();
                },
                minlength:10
            },
            email: {
                required: function () {
                    let $field = $('#created_user_yes');
                    if ($field.length) {
                        console.log($field.is(':checked'));
                        return !!$field.is(':checked');
                    }
                    else {
                        return true;
                    }
                },
                email: true,
                remote: {
                    url: laroute.route('admin.user.check.email'),
                    type: "post",
                    data: {
                        email: function () {
                            return $("#email").val();
                        },
                        id: $form.data('id')
                    },
                    dataType: 'json',
                }
            },
            password:{
                required: function (element) {
                    if($(element).attr('required') === 'required'){
                        return true
                    }
                    return false;
                },
                pwcheck: function () {
                    if($('#password').attr('required') === 'required' || $('#password').val() !== ''){
                        return true
                    }
                    return false;
                }
            },
            password_confirm: {
                equalTo: "#password"
            },
            slug: {
                required: true,
                remote: {
                    url: laroute.route('admin.check.slug.availability'),
                    type: "post",
                    data: {
                        slug: function () {
                            return $("#slug").val();
                        },
                        id: $form.data('id'),
                        referer: $form.data('referer')
                    },
                    dataType: 'json',
                }
            },
            card_exp_month: {
                CCExp: true
            },
            card_exp_year: {
                CCExp: true
            }
        },
        // Specify validation error messages
        messages: {
            email: {
                required: "Please enter a valid email address",
                remote: "The email is already in use!"
            },
            username: {
                remote: "The username is already in use!"
            },
            slug: {
                remote: "This slug is already in use!"
            },
            password_confirm: "Password does not match!"
        },


        errorPlacement: function(error, element) {
            let elem = $(element);

            var acceptanceErrorBox = $('#accept-error');
            if (elem.attr('id') == "agree") {
                acceptanceErrorBox.html(error);
            }

            else {
                error.insertAfter(element);
            }
        },



        // Make sure the form is submitted to the destination defined
        submitHandler: function (form) {
            // console.log($(form));

            // console.log("here")
            if( !$(form).hasClass('action-submit') ) {
                submit($(form));
                return false;
            }
            form.submit();
            // $(form).submit();
        }
    })
};

function submit($form) {
    $form.find(":submit").attr("disabled", true);
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
    let formData = new FormData($form[0]);
    $.ajax({
        url: $form.prop('action'),
        dataType: 'json',
        type: $form.prop('method'),
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function () {
            waitingDialog.show('Saving data...', {
                dialogSize: 'm',
                progressType: 'success'
            });
        },
        success: function (response) {
            if (response.status === 'success') {
                let $images = $form.find('canvas[data-role="imageUploader"]');

                if ($images.length) {
                    uploadImage($images, 0, $images.length, response, function () {
                        if (response.redirectTo)
                            window.location.href = response.redirectTo;
                        else {
                            waitingDialog.hide();
                            var $modal = $('#new-artist-modal');
                            if ($modal.length > 0) {
                                $modal.modal('hide');
                                $modal.find('.form-group input').val('');
                                $modal.find('.img-preview-artist').attr("src", "/images/default-picture.png")
                                $modal.find('canvas').remove();
                                $form.find(":submit").attr("disabled", false);
                                let newOption = new Option(response.label, response.id_row, false, true);
                                $('#artists').append(newOption);
                            }
                        }
                    })
                } else {
                    if (response.redirectTo)
                        window.location.href = response.redirectTo;
                    else {
                        waitingDialog.hide();
                        if ($('#new-artist-modal').length > 0) {
                            $('#new-artist-modal').modal('hide');
                            let newOption = new Option(response.label, response.id_row, false, true);
                            $('#artists').append(newOption);
                        }
                    }
                }
            } else {
                $form.find(":submit").attr("disabled", false);
            }
        }
    });
}

function uploadImage(elements, i, length, main_response, callback) {
    console.log(i , length);
    if (i !== length) {
        let data = elements.get(i).toDataURL("image/jpeg", 0.8);
        $.ajax({
            url: '/admin/ajax/upload/images',
            type: 'POST',
            data: {
                'name' :  $(elements.get(i)).data('filename'),
                'label':  $(elements.get(i)).data('label'),
                'path': main_response.directory,
                'id_row': main_response.id_row,
                'stream': data
            },
            success: function (response) {
                if (response.status === 'success') {
                    uploadImage(elements, i + 1, length, main_response, callback)
                }
            }
        })
    } else {
        callback();
    }
}

function attachCanvas(elements, formData, length, i, callback) {
    if (i !== length) {
        let data = elements.get(i).toDataURL("image/jpeg", 0.7);
        let inputName = $(elements.get(i)).data('name');
        formData.append(inputName + "[" + i + "][name]", $(elements.get(i)).data('filename'));
        formData.append(inputName + "[" + i + "][path]", $(elements.get(i)).data('virtualpath'));
        formData.append(inputName + "[" + i + "][stream]", data);
        attachCanvas(elements, formData, length, i + 1, callback)
    } else {
        callback(formData);
    }
}

function slugify(text) {
    const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;';
    const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------';
    const p = new RegExp(a.split('').join('|'), 'g');

    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(p, c =>
            b.charAt(a.indexOf(c)))     // Replace special chars
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')             // Trim - from end of text
}

window.slugify = slugify;

$.fn.serializeObject = function () {
    var o = {};
    //    var a = this.serializeArray();
    $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select').each(function () {
        if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
            var $parent = $(this).parent();
            var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
            if ($chb != null) {
                if ($chb.prop('checked')) return;
            }
        }
        if (this.name === null || this.name === undefined || this.name === '')
            return;
        var elemValue = null;
        if ($(this).is('select'))
            elemValue = $(this).find('option:selected').val();
        else elemValue = this.value;
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(elemValue || '');
        } else {
            o[this.name] = elemValue || '';
        }
    });
    return o;
};
