<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = [];

    public function artwork(){
        return $this->hasOne('App\Models\Artwork','id', 'artwork_id');
    }
}
