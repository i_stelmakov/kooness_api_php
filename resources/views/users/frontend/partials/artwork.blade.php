<?php
$artist_name = null;
$artist_link = null;

$artist = $artwork->artist()->first();
if($artist){
    $artist_name = $artist->first_name . " " . $artist->last_name;
    $artist_link = route('artists.single', [$artist->slug]);
}
?>
<div class="slider-item one-fourth-col-with-margin">
    <i title="Remove" class="fa fa-times delete-item" data-id="{{ $artwork->id }}" data-section="artworks"></i>
    <div class="slider-item-img square-box">
        <a href="{{ route('artworks.single', [$artwork->slug]) }}">
            <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
        </a>
    </div>
    <div class="slider-item-txt">
        <div class="default-sheet-row slider-item-row artist-name">
            <div class="default-sheet-row-cell">
                <h3><a class="dark-text" href="{{ $artist_link }}">{{ $artist_name }}</a></h3>
            </div>
        </div>
        <h2><a class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a></h2>
        <p class="artwork-date">{{ $artwork->year }}</p>
        <p class="artwork-measures">{{ $artwork->measure_inc }} in {{ $artwork->measure_cm }} cm</p>
        <div class="default-sheet-row slider-item-row">
            <div class="default-sheet-row-cell">
                @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $medium)
                    <a title="{{ $medium->name }}" href="{{ route('artworks.single', [$medium->slug]) }}">{{ $medium->name }}</a>
                @endforeach
            </div>
            @if(!$artwork->available_in_fair || $artwork->show_price)
                <div class="default-sheet-row-cell">
                    {{-- SE artwork segnato come disponibile E artwork non è in nessuna fiera --}}
                    @if( $artwork->status == 1 && !$artwork->available_in_fair)
                        <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                    @else
                        <p class="dark-text artwork-price">SOLD OUT</p>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>