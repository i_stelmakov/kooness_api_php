<?php

namespace App\Console\Commands;

use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\ArtworksMediumCategory;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\Page;
use App\Models\Post;
use App\Models\PostsCategory;
use App\Models\SiteMap;
use App\Models\Tag;
use Illuminate\Console\Command;

class populateSiteMapUrl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create url site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $artists = Artist::all();
        foreach ($artists as $artist) {
            SiteMap::create([
                "url" => "/artists/" . $artist->slug,
                "type" => 'Artist',
                "referer_id" => $artist->id
            ]);
        }

        $artists_categories = ArtistsCategory::all();
        foreach ($artists_categories as $category) {
            SiteMap::create([
                "url" => "/artists/" . $category->slug,
                "type" => 'ArtistsCategory',
                "referer_id" => $category->id
            ]);
        }

        $artworks = Artwork::all();
        foreach ($artworks as $artwork) {
            SiteMap::create([
                "url" => "/artworks/" . $artwork->slug,
                "type" => 'Artwork',
                "referer_id" => $artwork->id
            ]);
        }

        $artworks_categories = ArtworksCategory::all();
        foreach ($artworks_categories as $category) {
            SiteMap::create([
                "url" => "/artworks/" . $category->slug,
                "type" => 'ArtworksCategory',
                "referer_id" => $category->id
            ]);
        }

        $artworks_medium_categories = ArtworksMediumCategory::all();
        foreach ($artworks_medium_categories as $artworks_media_category) {
            $medium = $artworks_media_category->medium()->first();
            $category = $artworks_media_category->category()->first();
            SiteMap::create([
                "url" => "/artworks/" . $medium->slug . "/" . $category->slug,
                "type" => 'ArtworksMediumCategory',
                "referer_id" => $artworks_media_category->id
            ]);
        }

        $exhibitions = Exhibition::all();
        foreach ($exhibitions as $exhibition) {
            SiteMap::create([
                "url" => "/exhibition/" . $exhibition->slug,
                "type" => 'Exhibition',
                "referer_id" => $exhibition->id
            ]);
        }

        $fairs = Fair::all();
        foreach ($fairs as $fair) {
            SiteMap::create([
                "url" => "/fairs/" . $fair->slug,
                "type" => 'Fair',
                "referer_id" => $fair->id
            ]);
        }

        $galleries = Gallery::all();
        foreach ($galleries as $gallery) {
            SiteMap::create([
                "url" => "/galleries/" . $gallery->slug,
                "type" => 'Gallery',
                "referer_id" => $gallery->id
            ]);
        }

        $galleries_categories = GalleriesCategory::all();
        foreach ($galleries_categories as $category) {
            SiteMap::create([
                "url" => "/galleries/" . $category->slug,
                "type" => 'GalleriesCategory',
                "referer_id" => $category->id
            ]);
        }

        $posts_magazine = Post::whereType('magazine')->get();
        foreach ($posts_magazine as $post_magazine) {
            SiteMap::create([
                "url" => "/posts/magazine/" . $post_magazine->slug,
                "type" => 'Post',
                "referer_id" => $post_magazine->id
            ]);
        }

        $posts_news = Post::whereType('news')->get();
        foreach ($posts_news as $post_news) {
            SiteMap::create([
                "url" => "/posts/news/" . $post_news->slug,
                "type" => 'Post',
                "referer_id" => $post_news->id
            ]);
        }

        $posts_categories = PostsCategory::all();
        foreach ($posts_categories as $category) {
            SiteMap::create([
                "url" => "/posts/magazine/" . $category->slug,
                "type" => 'PostsCategory',
                "referer_id" => $category->id
            ]);
        }

//        $tags = Tag::all();
//        foreach ($tags as $tag) {
//            SiteMap::create([
//                "url" => "/tags/" . $tag->slug,
//                "type" => 'Tag',
//                "referer_id" => $tag->id
//            ]);
//        }

        $pages = Page::all();
        foreach ($pages as $page) {
            SiteMap::create([
                "url" => "/pages/" . $page->slug,
                "type" => 'GenericSeoDatum',
                "referer_id" => NULL
            ]);
        }

        /* Statics Route */
        SiteMap::create([
            "url" => "/",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/home",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/artworks",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/galleries",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/galleries/archive",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/artists",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/artists/archive",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/fairs",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/fairs/archive",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/posts/magazine",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/posts/news",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/exhibitions",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/register",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/password/reset",
            "type" => 'GenericSeoDatum'
        ]);
        SiteMap::create([
            "url" => "/cart",
            "type" => 'GenericSeoDatum'
        ]);

    }
}
