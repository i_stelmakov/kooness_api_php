{{-- Mail - Membership Card Expiration --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_CARD_EXPIRATION}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
			The card ending with "{{ $card->end_with }}", associated with the membership plan "{{ $plan->name }}" is about to expire. We encourage you to update it via your control panel.
		</p>

	</div>
@stop
