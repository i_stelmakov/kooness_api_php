@extends('layouts.admin')
@section('title', "Admin | Edit Seo Data for url `{$data->url}`")
@section('page-header', "Admin - Edit Seo Data for url `{$data->url}`")

@section('content')
    @include('seo-data.admin.form', ['action' => route('admin.seo-data.update', [$data->id]), 'method' => 'PATCH'])
@endsection