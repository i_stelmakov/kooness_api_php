@if (count($breadcrumbs))
    <p>
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
            @else
                {{ $breadcrumb->title }}
            @endif
        @endforeach
    </p>
@endif