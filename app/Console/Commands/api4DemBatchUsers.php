<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Api4Dem\Api4DemDataParser;
use Illuminate\Console\Command;

class api4DemBatchUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:4dem:batch:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Batch command for load all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time_start = microtime(true);

        if (!isset($_SERVER['REMOTE_ADDR'])) {
            $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        }
        if (!isset($_SERVER['SERVER_NAME'])) {
            $_SERVER['SERVER_NAME'] = 'Kooness Batch Import';
        }
        $users = User::all();
        $api4Dem = new Api4Dem();

        // indice per limitare il numero di elementi ciclati
//        $i = 0;
        foreach ($users as $user) {
            // check if e-mail address is well-formed
            if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                continue;
            }
            $api4Dem->userSave($user);
            // exit;
            // per limitare il numero di elementi ciclati
//            if (++$i == 2) break;

        }

        $time_end = microtime(true);

        $execution_time = ($time_end - $time_start);

        echo '########################################################################################################################'."\n";
        echo '########################################################################################################################'."\n";
        echo '=== Total Execution Time: ' . $execution_time . ' Sec';
    }
}
