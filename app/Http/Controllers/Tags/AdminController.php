<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Models\SiteMap;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use View;

class AdminController extends Controller
{
    private $_type_url = 'Tag';

    private $_url_prefix = '/tags/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('tags.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'created_at',
            3 => 'options',
        );

        $totalData = Tag::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $tags = Tag::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $tags = Tag::where('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Tag::where('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $edit = route('admin.tags.edit', $tag->id);
                $delete = route('admin.tags.destroy', $tag->id);

                $nestedData['id'] = $tag->id;
                $nestedData['name'] = $tag->name;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($tag->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $tag = new Tag();
        return view('tags.admin.create', compact('tag'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $tag = Tag::create(Input::all());
            if ($tag) {

                SiteMap::create([
                    "url" => $this->_url_prefix . $tag->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $tag->id
                ]);

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.tags.index')
                ];
                $request->session()->flash("success", "Tag with name `{$tag->name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($tag)
    {
        $tag = Tag::find($tag);
        return view('tags.admin.edit', compact('tag'));
    }

    public function update(Request $request, $tag)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $tag = Tag::find($tag);
        if ($request->method() == 'PATCH' && $tag) {
            if ($tag->update(Input::all())) {

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($tag->id)->first();
                $site_map->url = $this->_url_prefix . $tag->slug;
                $site_map->save();

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.tags.index')
                ];
                $request->session()->flash("success", "Tag with name `{$tag->name}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public
    function destroy($tag)
    {
        $tag = Tag::find($tag);
        $tag->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($tag->id)->first();
        $site_map->delete();

        // redirect
        Session::flash('success', "Tag with name `{$tag->name}` is successful removed!");
        return redirect(route('admin.tags.index'));
    }

    public function checkTagIfExist(Request $request)
    {
        if ($request->method() == 'POST') {
            $name = strtolower($request->get('name'));
            $id = $request->get('id');
            if ($id) {
                if (Tag::whereId($id)->whereRaw("LOWER(name) like '{$name}'")->count()) {
                    return response()->json(true);
                }
            }
            if (Tag::whereRaw("LOWER(name) like '{$name}'")->count()) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        }
    }
}
