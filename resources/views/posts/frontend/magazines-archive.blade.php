@extends('layouts.app')

@section('title', ($category && $category->meta_title) ? $category->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : 'Kooness | Magazine'))

@section('body-class', 'magazine-archive')

@section('header-og-abstract', ($category && $category->meta_description) ? $category->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($category && $category->canonical) ? $category->canonical :($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($category && $category->meta_keywords) ? $category->meta_keywords : ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span>
                                @if($category && $category->h1)
                                    <h1>{{ $category->h1 }}</h1>
                                @elseif(($seo_data && $seo_data->h1))
                                    <h1>{{ $seo_data->h1 }}</h1>
                                @elseif($category)
                                    <h1>All {{ $category->name }} post</h1>
                                @else
                                    <h1>Magazine</h1>
                                @endif

                                @if( $category && $category->description )
                                    {!! $category->description !!}
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Gallery Sheet Header !-->


        @if(isset($categories) && count($categories))
            <section id="page-header">
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <section id="category-filter" class="show">
                            <!-- Hiding Box Header !-->
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h3>Featured subjects</h3>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="col-container-with-offset">
                                <div class="category-filter-container">
                                    @foreach($categories as $category_tmp)
                                        <div class="slider-item-img wide-box category-filter-item {{ ($category && $category_tmp->id == $category->id)? 'active':'' }}">
                                            <a href="{{ route('posts.single', ['magazine',$category_tmp->slug]) }}">
                                                <div class="category-filter-item-img background-cover lazy" data-src="{{($category_tmp->url_cover)? $category_tmp->url_cover : $category_tmp->randomImage()}}">
                                                    <h4>{{ $category_tmp->name }}</h4>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        @endif
        <section id="masonry">
            <div class="container boxed-container">

                <!-- Masonry Grid !-->
                <div id="masonry-grid" class="container-vertical-padding-top container-load">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
                    <!-- Masonry Item !-->
                    @foreach($posts as $post_key=>$post)
                        <div class="masonry-item grid-item">
                            <a class="masonry-item-img-link"
                               href="{{ route('posts.single', ['magazine', $post->slug]) }}">
                                <img data-src="{!! ($post->url_full_image) ? $post->url_full_image: '/images/default-cover.jpg' !!}" src="" class="lazy">
                            </a>
                            <div class="masonry-item-txt">
                                <p class="news-date">{{  date('d F Y', strtotime($post->date)) }}</p>
                                <div class="default-sheet-row">
                                    <h2>
                                        <a title="{{ $post->title }}" class="dark-text"
                                           href="{{ route('posts.single', ['magazine', $post->slug]) }}">{{ $post->title }}</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- Masonry Item !-->
                </div>
                <!-- Masonry Grid !-->

            </div>
        </section>
        {!! $posts->links('layouts.includes.pagination') !!}
    </div>
@endsection