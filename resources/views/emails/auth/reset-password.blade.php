{{-- Mail - User Reset Password --}}

@extends('emails.layout')

@section('title')
    {{SUBJECT_MAIL_REGISTRATION_USER_RESET_PSW}}
@endsection

@section('content')
    <br>
    <p style='margin: 16px 30px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
        Hello!<br>
        You are receiving this email because we received a password reset request for your account.
    </p>
    <a title="Reset Password" style="background-color: rgb(16, 16, 16); border: none; box-sizing: border-box; color: rgb(255, 255, 255); cursor: pointer; display: inline-block; font-family: Muli, sans-serif; font-size: 20px; font-stretch: 100%; font-style: normal; font-weight: 400; height: 44px; line-height: 20px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 10px; padding-bottom: 12px; padding-left: 20px; padding-right: 20px; padding-top: 12px; text-decoration-line: none; text-decoration: none; text-decoration-style: solid; vertical-align: baseline; white-space: normal; -webkit-font-smoothing: antialiased;text-decoration: none"
       href="{{ $link }}">Reset Password</a>
    <p style='margin: 16px 30px;; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
        If you did not request a password reset, no further action is required.
    </p>
    <p style='margin: 16px 30px;; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
        Regards, <br>
        Kooness
    </p>

    <hr style='margin: 16px 30px;; color: #636363;'>
    <p style='margin: 16px 30px;; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 10px; line-height: 150%; text-align: left;'>

        If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: <br/>
        {{ $link }}
    </p>
@stop