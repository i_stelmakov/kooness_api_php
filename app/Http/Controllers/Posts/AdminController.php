<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\Post;
use App\Models\PostsCategory;
use App\Models\SiteMap;
use App\Models\Tag;
use App\Models\User;
use App\Services\Notification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use View;

class AdminController extends Controller
{
    private $_directory_images = 'posts';

    private $_type = 'magazine';

    private $_type_url = 'Post';

    private $_url_prefix = '/posts/';


    // variabili private dell'istanza della classe
    private $gallery_id = 0;

    private $artist_id = 0;

    private $fair_id = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if(auth()->user()) {
            // recupero eventuale Gallerist ID che sta accedendo al dato
            $gallery = auth()->user()->gallery()->first();
            if($gallery){
                $this->gallery_id = $gallery->id;
            }
            // recupero eventuale Artist ID che sta accedendo al dato
            $artist = auth()->user()->artist()->first();
            if($artist){
                $this->artist_id = $artist->id;
            }
            // recupero eventuale Fair ID che sta accedendo al dato
            $fair = auth()->user()->fair()->first();
            if($fair){
                $this->fair_id = $fair->id;
            }
        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('posts.admin.index');
    }

    public function retrieve(Request $request)
    {
        if ($request->is('*news*'))
            $this->_type = 'news';

        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'slug',
            3 => 'author',
            4 => 'status',
            5 => 'created_at',
            6 => 'options'
        );


        // creazione di una nuova query basata su model
        $totalData_query = new Post();

        // creazione di una nuova query partendo dall'istanza di Artist, per integrare le condizioni sotto
        $totalData_query = $totalData_query->newQuery();

        $totalData_query->where("type", "=", $this->_type);

        // se è gallerist, artist, fair mostra solo i suoi associati
        if( roleCheck('editor|gallerist|artist|fair') ){
            $totalData_query->whereRaw(" created_by = '".auth()->user()->id."' ");
        }

        // conteggia gli elementi trovati
        $totalData = $totalData_query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        // se non c'è una ricerca
        if (empty($request->input('search.value'))) {
            $posts_query = Post::offset($start)
                ->where("type", "=", $this->_type)
                ->limit($limit)
                ->orderBy($order, $dir);

            // se è gallerist, artist, fair mostra solo i suoi associati
            if( roleCheck('editor|gallerist|artist|fair') ){
                $posts_query->whereRaw(" created_by = '".auth()->user()->id."' ");
            }

            $posts = $posts_query->get();

        }

        // se c'è una ricerca
        else {
            $search = $request->input('search.value');

            $posts_query = Post::where('title', 'LIKE', "%{$search}%")
                ->where("type", "=", $this->_type)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);

            // se è gallerist, artist, fair mostra solo i suoi associati
            if( roleCheck('editor|gallerist|artist|fair') ){
                $posts_query->whereRaw(" created_by = '".auth()->user()->id."' ");
            }

            $posts = $posts_query->get();

            $totalFiltered_query = Post::where('title', 'LIKE', "%{$search}%")
                ->where("type", "=", $this->_type);

            // se è gallerist, artist, fair mostra solo i suoi associati
            if( roleCheck('editor|gallerist|artist|fair') ){
                $totalFiltered_query->whereRaw(" created_by = '".auth()->user()->id."' ");
            }

            $totalFiltered = $totalFiltered_query->count();

        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $edit = route("admin.{$this->_type}.edit", $post->id);
                $delete = route("admin.{$this->_type}.destroy", $post->id);

                $nestedData['id'] = $post->id;
                $nestedData['title'] = $post->title;
                $nestedData['slug'] = $post->slug;
                $nestedData['author'] = $post->author;
                $nestedData['status'] = get_state_status($post->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($post->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create(Request $request)
    {
        if ($request->is('*news*'))
            $this->_type = 'news';

        $post = new Post();
        $tags = Tag::select("id", "name")->get();
        $categories = PostsCategory::select("id", "name")->where("type", "=", $this->_type)->get();
        $gallery_id = $this->gallery_id;
        $artist_id = $this->artist_id;
        $fair_id = $this->fair_id;

        // se gallerista mostra solo le sue relazioni
        if( roleCheck('gallerist') ){

            // servirà per recuperare la select di "Refer to"
            $galleries = Gallery::select("id", "name")->where('id', '=', $this->gallery_id)->get();

            // recupero galleria associata ad utente attuale...
            $user_gallery = Gallery::find($this->gallery_id);

            // ... per recuperare gli artisti associati alla Gallery (tutti)...
            $artists = $user_gallery->artists()->get();
            // ... e per recuperare gli artworks associati alla Gallery (tutti)
            $artworks = $user_gallery->artworks()->get();
            // dd($artists, $artworks);

            $fairs = [];
        }
        // se artist mostra solo le sue relazioni
        elseif( roleCheck('artist') ) {

            $galleries = [];

            // per recuperare gli artisti associati alla Gallery (tutti)...
            $artists = Artist::select("id", "first_name", "last_name")->where('id', '=', $this->artist_id)->get();
            // e per recuperare gli artworks associati alla Gallery (tutti)
            $artworks = Artwork::whereArtistId($this->artist_id)->get();
            // dd($artists, $artworks);

            $fairs = [];

        }
        // se artist mostra solo le sue relazioni
        elseif( roleCheck('fair') ) {

            // recupero fair associata ad utente attuale...
            $user_fair = Fair::find($this->fair_id);

            // recupero galleries di fiera attuale
            $galleries = $user_fair->galleries()->get();

            // recupero artworks di fiera attuale
            $artworks = $user_fair->artworks()->get();

            $artists = [];
            // per ogni artwork associato alla fiera
            foreach ($artworks as $artwork) {

                // recupero artist associato a attuale artworks ciclato...
                $artist = $artwork->artist()->first();

                if(!in_array($artist, $artists)) {
                    $artists[] = $artist;
                }

            }
            $fairs = [$user_fair];
        }
        // altrimenti (tutti gli altri gruppi utenza)
        else {
            $galleries = Gallery::select("id", "name")->get();
            $artists = Artist::select("id", "first_name", "last_name")->get();
            $artworks = Artwork::select("id", "title")->get();
            $fairs = Fair::select("id", "name")->get();
        }

        return view('posts.admin.create', compact('post', 'tags', 'categories', 'artists', 'artworks', 'fairs', 'galleries', 'fair_id', 'artist_id', 'gallery_id'));
    }

    public function store(Request $request)
    {
        if ($request->is('*news*'))
            $this->_type = 'news';

        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['date'])));
            $data['created_by'] = Auth::user()->id;
            $data['type'] = $this->_type;
            $post = Post::create($data);
            if ($post) {

                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $post, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $post, '\App\Models\PostsCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['artworks'])) ? $data['artworks'] : null, $post, '\App\Models\Artwork', 'artworks', 'artwork_ids');
                manyToManyAssociation((isset($data['artists'])) ? $data['artists'] : null, $post, '\App\Models\Artist', 'artists', 'artist_ids');
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $post, '\App\Models\Gallery', 'galleries', 'gallery_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $post, '\App\Models\Fair', 'fairs', 'fair_ids');

                SiteMap::create([
                    "url" => $this->_url_prefix . $this->_type . "/" . $post->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $post->id
                ]);

                if($this->_type == 'news'){
                    if(roleCheck('fair')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_NEW_POST_NEWS_BY_FAIR, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                    if(roleCheck('gallerist')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_NEW_POST_NEWS_BY_GALLERY, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                    if(roleCheck('artist')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_NEW_POST_NEWS_BY_ARTIST, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                } else{
                    if(roleCheck('editor')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_NEW_POST_MAGAZINE, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $post->id,
                    'redirectTo' => route("admin.{$this->_type}.index")
                ];
                if ($this->_type == 'magazine')
                    $request->session()->flash("success", "Post with title `{$post->title}` is successful created!");
                else
                    $request->session()->flash("success", "News with title `{$post->title}` is successful created!");

            }
        }
        return response()->json($response);
    }

    public function edit($post)
    {
        $post = Post::find($post);
        $post->date = date('d/m/Y', strtotime($post->date));
        $tags = Tag::select("id", "name")->get();
        $categories = PostsCategory::select("id", "name")->where("type", "=", $this->_type)->get();

        // se gallerista mostra solo le sue relazioni
        if( roleCheck('gallerist') ){

            // servirà per recuperare la select di "Refer to"
            $galleries = Gallery::select("id", "name")->where('id', '=', $this->gallery_id)->get();

            // recupero galleria associata ad utente attuale...
            $user_gallery = Gallery::find($this->gallery_id);

            // ... per recuperare gli artisti associati alla Gallery (tutti)...
            $artists = $user_gallery->artists()->get();
            // ... e per recuperare gli artworks associati alla Gallery (tutti)
            $artworks = $user_gallery->artworks()->get();
            // dd($artists, $artworks);

            $fairs = [];
        }
        // se artist mostra solo le sue relazioni
        elseif( roleCheck('artist') ) {

            $galleries = [];

            // per recuperare gli artisti associati alla Gallery (tutti)...
            $artists = Artist::select("id", "first_name", "last_name")->where('id', '=', $this->artist_id)->get();
            // e per recuperare gli artworks associati alla Gallery (tutti)
            $artworks = Artwork::whereArtistId($this->artist_id)->get();
            // dd($artists, $artworks);

            $fairs = [];

        }
        // se artist mostra solo le sue relazioni
        elseif( roleCheck('fair') ) {

            // recupero fair associata ad utente attuale...
            $user_fair = Fair::find($this->fair_id);

            // recupero galleries di fiera attuale
            $galleries = $user_fair->galleries()->get();

            // recupero artworks di fiera attuale
            $artworks = $user_fair->artworks()->get();

            $artists = [];
            // per ogni artwork associato alla fiera
            foreach ($artworks as $artwork) {

                // recupero artist associato a attuale artworks ciclato...
                $artist = $artwork->artist()->first();

                if(!in_array($artist, $artists)) {
                    $artists[] = $artist;
                }

            }
            $fairs = [$user_fair];
        }
        // altrimenti (tutti gli altri gruppi utenza)
        else {
            $galleries = Gallery::select("id", "name")->get();
            $artists = Artist::select("id", "first_name", "last_name")->get();
            $artworks = Artwork::select("id", "title")->get();
            $fairs = Fair::select("id", "name")->get();
        }


        $gallery_id = $this->gallery_id;
        $artist_id = $this->artist_id;
        $fair_id = $this->fair_id;

        return view('posts.admin.edit', compact('post', 'tags', 'categories', 'artists', 'artworks', 'fairs', 'galleries', 'fair_id', 'artist_id', 'gallery_id'));
    }

    public function update(Request $request, $post)
    {
        if ($request->is('*news*')) {
            $this->_type = 'news';
        }

        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $post = Post::find($post);
        if ($request->method() == 'PATCH' && $post) {
            $data = Input::all();

            if(isset($data['date'])) {
                $data['date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['date'])));
            }
            if ($post->update($data)) {

                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $post, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $post, '\App\Models\PostsCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['artworks'])) ? $data['artworks'] : null, $post, '\App\Models\Artwork', 'artworks', 'artwork_ids');
                manyToManyAssociation((isset($data['artists'])) ? $data['artists'] : null, $post, '\App\Models\Artist', 'artists', 'artist_ids');
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $post, '\App\Models\Gallery', 'galleries', 'gallery_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $post, '\App\Models\Fair', 'fairs', 'fair_ids');

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($post->id)->first();
                $site_map->url = $this->_url_prefix . $this->_type . "/" . $post->slug;
                $site_map->save();

                if($this->_type == 'news'){
                    if(roleCheck('fair')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_EDIT_POST_NEWS_BY_FAIR, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                    if(roleCheck('gallerist')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_EDIT_POST_NEWS_BY_GALLERY, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                    if(roleCheck('artist')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_EDIT_POST_NEWS_BY_ARTIST, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                } else{
                    if(roleCheck('editor')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_EDIT_POST_MAGAZINE, 'post', $post);
                        Notification::addUsers($notify, $users);
                    }
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $post->id,
                    'redirectTo' => route("admin.{$this->_type}.index")
                ];
                if ($this->_type == 'magazine')
                    $request->session()->flash("success", "Post with title `{$post->title}` is successful saved!");
                else
                    $request->session()->flash("success", "News with title `{$post->title}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public function destroy($post)
    {
        $post = Post::find($post);
        if (Storage::disk()->exists(public_path("uploads/posts/{$post->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/posts/{$post->id}"));
        }
        $post->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($post->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('posts.single', [$post->type, $post->slug]), PHP_URL_PATH), parse_url(route('posts.archive.all', [$post->type]), PHP_URL_PATH));

        if ($post->type == 'magazine')
            Session::flash('success', "Post with title `{$post->name}` is successful removed!");
        else
            Session::flash('success', "News with title `{$post->name}` is successful removed!");
        return redirect(route("admin.{$this->_type}.index"));
    }

}
