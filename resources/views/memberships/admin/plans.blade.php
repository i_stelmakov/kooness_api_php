@extends('layouts.admin')
@section('title', 'Admin | Memberships Plans')
@section('page-header', 'Memberships Plans')
@section('styles')
    <style>
        .membership-card {
            border: 1px solid silver;
            text-align: center;
            padding: 20px;
            margin-bottom: 20px;
            border-radius: 8px;
            background: white;
            color: black;
        }
        .membership-card.selected,
        .membership-card:hover {
            background: #48b6ac;
            color: white;
        }
        .membership-card {
            border: 1px solid silver;
            text-align: center;
            padding: 20px;
            margin-bottom: 20px;
            border-radius: 4px;
            color: black;
        }
        .membership-card h3 {
            font-size: 32px;
            margin-bottom: 40px;
            font-weight: 700;
            color: #48b6ac;
            /* text-decoration: underline; */
        }
        .membership-card b {
            font-size: 16px;
            margin: 0;
        }
        .membership-card h4 {
            font-size: 28px;
            color: #48b6ac;
        }
        .membership-card:hover h4,
        .membership-card:hover h3,
        .membership-card.selected h4,
        .membership-card.selected h3 {
            color: white;
        }

        .membership-card button {
            margin-top: 40px;
            margin-bottom: 20px;
        }
        .current-plan {
            pointer-events: none;
        }
        .current-plan .membership-card:hover{
            background: white;
            color: black;
         }
        .current-plan .membership-card:hover h4,
        .current-plan .membership-card:hover h3{
            color: #48b6ac;
        }

        .current-plan .membership-card {
            position: relative;
        }

        .current-plan .membership-card {
            position: relative;
            overflow: hidden;
        }
        .current-plan .membership-card::before {
            content: "Active";
            position: absolute;
            top: 25px;
            right: -40px;
            z-index: 9999;
            width: 150px;
            height: auto;
            background-color: #48b6ac;
            color: white;
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Safari 3-8 */
            transform: rotate(45deg);

        }
        .current-plan {
            opacity: 1;
        }
        .current-plan button {
            opacity: 0.3;
            pointer-events: none;

        }


    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <p>Below you can monitor and change the status of your membership</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="row">
                        @foreach($plans as $plan)
                            <div class="col-md-4 {{ ($current_plan && $current_plan->id == $plan->id)? 'current-plan' : '' }}">
                                <div class="membership-card">
                                    <h3>{{$plan->name}}</h3>
                                    <b>Fixed Fee:</b>
                                    <h4>{{$plan->fixed_fee}}{{ $plan->getSymbolCurrency() }}/month</h4>
                                    <br>
                                    <b>Success Fee:</b>
                                    <h4>{{$plan->success_fee}}%</h4>
                                    <button class="btn btn-primary choose-plan" data-id="{{$plan->id}}">Choose Plan</button>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <form class="action-submit" action="{{ route('admin.memberships.billing') }}" data-attachment="validate" method="POST">
                            @csrf
                            <input type="hidden" name="membership-plan" value="">
                            @if($current_plan)
                                <input type="hidden" name="request-change-plan" value="true">
                            @endif
                            <input id="membership-card-submit" class="btn btn-primary" type="submit" value="Confirm" disabled>
                        </form>
                    </div>
                </div>
    
            </div>
        </div>
    </div>


    <script>
        $(function() {
            var membershipInput = $('input[name=membership-plan]');
            $(".membership-card").on('click', function(e) {
                var thisBox = $(e.currentTarget);
                var thisBoxClickedElement = $(e.target);
                var allBoxes = $(".membership-card");
                var submitButton = $("#membership-card-submit");

                if ( thisBoxClickedElement.hasClass("choose-plan") || thisBoxClickedElement.parents(".choose-plan").length ) {
                    console.log("Inside button");
                    if (thisBox.hasClass( 'selected' )) {
                        console.log("ha classe");
                        membershipInput.val('');
                        submitButton.attr('disabled', true);

                        allBoxes.removeClass('selected');
                    } else {
                        console.log("NON ha classe");
                        // console.log(thisBoxClickedElement.data('id'));
                        membershipInput.val(thisBoxClickedElement.data('id'));
                        allBoxes.removeClass('selected');
                        thisBox.addClass('selected');
                        submitButton.attr('disabled', false);

                    }
                } else {
                    console.log("Outside button");
                }
            });
        })
    </script>

@endsection