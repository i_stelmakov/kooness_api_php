<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exhibition extends Model
{
    protected $guarded = [];

    protected $appends = ['country', 'artist_ids'];

    protected $fillable = [
        'name',
        'slug',
        'description',
        'url_cover',
        'country_id',
        'city',
        'address',
        'zip',
        'phone',
        'start_date',
        'end_date',
        'status',
        'gallery_id',
        'created_by',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description'
    ];

    public function gallery()
    {
        return $this->hasOne('App\Models\Gallery', 'id', 'gallery_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function artists(){
        return $this->belongsToMany('App\Models\Artist','exhibitions_artists','exhibition_id','artist_id');
    }

    public function getCountryAttribute()
    {
        if ($this->country()->first())
            return $this->country()->first()->name;
        else
            return "";

    }

    public function getArtistIdsAttribute()
    {
        return $this->artists->pluck('id')->toArray();
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-square-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-wide-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
