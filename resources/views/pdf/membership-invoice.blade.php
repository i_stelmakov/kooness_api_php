<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 100%;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }


        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {

            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }


        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }

        table th,
        table td {
            text-align: left;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        /** {*/
            /*border: 1px solid aqua;*/
        /*}*/

        table.main td {
            padding: 5px 20px;
        }

        table.second td {
            padding: 10px 20px;
        }


        #notices .notice {
            color: #5D6975;
            font-size: 1em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 15px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo" style="margin-bottom:20px;">
        <img src="{{public_path('/images/logoblack-alta.png')}}">
    </div>
    <h1>Invoice n.{{ $invoice_number }}</h1>

    <table>
        <tr>
            <td width="50%" align="left" style="padding: 20px">
                <table>
                    <tr>
                        <td>CLIENT</td>
                        <td>{{ $payment->full_name }}</td>
                    </tr>
                    <tr>
                        <td>CF. / PIVA</td>
                        <td>{{ $payment->vat_number ? $payment->vat_number : $payment->cf }}</td>
                    </tr>
                    <tr>
                        <td>ADDRESS</td>
                        <td>{{ $payment->address }}<br>
                            {{ $payment->city }} {{ $payment->zip }} {{ ($payment->state)? ", ".$payment->state : '' }}<br>
                            {{ \App\Models\Country::find($payment->{'country'})->name}}</td>
                    </tr>
                    <tr>
                        <td>DATE</td>
                        <td>{{ date('d/m/Y') }}</td>
                    </tr>
                </table>
            </td>
            <td width="50%" align="right" style="padding: 20px">
                <div id="company" class="" style="text-align:right;" >
                    <div>U-ART SRL</div>
                    <div>Via Cesare Battisti 49</div>
                    <div>46043 Castiglione delle siviere (MN)</div>
                    <div>info@kooness.com</div>
                    <div>VAT number 02439970209</div>
                </div>
            </td>
        </tr>
    </table>
</header>
<main>
    <table class="main">
        <thead>
        <tr>
            <th width="45%">PRODUCT</th>
            <th width="35%">GALLERY</th>
            <th width="20%">TOTAL</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><p>{{ $plan->name }}<br>{{ date('d/m/Y', strtotime($payment->created_at)) }} - {{ date('d/m/Y', strtotime($user->premium_exp_date)) }}</p></td>
                <td><p>{{ $gallery->name }}</p></td>
                <td><p style="text-align: right;">{{  number_format($payment->subtotal, 2, ',','') }} {{ $payment->symbol_currency }}</p></td>
            </tr>
            @if( $payment->tax > 0 )
                <tr>
                    <td>&nbsp;</td>
                    <td><p style="text-align: right;">VAT</p></td>
                    <td><p style="text-align: right;">{!! ($payment->tax)?  number_format($payment->tax , 1, ',','') : '0' !!}% </p></td>
                </tr>
            @endif
            <tr>
                <td>&nbsp;</td>
                <td><p style="text-align: right;"><b>TOTAL</b></p></td>
                <td><p style="text-align: right;"><b>{{ number_format($payment->total, 2, ',','') }} {{ $payment->symbol_currency }}</b></p></td>
            </tr>
        </tbody>
    </table>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
