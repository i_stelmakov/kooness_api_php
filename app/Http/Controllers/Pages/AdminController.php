<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\SiteMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use View;

class AdminController extends Controller
{
    private $_type_url = 'Artist';

    private $_url_prefix = '/artists/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('pages.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'slug',
            3 => 'created_at',
            4 => 'options'
        );

        $totalData = Page::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $pages = Page::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $pages = Page::where('title', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Page::where('title', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($pages)) {
            foreach ($pages as $page) {
                $edit = route('admin.pages.edit', $page->id);
                $delete = null;

                $nestedData['id'] = $page->id;
                $nestedData['title'] = $page->title;
                $nestedData['slug'] = $page->slug;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($page->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function edit($page)
    {
        $page = Page::find($page);
        return view('pages.admin.edit', compact('page'));
    }

    public function update(Request $request, $page)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $page = Page::find($page);
        if ($request->method() == 'PATCH' && $page) {
            $data = Input::all();
            if ($page->update($data)) {

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($page->id)->first();
                $site_map->url = $this->_url_prefix . $page->slug;
                $site_map->save();

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.pages.index')
                ];
                $request->session()->flash("success", "Page with title `{$page->title}` is successful saved!");
            }
        }

        return response()->json($response);
    }

}
