@extends('layouts.admin')@section('title', 'Admin | Orders #'. $order->serial)@section('page-header', 'View Order #'. $order->serial)

@section('styles')
    <style>
        .box-header.with-border {

        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-info-circle"></i>
                            <h3 class="box-title">Order details</h3>
                            @if(!$order->incomplete_order)
                                <div class="pull-right">
                                    <a href="{{ route('admin.orders.resend.current.mail', [$order->id]) }}" class="btn btn-primary"><i class="fa fa-mail-reply"></i>&nbsp; Resend {{ ($order->status == 1) ? 'received' : ($order->status == 2 ? 'completed' : 'shipping') }} email</a>
                                </div>
                            @endif
                        </div>
                        <div class="box-body">
                            <div class="col-md-6">
                                Order number
                                <strong>#{{ $order->serial }}</strong> of {{ date('d/m/Y', strtotime($order->created_at)) }} carried out by {{ $order->user()->first()->last_name }} {{ $order->user()->first()->first_name }}
                                <br> Paid by {{ get_payment_method($order->payment_method) }}<br><br>
                                @if(!$order->incomplete_order)
                                    <div class="form-group">
                                        <form class="ajax-form" method="POST" action="{{ route('admin.orders.update.status', [$order->id]) }}" data-attachment="validate">
                                            @csrf
                                            <label for="status">Status </label><span class="horizontal-space-sm"></span>
                                            <select class="form-control inline order-status" name="status" {!! ($order->status == 3)? 'disabled': '' !!}>
                                                <option value="1" {!! ($order->status == 1)? 'selected="selected"': '' !!} {!! ($order->status >= 1)? 'disabled': '' !!}>Pending</option>
                                                <option value="2" {!! ($order->status == 2)? 'selected="selected"': '' !!} {!! ($order->status >= 2)? 'disabled': '' !!}>Completed</option>
                                                @if($order->status >= 2)
                                                    <option value="3" {!! ($order->status == 3)? 'selected="selected"': '' !!}>Shipped</option>
                                                @endif
                                            </select><span class="horizontal-space-sm"></span>
                                            <input class="btn btn-md btn-primary" type="submit" value="Update" disabled>
                                        </form>
                                        <br>
                                        @if($order->status == 2)
                                            <a class="btn btn-md btn-primary" href="{{ route('admin.orders.current.invoices.pdf', [$order->serial]) }}" download>Download Invoice PDF</a>
                                        @endif
                                    </div>
                                @else
                                    <div class="col-md-12 no-padding">
                                        <p><b>Status:</b> <span>{{ get_status_order($order->status)  }}</span></p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-money"></i>
                            <h3 class="box-title">Billing Address</h3>
                        </div>
                        <div class="box-body">
                            <div class="margin">
                                @if($order->billing_first_name && $order->billing_last_name)
                                    {{ $order->billing_first_name }} {{ $order->billing_last_name }}<br>
                                @else
                                    {{ $order->billing_company_name }}<br>
                                @endif
                                @if($order->billing_fiscal_code)
                                    {{ $order->billing_fiscal_code }}<br>
                                @endif
                                @if($order->billing_vat_number)
                                    {{ $order->billing_vat_number }}<br>
                                @endif
                                {{ $order->billing_address }}<br>
                                {{ $order->billing_city }} {{ $order->billing_zip }} {{ ($order->billing_state)? ", ".$order->billing_state : '' }}
                                <br/>
                                {{ $order->billing_country }} ({{ $order->billing_country_code }})<br/>
                                {{ $order->billing_phone }}<br/> Email: <a>{{ $order->billing_email }}</a><br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-truck"></i>
                            <h3 class="box-title">Shipping Address</h3>
                        </div>
                        <div class="box-body">
                            <div class="margin">
                                {{ $order->shipping_first_name }}  {{ $order->shipping_last_name }}<br>
                                {{ $order->shipping_address }}<br>
                                {{ $order->shipping_city }} {{ $order->shipping_zip }} {{ ($order->shipping_state)? ", ".$order->shipping_state : '' }}
                                <br/>
                                {{ $order->shipping_country }} ({{ $order->shipping_country_code }})<br/>
                                {{ $order->shipping_phone }}<br/> Email: <a>{{ $order->shipping_email }}</a><br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-puzzle-piece"></i>
                            <h3 class="box-title">Order Items</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered table-striped admin-data-table orderTable">
                                <thead>
                                <th>Image</th>
                                <th>Reference</th>
                                <th>Title</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Partial</th>
                                </thead>
                                <tbody>
                                @foreach($order->items()->get() as $item)
                                <tr>
                                    <td>
                                        <img src="{{$item->artwork()->first()->main_image->url }}" style="max-height: 100px" alt="">
                                    </td>
                                    <td><p>{{ $item->artwork()->first()->reference }}</p></td>
                                    <td><p>{{ $item->artwork()->first()->title }}</p></td>
                                    <td><p>{{ $item->quantity }}</p></td>
                                    <td><p>{{ $item->subtotal }} €</p></td>
                                    <td><p>{{ number_format(($item->quantity * $item->subtotal),2,'.','') }} €</p></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td colspan="4" width="70%"></td>
                                    <td align="right" width="20%"><strong>Subtotal</strong></td>
                                    <td width="10%" align="right"> {{ $order->subtotal }} €</td>
                                </tr>
                                @if($order->promo_code)
                                    <tr>
                                        <td colspan="4" width="70%"></td>
                                        <td align="right" width="20%"><strong>Coupon</strong></td>
                                        <td width="10%" align="right"> {{ number_format(-($order->subtotal * $order->promo()->first()->value) / 100, 2, '.', '') }} €</td>
                                    </tr>
                                @endif
                                @if(!$order->incomplete_order)
                                    <tr>
                                        <td colspan="4" width="70%"></td>
                                        <td align="right" width="20%"><strong>Shipping</strong></td>
                                        <td width="10%" align="right">{{ number_format($order->shipping,2,'.','')  }} €</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="4" width="70%"></td>
                                        <td align="right" width="20%" style="vertical-align: middle"><strong>Shipping</strong></td>
                                        <td width="10%" align="right">
                                            <form id="update-shipping" class="ajax-form" method="POST" action="{{ route('admin.orders.update.shipping', [$order->id]) }}" data-attachment="validate">
                                                <input type="number" value="0.00" style="text-align: right; border: 3px solid #48b6ac" min="0" step="0.01" name="shipping" required>
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="4" width="70%"></td>
                                    <td align="right" width="20%"><strong>Total</strong></td>
                                    <td width="10%" align="right">{{ $order->amount }} €</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @if($order->incomplete_order)
                <div class="row">
                    {{-- Footer & Submit --}}
                    <div class='col-md-12 col-xs-12'>
                        <div class="box box-primary box-solid">
                            <div class="box-footer">
                                <div class="pull-right ">
                                    <input class="btn btn-primary" type="submit" value="Update" form="update-shipping"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection