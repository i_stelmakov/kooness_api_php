<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $category->id }}" data-referer="category-artwork">
            {{ method_field($method) }}
            @csrf
            <input type="hidden" name="is_main" value="{{ $is_main }}">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="col-md-12  col-xs-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input id="generated_url" class="form-control" type="text" data-prefix="{{ url('') }}/artworks/" value="{{ ($category->{'slug'})? url('') . '/artworks/' . $category->{'slug'} : '' }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Name (*)</label>
                            <input id="name" class="form-control" type="text" data-slug="#slug" placeholder="Name" name="name" value="{{ old('name', $category->{'name'}) }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="slug">Slug (*)</label>
                            <input id="slug" class="form-control" type="tel" placeholder="Slug" name="slug" value="{{ old('slug', $category->{'slug'}) }}" data-url="#generated_url" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control ck-editor" placeholder="Description" name="description" rows="5">{{ $category->{'description'} }}</textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Images
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Cover</label>
                            @if(!$category->url_cover)
                                <img src="/images/placeholder-wide-box.jpg" class="img-responsive img-preview-2">
                            @else
                                <img src="{{ asset($category->url_cover) }}" class="img-responsive img-preview-2">
                            @endif
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Upload new image…
                                            <input type="file" class="img-uploader" data-label="cover" {{-- variable cover / picture / img1 / img2 --}}data-preview=".img-preview-2" {{-- variable --}}data-minWidth="600" data-minHeight="200" data-maxWidth="2048" data-maxHeight="2048" data-cropper="true" data-aspectRatio="1.777777777777778">
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    SEO Management
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_title">SEO - title</label>
                                <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $category->{'meta_title'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_keywords">SEO - keywords</label>
                                <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $category->{'meta_keywords'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="h1">SEO - H1</label>
                                <input class="form-control"
                                       name="h1" type="text"
                                       placeholder="SEO - H1"
                                       id="h1"
                                       value="{{ $category->{'h1'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="canonical">SEO - Canonical Url</label>
                                <input class="form-control"
                                       name="canonical" type="text"
                                       placeholder="SEO - Canonical Url"
                                       id="canonical"
                                       value="{{ $category->{'canonical'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meta_description">SEO - Description</label>
                                <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $category->{'meta_description'} }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>