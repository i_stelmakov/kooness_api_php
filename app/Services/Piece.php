<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 18/07/18
 * Time: 10:26
 */

namespace App\Services;


use App\Models\Artwork;
use DHL\Datatype\AM\PieceType;

class Piece
{
    private $_id;

    private $_height;

    private $_width;

    private $_depth;

    private $_weight;


    public function __construct(Artwork $artwork)
    {
        $this->_id = $artwork->id;
        $this->_height = $artwork->height;
        $this->_depth = ($artwork->depth)? $artwork->depth : 1;
        $this->_width = $artwork->width;
        $this->_weight = $artwork->weight;
    }

    public function getPiece()
    {
        $piece = new PieceType();
        $piece->PieceID = $this->_id;
        $piece->Height = $this->_height;
        $piece->Depth = $this->_depth;
        $piece->Width = $this->_width;
        $piece->Weight = $this->ceiling(($this->_height * $this->_depth * $this->_width) / 5000, 0.5);
        return $piece;
    }

    function ceiling($number, $significance = 1)
    {
        return (is_numeric($number) && is_numeric($significance)) ? (ceil($number / $significance) * $significance) : false;
    }
}