<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('cart_id')->unsigned()->nullable();
            $table->string('promo_code')->nullable();
            $table->decimal('amount', 10,2)->default(0);
            $table->decimal('subtotal', 10,2)->default(0);
            $table->decimal('tax', 10,2)->default(0);
            $table->decimal('shipping', 10,2)->default(0);
            $table->string('currency')->default('eur');
            $table->string('tracking_code')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('shipping_method');
            $table->string('payment_method');

            $table->string('billing_email');
            $table->string('billing_first_name');
            $table->string('billing_last_name');
            $table->string('billing_phone');
            $table->string('billing_address');
            $table->string('billing_number');
            $table->string('billing_country');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_zip');

            $table->string('shipping_email')->nullable();
            $table->string('shipping_first_name')->nullable();
            $table->string('shipping_last_name')->nullable();
            $table->string('shipping_phone')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_number')->nullable();
            $table->string('shipping_country')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_zip')->nullable();

            $table->string('status');
            $table->text('note')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('cart_id')
                ->references('id')
                ->on('cart')
                ->onDelete('set null');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
