<header id="main-header">
    <!-- upper header !-->
    <div id="upper-header">
        <div class="boxed-container">
            <div class="container col-container-with-offset">
                <!-- upper header col 1 !-->
                <div class="upper-header-col">
                    <a title="Kooness" href="/">
                        <div id="big-logo-container">
                            <img src="/images/logo.png" />
                        </div>
                    </a>
                </div>
                <!-- upper header col 2 !-->
                <div class="upper-header-col">
                    <div class="default-sheet-row no-margin">
                        <h3><strong>To Dream, to Collect</strong></h3>
                    </div>
                    <div id="header-social-row" class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h5 class="accent-color">Follow</h5>
                        </div>
                        <div class="default-sheet-row-cell">
                            <div class="icon-container">
                                <a href="https://www.facebook.com/koonessofficial/"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/KoonessOfficial"><i class="fa fa-twitter"></i></a>
                                <a href="https://it.pinterest.com/kooness/"><i class="fa fa-pinterest"></i></a>
                                <a href="https://www.instagram.com/koonessofficial/"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main navbar !-->
    <div id="main-navbar" class="glut-nav">
        <div class="container boxed-container">
            <div id="nav-container">
                <!-- Mobile Nav !-->
                <div id="mobile-nav">
                    <div id="small-logo-container">
                        <a title="Kooness" href="{{ route('home') }}">
                            <img src="/images/logo.png" />
                        </a>
                    </div>

                    @if(Auth::user())
                        {{-- Cart Logged --}}
                        <a id="mobile-nav-cart-logged" class="notification-box no-desktop" title="cart" href="{{ route('orders.cart') }}">
                            <i class="notification-icon fa fa-shopping-cart"></i>
                            @if( $cart->items()->count() > 0 )
                                <span class="notification-counter">
                                    <small>
                                        {{ $cart->items()->count() }}
                                    </small>
                                </span>
                            @endif
                        </a>
                    @else
                        {{-- Cart Unlogged --}}
                        <a id="mobile-nav-cart-unlogged" class="notification-box no-desktop" title="cart" href="{{ route('orders.cart') }}">
                            <i class="notification-icon fa fa-shopping-cart"></i>
                            @if( $cart->items()->count() > 0 )
                                <span class="notification-counter">
                                    <small>
                                        {{ $cart->items()->count() }}
                                    </small>
                                </span>
                            @endif
                        </a>
                    @endif

                    <i id="mobile-icon" class="fa fa-bars fa-2x"></i>
                </div>
                <!-- Main Nav !-->
                <nav id="main-nav">
                    <!-- Main Menu !-->
                    <div id="main-menu">
                        <ul>
                            {{-- Home --}}
                            <li class="{{ Request::is('home') ? 'current-menu-item' : '' }}">
                                <a href="{{ route('home') }}">Home</a>
                            </li>

                            {{-- Artworks --}}
                            <li class="has-sub-menu {{ Request::is('artworks') ? 'current-menu-item' : '' }}">
                                <a title="Artworks Archive" href="{{ route('artworks.archive.all') }}"><span class="sub-menu-toggler"><i class="fa fa-chevron-up"></i></span>Artworks</a>
                                <ul>
                                    @foreach($medium_menu as $media)
                                        <li class="{{ Request::is('artworks/medium/'.$media->slug) ? 'current-menu-item' : '' }}">
                                            <a title="{{ $media->name }}" href="{{ route('artworks.single', [$media->slug]) }}">{{ $media->name }}</a>
                                        </li>
                                    @endforeach
                                    <li class="{{ Request::is('artworks') ? 'current-menu-item' : '' }}">
                                        <a title="Artwork Archive" href="{{ route('artworks.archive.all') }}">View all...</a>
                                    </li>
                                </ul>
                            </li>

                            {{-- Galleries --}}
                            <li class="has-sub-menu {{ Request::is('galleries/view/intro') ? 'current-menu-item' : '' }}">
                                <a title="Galleries Intro" href="{{ route('galleries.intro') }}"><span class="sub-menu-toggler"><i class="fa fa-chevron-up"></i></span>Galleries</a>
                                <ul>
                                    @foreach($galleries_menu as $gallery)
                                        <li class="{{ Request::is('galleries/'.$gallery->slug) ? 'current-menu-item' : '' }}">
                                            <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                        </li>
                                    @endforeach
                                    <li class="{{ Request::is('galleries') ? 'current-menu-item' : '' }}">
                                        <a title="Galleries Archive" href="{{ route('galleries.archive.all') }}">View all...</a>
                                    </li>
                                </ul>
                            </li>

                            {{-- Artists --}}
                            <li class="has-sub-menu {{ Request::is('artists/view/intro') ? 'current-menu-item' : '' }}">
                                <a title="Artists Intro" href="{{ route('artists.intro') }}"><span class="sub-menu-toggler"><i class="fa fa-chevron-up"></i></span>Artists</a>
                                <ul>
                                    @foreach($artists_menu as $artist)
                                        <li class="{{ Request::is('artists/'.$artist->slug) ? 'current-menu-item' : '' }}">
                                            <a title="{{ $artist->first_name }} {{ $artist->last_name }}" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                        </li>
                                    @endforeach
                                    <li class="{{ Request::is('artists') ? 'current-menu-item' : '' }}">
                                        <a title="View all" href="{{ route('artists.archive.all') }}">View All...</a>
                                    </li>
                                </ul>
                            </li>

                            {{-- Fairs --}}
                            <li class="{{ Request::is('fairs/view/intro') ? 'current-menu-item' : '' }}">
                                <a title="Fairs Intro" href="{{ route('fairs.intro') }}">Fairs</a>
                                {{--<ul>--}}
                                    {{--<li>--}}
                                        {{--<a title="Fairs Archive All" href="{{ route('fairs.archive.all') }}">Fairs Archive All</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a title="Fairs Single" href="{{ route('fairs.single', ['name-1-surname-1']) }}">Fairs Single</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            </li>

                            {{-- Posts --}}
                            <li class="{{ Request::is('posts/magazine') ? 'current-menu-item' : '' }}">
                                <a title="Magazine Archive All" href="{{ route('posts.archive.all', ['magazine']) }}">Magazine</a>
                            </li>

                            {{-- Posts 2 --}}
                            {{--<li>--}}
                                {{--<a title="News Archive All" href="{{ route('posts.archive.all', ['news']) }}">News</a>--}}
                            {{--</li>--}}

                            {{-- <li><a href="news">Magazine</a>
                                <ul>
                                    <li><a href="gallery-news-archive">Gallery News</a></li>
                                    <li><a href="blog">Blog</a></li>
                                    <li><a href="news">News</a></li>
                                </ul>
                            </li> --}}
                            <li><a title="newsletter" href="#newsletter">Newsletter</a></li>
                            {{--<li><a title="app" href="app-page">App</a></li>--}}
                        </ul>
                    </div>

                    <!-- Account Menu !-->
                    <div id="account-menu">
                        @if(Auth::user())
                            @if( roleCheck('superadmin|admin|gallerist') )
                                {{-- Notifiche --}}
                                <a class="notification-box" title="notifications" href="{{ route('admin.notifications.index') }}">
                                    <span class="notification-text">NOTIFICATIONS</span>
                                    <i class="notification-icon fa fa-bell-o"></i>
                                    @if( \App\Services\Notification::getUnreadCount(auth()->user()->id) > 0 )
                                        <span class="notification-counter">
                                            <small>
                                                {{ \App\Services\Notification::getUnreadCount(auth()->user()->id) }}
                                            </small>
                                        </span>
                                    @endif
                                </a>
                            @endif
                            <ul>
                                <li>
                                    {{-- Username --}}
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        {{-- <span class="user-name">{{ Auth::user()->username }}</span> --}}
                                        <span class="user-name">{{ Auth::user()->first_name }}</span>
                                    </a>
                                    {{-- User sub-menu --}}
                                    <ul>
                                        {{-- User Page --}}
                                        <li class="{{ Request::is('users/profile') ? 'current-menu-item' : '' }}">
                                            <a title="Setting" href="{{ route('users.profile') }}">USER PAGE</a>
                                        </li>
                                        <li class="divider"></li>
                                        @if( roleCheck('superadmin|admin|fair|gallerist|artist|editor|seo') )
                                            {{-- CMS --}}
                                            <li><a title="CMS" href="{{ route('admin.dashboard') }}">CMS</a></li>
                                            <li class="divider"></li>
                                        @endif
                                        {{-- Logout --}}
                                        <li><a title="Logout" href="{{ route('logout') }}">Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                            {{-- Cart Logged --}}
                            <a id="main-nav-cart-logged" class="notification-box" title="cart" href="{{ route('orders.cart') }}">
                                <span class="notification-text">CART</span>
                                <i class="notification-icon fa fa-shopping-cart no-mobile"></i>
                                @if( $cart->items()->count() > 0 )
                                    <span class="notification-counter">
                                        <small>
                                            {{ $cart->items()->count() }}
                                        </small>
                                    </span>
                                @endif
                            </a>
                        @else
                            {{-- Login --}}
                            <a title="Login" href="{{ route('register') }}">Login</a>
                            {{-- Cart Unlogged --}}
                            <a id="main-nav-cart-unlogged" class="notification-box" title="cart" href="{{ route('orders.cart') }}">
                                <span class="notification-text">CART</span>
                                <i class="notification-icon fa fa-shopping-cart no-mobile"></i>
                                @if( $cart->items()->count() > 0 )
                                    <span class="notification-counter">
                                        <small>
                                            {{ $cart->items()->count() }}
                                        </small>
                                    </span>
                                @endif
                            </a>

                        @endif
        

                    </div>
                    <!-- .Account Menu !-->

                    
                </nav>
            </div>
        </div>
        <!-- Search !-->
        <div id="header-search">
            <div class="boxed-container">
                <!-- Payoff !-->
                <div id="payoff-container">
                    <i class="fa fa-angle-left payoff-prev" title="Prev"></i>

                    <div class="payoff-slick">
                    @if($header_link)
                        @foreach($header_link as $link)
                            <div class="payoff">
                                <p>
                                    <a href="{{ $link->link }}" title="{{ $link->title }}">{!!  $link->title !!}</a>
                                </p>
                            </div>
                        @endforeach
                    @endif
                    </div>
                    <i class="fa fa-angle-right payoff-next" title="Next"></i>
                </div>
                <div id="header-search-container">
                    <form onsubmit="return false">
                        <input type="search" placeholder="Search..." name="search" id="search" placeholder="Search Employee Details" class="form-control" />
                        <i class="fa fa-search search-button"></i>
                    </form>
                </div>

                <div class="header-search-accordion">
                    <div class="spacer">
                        <div class="header-search-accordion-toggle-box">
                            <div class="header-search-accordion-toggle active">
                                <h4 id="artist-search-accordion-toggler" data-accordion="1">Artists <span id="artists-result-count"></span></h4>
                            </div>
                            <div class="header-search-accordion-toggle">
                                <h4 id="galleries-search-accordion-toggler" data-accordion="2">Galleries <span id="galleries-result-count"></span></h4>
                            </div>
                            <div class="header-search-accordion-toggle">
                                <h4 id="categories-search-accordion-toggler" data-accordion="3">Categories <span id="categories-result-count"></span></h4>
                            </div>
                        </div>
                        <div id="search-results">
                            <div id="#panel-1" class="header-search-accordion-content active" data-slide="1">
                                <ul id="artists-result"></ul>
                            </div>
                            <div id="#panel-2" class="header-search-accordion-content" data-slide="2">
                                <ul id="galleries-result"></ul>
                            </div>
                            <div id="#panel-3" class="header-search-accordion-content" data-slide="3">
                                <ul id="categories-result"></ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>