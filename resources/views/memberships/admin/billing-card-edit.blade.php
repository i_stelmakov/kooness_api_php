@extends('layouts.admin')
@section('title', 'Admin | Memberships Billing & Card information')
@section('page-header', 'Memberships Billing & Card information')
@section('styles')
    <style>
        .card-item-account {
            background-size: auto 100%;
            background-repeat: no-repeat;
            background-position: 4px center;
            height: auto;
            width: auto;
            display: block;
            padding: 0 0 0 44px;
        }
        table {
            width: 100%;
        }
        table td, table tr, table th {
            padding: 10px;
        }
        table td {
            border: 1px solid silver;
        }
        .card-edit-box {
            overflow: hidden;
            max-height: 1800px;
            -webkit-transition: max-height 0.3s;
            -moz-transition: max-height 0.3s;
            -ms-transition: max-height 0.3s;
            -o-transition: max-height 0.3s;
            transition: max-height 0.3s;
        }
        .card-edit-box.collapsed {
            max-height: 0;
        }
    </style>
@endsection

@section('content')
    @if( roleCheck('gallerist') )

        {{--Billing information--}}
        <form id="address-edit" class="action-submit row no-margin" role="form" method="POST" data-attachment="validate">
            @csrf

            <input type="hidden" value="{{ $address->id }}" name="address_id">
            {{-- Section intro --}}
            <div class="">
                <p>The fields marked with asterisk (*) are required </p>
            </div>

            {{-- Details --}}
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Billing Details
                </div>
                <div class="box-body">
                    <div class="row">

                        {{-- Nome/Cognome o Ragione sociale --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="full_name">Name/Surname or Business name (*)</label>
                                <input
                                    id="full_name"
                                    class="form-control"
                                    type="text"
                                    data-slug="#slug"
                                    placeholder="Name/Surname o Business name"
                                    name="full_name"
                                    value="{{ old('full_name', $address->{'full_name'}) }}"
                                    required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Codice Fiscale --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fiscal_code">Fiscal Code (*)</label>
                                <input
                                    id="fiscal_code"
                                    class="form-control"
                                    type="text"
                                    data-slug="#slug"
                                    placeholder="Fiscal Code"
                                    name="fiscal_code"
                                     value="{{ old('fiscal_code', $address->{'fiscal_code'}) }}"
                                    required
                                >
                            </div>
                        </div>

                        {{-- Partita IVA --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="vat_number">VAT number (*)</label>
                                <input
                                    id="vat_number"
                                    class="form-control"
                                    type="text"
                                    data-slug="#slug"
                                    placeholder="VAT numbers"
                                    name="vat_number"
                                    required
                                    value="{{ old('vat_number', $address->{'vat_number'}) }}"
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Country --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <select class="select2 form-control" name="country">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $country->id  == $address->country ? 'selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{-- Address --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Address (*)</label>
                                <input
                                        id="address"
                                        class="form-control"
                                        type="text"
                                        placeholder="Address"
                                        name="address"
                                        value="{{ old('address', $address->{'address'}) }}"
                                        required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- City --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">City (*)</label>
                                <input
                                        id="city"
                                        class="form-control"
                                        type="text"
                                        placeholder="City"
                                        name="city"
                                        value="{{ old('city', $address->{'city'}) }}"
                                        required
                                >
                            </div>
                        </div>

                        {{-- State --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="state">State (*)</label>
                                <input
                                        id="state"
                                        class="form-control"
                                        type="text"
                                        placeholder="State"
                                        name="state"
                                        value="{{ old('city', $address->{'city'}) }}"
                                        required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Zip --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zip">Zip code (*)</label>
                                <input
                                        id="zip"
                                        class="form-control"
                                        type="text"
                                        placeholder="Zip Code"
                                        name="zip"
                                        value="{{ old('zip', $address->{'zip'}) }}"
                                        required
                                >
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="pull-right ">
                                <input class="btn btn-primary" type="submit" form="address-edit" value="Update"/>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </form>


        {{--Card Edit--}}
        <div class="row no-margin">

            {{-- Details --}}
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Card Details
                </div>
                <div class="box-body">
                    <div class="row">

                        @if(1 == 1)
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <th>Credit Card Circuit</th>
                                        <th>Credit Card Number</th>
                                        <th>Name on card</th>
                                        <th>Expires on</th>
                                    </tr>
                                    <tr>
                                        @if($card)
                                            <td>
                                                <span class="card-item-account" id="card-2" style="background-image: url('/images/credit_cards/light/{{ strtolower($card->type) }}.png')">{{ $card->type }}</span></td>
                                            <td>
                                                end in ***{{ $card->end_with }}
                                            </td>
                                            <td>{{ $card->name }}</td>
                                            <td>{{ date('m/Y', strtotime($card->expire_date )) }}</td>
                                        @endif
                                    </tr>
                                </table>
                                <br>
                                <span class="btn btn-primary card-edit-toggle"><i class="fa fa-edit"></i> Change Credit Card</span>
                            </div>
                        @else
                        @endif


                        <div class="col-md-12 card-edit-box">

                            <div class="col-md-12 spacer"></div>

                            <form id="card-edit" class="action-submit row no-margin" role="form" method="POST" data-attachment="validate" data-kind="cards-editing">
                                @csrf

                                {{--Card Owner--}}

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="card_owner">Card Owner</label>
                                        <input class="form-control" type="text" name="card_owner" placeholder="Owner" required="" value="">
                                    </div>
                                </div>
                                {{--Card Number--}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="card_number">Card Number</label>
                                        <input class="form-control card-number" type="text" name="card_number" placeholder="Number" required="" value="">
                                    </div>
                                </div>
                                {{--Card expire year--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_exp_year">Card expire year</label>
                                        <select class="form-control" name="card_exp_year" id="card_exp_year" required onchange="$('#card_exp_month').valid();">
                                            @for($i = date('Y'); $i < date('Y') + 10; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                {{--Card expire month--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_exp_month">Card expire month</label>
                                        <select id="card_exp_month" class="form-control" name="card_exp_month" onchange="$('#card_exp_year').valid();" required>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                </div>
                                {{--Card CVV--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_cvv">Card CVV</label>
                                        <input class="form-control" type="text" name="card_cvv" placeholder="CVV" required="" value="">
                                    </div>
                                </div>
                                {{--Card Type--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_type">Card Type</label>
                                        <input class="form-control" type="text" name="card_type" placeholder="Type" required="" readonly value="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="agree" value="1" id="agree" {{ old('agree')? 'checked="checked"' : '' }} required>
                                            I agree with the terms and conditions of the subscribed membership agreement
                                        </label>
                                        <br>
                                        <span id="accept-error"></span>
                                    </div>
                                </div>

                                {{--Form Footer & Submit--}}
                                <div class="col-md-12">
                                    <div class="pull-right ">
                                        <input class="btn btn-primary" type="submit" form="card-edit" value="Update"/>
                                    </div>
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </div>

        </div>

        <script>
            window.addEventListener("load", function () {

                // controlli su Numero carta e  CVV
                $('form[data-kind="cards-editing"]').each(function () {
                    var $cardNumber = $(this).find('input[name=card_number]');
                    var $cardType = $(this).find('input[name=card_type]');
                    // inserisco tipologia carta in campo
                    $cardNumber.payform('formatCardNumber');
                    $cardNumber.keyup(function () {
                        var cardType = $.payform.parseCardType($cardNumber.val());
                        if (cardType && $.inArray(cardType, ['visa', 'mastercard', 'amex', 'visaelectron', 'maestro']) > -1) {
                            $cardType.val(cardType.charAt(0).toUpperCase() + cardType.slice(1));
                        } else {
                            $cardType.val('')
                        }
                    });
                    // formatta CVV
                    $(this).find('input[name=card_cvv]').payform('formatCardCVC');
                });

                // aggiunge regola a Validate per verificare le date
                jQuery.validator.addMethod("CCExp", function (value, element, params) {
                    var minMonth = new Date().getMonth() + 1;
                    var minYear = new Date().getFullYear();

                    var formMonth = $(element).parents('form').find('select[name="card_exp_month"]');
                    var formYear = $(element).parents('form').find('select[name="card_exp_year"]');

                    var month = parseInt(formMonth.val(), 10);
                    var year = parseInt(formYear.val(), 10);

                    if ((year > minYear) || ((year === minYear) && (month >= minMonth))) {
                        return true;
                    } else {
                        return false;
                    }
                }, "Your Credit Card Expiration date is invalid.");

                // controlli validate su Numero carta e required
                jQuery.validator.addClassRules('card-number', {
                    required: true,
                    creditcard: true
                });


                /* Toggle Nav with Raw JavaScript */
                // Set variables for key elements
                var cardEditToggle = document.getElementsByClassName('card-edit-toggle')[0];
                var cardEditBox = document.getElementsByClassName('card-edit-box')[0];
                console.log(cardEditBox);

                // Start by adding the class "collapse" to the cardEditBox
                cardEditBox.classList.add('collapsed');

                // Establish a function to toggle the class "collapse"
                function cardEditBoxToggle() {
                    cardEditBox.classList.toggle('collapsed');
                }

                // Add a click event to run the cardEditBoxToggle function
                cardEditToggle.addEventListener('click', cardEditBoxToggle);



            }); // END - jQuery window load

        </script>

    @endif
@endsection