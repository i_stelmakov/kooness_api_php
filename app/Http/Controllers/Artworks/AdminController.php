<?php

namespace App\Http\Controllers\Artworks;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\Country;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\SiteMap;
use App\Models\Tag;
use App\Models\User;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Notification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;
use View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $_directory_images = 'artworks';

    private $_type_url = 'Artwork';

    private $_url_prefix = '/artworks/';

    // variabili private dell'istanza della classe
    private $gallery_id = 0;

    private $artist_id = 0;

    private $categories_query = " 
            (SELECT GROUP_CONCAT(t.name ORDER BY t.name SEPARATOR ', ')
             FROM (SELECT ac.name, aac.artwork_id
                   FROM artworks_categories AS ac 
                   JOIN artworks_artworks_categories AS aac ON ac.id = aac.category_id 
                   WHERE NOT ac.is_medium 
                   ORDER BY ac.name ASC) AS t
             WHERE t.artwork_id = artworks.id
             ORDER BY t.name ASC)";

    private $medium_query = "
            (SELECT GROUP_CONCAT(t.name ORDER BY t.name SEPARATOR ', ')
             FROM (SELECT ac.name, aac.artwork_id
                   FROM artworks_categories AS ac 
                   JOIN artworks_artworks_categories AS aac ON ac.id = aac.category_id 
                   WHERE ac.is_medium 
                   ORDER BY ac.name ASC) AS t
             WHERE t.artwork_id = artworks.id
             ORDER BY t.name ASC)";

    public function __construct()
    {
        parent::__construct();
        // recupero eventuale gallerist ID che sta accedendo al dato
        $gallery = auth()->user()->gallery()->first();
        if ($gallery) {
            $this->gallery_id = $gallery->id;
        }

        // recupero eventuale Artist ID che sta accedendo al dato
        $artist = auth()->user()->artist()->first();
        if ($artist) {
            $this->artist_id = $artist->id;
        }

    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'reference',
            2 => 'title',
            3 => 'category',
            4 => 'medium',
            5 => 'price',
            6 => 'artist',
            7 => 'gallery',
            8 => 'sold',
            9 => 'status',
            10 => 'created_at',
            11 => 'options',
        );

        // creazione di una nuova query basata su model
        $totalData_query = new Artwork();

        // creazione di una nuova query partendo dall'istanza di Artist, per integrare le condizioni sotto
        $totalData_query = $totalData_query->newQuery();

        // se è gallerist mostra solo i suoi associati
        if (roleCheck('gallerist')) {
            $totalData_query->whereRaw("gallery_id = '$this->gallery_id'");
        }

        // se è artist mostra solo i suoi associati
        if (roleCheck('artist')) {
            $totalData_query->whereRaw("artist_id = '$this->artist_id'");
        }

        // conteggia gli elementi trovati
        $totalData = $totalData_query->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $filter_categories = false;
        $filter_categories_null = false;
        $filter_medium = false;
        $filter_medium_null = false;

        foreach ($request->get('columns') as $column) {
            if($column['data'] == 'category'){
                if($column['search']['value'] == '_'){
                    $filter_categories_null = true;
                } else{
                    $filter_categories = $column['search']['value'];
                }
            }
            if($column['data'] == 'medium' ){
                if($column['search']['value'] == '_'){
                    $filter_medium_null = true;
                } else{
                    $filter_medium = $column['search']['value'];
                }
            }
        }

        if($order == 'category'){
            $order = 'categories';
        }
        // se non c'è una ricerca
        if (empty($request->input('search.value'))) {
            $artworks_query = Artwork::selectRaw("
             artworks.*,
             {$this->categories_query} as categories,
             {$this->medium_query} as medium")
                ->orderBy($order, $dir);

            // se è gallerist recupera solo i suoi associati
            if (roleCheck('gallerist')) {
                $artworks_query->whereRaw("gallery_id = '$this->gallery_id'");
            }

            // se è artist mostra solo i suoi associati
            if (roleCheck('artist')) {
                $artworks_query->whereRaw("artist_id = '$this->artist_id'");
            }

            $change_total = false;
            if($filter_categories){
                $change_total = true;
                $artworks_query->whereRaw("{$this->categories_query} LIKE '%$filter_categories%'");
            }

            if($filter_categories_null){
                $change_total = true;
                $artworks_query->whereRaw("{$this->categories_query} IS NULL");
            }

            if($filter_medium){
                $change_total = true;
                $artworks_query->whereRaw("{$this->medium_query} LIKE '%$filter_medium%'");
            }

            if($filter_medium_null){
                $change_total = true;
                $artworks_query->whereRaw("{$this->medium_query} IS NULL");
            }


            if($change_total){
                $totalFiltered = count($artworks_query->get());
            }

            $artworks = $artworks_query
                ->offset($start)
                ->limit($limit)
                ->get();


        } // se c'è una ricerca
        else {
            $search = $request->input('search.value');

            $artworks_query = Artwork::selectRaw("
            artworks.*,
            {$this->categories_query} as categories,
            {$this->medium_query} as medium
            ")
                ->where('artworks.title', 'LIKE', "%{$search}%")
                ->orderBy($order, $dir);

            // se è gallerist recupera solo i suoi associati
            if (roleCheck('gallerist')) {
                $artworks_query->whereRaw("gallery_id = '$this->gallery_id'");
            }

            // se è artist mostra solo i suoi artworks
            if (roleCheck('artist')) {
                $artworks_query->whereRaw("artist_id = '$this->artist_id'");
            }

            if($filter_categories){
                $artworks_query->whereRaw("{$this->categories_query} LIKE '%$filter_categories%'");
            }

            if($filter_categories_null){
                $artworks_query->whereRaw("{$this->categories_query} IS NULL");
            }

            if($filter_medium){
                $artworks_query->whereRaw("{$this->medium_query} LIKE '%$filter_medium%'");
            }

            if($filter_medium_null){
                $artworks_query->whereRaw("{$this->medium_query} IS NULL");
            }

            $totalFiltered = count($artworks_query->get());

            $artworks = $artworks_query
                ->offset($start)
                ->limit($limit)
                ->get();

        }

        $data = array();
        if (!empty($artworks)) {
            foreach ($artworks as $artwork) {
                $edit = route('admin.artworks.edit', $artwork->id);

                // passa la possibilità di eliminare solo se non è utente artista
                if (!roleCheck('artist')) {
                    $delete = route('admin.artworks.destroy', $artwork->id);
                } else {
                    $delete = null;
                }
                $nestedData['id'] = $artwork->id;
                $nestedData['reference'] = $artwork->reference;
                $nestedData['title'] = $artwork->title;
                $nestedData['category'] = $artwork->categories ?: '-';
                $nestedData['medium'] =  $artwork->medium ?: '-';
//                $nestedData['year'] = $artwork->year;
                $nestedData['price'] = $artwork->price;
                $nestedData['artist'] = $artwork->artist_name;
                $nestedData['gallery'] = $artwork->gallery_name;
                $nestedData['sold'] = get_state_status($artwork->status == 2);
                $nestedData['status'] = get_state_status(($artwork->status == 1 || $artwork->status == 2));
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($artwork->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('artworks.admin.index');
    }

    public function create()
    {
        $artwork = new Artwork();
        $countries = Country::all();
        $medium = ArtworksCategory::select("id", "name")->whereIsMedium(true)->get();
        $categories = ArtworksCategory::select("id", "name")->whereIsMedium(false)->get();

        // se gallerista mostra nel select di associazione galleria solo il suo
        if (roleCheck('gallerist')) {
            $galleries = Gallery::select("id", "name")->where('id', '=', $this->gallery_id)->get();
        } else {
            $galleries = Gallery::select("id", "name")->get();
        }

        $artists = Artist::select("id", "first_name", "last_name")->get();
        $tags = Tag::select("id", "name")->get();
        $fairs = Fair::all();
        return view('artworks.admin.create', compact('artwork', 'countries', 'categories', 'galleries', 'artists', 'tags', 'medium', 'fairs'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['created_by'] = Auth::user()->id;
            $data['single_piece'] = isset($data['single_piece']) ? true : false;
            $data['signed'] = isset($data['signed']) ? true : false;
            $data['dated'] = isset($data['dated']) ? true : false;
            $data['titled'] = isset($data['titled']) ? true : false;
            $data['framed'] = isset($data['framed']) ? true : false;
            $data['reference'] = DB::raw('LEFT(UUID(), 8)');

            $artwork = Artwork::create($data);
            if ($artwork) {

                $category = [];
                if (isset($data['categories'])) {
                    $category = array_merge($category, $data['categories']);
                }
                if (isset($data['medium'])) {
                    $category = array_merge($category, $data['medium']);
                }
                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $artwork, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((count($category)) ? $category : null, $artwork, '\App\Models\ArtworksCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $artwork, '\App\Models\Fair', 'fairs', 'fair_ids');

                SiteMap::create([
                    "url" => $this->_url_prefix . $artwork->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $artwork->id
                ]);

                if (roleCheck('gallerist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ARTWORK_BY_GALLERY, 'artwork', $artwork);
                    Notification::addUsers($notify, $users);
                }

                if (roleCheck('artist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ARTWORK_BY_ARTIST, 'artwork', $artwork);
                    Notification::addUsers($notify, $users);
                    $gallery = $artwork->gallery()->first();
                    if ($gallery) {
                        $user = $gallery->user()->first();
                        if ($user) {
                            $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ARTWORK_BY_ARTIST, 'artwork', $artwork);
                            Notification::addUsers($notify, [$user]);
                        }
                    }
                }

                // 4Dem Add product
                $artwork = Artwork::find($artwork->id); // find id after first insert
                $api4Dem = new Api4Dem();
                $api4Dem->artworkSave($artwork);

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $artwork->id,
                    'redirectTo' => route('admin.artworks.index')
                ];
                $request->session()->flash("success", "Artwork with name `{$artwork->title}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($artwork)
    {
        $artwork = Artwork::find($artwork);

        $countries = Country::all();
        $medium = ArtworksCategory::select("id", "name")->whereIsMedium(true)->get();
        $categories = ArtworksCategory::select("id", "name")->whereIsMedium(false)->get();

        // se artista 
        if ( roleCheck('artist') ) {
            // trovo la row/record dell'artista 
            $artist = Artist::find($this->artist_id);
            // se lo trovo 
            if($artist){
                // recupero le galleries associate
                $galleries = $artist->galleries()->get();
            } else {
                // altrimenti passo un array vuoto
                $galleries = [];
            }
        } else {
            // per tutti gli altri ruoli recupero tutte le gallerie
            $galleries = Gallery::select("id", "name")->get();
        }


        $artists = Artist::select("id", "first_name", "last_name")->get();
        $tags = Tag::select("id", "name")->get();
        $fairs = Fair::all();
        return view('artworks.admin.edit', compact('artwork', 'countries', 'categories', 'galleries', 'artists', 'tags', 'medium', 'fairs'));
    }

    public function update(Request $request, $artwork)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $artwork = Artwork::find($artwork);
        if ($request->method() == 'PATCH' && $artwork) {
            $data = Input::all();
            $data['single_piece'] = isset($data['single_piece']) ? true : false;
            $data['signed'] = isset($data['signed']) ? true : false;
            $data['dated'] = isset($data['dated']) ? true : false;
            $data['titled'] = isset($data['titled']) ? true : false;
            $data['framed'] = isset($data['framed']) ? true : false;
            if ($artwork->update($data)) {

                $category = [];
                if (isset($data['categories'])) {
                    $category = array_merge($category, $data['categories']);
                }
                if (isset($data['medium'])) {
                    $category = array_merge($category, $data['medium']);
                }
                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $artwork, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((count($category)) ? $category : null, $artwork, '\App\Models\ArtworksCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $artwork, '\App\Models\Fair', 'fairs', 'fair_ids');

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($artwork->id)->first();
                $site_map->url = $this->_url_prefix . $artwork->slug;
                $site_map->save();

                if (roleCheck('gallerist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_ARTWORK_BY_GALLERY, 'artwork', $artwork);
                    Notification::addUsers($notify, $users);
                }

                if (roleCheck('artist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_ARTWORK_BY_ARTIST, 'artwork', $artwork);
                    Notification::addUsers($notify, $users);
                    $gallery = $artwork->gallery()->first();
                    if ($gallery) {
                        $user = $gallery->user()->first();
                        if ($user) {
                            $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_ARTWORK_BY_ARTIST, 'artwork', $artwork);
                            Notification::addUsers($notify, [$user]);
                        }
                    }
                }

                // 4Dem Add product
                $api4Dem = new Api4Dem();
                $api4Dem->artworkSave($artwork);

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $artwork->id,
                    'redirectTo' => route('admin.artworks.index')
                ];
                $request->session()->flash("success", "Artwork with title `{$artwork->title}` is successful saved!");

            }
        }
        return response()->json($response);
    }

    public function destroy($artwork)
    {
        $artwork = Artwork::find($artwork);
        if (Storage::disk()->exists(public_path("uploads/artworks/{$artwork->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/artworks/{$artwork->id}"));
        }

        // 4Dem Remove product
        $api4Dem = new Api4Dem();
        $api4Dem->artworkDelete($artwork);

        $artwork->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($artwork->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('artworks.single', [$artwork->slug]), PHP_URL_PATH), parse_url(route('artworks.archive.all'), PHP_URL_PATH));

        Session::flash('success', "Artwork with name `{$artwork->title}` is successful removed!");
        return redirect(route('admin.artworks.index'));
    }
}
