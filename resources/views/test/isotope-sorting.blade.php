{{-- Test - Intro --}}

@extends('layouts.app')

@section('title')
Test - Intro
@endsection

@section('body-class')
test-intro
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js"></script>
@endsection

@section('content')
<div class="container boxed-container">
<h1>Test Zone ☢</h1>️
<style>

    /* --------------------------------------- */
    /* Isotope Grid */
    /* --------------------------------------- */
    .grid {
        background: #EEE;
        border: 1px solid red;
        width: 100%;
        max-width: 1200px;
        position: relative;
        display: block;
    }
    /* clear fix */
    .grid:after {
        content: '';
        display: block;
        clear: both;
    }

    /* --------------------------------------- */
    /* Isotope item */
    /* --------------------------------------- */
    .item.red { background: #b0221b; }
    .item.blue { background: #2175b0; }
    .item.green { background: green; }

    /* --------------------------------------- */
    /* Extras */
    /* --------------------------------------- */
    .item {
        text-align: center !important;
        color: #fff;
        height: 200px;
        width: 33.3%;
        overflow: hidden;
        display: block;
        margin: 0%;
    }
</style>

    <select class="btn-filter" name="color">
        <option value="*">Any</option>
        <option value="red">Red</option>
        <option value="blue">Blue</option>
    </select>


    <div class="grid">
        <div id="001" class="item red">
            <h3 class="name">red</h3>
        </div>
        <div id="002" class="item blue">
            <h3 class="name">blue</h3>
        </div>
        <div id="003" class="item green">
            <h3 class="name">green</h3>
        </div>
    </div>

</div>

<script type="text/javascript">
    jQuery(document).ready(function($){

        // Variables
        var $ = jQuery;

        // Initialise Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.item',
            layout: 'masonry',
        });

        // Ajax call on filter click
        $('.btn-filter').on('change',function(){

            var actual_item_list = [];
            $(".item").each(function (index, element) {
                actual_item_list.push(element.id);
            });
            console.log("Actual Items list => " + actual_item_list);
            var actual_filter = $( this )[0].name;
            var color_filter_value = $( "select[name='color']" )[0].value;
            console.log("Actual filter => " + actual_filter + ': ' + color_filter_value);


        });

    });
</script>
</div>
@endsection