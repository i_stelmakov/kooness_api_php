@extends('layouts.app')

@section('title', ($exhibition && $exhibition->meta_title) ? $exhibition->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$exhibition->name}"))

@section('body-class', 'exhibitions-single')

@section('header-og-abstract', ($exhibition && $exhibition->meta_description) ? $exhibition->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($exhibition && $exhibition->canonical) ? $exhibition->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($exhibition && $exhibition->meta_keywords) ? $exhibition->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($exhibition->url_full_image) ? $exhibition->url_full_image : null)

@section('header-og-url', Request::url())

@section('content')
<div class="sections">
    <!-- Gallery Sheet Header !-->
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>{{ ($exhibition->h1) ? $exhibition->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $exhibition->name) }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Event Sheet !-->
    <section id="event-sheet" class="default-sheet-section">
        <div class="container boxed-container">
            <div class="container col-container">
                <!-- Product Sheet Image Col !-->
                <div id="product-img-col" class="col two-third-col">
                    <!-- Product Sheet Image !-->
                    <div class="slider-item-img wide-box">
                        <a href="{{ $exhibition->url_full_image }}" data-lightbox="example-set">
                            <div class="background-cover lazy" data-src="{{ $exhibition->url_wide_box }}"></div>
                        </a>
                    </div>
                </div>
                <!-- Event Sheet Col !-->
                <div id="default-sheet" class="col one-third-col">

                    <!-- Event Info !-->
                    <div id="default-sheet-info">
                        <div class="default-sheet-box">
                            <div class="event-date">
                                <p>{{ $exhibition->city }}</p>
                            </div>
                            <div class="default-sheet-row flex-wrap">
                                <div id="event-adress" class="default-sheet-row">
                                    <h4>From {{ date('d/m/Y', strtotime($exhibition->start_date)) }} to {{ date('d/m/Y', strtotime($exhibition->end_date)) }}</h4>
                                </div>
                                <div class="default-sheet-row">
                                    <p class="dark-text">
                                        <strong>Location</strong>
                                    </p>
                                </div>
                                <div class="default-sheet-row">
                                    <h2 class="no-margin"><a href="{{ route('galleries.single', [$exhibition->gallery()->first()->slug]) }}">{{ $exhibition->gallery()->first()->name }}</a></h2>
                                </div>
                                <div class="default-sheet-row">
                                    <p>{{ $exhibition->city }}, {{ $exhibition->address }}<br>{{ $exhibition->zip }}</p>
                                </div>
                            </div>
                        </div>
                        <!-- Gallery Sheet Social !-->
                        <div id="default-sheet-info" class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="dark-text">
                                        <strong>Share</strong>
                                    </p>
                                </div>

                                <div class="default-sheet-row-cell">
                                    <div class="icon-container">
                                        <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($exhibition->description, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                        <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($exhibition->description, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                        <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($exhibition->description, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                        <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($exhibition->description, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                        <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($exhibition->description, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                    </div>
                                </div>
        
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    <div class="container boxed-container">
        <hr class="divider" />
    </div>
    <!-- Description !-->
    <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
        {!! $exhibition->description !!}
    </div>
    <section id="represented-artists-slider" class="default-slider-section ">
        <div class="container boxed-container">
            <div class="default-slider-header">
                <span class="h1">Exhibited artists</span>
                <p>
                    <a class="read-more" href="{{ route('artists.archive.all', ["exhibition" => $exhibition->slug]) }}">All represented Artists</a>
                </p>
            </div>
            <div class="container col-container-with-offset-and-margin">
                @foreach($exhibition->artists()->whereStatus(1)->limit(5)->get() as $artist)
                    <div class="slider-item one-fifths-col-with-margin">
                        <div class="slider-item-img square-box">
                            @if($artist->slug)
                                <a href="{{ route('artists.single', [$artist->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $artist->url_square_box }}"></div>
                                </a>
                            @else
                                <div class="background-cover lazy" data-src="{{ $artist->url_wide_box }}"></div>
                            @endif
                        </div>
                        <div class="slider-item-txt">
                            <div class="default-sheet-row slider-item-row">
                                <div class="default-sheet-row-cell">
                                    <h2>
                                        @if($artist->slug)
                                            <a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->last_name }} {{ $artist->first_name }}</a>
                                        @else
                                            <span title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text">{{ $artist->first_name }} {{ $artist->last_name }}</span>
                                        @endif
                                    </h2>
                                </div>

                                @if($artist->slug)
                                    @if(Auth::user())
                                    <a class="follow secondary-link {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                        @if(Auth::user()->collectionArtists->contains($artist->id))
                                            <i class="fa fa-heart"> </i>
                                            <span class="follow-unfollow">
                                                Unfollow
                                            </span>
                                        @else
                                            <i class="fa fa-heart-o"> </i>
                                            <span class="follow-unfollow">
                                                Follow
                                            </span>
                                        @endif
                                    </a>
                                    @endif
                                @endif
                                
                            </div>
                            @if($artist->slug)
                                <a title="Read more" class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    @if(count(array_filter($exhibitions)))
        <section id="event-slider" class="default-slider-section slider-border">
            <div class="container boxed-container">
                <div class="default-slider-header">
                    <span class="h1">Featured events</span>
                    <p>
                        <a class="read-more" href="{{ route('exhibitions.archive') }}">All featured events</a>
                    </p>
                </div>
                <div class="container col-container-with-offset-and-margin">
                    @foreach($exhibitions AS $key=>$exhibition)
                        @if(!$exhibition)
                            @continue
                        @endif
                        <div class="slider-item one-third-col-with-margin">
                            <div class="on-going-ticket">
                                <h4>{{ ucfirst($key) }}</h4>
                            </div>
                            <div class="slider-item-img wide-box">
                                <a title="{{ $exhibition->name }}" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $exhibition->url_wide_box }}"></div>
                                </a>
                            </div>
                            <div class="slider-item-txt">
                                <h2><a title="{{ $exhibition->name }}" class="dark-text" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">{{ $exhibition->name }}</a></h2>
                                <div class="event-date">
                                    <p class="event-date-p">{{ $exhibition->city }}, {{ $exhibition->country()->first()->code }}</p>
                                </div>
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($exhibition->start_date)) }} to {{ date('d/m/Y', strtotime($exhibition->end_date)) }}</p>
                                    </div>
                                </div>
                                <a title="Read more" class="read-more" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">Read more</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
</div>
@endsection