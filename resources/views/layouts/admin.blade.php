<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="/css/main-admin.css?v={{ get_asset_rev() }}" rel="stylesheet">
    <script src="/js/laroute.js"></script>
    <script src="/js/main-admin.js?v={{ get_asset_rev() }}"></script>

    @yield('scripts')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('styles')
</head>
<body class="hold-transition skin-black sidebar-mini @yield('body-class')">
<div class="wrapper">
    <header class="main-header">


        <a href="{{ route('home') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                <img src="{{asset('/images/logo_email.png')}}" width="20">
            </span>
            <span class="logo-lg">
                <img src="{{asset('/images/logo_full.png')}}">
            </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Guide Menu -->
                    @if( membershipGuideExist() && roleCheck('gallerist') )
                        <li class="membership-guide-menu">
                            <a class="btn btn-md btn-warning" href="{{ route('admin.download.guide.pdf') }}" target="_blank">User Guide</a>
                        </li>
                    @endif

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            @if(\App\Services\Notification::getUnreadCount(auth()->user()->id) > 0)
                               <span class="label label-warning">{{ \App\Services\Notification::getUnreadCount(auth()->user()->id) }}</span>
                            @endif
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have {{ \App\Services\Notification::getUnreadCount(auth()->user()->id) }} notifications
                                {{-- <a href="#" class="float-right">Mark all as read</a> --}}
                            </li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <!-- start notification -->
                                    @foreach(\App\Services\Notification::getUnread(auth()->user()->id) as $notification)
                                        <li>
                                            @if($notification->artwork_id)
                                                <a href="{{ route('admin.artworks.edit', [$notification->artwork_id]) }}">
                                                    <i class="fa fa-paint-brush"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->artist_id)
                                                <a href="{{ route('admin.artists.edit', [$notification->artist_id]) }}">
                                                    <i class="fa fa-address-book-o"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->gallery_id)
                                                <a href="{{ route('admin.galleries.edit', [$notification->gallery_id]) }}">
                                                    <i class="fa fa-picture-o"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->exhibition_id)
                                                <a href="{{ route('admin.exhibitions.edit', [$notification->exhibition_id]) }}">
                                                    <i class="fa fa-th-large"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->post_id)
                                                <?php
                                                    $post = \App\Models\Post::find($notification->post_id);
                                                ?>
                                                @if($post->type == 'magazine')
                                                    <a href="{{ route('admin.magazine.edit', [$notification->post_id]) }}">
                                                        <i class="fa fa-book"></i> {{ $notification->content }}
                                                    </a>
                                                @else
                                                    <a href="{{ route('admin.news.edit', [$notification->post_id]) }}">
                                                        <i class="fa fa-newspaper-o"></i> {{ $notification->content }}
                                                    </a>
                                                @endif
                                            @endif
                                            @if($notification->order_id)
                                                <a href="{{ route('admin.orders.view', [$notification->order_id]) }}">
                                                    <i class="fa fa-shopping-cart"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->order_incomplete_id)
                                                <a href="{{ route('admin.orders.view', [$notification->order_incomplete_id]) }}">
                                                    <i class="fa fa-shopping-cart"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->offers_id)
                                                <a href="{{ route('admin.offers.edit', [$notification->offers_id]) }}">
                                                    <i class="fa fa-money"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                            @if($notification->user_id)
                                                <a href="{{ route('admin.memberships.list') }}">
                                                    <i class="fa fa-money"></i> {{ $notification->content }}
                                                </a>
                                            @endif
                                        </li>
                                    @endforeach
                                    <!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ route('admin.dashboard') }}/notifications">View all</a></li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="/images/default-avatar.png" class="user-image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->username }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/images/default-avatar.png">
                                <p>
                                    {{ Auth::user()->full_name }}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                {{--<div class="pull-left">--}}
                                    {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                {{--</div>--}}
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/images/default-avatar.png" class="img-circle">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->username }}</p>
                    <!-- Status -->
                    <a href="#">
                        {{-- <i class="fa fa-circle text-success"></i> --}}
                        {{-- Role: --}}
                        ( {{ get_user_role_name(Auth::user()->role) }} )
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            @include('layouts.includes.sidebar')
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">

            {!! Breadcrumbs::render() !!}

            <h1>
                @yield('page-header')
                <small>@yield('page-description')</small>
            </h1>


        </section>
        <!-- Main content -->
        <section class="content container-fluid">

            @include('partials.form-alert')
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            <p>Developed by <a title="Notomia" href="https://www.notomia.com/">Notomia</a></p>
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="https://www.notomia.com/">Notomia</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->
</body>
</html>