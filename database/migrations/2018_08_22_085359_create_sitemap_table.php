<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitemapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitemap', function (Blueprint $table) {
            $table->increments('id');
            $table->text('url');
            $table->enum('type', [
                'Artist',
                'ArtistsCategory',
                'Artwork',
                'ArtworksCategory',
                'ArtworksMediumCategory',
                'Exhibition',
                'Fair',
                'Gallery',
                'GalleriesCategory',
                'GenericSeoDatum',
                'Post',
                'PostsCategory',
                'Tag',
            ]);
            $table->integer('referer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitemap');
    }
}
