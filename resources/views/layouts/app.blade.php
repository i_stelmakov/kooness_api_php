<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ app()->getLocale() }}">
    @include('layouts.includes.head')
    <body class="@yield('body-class')">

        {{--SEM / SEO Customs scripts per Page--}}
        @yield('seo-customs-scripts')

        {{--Google Tag Manager--}}
        @if( env('APP_ENVIRONMENT_KIND') == 'prod' )
            @include('googletagmanager::body')
        @endif

        {{--Page Loader--}}
        <div id="loader-body"></div>

        {{--Page Content - Remodal Blur -> Put all the content inside a remodal-bg for obtain blur effect--}}
        <div class="remodal-bg content-loader" id="loader-body-content">
            @include('layouts.includes.header')
                <div class="sections">
                    <section id="breadcrumbs">
                        <div class="container boxed-container">
                            @include('partials.breadcrumbs-front')
                        </div>
                    </section>
                    @yield('content')
                    @if($recentViewed)
                        <section id="recent-viewed-slider" class="default-slider-section">
                            <div class="container boxed-container">
                                <div class="default-slider-header">
                                    <span class="h1">Recently viewed</span>
                                </div>
                                <div class="container col-container-with-offset-and-margin">
                                    @foreach($recentViewed as $recent)
                                        <?php
                                        $artwork = \App\Models\Artwork::find($recent);
                                        ?>
                                        @if($artwork)
                                            <div class="slider-item one-fifths-col-with-margin">
                                                <div class="slider-item-img vertical-box">
                                                    <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                                                        <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    @endif
                    <section id="newsletter">
                        <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
                            <div class="container col-container-with-offset-and-margin">
                                <div class="full-col-with-margin">
                                    <span class="h1">Newsletter</span>
                                </div>
                                <div class="one-third-col-with-margin">
                                    <div class="newsletter-txt">
                                        <p>
                                            Sign up now to our newsletter and get advanced access to the latest contemporary artworks each week directly in your inbox.<span class="no-mobile"> Customize your browsing preferences, and learn more about artists and artworks you love most. Plus, stay updated with all breaking news, exhibition openings and events from the contemporary art world. Create your own art collection, stay tuned and join us!</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="two-third-col-with-margin">
                                    <form method="POST" action="{{ route('subscribe.to.newsletter') }}">
                                        {!! Honeypot::generate('honey_name', 'honey_time') !!}
                                        @csrf
                                        <input type="text" name="firstname" placeholder="Name" required>
                                        <input type="text" name="lastname" placeholder="Surname" required>
                                        <input type="email" name="email" placeholder="Email" required>
                                        <div class="form-privacy">
                                            <p>
                                                <input type="checkbox" required> I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                                            </p>
                                        </div>
                                        <input class="default-input-button" type="submit" value="Sign up">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            @include('layouts.includes.footer')
        </div>{{--END Remodal Blur--}}

        @if(isset($envKind) && $envKind == 'qa')
            <!-- Lead Plus INIZIO codice integrazione per il gruppo KoonessTEst --> 
            <script src="https://app.leadplus.it/render/app.js" id="fogApp" data-group="5ba8b298c673e43b9ff64daf"></script>
        @endif

    </body>
</html>