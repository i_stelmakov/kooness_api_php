@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : "Kooness | {$page->title}")

@section('body-class', '404')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
{{--GENERIC--}}

<section id="page-header">
    <div class="container boxed-container">
        <div class="col-container-with-offset">
            <div class="default-sheet-row">
                <div class="default-sheet-row-cell">
                    <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : $page->title }}</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="user-tabs" class="tab-section">
    <div class="container boxed-container">
        <div class="container col-container-with-offset-and-margin container-vertical-padding-bottom">
            <div class="col">
                <p>{!! $page->content !!}</p>
            </div>
        </div>
    </div>
</section>

@endsection
