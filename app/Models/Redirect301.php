<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Redirect301 extends Model
{
    protected $guarded = [];

    protected $table = '301_redirects';
    
}
