<?php

namespace App\Console\Commands;

use App\Models\Artwork;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Api4Dem\Api4DemDataParser;
use Illuminate\Console\Command;

class api4DemBatchProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:4dem:batch:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Batch command for load all products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time_start = microtime(true);

        if (!isset($_SERVER['REMOTE_ADDR'])) {
            $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        }
        if (!isset($_SERVER['SERVER_NAME'])) {
            $_SERVER['SERVER_NAME'] = 'Kooness Batch Import';
        }
        $artworks = Artwork::all();
        $api4Dem = new Api4Dem();

        // indice per limitare il numero di elementi ciclati
//        $i = 0;
        foreach ($artworks as $artwork) {
            $api4Dem->artworkSave($artwork);
//            if (++$i == 2) break;
        }

        $time_end = microtime(true);

        $execution_time = ($time_end - $time_start);

        echo '########################################################################################################################'."\n";
        echo '########################################################################################################################'."\n";
        echo '=== Total Execution Time: ' . $execution_time . ' Sec';
    }
}
