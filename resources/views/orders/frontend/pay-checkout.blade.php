@extends('layouts.app')

@section('title')
    Pay your order
@endsection

@section('body-class')
    checkout
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/checkout.js"></script>
@endsection

@section('content')
    <section class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>Pay your order</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="cart">
            <div class="boxed-container">
                <div class="in-page-warning warning-not-leave-the-page warning">If the page is closed before payment, please go back to your last email to purchase.</div>

                <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                    @if($order->items()->count())
                        @foreach($order->items()->get() as $item)
                            <?php
                                $product = $item->artwork()->first();
                            ?>
                            <div class="full-col-with-margin cart-item">
                                <div class="col-container">
                                    <div class="one-third-col-with-margin">
                                        <div class="cart-product-img">
                                            <a href="{{ route('artworks.single', [$product->slug]) }}" class="cart-thumb">
                                                <img data-src="{{ $product->main_image->url }}" src="" class="lazy"> </a>
                                        </div>
                                    </div>
                                    <div class="two-third-col-with-margin">
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <h3 class="no-margin">
                                                        <a title="{{ $product->artist_name }}" class="dark-text" href="{{ route('artists.single', [$product->artist->slug]) }}">{{ $product->artist_name }}</a>
                                                    </h3>
                                                </div>
                                                <a class="follow secondary-link"><i class="fa fa-heart-o"></i> Follow</a>
                                            </div>
                                            <div class="default-sheet-row">
                                                <span class="h1" class="no-margin">{{ $product->title }}</span>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Reference</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->reference }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Year</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->year }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Medium</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->medium_string }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Size</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->measure_cm }} cm<br>{{ $product->measure_inc }} in
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box default-sheet-price">
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Price</p>
                                                <div class="default-sheet-row flex-align-center">
                                                    <div class="default-sheet-row-cell">
                                                        <h4 data-price="{{ $product->price }}" data-currency="EUR">{{ $product->pretty_price }}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Artwork offered by</p>
                                                <div class="default-sheet-row">
                                                    <div class="default-sheet-row-cell">
                                                        <h4>
                                                            <a href="{{ route('galleries.single', [$product->gallery->slug]) }}">{{ $product->gallery->name }}</a>, {{ $product->gallery->city }}
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </section>
        {{-- Card Modal--}}
        <div class="remodal" data-remodal-id="modal-cards" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>
            <span class="h1">Cards</span>
            <p>Add or edit all your cards for shipping o billing.</p>
            <a class="accordion" href="#"><i class="fa fa-plus"></i> Add New Card</a>
            <div class="panel">
                <form id="cardForm0" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="cards-editing" data-id="">
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_owner" placeholder="Owner" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col card-number" type="text" name="card_number" placeholder="Number" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <select class="full-col" name="card_exp_month" required>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <select class="full-col" name="card_exp_year" required>
                            @for($i = date('Y'); $i < date('Y') + 10; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_cvv" placeholder="CVV" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_type" placeholder="Type" required="" readonly value="">
                    </div>
                    <input class="small-input-button" type="submit" value="Add New">
                </form>
            </div>
            @foreach($payments as $payment)
                <div class="card-item" data-id="{{ $payment->id }}">
                    <hr>
                    <div class="card-text"><img src="/images/credit_cards/light/{{ strtolower($payment->type) }}.png">
                        <p>{{ $payment->type }} - end in ***{{ $payment->end_with }}</p></div>
                    <a class="delete-card"><i class="fa fa-close"></i> Delete Card</a>
                </div>
            @endforeach
            <hr>
            <br>
            {{-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> --}}
            <button data-remodal-action="confirm" class="remodal-confirm">Close</button>
        </div>
        {{-- Card Modal--}}
        <form method="POST" action="{{ route('orders.checkout.pay') }}" class="form-validate">
            @csrf
            <input type="hidden" value="{{ $order->token }}" name="order_token">
            <section class="cart-payment">
                <div class="boxed-container">
                    <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                        <div class="one-half-col-with-margin">
                            {{--Billing Address Section--}}
                            <div id="billing-addresses" class="container col-container">
                                <div class="col full-col">
                                    <h4>Billing details</h4>
                                    <p>The billing address you chose during the order</p>
                                    <select id="billing-address" name="billing-address" class="address-select" disabled>
                                        <option value="">{{ $order->billing_address . ", " . $order->billing_zip . " " . $order->billing_city . ($order->billing_state ? ", {$order->billing_state }," : "") . " " . $order->billing_country_code }}</option>
                                    </select>
                                </div>
                            </div>
                            {{--Shipping Address Section--}}
                            <div id="shipping-addresses" class="container col-container">
                                <div class="col full-col">
                                    <h4>Shipping details</h4>
                                    <p>The shipping address you chose during the order</p>
                                    <select id="shipping-address" name="shipping-address" class="address-select" disabled>
                                        <option value="">{{ $order->shipping_address . ", " . $order->shipping_zip . " " . $order->shipping_city . ($order->shipping_state ? ", {$order->shipping_state }," : "") . " " . $order->shipping_country_code }}</option>
                                    </select>
                                </div>
                            </div>
                            <div id="note" class="container col-container">
                                <div class="col full-col">
                                    <h4>Shipping Note</h4>
                                    <p>If you have further notes enter them here</p>
                                    <textarea name="note" id="note" style="resize: none" rows="7" placeholder="Notes...">{{ $order->note }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="one-half-col-with-margin">
                            <!-- Cart Summary !-->
                            <div class="default-sheet-box cart-box">
                                <h4>Summary</h4>
                                @if($promo)
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row row-code">
                                            <p class="dark-text">KOONESS: {{ $promo->value }}%</p>
                                        </div>
                                    </div>
                                @endif
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Subtot</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell cart-subtotal">
                                        @if($promo)
                                        @else
                                            <p>{{ number_format($order->subtotal, 2, ',', '') }} €</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Delivery</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                         <p> {{ number_format($order->shipping, 2, ',', '') }} €</p>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Tot</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        @if($promo)
                                            <p class="dark-text cart-price cart-total" data-total="{{ $cart->total_price * (1 - ($promo->value / 100)) }}">
                                                <strong>{{ number_format($order->total_price * (1 - ($promo->value / 100)), 2, ',', '') }} €</strong>
                                            </p>
                                        @else
                                            <p class="dark-text cart-price cart-total" data-total="{{ $cart->total_price }}">
                                                <strong>{{ number_format($order->amount, 2, ',', '') }} €</strong>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- Payment Method !-->
                            <div class="default-sheet-box cart-box payment-box">
                                <h4>Payment method</h4>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Paypal</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <input type="radio" name="payment_method" value="paypal" required>
                                        <img src="/images/paypal.png">
                                    </div>
                                </div>
                                <hr>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Credit Cards</strong></p>
                                    </div>

                                    <div id="credit-cards" class="container col-container">
                                        <div class="col full-col">

                                            <div>
                                                <h4 style="display: inline;">Credit Cards</h4>
                                                <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/mastercard.png">
                                                <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/amex.png">
                                                <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/maestro.png">
                                                <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/visa.png">
                                            </div>

                                            <p>Select your credit card from this list, or add a new one</p>
                                            @foreach($payments as $payment)
                                                <div class="target-card">
                                                    <input id="card-{{ $payment->id }}" type="radio" class="card" name="payment_method" value="{{ $payment->id }}">
                                                    <label for="card-1" style="background-image: url('/images/credit_cards/light/{{ strtolower($payment->type) }}.png')"> {{ $payment->type }} - end in ***{{ $payment->end_with }}</label>
                                                    <br>
                                                </div>
                                            @endforeach
                                            <br>
                                            <a data-remodal-target="modal-cards" href="#"><i class="fa fa-edit"></i> Edit Credit Card list</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="default-sheet-row align-top">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Bank transfer</strong></p>
                                    </div>
                                    <div class="default-sheet-col">
                                        <div class="default-sheet-row-cell">
                                            <input type="radio" name="payment_method" value="bank_transfer" required>
                                            <p class=""><strong>Pay with bank transfer</strong></p>
                                        </div>

                                        <div class="default-sheet-row align-top">
                                            <p class="small-p">
                                                    Contact us at <a href="mailto:info@kooness.com">info@kooness.com</a> to get our bank details. For any questions you can also visit our <u><a href="pages/support-center">FAQ</a></u> page
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <p>
                                        <input id="privacy_policy" type="checkbox" name="privacy_policy" value="privacy_policy" required="">
                                        I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                                        <br>
                                        <span id="accept-error"></span>
                                    </p>
                                </div>
                            </div>
                            <button id="checkout-btn" class="black-button" type="submit">Checkout</button>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </section>

@endsection