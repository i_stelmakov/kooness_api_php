<?php

namespace App\Http\Controllers\Galleries;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\LayoutPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Validator;

class FrontendController extends FrontendExtendedController
{
    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Artworks Intro
    public function intro()
    {
        $slides = [];
        $widgets = [];
        $layout = LayoutPage::whereLabel('galleries')->first();
        if ($layout) {
            $slider = $layout->slider()->first();
            if ($slider) {
                $contents = json_decode($slider->contents);
                foreach ($contents as $content) {
                    $gallery = Gallery::find($content);
                    if ($gallery) {
                        $slides[] = $gallery;
                    }
                }
            }
            $result = $layout->widgets()->orderBy('order', "ASC")->get();
            foreach ($result as $row) {
                $tmp = [
                    'title' => $row->title,
                    'subtitle' => $row->subtitle,
                    'description' => $row->description,
                    'section' => $row->section,
                    'letter' => 'a',
                    'link' => null,
                    'link_title' => "View all",
                    'template' => $row->template,
                    'items' => []
                ];
                $items = json_decode($row->items);
                foreach ($items as $item) {
                    if ($row->section == 'artworks') {
                        $rows = Artwork::find($item);
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artworks";
                    } else if ($row->section == 'artists') {
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artists";
                        $rows = Artist::find($item);
                    } else if ($row->section == 'galleries') {
                        $tmp['letter'] = 'g';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured galleries";
                        $rows = Gallery::find($item);
                    }
                    $tmp['link'] = $row->link;
                    if (isset($rows) && $rows) {
                        $tmp['items'][] = $rows;
                    }
                }
                $widgets[] = $tmp;
            }
        }
        $galleries = Gallery::whereStatus(1)->orderBy('created_at', "DESC")->limit(12)->get();
        $cities = DB::table('galleries')
            ->select('countries.*')
            ->join('countries', 'galleries.country_id', '=', 'countries.id')
            ->groupBy('countries.id')
            ->orderBy('countries.name', 'ASC')
            ->get();
        $categories = GalleriesCategory::select("slug", "name")->orderBy('created_at', "DESC")->get();
        return view('galleries.frontend.intro', compact('slides', 'widgets', 'galleries', 'categories', 'cities'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Archive All
    public function archive(Request $request, $slug_name = null)
    {
        $fair = null;
        $category = null;
        $filter_fair = $request->get('fair');
        $filter_start = $request->get('startWith');
        $filter_location = $request->get('location');
        $filter_category = $request->get('category');
        $cities = DB::table('galleries')
            ->select('countries.*')
            ->join('countries', 'galleries.country_id', '=', 'countries.id')
            ->groupBy('countries.id')
            ->orderBy('countries.name', 'ASC')
            ->get();
        $categories = GalleriesCategory::select("slug", "name")->orderBy('created_at', "DESC")->get();
        $query = Gallery::select('galleries.*')->whereRaw('galleries.status = 1')->orderBy('galleries.created_at', "DESC");
        if ($filter_location) {
            $query->join('countries', 'galleries.country_id', '=', 'countries.id')
                ->where("countries.code", "=", $filter_location);
        }
        if ($slug_name) {
            $category = GalleriesCategory::whereSlug($slug_name)->first();
        }

        if ($filter_category) {
            $query->join('galleries_galleries_categories as p1', "galleries.id", "=", "p1.gallery_id");
            $query->join('galleries_categories as c1', "p1.category_id", "=", "c1.id");
            $query->where("c1.slug", "=", $filter_category);

        }
        if ($category) {
            $query->join('galleries_galleries_categories as p2', "galleries.id", "=", "p2.gallery_id");
            $query->join('galleries_categories as c2', "p2.category_id", "=", "c2.id");
            $query->where("c2.slug", "=", $category->slug);
        }
        if ($filter_start) {
            $query->where("galleries.name", "LIKE", $filter_start . "%");
        }
        if ($filter_fair) {
            $fair = Fair::whereSlug($filter_fair)->first();
            if ($fair) {
                $filter_value = $fair->slug;
                $filter_type = 'fair';
                $query->join('fairs_galleries', "galleries.id", "=", "fairs_galleries.gallery_id");
                $query->join('fairs', "fairs_galleries.fair_id", "=", "fairs.id");
                $query->where("fairs.slug", "=", $filter_fair);
            }
        }
        $galleries = $query->paginate(18);
        $galleries->appends($request->all());

        return view('galleries.frontend.archive', compact('galleries', 'categories', 'fair', 'filter_type', 'filter_value', 'cities', 'category', 'hide_load_more'));
    }

    // Galleries Single
    public function single(Request $request, $slug_name)
    {
        $category = GalleriesCategory::whereSlug($slug_name)->first();
        if ($category) {
            return $this->archive($request, $slug_name);
        }
        $gallery = Gallery::whereSlug($slug_name)->first(); // select * from galleries (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)
        if ($gallery) {
            $prev = Gallery::whereStatus(1)->where("id", "<", $gallery->id)->orderBy('created_at', "DESC")->first();
            $next = Gallery::whereStatus(1)->where("id", ">", $gallery->id)->orderBy('created_at', "ASC")->first();
            $exhibitions = [
                'past' => $gallery->exhibitions()->whereRaw("end_date < CURDATE()")->orderBy('end_date', "DESC")->first(),
                'current' => $gallery->exhibitions()->whereRaw("start_date <= CURDATE() AND end_date > CURDATE()")->orderBy('end_date', "ASC")->first(),
                'future' => $gallery->exhibitions()->whereRaw("start_date > CURDATE()")->orderBy('start_date', "ASC")->first()
            ];
            $country = $gallery->country()->first();
            $categories = $gallery->categories()->get();
            $tags = $gallery->tags()->get();
            return view('galleries.frontend.single', compact('gallery', 'country', 'categories', 'tags', 'prev', 'next', 'exhibitions'));
        } else {
            return redirect(route('galleries.archive.all'));
        }
    }

//    public function loadMore(Request $request)
//    {
//        if ($request->ajax()) {
//            $category_id = $request->get('category');
//            $filter_value = $request->get('filter_value');
//            $filter_type = $request->get('filter_type');
//
//            $filter_alpha = $request->get('filter_alpha');
//            $filter_location = $request->get('filter_location');
//            $filter_category = $request->get('filter_category');
//
//            $offset = $request->get('offset');
//
//            $category = null;
//
//            $query = Gallery::select('galleries.*')->offset($offset)->whereRaw('galleries.status = 1')->orderBy('galleries.created_at', "DESC")->limit(19);
//            if ($filter_location) {
//                $query->join('countries', 'galleries.country_id', '=', 'countries.id')
//                    ->where("countries.code", "=", $filter_location);
//            }
//            if ($category_id) {
//                $category = GalleriesCategory::whereId($category_id)->first();
//            }
//
//            if ($category) {
//                $query->join('galleries_galleries_categories as p2', "galleries.id", "=", "p2.gallery_id");
//                $query->join('galleries_categories as c2', "p2.category_id", "=", "c2.id");
//                $query->where("c2.slug", "=", $category->slug);
//            }
//
//            if ($filter_category) {
//                $query->join('galleries_galleries_categories', "galleries.id", "=", "galleries_galleries_categories.gallery_id");
//                $query->join('galleries_categories', "galleries_galleries_categories.category_id", "=", "galleries_categories.id");
//                $query->where("galleries_categories.slug", "=", $filter_category);
//            }
//
//            if ($filter_alpha) {
//                $query->where("galleries.name", "LIKE", $filter_alpha . "%");
//            }
//
//            if ($filter_type && $filter_value) {
//                $query->join('fairs_galleries', "galleries.id", "=", "fairs_galleries.gallery_id");
//                $query->join('fairs', "fairs_galleries.fair_id", "=", "fairs.id");
//                $query->where("fairs.slug", "=", $filter_value);
//            }
//
//            $galleries = $query->get();
//
//            $hide_load_more = false;
//            if(count($galleries) <= 18){
//                $hide_load_more = true;
//            } else{
//                $galleries->pop();
//            }
//
//            $return = [];
//            foreach ($galleries as $key => $gallery) {
//                $return[] = [
//                    "id" => $gallery->id,
//                    "name" => $gallery->name,
//                    "url" => route('galleries.single', [$gallery->slug]),
//                    "image" => $gallery->url_wide_box,
//                    "city" => $gallery->city,
//                    "address" => $gallery->address,
//                    "auth_user" => (Auth::user()) ? true : false,
//                    "follow_status" => (Auth::user() && Auth::user()->collectionGalleries->contains($gallery->id)) ? true : false
//                ];
//            }
//
//            return response()->json([
//                "result" => "success",
//                "item_list" => $return,
//                "hide_load_more" => $hide_load_more
//            ]);
//        }
//    }

    public function searchBy(Request $request)
    {
        if ($request->ajax()) {
            $filter_data = $request->get('filter_data');
            $current_ids = ($request->get('current')) ? $request->get('current') : [];
            $query = Gallery::select('galleries.*')->whereStatus(1)->orderBy('galleries.created_at', "DESC")->limit(12);
            if ($filter_data['location']) {
                $query->join('countries', 'galleries.country_id', '=', 'countries.id')
                    ->where("countries.code", "=", $filter_data['location']);
            }
            if ($filter_data['category']) {
                $query->join('galleries_galleries_categories', "galleries.id", "=", "galleries_galleries_categories.gallery_id");
                $query->join('galleries_categories', "galleries_galleries_categories.category_id", "=", "galleries_categories.id");
                $query->where("galleries_categories.slug", "=", $filter_data['category']);
            }
            if ($filter_data['alpha'] && $filter_data['alpha'] != 'all') {
                $query->where("galleries.name", "LIKE", $filter_data['alpha'] . "%");
            }
            $galleries = $query->get();

            $tmp = [];
            foreach ($galleries as $key => $gallery) {
                if (!empty($current_ids)) {
                    if (in_array($gallery->id, $current_ids)) {
                        $index = array_search($gallery->id, $current_ids);
                        if ($index > -1) {
                            unset($current_ids[$index]);
                        }
                        continue;
                    }
                }
                $tmp[] = [
                    "id" => $gallery->id,
                    "name" => $gallery->name,
                    "url" => route('galleries.single', [$gallery->slug]),
                    "image" => $gallery->url_wide_box,
                    "city" => $gallery->city,
                    "address" => $gallery->address,
                ];
            }

            $limit = 12;
            if ((12 - count($current_ids)) > 0) {
                if ((12 - count($current_ids) < count($tmp))) {
                    $limit = count($tmp);
                } elsE {
                    $limit = 12 - count($current_ids);
                }
            }
            $return = array_slice($tmp, 0, $limit);

            $results_array = [
                "new_item" => $return,
                "remove_item" => $current_ids
            ];

            return response()->json(["result" => "success", 'items' => $results_array]);
        }
    }

    public function requestSubscription()
    {

        $data = Input::all();

        $validator = Validator::make($data, [
            'honey_name' => 'honeypot',
            'honey_time' => 'required|honeytime:5'
        ]);

        if ($validator->fails()) {
            return redirect()->back();
        }

        $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');
        $name = 'Kooness Gallery Application';

        $subject = SUBJECT_MAIL_GALLERIST_ACTIVATION;

        $data['phone'] = $data['phone'] ?: 'not specified';
        $data['website'] = $data['website'] ?: 'not specified';
        $data['events'] = $data['events'] ?: 'not specified';

        Mail::send('emails.subscription-gallery', $data, function ($message) use ($to, $name, $subject) {
            $message->to($to, $name)->subject($subject);
        });

        return View::make('galleries.frontend.thank-you-subscription');
    }
}
