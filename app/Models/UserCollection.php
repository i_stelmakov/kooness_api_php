<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCollection extends Model
{
    protected $guarded = [];

    public function artworks(){
        return $this->belongsToMany('App\Models\Artwork', 'user_collection_artworks', 'collection_id' ,'artwork_id')->withPivot('id');
    }
    
}
