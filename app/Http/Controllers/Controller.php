<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if(!Auth::user()){
//            dd("here");
//            return redirect()->route('register')->withErrors(['Error!' => "You can't access to this section until you have been logged in"])->send();
            return redirect()->route('register')->send();
        }
        if(roleCheck('gallerist') &&  !isPremiumUser() && !isTrialUser()){
            return redirect()->route('admin.memberships.plans')->send();
        }

    }
}
