@extends('layouts.app')@section('title', 'Login')

@section('seo-customs-scripts')
    <script>
        window.addEventListener('load', function() {

            // Complete registration (track event on inline action)
            var completeRegistrationButton = document.getElementById('completeRegistrationButton');
            completeRegistrationButton.addEventListener(
                'click',
                function() {
                    fbq('track', 'CompleteRegistration');
                    console.log('Facebook Pixel CompleteRegistration triggered!');
                },
                false
            );
        });
    </script>
@endsection

@section('content')
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>Welcome to Kooness</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="boxed-container">
        @if(Session::get('registered') && Session::get('registered') == 'success')
            <div class="in-page-warning success">
                Congratulations! You have correctly registers to our system. Now you can login with the credentials you have chosen.
            </div>
        @endif

        @if(isset($from_checkout))
            <div class="in-page-warning error">You must be registered and logged in order to proceed with the checkout.</div>
        @endif
        {{--@dd($errors);--}}
        @if (count($errors))
            @foreach($errors->all() as $error)
                <div class="in-page-warning error">{{ $error }}</div>
            @endforeach
        @endif

        @if(!Auth::user() && Request::get('referer') && Request::get('referer') == '6ba3e2f93fad23c82b0d5deeca05bc5d5f6f26c4')
            <div class="in-page-warning error">You must be registered and logged in order to proceed with the offer</div>
        @endif

        @if(!Auth::user() && Request::get('referer') && Request::get('referer') == '95310609c1d2ca08ef1b2899b8d273d36ef53f7a')
            <div class="in-page-warning error">You must be registered and logged in order to proceed with the request</div>
        @endif

        <div class="container col-container-with-margin container-vertical-padding-bottom">
            <!-- Tab 1 !-->
            <div class="col one-third-col-with-margin container-vertical-padding-bottom login-col">
                <span class="h1">Login</span>
                <form method="POST" action="{{ route('login') }}" class="col-container-with-offset-and-margin k-form">
                    @csrf
                    @if(isset($from_checkout))
                        <input type="hidden" value="true" name="from_checkout">
                    @endif

                    @if(!Auth::user() && Request::get('referer') && Request::get('referer') == '6ba3e2f93fad23c82b0d5deeca05bc5d5f6f26c4')
                        <input type="hidden" value="{{ urldecode(Request::get('id')) }}" name="from_offer_id">
                    @endif

                    @if(!Auth::user() && Request::get('referer') && Request::get('referer') == '95310609c1d2ca08ef1b2899b8d273d36ef53f7a')
                        <input type="hidden" value="{{ urldecode(Request::get('id')) }}" name="from_available_id">
                    @endif

                    <div class="full-col-with-margin form-privacy social-button facebook">
                        <p>
                            <a href="{{ route('auth.social', 'facebook') }}"><i class="fa fa-facebook"></i>Login with Facebook</a>
                        </p>
                    </div>
                    <div class="full-col-with-margin form-privacy social-button twitter">
                        <p>
                            <a href="{{ route('auth.social', 'twitter') }}"><i class="fa fa-twitter"></i>Login with Twitter</a>
                        </p>
                    </div>
                    <input id="username_login" placeholder="Username" type="text" class="full-col-with-margin {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>
                    <input id="password_login" placeholder="Password" type="password" class="full-col-with-margin {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    <div class="full-col-with-margin">
                        <p>
                            <a title="Forgot your password" href="{{ route('password.request') }}">Forgot your password?</a>
                        </p>
                    </div>
                    <div class="full-col-with-margin">
                        <input class="default-input-button" type="submit" value="Login">
                    </div>
                </form>
            </div>
            <!-- Tab 2 !-->
            <div class="col two-third-col-with-margin container-vertical-padding-bottom register-col">
                <span class="h1">Register</span>
                <form class="col-container-with-offset-and-margin k-form" role="form" method="POST" action="{{ url('/register') }}">
                    {!! Honeypot::generate('honey_name-register', 'honey_time-register') !!}
                    @csrf

                    @if(isset($from_checkout))
                        <input type="hidden" value="true" name="from_checkout">
                    @endif

                    @if(!Auth::user() && Request::get('referer') && Request::get('referer') == '6ba3e2f93fad23c82b0d5deeca05bc5d5f6f26c4')
                        <input type="hidden" value="{{ urldecode(Request::get('id')) }}" name="from_offer_id">
                    @endif

                    <input id="first_name" class="one-half-col-with-margin {{ $errors->has('first_name') ? ' is-invalid' : '' }}" type="text" placeholder="Name" name="first_name" value="{{ old('first_name') }}" required/>
                    <input id="last_name" class="one-half-col-with-margin {{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text" placeholder="Surname" name="last_name" value="{{ old('last_name') }}" required/>
                    <input id="email" class="one-half-col-with-margin" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required/>
                    <input id="username" class="one-half-col-with-margin {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" placeholder="Username" name="username" value="{{ old('username') }}" required/>
                    <input id="password" class="full-col-with-margin {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Enter your password" name="password" value="" required/>
                    <input id="password_confirmation" class="full-col-with-margin {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" placeholder="Repeat your password" autocomplete="false" name="password_confirmation" value="" required/>
                    <div class="full-col-with-margin form-privacy">
                        <p>
                            <input id="privacy_policy" type="checkbox" name="privacy_policy" value="privacy_policy" required> I read the
                            <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data</p>
                        <p>
                            <input id="newsletter" type="checkbox" name="newsletter" value="newsletter"> Subscribe to newsletter
                        </p>
                    </div>
                    <div class="full-col-with-margin">
                        <input id="completeRegistrationButton" class="default-input-button" type="submit" value="Register"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
