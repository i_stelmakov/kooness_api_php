<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCfVatAndSocetyNameToUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->string('company_name')->nullable()->after('last_name');
            $table->string('fiscal_code')->nullable()->after('company_name');
            $table->string('vat_number')->nullable()->after('fiscal_code');
            $table->enum('type', ['private', 'company'])->default('private')->after('country_code');
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->dropColumn('company_name');
            $table->dropColumn('fiscal_code');
            $table->dropColumn('vat_number');
            $table->dropColumn('type');
            $table->string('first_name')->nullable(false)->change();
            $table->string('last_name')->nullable(false)->change();
        });
    }
}
