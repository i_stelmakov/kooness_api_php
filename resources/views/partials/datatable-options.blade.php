@if(isset($upload) && $upload)
    <a href='{{$upload}}' title='UPLOAD'>
        <button class='btn btn-info'><i class='fa fa-upload'></i></button>
    </a>
@endif

@if($edit)
    <a href='{{$edit}}' title='EDIT'>
        <button class='btn btn-warning'><i class='fa fa-edit'></i></button>
    </a>
@endif
@if(isset($not_editable) && $not_editable)
    <span class="error">
        NOT EDITABLE*
    </span>
@endif
@if(isset($view))
    <a href='{{$view}}' title='VIEW'>
        <button class='btn btn-info'><i class='fa fa-eye'></i></button>
    </a>
@endif
@if(isset($preview) && $preview)
    <a>
        <button class='btn btn-xs btn-danger'><i class='fa fa-eye'></i></button>
    </a>
@endif
@if(isset($accept) && $accept)
    <a href='{{$accept}}' title='ACCEPT'>
        <button class='btn btn-xs btn-success'><i class='fa fa-check-circle-o'></i> ACCEPT</button>
    </a>
@endif
@if(isset($reject) && $reject)
    <a href='{{$reject}}' title='REJECT'>
        <button class='btn btn-xs btn-danger'><i class='fa fa-times-circle-o'></i> REJECT</button>
    </a>
@endif
@if(isset($invoice) && $invoice)
    <a href='{{$invoice}}' title='INVOICE' target="_blank">
        <button class='btn btn-xs btn-info'><i class='fa fa-file-pdf-o'></i> INVOICE</button>
    </a>
@endif
@if($delete)
    @if(roleCheck('superadmin|admin|gallerist|artist') || (roleCheck('seo') && Request::is('admin/ajax/redirects/retrieve')))
        <form method="POST" action="{{$delete}}" class="inline" onsubmit="return alert(this)">
            @csrf
            {{method_field('DELETE')}}
            <button class='btn btn-danger' onclick=""><i class='fa fa-remove'></i></button>
        </form>
        <script>
            function alert(form) {
                swal({
                        title: 'Are you sure?',
                        text: 'You will not be able to recover this data!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'Cancel'
                    },
                    function () {
                        $(form)[0].submit();
                    });
                return false;
            }
        </script>
    @endif

@endif