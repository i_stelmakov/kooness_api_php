@extends('layouts.app')
@section('title', 'Reset your password')

@section('content')
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>Reset your password</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container boxed-container">
        <p>Please enter your e-mail address and submit to receive a message with instruction to reset your password.</p>
        <form method="POST" action="{{ route('password.email') }}" class="col-container-with-offset-and-margin k-form">
            @csrf
            <input id="email" class="one-half-col-with-margin" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required/>
            <div class="full-col-with-margin">
                <input class="default-input-button" type="submit" value="Send Password Reset Link" />
            </div>
        </form>
    </div>
    <br><br>
@endsection
