@extends('layouts.admin')
@section('title', "Admin | Edit User `{$user->full_name}`")
@section('page-header', "Admin - Edit User `{$user->full_name}`")

@section('content')

    @include('users.admin.form', ['action' => route('admin.users.update', [$user->id]), 'method' => 'PATCH'])
@endsection