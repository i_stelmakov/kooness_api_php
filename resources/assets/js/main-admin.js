/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Vendors
import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;
window.jquery = jquery;
require('../../../node_modules/bootstrap/dist/js/bootstrap.min.js'); // "^3.3.7",
require('../../../node_modules/admin-lte/dist/js/adminlte.min.js'); // "^2.4.3",
require('../../../node_modules/datatables.net/js/jquery.dataTables.js'); // "^1.10.15",
require('./vendors/jquery.validate.js'); // "jquery-validation": "^1.13.0",
require('jquery-validation/dist/additional-methods'); // "jquery-validation additional method"
require('../../../node_modules/datatables.net-bs/js/dataTables.bootstrap.js'); // "^2.1.1",
require('../../../node_modules/select2/dist/js/select2.full.js'); //"^4.0.3",
require('../../../node_modules/admin-lte/plugins/iCheck/icheck.js'); // v1.0.1
require('./vendors/cropper.min.js'); //"3.0.0-rc",
require('./vendors/sweetalert.min.js'); // "^1.0.1",
require('../../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js'); // "^1.7.0",
require('../../../node_modules/bootstrap-timepicker/js/bootstrap-timepicker.js'); //  "^0.5.2",
require('../../../node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js'); // ^3.3.7",
window.CKEDITOR_BASEPATH = '/js/ckeditor/';
require('ckeditor'); // "^4.7.0"
window.waitingDialog = require('../../../node_modules/bootstrap-waitingfor/build/bootstrap-waitingfor.min.js');
require('../../../node_modules/jquery.repeater/jquery.repeater.min.js'); // "^1.2.1",
require('../../../node_modules/jquery-sortable/source/js/jquery-sortable.js'); // "^0.9.13"
require('./vendors/jquery.imageuploader.js');
require('payform');
require('payform/dist/jquery.payform');

// Modules
require('./components/generic-admin.js');
