<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworksCategoriesMediumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artworks_medium_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medium_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->timestamps();

            $table->foreign('medium_id')
                ->references('id')
                ->on('artworks_categories')
                ->onDelete('CASCADE');

            $table->foreign('category_id')
                ->references('id')
                ->on('artworks_categories')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artworks_medium_categories');
    }
}
