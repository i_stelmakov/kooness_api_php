<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMbrAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('full_name');
            $table->string('vat_number')->nullable(); // p.iva
            $table->string('fiscal_code'); // cf
            // $table->string('phone');
            $table->string('address');
            $table->integer('country')->unsigned()->nullable();
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('zip');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('country')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_addresses');
    }
}
