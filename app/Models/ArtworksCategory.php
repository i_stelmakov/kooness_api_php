<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtworksCategory extends Model
{
    protected $guarded = [];

    public function artworks(){
        return $this->belongsToMany('App\Models\Artwork', 'artworks_artworks_categories', 'category_id', 'artwork_id');
    }

    public function randomImage(){
        $artwork = $this->artworks()->whereRaw('status IN (1,2)')->orderByRaw("RAND()")->first();
        if($artwork && !$artwork->main_image){
            return $this->randomImage();
        }else{
            if(!$artwork)
                return null;
            return $artwork->main_image->url;
        }
    }

    public function medium(){
        return $this->belongsToMany('App\Models\ArtworksCategory', 'artworks_medium_categories', 'category_id', 'medium_id')->withTimestamps();
    }

    public function categories(){
        return $this->belongsToMany('App\Models\ArtworksCategory', 'artworks_medium_categories', 'medium_id', 'category_id')->withTimestamps();
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
