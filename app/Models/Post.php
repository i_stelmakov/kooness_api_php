<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    protected $appends = ['tag_ids', 'category_ids', 'artwork_ids', 'artist_ids', 'gallery_ids', 'fair_ids'];

    protected $fillable = [
        'type',
        'title',
        'slug',
        'content',
        'status',
        'created_by',
        'author',
        'date',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'posts_tags', 'post_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\PostsCategory', 'posts_posts_categories', 'post_id', 'category_id');
    }

    public function artworks()
    {
        return $this->belongsToMany('App\Models\Artwork', 'posts_artworks', 'post_id', 'artwork_id');
    }

    public function artists()
    {
        return $this->belongsToMany('App\Models\Artist', 'posts_artists', 'post_id', 'artist_id');
    }

    public function galleries()
    {
        return $this->belongsToMany('App\Models\Gallery', 'posts_galleries', 'post_id', 'gallery_id');
    }

    public function fairs()
    {
        return $this->belongsToMany('App\Models\Fair', 'posts_fairs', 'post_id', 'fair_id');
    }

    public function getTagIdsAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function getArtworkIdsAttribute()
    {
        return $this->artworks->pluck('id')->toArray();
    }

    public function getArtistIdsAttribute()
    {
        return $this->artists->pluck('id')->toArray();
    }

    public function getGalleryIdsAttribute()
    {
        return $this->galleries->pluck('id')->toArray();
    }

    public function getFairIdsAttribute()
    {
        return $this->fairs->pluck('id')->toArray();
    }

    public function getYears($section){
        return $this->selectRaw("YEAR(date) as year")->distinct()->orderBy('year','DESC')->whereType($section)->get();
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-square-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-wide-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
