<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTableAddPremiumColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_premium')->default(0)->after('status');
            $table->date('premium_exp_date')->after('is_premium')->nullable();
            $table->integer('current_plan_id')->unsigned()->nullable()->after('premium_exp_date');
            $table->boolean('is_trial')->default(1)->after('current_plan_id');
            $table->date('trial_activation_date')->after('is_trial')->nullable();
            $table->date('deactivate_premium_request_date')->after('trial_activation_date')->nullable();

            $table->foreign('current_plan_id')
                ->references('id')
                ->on('mbr_plans')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['current_plan_id']);

            $table->dropColumn('is_premium');
            $table->dropColumn('premium_exp_date');
            $table->dropColumn('is_trial');
            $table->dropColumn('current_plan_id');
            $table->dropColumn('trial_activation_date');
        });
    }
}
