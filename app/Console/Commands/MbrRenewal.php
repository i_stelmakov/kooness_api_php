<?php

namespace App\Console\Commands;

use App\Http\Controllers\Memberships\AdminController;
use App\Models\Country;
use App\Models\MbrPayment;
use App\Models\User;
use App\Services\Notification;
use Illuminate\Console\Command;
use Cartalyst\Stripe\Stripe;
use Illuminate\Support\Facades\Mail;

class mbrRenewal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:renewal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs renewal of all expiring memberships';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stripe = Stripe::make(config('services.stripe.secret'));
        $users = User::whereRaw('(NOW() > `premium_exp_date`) AND `deactivate_premium_request_date` IS NULL')->get();
        foreach ($users as $user) {
            $mbrAddress = $user->mbr_address()->first();
            $mbrCard = $user->mbr_card()->first();
            $mbr_request = null;
            if($user->have_pending_request()){
                $mbr_request = $user->pending_request()->first();
                $plan = $mbr_request->plan()->first();
            } else{
                $plan = $user->mbr_plan()->first();
            }
            $plan->setUser($user);

            $payment = new MbrPayment();
            $payment->serial = strtoupper(substr(sha1(time()), 0, 10));
            $payment->user_id = $user->id;
            $payment->plan_id = $plan->id;
            $payment->subtotal = $plan->fixed_fee;
            $gallery = $user->gallery()->first();
            $is_italian = false;
            if($gallery){
                if($gallery->country()->first()->code == 'IT'){
                    $is_italian = true;
                }
            }
            $payment->total = $plan->fixed_fee;
            if ($is_italian) {
                $payment->tax = 22;
                $payment->total = $plan->fixed_fee * 1.22;
            }
            $payment->currency = $plan->getCurrency();
            $payment->full_name = $mbrAddress->full_name;
            $payment->vat_number = $mbrAddress->vat_number;
            $payment->fiscal_code = $mbrAddress->fiscal_code;
            $payment->address = $mbrAddress->address;
            $payment->country = $mbrAddress->country;
            $payment->city = $mbrAddress->city;
            $payment->state = $mbrAddress->state;
            $payment->zip = $mbrAddress->zip;
            $payment->card_type = $mbrCard->type;
            $payment->save();

            $charge_data = [
                'source' => $mbrCard->token,
                'customer' => $user->customer_id,
                'currency' => $payment->currency,
                'amount' => $payment->total,
                'description' => "Mambership #" . $payment->serial,
            ];

            $error_message = 'generic_error';
            try{
                $charge = $stripe->charges()->create($charge_data);
            }catch (\Cartalyst\Stripe\Exception\CardErrorException $e){
                $array_error = $e->getRawOutput();
                if(isset($array_error['error']) && isset($array_error['error']['message'])){
                    $error_message = $array_error['error']['message'];
                }
                $charge = ['status' => "failed"];
            }
            if ($charge['status'] == 'succeeded') {
                $payment->status = 1;
                $payment->transaction_id = $charge['id'];
                $payment->save();
                $user->current_plan_id = $plan->id;
                if($mbr_request){
                    $mbr_request->delete();
                }
                $user->premium_exp_date = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 1, date('Y')));
                $user->save();

                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_MEMBERSHIP_SUCCESSFUL_RENEWAL;
                $extras = [
                    "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                    "plan" => $plan, //	name
                    "payment" => $payment,  // subtotal, tax, total, currency, full_name, vat_number, fiscal_code, address, country(ID riferimento tabella country), city, state, zip, card_type
                ];
                $class = new AdminController();
                if($class->generateInvoice($payment, $user, $user->gallery()->first(), $plan)) {
                    Mail::send('emails.gallerists.success-renewal', $extras, function ($message) use ($to, $name, $subject, $payment) {
                        $message->to($to, $name)->subject($subject);
                        $message->attach(storage_path("/invoices/memberships/{$payment->serial}.pdf"));
                    });
                }

            } else{
                if($mbr_request){
                    $user->current_plan_id = $plan->id;
                    $mbr_request->delete();
                }
                $user->renewal_failed_date = date('Y-m-d');
                $user->renewal_failed_payment_id = $payment->id;
                $user->save();

                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_MEMBERSHIP_UNSUCCESSFUL_RENEWAL;
                $extras = [
                    "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                    "plan" => $plan, //	name
                    "payment" => $payment,  // subtotal, tax, total, currency, full_name, vat_number, fiscal_code, address, country(ID riferimento tabella country), city, state, zip, card_type
                    "error_message" => $error_message
                ];
                Mail::send('emails.gallerists.renewal-not-successful', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });

                $users = User::permission('admin-level')->get();
                $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_FAILED_PAYMENT_MEMBERSHIP, 'membership', $user);
                Notification::addUsers($notify, $users);
            }
        }
    }
}