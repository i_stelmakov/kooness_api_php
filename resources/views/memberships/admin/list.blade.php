@extends('layouts.admin')
@section('title', 'Admin | Galleries Memberships')
@section('page-header', 'Galleries Memberships')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-body">
                    <table id="table-galleries" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Gallery Name</th>
                            <th>Membership Plan</th>
                            <th>Amount</th>
                            <th>Fee</th>
                            <th>Activation Date</th>
                            <th>Renewal</th>
                            <th>Payment</th>
                            <th>Status</th>
                            <th>Payment Status</th>
                            {{--<th>Actions</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table-galleries').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [[ 1, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.memberships.list.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "gallery_name"},
                    {"data": "membership_plan", "bSortable": false},
                    {"data": "amount", "bSortable": false},
                    {"data": "fee", "bSortable": false},
                    {"data": "activation_date"},
                    {"data": "renewal"},
                    {"data": "payment", "bSortable": false},
                    {"data": "status"},
                    {"data": "payment_status", "bSortable": false},
                    // {"data": "options", "bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                        // esempio di attivazione di attributi CSS personalizzati per una singola cella 
                        // if ( cellData < 1 ) {
                        //     $(td).css('color', 'red')
                        // }
                    }
                ]

            });
        });
    </script>
@endsection