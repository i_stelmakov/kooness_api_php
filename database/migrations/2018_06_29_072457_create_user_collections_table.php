<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });

        Schema::create('user_collection_artworks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('collection_id')->unsigned()->nullable();
            $table->integer('artwork_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('collection_id')
                ->references('id')
                ->on('user_collections')
                ->onDelete('CASCADE');

            $table->foreign('artwork_id')
                ->references('id')
                ->on('artworks')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_collections');
        Schema::dropIfExists('user_collection_artworks');
    }
}
