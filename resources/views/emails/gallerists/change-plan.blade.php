{{-- Mail - Membership Changed --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_CHANGE_PLAN}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
			Your Membership with Kooness has been changed as required.<br>
			This plan will remain active until {{ date('d/m/Y', strtotime($user->premium_exp_date)) }}
		</p>

	</div>
@stop
