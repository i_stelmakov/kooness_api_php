{{-- Mail - Order Received --}}

@extends('emails.layout')

@section('title')
	@if(!$order->token)
		{{SUBJECT_MAIL_ORDER_RECEIVED}}
	@else
		{{SUBJECT_MAIL_PAY_ORDER}}
	@endif
@endsection

@section('content')
	{{--            Hi {{ $username }}.<br>--}}
	<!-- Body -->
	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
		<tr>
			<td valign="top" id="body_content" style="background-color: #ffffff;">
				<!-- Content -->
				<table border="0" cellpadding="20" cellspacing="0" width="100%">
					<tr>
						<td valign="top" style="padding: 48px 48px 0;">
							<div id="body_content_inner" style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
								<p style="margin: 0 0 16px;">
									@if($order->incomplete_order && !$order->token)
										Thanks for your order.<br>You will receive the shipping quote for this artwork within 2 working days. If approved, you will be able to complete your payment.
									@elseif($order->token)
										Here below you can find your shipping quote to continue your purchase.
									@else
										Your order has been received.<br>Here below you can find your order data:
									@endif
								</p>
								<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 0 0 18px; text-align: left;'>
									Order #{{ $order->serial }} ({{ date('d/m/Y', strtotime($order->created_at)) }})
								</h2>
								<div style="margin-bottom: 40px;">
									<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px;" border="1">
										<thead>
										<tr>
											<th class="td" scope="col" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Image</th>
											<th class="td" scope="col" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Product</th>
											<th class="td" scope="col" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Quantity</th>
											<th class="td" scope="col" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Price</th>
										</tr>
										</thead>
										<tbody>
										@foreach($order->items()->get() as $item)
											<tr class="order_item">
												<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
													<img src="{{ url($item->artwork()->first()->main_image->url) }}" style="width: 50px; height: 50px;">
												</td>
												<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
													<span>{{ $item->artwork()->first()->title }}</span>
												</td>
												<td valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; color: #636363; padding: 12px;">
													{{ $item->quantity }}
												</td>
												<td valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; color: #636363; padding: 12px;">
													<span class="Price-amount amount">{{ number_format(($item->quantity * $item->subtotal),2,'.','') }}<span class="Price-currencySymbol">€</span></span>
												</td>
											</tr>
										@endforeach
										</tbody>
										<tfoot>
										<tr>
											<th class="td" scope="row" colspan="3" style="text-align: left; border-top-width: 4px; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
												Subtotal:
											</th>
											<td class="td" style="text-align: left; border-top-width: 4px; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
                                                <span class="Price-amount amount">{{ $order->subtotal }}<span class="Price-currencySymbol">€</span>
                                                </span>
											</td>
										</tr>
										<tr>
											<th class="td" scope="row" colspan="3" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
												Shipment:
											</th>
											<td class="td" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
                                                <span class="Price-amount amount">{{ $order->shipping }}<span class="Price-currencySymbol">€</span>
                                                </span>
											</td>
										</tr>
										@if($order->promo_code)
											<tr>
												<th class="td" scope="row" colspan="3" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
													Coupons:
												</th>
												<td class="td" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
                                                <span class="Price-amount amount">{{ number_format(-($order->subtotal * $order->promo()->first()->value) / 100, 2, '.', '') }}<span class="Price-currencySymbol">€</span>
                                                </span>
												</td>
											</tr>
										@endif
										<tr>
											<th class="td" scope="row" colspan="3" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
												Total:
											</th>
											<td class="td" style="text-align: left; color: #636363; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">
												<span class="Price-amount amount">{{ $order->amount }}<span class="Price-currencySymbol">€</span></span>
											</td>
										</tr>
										</tfoot>
									</table>
								</div>
								@if($order->token)
									<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: center;'>
										<a title="View all" style="background-color: rgb(16, 16, 16); border: none; box-sizing: border-box; color: rgb(255, 255, 255); cursor: pointer; display: inline-block; font-family: Muli, sans-serif; font-size: 20px; font-stretch: 100%; font-style: normal; font-weight: 400; height: 44px; line-height: 20px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 10px; padding-bottom: 12px; padding-left: 20px; padding-right: 20px; padding-top: 12px; text-decoration-line: none; text-decoration: none; text-decoration-style: solid; vertical-align: baseline; white-space: normal; -webkit-font-smoothing: antialiased;"
										   href="{{ route('orders.pay.order', [$order->token]) }}">Continue</a>
									</p>
								@endif
								@if(!$order->token)
									<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; text-align: left;'>
										Payment
									</h2>
									<p style="margin: 0 0 16px;">Payment by {{ get_payment_method($order->payment_method) }}.</p>
									@if( get_payment_method($order->payment_method) == 'Bank Transfer' )
										The order will be sent once the deposit has been received, you can proceed with the payment with the following data:<br><br>
										<b>Company name:</b> U-art S.r.l.<br>
										<b>Bank name:</b> Unicredit<br>
										<b>Bank branch:</b> Via Europa, 28/30 - 46043 - Castiglione Delle Stiviere (MN)<br>
										<b>CC:</b> N° 000103346275<br>
										<b>BIC/SWIFT:</b> UNCRITM1Q62<br>
										<b>IBAN:</b> IT53 U020 0857 5700 0010 3346 275<br>
										<b>Causal transfer:</b> Purchase Kara fu 17<br><br>
									@endif

									<table id="addresses" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin-bottom: 40px; padding: 0;" border="0">
										<tr>
											<td style="text-align: left; border: 0; padding: 0;" valign="top" width="50%">
												<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; text-align: left;'>
													Billing address
												</h2>
												<address class="address" style="color: #636363;">
													@if($order->billing_first_name && $order->billing_last_name)
														{{ $order->billing_first_name }} {{ $order->billing_last_name }}<br>
													@else
														{{ $order->billing_company_name }}<br>
													@endif
													@if($order->billing_fiscal_code)
														{{ $order->billing_fiscal_code }}<br>
													@endif
													@if($order->billing_vat_number)
														{{ $order->billing_vat_number }}<br>
													@endif													{{ $order->billing_address }}<br>
													{{ $order->billing_city }} {{ ($order->billing_state) ? " (".$order->billing_state.")" : '' }}, {{ $order->billing_zip }}<br>
													{{ $order->billing_country }}
													({{ $order->billing_country_code }})<br>
													{{ $order->billing_phone }}
													<p style="margin: 0 0 16px;">{{ $order->billing_email }}</p>
												</address>
											</td>
											<td style="text-align: left; padding: 0;" valign="top" width="50%">
												<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; text-align: left;'>
													Shipping address
												</h2>
												<address class="address" style="color: #636363;">
													{{ $order->shipping_first_name }} {{ $order->shipping_last_name }}<br>
													{{ $order->shipping_address }}<br>
													{{ $order->shipping_city }} {{ ($order->shipping_state) ? " (".$order->shipping_state.")" : '' }}, {{ $order->shipping_zip }}<br>
													{{ $order->shipping_country }}
													({{ $order->shipping_country_code }})<br>
													{{ $order->shipping_phone }}
													<p style="margin: 0 0 16px;">{{ $order->shipping_email }}</p>
												</address>
											</td>
										</tr>
									</table>
								@endif
							</div>
						</td>
					</tr>
				</table>
				<!-- End Content -->
			</td>
		</tr>
	</table>
	<!-- End Body -->
@stop