<?php

namespace App\Console\Commands;

use App\Models\Artwork;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class populateCurrentLocationArtworks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:artowrks-location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $artworks = Artwork::all();
        foreach ($artworks as $artwork) {
            $gallery = $artwork->gallery()->first();
            if($gallery){
                if($gallery->city)
                    $artwork->current_city = $gallery->city;
                else
                    Log::channel('location')->error("{$gallery->name} has not city");
                if($gallery->zip)
                    $artwork->current_zip = $gallery->zip;
                else
                    Log::channel('location')->error("{$gallery->name} has not zip code");
                if($gallery->country)
                    $artwork->current_country = $gallery->country_id;
                else
                    Log::channel('location')->error("{$gallery->name} has not country");
                $artwork->save();
            }else{
                Log::channel('location')->error("{$artwork->title} has not gallery");
            }
        }
    }
}
