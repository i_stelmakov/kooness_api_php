<?php

namespace App\Console\Commands;

use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\ArtworksImage;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\Page;
use App\Models\Post;
use App\Models\PromoCode;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Queue\Connectors\DatabaseConnector;
use Illuminate\Support\Facades\DB;

class convertDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:db {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert old db';

    /**
     * Old DB Connection
     *
     * @var DB::connection
     */
    protected $old_db = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->old_db = DB::connection('old');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('type') == 'artworks') {
            $this->transferArtworks();
        } else if ($this->argument('type') == 'artists') {
            $this->transferArtists();
        } else if ($this->argument('type') == 'galleries') {
            $this->transferGalleries();
        } else if ($this->argument('type') == 'fairs') {
            $this->transferFairs();
        } else if ($this->argument('type') == 'posts') {
            $this->transferPost();
        } else if ($this->argument('type') == 'other'){
            $this->transferOther();
        } else if ($this->argument('type') == 'tags'){
            $this->transferTags();
        } else if ($this->argument('type') == 'associations'){
            $this->transferAssociations();
        }
    }

    public function transferGalleries(){
        $galleries = $this->old_db->table('galleries')->get();
        foreach ($galleries as $gallery) {

            /**
             *  SEARCH GALLERY USER
             */
            $user_id = null;
            $user = $this->old_db->table('users')->whereRaw("profile_id = '{$gallery->id}' AND role = 31")->first();
            if (!$user) {
                $user = $this->old_db->table('users')->whereRaw("profile_id = '{$gallery->id}' AND role IS NULL")->first();
            }
            if ($user) {
                /**
                 *  IF USER EXIST CREATE IT
                 */

                $new_user = new User();
                $new_user->username = $user->username;
                $new_user->password = $user->password;
                $new_user->email = $user->email;
                $new_user->first_name = $user->first_name;
                $new_user->last_name = $user->last_name;
                $new_user->url_cover = $user->url_cover;
                $new_user->url_avatar = $user->url_avatar;
                $new_user->status = $user->status;
                $new_user->created_at = $user->created_at;
                $new_user->updated_at = $user->updated_at;

                if ($new_user->save()) {
                    $new_user->assignRole('gallerist');
                    $user_id = $new_user->id;
                }
            }
            /**
             *  CREATE GALLERY ENTITY
             */
            $new_gallery = new Gallery();
            $new_gallery->name = $gallery->name;
            $new_gallery->slug = trim($gallery->slug);
            $new_gallery->about_gallery = $gallery->bio;
            $new_gallery->country_id = $gallery->country_id;
            $new_gallery->city = $gallery->city;
            $new_gallery->address = $gallery->address;
            $new_gallery->zip = $gallery->zip;
            $new_gallery->url_cover = $gallery->url_cover;
            $new_gallery->url_cover = $gallery->url_cover;
            $new_gallery->featured = $gallery->featured;
            $new_gallery->status = $gallery->status;
            $new_gallery->user_id = $user_id;
            $new_gallery->created_at = $gallery->created_at;
            $new_gallery->updated_at = $gallery->updated_at;
            $new_gallery->meta_title = $gallery->meta_title;
            $new_gallery->meta_keywords = $gallery->meta_keywords;
            $new_gallery->meta_description = $gallery->meta_description;

            $new_gallery->save();

        }
    }

    public function transferArtists(){
        $artists = $this->old_db->table('artists')->get();
        foreach ($artists as $artist) {
            $user_id = null;
            $user = $this->old_db->table('users')->whereRaw("profile_id = '{$artist->id}' AND role = 21")->first();
            if (!$user) {
                $user = $this->old_db->table('users')->whereRaw("profile_id = '{$artist->id}' AND role IS NULL")->first();
            }
            if ($user) {
                $new_user = new User();
                $new_user->username = $user->username;
                $new_user->password = $user->password;
                $new_user->email = $user->email;
                $new_user->first_name = $user->first_name;
                $new_user->last_name = $user->last_name;
                $new_user->url_cover = $user->url_cover;
                $new_user->url_avatar = $user->url_avatar;
                $new_user->status = $user->status;
                $new_user->created_at = $user->created_at;
                $new_user->updated_at = $user->updated_at;

                if ($new_user->save()) {
                    $new_user->assignRole('artist');
                    $user_id = $new_user->id;
                }
            }
            /**
             *  CREATE ARTIST ENTITY
             */
            $new_artists = new Artist();
            $new_artists->first_name = $artist->first_name;
            $new_artists->last_name = $artist->last_name;
            $new_artists->slug = trim($artist->slug);
            $new_artists->url_picture = $artist->url_thumb;
            $new_artists->url_cover = $artist->url_cover;
            $new_artists->about_the_artist = $artist->bio;
            $new_artists->awards = $artist->prizes;
            if ($artist->birth_date && $artist->birth_date != '0000-00-00')
                $new_artists->date_of_birth = date('Y', strtotime($artist->birth_date));
            if ($artist->death_date && $artist->death_date != '0000-00-00')
                $new_artists->date_of_birth = date('Y', strtotime($artist->death_date));
            $new_artists->country_of_birth = $artist->country_id;
            $new_artists->featured = $artist->featured;
            $new_artists->status = $artist->status;
            $new_artists->user_id = $user_id;
            $new_artists->created_at = $artist->created_at;
            $new_artists->updated_at = $artist->updated_at;
            $new_artists->meta_title = $artist->meta_title;
            $new_artists->meta_keywords = $artist->meta_keywords;
            $new_artists->meta_description = $artist->meta_description;

            $new_artists->save();
        }
    }

    public function transferArtworks(){
        $sku = 1;
        $artworks = $this->old_db->table('artworks')->get();
        foreach ($artworks as $artwork) {
            $artist = $this->old_db->table('artists')->whereRaw("id = '{$artwork->artist_id}'")->first();
            $new_artist_id = null;
            if($artist){
                $new_artist = Artist::whereSlug($artist->slug)->first();
                if($new_artist){
                    $new_artist_id = $new_artist->id;
                }
            }
            $gallery = $this->old_db->table('galleries')->whereRaw("id = '{$artwork->gallery_id}'")->first();
            $new_gallery_id = null;
            if($gallery){
                $new_gallery = Gallery::whereSlug($gallery->slug)->first();
                if($new_gallery){
                    $new_gallery_id = $new_gallery->id;
                }
            }
            if($new_artist_id && $new_gallery_id){
                $new_artwork = new Artwork();
                $new_artwork->artist_id = $new_artist_id;
                $new_artwork->gallery_id = $new_gallery_id;
                $new_artwork->reference = str_pad($sku, 8, "0", STR_PAD_LEFT);
                $new_artwork->title = $artwork->title;
                $new_artwork->slug = trim($artwork->slug);
                if($artwork->release_year && $artwork->release_year != '-' && strlen(trim($artwork->release_year)) == 4)
                    $new_artwork->year = $artwork->release_year;
                else
                    $new_artwork->year = '0000';
                $new_artwork->width = $artwork->width / 10;
                $new_artwork->height = $artwork->height / 10;
                $new_artwork->depth = $artwork->depth / 10;
                $new_artwork->price = $artwork->price;
                $new_artwork->available_in_fair = $artwork->in_fair;
                $new_artwork->about_the_work = $artwork->description;
                $new_artwork->news = $artwork->prizes;
                if ($artwork->status == 1) {
                    $new_artwork->status = 1;
                }
                if ($artwork->status == 12) {
                    $new_artwork->status = 2;
                }
                if ($artwork->status == -2) {
                    $new_artwork->status = 3;
                }
                if ($artwork->status == -1) {
                    $new_artwork->status = 4;
                }
                $new_artwork->meta_title = $artwork->meta_title;
                $new_artwork->meta_keywords = $artwork->meta_keywords;
                $new_artwork->meta_description = $artwork->meta_description;
                $new_artwork->created_at = $artwork->created_at;
                $new_artwork->updated_at = $artwork->updated_at;

                if ($new_artwork->save()) {
                    $sku++;
                    $image = new ArtworksImage();
                    $image->artwork_id = $new_artwork->id;;
                    $image->url = $artwork->url_picture;
                    $image->label = 1;
                    $image->save();
                }
            }
        }
    }

    public function transferFairs(){
        $fairs = $this->old_db->table('fairs')->get();
        foreach ($fairs as $fair){
            $fair_edition = $this->old_db->table('fair_editions')->whereRaw("fair_id = '{$fair->id}'")->first();
            if($fair_edition){
                $new_fair = new Fair();
                $new_fair->name = $fair->name;
                $new_fair->slug = trim($fair_edition->slug);
                $new_fair->description = $fair->bio;
                $new_fair->url_picture = $fair->url_thumb;
                $new_fair->url_cover = $fair->url_cover;
                $new_fair->country_id = $fair->country_id;
                $new_fair->city = $fair->city;
                $new_fair->address = $fair->address;
                $new_fair->zip = $fair->zip;
                $new_fair->start_date = $fair_edition->from_date;
                $new_fair->end_date = $fair_edition->to_date;
                $new_fair->tickets = $fair_edition->tickets;
                $new_fair->map = $fair_edition->gmap;
                $new_fair->status = $fair_edition->status;

                $new_fair->meta_title = $fair_edition->meta_title;
                $new_fair->meta_keywords = $fair_edition->meta_keywords;
                $new_fair->meta_description = $fair_edition->meta_description;

                $new_fair->created_at = $fair_edition->created_at;
                $new_fair->updated_at = $fair_edition->updated_at;

                $new_fair->save();
            }
        }
    }

    public function transferPost(){
        $posts = $this->old_db->table('posts')->get();
        foreach ($posts as $post) {
            $new_post = new Post();
            $new_post->title = $post->title;
            $new_post->type = 'magazine';
            $new_post->slug = trim($post->slug);
            $new_post->content = $post->content;
            $new_post->url_picture = $post->url_picture;
            $new_post->url_cover = $post->url_cover;
            $new_post->status = $post->status;
            $new_post->author = $post->author;
            $new_post->date = $post->date;

            $new_post->meta_title = $post->meta_title;
            $new_post->meta_keywords = $post->meta_keywords;
            $new_post->meta_description = $post->meta_description;

            $new_post->created_at = $post->created_at;
            $new_post->updated_at = $post->updated_at;

            $new_post->save();
        }
    }

    public function transferTags(){
        $tags = $this->old_db->table('entity_tag')->get();
        foreach ($tags as $tag){
            $tag_row =  $this->old_db->table('tags')->whereRaw("id = '{$tag->tag_id}'")->first();
            if($tag_row){
                if($tag->artwork_id){
                    $artwork =  $this->old_db->table('artworks')->whereRaw("id = '{$tag->artwork_id}'")->first();
                    if($artwork){
                        $new_artwork = Artwork::whereSlug($artwork->slug)->first();
                        if($new_artwork){
                            if($tag_row->type == 1 || $tag_row->type == 2){
                                $category = ArtworksCategory::whereSlug(str_slug($tag_row->name))->first();
                                if($category){
                                    if(!$new_artwork->categories->contains($category->id)){
                                        $new_artwork->categories()->attach($category->id);
                                    }
                                } else{
                                    $category = new ArtworksCategory();
                                    $category->name = $tag_row->name;
                                    $category->slug = str_slug($tag_row->name);
                                    $category->is_medium = ($tag_row->type == 1) ? 1 : 0;
                                    $category->meta_title = $tag_row->title;
                                    $category->meta_keywords = $tag_row->keywords;
                                    $category->meta_description = $tag_row->description;
                                    if ($category->save()) {
                                        if(!$new_artwork->categories->contains($category->id)){
                                            $new_artwork->categories()->attach($category->id);
                                        }
                                    }
                                }
                            } else if($tag_row->type == 4){
                                $new_tag = Tag::whereSlug(str_slug($tag_row->name))->first();
                                if($new_tag){
                                    if(!$new_artwork->tags->contains($new_tag->id)){
                                        $new_artwork->tags()->attach($new_tag->id);
                                    }
                                } else{
                                    $new_tag = new Tag();
                                    $new_tag->name = $tag_row->name;
                                    $new_tag->slug = str_slug($tag_row->name);
                                    $new_tag->meta_title = $tag_row->title;
                                    $new_tag->meta_keywords = $tag_row->keywords;
                                    $new_tag->meta_description = $tag_row->description;
                                    if ($new_tag->save()) {
                                        if(!$new_artwork->tags->contains($new_tag->id)){
                                            $new_artwork->tags()->attach($new_tag->id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if($tag->gallery_id){
                    $gallery = $this->old_db->table('galleries')->whereRaw("id = '{$tag->gallery_id}'")->first();
                    if($gallery){
                        $new_gallery = Gallery::whereSlug($gallery->slug)->first();
                        if($new_gallery){
                            if($tag_row->type == 2){
                                $category = GalleriesCategory::whereSlug(str_slug($tag_row->name))->first();
                                if($category){
                                    if(!$new_gallery->categories->contains($category->id)){
                                        $new_gallery->categories()->attach($category->id);
                                    }
                                } else{
                                    $category = new GalleriesCategory();
                                    $category->name = $tag_row->name;
                                    $category->slug = str_slug($tag_row->name);
                                    $category->meta_title = $tag_row->title;
                                    $category->meta_keywords = $tag_row->keywords;
                                    $category->meta_description = $tag_row->description;
                                    if ($category->save()) {
                                        if(!$new_gallery->categories->contains($category->id)){
                                            $new_gallery->categories()->attach($category->id);
                                        }
                                    }
                                }
                            } else if($tag_row->type == 4){
                                $new_tag = Tag::whereSlug(str_slug($tag_row->name))->first();
                                if($new_tag){
                                    if(!$new_gallery->tags->contains($new_tag->id)){
                                        $new_gallery->tags()->attach($new_tag->id);
                                    }
                                } else{
                                    $new_tag = new Tag();
                                    $new_tag->name = $tag_row->name;
                                    $new_tag->slug = str_slug($tag_row->name);
                                    $new_tag->meta_title = $tag_row->title;
                                    $new_tag->meta_keywords = $tag_row->keywords;
                                    $new_tag->meta_description = $tag_row->description;
                                    if ($new_tag->save()) {
                                        if(!$new_gallery->tags->contains($new_tag->id)){
                                            $new_gallery->tags()->attach($new_tag->id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if($tag->artist_id){
                    $artist = $this->old_db->table('artists')->whereRaw("id = '{$tag->artist_id}'")->first();
                    if($artist){
                        $new_artist = Artist::whereSlug($artist->slug)->first();
                        if($new_artist){
                            if($tag_row->type == 2){
                                $category = ArtistsCategory::whereSlug(str_slug($tag_row->name))->first();
                                if($category){
                                    if(!$new_artist->categories->contains($category->id)){
                                        $new_artist->categories()->attach($category->id);
                                    }
                                } else{
                                    $category = new ArtistsCategory();
                                    $category->name = $tag_row->name;
                                    $category->slug = str_slug($tag_row->name);
                                    $category->meta_title = $tag_row->title;
                                    $category->meta_keywords = $tag_row->keywords;
                                    $category->meta_description = $tag_row->description;
                                    if ($category->save()) {
                                        if(!$new_artist->categories->contains($category->id)){
                                            $new_artist->categories()->attach($category->id);
                                        }
                                    }
                                }
                            } else if($tag_row->type == 4){
                                $new_tag = Tag::whereSlug(str_slug($tag_row->name))->first();
                                if($new_tag){
                                    if(!$new_artist->tags->contains($new_tag->id)){
                                        $new_artist->tags()->attach($new_tag->id);
                                    }
                                } else{
                                    $new_tag = new Tag();
                                    $new_tag->name = $tag_row->name;
                                    $new_tag->slug = str_slug($tag_row->name);
                                    $new_tag->meta_title = $tag_row->title;
                                    $new_tag->meta_keywords = $tag_row->keywords;
                                    $new_tag->meta_description = $tag_row->description;
                                    if ($new_tag->save()) {
                                        if(!$new_artist->tags->contains($new_tag->id)){
                                            $new_artist->tags()->attach($new_tag->id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if($tag->post_id){
                    $post =  $this->old_db->table('posts')->whereRaw("id = '{$tag->post_id}'")->first();
                    if($post){
                        $new_post = Post::whereSlug($post->slug)->first();
                        if($new_post){
                            if($tag_row->type == 4){
                                $new_tag = Tag::whereSlug(str_slug($tag_row->name))->first();
                                if($new_tag){
                                    if(!$new_post->tags->contains($new_tag->id)){
                                        $new_post->tags()->attach($new_tag->id);
                                    }
                                } else{
                                    $new_tag = new Tag();
                                    $new_tag->name = $tag_row->name;
                                    $new_tag->slug = str_slug($tag_row->name);
                                    $new_tag->meta_title = $tag_row->title;
                                    $new_tag->meta_keywords = $tag_row->keywords;
                                    $new_tag->meta_description = $tag_row->description;
                                    if ($new_tag->save()) {
                                        if(!$new_post->tags->contains($new_tag->id)){
                                            $new_post->tags()->attach($new_tag->id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function transferOther(){
        $pages = $this->old_db->table('pages')->get();
        foreach ($pages as $page){
            $page_translation = $this->old_db->table('page_translations')->whereRaw("page_id = '{$page->id}' AND language = 'it'")->first();
            if($page_translation){
                $new_page = new Page();
                $new_page->title = $page_translation->title;
                $new_page->slug = trim($page->slug);
                $new_page->content = $page_translation->content;
                $new_page->created_at = date('Y-m-d H:i:s');
                $new_page->updated_at = date('Y-m-d H:i:s');
                $new_page->save();
            }
        }

        $promo_codes = $this->old_db->table('promo_codes')->get();
        foreach ($promo_codes as $code){
            $new_code = new PromoCode();
            $new_code->name = $code->name;
            $new_code->value = $code->percentage;
            $new_code->status = $code->status;
            $new_code->created_at = date('Y-m-d H:i:s');
            $new_code->updated_at = date('Y-m-d H:i:s');
            $new_code->save();
        }

        $users = $this->old_db->table('users')->whereRaw("profile_id IS NULL")->get();
        foreach ($users as $user){
            if(!$user->email)
                continue;
            $new_user = new User();
            $new_user->username = $user->username;
            $new_user->password = $user->password;
            $new_user->email = $user->email;
            $new_user->first_name = $user->first_name;
            $new_user->last_name = $user->last_name;
            $new_user->url_cover = $user->url_cover;
            $new_user->url_avatar = $user->url_avatar;
            $new_user->status = $user->status;
            $new_user->created_at = $user->created_at;
            $new_user->updated_at = $user->updated_at;

            if ($new_user->save()) {
                if($user->role == 91 || $user->role == 92)
                    $new_user->assignRole('admin');
                else if ($user->role == 14)
                    $new_user->assignRole('editor');
                else
                    $new_user->assignRole('user');
            }
        }
    }

    public function transferAssociations(){
        $artist_gallery = $this->old_db->table('artist_gallery')->get();
        foreach ($artist_gallery as $item) {
            $gallery = $this->old_db->table('galleries')->whereRaw("id = '{$item->gallery_id}'")->first();
            $artist = $this->old_db->table('artists')->whereRaw("id = '{$item->artist_id}'")->first();
            if($gallery && $artist){
                $new_gallery = Gallery::whereSlug(trim($gallery->slug))->first();
                $new_artist = Artist::whereSlug(trim($artist->slug))->first();
                if($new_artist && $new_gallery){
                    if(!$new_gallery->artists->contains($new_artist->id)){
                        $new_gallery->artists()->attach($new_artist->id);
                    }
                }
            }
        }

        $fair_edition_gallery =  $this->old_db->table('fair_edition_gallery')->get();
        foreach ($fair_edition_gallery as $item){
            $fair_editions = $this->old_db->table('fair_editions')->whereRaw("id = '{$item->fair_edition_id}'")->first();
            $gallery = $this->old_db->table('galleries')->whereRaw("id = '{$item->gallery_id}'")->first();
            if($fair_editions && $gallery){
                $new_fair = Fair::whereSlug(trim($fair_editions->slug))->first();
                $new_gallery = Gallery::whereSlug(trim($gallery->slug))->first();
                if($new_fair && $new_gallery){
                    if(!$new_fair->galleries->contains($new_gallery->id)){
                        $new_fair->galleries()->attach($new_gallery->id);
                    }
                }
            }
        }

        $fair_edition_artworks =  $this->old_db->table('fair_edition_artwork')->get();
        foreach ($fair_edition_artworks as $item){
            $fair_editions = $this->old_db->table('fair_editions')->whereRaw("id = '{$item->fair_edition_id}'")->first();
            $artwork = $this->old_db->table('artworks')->whereRaw("id = '{$item->artwork_id}'")->first();
            if($fair_editions && $artwork){
                $new_fair = Fair::whereSlug(trim($fair_editions->slug))->first();
                $new_artwork = Artwork::whereSlug(trim($artwork->slug))->first();
                if($new_fair && $new_artwork){
                    if(!$new_fair->artworks->contains($new_artwork->id)){
                        $new_fair->artworks()->attach($new_artwork->id);
                    }
                }
            }
        }
    }
}
