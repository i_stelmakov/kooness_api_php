<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablesForApi4dem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('api4dem_referer_id')->after('customer_id')->nullable();
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->integer('api4dem_referer_id')->after('user_id')->nullable();
        });

        Schema::table('cart_lines', function (Blueprint $table) {
            $table->integer('api4dem_referer_id')->after('cart_id')->nullable();
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->integer('api4dem_referer_id')->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('api4dem_referer_id');
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn('api4dem_referer_id');
        });

        Schema::table('cart_lines', function (Blueprint $table) {
            $table->dropColumn('api4dem_referer_id');
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->dropColumn('api4dem_referer_id');
        });

    }
}
