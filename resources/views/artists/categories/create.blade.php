@extends('layouts.admin')
@section('title', 'Admin | Create Artist Category')
@section('page-header', 'Admin - Create Category')

@section('content')
    @include('artists.categories.form', ['action' => route('admin.categories.artists.store'), 'method' => 'POST'])
@endsection