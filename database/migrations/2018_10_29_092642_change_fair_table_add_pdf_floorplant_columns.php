<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFairTableAddPdfFloorplantColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fairs', function (Blueprint $table) {
            $table->text('floorplant_pdf_url')->nullable()->after('url_cover');
            $table->text('floorplant_preview_url')->nullable()->after('floorplant_pdf_url');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fairs', function (Blueprint $table) {
            $table->dropColumn('floorplant_pdf_url');
            $table->dropColumn('floorplant_preview_url');
        });
    }
}