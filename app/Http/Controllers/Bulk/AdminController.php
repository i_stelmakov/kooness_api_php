<?php

namespace App\Http\Controllers\Bulk;

use App\Http\Controllers\Controller;

use App\Models\GenericSeoDatum;
use App\Models\Redirect301;
use App\Models\SiteMap;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Collections\SheetCollection;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Artwork;


class AdminController extends Controller
{

    protected $pathToUpload = 'uploads';

    protected $extension = '.xls';

    protected $column_validator_pages = [
        "type",
        "url"
    ];

    protected $column_validator_redirects = [
        "url",
        "redirect_to"
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function uploadFile(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'file' => 'required',
        ]);

        $fileName = substr(sha1(uniqid()), 0, 8);

        if (Input::file('file')->isValid()) {
            if (!is_dir('../storage/' . $this->pathToUpload . "/" . $fileName)) {
                mkdir('../storage/' . $this->pathToUpload . "/" . $fileName, 0777);
            }

            $extension = Input::file('file')->getClientOriginalExtension();
            Input::file('file')->move('../storage/' . $this->pathToUpload . "/" . $fileName, $fileName . '.' . $extension);

            return Response::json(['file' => $fileName]);
        }
    }

    public function verifyXls($xls_name, $type)
    {
        $json_array = ["result" => "failed"];

        if (!$xls_name) {
            $this->rrmdir('../storage/' . $this->pathToUpload . '/' . $xls_name);
            return Response::json($json_array);
        }

        $results = Excel::load('../storage/' . $this->pathToUpload . '/' . $xls_name . "/" . $xls_name . $this->extension, function ($reader) {
            $r = $reader->get();
        });


        $results = $results->get();


        if ($results instanceof SheetCollection) {
            $results = $results->first();
        }

        if (count($results->toArray()) <= 0)
            return Response::json($json_array);


        $array_keys = array_keys($results->first()->toArray());

        $columns = $this->column_validator_pages;
        if ($type == 'redirects') {
            $columns = $this->column_validator_redirects;
        }
        foreach ($columns as $column) {
            if (!in_array(strtolower($column), $array_keys, true)) {
                $this->rrmdir('../storage/' . $this->pathToUpload . '/' . $xls_name);
                return Response::json($json_array);
            }
        }
        $json_array['result'] = "success";
        $json_array['file'] = $xls_name;

        $info_path = '../storage/' . $this->pathToUpload . '/' . $xls_name . "/" . "info";
        file_put_contents($info_path, json_encode(["lines" => count($results->toArray()), "elaborated" => 0]));

        return Response::json($json_array);
    }

    private function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function processXls($xls_name, $type)
    {

        $json_array = ["result" => "failed"];

        if (!$xls_name) {
            $this->rrmdir('../storage/' . $this->pathToUpload . '/' . $xls_name);
            return Response::json($json_array);
        }
        $uses["xls_name"] = $xls_name;

        $results = Excel::load('../storage/' . $this->pathToUpload . '/' . $xls_name . "/" . $xls_name . $this->extension, function ($reader) {
            $r = $reader->get();
        });

        $results = $results->get();

        if ($results instanceof SheetCollection) {
            $results = $results->first();
        }

        if ($type == 'redirects') {
            Redirect301::query()->truncate();
        }

        $count = 2;
        $results->each(function ($row) use ($uses, $count, $type) {
            if ($type == 'pages') {
                $error = [];
                if (class_exists("\App\Models\\" . $row->type)) {
                    $parse = parse_url($row->url, PHP_URL_PATH);
                    $url = null;
                    if ($parse) {
                        $url = $parse;
                    } else {
                        $error[] = "Incorrect url formatting in excel at row {$count}";
                    }
                    $map = SiteMap::whereType($row->type)->whereUrl($url)->first();
                    if ($map) {
                        if ($map->type != 'GenericSeoDatum') {
                            $class_name = "\App\Models\\" . $map->type;
                            $class = new $class_name;
                            $record = $class::find($map->referer_id);
                        } else {
                            $class = new GenericSeoDatum();
                            $record = $class::whereUrl($map->url)->first();
                            if (!$record) {
                                $record = new GenericSeoDatum();
                                $record->url = $map->url;
                            }
                        }
                        if ($record) {
                            if (isset($row->page_title) && trim($row->page_title) != '') {
                                $record->meta_title = trim($row->page_title);
                            }
                            if (isset($row->meta_description) && trim($row->meta_description) != '') {
                                $record->meta_description = trim($row->meta_description);
                            }
                            if (isset($row->meta_keywords) && trim($row->meta_keywords) != '') {
                                $record->meta_keywords = trim($row->meta_keywords);
                            }
                            if (isset($row->h1) && trim($row->h1) != '') {
                                $record->h1 = trim($row->h1);
                            }
                            if (isset($row->canonical_url) && trim($row->canonical_url) != '') {
                                $record->canonical = trim($row->canonical_url);
                            }
                            $record->save();
                        } else {
                            $error[] = "No type referer found at row {$count}";
                        }
                    } else {
                        $error[] = "Url no match in excel at row {$count}";
                    }
                } else {
                    $error[] = "Class Type not found in excel at row {$count}";
                }
            } else {
                $path = parse_url($row->url, PHP_URL_PATH);
                $to_path = parse_url($row->redirect_to, PHP_URL_PATH);
                if ($path && $to_path)
                    Redirect301::create([
                        'old_url' => $path,
                        'new_url' => $to_path,
                        'redirect_type' => 301,
                    ]);
            }
            $info = file_get_contents('../storage/' . $this->pathToUpload . '/' . $uses['xls_name'] . "/" . "info");
            $json = json_decode($info);
            $json->elaborated = intval($json->elaborated) + 1;
            file_put_contents('../storage/' . $this->pathToUpload . '/' . $uses['xls_name'] . "/" . "info", json_encode($json));
        });

        $this->rrmdir('../storage/' . $this->pathToUpload . '/' . $xls_name);
        $json_array['result'] = 'success';

        return Response::json($json_array);
    }

    public function getStateProcessXls($xls_name)
    {
        $json_array = ["result" => "failed"];

        if (!$xls_name) {
            return Response::json($json_array);
        }

        if (is_file('../storage/' . $this->pathToUpload . '/' . $xls_name . "/" . "info")) {
            $info = file_get_contents('../storage/' . $this->pathToUpload . '/' . $xls_name . "/" . "info");
            $json = json_decode($info);

            $json_array['result'] = 'success';
            $progress = round((intval($json->elaborated) * 100) / intval($json->lines), 2);
            $json_array['progress'] = (($progress > 99.8) ? 100 : $progress);

        } else {
            $json_array['result'] = 'success';
            $json_array['progress'] = '100%';
        }


        return Response::json($json_array);
    }

    public function downloadXls($type)
    {
        // se è tipo pagina
        if ($type == 'pages') {
            $urls = SiteMap::all();
            return Excel::create('pages_seo_data', function ($excel) use ($urls) {
                $excel->sheet('mySheet', function ($sheet) use ($urls) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('type');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('url');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('page_title');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('meta_description');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('meta_keywords');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('h1');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('canonical_url');
                    });

                    $i = 2;
                    foreach ($urls as $key => $value) {
                        if ($value->type != 'GenericSeoDatum') {
                            $class_name = "\App\Models\\" . $value->type;
                            $class = new $class_name;
                            $row = $class::find($value->referer_id);
                        } else {
                            $class = new GenericSeoDatum();
                            $row = $class::whereUrl($value->url)->first();
                        }

                        $sheet->cell('A' . $i, $value->type);
                        $sheet->cell('B' . $i, url('') . $value->url);

                        if ($row) {
                            $sheet->cell('C' . $i, $row->meta_title);
                            $sheet->cell('D' . $i, $row->meta_description);
                            $sheet->cell('E' . $i, $row->meta_keywords);
                            $sheet->cell('F' . $i, $row->h1);
                            $sheet->cell('G' . $i, $row->canonical);
                            $i++;
                        } else {
                            $sheet->cell('C' . $i, '');
                            $sheet->cell('D' . $i, '');
                            $sheet->cell('E' . $i, '');
                            $sheet->cell('F' . $i, '');
                            $sheet->cell('G' . $i, '');
                            $i++;
                        }
                    }
                });
            })->download('xls');
        }
        // se è tipo redirects
        elseif ($type == 'redirects') {
            $redirects = Redirect301::all();
            return Excel::create('redirects_seo_data', function ($excel) use ($redirects) {
                $excel->sheet('mySheet', function ($sheet) use ($redirects) {
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('url');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('redirect_to');
                    });
                    $i = 2;
                    foreach ($redirects as $key => $value) {
                        $sheet->cell('A' . $i, url('') . $value->old_url);
                        $sheet->cell('B' . $i, url('') . $value->new_url);
                        $i++;
                    }
                });
            })->download('xls');
        }
        // se è tipo googleshop
        elseif ($type == 'googleshop') {
            $artworks = Artwork::whereRaw('artworks.status IN (1,2)')->with([
                'artist',
                'categories' => function ($query) {
                    $query->whereIsMedium(0);
//                }])->whereReference('507235e8')->get();
//                }])->offset('5')->limit('5')->get();
        }])->get();
//                }])->get();
            return Excel::create('googleshop_product_list', function ($excel) use ($artworks) {
                $excel->sheet('mySheet', function ($sheet) use ($artworks) {
                    $sheet->setColumnFormat(array(
                        'A' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                    ));
                    $sheet->cell('A1', function ($cell) {
                        $cell->setValue('id');
                    });
                    $sheet->cell('B1', function ($cell) {
                        $cell->setValue('title');
                    });
                    $sheet->cell('C1', function ($cell) {
                        $cell->setValue('description');
                    });
                    $sheet->cell('D1', function ($cell) {
                        $cell->setValue('google_​product_​category');
                    });
                    $sheet->cell('E1', function ($cell) {
                        $cell->setValue('product_type');
                    });
                    $sheet->cell('F1', function ($cell) {
                        $cell->setValue('link');
                    });
                    $sheet->cell('G1', function ($cell) {
                        $cell->setValue('image_link');
                    });
                    $sheet->cell('H1', function ($cell) {
                        $cell->setValue('availability');
                    });
                    $sheet->cell('I1', function ($cell) {
                        $cell->setValue('price');
                    });
                    $sheet->cell('J1', function ($cell) {
                        $cell->setValue('brand');
                    });
                    $sheet->cell('K1', function ($cell) {
                        $cell->setValue('size');
                    });
                    $sheet->cell('L1', function ($cell) {
                        $cell->setValue('identifier_exists');
                    });
                    $sheet->cell('M1', function ($cell) {
                        $cell->setValue('custom_​label_​0');
                    });
                    $sheet->cell('N1', function ($cell) {
                        $cell->setValue('custom_​label_​1');
                    });
                    $sheet->cell('O1', function ($cell) {
                        $cell->setValue('shipping');
                    });
                    $i = 2;
                    foreach ($artworks as $key => $artwork) {
                        $clearDescription = strip_tags($artwork->about_the_work);
                        $sheet->cell('A' . $i, function ($cell) use ($artwork){
                            $cell->setValue($artwork->reference.' ');
                        });
                        $sheet->cell('B' . $i, $artwork->artist->full_name . ' - ' . $artwork->title . ' - ' . $artwork->year);
                        $sheet->cell('C' . $i, $clearDescription);
//                        $sheet->cell('D' . $i, 'Home & Garden > Decor > Artwork > Posters, Prints, & Visual Artwork');
                        $sheet->cell('D' . $i, '500044');
                        $sheet->cell('E' . $i, join(', ',$artwork->medium->pluck('name')->toArray()) );
                        $sheet->cell('F' . $i, route('artworks.single', [$artwork->slug]) );
//                        $sheet->cell('F' . $i, 'https://qa.kooness.com/artworks/'.$artwork->slug);
                        $sheet->cell('G' . $i, $artwork->main_image->url);
                        $sheet->cell('H' . $i, ($artwork->status == 1) ? 'in stock' : 'out of stock' );
                        $sheet->cell('I' . $i, number_format($artwork->price, 2, '.','') . ' EUR');
                        $sheet->cell('J' . $i, $artwork->artist->full_name);
                        $sheet->cell('K' . $i, $artwork->measure_inc . ' in');
                        $sheet->cell('L' . $i, 'no');
                        $sheet->cell('M' . $i, $artwork->gallery_name);
                        $sheet->cell('N' . $i, $artwork->year);
                        $sheet->cell('O' . $i, 'IT:::0 EUR');
                        $i++;
                    }
                });
            })->download('csv');
        }
    }

    public function bulkUpload()
    {
        $type = 'pages';
        return view('bulk.bulk-upload', compact('type'));
    }

    public function bulkGoogleShop()
    {
        $type = 'googleshop';
        $artworks = Artwork::whereRaw('artworks.status IN (1,2)')->with([
            'artist',
            'categories' => function ($query) {
                $query->whereIsMedium(0);
            },
//            'medium' => function ($query) {
//                $query->whereIsMedium(1);
//            }
        ])->take(2)->get();

        return view('bulk.google-shop', compact('type', 'artworks'));
    }


}
