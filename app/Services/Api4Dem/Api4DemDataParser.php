<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 09/10/18
 * Time: 16:02
 */

namespace App\Services\Api4Dem;


use App\Models\Artwork;
use App\Models\User;
use App\Services\Cart;
use Hassansin\DBCart\Models\CartLine;
use Illuminate\Support\Facades\Auth;

class Api4DemDataParser
{

    private $arg = [];

    /*control parameters*/

    function __construct()
    {

    }

    // User Parse Add / Edit
    function parseUserSave(User $user)
    {
        $this->arg = array(
            "user_id" => $user->id,
            "email_address" => $user->email,
            "subscription" => array( //optional
                "ip" => $_SERVER['REMOTE_ADDR'], //if subscription exists is required
                "status" => 'Subscribed',
                "date" => date("Y-m-d H:i:s") //optional
            ),
            "triggers" => array(
                "automation" => true,
                "behaviors" => true
            ),
            "update_if_duplicate" => true,
            "custom_fields" => [
                [
                    "id" => env('4DEM_CUSTOM_FIELD_ECOMMERCE_ID_FIRST_NAME'),
                    "value" => $user->first_name
                ],
                [
                    "id" => env('4DEM_CUSTOM_FIELD_ECOMMERCE_ID_LAST_NAME'),
                    "value" => $user->last_name
                ],
                [
                    "id" => env('4DEM_CUSTOM_FIELD_ECOMMERCE_ID_USERNAME'),
                    "value" => $user->username
                ]
            ]
        );
    }

    // User Parse Subscription (Newsletter)
    function parseUserSubscribe(User $user)
    {
        $this->arg = array(
            "email_address" => $user->email,
            "subscription" => array( //optional
                "ip" => $_SERVER['REMOTE_ADDR'], //if subscription exists is required
                "status" => 'Subscribed',
                "date" => date("Y-m-d H:i:s") //optional
            ),
            "triggers" => array(
                "automation" => true,
                "behaviors" => true
            ),
            "update_if_duplicate" => true,
            "custom_fields" => [
                [
                    "id" => env('4DEM_CUSTOM_FIELD_NESWLETTER_ID_FIRST_NAME'),
                    "value" => $user->first_name
                ],
                [
                    "id" => env('4DEM_CUSTOM_FIELD_NESWLETTER_ID_LAST_NAME'),
                    "value" => $user->last_name
                ]
            ]
        );

    }

    // User Parse Delete
    function parseUserUnsubscribe(User $user)
    {
        $this->arg = array(
            "subscriber_id" => $user->api4dem_referer_id,
            "ip" => $_SERVER['REMOTE_ADDR'] //if subscription exists is required
        );
    }

    // Artwork Parse Add / Edit
    public function parseArtworkSave(Artwork $artwork)
    {
        $categories = $artwork->categories()->whereIsMedium(0)->pluck('name')->toArray();
        $medium = $artwork->categories()->whereIsMedium(1)->pluck('name')->toArray();
        $this->arg = [
            'row_id' => $artwork->id,
            'product_id' => $artwork->reference,
            'title' => $artwork->title,
            'category' => join(', ', $categories),
            'status' => 'enabled',
            'category_med' => join(', ', $medium),
            'product_url' => url(route('artworks.single', [$artwork->slug])),
            'product_image_url' => $artwork->main_image->url,
            'brand' => $artwork->artist()->first()->full_name,
            'price' => $artwork->price
        ];
    }


    // Cart Create Parse
    public function parseCartCreate(Cart $cart, User $user)
    {
        $cart_items = $cart->items()->get(); // get cart lines

        $this->arg =
            [
                'cart_id' => $cart->id,
                'subscriber_id' => $user->api4dem_referer_id,
                'shop_user_id' => 0,
                'status' => 'open',
                'total' => ($cart->total_price > 0) ? $cart->total_price : 0,
                'opened_at' => date("Y-m-d H:i:s", strtotime($cart->created_at)),
                "triggers" => [
                    "automation" => true
                ],
                'products' => []
            ];
        foreach ($cart_items as $cart_item) {
            $product = $cart_item->product()->first();
            $this->arg['products'][] = [
                'line_id' => $cart_item->id,
                'product_id' => $product->reference,
                'price' => $product->price,
                'quantity' => 1
            ];
        }
    }


    // Cart Artwork Add Parse
    public function parseCartArtworkAdd(CartLine $cartLine)
    {
        $product = $cartLine->product()->first();

        $this->arg =
            [
                'line_id' => $cartLine->id,
                'product_id' => $product->reference,
                'price' => $product->price,
                'quantity' => 1
            ];
    }


    // Cart Close Parse
    public function parseCartClose(Cart $cart)
    {

        $this->arg =
            [
                'cart_id' => $cart->id,
                'status' => 'close',
                'closed_at' => date('Y-m-d H:i:s'),
                'products' => [],
                'total' => ($cart->total_price > 0) ? $cart->total_price : 0,
            ];

        foreach ($cart->items()->get() as $cart_item) {
            $product = $cart_item->product()->first();
            $this->arg['products'][] = [
                'line_id' => $cart_item->id,
                'product_id' => $product->reference,
                'price' => $product->price,
                'quantity' => 1
            ];
        }
    }


    // Cart Update Total Parse
    public function parseCartTotalUpdate(Cart $cart)
    {

        $this->arg =
            [
                'cart_id' => $cart->id,
                'total' => ($cart->total_price > 0) ? $cart->total_price : 0,
                'products' => []
            ];

        foreach ($cart->items()->get() as $cart_item) {
            $product = $cart_item->product()->first();
            $this->arg['products'][] = [
                'line_id' => $cart_item->id,
                'product_id' => $product->reference,
                'price' => $product->price,
                'quantity' => 1
            ];
        }
    }


    /**
     * @return array
     */
    public function getArg(): array
    {
        return $this->arg;
    }

    /**
     * @param array $arg
     */
    public function setArg(array $arg): void
    {
        $this->arg = $arg;
    }

    private function getIpAddress()
    {


    }
}