{{-- Mail - User Registered from Admin / Gallerist --}}
@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px 30px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
		Hi {{ $username }}.<br>
		{{ $user_kind_with_article }} has registered you on Kooness to allow you to independently manage your content.<br>
		You can now log into Kooness, here below your credentials:
	</p>
	<div style="border: 1px solid #CCCCCC; padding: 5px; display: inline-block;">
		<span style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>Username: <strong>{{ $username }}</strong></span><br>
		<span style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>Password: <strong>{{ $password }}</strong></span>
	</div>
	<p style='margin: 16px 30px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
		You can freely change the password after the first access from your user panel.
		<br>
		Enjoy!<br>
	</p>
@stop