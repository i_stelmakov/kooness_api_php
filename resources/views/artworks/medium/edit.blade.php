@extends('layouts.admin')
@section('title', "Admin | Edit Artwork Media `{$category->name}`")
@section('page-header', "Admin - Edit Artwork Media `{$category->name}`")

@section('content')
    @include('artworks.medium.form', ['action' => route('admin.medium.artworks.update', [$category->id]), 'method' => 'PATCH'])
@endsection