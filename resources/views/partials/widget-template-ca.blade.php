<section id="event-slider" class="event-slider widget-box default-slider-section slider-border">
    <div class="container boxed-container">
        <div class="default-slider-header">
            <span class="h1">{{ $data['title'] }}</span>
            <p>
                <a title="{{ $data['link_title'] }}" class="read-more" href="{{ $data['link'] }}">{{ $data['link_title'] }}</a>
            </p>
        </div>
        <div class="container col-container-with-offset widget-slick-deactivated">
            @foreach($data['items'] as $key=>$item)
                <div class="slider-item one-third-col-with-margin">
                    <i title="Remove"  class="fa fa-times delete-item">
                    </i>
                    <div class="slider-item-img wide-box">
                        <a href="{{ route('posts.single', ['magazine',$item->slug]) }}">
                            <div class="background-cover gallery lazy" data-src="{{ $item->url_wide_box }}"></div>
                        </a>
                    </div>
                    <div class="slider-item-txt">
                        <h2>
                            <a title="{{ $item->title }}" class="dark-text" href="{{ route('posts.single', ['magazine',$item->slug]) }}">{{ $item->title }}</a>
                        </h2>
                        @if(count($item->categories()->pluck('name')->toArray()))
                            <div class="event-date">
                                <p class="event-date-p">{{ join(',',$item->categories()->pluck('name')->toArray()) }}</p>
                            </div>
                        @endif
                        <a title="Read more" class="read-more" href="{{ route('posts.single', ['magazine',$item->slug]) }}">Read more</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>