<?php

namespace App\Http\Controllers\SeoData;

use App\Http\Controllers\Controller;
use App\Models\GenericSeoDatum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('seo-data.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'url',
            1 => 'meta_title',
            2 => 'created_at',
            3 => 'options',
        );

        $totalData = GenericSeoDatum::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $seo_data = GenericSeoDatum::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $seo_data = GenericSeoDatum::where('url', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = GenericSeoDatum::where('url', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($seo_data)) {
            foreach ($seo_data as $datum) {
                $edit = route('admin.seo-data.edit', $datum->id);
                $delete = route('admin.seo-data.destroy', $datum->id);

                $nestedData['id'] = $datum->id;
                $nestedData['url'] = url($datum->url);
                $nestedData['meta_title'] = $datum->meta_title;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($datum->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $data = new GenericSeoDatum();
        return view('seo-data.admin.create', compact('data'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $inputs = Input::all();
            $inputs['no_index'] = isset($inputs['no_index']) ? true : false;
            $inputs['no_follow'] = isset($inputs['no_follow']) ? true : false;
            $data = GenericSeoDatum::create($inputs);
            if ($data) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.seo-data.index')
                ];
                $request->session()->flash("success", "Seo Data for url `{$data->url}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($data)
    {
        $data = GenericSeoDatum::find($data);
        return view('seo-data.admin.edit', compact('data'));
    }

    public function update(Request $request, $data)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $data = GenericSeoDatum::find($data);
        if ($request->method() == 'PATCH' && $data) {
            $inputs = Input::all();
            $inputs['no_index'] = isset($inputs['no_index']) ? true : false;
            $inputs['no_follow'] = isset($inputs['no_follow']) ? true : false;
            if ($data->update($inputs)) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.seo-data.index')
                ];
                $request->session()->flash("success", "Seo Data for url `{$data->url}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public function destroy($data)
    {
        $data = GenericSeoDatum::find($data);
        $data->delete();
        // redirect
        Session::flash('success', "Seo Data for url `{$data->url}` is successful removed!");
        return redirect(route('admin.seo-data.index'));
    }
}
