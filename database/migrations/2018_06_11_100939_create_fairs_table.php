<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->string('url_picture')->nullable();
            $table->string('url_cover')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('city');
            $table->string('address');
            $table->string('zip');
            $table->string('phone');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('times')->nullable();
            $table->text('tickets')->nullable();
            $table->text('map')->nullable();
            $table->string('contact_name')->nullable();
            $table->integer('contact_country_id')->unsigned()->nullable();
            $table->string('contact_city')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('contact_zip')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_website')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');

            $table->foreign('contact_country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fairs');
    }
}
