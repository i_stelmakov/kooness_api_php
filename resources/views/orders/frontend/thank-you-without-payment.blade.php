@extends('layouts.app')

@section('title')
    Thank you
@endsection

@section('body-class')
    thank-you
@endsection

@section('content')

    <div class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <i class="fa fa-check main-color-txt thanks-icon"></i><h1 class="thanks-title">Thank You</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-vertical-padding-bottom">
            <div class="container boxed-container">
                
                <div class="default-sheet-row">
                    <p>Your request has been submitted and your shipping quote is being calculated.</p>
                </div>
                <a class="black-button" href="{{ route('home') }}">Back to home</a>
            </div>
        </section>
    </div>
@endsection


