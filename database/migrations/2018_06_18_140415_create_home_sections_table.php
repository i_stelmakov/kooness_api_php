<?php

use App\Models\HomeSection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pointer');
            $table->text('content');
            $table->timestamps();
        });

        HomeSection::create([
            "pointer" => "block1",
            "content" => "{}"
        ]);
        HomeSection::create([
            "pointer" => "block2",
            "content" => "{}"
        ]);
        HomeSection::create([
            "pointer" => "block3",
            "content" => "{}"
        ]);
        HomeSection::create([
            "pointer" => "block4",
            "content" => "{}"
        ]);
        HomeSection::create([
            "pointer" => "block5",
            "content" => "{}"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_sections');
    }
}
