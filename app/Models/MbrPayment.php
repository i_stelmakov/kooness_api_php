<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MbrPayment extends Model
{
    protected $guarded = [];

    public function plan()
    {
        return $this->hasOne('App\Models\MbrPlan', 'id', 'plan_id');
    }

    public function getSymbolCurrencyAttribute(){
        if($this->currency == 'EUR') {
            return '€';
        }
        elseif($this->currency == 'USD') {
            return '$';
        }
        elseif($this->currency == 'GBP') {
            return '£';
        }
    }
}
