@extends('layouts.admin')@section('title', 'Management Intro')@section('page-header', "Intro `". ucfirst($section) ."`")

@section('content')
    @if($section != 'artists' && $section != 'artworks')
        <form class="ajax-form" role="form" method="POST" action="{{ route('admin.main.page.save', [$section]) }}">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Slider Widget
                </div>
                <div class="box-body">
                    <div class="row home-section">
                        @for($i = 1; $i <= 5; $i++ )
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="contents">Slide {{ $i }}</label>
                                    <select class="select2 form-control" name="contents[]">
                                        <option value="">None</option>
                                        @foreach($data as $row)
                                            <option value="{{ $row->id }}" {{ isset($contents[$i -1]) && $row->id == $contents[$i -1]? 'selected="selected"' : '' }}>{{ $row->text }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif

    <form class="ajax-form" role="form" method="POST" action="{{ route('admin.main.page.save', [$section]) }}">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Home page Widgets
            </div>
            <div class="box-body widget-repeater">
                @csrf
                <input type="hidden" name="label" value="{{ $section }}">
                <ul data-repeater-list="widget" class="widget-repeater-container">
                    @if(count($widgets))
                        @foreach($widgets as $widget)
                            @include('home.admin.partials.widget', ['widget' => $widget])
                        @endforeach
                    @else
                        @include('home.admin.partials.widget', ['widget' => null])
                    @endif
                </ul>
                <input data-repeater-create type="button" value="Add"/>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-footer">
                <div class="col-md-12">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection