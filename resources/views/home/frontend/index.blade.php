@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Home')

@section('body-class', 'home')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')


<div class="sections">

    {{--Rapid Login in home - Mobile Only--}}
    <section id="in-page-login" class="mobile-only">

        <div class="mobile-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <h1>Kooness: the marketplace of your Art</h1>
                </div>
            </div>
        </div>

        <div class="slick-carousel">
            {{--Slider Item 1 --}}
            <div class="slide-item">
                <!-- Box 1 !-->
                @if(isset($blocks['block1']))
                    <div id="home-box-1" class="background-cover home-col-row home-box" style="background-image: url('{{ $blocks['block1']['bg'] }}')">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block1']['title'] }}</span>
                            <a title="{{ $blocks['block1']['view_all_label_2'] }}" class="white-button" href="{{ $blocks['block1']['view_all'] }}">
                                {{ $blocks['block1']['view_all_label_2'] }}
                            </a>
                            <span class="home-box-caption"> {{ $blocks['block1']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
            {{--Slider Item 2 --}}
            <div class="slide-item">
                <!-- Box 2 !-->
                @if(isset($blocks['block2']))
                    <div id="home-box-2" class="background-cover home-col-row-cell home-box" style="background-image: url('{{ $blocks['block2']['bg'] }}')">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block2']['title'] }}</span>
                            <a title="{{ $blocks['block2']['view_all_label_2'] }}" class="white-button" href="{{ $blocks['block2']['view_all'] }}">
                                {{ $blocks['block2']['view_all_label_2'] }}
                            </a>
                            <span class="home-box-caption"> {{ $blocks['block2']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
            {{--Slider Item 3 --}}
            <div class="slide-item">
                <!-- Box 3 !-->
                @if(isset($blocks['block3']))
                    <div id="home-box-3" class="background-cover home-col-row-cell home-box" style="background-image: url('{{ $blocks['block3']['bg'] }}')">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block3']['title'] }}</span>
                            <a title="{{ $blocks['block3']['view_all_label_2'] }}" class="white-button" href="{{ $blocks['block3']['view_all'] }}">
                                {{ $blocks['block3']['view_all_label_2'] }}
                            </a>
                            <span class="home-box-caption"> {{ $blocks['block3']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
            {{--Slider Item 4 --}}
            <div class="slide-item">
                <!-- Box 4 !-->
                @if(isset($blocks['block4']))
                    <div id="home-box-4" class="background-cover home-box" style="background-image: url('{{ $blocks['block4']['bg'] }}')">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block4']['title'] }}</span>
                            <a title="{{ $blocks['block4']['view_all_label_2'] }}" class="white-button" href="{{ $blocks['block4']['view_all'] }}">
                                {{ $blocks['block4']['view_all_label_2'] }}
                            </a>
                            <span class="home-box-caption"> {{ $blocks['block4']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
            {{--Slider Item 5 --}}
            <div class="slide-item">
                <!-- Box 5 !-->
                @if(isset($blocks['block5']))
                    <div id="home-box-5" class="background-cover home-box" style="background-image: url('{{ $blocks['block5']['bg'] }}')">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block5']['title'] }}</span>
                            <a title="{{ $blocks['block5']['view_all_label_2'] }}" class="white-button" href="{{ $blocks['block5']['view_all'] }}">
                                {{ $blocks['block5']['view_all_label_2'] }}
                            </a>
                            <span class="home-box-caption"> {{ $blocks['block5']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        {{-- Mobile header login --}}
        @if(!Auth::user())
            <div class="mobile-header-login container boxed-container">

                <div class="col-container-with-offset">
                    <h1>Enter Kooness world and live art at 360</h1>
                </div>

                <div class="col-container-with-offset">
                    <form method="POST" action="{{ route('login') }}" class="col-container-with-offset-and-margin k-form">
                        @csrf
                        <div class="full-col-with-margin form-privacy social-button facebook">
                            <p>
                                <a href="{{ route('auth.social', 'facebook') }}"><i class="fa fa-facebook"></i>Login with Facebook</a>
                            </p>
                        </div>
                        <div class="full-col-with-margin form-privacy social-button twitter">
                            <p>
                                <a href="{{ route('auth.social', 'twitter') }}"><i class="fa fa-twitter"></i>Login with Twitter</a>
                            </p>
                        </div>
                        <input id="username_login" placeholder="Username" type="text" class="full-col-with-margin {{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>
                        <input id="password_login" placeholder="Password" type="password" class="full-col-with-margin {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <div class="full-col-with-margin">
                            <p>
                                <a title="Forgot your password" href="{{ route('password.request') }}">Forgot your password?</a>
                            </p>
                        </div>
                        <div class="full-col-with-margin">
                            <input class="default-input-button" type="submit" value="Login">
                            <a href="{{ route('register') }}" class="default-input-button">Register</a>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    </section>
    {{--Hidden Page title for Google--}}
    <section id="page-header" class="no-display">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <h1>Home Page</h1>
            </div>
        </div>
    </section>
    <section id="home-first-viewport">
        <div id="home-container" class="no-mobile">
            <div id="home-col-1" class="home-col">
                <!-- Box 1 !-->
                @if(isset($blocks['block1']))
                    <div id="home-box-1" class="background-cover home-col-row home-box lazy" data-src="{{ $blocks['block1']['bg'] }}">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block1']['title'] }}</span>
                            <div class="home-box-menu">
                                @if(count($blocks['block1']['links']))
                                    <ul>
                                        @foreach($blocks['block1']['links'] as $link)
                                            <li>
                                                <a title="{{ $link['name'] }}" href="{{ $link['link'] }}">{{ $link['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <a title="{{ $blocks['block1']['view_all_label'] }}" class="white-button" href="{{ $blocks['block1']['view_all'] }}">{{ $blocks['block1']['view_all_label'] }}</a>
                            <span class="home-box-caption"> {{ $blocks['block1']['caption'] }}</span>
                        </div>
                    </div>
                @endif
                <div class="home-col-row">
                    <!-- Box 2 !-->
                    @if(isset($blocks['block2']))
                        <div id="home-box-2" class="background-cover home-col-row-cell home-box lazy" data-src="{{ $blocks['block2']['bg'] }}">
                            <div class="home-box-overlay">
                                <span class="h1 home-box-title">{{ $blocks['block2']['title'] }}</span>
                                <div class="home-box-menu">
                                    @if(count($blocks['block2']['links']))
                                        <ul>
                                            @foreach($blocks['block2']['links'] as $link)
                                                <li>
                                                    <a title="{{ $link['name'] }}" href="{{ $link['link'] }}">{{ $link['name'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                                <a title="{{ $blocks['block2']['view_all_label'] }}" class="white-button" href="{{ $blocks['block2']['view_all'] }}">{{ $blocks['block2']['view_all_label'] }}</a>
                                <span class="home-box-caption"> {{ $blocks['block2']['caption'] }}</span>
                            </div>
                        </div>
                    @endif
                    <!-- Box 3 !-->
                    @if(isset($blocks['block3']))
                        <div id="home-box-3" class="background-cover home-col-row-cell home-box lazy" data-src="{{ $blocks['block3']['bg'] }}">
                            <div class="home-box-overlay">
                                <span class="h1 home-box-title">{{ $blocks['block3']['title'] }}</span>
                                <div class="home-box-menu">
                                    @if(count($blocks['block3']['links']))
                                        <ul>
                                            @foreach($blocks['block3']['links'] as $link)
                                                <li>
                                                    <a title="{{ $link['name'] }}" href="{{ $link['link'] }}">{{ $link['name'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                                <a title="{{ $blocks['block3']['view_all_label'] }}" class="white-button" href="{{ $blocks['block3']['view_all'] }}">{{ $blocks['block3']['view_all_label'] }}</a>
                                <span class="home-box-caption"> {{ $blocks['block3']['caption'] }}</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div id="home-col-2" class="home-col">
                <!-- Box 4 !-->
                @if(isset($blocks['block4']))
                    <div id="home-box-4" class="background-cover home-box lazy" data-src="{{ $blocks['block4']['bg'] }}">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block4']['title'] }}</span>
                            <div class="home-box-menu">
                                @if(count($blocks['block4']['links']))
                                    <ul>
                                        @foreach($blocks['block4']['links'] as $link)
                                            <li>
                                                <a title="{{ $link['name'] }}" href="{{ $link['link'] }}">{{ $link['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <a title="{{ $blocks['block4']['view_all_label'] }}" class="white-button" href="{{ $blocks['block4']['view_all'] }}">{{ $blocks['block4']['view_all_label'] }}</a>
                            <span class="home-box-caption"> {{ $blocks['block4']['caption'] }}</span>
                        </div>
                    </div>
                @endif
                <!-- Box 5 !-->
                @if(isset($blocks['block5']))
                    <div id="home-box-5" class="background-cover home-box lazy" data-src="{{ $blocks['block5']['bg'] }}">
                        <div class="home-box-overlay">
                            <span class="h1 home-box-title">{{ $blocks['block5']['title'] }}</span>
                            <div class="home-box-menu">
                                @if(count($blocks['block5']['links']))
                                    <ul>
                                        @foreach($blocks['block5']['links'] as $link)
                                            <li>
                                                <a title="{{ $link['name'] }}" href="{{ $link['link'] }}">{{ $link['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                            <a title="{{ $blocks['block5']['view_all_label'] }}" class="white-button" href="{{ $blocks['block5']['view_all'] }}">{{ $blocks['block5']['view_all_label'] }}</a>
                            <span class="home-box-caption"> {{ $blocks['block5']['caption'] }}</span>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    @foreach($widgets as $widget)
        @if($widget['template'] == 'tplA')
            @include('partials.widget-template-a', ['data' => $widget])
        @endif
        @if($widget['template'] == 'tplB' || $widget['template'] == 'tplBf')
            @include('partials.widget-template-b', ['data' => $widget])
        @endif
        @if($widget['template'] == 'tplCa')
            @include('partials.widget-template-ca', ['data' => $widget])
        @endif
        @if($widget['template'] == 'tplC')
            @include('partials.widget-template-c', ['data' => $widget])
        @endif
    @endforeach
</div>
@endsection