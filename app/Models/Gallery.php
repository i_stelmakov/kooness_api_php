<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];

    protected $appends = ['tag_ids', 'category_ids', 'fair_ids', 'email', 'country'];

    protected $fillable = [
        'name',
        'slug',
        'description',
        'about_gallery',
        'meet_gallerist',
        'country_id',
        'city',
        'address',
        'zip',
        'country_id_2',
        'city_2',
        'address_2',
        'zip_2',
        'country_id_3',
        'city_3',
        'address_3',
        'zip_3',
        'url_full_image',
        'url_square_box',
        'url_wide_box',
        'url_vertical_box',
        'featured',
        'status',
        'user_id',
        'created_by',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'galleries_tags', 'gallery_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\GalleriesCategory', 'galleries_galleries_categories', 'gallery_id', 'category_id');
    }

    public function fairs()
    {
        return $this->belongsToMany('App\Models\Fair', 'fairs_galleries', 'gallery_id', 'fair_id');
    }

    public function exhibitions()
    {
        return $this->hasMany('App\Models\Exhibition', 'gallery_id', 'id');
    }

    public function artworks()
    {
        return $this->hasMany('App\Models\Artwork', 'gallery_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function artists()
    {
        return $this->belongsToMany('App\Models\Artist', 'artists_galleries', 'gallery_id', 'artist_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function country_2()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id_2');
    }

    public function country_3()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id_3');
    }

    public function news()
    {
        return $this->belongsToMany('App\Models\Post', 'posts_galleries', 'gallery_id', 'post_id');
    }

    public function getTagIdsAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function getFairIdsAttribute()
    {
        return $this->fairs->pluck('id')->toArray();
    }


    public function getEmailAttribute()
    {
        if ($this->user()->count())
            return $this->user()->first()->email;
    }

    public function getCountryAttribute()
    {
        return $this->country()->first()->name;
    }

    public function getCountry2Attribute()
    {
        return $this->country()->first()->name;
    }

    public function getCountry3Attribute()
    {
        return $this->country()->first()->name;
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-square-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-wide-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
