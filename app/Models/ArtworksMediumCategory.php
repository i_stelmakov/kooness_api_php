<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtworksMediumCategory extends Model
{

    protected $guarded = [];

    public function medium(){
        return $this->hasOne('App\Models\ArtworksCategory', 'id', 'medium_id');
    }

    public function category(){
        return $this->hasOne('App\Models\ArtworksCategory', 'id', 'category_id');
    }
}
