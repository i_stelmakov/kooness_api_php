@extends('layouts.admin')
@if(Request::is('admin/magazine*'))
    @section('title', "Magazine Category {$category->name}`")
    @section('page-header', "Admin - Edit Magazine Category `{$category->name}`")
@else
    @section('title', "News Category {$category->name}`")
@section('page-header', "Admin - Edit News Category `{$category->name}`")
@endif

@section('content')
    @include('posts.categories.form', ['action' => (Request::is('admin/magazine*')) ? route('admin.categories.magazine.update', [$category->id]) : route('admin.categories.news.update', [$category->id]), 'method' => 'PATCH'])
@endsection