@extends('layouts.app')

@section('title')
    Thank you
@endsection

@section('body-class')
    thank-you
@endsection

@section('seo-customs-scripts')
    @if($envKind == 'prod')
        <script>
            window.addEventListener('load', function() {
                // Purchase (track event on page load)
                fbq('track', 'Purchase', {
                    contents: [
                        @foreach ($order->items()->get() as $orderItem)
                            {
                                'id': '{{$orderItem->artwork()->first()->reference}}',
                                'quantity': {{$orderItem->quantity}},
                                'item_price': {{$orderItem->subtotal}}
                            },
                        @endforeach
                    ],
                    content_type: 'product',
                    value: {{$order->amount}},
                    currency: 'EUR'
                });
                console.log('Facebook Pixel Purchase triggered!');
            });
        </script>
    @endif
@endsection

@section('content')

    <div class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <i class="fa fa-check main-color-txt thanks-icon"></i><h1 class="thanks-title">Thank You</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-vertical-padding-bottom">
            <div class="container boxed-container">
                
                <div class="default-sheet-row">
                    <p>Your order ID is <a href="/users/profile#orders">#{{ strtoupper($order->serial) }}</a></p>
                </div>
                <div class="default-sheet-row">
                    <p>An email receipt containing information about your order will soon follow. Please keep it for records.</p>
                </div>
                <div class="default-sheet-row">
                    <p>Contact us at <a href="mailto:info@kooness.it" title="lorel ipsum">info@kooness.it</a></p>
                </div>
                <div class="default-sheet-row">
                    <h3>Thank you for purchase at <a href="{{ route('home') }}">Kooness</a></h3>
                </div>
                <a class="black-button" href="{{ route('home') }}">Back to home</a>
            </div>
        </section>
    </div>
@endsection


