<?php

return [
    /*
     * This is the class responsible for providing the URLs which must be redirected.
     * The only requirement for the redirector is that it needs to implement the
     * `Spatie\MissingPageRedirector\Redirector\Redirector`-interface
     */
    'redirector' => \App\Services\ConfigurationRedirector::class,

    /*
     * By default the package will only redirect 404s. If you want to redirect on other
     * response codes, just add them to the array. Leave the array empty to redirect
     * always no matter what the response code.
     */
    'redirect_status_codes' => [
        \Symfony\Component\HttpFoundation\Response::HTTP_CONTINUE,
        \Symfony\Component\HttpFoundation\Response::HTTP_SWITCHING_PROTOCOLS,
        \Symfony\Component\HttpFoundation\Response::HTTP_PROCESSING,
        \Symfony\Component\HttpFoundation\Response::HTTP_EARLY_HINTS,
        \Symfony\Component\HttpFoundation\Response::HTTP_OK,
        \Symfony\Component\HttpFoundation\Response::HTTP_CREATED,
        \Symfony\Component\HttpFoundation\Response::HTTP_ACCEPTED,
        \Symfony\Component\HttpFoundation\Response::HTTP_NON_AUTHORITATIVE_INFORMATION,
        \Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT,
        \Symfony\Component\HttpFoundation\Response::HTTP_RESET_CONTENT,
        \Symfony\Component\HttpFoundation\Response::HTTP_PARTIAL_CONTENT,
        \Symfony\Component\HttpFoundation\Response::HTTP_MULTI_STATUS,
        \Symfony\Component\HttpFoundation\Response::HTTP_ALREADY_REPORTED,
        \Symfony\Component\HttpFoundation\Response::HTTP_IM_USED,
        \Symfony\Component\HttpFoundation\Response::HTTP_MULTIPLE_CHOICES,
        \Symfony\Component\HttpFoundation\Response::HTTP_MOVED_PERMANENTLY,
        \Symfony\Component\HttpFoundation\Response::HTTP_FOUND,
        \Symfony\Component\HttpFoundation\Response::HTTP_SEE_OTHER,
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_MODIFIED,
        \Symfony\Component\HttpFoundation\Response::HTTP_USE_PROXY,
        \Symfony\Component\HttpFoundation\Response::HTTP_RESERVED,
        \Symfony\Component\HttpFoundation\Response::HTTP_TEMPORARY_REDIRECT,
        \Symfony\Component\HttpFoundation\Response::HTTP_PERMANENTLY_REDIRECT,
        \Symfony\Component\HttpFoundation\Response::HTTP_BAD_REQUEST,
        \Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED,
        \Symfony\Component\HttpFoundation\Response::HTTP_PAYMENT_REQUIRED,
        \Symfony\Component\HttpFoundation\Response::HTTP_FORBIDDEN,
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND,
        \Symfony\Component\HttpFoundation\Response::HTTP_METHOD_NOT_ALLOWED,
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_ACCEPTABLE,
        \Symfony\Component\HttpFoundation\Response::HTTP_PROXY_AUTHENTICATION_REQUIRED,
        \Symfony\Component\HttpFoundation\Response::HTTP_REQUEST_TIMEOUT,
        \Symfony\Component\HttpFoundation\Response::HTTP_CONFLICT,
        \Symfony\Component\HttpFoundation\Response::HTTP_GONE,
        \Symfony\Component\HttpFoundation\Response::HTTP_LENGTH_REQUIRED,
        \Symfony\Component\HttpFoundation\Response::HTTP_PRECONDITION_FAILED,
        \Symfony\Component\HttpFoundation\Response::HTTP_REQUEST_ENTITY_TOO_LARGE,
        \Symfony\Component\HttpFoundation\Response::HTTP_REQUEST_URI_TOO_LONG,
        \Symfony\Component\HttpFoundation\Response::HTTP_UNSUPPORTED_MEDIA_TYPE,
        \Symfony\Component\HttpFoundation\Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE,
        \Symfony\Component\HttpFoundation\Response::HTTP_EXPECTATION_FAILED,
        \Symfony\Component\HttpFoundation\Response::HTTP_I_AM_A_TEAPOT,
        \Symfony\Component\HttpFoundation\Response::HTTP_MISDIRECTED_REQUEST,
        \Symfony\Component\HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY,
        \Symfony\Component\HttpFoundation\Response::HTTP_LOCKED,
        \Symfony\Component\HttpFoundation\Response::HTTP_FAILED_DEPENDENCY,
        \Symfony\Component\HttpFoundation\Response::HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL,
        \Symfony\Component\HttpFoundation\Response::HTTP_UPGRADE_REQUIRED,
        \Symfony\Component\HttpFoundation\Response::HTTP_PRECONDITION_REQUIRED,
        \Symfony\Component\HttpFoundation\Response::HTTP_TOO_MANY_REQUESTS,
        \Symfony\Component\HttpFoundation\Response::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE,
        \Symfony\Component\HttpFoundation\Response::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS,
        \Symfony\Component\HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR,
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_IMPLEMENTED,
        \Symfony\Component\HttpFoundation\Response::HTTP_BAD_GATEWAY,
        \Symfony\Component\HttpFoundation\Response::HTTP_SERVICE_UNAVAILABLE,
        \Symfony\Component\HttpFoundation\Response::HTTP_GATEWAY_TIMEOUT,
        \Symfony\Component\HttpFoundation\Response::HTTP_VERSION_NOT_SUPPORTED,
        \Symfony\Component\HttpFoundation\Response::HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL,
        \Symfony\Component\HttpFoundation\Response::HTTP_INSUFFICIENT_STORAGE,
        \Symfony\Component\HttpFoundation\Response::HTTP_LOOP_DETECTED,
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_EXTENDED,
        \Symfony\Component\HttpFoundation\Response::HTTP_NETWORK_AUTHENTICATION_REQUIRED,
    ],

    /*
     * When using the `ConfigurationRedirector` you can specify the redirects in this array.
     * You can use Laravel's route parameters here.
     */
    'redirects' => [
//        '/non-existing-page' => '/existing-page',
//        '/old-blog/{url}' => '/new-blog/{url}',
    ],

];
