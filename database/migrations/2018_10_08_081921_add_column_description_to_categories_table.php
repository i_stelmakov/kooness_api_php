<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDescriptionToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists_categories', function (Blueprint $table) {
            $table->text('description')->after('url_cover')->nullable();
        });

        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->text('description')->after('order')->nullable();
        });

        Schema::table('galleries_categories', function (Blueprint $table) {
            $table->text('description')->after('url_cover')->nullable();
        });

        Schema::table('posts_categories', function (Blueprint $table) {
            $table->text('description')->after('url_cover')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists_categories', function (Blueprint $table) {
            $table->dropColumn('description');
        });

        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->dropColumn('description');
        });

        Schema::table('galleries_categories', function (Blueprint $table) {
            $table->dropColumn('description');
        });

        Schema::table('posts_categories', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
}
