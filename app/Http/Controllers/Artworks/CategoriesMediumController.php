<?php

namespace App\Http\Controllers\Artworks;

use App\Http\Controllers\Controller;
use App\Models\ArtworksCategory;
use App\Models\ArtworksMediumCategory;
use App\Models\SiteMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use View;

class CategoriesMediumController extends Controller
{

    private $_type_url = 'ArtworksMediumCategory';

    private $_url_prefix = '/artworks/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('artworks.medium-categories.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'medium',
            2 => 'category',
            3 => 'created_at',
            4 => 'options',
        );

        $totalData = ArtworksMediumCategory::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $relations = ArtworksMediumCategory::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $relations = DB::table('artworks_medium_categories as amc')
                ->select('amc.*')
                ->join('artworks_categories as ac1', 'amc.category_id','=','ac1.id')
                ->join('artworks_categories as ac2', 'amc.medium_id','=','ac2.id')
                ->where('ac1.name', 'LIKE',"%{$search}%")
                ->orWhere('ac2.name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $relations = ArtworksMediumCategory::hydrate($relations->toArray());

            $totalFiltered = DB::table('artworks_medium_categories as amc')
                ->select('amc.*')
                ->join('artworks_categories as ac1', 'amc.category_id','=','ac1.id')
                ->join('artworks_categories as ac2', 'amc.medium_id','=','ac2.id')
                ->where('ac1.name', 'LIKE',"%{$search}%")
                ->orWhere('ac2.name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        $count = 1;
        if (!empty($relations)) {
            foreach ($relations as $relation) {
                $edit = route('admin.artworks.medium.categories.edit', $relation->id);
                $delete = route('admin.artworks.medium.categories.destroy', $relation->id);
                $nestedData['id'] = $count;
                $nestedData['medium'] = $relation->medium()->first()->name;
                $nestedData['category'] = $relation->category()->first()->name;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($relation->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;
                $count++;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $categoryMedium = new ArtworksMediumCategory();
        $medium = ArtworksCategory::whereIsMedium(1)->get();
        $categories = ArtworksCategory::whereIsMedium(0)->get();

        return view('artworks.medium-categories.create', compact('medium', 'categories', 'categoryMedium'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $category = ArtworksCategory::find($request->get('category'));
            $medium = ArtworksCategory::find($request->get('media'));
            if ($category) {
                if (!$category->medium->contains($request->get('media'))) {
                    $category->medium()->attach([$request->get('media') => [
                        'h1' => $request->get('h1'),
                        'canonical' => $request->get('canonical'),
                        'description' => $request->get('description'),
                        'meta_title' => $request->get('meta_title'),
                        'meta_keywords' => $request->get('meta_keywords'),
                        'meta_description' => $request->get('meta_description')
                    ]]);
                    $relation = ArtworksMediumCategory::whereMediumId($medium->id)->whereCategoryId($category->id)->first();
                    if($relation){
                        SiteMap::create([
                            "url" => $this->_url_prefix . $medium->slug . "/" . $category->slug,
                            "type" => 'ArtworksMediumCategory',
                            "referer_id" => $relation->id
                        ]);
                    }
                    $response = [
                        'status' => 'success',
                        'redirectTo' => route('admin.artworks.medium.categories.index')
                    ];
                    $request->session()->flash("success", "Relation between `{$medium->name} - {$category->name}` is successful created!");
                }
            }
        }
        return response()->json($response);
    }

    public function edit($categoryMedium)
    {
        $categoryMedium = ArtworksMediumCategory::find($categoryMedium);
        $medium = ArtworksCategory::whereIsMedium(1)->get();
        $categories = ArtworksCategory::whereIsMedium(0)->get();
        return view('artworks.medium-categories.edit', compact('medium', 'categories', 'categoryMedium'));
    }

    public function update(Request $request, $categoryMedium)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $category = ArtworksCategory::find($request->get('category'));
        $medium = ArtworksCategory::find($request->get('media'));
        if ($request->method() == 'PATCH' && $category) {
            $relation = ArtworksMediumCategory::find($categoryMedium);
            $relation->delete();
            if (!$category->medium->contains($request->get('media'))) {
                $category->medium()->attach([$request->get('media') => [
                    'h1' => $request->get('h1'),
                    'canonical' => $request->get('canonical'),
                    'description' => $request->get('description'),
                    'meta_title' => $request->get('meta_title'),
                    'meta_keywords' => $request->get('meta_keywords'),
                    'meta_description' => $request->get('meta_description')
                ]]);
                $new_relation = ArtworksMediumCategory::whereMediumId($medium->id)->whereCategoryId($category->id)->first();
                if($new_relation) {
                    $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($relation->id)->first();
                    $site_map->url = $this->_url_prefix . $medium->slug . "/" . $category->slug;
                    $site_map->referer_id = $new_relation->id;
                    $site_map->save();
                }

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.artworks.medium.categories.index')
                ];
                $request->session()->flash("success", "Relation between `{$medium->name} - {$category->name}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public function destroy($categoryMedium)
    {
        $categoryMedium = ArtworksMediumCategory::find($categoryMedium);
        $categoryMedium->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($categoryMedium->id)->first();
        $site_map->delete();

        $category =  $categoryMedium->category()->first();
        $medium =  $categoryMedium->medium()->first();

        create_redirect_301_on_delete(parse_url(route('artworks.categories.medium', [$medium->slug, join('-', [$category->slug, $medium->slug])]), PHP_URL_PATH), parse_url(route('artworks.archive.all'), PHP_URL_PATH));

        // redirect
        Session::flash('success', "Relation between `{$categoryMedium->medium()->first()->name} - {$categoryMedium->category()->first()->name}` is successful removed!");
        return redirect(route('admin.artworks.medium.categories.index'));
    }
}
