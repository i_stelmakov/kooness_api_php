{{-- Artworks - Single --}}
<?php
// Recupero delle 4 immagini in variabili
$first_image_url = '';
$second_image_url = '';
$third_image_url = '';
$fourth_image_url = '';
$first_image_url = $artwork->main_image->url;
foreach ($images as $image_key => $image) {
    if ($image->label == 2) {
        $second_image_url = $image->url;
    }
    if ($image->label == 3) {
        $third_image_url = $image->url;
    }
    if ($image->label == 4) {
        $fourth_image_url = $image->url;
    }
}
?>

@extends('layouts.app')

@section('title', ($artwork && $artwork->meta_title) ? $artwork->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$artwork->title}"))

@section('body-class', 'artwork-single')

@section('header-og-abstract', ($artwork && $artwork->meta_description) ? $artwork->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($artwork && $artwork->canonical) ? $artwork->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($artwork && $artwork->meta_keywords) ? $artwork->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-title', $artwork->title)

@section('header-og-image', url($first_image_url))

@section('header-og-url', Request::url())

@section('seo-customs-scripts')
    @if( env('APP_ENVIRONMENT_KIND') == 'prod' )
        <script>

           $(function() {

                // Add to Cart button
                var addToCartButton = document.getElementById('addToCartButton');
                if(addToCartButton !== null) {
                    addToCartButton.addEventListener(
                        'click',
                        function() {

                            // For facebook pixel
                            fbq('track', 'AddToCart', {
                                content_name: '{{ $artwork->title }}',
                                content_category: '@foreach($mediums as $media_key=>$media){{ $media->name }}@if(!$loop->last), @endif @endforeach',
                                content_ids: ['{{ $artwork->reference }}'],
                                content_type: 'product',
                                value: {{ $artwork->price }},
                                currency: 'EUR'
                            });
                            console.log('Facebook Pixel "AddToCart" triggered!');

                            // For Google Analytics
                            ga('send', 'event', 'Funnel', 'Add to Cart');
                            console.log('Google Analytics "AddToCart" triggered!');
                        },
                        false
                    );
                }

                // Make an Offer button
                var submitApplicationButton = document.getElementById('send-make-an-offer');

                if(submitApplicationButton !== null) {
                    submitApplicationButton.addEventListener(
                        'click',
                        function() {
                            console.log('"Make an Offer" triggered!');

                            // For facebook pixel
                            fbq('track', 'SubmitApplication');
                            console.log('Facebook Pixel "Make an Offer" triggered!');

                            // For Google Analytics
                            ga('send', 'event', 'Lead', 'Make an Offer');
                            console.log('Google Analytics "Make an Offer" triggered!');

                        },
                        false
                    );
                }
                // Available on Fair button
                var availableOnFairButton = document.getElementById('send-available-in-fair');

                if(availableOnFairButton !== null) {
                    availableOnFairButton.addEventListener(
                        'click',
                        function() {
                            console.log('"Available On Fair" triggered!');

                            // For facebook pixel
                            fbq('track', 'Lead');
                            console.log('Facebook Pixel "Make an Offer" triggered!');

                            // For Google Analytics
                            ga('send', 'event', 'Lead', 'Available On Fair');
                            console.log('Google Analytics "Available On Fair" triggered!');

                        },
                        false
                    );
                }

            });
        </script>
    @endif
@endsection

@section('content')

    @if(Session::has('msg_added_to_cart'))
        <script>
            window.onload = function() {
                var isAddedToCartMsg = '{{ Session::get('msg_added_to_cart') }}';
                toastr.success(isAddedToCartMsg);
            };
        </script>
    @endif
    <!--view in room-->
    <div class="view-in-room-overlay"></div>
    <div class="artwork-view-in-room out" data-state="in">
        <div class="vir-room" style="-webkit-transform: scale(0.75);" data-room="salotto">
            <a style="bottom: 15%; width: 100%; text-align: center; position:absolute;" href="https://itunes.apple.com/us/app/kooness/id963773992?mt=8" target="_blank">
                <img style="width: 300px; margin: 0 10px; display: inline-block;" class="img-responsive lazy" data-src="{{ url('/images/app-store.svg') }}" src="">
            </a>
            {{-- <a style="bottom: 25%; width: 100%; text-align: center; position:absolute;" target="_blank" href="https://play.google.com/store/apps/details?id=com.sma.kooness">
                <img style="height: 89px; margin: 0 10px; display: inline-block; padding-left:42px;" data-src="{{ url('/images/google-play.svg') }}" class="img-responsive lazy" src="">
            </a> --}}
        </div>
        <img data-width="{{ $artwork->width * 10 }}" id="vir-artwork" data-src="{{ $first_image_url }}" class="is-transition is-notransition lazy" style="position: absolute; left: 0; right: 0; margin: 0 auto;" src="">
        <div rel="nofollow" class="dz-close view-in-room-close">
            <span class="icon-close"><i class="fa fa-close"></i></span>
        </div>
        <button type="button" class="room-btn" id="prev-room" data-dir="-1"></button>
        <button type="button" class="room-btn" id="next-room" data-dir="1"></button>
    </div>
    <!--//view in room-->

    {{-- Add to My Galleries--}}
    @if(Auth::user())
        <div class="remodal" data-remodal-id="modal-my-gallery" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>
            <form class="collect-form">
                <h2 class="collect-form-header">Select a Collection</h2>
                <input name="id" type="hidden" value="{{ $artwork->id }}"> <select name="collection_id">
                    @if(count(Auth::user()->collections()->get()))
                        @foreach(Auth::user()->collections()->get() as $collection)
                            <option value="{{ $collection->id }}">{{ $collection->name }}</option>
                        @endforeach
                    @else
                        <option value="">Not collection yet</option>
                    @endif
                </select><br> <a class="accordion" href="#"><i class="fa fa-arrow-right"></i> Go to Collections</a>

                <h3 class="collect-form-header">or create a new one below:</h3>

                <input type="text" name="collection_name" placeholder="New Collection name">
                <a class="add-new-collection"><i class="fa fa-plus"></i> Add New</a>

                <div>
                    <input class="collection-save small-input-button" type="submit" value="Save">
                    <button class="collection-close small-input-button" data-remodal-action="cancel">Cancel</button>
                </div>
            </form>
        </div>
    @endif
    {{-- .Add to My Galleries--}}

    <div class="sections">
        <!-- Product Sheet !-->
        <section class="default-sheet-section">
            <div class="container boxed-container">
                <div class="container col-container">
                    <!-- Product Sheet Image Col !-->
                    <div id="product-img-col" class="col one-half-col">
                        <!-- Product Sheet Image !-->
                        <a title="{{ $artwork->title }}" class="default-sheet-img-link" href="{{ $first_image_url }}" data-lightbox="example-set">

                            {{--<div class="ratio-container-1-1" style="border: 2px inset blue;">--}}
                                {{--<div class="ratio-content cover-image" style="background-image: url('{{ $first_image_url }}');"></div>--}}
                            {{--</div>--}}

                            {{--<div class="ratio-container-1-1 bg position" style="border: 2px inset red;">--}}
                                {{--<div class="ratio-content contain-image" style="background-image: url('{{ $first_image_url }}');"></div>--}}
                            {{--</div>--}}

                            <img class="default-sheet-img lazy" data-src="{{ $first_image_url }}" src="" />

                        </a>
                        <div class="col-container-with-offset my-gallery-thumb-row">
                            @if($second_image_url)
                                <div class="slider-item-img square-box one-third-col">
                                    <a title="{{ $artwork->title }}" href="{{ $second_image_url }}" data-lightbox="example-set">
                                        <div class="background-cover lazy" data-src="{{ $second_image_url }}"></div>
                                    </a>
                                </div>
                            @endif
                            @if($third_image_url)
                                <div class="slider-item-img square-box one-third-col">
                                    <a title="{{ $artwork->title }}" href="{{ $third_image_url }}" data-lightbox="example-set">
                                        <div class="background-cover lazy" data-src="{{ $third_image_url }}"></div>
                                    </a>
                                </div>
                            @endif
                            @if($fourth_image_url)
                                <div class="slider-item-img square-box one-third-col">
                                    <a title="{{ $artwork->title }}" href="{{ $fourth_image_url }}" data-lightbox="example-set">
                                        <div class="background-cover lazy" data-src="{{ $fourth_image_url }}"></div>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <!-- Product Sheet Meta !-->
                        <div id="default-sheet-meta" class="default-sheet-box">
                            <div class="default-sheet-row">
                                <!-- Product Sheet Meta Links !-->
                                <div class="default-sheet-col">
                                    @if(Auth::user())
                                        <a data-remodal-target="modal-my-gallery" href="#" class="icon-link secondary-link">
                                            <i class="fa fa-plus"></i> Add/Create your favourite gallery
                                        </a>
                                    @endif
                                        <a id="view-in-room" class="icon-link secondary-link">
                                            <i class="fa fa-file-image-o"> </i> View in a Room
                                        </a>
                                </div>
                                <!-- Share !-->
                                <div id="default-sheet-share" class="default-sheet-col">
                                    <h5>
                                        <strong>Share</strong>
                                    </h5>
                                    <div class="icon-container">
                                        <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artwork->about_the_work, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                        <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artwork->about_the_work, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                        <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artwork->about_the_work, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                        <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artwork->about_the_work, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                        <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artwork->about_the_work, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Product Sheet Col !-->
                    <div id="default-sheet" class="col one-half-col">
                        <!-- Product Sheet Header !-->
                        <div id="default-sheet-header" class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h3 class="no-margin">
                                        <a class="artist-name dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                    </h3>
                                </div>

                                @if(Auth::user())
                                <a class="follow secondary-link {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                    @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                        <i class="fa fa-heart"> </i>
                                        <span class="follow-unfollow">
                                            Unfollow
                                        </span>
                                    @else
                                        <i class="fa fa-heart-o"> </i>
                                        <span class="follow-unfollow">
                                            Follow
                                        </span>
                                    @endif
                                </a>
                                @endif

                            </div>
                            <div class="default-sheet-row">
                                <h1 class="no-margin">{{ ($artwork->h1)? $artwork->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $artwork->title) }}</h1>
                            </div>
                            @if($artwork->from_the_series)
                                <div class="default-sheet-row">
                                    <p class="dark-text">From the series {{ $artwork->from_the_series }}</p>
                                </div>
                            @endif
                            <div id="title-note" class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="small-p artwork-features">
                                        @if($artwork->single_piece == 1)
                                            <em>Single piece</em>
                                        @endif
                                        
                                        @if($artwork->signed == 1)
                                            <em>Signed</em>
                                        @endif

                                        @if($artwork->dated == 1)
                                            <em>Dated</em>
                                        @endif

                                        @if($artwork->titled == 1)
                                            <em>Titled</em>
                                        @endif

                                        @if($artwork->framed == 1)
                                            <em>Framed</em>
                                        @endif
                                        {{--<em>Single piece<br />Signed, dated and titled</em>--}}
                                    </p>
                                </div>
                                <div class="default-sheet-row-cell flex-align-center purchase-row">
                                    <div class="default-sheet-col">
                                        <a href="/pages/support-center#shippings-and-returns" title="Best price">
                                            <div class="default-sheet-row-cell payment-logo flex-align-center">
                                                <svg class="svg-icon" height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 74.42 79.06"><g data-name="Livello 2"><g data-name="Livello 3"><path d="M33.92 55.06V50h-3.67V27.81h-6.88A29.12 29.12 0 0 1 20.09 31a26 26 0 0 1-3.59 1.84l1.92 4.31 3.83-1.91V50h-3.83v5.08zM56.75 47.31v-4.16h-2.17V27.81h-7.91l-11.33 14v5.53h11.32V50h-2.58v5.08h12.67V50h-2.17v-2.69zm-10.08-4.16h-5.95l5.95-7.55z"/><path d="M42.92 5.08v8a29.36 29.36 0 1 1-12.67.27c.14.65 1.13 5.65 1.13 5.65l7.37-11.62L27.09 0s.63 2.56 1.43 5.66a37.22 37.22 0 1 0 14.39-.58z"/></g></g></svg>
                                            </div>
                                            <div class="default-sheet-row-cell">
                                                <p class="small-p">Free Return</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="default-sheet-col">
                                        <a href="/pages/support-center#payments" title="Secure payment">
                                            <div class="default-sheet-row-cell payment-logo flex-align-center">
                                                <i class="fa fa-lock"> </i>
                                            </div>
                                            <div class="default-sheet-row-cell">
                                                <p class="small-p">Secure Payment</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="default-sheet-col">
                                        <a href="/pages/support-center#shippings-and-returns" title="Delivery">
                                            <div class="default-sheet-row-cell payment-logo flex-align-center">
                                                <i class="fa fa-truck"> </i>
                                            </div>
                                            <div class="default-sheet-row-cell">
                                                <p class="small-p">Delivery</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Product Sheet Info !-->
                        <div id="default-sheet-info" class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="dark-text">
                                        <strong>Reference</strong>
                                    </p>
                                </div>
                                <div class="default-sheet-row-cell">
                                    <p>{{ $artwork->reference }}</p>
                                </div>
                            </div>
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="dark-text">
                                        <strong>Year</strong>
                                    </p>
                                </div>
                                <div class="default-sheet-row-cell">
                                    <p>{{ $artwork->year }}</p>
                                </div>
                            </div>
                            @if(count($mediums))
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Medium</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <p>
                                            @foreach($mediums as $media_key=>$media)
                                                {{ $media->name }}
                                                @if(!$loop->last)
                                                    ,
                                                @endif
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                            @endif
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="dark-text">
                                        <strong>Size</strong>
                                    </p>
                                </div>
                                <div class="default-sheet-row-cell">
                                    <p>{{ $artwork->measure_cm  }} cm
                                        <br/>{{ $artwork->measure_inc }} in
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Product Sheet Buy !-->
                        @if(!$artwork->available_in_fair || $artwork->show_price)
                        <div id="default-sheet-buy" class="default-sheet-box">
                            @if(Auth::user())
                                @if(!Auth::user()->offers()->whereArtworkId($artwork->id)->whereStatus(2)->count())
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Promo code</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row row-code" style="display: none;">
                                        <p data-value="0" class="dark-text"></p>
                                        <a title="Remove promo code" class="remove-promo-code text-left-space"> Remove discount</a>
                                    </div>
                                    <div class="default-sheet-row-cell row-insert-code">
                                        <form onSubmit="return false">
                                            <input type="text" class="promo-code" placeholder="Example: AXCF1245">
                                        </form>
                                        <a title="Add Promo Code" class="add-promo-code text-left-space">Apply discount</a>
                                    </div>
                                </div>
                                @endif
                            @endif
                            <div id="default-sheet-price" class="default-sheet-col default-sheet-price">
                                <p>Price</p>
                                <div class="default-sheet-row flex-align-center">
                                    <div class="default-sheet-row-cell">
                                        <h4 data-price="{{ $artwork->price }}" data-currency="EUR">{{ $artwork->pretty_price }} </h4>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                    <select class="currency-change no-margin">
                                        <option value="EUR" selected="selected">€</option>
                                        <option value="USD">$</option>
                                        <option value="GBP">£</option>
                                    </select>
                                    </div>
                                </div>
                                <p>Contact our advisors for more info at
                                    <a href="mailto:info@kooness.com">info@kooness.com</a></p>
                                <div class="default-sheet-row flex-align-center purchase-row">
                                    <div class="default-sheet-row-cell flex-align-center">
                                        @if( $artwork->status == 1 && !$artwork->available_in_fair)
                                            @if(!$cart->items()->whereProductId($artwork->id)->count())
                                                <form class="avoid-form-multiple-submission" method="POST" action="{{ route('orders.add.to.cart') }}" >
                                                    @csrf
                                                    <input type="hidden" value="{{ $artwork->id }}" name="id">
                                                    <button id="addToCartButton" class="black-button">PURCHASE</button>
                                                </form>
                                            @else
                                                <button class="black-button">ALREADY IN CART</button>
                                            @endif
                                        @else
                                            <button class="black-button">SOLD OUT</button>
                                        @endif
                                    </div>
                                </div>
                                @if($artwork->status == 1 && !$artwork->available_in_fair)
                                    @if(Auth::user())
                                        @if(Auth::user()->offers()->whereArtworkId($artwork->id)->whereStatus(1)->count())
                                            <a class="light-button">OFFERT SENT</a>
                                        @elseif(Auth::user()->offers()->whereArtworkId($artwork->id)->whereStatus(2)->count())
                                            <a class="light-button">OFFERT ACCEPTED</a>
                                        @else
                                            <div class="box-offer-info">
                                                <a id="submitApplicationButton" class="light-button make-an-offer">Make an offer</a>
                                                <a href="/pages/support-center#make-an-offer" title="Make an Offer" class="box-offer-info-icon">
                                                    <div class="default-sheet-row-cell payment-logo flex-align-center">
                                                        <i class="fa fa-info"> </i>
                                                    </div>
                                                    <div class="default-sheet-row-cell flex-align-center">
                                                        <p class="small-p">More about Offer</p>
                                                    </div>
                                                </a>
                                            </div>

                                        @endif
                                        {{--Modal make an offer--}}
                                        <div class="remodal" data-remodal-id="modal-make-an-offer" data-remodal-options="hashTracking: false">
                                            <button data-remodal-action="close" class="remodal-close"></button>
                                            <h2 class="collect-form-header">Make an offer for {{ $artwork->title }}</h2>
                                            <form class="make-an-offer-form k-form container col-container-with-offset-and-margin">
                                                <input type="hidden" name="artwork_id" value="{{ $artwork->id }}">
                                                <div class="full-col-with-margin">
                                                    <label>Proposed Amount</label>
                                                    <input class="full-col reset" type="number" id="amount" name="amount" step="1" placeholder="0.00" required>
                                                </div>
                                                <div class="full-col-with-margin">
                                                    <label>Message</label>
                                                    <textarea class="full-col reset" id="text" name="text" placeholder="Insert custom message"></textarea>
                                                </div>
                                                <div>
                                                    <button id="send-make-an-offer" class="small-input-button" type="submit" value=>Send</button>
                                                    <button class="small-input-button" data-remodal-action="cancel">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{--end modal--}}
                                    @else
                                        <a id="submitApplicationButton" rel="nofollow" title="Make an offer" class="light-button" href="{{ route( 'register', [ 'referer'=> '6ba3e2f93fad23c82b0d5deeca05bc5d5f6f26c4', 'id'=> base64_encode($artwork->id) ] ) }}">Make an offer</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                        @endif
                        @if( $artwork->available_in_fair)
                            <div class="default-sheet-row flex-align-center purchase-row">
                                <div class="default-sheet-row-cell flex-align-center">
                                    @if(Auth::user())
                                        <a id="available-on-fair" rel="nofollow" title="Request info for artwork Available on Fair" class="black-button available-in-fair">AVAILABLE ON FAIR</a>
                                    @else
                                        <a id="available-on-fair" rel="nofollow" title="Request info for artwork Available on Fair" class="black-button" href="{{ route( 'register', [ 'referer'=> '95310609c1d2ca08ef1b2899b8d273d36ef53f7a', 'id'=> base64_encode($artwork->id) ] ) }}">AVAILABLE ON FAIR</a>
                                    @endif
                                </div>
                            </div>
                            {{--Modal Available on Fair--}}
                            <div class="remodal" data-remodal-id="modal-available-in-fair" data-remodal-options="hashTracking: false">
                                <button data-remodal-action="close" class="remodal-close"></button>
                                <h2 class="collect-form-header">Make a request for {{ $artwork->title }} available on fair</h2>
                                <form class="available-in-fair-form k-form container col-container-with-offset-and-margin">
                                    <input type="hidden" name="artwork_id" value="{{ $artwork->id }}">
                                    <div class="full-col-with-margin">
                                        <label>Message</label>
                                        <textarea class="full-col reset" id="text" name="text" placeholder="Insert custom message"></textarea>
                                    </div>
                                    <div>
                                        <button id="send-available-in-fair" class="small-input-button" type="submit" value=>Send</button>
                                        <button class="small-input-button" data-remodal-action="cancel">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            {{--end modal--}}

                        @endif

                        <!-- Product Sheet Art Gallery !-->
                        @if($gallery)
                            <div id="default-sheet-gallery" class="default-sheet-box">
                                <div id="default-sheet-price" class="default-sheet-col">
                                    <p>Artwork offered by</p>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <h4>
                                                <a title="" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name}}</a>, {{ $gallery->city }}
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endif
                    <!-- Product Sheet Category/Tags !-->
                        <div id="default-sheet-category" class="default-sheet-box">
                            @if(count($categories))
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Category</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <ul>
                                            @foreach($categories as $category)
                                                <li>
                                                    <a title="{{ $category->name }}" href="{{ route('artworks.single', [$category->slug]) }}">{{ $category->name  }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @if(count($tags))
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Tags</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <ul>
                                            @foreach($tags as $tag)
                                                <li>
                                                    <a title="{{ $tag->name }}" href="{{ route('tags.single', [$tag->slug]) }}">{{ $tag->name  }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container boxed-container">
            <hr class="divider"/>
        </div>

        <div class="artwork-goto-buttons container boxed-container">
            <a class="black-button" href="#about-the-work">ABOUT THE WORK</a>
            <a class="black-button" href="#about-the-artist">ABOUT THE ARTIST</a>
            <a class="black-button" href="#more-from-the-artist">MORE FROM THE ARTIST</a>
            <a class="black-button" href="#about-the-gallery">ABOUT THE GALLERY</a>
            <a class="black-button" href="#similar-works">SIMILAR WORKS</a>
        </div>



        <!-- Description !-->
        <section class="tab-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin container-vertical-padding-top container-vertical-padding-bottom">
                    <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                        <div id="about-the-work"></div>
                        <ul>
                            @if($artwork->about_the_work)
                                <li class="current-tab-item tab-button" data-id="1">About the work</li>
                            @endif
                            @if($artwork->news()->whereStatus(1)->count())
                                <li class="tab-button" data-id="2">News</li>
                            @endif
                            @if($artwork->bibliography)
                                <li class="tab-button" data-id="3">Biblioraphy</li>
                            @endif
                        </ul>
                    </div>
                    <!-- Tab 1 !-->
                    @if($artwork->about_the_work)
                        <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                            <div class="show-more-text-container">
                                <div class="show-more-content">
                                    <div class="show-more-content-inner">
                                        {!! $artwork->about_the_work !!}
                                    </div>
                                </div>
                                <div class="show-more-button">
                                    <a href="#">Show more</a>
                                </div>
                            </div>​
                        </div>
                    @endif
                    @if($artwork->news()->whereStatus(1)->count())
                    <!-- Tab 2 !-->
                        <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                            <section id="featured-news">
                                <div class="container boxed-container">
                                    <div class="default-slider-header">
                                        <span class="h1">Featured News</span>
                                        <p>
                                            <a class="read-more" href="{{ route('posts.archive.all', ["news"]) }}">All featured news</a>
                                        </p>
                                    </div>
                                    <div class="container col-container-with-offset-and-margin">
                                        @foreach($artwork->news()->whereStatus(1)->limit(3)->get() as $news)
                                            <div class="slider-item one-third-col-with-margin">
                                                <div class="slider-item-img wide-box">
                                                    <a title="{{ $news->title }}" href="{{ route('posts.single', ["news", $news->slug]) }}">
                                                        <div class="background-cover gallery lazy" data-src="{{ $news->url_wide_box }}"></div>
                                                    </a>
                                                </div>
                                                <div class="featured-news-item-txt">
                                                    <p class="news-date">{{  date('d F Y', strtotime($news->date)) }}</p>
                                                    <div class="default-sheet-row">
                                                        <h2>
                                                            <a class="dark-text" href="{{ route('posts.single', ["news", $news->slug]) }}">{{ $news->title }}</a>
                                                        </h2>
                                                    </div>
                                                    <div class="default-sheet-row-cell">
                                                        <p class="dark-text news-author">By <a>{{ $news->author }}</a>
                                                        <p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endif
                <!-- Tab 3 !-->
                    @if($artist->bibliography)
                        <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                            {!! $artist->bibliography !!}
                        </div>
                    @endif
                </div>
            </div>
        </section>
        <!-- Post Navigation !-->
        <div class="container boxed-container">
            <hr class="divider"/>
        </div>
        <!-- Artist Preview !-->
        <div id="about-the-artist"></div>
        <section class="inner-title-section">
            <div class="container boxed-container">
                <div class="inner-title-container">
                    <span class="h1">About the <span class="main-color-txt">Artist</span></span>
                </div>
            </div>
        </section>
        <section class="preview-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin">
                    <div class="one-fifths-col-with-margin container-vertical-padding-bottom">
                        <div class="square-box">
                            <a href="{{ route('artists.single', [$artist->slug]) }}">
                                <div class="background-cover lazy" data-src="{{ $artist->url_square_box }}"></div>
                            </a>
                        </div>
                    </div>
                    <div class="four-fifths-col-with-margin preview-txt-col">
                        <div class="preview-title default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <div class="default-sheet-row-cell">
                                    <h2>
                                        <a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                    </h2>
                                </div>
                            </div>
                            <div class="default-sheet-row-cell">
                                <div class="icon-container">
                                    <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                    <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                    <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                    <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                    <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="default-sheet">
                            <div class="default-sheet-row">
                                <p>
                                    <span class="dark-text">{{ $artist->date_of_birth }}</span> {{ $artist->city_of_birth }}, {{ $artist->countryBirth()->first()->{'name'} }}
                                </p>
                            </div>
                        </div>
                        <div class="default-sheet-box default-sheet-txt-col">


                            <div class="show-more-text-container">
                                <div class="show-more-content">
                                    <div class="show-more-content-inner">
                                        {!! $artist->about_the_artist !!}
                                    </div>
                                </div>
                                <div class="show-more-button no-display">
                                    <a href="#">Show more</a>
                                </div>
                                <br>
                                <a class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">Read more</a>

                            </div>​


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Product Slider !-->
        <div id="more-from-the-artist"></div>
        @if($artist->artworks()->where("id", "!=", $artwork->id)->whereRaw('status IN (1,2)')->limit(3)->count())
        <section id="product-slider" class="default-slider-section slider-border">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin">
                    <div class="one-fourth-col-with-margin slider-title-box">
                        <div class="slider-title-box-txt">
                            <span class="h1">More from the 
                                <span class="main-color-txt slider-title-span">Artist</span>
                            </span>
                            <p>See other works by
                                <span class="dark-text">{{ $artist->first_name }} {{ $artist->last_name }}</span></p>
                            <a class="black-button" href="{{ route('artists.single', [$artist->slug]) }}">View all</a>
                        </div>
                        <div class="slider-title-drop-caps">
                            <h6 class="lazy" data-src="{{ $artist->url_square_box }}">k</h6>
                        </div>
                    </div>
                    <!-- Item 1 !-->
                    @foreach($artist->artworks()->where("id", "!=", $artwork->id)->whereRaw('status IN (1,2)')->limit(3)->get() as $art)
                        <div class="slider-item one-fourth-col-with-margin half-width-mobile">
                            <i title="Remove" class="fa fa-times delete-item"> </i>
                            @if(Auth::user())
                                <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                    @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                        <i class="fa fa-heart"></i>
                                    @else
                                        <i class="fa fa-heart-o"></i>
                                    @endif
                                </div>
                            @endif
                            <div class="slider-item-img square-box">
                                <a title="{{ $art->title }}" href="{{ route('artworks.single', [$art->slug])  }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $art->main_image->url }}"></div>
                                </a>
                            </div>
                            @include('partials.includes.box-artwork',  ["item" => $art])
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        @endif

        <!-- Gallery Preview !-->
        <div id="about-the-gallery"></div>
        <section class="inner-title-section">
            <div class="container boxed-container">
                <div class="inner-title-container">
                    <span class="h1" class="inner-title">About the <span class="main-color-txt">Gallery</span></span>
                </div>
            </div>
        </section>
        <section class="preview-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin">
                    <div class="one-fifths-col-with-margin container-vertical-padding-bottom">
                        <div class="square-box">
                            <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">
                                <div class="background-cover lazy" data-src="{{ $gallery->url_square_box }}"></div>
                            </a>
                        </div>
                    </div>
                    <div class="four-fifths-col-with-margin preview-txt-col">
                        <div class="preview-title default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <div class="default-sheet-row-cell">
                                    <h2>
                                        <a class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                    </h2>
                                </div>
                            </div>
                            <div class="default-sheet-row-cell">
                                <div class="icon-container">
                                    <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                    <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                    <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                    <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                    <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="default-sheet">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="dark-text">
                                        <strong>Address</strong>
                                    </p>
                                </div>
                                <div class="default-sheet-row-cell">
                                    <p>{{ $gallery->city }}, {{ $gallery->address }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="default-sheet-box">
                            <p>
                                {!! substr($gallery->about_gallery, 0, 360).'...' !!}
                            </p>
                            <p>
                                <a class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">Read More</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            $category_first = $artwork->categories()->whereIsMedium(1)->first();
        ?>
        @if($category_first)
            <!-- Similar works Preview !-->
            <div id="similar-works"></div>
            <section id="product-slider" class="default-slider-section slider-border">
                <div class="container boxed-container">
                    <div class="container col-container-with-offset">
                        <div class="one-fourth-col-with-margin slider-title-box">
                            <div class="slider-title-box-txt">
                                <span class="h1">Similar 
                                    <span class="main-color-txt slider-title-span">Works by</span>
                                </span>
                                <p>Start browsing similar works according to Medium</p>
                                <a class="black-button" href="{{ route('artworks.single', [$category_first->slug]) }}">{{ $category_first->name }}</a>
                            </div>
                            <div class="slider-title-drop-caps">
                                <h6 style="background-image: url(/images/opera-3.jpg)">k</h6>
                            </div>
                        </div>
                        @foreach($category_first->artworks()->whereRaw("status IN (1,2) AND artworks.id != '$artwork->id'")->limit(3)->get() as $artwork_key=>$artwork)
                            <div class="slider-item one-fourth-col-with-margin">

                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                        @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif

                                <div class="slider-item-img square-box">
                                    <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <h2>
                                        <a class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a>
                                    </h2>
                                    <p class="artwork-date">{{ $artwork->year }}</p>
                                    <p class="artwork-measures">{{ $artwork->measure_cm }} cm</p>
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="artwork-type">
                                                @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $ArtworkMedium)
                                                    <a title="{{ $ArtworkMedium->name }}" href="{{ route('artworks.single', [$ArtworkMedium->slug]) }}">{{ $ArtworkMedium->name }}</a>
                                                    <br>
                                                @endforeach
                                            </p>
                                        </div>
                                        @if(!$artwork->available_in_fair || $artwork->show_price)
                                            <div class="default-sheet-row-cell">
                                                <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
    </div>
@endsection