<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIncorrectField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string('url_picture')->after('phone')->nullable();
            $table->dropColumn('phone');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('phone');
            $table->dropColumn('address2');
            $table->dropColumn('address3');

            $table->integer('country_id_2')->unsigned()->nullable()->after('zip');
            $table->string('city_2')->nullable()->after('country_id_2');
            $table->string('address_2')->nullable()->after('city_2');
            $table->string('zip_2')->nullable()->after('address_2');

            $table->integer('country_id_3')->unsigned()->nullable()->after('zip_2');
            $table->string('city_3')->nullable()->after('country_id_3');
            $table->string('address_3')->nullable()->after('city_3');
            $table->string('zip_3')->nullable()->after('address_3');

            $table->foreign('country_id_2')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');

            $table->foreign('country_id_3')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');
        });

        Schema::table('exhibitions', function (Blueprint $table) {
            $table->dropColumn('phone');
        });

        Schema::table('fairs', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->string('url_floorplant')->nullable()->after('tickets');
            $table->string('thumb_floorplant')->nullable()->after('url_floorplant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
