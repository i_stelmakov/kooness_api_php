@extends('layouts.app')

@section('title', ($tag && $tag->meta_title) ? $tag->meta_title : "Kooness | {$tag->name}")

@section('body-class', 'tags-archive')

@section('header-og-abstract', ($tag && $tag->meta_description) ? $tag->meta_description : null)

@section('canonical', ($tag && $tag->canonical) ? $tag->canonical : null)

@section('seo-keywords', ($tag && $tag->meta_keywords) ? $tag->meta_keywords : null)

@section('index_opt', 'noindex')

@section('follow_opt', 'follow')

@section('content')

<div class="sections">

    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <h1>Tags</h1>
            </div>
        </div>
    </div>

        <!-- Gallery Sheet Header !-->
        @if(count($result['artworks']))
            <section>
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <span class="h1">Artworks</span>
                            </div>
                        </div>
                        @if(!$type)
                            <div>
                                <a class="page-header-link" title="Galleries Archive" href="{{ route('tags.single', [$tag->slug, 'artworks']) }}">All tagged artworks</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="container boxed-container">
                <div class="col-container">
                    <div class="category-filter-container container-load">
                        @foreach($result['artworks'] as $artwork)
                            {{--START - Item--}}
                            <div class="slider-item one-fourth-col-with-margin">
                                <i title="Remove" class="fa fa-times delete-item"> </i>

                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                        @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif

                                <div class="slider-item-img square-box">
                                    <a title="{{ $artwork->title }}" href="{{ route('artworks.single', [$artwork->slug])  }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                                    </a>
                                </div>
                                @include('partials.includes.box-artwork',  ["item" => $artwork])
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(count($result['artists']))
            <section>
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <span class="h1">Artists</span>
                            </div>
                        </div>
                        @if(!$type)
                            <div>
                                <a class="page-header-link" title="Galleries Archive" href="{{ route('tags.single', [$tag->slug, 'artists']) }}">All tagged artists</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="container boxed-container">
                <div class="col-container">
                    <div class="category-filter-container container-load">
                        @foreach($result['artists'] as $artist_key=>$artist)
                            {{--START - Item--}}

                            <div class="slider-item one-fourth-col-with-margin">
                                <i title="Remove" class="fa fa-times delete-item"></i>

                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                        @if(Auth::user()->collectionArtists->contains($artist->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif

                                <div class="slider-item-img square-box">
                                    <a title="{{ $artist->first_name . " " . $artist->last_name }}" href="{{  route('artists.single', [$artist->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $artist->url_square_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <h2>
                                                <a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                            </h2>
                                        </div>
                                    </div>
                                    @if($artist->countryBirth()->count())
                                        <p class="dark-text provenance">{{ $artist->countryBirth()->first()->name }}</p>
                                    @endif
                                    <p class="exposure">{{ $artist->artworks()->count() }} Works exhibited</p>
                                    <a class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
                                </div>
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(count($result['galleries']))
            <section>
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <span class="h1">Galleries</span>
                            </div>
                        </div>
                        @if(!$type)
                            <div>
                                <a class="page-header-link" title="Galleries Archive" href="{{ route('tags.single', [$tag->slug, 'galleries']) }}">All tagged galleries</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="container boxed-container">
                <div class="col-container">
                    <div class="category-filter-container container-load">
                        @foreach($result['galleries'] as $gallery)
                            {{--START - Item--}}
                            <div class="slider-item one-third-col-with-margin">
                                <i title="Remove" class="fa fa-times delete-item"> </i>

                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionGalleries->contains($gallery->id)) ? 'active' : '' }}" data-section="galleries" data-id="{{$gallery->id}}">
                                        @if(Auth::user()->collectionGalleries->contains($gallery->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif

                                <div class="slider-item-img wide-box">
                                    <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $gallery->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <h2>
                                                <a title="{{ $gallery->name }}" class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="gallery-adress">{{ $gallery->city }}, {{ $gallery->address }}</p>
                                        </div>
                                    </div>
                                    <a title="{{ $gallery->name }}" class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">read more</a>
                                </div>
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(count($result['magazine']))
            <section>
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <span class="h1">Magazine</span>
                            </div>
                        </div>
                        @if(!$type)
                            <div>
                                <a class="page-header-link" title="Galleries Archive" href="{{ route('tags.single', [$tag->slug, 'magazine']) }}">All tagged magazine</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="container boxed-container">
                <div class="col-container">
                    <div class="category-filter-container container-load">
                        @foreach($result['magazine'] as $post)
                            {{--START - Item--}}
                            <div class="slider-item one-third-col-with-margin">
                                <div class="slider-item-img wide-box">
                                    <a href="{{ route('posts.single', ['magazine', $post->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $post->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="featured-news-item-txt">
                                    <p class="news-date">{{  date('d F Y', strtotime($post->date)) }}</p>
                                    <div class="default-sheet-row">
                                        <h2>
                                            <a class="dark-text" href="{{ route('posts.single', ['magazine', $post->slug]) }}">{{ $post->title }}</a>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if(count($result['news']))
            <section>
                <div class="container boxed-container">
                    <div class="col-container-with-offset">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <span class="h1">News</span>
                            </div>
                        </div>
                        @if(!$type)
                            <div>
                                <a class="page-header-link" title="Galleries Archive" href="{{ route('tags.single', [$tag->slug, 'news']) }}">All tagged news</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="container boxed-container">
                <div class="col-container">
                    <div class="category-filter-container container-load">
                        @foreach($result['news'] as $post)
                            {{--START - Item--}}
                            <div class="slider-item one-third-col-with-margin">
                                <div class="slider-item-img wide-box">
                                    <a href="{{ route('posts.single', ['news', $post->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $post->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="featured-news-item-txt">
                                    <p class="news-date">{{  date('d F Y', strtotime($post->date)) }}</p>
                                    <div class="default-sheet-row">
                                        <h2>
                                            <a class="dark-text" href="{{ route('posts.single', ['news', $post->slug]) }}">{{ $post->title }}</a>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if($type)
            <div class="load-more-box">
                <button data-offset="{{ $limit }}" data-limit="{{ $limit }}" data-section="{{ $type }}" data-tagged="{{ $tag->id }}" class="btn-load-more btn_load avoid-multiple-submission">Load More</button>
            </div>
        @endif
    </div>
@endsection