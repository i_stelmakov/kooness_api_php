@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : "Kooness | {$page->title}")

@section('body-class', 'page')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)


@section('content')
{{--ABOUT--}}
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : $page->title }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

<div class="container boxed-container">
    <div class="container col-container-with-offset-and-margin container-vertical-padding-bottom">
        <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
            <ul>
                <li class="tab-button current-tab-item" data-id="1"><a href="#who-we-are">Who We Are</a></li>
                <li class="tab-button" data-id="2"><a href="#the-team">The Team</a></li>
                {{-- Disattivato in attesa dei contenuti --}}
                {{-- <li class="tab-button" data-id="3"><a href="#press">Press</a></li> --}}
                <li class="tab-button" data-id="4"><a href="#careers">Careers</a></li>
                <li class="tab-button" data-id="5"><a href="#contact-us">Contact Us</a></li>
                <li class="tab-button no-display" data-id="6"><a href="#special-projects">Special Projects</a></li>
            </ul>
        </div>
        <!-- Tab 1 !-->
        <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
            <div class="container col-container-with-margin">
                <div class="col one-half-col-with-margin">
                    <p>
                        <span class="drop-caps dark-text">F</span>
                        <strong class="dark-text">
                                ounded in 2015 and based in Milan - the city of art, fashion and design - Kooness is a leading marketplace for the art world. Kooness provides everyone passionate about art –from newbies to major art collectors – a new and simple way to discover, share and buy contemporary art.
                        </strong>
                    </p>
                    <blockquote>
                        <em class="dark-text">
                                Kooness is committed to sourcing quality artworks in the globe and offers art enthusiasts and collectorsa one-stop resource for those interested in discovering and acquiring contemporary art pieces from top emerging artists. Our growing online database of contemporary art can be used by art lovers, students, art dealers, museum-goers, patrons, collectors, curators, artists and educators to discover, collect and learn about art. Kooness wants to give to all users the correct tools to access the vast contemporary art world, in a very simple way.
                        </em>
                    </blockquote>
                    <h3>How it works</h3>
                    <p>
                            Kooness is an online platform that allows you to step in the fine art gallery world by providing a virtual access to galleries all over the world. It is a revolutionary way to explore and collect art by buying directly from the finest art galleries. On Kooness can find the best selection of artworks by both emerging and established artists, accurately selected by our curatorial team. To speed up and simplify the research for your favorite artwork we added a tool to filter through different categories and tags. Kooness will shake up the art world by attracting new collectors with the use of the most advanced technologies to enjoy art even more.
                    </p>
                </div>
                <div class="col one-half-col-with-margin">
                    <img src="/images/fair2.jpg">
                </div>
            </div>
        </div>
        <!-- Tab 2 !-->
        <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
            <h3>The Team</h3>
            <p class="container container-vertical-padding-bottom">
                Kooness consists of a team of young talented dreamers who want to democratize the access to the art world. Our team believes in the idea of art democratization emerging from a rising generation of collectors.  With high ambitions, we wish to build respected and trustful relationships with our clients by offering the best service and transparency. We believe in the power of cultural, professional and geographic diversity. Our collaborative team features experts with a diverse range of backgrounds who work to build new ways for people to discover, encourage and appreciate art. We are not a company looking to go global. We were born global.
            </p>

            {{-- Team member 1 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/lorenzo.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Lorenzo Uggeri</h2>
                    <h4 class="main-color-txt">CEO</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                Lorenzo is the founder of Kooness and a young experienced entrepreneur with a professional background as business analyst. When looking at the art market, he quickly understood the growth opportunities of the online art trade and decided to gain a more detailed knowledge of contemporary art dynamics at Sotheby’s Institute of Art in New York. With the strong ambition of democratizing art and opening galleries’ doors to a global and new audience, he implemented Kooness business plan and melted his passion for the contemporary art world with online technologies. He has a BA in Economics and Management and a MS in Economics and Management of Innovations and Technologies from Bocconi University. Besides being one of the 30 Forbes Under 30, Lorenzo is a passionate wake boarder, an art collector and an incurable optimist. 
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/lorenzo-uggeri-54492b59/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 2 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/agnese.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Agnese Bonanno</h2>
                    <h4 class="main-color-txt">COO</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                As Chief Operating Officer at Kooness, Agnese plays a key role being responsible for the efficiency of the whole business. She is in charge of all operations and responsible for building and maintaining new and solid client relationships, with galleries, collectors and art fairs. Her deep passion for contemporary art led Agnese to join Kooness, alongside by Lorenzo, from the very beginning of his adventure. Prior to joining Kooness, she held a one year position at Sotheby’s and previous working experiences in contemporary art galleries in Milan. Agnese holds a BA and MA in Economics and Management of Art and Culture from Bocconi University with a major in Art Markets. She is an obsessed classicist, an avid skier and a meticulous young art collector. 
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/agnese-bonanno-90469667/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 3 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/rossella.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Rossella Farinotti</h2>
                    <h4 class="main-color-txt">Chief Curator</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                Rossella is a contemporary art critic, curator and writer. She is contributor to publications and magazines such as “Zero.eu”, “Flash Art Italia”, “Arte”, “Exibart”, “Sofà”, “Pagina99”. Executive coordinator of Gio’ Pomodoro’s archive (Milan) and art advisor at Sergio Rossi, in 2013, she published il Quadro che visse due volte, a book on the close relationship between art and film. From 2009 to 2011 she was the assistant of the Ministry of culture of Milan. In the last years she curated different exhibitions on young Italian artists. She has a degree in Fine Arts from the University of Milan, and an advanced degree in Communication and Organization of Contemporary Art from Accademia di Belle Arti di Brera in Milan. She lives and works between Milan and Chicago since 2014.
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/rossella-farinotti-76839042/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 4 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/elisabetta.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Elisabetta Rastelli</h2>
                    <h4 class="main-color-txt">Communication & Content Manager</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                Elisabetta is the Content Manager of Kooness with a particular attention to the magazine area as an Editor-in-Chief. Elisabetta is also the Managing Editor of THAT’S CONTEMPORARY, an online guide for contemporary art spaces in Milan. Her passion for art started through her studies in History of Art at the University of Literature and Philosophy in Parma and also thanks to the Master in Visual Cultures and Curatorial Practice at the Brera Academy in Milan. She is specialized in the Postcolonial studies but also in the field of the most recent experimentations of the contemporary art. 
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/elisabettarastelli/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 5 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/davide.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Davide Rizzi</h2>
                    <h4 class="main-color-txt">SEO Project Manager</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                Davide Rizzi is the SEO expert of Kooness and he is responsible for driving organic traffic to the site. Davide is a veteran of creating and managing SEO strategies across multiple industries. Davide education background include a master degree in international marketing management and he is also a PMI Agile Certified Practicioner. Davide doesn’t believe in magic yet, he strives to combine data and creativity when crafting a remarkable SEO strategy.
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/daviderizzi/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 6 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/default-avatar.png">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Giovanni Moratti</h2>
                    <h4 class="main-color-txt">Head of Business Development</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                {{-- xxx --}}
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/giovanni-moratti-73534b5b/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Team member 7 --}}
            <div class="container col-container-with-margin team-member">
                <div class="one-fourth-col-with-margin team-img-col">
                    <img src="/images/team/damiano.jpg">
                </div>
                <div class="three-fourth-col-with-margin team-txt-col">
                    <h2>Damiano Bauce</h2>
                    <h4 class="main-color-txt">CTO</h4>
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <p>
                                {{-- xxx --}}
                            </p>
                        </div>
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <a href="https://www.linkedin.com/in/damiano-bauce/"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Tab 3 !-->
        {{-- Disattivato in attesa dei contenuti --}}
        {{-- <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
            <div class="container col-container-with-offset-and-margin">
                <div class="col fullcol-with-margin">
                    <h2>Press</h2>
                    <p>
                            As featured in...
                    </p>
                    <div class="col-container-with-offset press-container">
                        <div class="one-fourth-col wide-box press-item">
                            <a href="#self"><img src="/images/forbes-logo.png"></a>
                        </div>
                        <div class="one-fourth-col wide-box press-item">
                            <a href="#self"><img src="/images/sky-news-logo.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- Tab 4 !-->
        <div class="tab-container four-fifths-col-with-margin tab" data-id="4">
            <div class="container col-container-with-margin">
                <div class="col full-col-with-margin">
                    <h2>Careers</h2>
                    <p>
                            We are always looking for a creative mind, do you want to join us?<br>
                            Passion, creativity and more than anything energy, are the keys to our success!
                    </p>
                    <blockquote>
                        <em class="dark-text">
                                Kooness always welcomes talented people who want to create something great while striving for excellence within the art world. We are enthusiastic to be part of a unique and innovative project in an international environment and we know that to make art accessible to anyone with an internet connection, we need people with different skills, primarily the sensibility to conceive an open environment in which ideas are shared and feedback is welcomed in a uniquely creative and forward-thinking company. So check the list of the vacant positions and email us... we are looking forward to meet you.
                        </em>
                    </blockquote>
                    <h3>CURRENT VACANCIES</h3>
                    <b>- Gallery Network Developer</b>
                    <p>
                        <u>Locations:</u> New York, London, Paris, Berlin, Milan.<br>
                        Kooness is looking for a Gallery Network Developer who is responsible for driving and closing new, global-scale Kooness partnership agreements business. The Gallery Network Developer will focus on generating new gallery prospects who might join Kooness platform. The ideal candidate has exceptional oral communications skliss, with experience in the commercial art world.
                    </p>
                    <h3>TO APPLY</h3>
                    <p>
                        Do you wanna join us? Send us your CV at <a href="mailto:info@kooness.com">info@kooness.com</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- Tab 5 !-->
        <div class="tab-container four-fifths-col-with-margin tab" data-id="5">
            {{--@dd($_GET)--}}
            <div class="container-vertical-padding-bottom">
                @if(Session::get('send') && Session::get('send') == 'success')
                    <div class="in-page-warning success">
                        Message successful sent! We will reply as soon as possible.
                    </div>
                @endif
                <h2>Send a message</h2>
                <form class="contact-form col-container-with-offset-and-margin k-form" method="POST" action="{{ route('send.contact.message') }}">
                    {!! Honeypot::generate('honey_name', 'honey_time') !!}
                    @csrf
                    <input type="hidden" name="redirect_to" value="{{ route('pages.intro', ['contact']) }}?send=success#contact-us">

                    <div class="one-half-col-with-margin">
                        <input type="text" name="name" placeholder="Name" required>
                    </div>
                    <div class="one-half-col-with-margin">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="full-col-with-margin">
                        <input type="text" name="subject" placeholder="Subject" required>
                    </div>
                    <div class="full-col-with-margin">
                        <textarea name="body" placeholder="Message" rows="6" required></textarea>
                    </div>
                    <div class="full-col-with-margin form-privacy">
                        <p>
                            <input name="privacy" type="checkbox" required> I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                        <p class="errorTxt"></p>
                        </p>
                    </div>
                    <div class="full-col-with-margin">
                        <input class="default-input-button" type="submit" value="Submit" id="contact-form-submit-button">
                    </div>
                </form>

            </div>

            <h2>Our Office</h2>
            <div id="default-sheet" class="container-vertical-padding-bottom">
                <div class="default-sheet-box">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <p>Viale Tunisia 50</p>
                        </div>
                    </div>
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <p>7th floor</p>
                        </div>
                    </div>
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <p>Milan, 20124 Italy</p>
                        </div>
                    </div>
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <p>Italy</p>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Contacts</h2>
            <div id="default-sheet" class="container-vertical-padding-bottom">
                <div class="default-sheet-box">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <p>
                                <a href="mailto:info@kooness.com">info@kooness.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="default-sheet-row">

                        <div class="icon-container">
                            <a title="Kooness Facebook" href="https://www.facebook.com/koonessofficial/">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a title="Kooness Twitter" href="https://twitter.com/KoonessOfficial">
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a title="Kooness Pinterest" href="https://it.pinterest.com/kooness/">
                                <i class="fa fa-pinterest"></i>
                            </a>
                            <a title="Kooness Tumblr" href="https://www.instagram.com/koonessofficial/">
                                <i class="fa fa-tumblr"></i>
                            </a>
                            <a title="Kooness Mail" href="mailto:info@kooness.com">
                                <i class="fa fa-envelope"></i>
                            </a>
                        </div>
                
                    </div>
                </div>
            </div>
        </div>
        <!-- Tab 6 !-->
        <div class="tab-container four-fifths-col-with-margin tab no-display" data-id="6">
            <div class="container col-container-with-margin">
                <div class="col full-col-with-margin">
                    <h2>Special Projects</h2>
                    <p>
                        Coming Soon!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
