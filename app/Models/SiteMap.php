<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteMap extends Model
{
    protected $guarded = [];

    protected $table = 'sitemap';

}
