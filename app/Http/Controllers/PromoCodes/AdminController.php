<?php

namespace App\Http\Controllers\PromoCodes;

use App\Http\Controllers\Controller;
use App\Models\PromoCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('promo-codes.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'value',
            3 => 'status',
            4 => 'created_at',
            5 => 'options',
        );

        $totalData = PromoCode::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $codes = PromoCode::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $codes = PromoCode::where('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = PromoCode::where('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($codes)) {
            foreach ($codes as $code) {
                $edit = route('admin.promo-codes.edit', $code->id);
                $delete = route('admin.promo-codes.destroy', $code->id);

                $nestedData['id'] = $code->id;
                $nestedData['name'] = $code->name;
                $nestedData['value'] = $code->value . "%";
                $nestedData['status'] = get_status_promo_code($code->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($code->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $code = new PromoCode();
        return view('promo-codes.admin.create', compact('code'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $code = PromoCode::create(Input::all());
            if ($code) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.promo-codes.index')
                ];
                $request->session()->flash("success", "Promo Code with name `{$code->name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($code)
    {
        $code = PromoCode::find($code);
        return view('promo-codes.admin.edit', compact('code'));
    }

    public function update(Request $request, $code)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $code = PromoCode::find($code);
        if ($request->method() == 'PATCH' && $code) {
            if ($code->update(Input::all())) {
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.promo-codes.index')
                ];
                $request->session()->flash("success", "Promo Code with name `{$code->name}` is successful saved!");
            }
        }

        return response()->json($response);
    }

    public
    function destroy($code)
    {
        $code = PromoCode::find($code);
        $code->delete();
        // redirect
        Session::flash('success', "Promo Code with name `{$code->name}` is successful removed!");
        return redirect(route('admin.promo-codes.index'));
    }
}
