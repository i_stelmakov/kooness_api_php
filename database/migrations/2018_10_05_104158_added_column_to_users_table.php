<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('renewal_failed_date')->nullable()->after('premium_exp_date');
            $table->integer('renewal_failed_payment_id')->unsigned()->nullable()->after('renewal_failed_date');

            $table->foreign('renewal_failed_payment_id')
                ->references('id')
                ->on('mbr_payments')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['renewal_failed_payment_id']);

            $table->dropColumn('renewal_failed_date');
            $table->dropColumn('renewal_failed_payment_id');
        });
    }
}
