<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class FrontendController extends FrontendExtendedController
{
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function single(Request $request, $slug_name, $type = null)
    {
        $tag = Tag::whereSlug($slug_name)->first();
        $result = [
            'artworks' => [],
            'artists' => [],
            'galleries' => [],
            'magazine' => [],
            'news' => [],
        ];
        $limit = null;
        $hide_load_more = false;
        if ($tag) {
            if(!$type || $type == 'artworks'){
                $limit = 4;
                if($type){
                    $limit = 21;
                }
                $rows = $tag->artworks()->limit($limit)->orderBy("created_at", "DESC")->get();
                if(count($rows) <= 20){
                    $hide_load_more = true;
                } else{
                    $rows->pop();
                }
                $result['artworks'] = $rows;
            }
            if(!$type || $type == 'artists'){
                $limit = 4;
                if($type){
                    $limit = 21;
                }
                $rows = $tag->artists()->limit($limit)->orderBy("created_at", "DESC")->get();
                if(count($rows) <= 20){
                    $hide_load_more = true;
                } else{
                    $rows->pop();
                }
                $result['artists'] = $rows;
            }
            if(!$type || $type == 'galleries'){
                $limit = 3;
                if($type){
                    $limit = 19;
                }
                $rows = $tag->galleries()->limit($limit)->orderBy("created_at", "DESC")->get();
                if(count($rows) <= 18){
                    $hide_load_more = true;
                } else{
                    $rows->pop();
                }
                $result['galleries'] = $rows;
            }
            if(!$type || $type == 'magazine'){
                $limit = 3;
                if($type){
                    $limit = 19;
                }
                $rows = $tag->magazine()->limit($limit)->orderBy("created_at", "DESC")->get();
                if(count($rows) <= 18){
                    $hide_load_more = true;
                } else{
                    $rows->pop();
                }
                $result['magazine'] = $rows;
            }
            if(!$type || $type == 'news'){
                $limit = 3;
                if($type){
                    $limit = 19;
                }
                $rows = $tag->news()->limit($limit)->orderBy("created_at", "DESC")->get();
                if(count($rows) <= 18){
                    $hide_load_more = true;
                } else{
                    $rows->pop();
                }
                $result['news'] = $rows;
            }
            return view('tags.frontend.single', compact('tag', 'result', 'type', 'limit', 'hide_load_more'));
        } else {
            abort(404);
        }
    }

    public function loadMore(Request $request, $section)
    {
        if ($request->ajax()) {
            $return = [];

            $offset = $request->get('offset');
            $tagged = $request->get('tagged');
            $tag = Tag::find($tagged);
            $hide_load_more = false;
            if($tag){
                if($section == 'artworks'){
                    $artworks = $tag->artworks()->offset($offset)->limit(21)->orderBy('created_at', "DESC")->get();
                    if(count($artworks) <= 20){
                        $hide_load_more = true;
                    } else{
                        $artworks->pop();
                    }
                    foreach ($artworks as $key => $artwork) {
                        $artist = $artwork->artist()->first();
                        $artist_name = null;
                        $artist_link = null;
                        if($artist){
                            $artist_name = $artist->first_name . " " . $artist->last_name;
                            $artist_link = route('artists.single', [$artist->slug]);
                        }
                        $medium = $artwork->categories()->where("is_medium", "=", "1")->get();
                        $return_medium = [];
                        foreach ($medium as $media) {
                            $return_medium[] = [
                                "name" => $media->name,
                                "url" => route('artworks.single', [$media->slug])
                            ];
                        }
                        $price = false;
                        if(!$artwork->available_in_fair || $artwork->show_price){
                            $price = $artwork->pretty_price;
                        }

                        $return[] = [
                            "id" => $artwork->id,
                            "name" => $artwork->title,
                            "image" => $artwork->url_square_box,
                            "url" => route('artworks.single', [$artwork->slug]),
                            "artist_name" => $artist_name,
                            "artist_url" => $artist_link,
                            "measure_cm" => $artwork->measure_cm,
                            "medium" => $return_medium,
                            "price" => $price,
                            "auth_user" => (Auth::user()) ? true : false,
                            "follow_status" => (Auth::user() && Auth::user()->collectionArtworks->contains($artwork->id)) ? true : false
                        ];
                    }
                } else if($section == 'artists'){
                    $artists = $tag->artists()->offset($offset)->limit(21)->orderBy('created_at', "DESC")->get();
                    if(count($artists) <= 20){
                        $hide_load_more = true;
                    } else{
                        $artists->pop();
                    }
                    foreach ($artists as $key => $artist) {
                        $return[] = [
                            "id" => $artist->id,
                            "name" => $artist->first_name . " " . $artist->last_name,
                            "url" => route('artists.single', [$artist->slug]),
                            "image" => $artist->url_square_box,
                            "count" => $artist->artworks()->count(),
                            "country" => ($artist->countryBirth()->count())? $artist->countryBirth()->first()->name : null,
                            "auth_user" => (Auth::user()) ? true : false,
                            "follow_status" => (Auth::user() && Auth::user()->collectionArtists->contains($artist->id)) ? true : false
                        ];
                    }
                } else if($section == 'galleries'){
                    $galleries = $tag->galleries()->offset($offset)->limit(19)->orderBy('created_at', "DESC")->get();
                    if(count($galleries) <= 18){
                        $hide_load_more = true;
                    } else{
                        $galleries->pop();
                    }
                    foreach ($galleries as $key => $gallery) {
                        $return[] = [
                            "id" => $gallery->id,
                            "name" => $gallery->name,
                            "url" => route('galleries.single', [$gallery->slug]),
                            "image" => $gallery->url_wide_box,
                            "city" => $gallery->city,
                            "address" => $gallery->address,
                            "auth_user" => (Auth::user()) ? true : false,
                            "follow_status" => (Auth::user() && Auth::user()->collectionGalleries->contains($gallery->id)) ? true : false
                        ];
                    }
                } else if($section == 'magazine'){
                    $posts = $tag->magazine()->offset($offset)->limit(19)->orderBy('date', "DESC")->get();
                    if(count($posts) <= 18){
                        $hide_load_more = true;
                    } else{
                        $posts->pop();
                    }
                    foreach ($posts as $key => $post) {
                        $return[] = [
                            "id" => $post->id,
                            "name" => $post->title,
                            "url" => route('posts.single', [$section, $post->slug]),
                            "image" => $post->url_wide_box,
                            "date" => date('d F Y', strtotime($post->date)),
                            "content" => ($section == 'news') ? '' : '',
                        ];
                    }
                } else if($section == 'news'){
                    $posts = $tag->news()->offset($offset)->limit(19)->orderBy('date', "DESC")->get();
                    if(count($posts) <= 18){
                        $hide_load_more = true;
                    } else{
                        $posts->pop();
                    }
                    foreach ($posts as $key => $post) {
                        $return[] = [
                            "id" => $post->id,
                            "name" => $post->title,
                            "url" => route('posts.single', [$section, $post->slug]),
                            "image" => $post->url_wide_box,
                            "date" => date('d F Y', strtotime($post->date)),
                            "content" => ($section == 'news') ? '' : '',
                            "auth_user" => (Auth::user()) ? true : false,
                            "follow_status" => (Auth::user() && Auth::user()->collectionGalleries->contains($post->id)) ? true : false
                        ];
                    }
                }
            }

            return response()->json([
                "result" => "success",
                "item_list" => $return,
                "hide_load_more" => $hide_load_more
            ]);
        }
    }
}
