<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('slug')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->string('url_cover')->nullable();
            $table->text('about_the_artist')->nullable();
            $table->text('meet_the_artist')->nullable();
            $table->text('bibliography')->nullable();
            $table->text('awards')->nullable();
            $table->year('date_of_birth')->nullable();
            $table->year('date_of_death')->nullable();
            $table->string('city_of_birth')->nullable();
            $table->integer('country_of_birth')->unsigned()->nullable();
            $table->string('current_location')->nullable();
            $table->boolean('featured')->default(0);
            $table->boolean('status')->default(0);

            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();

            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table->foreign('country_of_birth')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');
    }
}
