<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdToArtworksCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->after('id')->nullable();

            $table->foreign('parent_id')
                ->references('id')
                ->on('artworks_categories')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->removeColumn('parent_id');
        });
    }
}
