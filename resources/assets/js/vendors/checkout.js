/* ==========================================================================
Specifica pagina Checkout
========================================================================== */
window.addEventListener("load", function () {

    // Variables
    var $ = jQuery;

    if ($('body').hasClass('checkout')) {

        // For every form call SendData on form submit

        // Ajax send data from forms:
        function sendData(event) {

            // click event
            var thisEvent = event;
            var thisForm = event.target; // target => the form where start click event

            // Form ID
            var thisFormID = thisForm.id;

            // Form Data-kind
            var thisFormDataKind = thisForm.dataset.kind;

            // Form Data-id
            var thisFormDataID = thisForm.dataset.id;

            // create a new associative array for contain data
            var data = {};

            // if form is for cards
            if (thisFormDataKind === 'cards-editing') {
                console.log('cards-editing');
                console.log('form id => ' + thisFormDataID);

                // Retrieve id if edit, or empty if new
                if (thisFormDataID) {
                    data.id = thisFormDataID;
                }

                // Retrieve form inputs values for Cards
                data.owner = event.target.querySelector('input[name="card_owner"]').value;
                // console.log( '🔵' + form_data_id );

                data.cc = event.target.querySelector('input[name="card_number"]').value;
                data.month = event.target.querySelector('select[name="card_exp_month"]').value;
                data.year = event.target.querySelector('select[name="card_exp_year"]').value;
                data.cvv = event.target.querySelector('input[name="card_cvv"]').value;
                data.type = event.target.querySelector('input[name="card_type"]').value;

                $.ajax({
                    url: '/user/ajax/save/payment',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            var $form = $('#cardForm0');
                            $form.parents('.accordion').toggle("active");
                            $form.parents('.panel').css("maxHeight", "0");

                            var $template = $('<div class="card-item" data-id="' + response.payment.id + '"><hr><div class="card-text"></div></div>');
                            $template.find('.card-text').append('<img src="/images/credit_cards/light/' + response.payment.type.toLowerCase() + '.png">');
                            $template.find('.card-text').append('<p>' + response.payment.type + '- end in ***' + response.payment.end_with + '</p></div>')
                            var $delete = $('<a class="delete-card"><i class="fa fa-edit"></i> Delete Card</a>');
                            $delete.deletePayment();
                            $template.find('.card-text').append($delete)

                            if ($('div[data-remodal-id="modal-cards"]').find('.card-item').length)
                                $template.insertAfter($('div[data-remodal-id="modal-cards"]').find('.card-item:last'));
                            else
                                $template.insertAfter($('div[data-remodal-id="modal-cards"]').find('.panel'));

                            var $radio = $('<div class="target-card"><input id="card-' + response.payment.id + '" type="radio" class="card" name="payment_method" value="' + response.payment.id + '"><label for="card-' + response.payment.id + '" style="background-image: url(\'/images/credit_cards/light/' + response.payment.type.toLowerCase() + '.png\')"> ' + response.payment.type + ' - end in ***' + response.payment.end_with + '</label><br></div>')
                            if ($('.form-validate').find('.target-card').length) {
                                $radio.insertAfter($('.form-validate').find('.target-card:last'));
                            } else {
                                $radio.insertAfter($('.form-validate').find('#credit-cards p'));
                            }
                        }
                    }
                });
            }

            // if form is for addresses
            if (thisFormDataKind === 'addresses-editing') {

                // Retrieve id if edit, or empty if new
                if (thisFormDataID) {
                    data.id = thisFormDataID;
                }

                // Retrieve form inputs values for Addresses
                data.type = event.target.querySelector('select[name="type"]').value;
                data.first_name = event.target.querySelector('input[name="first_name"]').value;
                data.last_name = event.target.querySelector('input[name="last_name"]').value;
                data.company_name = event.target.querySelector('input[name="company_name"]').value;
                data.fiscal_code = event.target.querySelector('input[name="fiscal_code"]').value;
                data.vat_number = event.target.querySelector('input[name="vat_number"]').value;
                data.email = event.target.querySelector('input[name="email"]').value;
                data.phone = event.target.querySelector('input[name="phone"]').value;
                data.address_1 = event.target.querySelector('input[name="address"]').value + ", " + event.target.querySelector('input[name="number"]').value;
                data.city = event.target.querySelector('input[name="city"]').value;
                data.state = event.target.querySelector('input[name="state"]').value;
                data.zip = event.target.querySelector('input[name="zip_code"]').value;
                data.country = event.target.querySelector('input[name="country"]').value;
                data.country_code = event.target.querySelector('input[name="country_code"]').value;

                $.ajax({
                    url: '/user/ajax/save/address',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            if (response.action === 'new') {
                                var $form = $('#addressForm0');
                                $form.parents('.accordion').toggle("active");
                                $form.parents('.panel').css("maxHeight", "0");

                                var $row = $(' <div class="address-item"><hr><div class="address-text">' + response.address.label + '</div><a class="accordion" href="#"><i class="fa fa-edit"></i> Edit Address</a><div class="panel"></div></div>');

                                var $template = $form.clone();
                                $template.parents('.accordion').toggle("active");
                                $template.parents('.panel').css("maxHeight", "0");
                                $template.attr("data-id", response.address.id);
                                $template.attr("id", "addressForm" + response.address.id);
                                $.each(response.address, function (index, value) {
                                    if (index === 'label') {
                                        var $input = $template.find('#autocomplete-0');
                                        $input.attr("id", "autocomplete-" + response.address.id);
                                        $input.attr("name", "autocomplete-" + response.address.id);
                                        $input.initAutocomplete();
                                        $input.val(value);
                                    }
                                    if ($template.find('input[name=' + index + ']').length) {
                                        $template.find('input[name=' + index + ']').val(value);
                                    }
                                });

                                var $delete = $('<button class="small-input-button delete-address">Delete Address</button>');
                                $delete.deleteAddress();
                                $template.append($delete);

                                $template.find('input[type=submit]').attr('value', 'Save Address');

                                $template.validate({
                                    submitHandler: function (form, event) {
                                        sendData(event);
                                    }
                                });

                                $row.find('.panel').append($template);
                                $row.find('.accordion')[0].addEventListener("click", function () {
                                    this.classList.toggle("active");
                                    var panel = this.nextElementSibling;
                                    if (panel.style.maxHeight === "2000px") {
                                        panel.style.maxHeight = "0px";
                                    } else {
                                        panel.style.maxHeight = "2000px";
                                    }
                                });
                                if ($('div[data-remodal-id="modal-address"]').find('.address-item').length)
                                    $row.insertAfter($('div[data-remodal-id="modal-address"]').find('.address-item:last'));
                                else
                                    $row.insertAfter($('div[data-remodal-id="modal-address"]').find('.panel'));

                                var $select = $('.form-validate').find('select.address-select');
                                if ($select.find('option[value=""]').length) {
                                    $select.empty();
                                }

                                $select.append('<option value="' + response.address.id + '">' + response.address.label + '</option>')
                                $select.trigger('change');

                            } else {
                                var $form = $('#addressForm' + response.address.id);
                                $form.parents('.accordion').toggle("active");
                                $form.parents('.panel').css("maxHeight", "0");
                                $form.parents('.address-item').find('.address-text').text(response.address.label);
                                var $select = $('.form-validate').find('select.address-select');
                                $select.find('option[value="' + response.address.id + '"]').text(response.address.label)
                            }
                        }
                    }
                }); // END - Ajax
            }
        }

        // Accordion
        var acc = document.getElementsByClassName("accordion");
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight === "2000px") {
                    panel.style.maxHeight = "0px";
                } else {
                    panel.style.maxHeight = "2000px";
                }
            });
        }

        $('form[data-kind="cards-editing"]').each(function () {
            var $cardNumber = $(this).find('input[name=card_number]');
            var $cardType = $(this).find('input[name=card_type]');
            $cardNumber.payform('formatCardNumber');
            $cardNumber.keyup(function () {
                var cardType = $.payform.parseCardType($cardNumber.val());
                if (cardType && $.inArray(cardType, ['visa', 'mastercard', 'amex', 'visaelectron', 'maestro']) > -1) {
                    $cardType.val(cardType.charAt(0).toUpperCase() + cardType.slice(1));
                } else {
                    $cardType.val('')
                }
            });
            $(this).find('input[name=card_cvv]').payform('formatCardCVC');
        });

        jQuery.validator.addMethod("CCExp", function (value, element, params) {
            var minMonth = new Date().getMonth() + 1;
            var minYear = new Date().getFullYear();

            var formMonth = $(element).parents('form').find('select[name="card_exp_month"]');
            var formYear = $(element).parents('form').find('select[name="card_exp_year"]');

            var month = parseInt(formMonth.val(), 10);
            var year = parseInt(formYear.val(), 10);

            if ((year > minYear) || ((year === minYear) && (month >= minMonth))) {
                return true;
            } else {
                return false;
            }
        }, "Your Credit Card Expiration date is invalid.");

        jQuery.validator.addClassRules('address-autocomplete', {
            required: true,
            address: true
        });

        jQuery.validator.addClassRules('card-number', {
            required: true,
            creditcard: true
        });
        jQuery.validator.addMethod("selectCheck", function (value, element) {
            return value !== '';
        });

        jQuery.validator.addMethod("address", function (value, element) {
                var address = $(element).parent().find('.address').val();
                var number = $(element).parent().find('.number').val();
                var city = $(element).parent().find('.city').val();
                var country = $(element).parent().find('.country').val();
                var country_code = $(element).parent().find('.country_code').val();
                var zip = $(element).parent().find('.zip').val();
                if (address !== '' && number !== '' && city !== '' && country !== '' && zip !== '' && country_code !== '') {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter a valid address."
        );

        jQuery.fn.deleteAddress = function () {
            $(this).click(function (e) {
                var id = $(this).parents('form').data('id');
                var element = $(this).parents('.address-item');
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '/user/ajax/delete/address',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        var $select = $('.form-validate').find('select.address-select');
                        if (response.status === 'success') {
                            element.remove();
                            console.log('element removed!');

                            $select.find('option[value="' + id + '"]').remove();
                            if (!$select.find('option').length) {
                                $select.append('<option value="">No address found. Add new one.</option>')
                            }
                            $select.trigger('change');
                        }
                    }
                });
            })
        };

        jQuery.fn.deletePayment = function () {
            $(this).click(function (e) {
                var id = $(this).parents('.card-item').data('id');
                var element = $(this).parents('.card-item');
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: '/user/ajax/delete/payment',
                    type: 'POST',
                    dataType: 'json',
                    data: {id: id},
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            element.remove();
                            $('.form-validate').find('.card[value="' + id + '"]').parents('.target-card').remove();
                        }
                    }
                });
            })
        };

        $('body.checkout').on('change', 'select[name=type]', function () {
            var $container = $(this).closest('form');
            if($(this).val() === 'private'){
                $container.find('.for-company').hide();
                $container.find('.for-company>input').attr("disabled", true);
                $container.find('.for-private').show();
                $container.find('.for-private>input').attr("disabled", false);
            } else{
                $container.find('.for-private').hide();
                $container.find('.for-private>input').attr("disabled", true);
                $container.find('.for-company').show();
                $container.find('.for-company>input').attr("disabled", false);
            }
        });

        $(".add-edit-form").each(function (index, this_form) {
            // ...and take over its submit event.
            $(this_form).validate({
                rules: {
                    card_exp_month: {
                        CCExp: true
                    },
                    card_exp_year: {
                        CCExp: true
                    }
                },
                submitHandler: function (form, event) {
                    event.preventDefault();
                    sendData(event);
                }
            });
        });

        $('.delete-address').each(function () {
            $(this).deleteAddress();
        });

        $('.delete-card').each(function () {
            $(this).deletePayment();
        });

        $('.form-validate').validate({
            rules: {
                'shipping-address': {
                    required: true,
                    selectCheck: true
                },
                'billing-address': {
                    required: true,
                    selectCheck: true
                }
            },
            submitHandler: function (form) {
                hidePage(); // nascondo preventivamente la pagina per evitare un doppio invio del form di checkout
                form.submit();
            },

            errorPlacement: function(error, element) {
                var elem = $(element);
                var acceptanceErrorBox = $('#accept-error');
                var paymentErrorBox = $('#payment-error');

                if (elem.attr('id') == "privacy_policy") {
                    acceptanceErrorBox.html(error);
                }
                else if (elem.attr('name') == "payment_method") {
                    paymentErrorBox.html(error);
                }
                else {
                    error.insertAfter(element);
                }
            }


        });

        $('.add-promo-code').unbind('click').click(function () {
            if ($('input.promo-code').val()) {
                $.ajax({
                    url: '/user/ajax/promo/code',
                    type: 'POST',
                    dataType: 'json',
                    data: {code: $('input.promo-code').val()},
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        if (response.status === 'success') {
                            $('.cart-subtotal').find('p').text(response.amount);
                            var delivery = parseFloat($('.delivery-result').data('amount'));
                            var new_total = parseFloat(response.amount) + delivery;
                            var $total = $('.cart-total');
                            $total.find('strong').text(new_total.numberFormat(2, ',','') + ' €');
                            $total.data('total', parseFloat(response.amount))
                            var $rowCode = $('.row-code');
                            $('.row-insert-code').hide();
                            $rowCode.find('p').text(response.coupon.name + ": " + response.coupon.value + "%")
                            $rowCode.show();
                        }
                    }
                });
            }
        });

        $('.remove-promo-code').unbind('click').click(function () {
            $.ajax({
                url: '/user/ajax/promo/code/remove',
                type: 'POST',
                dataType: 'json',
                data: {code: $('input.promo-code').val()},
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        $('.cart-subtotal').find('p').text(response.amount);
                        var delivery = parseFloat($('.delivery-result').data('amount'));
                        var new_total = parseFloat(response.amount) + delivery;
                        var $total = $('.cart-total');
                        $total.find('strong').text(new_total.numberFormat(2, ',','') + ' €');
                        $total.data('total', parseFloat(response.amount))
                        var $rowCode = $('.row-code');
                        var $rowInsert = $('.row-insert-code');
                        $rowCode.hide();
                        $rowCode.find('p').text('');
                        $rowInsert.find('input').val('');
                        $rowInsert.show()
                    }
                }
            });
        });

        var $container = $('.delivery-result');
        var $total = $('.cart-total');
        $('#shipping-address').on('change', function () {
            var that = $(this);
            $('button[type=submit]').attr("disabled", true);
            if (that.val() !== "") {
                $('.cart-payment .in-page-warning.error.error-shipping-address').fadeOut();
                $container.find('p').html($('<span><i class="fa fa fa-spinner fa-spin"></i> Calculating Shippings</span>'));
                $('.cart-payment .in-page-warning.error.error-dhl').fadeOut();
                $.ajax({
                    url: 'get/shipping/rate',
                    type: 'POST',
                    dataType: 'json',
                    data: {address: that.val()},
                    complete: function (response) {
                        // console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        // se il calcolo delle spese avviene con successo
                        if (response.status === "success") {
                            // passo valore delle shipping calcolate da DHL in un un data-attribute
                            $container.data("amount", response.tax_amount);
                            // se è settato spese di spedizione gratuite a livello globale
                            if(response.is_free) {
                                // popolo contenitore spese delivery
                                $container.find('p').text('free shipping');
                            } else {
                                // popolo contenitore spese delivery
                                $container.find('p').text(response.tax_amount.numberFormat(2, ',','') + ' €');
                                // salvo in una variabile il valore recuperato
                                var amount = parseFloat($total.data('total'));
                                // sommo il totale alle spese di spedizione (amount +=)
                                amount += parseFloat(response.tax_amount);
                                // scrivo il valore nel box del totale
                                $total.find('strong').text(amount.numberFormat(2, ',','') + ' €');
                            }
                            // riabilito il pulsante del checkout
                            $('button[type=submit]').attr("disabled", false);
                        }
                        // se le spese non riescono ad essere calcolate
                        else {
                            $container.find('p').html($('<span class="error">Shipping cost cannot be calculated.</span>'));
                            $('.cart-payment .in-page-warning.error.error-dhl').fadeIn();
                            $('.payment-box').hide().find('input').attr("disabled", true);
                            $('button[type=submit]').attr("disabled", false);
                        }
                    }
                });
            } else {
                $('.cart-payment .in-page-warning.error.error-dhl').fadeOut();
                $('.cart-payment .in-page-warning.error.error-shipping-address').fadeIn();
                $container.find('p').html($('<span class="error">Shipping cost cannot be calculated.</span>'));
                $('button[type=submit]').attr("disabled", true);
            }
        })

        // verifico la select degli indirizzi non sia vuota
        if( !$('#shipping-address').val() ) {
            console.log('Shipping address list is empty');
            $('.cart-payment .in-page-warning.error.error-shipping-address').fadeIn();
            $container.find('p').html($('<span class="error">Shipping cost cannot be calculated.</span>'));
            if($('input[name="order_token"]').length <= 0) {
                $('button[type=submit]').attr("disabled", true);
            }
        } else {
            console.log('Shipping address list has some address');
            if($('input[name="order_token"]').length <= 0){
                $('#shipping-address').trigger('change');
            }
        }

    }
}); // END - jQuery window load
