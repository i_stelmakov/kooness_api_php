<li class="item-link" data-repeater-item>
    <i class="fa fa-arrows"></i>
    <i class="fa fa-times pull-right text-danger" data-repeater-delete style="cursor: pointer"></i>
    <input type="hidden" name="id" value="{{ $link->id }}">
    <input class="order" type="hidden" name="order" value="{{ ($link->{'order'}) ? $link->{'order'} : 0 }}">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="title_link">Title (*)<small> => max 100 characters</small></label>
                <input id="title_link" maxlength="100" class="form-control" type="text" placeholder="Title" name="title" value="{{ $link->{'title'} }}" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="link_link">Link (*)</label>
                <input id="link_link" class="form-control" type="text" placeholder="Link" name="link" value="{{ $link->{'link'} }}" required>
            </div>
        </div>
    </div>
</li>