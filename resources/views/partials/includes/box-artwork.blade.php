<?php
    $artist_name = null;
    $artist_link = null;

    $artist = $item->artist()->first();
    if($artist){
        $artist_name = $artist->first_name . " " . $artist->last_name;
        $artist_link = route('artists.single', [$artist->slug]);
    }
?>
<div class="slider-item-txt">
    <div class="default-sheet-row slider-item-row artist-name">
        <div class="default-sheet-row-cell">
            <h3><a title="{{ $artist_name }}" class="dark-text" href="{{ $artist_link }}">{{ $artist_name }}</a></h3>
        </div>
    </div>
    <h2><a class="dark-text" href="{{ route('artworks.single', [$item->slug]) }}">{{ $item->title }}</a></h2>
    <p class="artwork-measures">{{ $item->measure_cm }} cm</p>
    <div class="default-sheet-row slider-item-row">
        <div class="default-sheet-row-cell">
            <p class="artwork-type">
                @foreach($item->categories()->where("is_medium", "=", "1")->get() as $medium)
                    <a title="{{ $medium->name }}" href="{{ route('artworks.single', [$medium->slug]) }}">{{ $medium->name }}</a>
                @endforeach
            </p>
        </div>
        {{-- SE artwork non in fiera OPPURE artwork impone visualizzazione prezzo --}}
        @if(!$item->available_in_fair || $item->show_price)
            <div class="default-sheet-row-cell">
                {{-- SE artwork segnato come disponibile E artwork non è in nessuna fiera --}}
                @if( $item->status == 1 && !$item->available_in_fair)
                    <p class="dark-text artwork-price">{{ $item->pretty_price }}</p>
                @else
                    <p class="dark-text artwork-price">SOLD OUT</p>
                @endif
            </div>
        @endif
    </div>
</div>