@extends('layouts.app')

@section('title', ($post && $post->meta_title) ? $post->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$post->name}"))

@section('body-class', 'magazine-single')

@section('header-og-abstract', ($post && $post->meta_description) ? $post->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($post && $post->canonical) ? $post->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($post && $post->meta_keywords) ? $post->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($post->url_full_image) ? $post->url_full_image : null)

@section('header-og-url', Request::url())

@section('scripts')
    <script type="text/javascript" src="/js/modernizr.js"></script>
    {{--<script type="text/javascript" src="/js/jquery.mobile-1.5.0-alpha.1.min.js"></script>--}}
    <script type="text/javascript" src="/js/timeline-main.js"></script>
@endsection

@section('styles')
    <style>

        /* --------------------------------

        Horizontal Timeline - Typography

        -------------------------------- */

        /*!* cyrillic-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjEYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;*/
        /*}*/
        /*!* cyrillic *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjNYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;*/
        /*}*/
        /*!* greek-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjFYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+1F00-1FFF;*/
        /*}*/
        /*!* greek *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjKYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+0370-03FF;*/
        /*}*/
        /*!* vietnamese *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjGYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;*/
        /*}*/
        /*!* latin-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjHYTLVdlTOr0s.woff2) format('woff2');*/
            /*unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;*/
        /*}*/
        /*!* latin *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: italic;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Italic'), local('FiraSans-Italic'), url(https://fonts.gstatic.com/s/firasans/v8/va9C4kDNxMZdWfMOD5VvkrjJYTLVdlTO.woff2) format('woff2');*/
            /*unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;*/
        /*}*/
        /*!* cyrillic-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5VvmojLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;*/
        /*}*/
        /*!* cyrillic *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5Vvk4jLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;*/
        /*}*/
        /*!* greek-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5Vvm4jLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+1F00-1FFF;*/
        /*}*/
        /*!* greek *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5VvlIjLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+0370-03FF;*/
        /*}*/
        /*!* vietnamese *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5VvmIjLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;*/
        /*}*/
        /*!* latin-ext *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5VvmYjLazX3dGTP.woff2) format('woff2');*/
            /*unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;*/
        /*}*/
        /*!* latin *!*/
        /*@font-face {*/
            /*font-family: 'Fira Sans';*/
            /*font-style: normal;*/
            /*font-weight: 400;*/
            /*src: local('Fira Sans Regular'), local('FiraSans-Regular'), url(https://fonts.gstatic.com/s/firasans/v8/va9E4kDNxMZdWfMOD5Vvl4jLazX3dA.woff2) format('woff2');*/
            /*unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;*/
        /*}*/
        /*!* cyrillic *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 700;*/
            /*src: local('Playfair Display Bold'), local('PlayfairDisplay-Bold'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWoe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;*/
        /*}*/
        /*!* vietnamese *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 700;*/
            /*src: local('Playfair Display Bold'), local('PlayfairDisplay-Bold'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWEe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;*/
        /*}*/
        /*!* latin-ext *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 700;*/
            /*src: local('Playfair Display Bold'), local('PlayfairDisplay-Bold'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWAe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;*/
        /*}*/
        /*!* latin *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 700;*/
            /*src: local('Playfair Display Bold'), local('PlayfairDisplay-Bold'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lW4e5j5hNKc.woff2) format('woff2');*/
            /*unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;*/
        /*}*/
        /*!* cyrillic *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 900;*/
            /*src: local('Playfair Display Black'), local('PlayfairDisplay-Black'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWoe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;*/
        /*}*/
        /*!* vietnamese *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 900;*/
            /*src: local('Playfair Display Black'), local('PlayfairDisplay-Black'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWEe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;*/
        /*}*/
        /*!* latin-ext *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 900;*/
            /*src: local('Playfair Display Black'), local('PlayfairDisplay-Black'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWAe5j5hNKe1_w.woff2) format('woff2');*/
            /*unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;*/
        /*}*/
        /*!* latin *!*/
        /*@font-face {*/
            /*font-family: 'Playfair Display';*/
            /*font-style: normal;*/
            /*font-weight: 900;*/
            /*src: local('Playfair Display Black'), local('PlayfairDisplay-Black'), url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lW4e5j5hNKc.woff2) format('woff2');*/
            /*unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;*/
        /*}*/



        /* --------------------------------

        Primary style

        -------------------------------- */
        *, *::after, *::before {
            box-sizing: border-box;
        }

        /*html {*/
            /*font-size: 62.5%;*/
        /*}*/

        /*body {*/
            /*font-size: 1.6rem;*/
            /*font-family: "Fira Sans", sans-serif;*/
            /*color: #383838;*/
            /*background-color: #f8f8f8;*/
        /*}*/

        /*a {*/
            /*color: #7b9d6f;*/
            /*text-decoration: none;*/
        /*}*/

        /* --------------------------------

        Horizontal Timeline - Main Components

        -------------------------------- */
        .cd-horizontal-timeline {
            opacity: 0;
            margin: 2em auto;
            -webkit-transition: opacity 0.2s;
            -moz-transition: opacity 0.2s;
            transition: opacity 0.2s;
        }
        .cd-horizontal-timeline::before {
            /* never visible - this is used in jQuery to check the current MQ */
            content: 'mobile';
            display: none;
        }
        .cd-horizontal-timeline.loaded {
            /* show the timeline after events position has been set (using JavaScript) */
            opacity: 1;
        }
        .cd-horizontal-timeline .timeline {
            position: relative;
            height: 100px;
            width: 90%;
            max-width: 800px;
            margin: 0 auto;
        }
        .cd-horizontal-timeline .events-wrapper {
            position: relative;
            height: 100%;
            margin: 0 40px;
            overflow: hidden;
        }
        .cd-horizontal-timeline .events-wrapper::after, .cd-horizontal-timeline .events-wrapper::before {
            /* these are used to create a shadow effect at the sides of the timeline */
            content: '';
            position: absolute;
            z-index: 2;
            top: 0;
            height: 100%;
            width: 20px;
        }
        .cd-horizontal-timeline .events-wrapper::before {
            left: 0;
            background-image: -webkit-linear-gradient( left , #f8f8f8, rgba(248, 248, 248, 0));
            background-image: linear-gradient(to right, #f8f8f8, rgba(248, 248, 248, 0));
        }
        .cd-horizontal-timeline .events-wrapper::after {
            right: 0;
            background-image: -webkit-linear-gradient( right , #f8f8f8, rgba(248, 248, 248, 0));
            background-image: linear-gradient(to left, #f8f8f8, rgba(248, 248, 248, 0));
        }
        .cd-horizontal-timeline .events {
            /* this is the grey line/timeline */
            position: absolute;
            z-index: 1;
            left: 0;
            top: 49px;
            height: 2px;
            /* width will be set using JavaScript */
            background: #dfdfdf;
            -webkit-transition: -webkit-transform 0.4s;
            -moz-transition: -moz-transform 0.4s;
            transition: transform 0.4s;
        }
        .cd-horizontal-timeline .filling-line {
            /* this is used to create the green line filling the timeline */
            position: absolute;
            z-index: 1;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            background-color: #7b9d6f;
            -webkit-transform: scaleX(0);
            -moz-transform: scaleX(0);
            -ms-transform: scaleX(0);
            -o-transform: scaleX(0);
            transform: scaleX(0);
            -webkit-transform-origin: left center;
            -moz-transform-origin: left center;
            -ms-transform-origin: left center;
            -o-transform-origin: left center;
            transform-origin: left center;
            -webkit-transition: -webkit-transform 0.3s;
            -moz-transition: -moz-transform 0.3s;
            transition: transform 0.3s;
        }
        .cd-horizontal-timeline .events a {
            position: absolute;
            bottom: 0;
            z-index: 2;
            text-align: center;
            font-size: 1.3rem;
            padding-bottom: 15px;
            color: #383838;
            /* fix bug on Safari - text flickering while timeline translates */
            -webkit-transform: translateZ(0);
            -moz-transform: translateZ(0);
            -ms-transform: translateZ(0);
            -o-transform: translateZ(0);
            transform: translateZ(0);
        }
        .cd-horizontal-timeline .events a::after {
            /* this is used to create the event spot */
            content: '';
            position: absolute;
            left: 50%;
            right: auto;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);
            bottom: -5px;
            height: 12px;
            width: 12px;
            border-radius: 50%;
            border: 2px solid #dfdfdf;
            background-color: #f8f8f8;
            -webkit-transition: background-color 0.3s, border-color 0.3s;
            -moz-transition: background-color 0.3s, border-color 0.3s;
            transition: background-color 0.3s, border-color 0.3s;
        }
        .no-touch .cd-horizontal-timeline .events a:hover::after {
            background-color: #7b9d6f;
            border-color: #7b9d6f;
        }
        .cd-horizontal-timeline .events a.selected {
            pointer-events: none;
        }
        .cd-horizontal-timeline .events a.selected::after {
            background-color: #7b9d6f;
            border-color: #7b9d6f;
        }
        .cd-horizontal-timeline .events a.older-event::after {
            border-color: #7b9d6f;
        }
        @media only screen and (min-width: 1100px) {
            .cd-horizontal-timeline {
                margin: 6em auto;
            }
            .cd-horizontal-timeline::before {
                /* never visible - this is used in jQuery to check the current MQ */
                content: 'desktop';
            }
        }

        .cd-timeline-navigation a {
            /* these are the left/right arrows to navigate the timeline */
            position: absolute;
            z-index: 1;
            top: 50%;
            bottom: auto;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            transform: translateY(-50%);
            height: 34px;
            width: 34px;
            border-radius: 50%;
            border: 2px solid #dfdfdf;
            /* replace text with an icon */
            overflow: hidden;
            color: transparent;
            text-indent: 100%;
            white-space: nowrap;
            -webkit-transition: border-color 0.3s;
            -moz-transition: border-color 0.3s;
            transition: border-color 0.3s;
        }
        .cd-timeline-navigation a::after {
            /* arrow icon */
            content: '';
            position: absolute;
            height: 16px;
            width: 16px;
            left: 50%;
            top: 50%;
            bottom: auto;
            right: auto;
            -webkit-transform: translateX(-50%) translateY(-50%);
            -moz-transform: translateX(-50%) translateY(-50%);
            -ms-transform: translateX(-50%) translateY(-50%);
            -o-transform: translateX(-50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%);
            /*background: url(../img/cd-arrow.svg) no-repeat 0 0;*/
        }
        .cd-timeline-navigation a.prev {
            left: 0;
            -webkit-transform: translateY(-50%) rotate(180deg);
            -moz-transform: translateY(-50%) rotate(180deg);
            -ms-transform: translateY(-50%) rotate(180deg);
            -o-transform: translateY(-50%) rotate(180deg);
            transform: translateY(-50%) rotate(180deg);
        }
        .cd-timeline-navigation a.next {
            right: 0;
        }
        .no-touch .cd-timeline-navigation a:hover {
            border-color: #7b9d6f;
        }
        .cd-timeline-navigation a.inactive {
            cursor: not-allowed;
        }
        .cd-timeline-navigation a.inactive::after {
            background-position: 0 -16px;
        }
        .no-touch .cd-timeline-navigation a.inactive:hover {
            border-color: #dfdfdf;
        }

        .cd-horizontal-timeline .events-content {
            position: relative;
            width: 100%;
            margin: 2em 0;
            overflow: hidden;
            -webkit-transition: height 0.4s;
            -moz-transition: height 0.4s;
            transition: height 0.4s;
        }
        .cd-horizontal-timeline .events-content li {
            position: absolute;
            z-index: 1;
            width: 100%;
            left: 0;
            top: 0;
            -webkit-transform: translateX(-100%);
            -moz-transform: translateX(-100%);
            -ms-transform: translateX(-100%);
            -o-transform: translateX(-100%);
            transform: translateX(-100%);
            padding: 0 5%;
            opacity: 0;
            -webkit-animation-duration: 0.4s;
            -moz-animation-duration: 0.4s;
            animation-duration: 0.4s;
            -webkit-animation-timing-function: ease-in-out;
            -moz-animation-timing-function: ease-in-out;
            animation-timing-function: ease-in-out;
        }
        .cd-horizontal-timeline .events-content li.selected {
            /* visible event content */
            position: relative;
            z-index: 2;
            opacity: 1;
            -webkit-transform: translateX(0);
            -moz-transform: translateX(0);
            -ms-transform: translateX(0);
            -o-transform: translateX(0);
            transform: translateX(0);
        }
        .cd-horizontal-timeline .events-content li.enter-right, .cd-horizontal-timeline .events-content li.leave-right {
            -webkit-animation-name: cd-enter-right;
            -moz-animation-name: cd-enter-right;
            animation-name: cd-enter-right;
        }
        .cd-horizontal-timeline .events-content li.enter-left, .cd-horizontal-timeline .events-content li.leave-left {
            -webkit-animation-name: cd-enter-left;
            -moz-animation-name: cd-enter-left;
            animation-name: cd-enter-left;
        }
        .cd-horizontal-timeline .events-content li.leave-right, .cd-horizontal-timeline .events-content li.leave-left {
            -webkit-animation-direction: reverse;
            -moz-animation-direction: reverse;
            animation-direction: reverse;
        }
        .cd-horizontal-timeline .events-content li > * {
            max-width: 800px;
            margin: 0 auto;
        }
        .cd-horizontal-timeline .events-content h2 {
            font-weight: bold;
            font-size: 2.6rem;
            font-family: "Playfair Display", serif;
            font-weight: 700;
            line-height: 1.2;
        }
        .cd-horizontal-timeline .events-content em {
            display: block;
            font-style: italic;
            margin: 10px auto;
        }
        .cd-horizontal-timeline .events-content em::before {
            content: '- ';
        }
        .cd-horizontal-timeline .events-content p {
            font-size: 1.4rem;
            color: #959595;
        }
        .cd-horizontal-timeline .events-content em, .cd-horizontal-timeline .events-content p {
            line-height: 1.6;
        }
        @media only screen and (min-width: 768px) {
            .cd-horizontal-timeline .events-content h2 {
                font-size: 7rem;
            }
            .cd-horizontal-timeline .events-content em {
                font-size: 2rem;
            }
            .cd-horizontal-timeline .events-content p {
                font-size: 1.8rem;
            }
        }

        @-webkit-keyframes cd-enter-right {
            0% {
                opacity: 0;
                -webkit-transform: translateX(100%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateX(0%);
            }
        }
        @-moz-keyframes cd-enter-right {
            0% {
                opacity: 0;
                -moz-transform: translateX(100%);
            }
            100% {
                opacity: 1;
                -moz-transform: translateX(0%);
            }
        }
        @keyframes cd-enter-right {
            0% {
                opacity: 0;
                -webkit-transform: translateX(100%);
                -moz-transform: translateX(100%);
                -ms-transform: translateX(100%);
                -o-transform: translateX(100%);
                transform: translateX(100%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateX(0%);
                -moz-transform: translateX(0%);
                -ms-transform: translateX(0%);
                -o-transform: translateX(0%);
                transform: translateX(0%);
            }
        }
        @-webkit-keyframes cd-enter-left {
            0% {
                opacity: 0;
                -webkit-transform: translateX(-100%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateX(0%);
            }
        }
        @-moz-keyframes cd-enter-left {
            0% {
                opacity: 0;
                -moz-transform: translateX(-100%);
            }
            100% {
                opacity: 1;
                -moz-transform: translateX(0%);
            }
        }
        @keyframes cd-enter-left {
            0% {
                opacity: 0;
                -webkit-transform: translateX(-100%);
                -moz-transform: translateX(-100%);
                -ms-transform: translateX(-100%);
                -o-transform: translateX(-100%);
                transform: translateX(-100%);
            }
            100% {
                opacity: 1;
                -webkit-transform: translateX(0%);
                -moz-transform: translateX(0%);
                -ms-transform: translateX(0%);
                -o-transform: translateX(0%);
                transform: translateX(0%);
            }
        }

    </style>
@endsection


@section('content')


    {{--<div class="cd-horizontal-timeline">--}}
        {{--<div class="timeline">--}}
            {{--<div class="events-wrapper">--}}
                {{--<div class="events">--}}
                    {{--<ol>--}}
                        {{--<li><a href="#0" data-date="16/01/2014" class="selected">16 Jan</a></li>--}}
                        {{--<li><a href="#0" data-date="28/02/2014">28 Feb</a></li>--}}
                        {{--<li><a href="#0" data-date="20/04/2014">20 Mar</a></li>--}}
                        {{--<li><a href="#0" data-date="20/05/2014">20 May</a></li>--}}
                        {{--<li><a href="#0" data-date="09/07/2014">09 Jul</a></li>--}}
                        {{--<li><a href="#0" data-date="30/08/2014">30 Aug</a></li>--}}
                        {{--<li><a href="#0" data-date="15/09/2014">15 Sep</a></li>--}}
                        {{--<li><a href="#0" data-date="01/11/2014">01 Nov</a></li>--}}
                        {{--<li><a href="#0" data-date="10/12/2014">10 Dec</a></li>--}}
                        {{--<li><a href="#0" data-date="19/01/2015">29 Jan</a></li>--}}
                        {{--<li><a href="#0" data-date="03/03/2015">3 Mar</a></li>--}}
                    {{--</ol>--}}

                    {{--<span class="filling-line" aria-hidden="true"></span>--}}
                {{--</div> <!-- .events -->--}}
            {{--</div> <!-- .events-wrapper -->--}}

            {{--<ul class="cd-timeline-navigation">--}}
                {{--<li><a href="#0" class="prev inactive">Prev</a></li>--}}
                {{--<li><a href="#0" class="next">Next</a></li>--}}
            {{--</ul> <!-- .cd-timeline-navigation -->--}}
        {{--</div> <!-- .timeline -->--}}

        {{--<div class="events-content">--}}
            {{--<ol>--}}
                {{--<li class="selected" data-date="16/01/2014">--}}
                    {{--<h2>Horizontal Timeline</h2>--}}
                    {{--<em>January 16th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="28/02/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>February 28th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="20/04/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>March 20th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="20/05/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>May 20th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="09/07/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>July 9th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="30/08/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>August 30th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="15/09/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>September 15th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="01/11/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>November 1st, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="10/12/2014">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>December 10th, 2014</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="19/01/2015">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>January 19th, 2015</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}

                {{--<li data-date="03/03/2015">--}}
                    {{--<h2>Event title here</h2>--}}
                    {{--<em>March 3rd, 2015</em>--}}
                    {{--<p>--}}
                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.--}}
                    {{--</p>--}}
                {{--</li>--}}
            {{--</ol>--}}
        {{--</div> <!-- .events-content -->--}}
    {{--</div>--}}


    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-col">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <p class="news-date">{{  date('d F Y', strtotime($post->date)) }}</p>
                            </div>
                        </div>
                        <div class="default-sheet-row title-wrap">
                            <h1>{{ ($post->h1)? $post->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $post->name) }}</h1>
                        </div>
                        <div id="default-sheet-category">
                            <div class="default-sheet-row">
                                <ul>
                                    @foreach($categories as $category)
                                        <li>
                                            <a title="{{ $category->name }}" href="{{ route('posts.single', ['magazine', $category->slug]) }}">{{ $category->name }}</a>
                                        </li>
                                        {{--@if(!$loop->last) ,@endif--}}
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="featured-image">
        <div class="container boxed-container">
            <div class="ratio-container-3-1">
                <div class="ratio-content cover-image lazy" data-src="{{ $post->url_wide_box }}"></div>
            </div>
            {{--<img src="{{ $post->url_full_image }}">--}}
        </div>
    </section>

    <section id="news-container">
        <div class="container boxed-container">
            <div class="container col-container container-vertical-padding-top">
                <div class="col three-fourth-col">
                    <div class="news-content">
                        {!! $post->content !!}
                    </div>
                    <div id="default-sheet-info" class="default-sheet-box">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <p class="dark-text"><strong>Share</strong></p>
                            </div>
                            <div class="default-sheet-row-cell">
                                <div class="icon-container">
                                    <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                    <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                    <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                    <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                    <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="news-sidebar" class="col one-fourth-col sidebar">
                    @if(count($all_categories))
                        <div class="widget">
                            <div class="widget-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Category</h4>
                                </div>
                            </div>
                            <div class="widget-container">
                                <ul>
                                    @foreach($all_categories as $all_category)
                                        <li>
                                            <a title="{{ $all_category->name }}" href="{{ route('posts.single', ['magazine', $all_category->slug]) }}">{{ $all_category->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if(count($post_tags))
                        <div class="widget">
                            <div class="widget-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Keywords</h4>
                                </div>
                            </div>
                            <div class="widget-container tagcloud">
                                @foreach($post_tags as $post_tag)
                                    <a title="{{ $post_tag->name }}" href="{{ route('tags.single', [$post_tag->slug]) }}">{{ $post_tag->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if(count($years))
                        <div class="widget">
                            <div class="widget-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Archive</h4>
                                </div>
                            </div>
                            <div class="widget-container">
                                <ul>
                                    @foreach($years as $year)
                                        <li>
                                            <a href="{{ route('posts.archive.all', [$section, "filtered_by" => $year->year]) }}">{{$year->year}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="container boxed-container">
        <div id="post-nav" class="container col-container-with-offset">
            @if($prev)
                <div class="col one-third-col prev">
                    <div class="default-sheet-col">
                        <a class="prev-a" href="{{ route('posts.single', [$section, $prev->slug]) }}">Prev News</a>
                        <h3><a href="{{ route('posts.single', [$section, $prev->slug]) }}">{{ $prev->title }}</a></h3>
                    </div>
                </div>
            @endif
            @if($next)
                <div class="col one-third-col next">
                    <div class="default-sheet-col">
                        <a class="next-a" href="{{ route('posts.single', [$section, $next->slug]) }}">Next News</a>
                        <h3><a href="{{ route('posts.single', [$section, $next->slug]) }}">{{ $next->title }}</a></h3>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if(count($other_posts))
        <section id="featured-news">
            <div class="container boxed-container">
                <div class="default-slider-header">
                    <span class="h1">Featured News</span>
                    <p><a class="read-more" href="{{ route('posts.archive.all', [$section]) }}">All featured news</a></p>
                </div>
                <div class="container col-container-with-offset-and-margin">
                    @foreach($other_posts as $other_post)
                        <div class="slider-item one-third-col-with-margin">
                            <div class="slider-item-img wide-box">
                                <a href="{{ route('posts.single', [$section, $other_post->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $other_post->url_wide_box }}"></div>
                                </a>
                            </div>
                            <div class="featured-news-item-txt">
                                <p class="news-date">{{  date('d F Y', strtotime($other_post->date)) }}</p>
                                <div class="default-sheet-row">
                                    <h2>
                                        <a class="dark-text" href="{{ route('posts.single', [$section, $other_post->slug]) }}">{{ $other_post->title }}</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection