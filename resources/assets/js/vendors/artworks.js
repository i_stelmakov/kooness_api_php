$(window).on("load", function () {
    /* ==========================================================================
    Generics
    ========================================================================== */
    // console.log('☢️️ Artworks isotope scripts loaded!');

    // Variables
    var $ = jQuery;
    // set timer variable
    var timeout = null;

    var artworksFiltersButton = $(".artwoks-filter-button");
    var artworksFiltersPanel = $("#archive-sidebar");

    /* ==========================================================================
    Isotope Archive
    ========================================================================== */
    // Initialise Isotope on page enter
    var $grid = $('.grid').isotope({
        itemSelector: '.item',
        layout: 'masonry',
    });

    // chek for every filter => if it is setted apply its value to filters on page
    for (var key in activeFiltersFromPHP) {
        // console.log("Key is " + key + ", value is" + activeFiltersFromPHP[key]);
        if (activeFiltersFromPHP.hasOwnProperty(key)) {
            if (key == 'min_price') {
                minPriceInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'max_price') {
                maxPriceInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'min_width') {
                minWidthInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'max_width') {
                maxWidthInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'min_height') {
                minHeightInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'max_height') {
                maxHeightInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'medium') {
                thisMediumInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'artists') {
                thisCategoryInput.val(activeFiltersFromPHP[key]); // sets value to filter
            }
            if (key == 'order_by') {
                thisOrderBy.val(activeFiltersFromPHP[key]); // sets value to filter
            }

        }
    }


    //search list filter
    $.fn.search_by_input = function () {
        $(this).on('keyup', function () {
            var value = $(this).val().toLowerCase();
            $(this).parents('.widget-container').find('ul>li').filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        })
    };

    // click on list filter
    $.fn.click_item_list = function () {
        $(this).on('click', function () {
            var $element = $(this);
            var $container = $(this).parents('.widget-container').find('ul');
            var id = $element.data('id');
            if ($element.hasClass('active')) {
                if ($element.parents('ul').data('filter') === 'medium') {
                    var index = $.inArray(id, medium);
                    if (index !== -1)
                        medium.splice(index, 1);
                } else if ($element.parents('ul').data('filter') === 'artists') {
                    var index = $.inArray(id, artists);
                    if (index !== -1)
                        artists.splice(index, 1);

                } else if ($element.parents('ul').data('filter') === 'galleries') {
                    var index = $.inArray(id, galleries);
                    if (index !== -1)
                        galleries.splice(index, 1);
                }
                $element.detach();
                $element.removeClass('active');
                var order = parseInt($element.data('order'));
                if (order === 0) {
                    if ($container.find('li.active').length > 0) {
                        $element.insertAfter($container.find('li.active:last'))
                    } else {
                        $container.prepend($element)
                    }
                } else {
                    if ($container.find('li:not(.active)').length === 0) {
                        $container.append($element);
                    } else {
                        $.each($container.find('li:not(.active)'), function (index, value) {
                            if (parseInt($(value).data('order')) > order) {
                                $element.insertBefore($(value));
                                return false;
                            }
                            if ($container.find('li:not(.active)').length - 1 === index) {
                                $container.append($element);
                            }
                        })
                    }
                }
            } else {
                if ($element.parents('ul').data('filter') === 'medium') {
                    medium.push(id);
                } else if ($element.parents('ul').data('filter') === 'artists') {
                    artists.push(id);
                } else if ($element.parents('ul').data('filter') === 'galleries') {
                    galleries.push(id);
                }
                $element.detach();
                $element.addClass('active');
                if ($container.find('li.active').length > 0) {
                    $element.insertAfter($container.find('li.active:last'))
                } else {
                    $container.prepend($element)
                }
            }
        })
    };

    $('.apply-filters').click(function () {
        if (medium.length > 0) {
            pathName = updateQueryStringParameter(pathName, 'medium', encodeURI(medium.join(',')));
        } else {
            pathName = updateQueryStringParameter(pathName, 'medium');
        }

        if (artists.length > 0) {
            pathName = updateQueryStringParameter(pathName, 'artists', encodeURI(artists.join(',')));
        } else {
            pathName = updateQueryStringParameter(pathName, 'artists');
        }

        if (galleries.length > 0) {
            pathName = updateQueryStringParameter(pathName, 'galleries', encodeURI(galleries.join(',')));
        } else {
            pathName = updateQueryStringParameter(pathName, 'galleries');
        }

        pathName = attachOrderQueryString(pathName);

        window.history.replaceState("", "", pathName);

        window.location.reload(true);
    });

    $('.remove-filters').click(function () {
        pathName = '';

        pathName = attachOrderQueryString(pathName);

        window.history.replaceState("", "", pathName);

        window.location.reload(true);
    });

    $('.item-list').click_item_list();

    $('.filter-input').search_by_input();


    // // sync input range and number (by proximity)
    function updateValue(e) {
        // variables
        var actual_filter_value = e.target.value;
        var actual_filter_name = e.target.name;
        var actual_filter_data_attribute = e.target.getAttribute('data-filter');
        var actual_filter_min = e.target.getAttribute('min');
        var actual_filter_max = e.target.getAttribute('max');

        if((actual_filter_name === 'min-price' || actual_filter_name === 'min-width' || actual_filter_name === 'min-height') && actual_filter_value === actual_filter_min){
            pathName = updateQueryStringParameter(pathName, (actual_filter_name.replace("-", "_")));
        } else if((actual_filter_name === 'max-price' || actual_filter_name === 'max-width' || actual_filter_name === 'max-height') && actual_filter_value === actual_filter_max){
            pathName = updateQueryStringParameter(pathName, (actual_filter_name.replace("-", "_")));
        } else{
            pathName = updateQueryStringParameter(pathName, (actual_filter_name.replace("-", "_")), actual_filter_value);
        }

        pathName = attachOrderQueryString(pathName);
        // get parent input
        // var sibling = jE.siblings('input')[0];
        var sibling = $('input[data-filter="' + actual_filter_data_attribute + '"]').not(this)[0];

        // sync parent value
        sibling.value = actual_filter_value;
    }


    // When All Images are loaded (imagesLoaded.always) call again isotope layout
    $grid.imagesLoaded().always(function (instance) {
        console.log('♻️ imagesLoaded.ALWAYS - all images have been loaded, isotope re-layout');
        $grid.isotope('layout');
        // showPage();
    });

    // catch each range input variation
    $('.range-number').each(function (i, obj) {
        obj.addEventListener('input', updateValue);
    });

    $('select.filter_select').on('change', function () {
        pathName = attachOrderQueryString(pathName);
        window.history.replaceState("", "", pathName);
        window.location.reload(true)
     });

    function attachOrderQueryString(pathName) {
        pathName = updateQueryStringParameter(pathName, 'order_by', undefined);
        pathName = updateQueryStringParameter(pathName, 'order_by', thisOrderBy.val());
        return pathName
    }

    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        // se il parametro non è passato
        if (value === undefined) {
            var tmp = uri.replace(re, '$1');
            // se non ci sono parametri cancella il punto di domanda finale
            if (tmp.match(/\?$/i)) {
                return tmp.replace(/\?$/i, '');
            }
            // se rimane un punto di domanda ed una e commerciale, rimuovili entrambi
            if (tmp.match(/\?&/i)) {
                return tmp.replace(/\?&/i, '');
            }
            // se finisce con una e commerciale, rimuovila
            if (tmp.match(/\&$/i)) {
                return tmp.replace(/\&$/i, '');
            }
            return tmp;
        } else {
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                return uri + separator + key + "=" + value;
            }
        }
    }


    /* ==========================================================================
    Open/close filter box for mobile
    ========================================================================== */
    /* When the user clicks on the button, toggle between hiding and showing the dropdown content */
    artworksFiltersButton.each(function (index) {
        $(this).on("click", function () {
            console.log('artworks filter button clicked!');
            artworksFiltersPanel.toggleClass("open");
        });
    });
    // responsive
    if ($(window).width() <= untilSmall) {
        console.log('Less or equal ' + untilSmall);
        // Collapse all filters on page entry
        var filtersPanels = $(".hiding-box-header");
        filtersPanels.each(function (index) {
            var $this = $(this);
            $this.children(".hiding-box-trigger").toggleClass("show");
            $this.closest(".hiding-box-header").next("div").slideToggle("swing");
        });
    }

});
