<?php

namespace App\Http\Controllers\Artists;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Artwork;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\LayoutPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class FrontendController extends FrontendExtendedController
{

    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Artists Intro
    public function intro()
    {
        $slides = [];
        $widgets = [];
        $layout = LayoutPage::whereLabel('artists')->first();
        if ($layout) {
            $slider = $layout->slider()->first();
            if ($slider) {
                $contents = json_decode($slider->contents);
                foreach ($contents as $content) {
                    $artwork = Artist::find($content);
                    if ($artwork) {
                        $slides[] = $artwork;
                    }
                }
            }
            $result = $layout->widgets()->orderBy('order', "ASC")->get();
            foreach ($result as $row) {
                $tmp = [
                    'title' => $row->title,
                    'subtitle' => $row->subtitle,
                    'description' => $row->description,
                    'section' => $row->section,
                    'letter' => 'a',
                    'link' => null,
                    'link_title' => "View all",
                    'template' => $row->template,
                    'items' => []
                ];
                $items = json_decode($row->items);
                foreach ($items as $item) {
                    if ($row->section == 'artworks') {
                        $rows = Artwork::find($item);
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artworks";
                    } else if ($row->section == 'artists') {
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artists";
                        $rows = Artist::find($item);
                    } else if ($row->section == 'galleries') {
                        $tmp['letter'] = 'g';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured galleries";
                        $rows = Gallery::find($item);
                    }
                    $tmp['link'] = $row->link;
                    if (isset($rows) && $rows) {
                        $tmp['items'][] = $rows;
                    }
                }
                $widgets[] = $tmp;
            }
        }
        $artists = Artist::whereStatus(1)->whereRaw('slug IS NOT NULL')->orderBy('created_at', "DESC")->limit(12)->get();
        $categories = ArtistsCategory::limit(10)->get();
        $cities = DB::table('artists')
            ->select('countries.*')
            ->join('countries', 'artists.country_of_birth', '=', 'countries.id')
            ->groupBy('countries.id')
            ->orderBy('countries.name', 'ASC')
            ->get();
        return view('artists.frontend.intro', compact('slides', 'widgets', 'categories', 'artists', 'cities'));
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Archive All
    public function archive(Request $request, $slug_name = null)
    {
        $filter_fair = $request->get('fair');
        $filter_gallery = $request->get('gallery');
        $filter_exhibition = $request->get('exhibition');
        $filter_start = $request->get('startWith');
        $filter_location = $request->get('nationality');
        $artists = [];
        $fair = null;
        $gallery = null;
        $exhibition = null;
        $category = null;

        if ($slug_name) {
            $category = ArtistsCategory::whereSlug($slug_name)->first();
        }
        if ($filter_fair) {
            $fair = Fair::whereSlug($filter_fair)->first();
            if ($fair) {
                $filter_value = $fair->slug;
                $filter_type = 'fair';
                $query = DB::table('artists')
                    ->distinct()
                    ->select('artists.*')
                    ->join('artworks', 'artists.id', '=', 'artworks.artist_id')
                    ->join('artworks_fairs', 'artworks.id', '=', 'artworks_fairs.artwork_id')
                    ->join('fairs', 'artworks_fairs.fair_id', '=', 'fairs.id')
                    ->where('fairs.slug', 'LIKE', $fair->slug)
                    ->whereRaw('artists.slug IS NOT NULL')
                    ->orderBy('artists.created_at', "DESC");
                if ($filter_start) {
                    $query->where("artists.last_name", "LIKE", $filter_start . "%");
                }
                if ($filter_location) {
                    $query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
                        ->where("countries.code", "=", $filter_location);
                }
                if($category){
                    $query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
                        ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
                        ->where('artists_categories.slug', '=', $category->slug);
                }
                $paginate = $query->paginate(20);
                $paginate->appends($request->all());
                $artists = Artist::hydrate($paginate->items());
            }
        } else if ($filter_gallery) {
            $gallery = Gallery::whereSlug($filter_gallery)->first();
            if ($gallery) {
                $filter_value = $gallery->slug;
                $filter_type = 'gallery';
                $artists_query = $gallery->artists()->whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->orderBy('artists.created_at', "DESC");
                $artists_query->select('artists.*');
                if ($filter_start) {
                    $artists_query->where("artists.last_name", "LIKE", $filter_start . "%");
                }
                if ($filter_location) {
                    $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
                        ->where("countries.code", "=", $filter_location);
                }
                if($category){
                    $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
                        ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
                        ->where('artists_categories.slug', '=', $category->slug);
                }
                $artists = $artists_query->paginate(20);
                $artists->appends($request->all());
            }
        } else if ($filter_exhibition) {
            $exhibition = Exhibition::whereSlug($filter_exhibition)->first();
            if ($exhibition) {
                $filter_value = $exhibition->slug;
                $filter_type = 'exhibition';
                $artists_query = $exhibition->artists()->whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->orderBy('artists.created_at', "DESC");
                $artists_query->select('artists.*');
                if ($filter_start) {
                    $artists_query->where("artists.last_name", "LIKE", $filter_start . "%");
                }
                if ($filter_location) {
                    $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
                        ->where("countries.code", "=", $filter_location);
                }
                if($category){
                    $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
                        ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
                        ->where('artists_categories.slug', '=', $category->slug);
                }
                $artists = $artists_query->paginate(20);
                $artists->appends($request->all());
            }
        } else {
            $artists_query = Artist::whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->orderBy('artists.created_at', "DESC");
            $artists_query->select('artists.*');
            if ($filter_start) {
                $artists_query->where("artists.last_name", "LIKE", $filter_start . "%");
            }
            if ($filter_location) {
                $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
                    ->where("countries.code", "=", $filter_location);
            }
            if($category){
                $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
                    ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
                    ->where('artists_categories.slug', '=', $category->slug);
            }
            $artists = $artists_query->paginate(20);
            $artists->appends($request->all());
        }
        $categories = ArtistsCategory::all();
        $cities = DB::table('artists')
            ->select('countries.*')
            ->join('countries', 'artists.country_of_birth', '=', 'countries.id')
            ->groupBy('countries.id')
            ->orderBy('countries.name', 'ASC')
            ->get();

        return view('artists.frontend.archive', compact('artists', 'categories', 'filter_type', 'filter_value', 'fair', 'gallery', 'exhibition', 'cities', 'category', 'hide_load_more', 'paginate'));
    }

    // Artists Single
    public function single(Request $request, $slug_name)
    {
        $category = ArtistsCategory::whereSlug($slug_name)->first();
        if ($category) {
            return $this->archive($request, $slug_name);
        }
        $filter_order_by = $request->get('works-order-by');
        $order_by = 'created_at';
        $order_dir = 'DESC';
        if($filter_order_by) {
            if($filter_order_by == 'recent') {
                $order_by = "created_at";
            }
            elseif($filter_order_by == 'higher-price') {
                $order_by = "price";
                $order_dir = 'DESC';
            }
            elseif($filter_order_by == 'lower-price') {
                $order_by = "price";
                $order_dir = 'ASC';
            }
        }
        $artist = Artist::whereSlug($slug_name)->whereRaw('slug IS NOT NULL')->first(); // select * from artists (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)
        if ($artist) {
            $prev = Artist::whereStatus(1)->whereRaw('slug IS NOT NULL')->where("id", "<", $artist->id)->orderBy('created_at', "DESC")->first();
            $next = Artist::whereStatus(1)->whereRaw('slug IS NOT NULL')->where("id", ">", $artist->id)->orderBy('created_at', "ASC")->first();
            return view('artists.frontend.single', compact('artist', 'slug_name', 'prev', 'next', 'order_by', 'order_dir'));
        } else {
            return redirect(route('artists.archive.all'));
        }
    }

//    public function loadMore(Request $request)
//    {
//        if ($request->ajax()) {
//            $filter_value = $request->get('filter_value');
//            $filter_type = $request->get('filter_type');
//            $filter_alpha = $request->get('filter_alpha');
//            $filter_location = $request->get('filter_location');
//            $offset = $request->get('offset');
//            $category_id = $request->get('category');
//            $category = null;
//            $artists = [];
//            if ($category_id) {
//                $category = ArtistsCategory::whereId($category_id)->first();
//            }
//            if ($filter_type && $filter_value) {
//                if ($filter_type == 'fair') {
//                    $fair = Fair::whereSlug($filter_value)->first();
//                    if ($fair) {
//                        $query = DB::table('artists')
//                            ->distinct()
//                            ->select('artists.*')
//                            ->join('artworks', 'artists.id', '=', 'artworks.artist_id')
//                            ->join('artworks_fairs', 'artworks.id', '=', 'artworks_fairs.artwork_id')
//                            ->join('fairs', 'artworks_fairs.fair_id', '=', 'fairs.id')
//                            ->where('fairs.slug', 'LIKE', $fair->slug)
//                            ->whereRaw('artists.slug IS NOT NULL')
//                            ->orderBy('artists.created_at', "DESC")
//                            ->offset($offset)
//                            ->limit(21);
//                        if ($filter_alpha) {
//                            $query->where("artists.last_name", "LIKE", $filter_alpha . "%");
//                        }
//                        if ($filter_location) {
//                            $query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
//                                ->where("countries.code", "=", $filter_location);
//                        }
//                        if($category){
//                            $query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
//                                ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
//                                ->where('artists_categories.slug', '=', $category->slug);
//                        }
//                        $artists = Artist::hydrate($query->get()->toArray());
//                    }
//                } else if ($filter_type == 'gallery') {
//                    $gallery = Gallery::whereSlug($filter_value)->first();
//                    if ($gallery) {
//                        $artists_query = $gallery->artists()->whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->offset($offset)->limit(21)->orderBy('artists.created_at', "DESC");
//                        $artists_query->select('artists.*');
//                        $artists_query->distinct();
//                        if ($filter_alpha) {
//                            $artists_query->where("artists.last_name", "LIKE", $filter_alpha . "%");
//                        }
//                        if ($filter_location) {
//                            $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
//                                ->where("countries.code", "=", $filter_location);
//                        }
//                        if($category){
//                            $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
//                                ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
//                                ->where('artists_categories.slug', '=', $category->slug);
//                        }
//                        $artists = $artists_query->get();
//                    }
//                } else if ($filter_type == 'exhibition') {
//                    $exhibition = Exhibition::whereSlug($filter_value)->first();
//                    if ($exhibition) {
//                        $artists_query = $exhibition->artists()->whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->offset($offset)->limit(21)->orderBy('artists.created_at', "DESC");
//                        $artists_query->select('artists.*');
//                        $artists_query->distinct();
//                        if ($filter_alpha) {
//                            $artists_query->where("artists.last_name", "LIKE", $filter_alpha . "%");
//                        }
//                        if ($filter_location) {
//                            $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
//                                ->where("countries.code", "=", $filter_location);
//                        }
//                        if($category){
//                            $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
//                                ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
//                                ->where('artists_categories.slug', '=', $category->slug);
//                        }
//                        $artists = $artists_query->get();
//                    }
//                }
//            } else {
//                $artists_query = Artist::offset($offset)->whereRaw('artists.slug IS NOT NULL')->whereStatus(1)->limit(21)->whereStatus(1)->orderBy('artists.created_at', "DESC");
//                $artists_query->select('artists.*');
//                $artists_query->distinct();
//                if ($filter_alpha) {
//                    $artists_query->where("artists.last_name", "LIKE", $filter_alpha . "%");
//                }
//                if ($filter_location) {
//                    $artists_query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
//                        ->where("countries.code", "=", $filter_location);
//                }
//                if($category){
//                    $artists_query->join('artists_artists_categories', 'artists_artists_categories.artist_id', '=', 'artists.id')
//                        ->join('artists_categories', 'artists_categories.id', '=', 'artists_artists_categories.category_id')
//                        ->where('artists_categories.slug', '=', $category->slug);
//                }
//                $artists = $artists_query->get();
//            }
//
//            $return = [];
//            $hide_load_more = false;
//            if(count($artists) <= 20){
//                $hide_load_more = true;
//            } else{
//                $artists->pop();
//            }
//            foreach ($artists as $key => $artist) {
//                $return[] = [
//                    "id" => $artist->id,
//                    "name" => $artist->first_name . " " . $artist->last_name,
//                    "url" => route('artists.single', [$artist->slug]),
//                    "image" => $artist->url_square_box,
//                    "count" => $artist->artworks()->count(),
//                    "country" => ($artist->countryBirth()->count()) ? $artist->countryBirth()->first()->name : null,
//                    "auth_user" => (Auth::user()) ? true : false,
//                    "follow_status" => (Auth::user() && Auth::user()->collectionArtists->contains($artist->id)) ? true : false
//                ];
//            }
//
//            return response()->json([
//                "result" => "success",
//                "item_list" => $return,
//                "hide_load_more" => $hide_load_more
//            ]);
//        }
//    }

    public function searchBy(Request $request)
    {
        if ($request->ajax()) {
            $filter_data = $request->get('filter_data');
            $current_ids = ($request->get('current')) ? $request->get('current') : [];
            $query = Artist::whereStatus(1)->whereRaw('slug IS NOT NULL')->orderBy('artists.created_at', "DESC")->limit(12);
            if ($filter_data['alpha'] && $filter_data['alpha'] != 'all') {
                $query->where("last_name", "LIKE", $filter_data['alpha'] . "%");
            }
            if ($filter_data['location'] && $filter_data['location'] != '') {
                $query->join('countries', 'artists.country_of_birth', '=', 'countries.id')
                    ->where("countries.code", "=", $filter_data['location']);
            }
            $artists = $query->get();

            $tmp = [];
            foreach ($artists as $key => $artist) {
                if (!empty($current_ids)) {
                    if (in_array($artist->id, $current_ids)) {
                        $index = array_search($artist->id, $current_ids);
                        if ($index > -1) {
                            unset($current_ids[$index]);
                        }
                        continue;
                    }
                }
                $tmp[] = [
                    "id" => $artist->id,
                    "name" => $artist->first_name . " " . $artist->last_name,
                    "url" => route('artists.single', [$artist->slug]),
                    "image" => $artist->url_square_box,
                    "count" => $artist->artworks()->count(),
                    "country" => ($artist->countryBirth()->count()) ? $artist->countryBirth()->first()->name : null
                ];
            }

            $limit = 12;
            if ((12 - count($current_ids)) > 0) {
                if ((12 - count($current_ids) < count($tmp))) {
                    $limit = count($tmp);
                } elsE {
                    $limit = 12 - count($current_ids);
                }
            }
            $return = array_slice($tmp, 0, $limit);

            $results_array = [
                "new_item" => $return,
                "remove_item" => $current_ids
            ];

            return response()->json(["result" => "success", 'items' => $results_array]);
        }
    }
}
