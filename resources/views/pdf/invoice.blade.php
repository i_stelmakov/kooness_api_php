<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            width: 100%;
            height: 29.7cm;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }


        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {

            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }


        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }

        table th,
        table td {
            text-align: left;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        /** {*/
            /*border: 1px solid aqua;*/
        /*}*/

        table.main td {
            padding: 5px 20px;
        }

        table.second td {
            padding: 10px 20px;
        }


        #notices .notice {
            color: #5D6975;
            font-size: 1em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 15px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo" style="margin-bottom:20px;">
        <img src="{{public_path('/images/logoblack-alta.png')}}">
    </div>
    <h1>Invoice n.{{ $invoice_number }}</h1>

    <table>
        <tr>
            <td width="50%" align="left" style="padding: 20px">
                <table>
                    <tr>
                        <td>CLIENT</td>
                        @if($order->billing_first_name && $order->billing_last_name)
                            <td>{{ $order->billing_first_name }}  {{ $order->billing_last_name }}</td>
                        @else
                            <td>{{ $order->billing_company_name }}</td>
                        @endif
                    </tr>
                    @if($order->billing_fiscal_code)
                        <tr>
                            <td>FISCAL CODE</td>
                            <td>{{ $order->billing_fiscal_code }}</td>
                        </tr>
                    @endif
                    @if($order->billing_vat_number)
                        <tr>
                            <td>VAT NUMBER</td>
                            <td>{{ $order->billing_vat_number }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>ADDRESS</td>
                        <td>{{ $order->billing_address }}<br>
                            {{ $order->billing_city }} {{ $order->billing_zip }} {{ ($order->billing_state)? ", ".$order->billing_state : '' }}<br>
                            {{ $order->billing_country }} ({{ $order->billing_country_code }})
                        </td>
                    </tr>
                    <tr>
                        <td>EMAIL</td>
                        <td>{{ $order->billing_email }}</td>
                    </tr>
                    <tr>
                        <td>DATE</td>
                        <td>{{ date('d/m/Y') }}</td>
                    </tr>
                </table>
            </td>
            <td width="50%" align="right" style="padding: 20px">
                <div id="company" class="" style="text-align:right;" >
                    <div>U-ART SRL</div>
                    <div>Via Cesare Battisti 49</div>
                    <div>46043 Castiglione delle siviere (MN)</div>
                    <div>info@kooness.com</div>
                    <div>VAT 02439970209</div>
                </div>
            </td>
        </tr>
    </table>
</header>
<main>
    <table class="main">
        <thead>
        <tr>
            <th>REFERNCE ID</th>
            <th>TITLE</th>
            <th>ARTISTS</th>
            <th>GALLERY</th>
            <th>TOTAL</th>
        </tr>
        </thead>
        <tbody>
        @foreach($order->items()->get() as $item)
            <tr>
                <td><p>{{ $item->artwork()->first()->reference }}</p></td>
                <td><p>{{ $item->artwork()->first()->title }}</p></td>
                <td><p>{{ $item->artwork()->first()->artist()->first()->full_name }}</p></td>
                <td><p>{{ $item->artwork()->first()->gallery()->first()->name }}</p></td>
                <td><p style="text-align: right;">{{ number_format(($item->subtotal),2,'.','') }} €</p></td>
            </tr>
        @endforeach


        <tr>
            <td colspan="3"></td>
            <td><p style="text-align: right;">Subtotal</p></td>
            <td><p style="text-align: right;">{{ number_format(($order->amount - $order->shipping),2,'.','') }} €</p></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><p style="text-align: right;">Shipping</p></td>
            <td><p style="text-align: right;">{{ number_format($order->shipping,2,'.','')  }} €</p></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td><p style="text-align: right;"><b>TOTAL</b></p></td>
            <td><p style="text-align: right;"><b>{{ number_format(($order->amount),2,'.','')  }} €</b></p></td>
        </tr>



        </tbody>
    </table>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">You can ask for a refund within 7 days since the delivery of this invoice.</div>
    </div>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
