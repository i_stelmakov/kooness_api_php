@extends('layouts.admin')
@section('title', "Admin | Edit Artist `{$artist->first_name} {$artist->last_name}`")
@section('page-header', "Admin - Edit Artist `{$artist->first_name} {$artist->last_name}`")

@section('content')
    @include('artists.admin.form', ['action' => route('admin.artists.update', [$artist->id]), 'method' => 'PATCH'])
@endsection