<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistsCategory extends Model
{
    protected $guarded = [];

    public function artists(){
        return $this->belongsToMany('App\Models\Artist', 'artists_artists_categories', 'category_id', 'artist_id');
    }

    public function randomImage(){
        $artist = $this->artists()->whereStatus(1)->orderByRaw("RAND()")->first();
        if($artist && !$artist->url_full_image){
            return $this->randomImage();
        }else{
            if(!$artist)
                return null;
            return $artist->url_full_image;
        }
    }
}
