<?php

namespace App\Http\Controllers\Galleries;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\MbrPayment;
use App\Models\SiteMap;
use App\Models\Tag;
use App\Models\User;
use App\Services\Notification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Session;
use View;

class AdminController extends Controller
{

    private $_directory_images = 'galleries';

    private $_type_url = 'Gallery';

    private $_url_prefix = '/galleries/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Memberships List
    public function membershipsList()
    {
        return view('memberships.admin.list');
    }

    // Memberships Payments
    public function membershipsPayments()
    {
        return view('memberships.admin.payments');
    }

    // Galleries
    public function index()
    {
        return view('galleries.admin.index');
    }

    // Galleries Ajax List
    public function retrieve(\Illuminate\Http\Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'phone',
            4 => 'country',
            5 => 'city',
            6 => 'status',
            7 => 'created_at',
            8 => 'action',
        );

        $totalData = Gallery::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $galleries = Gallery::orderBy($order, $dir)
                ->offset($start)
                ->limit($limit)
                ->get();
        } else {
            $search = $request->input('search.value');

            $galleries = Gallery::where('name', 'LIKE', "%{$search}%")
                ->orderBy($order, $dir)
                ->offset($start)
                ->limit($limit)
                ->get();

            $totalFiltered = Gallery::where('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($galleries)) {
            foreach ($galleries as $gallery) {
                $edit = route('admin.galleries.edit', $gallery->id);
                $delete = route('admin.galleries.destroy', $gallery->id);
                $nestedData['id'] = $gallery->id;
                $nestedData['name'] = $gallery->name;
                $nestedData['email'] = $gallery->email;
                $nestedData['phone'] = $gallery->phone;
                $nestedData['city'] = $gallery->city;
                $nestedData['country'] = $gallery->country;
                $nestedData['status'] = get_state_status($gallery->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($gallery->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    // Memberships Ajax List all Memberships
    public function membershipsRetrieve(\Illuminate\Http\Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'gallery_name',
            2 => 'membership_plan',
            3 => 'amount',
            4 => 'fee',
            5 => 'activation_date',
            6 => 'renewal',
            7 => 'payment',
            8 => 'status',
            9 => 'payment_status',
//            9 => 'action',
        );

        $totalData = User::whereIsPremium(1)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $membership_users = (new User())->selectRaw('users.*, 
            (
                SELECT created_at 
                FROM mbr_payments 
                WHERE user_id = users.id 
                AND status = 1 
                ORDER BY created_at ASC 
                LIMIT 1
            ) AS activation_date, 
            (
                SELECT name 
                FROM galleries 
                WHERE user_id = users.id 
                LIMIT 1
            ) AS gallery_name, 
            premium_exp_date AS renewal, 
            (
                SELECT card_type 
                FROM mbr_payments 
                WHERE user_id = users.id 
                ORDER BY created_at ASC
                LIMIT 1
            ) AS card_type,
            (   
                CASE
                   WHEN users.renewal_failed_date IS NOT NULL 
                   THEN 2 #status => membership attiva, con pagamento fallito
                   WHEN users.deactivate_premium_request_date IS NOT NULL
                   THEN 1 #status => membership attiva, in disattivazione
                   ELSE 0 #status => membership attiva, con pagamento riuscito
                END 
            ) AS status')
                ->whereIsPremium(1)
                ->orderBy($order, $dir)
                ->offset($start)
                ->limit($limit)
                ->get();
//            dd($membership_users);
        }
        else {
            $search = $request->input('search.value');

            $membership_users_query = (new User())->selectRaw('users.*, 
            (
                SELECT created_at 
                FROM mbr_payments 
                WHERE user_id = users.id 
                AND status = 1 
                ORDER BY created_at ASC 
                LIMIT 1
            ) AS activation_date, 
            (
                SELECT name 
                FROM galleries 
                WHERE user_id = users.id 
                LIMIT 1
            ) AS gallery_name, 
            premium_exp_date AS renewal, 
            (
                SELECT card_type 
                FROM mbr_payments 
                WHERE user_id = users.id 
                ORDER BY created_at ASC
                LIMIT 1
            ) AS card_type,
            (   
                CASE
                   WHEN users.renewal_failed_date IS NOT NULL 
                   THEN 2 #status => membership attiva, con pagamento fallito
                   WHEN users.deactivate_premium_request_date IS NOT NULL
                   THEN 1 #status => membership attiva, in disattivazione
                   ELSE 0 #status => membership attiva, con pagamento riuscito
                END 
            ) AS status')
                ->join('galleries', 'galleries.user_id', '=', 'users.id')
                ->whereRaw("galleries.name LIKE '{$search}%'")
                ->whereIsPremium(1)
                ->orderBy($order, $dir)
                ->offset($start)
                ->limit($limit);

//            var_dump($membership_users_query->toSql());exit;
            $membership_users = $membership_users_query->get();

            $totalFiltered = $membership_users_query->count();
        }

        $data = array();
        if (!empty($membership_users)) {
            foreach ($membership_users as $membership_user) {
                $edit = null;
                $delete = null;
                $nestedData['id'] = $membership_user->id;
                $nestedData['gallery_name'] = $membership_user->gallery()->first()->name;
                $plan =  $membership_user->mbr_plan()->first();
                $plan->setUser($membership_user);
                $nestedData['membership_plan'] = $plan->name;
                $nestedData['amount'] = $plan->fixed_fee . $plan->getSymbolCurrency();
                $nestedData['fee'] = $plan->success_fee . '%';
                $nestedData['activation_date'] = date('d/m/Y', strtotime($membership_user->activation_date));
                $nestedData['renewal'] = date('d/m/Y', strtotime($membership_user->premium_exp_date));
                $nestedData['payment'] = $membership_user->card_type;
                $nestedData['status'] = ( ($membership_user->status == 0) ? '<span class="success">ACTIVE - PAYED</span>' : (   ($membership_user->status == 1) ? '<span class="warning">IN DEACTIVATION</span>' : '<span class="error">ACTIVE - PAYMENT FAILED</span>'  ) );
                $nestedData['payment_status'] = ( ($membership_user->status == 0) ? '<span class="success">OK</span>' : (   ($membership_user->status == 1) ? '<span class="success">OK</span>' : '<span class="error">FAILED</span>'  ) );
//                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    // Memberships Ajax List all Payments
    public function membershipsPaymentRetrieve(\Illuminate\Http\Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'serial',
            2 => 'gallery_name',
            3 => 'plan',
            4 => 'total',
            5 => 'currency',
            6 => 'status',
            7 => 'created_at',
            8 => 'pdf',
        );

        $totalData = MbrPayment::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $membership_payments = MbrPayment::selectRaw('mbr_payments.*, galleries.name AS gallery_name, mbr_plans.name AS plan_name')
                ->join('galleries', 'galleries.user_id', '=', 'mbr_payments.user_id')
                ->join('mbr_plans', 'mbr_payments.plan_id', '=', 'mbr_plans.id')
                ->orderBy($order, $dir)
                ->offset($start)
                ->limit($limit)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $membership_payments_query = MbrPayment::selectRaw('mbr_payments.*, galleries.name AS gallery_name, mbr_plans.name AS plan_name')
                ->join('galleries', 'galleries.user_id', '=', 'mbr_payments.user_id')
                ->join('mbr_plans', 'mbr_payments.plan_id', '=', 'mbr_plans.id')
                ->whereRaw("galleries.name LIKE '{$search}%'")
                ->orderBy($order, $dir)
                ->offset($start)
                ->limit($limit);

            $membership_payments = $membership_payments_query->get();

            $totalFiltered = $membership_payments_query->count();
        }

        $data = array();
        if (!empty($membership_payments)) {
            foreach ($membership_payments as $membership_payment) {

                $edit = null;
                $delete = null;
                $invoice = null;
                if($membership_payment->status){
                    $invoice = route('admin.memberships.view.invoice', [$membership_payment->serial]);
                }
                $nestedData['id'] = $membership_payment->id;
                $nestedData['serial'] = $membership_payment->serial;
                $nestedData['gallery_name'] = $membership_payment->gallery_name;
                $nestedData['plan'] =  $membership_payment->plan_name;
                $nestedData['total'] = $membership_payment->total;
                $nestedData['currency'] = $membership_payment->currency;
                $nestedData['status'] = ( ($membership_payment->status == 0) ? '<span class="error">Failed</span>' : '<span class="success">Ok</span>' );
                $nestedData['created_at'] = date('d/m/Y', strtotime($membership_payment->created_at));
                $nestedData['pdf'] =  (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete, 'invoice' => $invoice]))->render();;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    // Galleries Create
    public function create()
    {
        $gallery = new Gallery();
        $countries = Country::all();
        $categories = GalleriesCategory::select("id", "name")->get();
        $tags = Tag::select("id", "name")->get();
        $fairs = Fair::all();
        return view('galleries.admin.create', compact('gallery', 'countries', 'categories', 'tags', 'fairs'));
    }

    // Galleries Store
    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['created_by'] = Auth::user()->id;
            $gallery = Gallery::create($data);

            if ($gallery) {

                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $gallery, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $gallery, '\App\Models\GalleriesCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $gallery, '\App\Models\Fair', 'fairs', 'fair_ids');

                SiteMap::create([
                    "url" => $this->_url_prefix . $gallery->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $gallery->id
                ]);

                $this->createUser($gallery, $data);

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $gallery->id,
                    'redirectTo' => route('admin.galleries.index')
                ];
                $request->session()->flash("success", "Gallery with name `{$gallery->name}` is successful created!");
            }
        }

        return response()->json($response);
    }

    // Galleries Edit
    public function edit($gallery) {
        if(roleCheck('gallerist')){
            $gallery_user = Auth::user()->gallery()->first();
            if($gallery != $gallery_user->id){
                return redirect()->route('admin.dashboard');
            }
        }
        $gallery = Gallery::find($gallery);
        $countries = Country::all();
        $categories = GalleriesCategory::select("id", "name")->get();
        $tags = Tag::select("id", "name")->get();
        $fairs = Fair::all();
        return view('galleries.admin.edit', compact('gallery', 'countries', 'categories', 'tags', 'fairs'));
    }

    // Galleries Update
    public function update(Request $request, $gallery) {
        if(roleCheck('gallerist')){
            $gallery_user = Auth::user()->gallery()->first();
            if($gallery != $gallery_user->id){
                return redirect()->route('admin.dashboard');
            }
        }
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $gallery = Gallery::find($gallery);
        if ($request->method() == 'PATCH' && $gallery) {
            $data = Input::all();
            if ($gallery->update($data)) {

                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $gallery, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $gallery, '\App\Models\GalleriesCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['fairs'])) ? $data['fairs'] : null, $gallery, '\App\Models\Fair', 'fairs', 'fair_ids');

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($gallery->id)->first();
                $site_map->url = $this->_url_prefix . $gallery->slug;
                $site_map->save();

                // associo l'utenza salvata
                $user = $gallery->user()->first();
                if( !$user ) {
                    $this->createUser($gallery, $data);
                }
                else {
                    // abilitazione utenza se box checcato e con valore
                    if( $request->has('gallerist_status') && $request->get('gallerist_status') == 1 ) {
                        $user->status = 1;
                        $user->user_first_enable = 1;
                        // all'update, se si seleziona la creazione dell'utenza si deve riassociare la psw
                        $password = str_random(10);
                        $user->password = bcrypt($password);
                        $user->save();

                        // identifico il tipo di utenza che ha creato il nuovo utente
                        if( roleCheck('gallerist') ) {
                            $user_kind_with_article = 'A Gallerist';
                        } elseif( roleCheck('fair') ) {
                            $user_kind_with_article = 'A Fair';
                        } else {
                            $user_kind_with_article = 'An Administrator';
                        }

                        // mando mail
                        if ($user && $user->status == 1 ) {

                            $to = $user->email;
                            $name = $user->full_name;
                            $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                            $extras = array(
                                'username' => $user->username,
                                'password' => $password,
                                'user_kind_with_article' => $user_kind_with_article
                            );

                            Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                                $message->to($to, $name)->subject($subject);
                            });
                        }
                    }
                }

                if (roleCheck('gallerist')) {
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_GALLERY_PROFILE, 'gallery', $gallery);
                    Notification::addUsers($notify, $users);
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $gallery->id,
                    'redirectTo' => route('admin.galleries.index')
                ];
                $request->session()->flash("success", "Gallery with name `{$gallery->name}` is successful updated!");
            }
        }
        return response()->json($response);
    }

    // Galleries Delete
    public function destroy($gallery) {
        $gallery = Gallery::find($gallery);
        if (Storage::disk()->exists(public_path("uploads/galleries/{$gallery->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/galleries/{$gallery->id}"));
        }
        $gallery->user()->delete();
        $gallery->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($gallery->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('galleries.single', [$gallery->slug]), PHP_URL_PATH), parse_url(route('galleries.archive.all'), PHP_URL_PATH));

        Session::flash('success', "Gallery with name `{$gallery->name}` is successful removed!");
        return redirect(route('admin.galleries.index'));
    }

    // Galleries Create
    private function createUser($gallery, $data){
        if (isset($data['created_user']) && $data['created_user'] == '1') {
            $password = str_random(10);
            $username = strtolower($data['first_name'] . "." . $data['last_name']);
            $check = User::whereUsername($username)->count();
            if ($check > 0) {
                $username .= "." . str_random(6);
            }

            $expiration_date = date('Y-m-d', strtotime(str_replace('/', '-', $data['membership_expiration'])));
            $expiration_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($expiration_date)) . " -1 month"));
            $activation_code = hash_hmac('sha256', str_random(40), config('app.key'));
            $user = User::create([
                'username' => strtolower($username),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($password),
                'activation_code' => $activation_code,
                'status' => ($data['gallerist_status']) ? 1 : 0,
                'user_first_enable' => ($data['gallerist_status']) ? 1 : 0,
                'trial_activation_date' => $expiration_date
            ]);

            $user->assignRole('gallerist');

            $user->gallery()->save($gallery);

            
            // identifico il tipo di utenza che ha creato il nuovo utente
            if( roleCheck('gallerist') ) {
                $user_kind_with_article = 'A Gallerist';
            } elseif( roleCheck('fair') ) {
                $user_kind_with_article = 'A Fair';
            } else {
                $user_kind_with_article = 'An Administrator';
            }

            if ($user && $user->status == 1 ) {

                $to = $user->email;
                $name = $user->full_name;
//                $subject = "Hi {$user->full_name}! Welcome to Kooness.com";
                $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                $link = url("activate/$activation_code");
                $extras = array(
                    'username' => $user->username,
                    'password' => $password,
                    'user_kind_with_article' => $user_kind_with_article
                );

                Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });
            }
        }
    }

    // Donwload Galleries Membership Guide
    public function downloadGuide() {
        $destinationPath = storage_path('uploads/guide/gallerist_guide.pdf');
        $headers = array(
            'Content-Type: application/pdf',
            'Content-disposition: filename="gallerist_guide.pdf"',
    );
        return response()->file($destinationPath, $headers);
    }






}
