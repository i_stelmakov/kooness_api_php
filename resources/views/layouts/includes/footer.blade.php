@if( env('APP_ENVIRONMENT_KIND') == 'prod' )
    <script>
        window.addEventListener('load', function() {

            // Ask Info button
            var askInfoButton = document.getElementById('askInfoButton');
            if(askInfoButton !== null) {
                askInfoButton.addEventListener(
                    'click',
                    function() {

                        // For facebook pixel

                        // For Google Analytics
                        ga('send', 'event', 'Lead', 'Contact US');
                        console.log('Google Analytics "Ask Info" triggered!');
                    },
                    false
                );
            }

        });
    </script>
@endif

<footer id="main-footer">
    <div class="container boxed-container container-vertical-padding-top">
        <div class="container col-container">
            <div class="col full-col footer-col">
                <div class="widget footer-widget">
                    <ul>
                        <li>
                            <a href="{{ route('pages.intro', ['about']) }}#who-we-are">ABOUT US</a>
                        </li>
                        {{-- <li>
                            <a href="{{ route('pages.intro', ['our-services']) }}">OUR SERVICES</a>
                        </li> --}}
                        <li>
                            <a href="{{ route('pages.intro', ['terms-conditions']) }}">TERMS & CONDITIONS</a>
                        </li>
                        <li>
                            <a href="{{ route('pages.intro', ['gallery-membership']) }}">GALLERY MEMBERSHIP</a>
                        </li>
                        <li>
                            <a href="{{ route('pages.intro', ['contact']) }}#contact-us">CONTACT US</a>
                        </li>
                        <li>
                            <a href="{{ route('pages.intro', ['support-center']) }}">SUPPORT CENTER</a>
                        </li>
                    </ul>
                </div>
                <div class="widget footer-widget">
                    <ul>
                        <li>
                            <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="{{ route('pages.intro', ['cookie-policy']) }}">Cookie Policy</a>
                        </li>
                    </ul>
                </div>
                <div class="icon-container container-vertical-padding-top">
                    <a title="Kooness Facebook" href="https://www.facebook.com/koonessofficial/">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a title="Kooness Twitter" href="https://twitter.com/KoonessOfficial">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a title="Kooness Pinterest" href="https://it.pinterest.com/kooness/">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a title="Kooness Instagram" href="https://www.instagram.com/koonessofficial/">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a id="askInfoButton" title="Kooness Mail" href="{{ route('pages.intro', ['contact']) }}#contact-us">
                        <i class="fa fa-envelope"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="container col-container-with-offset">
            <div class="one-fifths-col">
                <h3 class="footer-section-title">Artworks</h3>
                <ul class="footer-sub-menu">
                    <li class="footer-sub-menu-macro">Paintings</li>
                    <ul class="footer-sub-menu-subs">
                        <li><a href="{{route('artworks.single', ['paintings'])}}">All Paintings</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['paintings','acrylic-paintings'])}}">Acrylic</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['paintings','oil-paintings'])}}">Oil</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['paintings','landscape-paintings'])}}">Landscape</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['paintings','black-and-white-paintings'])}}">Black and White</a></li>
                    </ul>
                </ul>
                <ul class="footer-sub-menu">
                    <li class="footer-sub-menu-macro">Sculptures</li>
                    <ul class="footer-sub-menu-subs">
                        <li><a href="{{route('artworks.single', ['sculpture'])}}">All Sculptures</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['sculpture','wood-sculpture'])}}">Wood</a></li>
                        <li><a href="{{route('artworks.categories.medium', ['sculpture','metal-sculpture'])}}">Metal</a></li>
                    </ul>
                </ul>
                <a class="footer-direct-link" href="{{route('artworks.single', ['photography'])}}">Photography</a>
                <a class="footer-direct-link" href="{{route('artworks.single', ['drawings-and-works-on-paper'])}}">Art drawings</a>
                <a class="footer-direct-link" href="{{route('artworks.single', ['prints'])}}">Prints</a>
                <a class="footer-direct-link" href="{{route('artworks.single', ['design'])}}">Design</a>
                <a class="footer-direct-link" href="{{route('artworks.single', ['multiples'])}}">Multiples</a>

            </div>
            <div class="one-fifths-col">
                <h3 class="footer-section-title">Artists</h3>
                {{--<a class="footer-direct-link" href="{{route('artists.archive.all')}}">All Artists</a>--}}
                <a class="footer-direct-link" href="{{route('artists.single', ['figurative'])}}">Figurative artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['street-art'])}}">Street art artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['geometric'])}}">Geometric artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['pop-art'])}}">Pop art artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['drawing'])}}">Drawing artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['abstract'])}}">Abstract artists</a>
                <a class="footer-direct-link" href="{{route('artists.single', ['conceptual'])}}">Conceptual artists</a>
            </div>
            <div class="one-fifths-col">
                <h3 class="footer-section-title">Galleries</h3>
                <a class="footer-direct-link" href="{{route('galleries.single', ['ideel-art'])}}">IdeelArt Gallery</a>
                <a class="footer-direct-link" href="{{route('galleries.single', ['art-for-sale-contemporary-art-gallery-abc-arte'])}}">ABC Arte</a>
                <a class="footer-direct-link" href="{{route('galleries.single', ['art-for-sale-contemporary-art-galleries-art3'])}}">Silas von Morisse Gallery</a>
                <a class="footer-direct-link" href="{{route('galleries.single', ['art-for-sale-contemporary-art-galleries-exhibit-by-aberson'])}}">Exhibit by Aberson</a>
                <a class="footer-direct-link" href="{{route('galleries.archive.all')}}">All Galleries</a>
            </div>
            <div class="one-fifths-col">
                <h3 class="footer-section-title">Magazine</h3>
                @foreach(\App\Models\Post::whereType('magazine')->limit(4)->orderBy('created_at', 'DESC')->get() as $post)
                    <a class="footer-direct-link" href="{{route('posts.single', ['magazine', $post->slug])}}">{{$post->title}}</a>
                @endforeach
                <a class="footer-direct-link" href="{{route('posts.archive.all', ['magazine'])}}">All Magazine Articles</a>
            </div>
            <div class="one-fifths-col footer-my-account-menu">
                <h3 class="footer-section-title">My Account</h3>
                <a class="footer-direct-link" href="{{route('users.collections')}}"><b>My Collection</b></a>
                <a class="footer-direct-link" href="{{route('users.favourite.artworks')}}"><b>Saved Artworks</b></a>
                <a class="footer-direct-link" href="{{route('users.favourite.artists')}}"><b>Saved Artists</b></a>
                <a class="footer-direct-link" href="{{route('users.favourite.galleries')}}"><b>Saved Galleries</b></a>
                <a class="footer-direct-link" href="{{route('users.profile')}}#orders"><b>My orders</b></a>
                <a class="footer-direct-link" href="{{route('users.profile')}}#profile"><b>My profile</b></a>
            </div>
        </div>

        <br><br>
        <script language="JavaScript" type="text/javascript">
            function footerMenuDropdown(){

                var footerSubMenus = $('.footer-sub-menu');

                footerSubMenus.each(function( index, element ) {
                    var thisSubMenu = $( element ).find('.footer-sub-menu-macro');
                    thisSubMenu.on('click', function() {
                        // console.log('cliccato su sub');
                        $( element ).toggleClass( "submenu-opened" );
                    });
                });
            }
            function footerMacroMenuDropdown(){

                var footerMacroMenus = $('#main-footer .one-fifths-col');

                footerMacroMenus.each(function( index, element ) {
                    var thisSubMenu = $( element ).find('.footer-section-title');
                    // console.log(thisSubMenu);

                    thisSubMenu.on('click', function() {
                        // console.log('cliccato su macro');
                        $( element ).toggleClass( "macromenu-opened" );
                    });
                });
            }
            window.onload = function() {
                footerMenuDropdown();
                footerMacroMenuDropdown();
            };
        </script>



        <div id="credits">
            <div class="credits-txt">
                <p>© <?php echo date("Y"); ?> Kooness - C.F./P.iva 02439970209 - Developed by <a href="http://www.villa-consulting.it/">Villa Consulting</a>, <a title="Notomia" href="https://www.notomia.com/">Notomia</a></p>
            </div>
            <div class="credits-app">
                <a href="https://itunes.apple.com/us/app/kooness/id963773992?mt=8">
                <img src="/images/app-store.svg" />
                </a>
                {{-- <a href="https://play.google.com/store/apps/details?id=com.sma.kooness">
                    <img src="/images/google-play.svg" />
                </a> --}}
            </div>
        </div>
    </div>
</footer>