@extends('errors::layout')

@section('title', 'Site under maintenance')

@section('message', "we'll get back to you as soon as possible!")