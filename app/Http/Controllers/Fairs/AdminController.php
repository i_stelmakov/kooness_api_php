<?php

namespace App\Http\Controllers\Fairs;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\SiteMap;
use App\Models\User;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use View;

class AdminController extends Controller
{

    private $_directory_images = 'fairs';

    private $_type_url = 'Fair';

    private $_url_prefix = '/fairs/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('fairs.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'slug',
            3 => 'start_date',
            4 => 'end_date',
            5 => 'status',
            6 => 'created_at',
            7 => 'options',
        );

        $totalData = Fair::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $fairs = Fair::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $fairs = Fair::where('name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Fair::where('name', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($fairs)) {
            foreach ($fairs as $fair) {
                $edit = route('admin.fairs.edit', $fair->id);
                $delete = route('admin.fairs.destroy', $fair->id);
                $upload = route('admin.fairs.floorplant', $fair->id);

                $nestedData['id'] = $fair->id;
                $nestedData['name'] = $fair->name;
                $nestedData['slug'] = $fair->slug;
                $nestedData['start_date'] = date('d/m/Y', strtotime($fair->start_date));
                $nestedData['end_date'] = date('d/m/Y', strtotime($fair->end_date));
                $nestedData['status'] = get_state_status($fair->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($fair->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete, 'upload' => $upload]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $fair = new Fair();
        $galleries = Gallery::select("id", "name")->get();
        $countries = Country::all();
        return view('fairs.admin.create', compact('fair', 'galleries', 'countries'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['created_by'] = Auth::user()->id;
            $data['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
            $data['end_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
            if (isset($data['times'])) {
                foreach ($data['widget-time'] as $time) {
                    $times[] = $time;
                }
                $data['times'] = json_encode($times);
            }
            $fair = Fair::create($data);
            if ($fair) {
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $fair, '\App\Models\Gallery', 'galleries', 'gallery_ids');

                $this->createUser($fair, $data);

                SiteMap::create([
                    "url" => $this->_url_prefix . $fair->slug,
                    "type" => $this->_type_url,
                    "referer_id" => $fair->id
                ]);

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $fair->id,
                    'redirectTo' => route('admin.fairs.index')
                ];
                $request->session()->flash("success", "Fair with name `{$fair->name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($fair)
    {
        if(roleCheck('fair')){
            $fair_user = Auth::user()->fair()->first();
            if($fair != $fair_user->id){
                return redirect()->route('admin.dashboard');
            }
        }
        $fair = Fair::find($fair);
        $fair->start_date = date('d/m/Y', strtotime($fair->start_date));
        $fair->end_date = date('d/m/Y', strtotime($fair->end_date));
        $fair->times = json_decode($fair->times);
        $galleries = Gallery::select("id", "name")->get();
        $countries = Country::all();
        return view('fairs.admin.edit', compact('fair', 'galleries', 'countries'));
    }

    public function update(Request $request, $fair)
    {
        if(roleCheck('fair')){
            $fair_user = Auth::user()->fair()->first();
            if($fair != $fair_user->id){
                return redirect()->route('admin.dashboard');
            }
        }
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $fair = Fair::find($fair);
        if ($request->method() == 'PATCH' && $fair) {
            $data = Input::all();

            if(isset($data['start_date'])) {
                $data['start_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
            }
            if(isset($data['end_date'])) {
                $data['end_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
            }

            if (isset($data['widget-time'])) {
                $times = [];
                foreach ($data['widget-time'] as $time) {
                    $times[] = $time;
                }
                $data['times'] = json_encode($times);
            } else{
                $data['times'] = null;
            }
            if ($fair->update($data)) {
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $fair, '\App\Models\Gallery', 'galleries', 'gallery_ids');

                // associo l'utenza salvata
                $user = $fair->user()->first();
                if( !$user ) {
                    $this->createUser($fair, $data);
                }
                else {
                    // abilitazione utenza se box checcato e con valore
                    if( $request->has('fair_status') && $request->get('fair_status') == 1 ) {
                        $user->status = 1;
                        $user->user_first_enable = 1;
                        // all'update, se si seleziona la creazione dell'utenza si deve riassociare la psw
                        $password = str_random(10);
                        $user->password = bcrypt($password);
                        $user->save();

                        // identifico il tipo di utenza che ha creato il nuovo utente
                        if( roleCheck('gallerist') ) {
                            $user_kind_with_article = 'A Gallerist';
                        } elseif( roleCheck('fair') ) {
                            $user_kind_with_article = 'A Fair';
                        } else {
                            $user_kind_with_article = 'An Administrator';
                        }

                        // mando mail
                        if ($user && $user->status == 1 ) {

                            $to = $user->email;
                            $name = $user->full_name;
                            $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                            $extras = array(
                                'username' => $user->username,
                                'password' => $password,
                                'user_kind_with_article' => $user_kind_with_article
                            );

                            Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                                $message->to($to, $name)->subject($subject);
                            });
                        }

                    }
                }

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($fair->id)->first();
                $site_map->url = $this->_url_prefix . $fair->slug;
                $site_map->save();

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $fair->id,
                    'redirectTo' => route('admin.fairs.index')
                ];
                $request->session()->flash("success", "Fair with name `{$fair->name}` is successful updated!");
            }
        }
        return response()->json($response);
    }

    public function destroy($fair)
    {
        $fair = Fair::find($fair);
        if (Storage::disk()->exists(public_path("uploads/fairs/{$fair->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/fairs/{$fair->id}"));
        }
        $fair->user()->delete();
        $fair->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($fair->id)->first();
        $site_map->delete();

        create_redirect_301_on_delete(parse_url(route('fairs.single', [$fair->slug]), PHP_URL_PATH), parse_url(route('fairs.archive.all'), PHP_URL_PATH));

        Session::flash('success', "Fair with name `{$fair->name}` is successful removed!");
        return redirect(route('admin.fairs.index'));
    }

    private function createUser($fair, $data)
    {
        if (isset($data['created_user']) && $data['created_user'] == '1') {
            $password = str_random(10);
            $username = strtolower($data['first_name'] . "." . $data['last_name']);
            $check = User::whereUsername($username)->count();
            if ($check > 0) {
                $username .= "." . str_random(6);
            }

            $activation_code = hash_hmac('sha256', str_random(40), config('app.key'));
            $user = User::create([
                'username' => strtolower($username),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($password),
                'activation_code' => $activation_code,
                'status' => ($data['fair_status']) ? 1 : 0,
                'user_first_enable' => ($data['fair_status']) ? 1 : 0
            ]);

            $user->assignRole('fair');

            $user->fair()->save($fair);

            // identifico il tipo di utenza che ha creato il nuovo utente
            if( roleCheck('gallerist') ) {
                $user_kind_with_article = 'A Gallerist';
            } elseif( roleCheck('fair') ) {
                $user_kind_with_article = 'A Fair';
            } else {
                $user_kind_with_article = 'An Administrator';
            }

            if ($user && $user->status == 1 ) {

                $to = $user->email;
                $name = $user->full_name;
//                $subject = "Hi {$user->full_name}! Welcome to Kooness.com";
                $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                $link = url("activate/$activation_code");
                $extras = array(
                    'username' => $user->username,
                    'password' => $password,
                    'user_kind_with_article' => $user_kind_with_article
                );

                Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });

            }
        }
    }

    public function floorplantManagement(Request $request, $id){
//        dd($request->all());
        $destinationPath = storage_path('/uploads/floorplants');
        $fair = Fair::find($id);
        if($fair) {
            if($request->method() == 'POST') {

                // se ne esiste già uno elimina il precedente da S3
                if($fair->floorplant_pdf_url) {
                    $floorplant_pdf_name = str_replace(env('CLOUD_FRONT_URL'), '', $fair->floorplant_pdf_url );
                    Storage::disk()->delete($floorplant_pdf_name);
                }
                if($fair->floorplant_preview_url) {
                    $floorplant_preview_name = str_replace(env('CLOUD_FRONT_URL'), '', $fair->floorplant_preview_url );
                    Storage::disk()->delete($floorplant_preview_name);
                }

                $this->validate($request, [
                    'pdf_floorplant' => 'required|mimes:pdf|max:200000',
                ]);

                // upload PDF
                $pdf = $request->file('pdf_floorplant');
                $pdf_file_name = 'fair_floorplant.pdf';

                $preview_file_name = 'fair_floorplant_preview.png';
                $preview_stream = $request->get('fair_floorplant_preview');

                // caricamento dei files su Amazon S3
                $directory = 'fairs';

                // se esiste cartella '/uploads/fair' altrimenti creala
                if (Storage::disk()->exists("/uploads/$directory") || Storage::disk()->makeDirectory("/uploads/$directory")) {
                    // se esiste cartella '/uploads/fair/{id}' altrimenti creala
                    if (Storage::disk()->exists("/uploads/$directory/$id") || Storage::disk()->makeDirectory("/uploads/$directory/$id")) {
                        // se esiste cartella '/uploads/fair/{id}/floorplant' altrimenti creala
                        if (Storage::disk()->exists("/uploads/$directory/$id/floorplant") || Storage::disk()->makeDirectory("/uploads/$directory/$id/floorplant")) {

                            // definisco path file PDF con data parsata nel nome
                            $path_pdf = "/uploads/$directory/$id/floorplant/" . substr(sha1(time()), 0, 8) . "_" . sanitize_file_name_chars($pdf_file_name);
                            // mando il file PDF su S3
                            Storage::disk()->put($path_pdf, file_get_contents($pdf), 'public');

                            // definisco path file PNG (preview) con data parsata nel nome
                            $path_preview = "/uploads/$directory/$id/floorplant/" . substr(sha1(time()), 0, 8) . "_" . sanitize_file_name_chars($preview_file_name);
                            // check sul base 64, esclude i due campi precedenti
                            $uri = substr($preview_stream, strpos($preview_stream, ",") + 1);
                            // mando il file PNG su S3
                            Storage::disk()->put($path_preview, base64_decode($uri), 'public');

                            // adatto i paths per CloudFront
                            if (env('FILESYSTEM_DRIVER', 'local') == 's3') {
                                // adatto il path del PDF
                                $path_pdf = env('CLOUD_FRONT_URL', '') . $path_pdf;
                                // adatto il path della Preview
                                $path_preview = env('CLOUD_FRONT_URL', '') . $path_preview;
                            }

                            // salvo i nuovi paths sul DB
                            $fair->floorplant_pdf_url = $path_pdf;
                            $fair->floorplant_preview_url = $path_preview;
                            $fair->save();

                            return back()->with('success','Pdf Upload successful');
                        }
                    }
                }
            }
            $pdf_preview = $fair->floorplant_preview_url;
            return view('fairs.admin.floorplant', compact('pdf_preview'));
        } else {
            return redirect()->route('admin.fairs.index');
        }
    }



}
