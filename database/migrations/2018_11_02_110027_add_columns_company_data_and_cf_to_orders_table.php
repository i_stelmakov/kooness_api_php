<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCompanyDataAndCfToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('billing_company_name')->nullable()->after('billing_last_name');
            $table->string('billing_fiscal_code')->nullable()->after('billing_company_name');
            $table->string('billing_vat_number')->nullable()->after('billing_fiscal_code');
            $table->string('billing_first_name')->nullable()->change();
            $table->string('billing_last_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('billing_company_name');
            $table->dropColumn('billing_fiscal_code');
            $table->dropColumn('billing_vat_number');
            $table->string('billing_first_name')->nullable(false)->change();
            $table->string('billing_last_name')->nullable(false)->change();
        });
    }
}
