<?php

//-----------------------------------------------------
// Generics Breadcumbs
//-----------------------------------------------------

Breadcrumbs::register('test.isotope', function ($breadcrumbs) {
    $breadcrumbs->push('Test Isotope', route('test.isotope'));
});
Breadcrumbs::register('test.isotope.sorting', function ($breadcrumbs) {
    $breadcrumbs->push('Test Isotope Sorting', route('test.isotope.sorting'));
});
Breadcrumbs::register('test.grid', function ($breadcrumbs) {
    $breadcrumbs->push('Test Grid', route('test.grid'));
});
Breadcrumbs::register('test.dhl', function ($breadcrumbs) {
    $breadcrumbs->push('Test dhl', route('test.dhl'));
});
Breadcrumbs::register('test.search', function ($breadcrumbs) {
    $breadcrumbs->push('Test search', route('test.search'));
});

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('register', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Register', route('register'));
});

Breadcrumbs::register('password.request', function ($breadcrumbs) {
    $breadcrumbs->parent('register');
    $breadcrumbs->push('Reset your password');
});

Breadcrumbs::register('password.reset', function ($breadcrumbs) {
    $breadcrumbs->parent('register');
    $breadcrumbs->push('Change your password');
});


//-----------------------------------------------------
// Admin Breadcumbs
//-----------------------------------------------------

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

Breadcrumbs::register('admin.main.page', function ($breadcrumbs, $section) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Management Intro', route('admin.main.page', [$section]));
});

// Artworks Medium
Breadcrumbs::register('admin.medium.artworks.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artwork Medium', route('admin.medium.artworks.index'));
});

Breadcrumbs::register('admin.medium.artworks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.medium.artworks.index');
    $breadcrumbs->push('Create Artwork Media', route('admin.medium.artworks.create'));
});

Breadcrumbs::register('admin.medium.artworks.edit', function ($breadcrumbs, $artwork) {
    $breadcrumbs->parent('admin.medium.artworks.index');
    $breadcrumbs->push('Edit Artwork Media', route('admin.medium.artworks.edit', [$artwork]));
});

// Artworks Category
Breadcrumbs::register('admin.categories.artworks.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artwork Categories', route('admin.categories.artworks.index'));
});

Breadcrumbs::register('admin.categories.artworks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.artworks.index');
    $breadcrumbs->push('Create Artwork Category', route('admin.categories.artworks.create'));
});

Breadcrumbs::register('admin.categories.artworks.edit', function ($breadcrumbs, $artwork) {
    $breadcrumbs->parent('admin.categories.artworks.index');
    $breadcrumbs->push('Edit Artwork Category', route('admin.categories.artworks.edit', [$artwork]));
});

// Artworks Medium Category
Breadcrumbs::register('admin.artworks.medium.categories.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artwork Medium -> Categories', route('admin.artworks.medium.categories.index'));
});

Breadcrumbs::register('admin.artworks.medium.categories.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.artworks.index');
    $breadcrumbs->push('Create Artwork Medium - Category', route('admin.artworks.medium.categories.create'));
});

Breadcrumbs::register('admin.artworks.medium.categories.edit', function ($breadcrumbs, $artwork) {
    $breadcrumbs->parent('admin.artworks.medium.categories.index');
    $breadcrumbs->push('Edit Artwork Medium - Category', route('admin.artworks.medium.categories.edit', [$artwork]));
});


// Artworks
Breadcrumbs::register('admin.artworks.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artworks List', route('admin.artworks.index'));
});

Breadcrumbs::register('admin.artworks.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.artworks.index');
    $breadcrumbs->push('Create Artwork', route('admin.artworks.create'));
});

Breadcrumbs::register('admin.artworks.edit', function ($breadcrumbs, $artwork) {
    $breadcrumbs->parent('admin.artworks.index');
    $breadcrumbs->push('Edit Artwork', route('admin.artworks.edit', [$artwork]));
});

// Artists Category
Breadcrumbs::register('admin.categories.artists.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artist Categories', route('admin.categories.artists.index'));
});

Breadcrumbs::register('admin.categories.artists.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.artists.index');
    $breadcrumbs->push('Create Artist Category', route('admin.categories.artists.create'));
});

Breadcrumbs::register('admin.categories.artists.edit', function ($breadcrumbs, $artist) {
    $breadcrumbs->parent('admin.categories.artists.index');
    $breadcrumbs->push('Edit Artist Category', route('admin.categories.artists.edit', [$artist]));
});


// Artists
Breadcrumbs::register('admin.artists.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artists', route('admin.artists.index'));
});

Breadcrumbs::register('admin.artists.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.artists.index');
    $breadcrumbs->push('Create Artist', route('admin.artists.create'));
});
Breadcrumbs::register('admin.artists.edit', function ($breadcrumbs, $artist) {
    if( roleCheck('artist')){
        $breadcrumbs->parent('admin.dashboard');
    } else{
        $breadcrumbs->parent('admin.artists.index');
    }
    $breadcrumbs->push('Edit Artist', route('admin.artists.edit', [$artist]));
});

// Galleries Category
Breadcrumbs::register('admin.categories.galleries.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Gallery Categories', route('admin.categories.galleries.index'));
});

Breadcrumbs::register('admin.categories.galleries.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.galleries.index');
    $breadcrumbs->push('Create Gallery Category', route('admin.categories.galleries.create'));
});

Breadcrumbs::register('admin.categories.galleries.edit', function ($breadcrumbs, $gallery) {
    $breadcrumbs->parent('admin.categories.galleries.index');
    $breadcrumbs->push('Edit Gallery Category', route('admin.categories.galleries.edit', [$gallery]));
});

// Galleries Memberships
Breadcrumbs::register('admin.memberships.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Membership list', route('admin.memberships.index'));
});
Breadcrumbs::register('admin.memberships.plans', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.memberships.index');
    $breadcrumbs->push('Memberships plans', route('admin.memberships.plans'));
});
Breadcrumbs::register('admin.memberships.card', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.memberships.index');
    $breadcrumbs->push('Memberships Card', route('admin.memberships.card'));
});
Breadcrumbs::register('admin.memberships.request.change.plan', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.memberships.index');
    $breadcrumbs->push('Memberships Change Plan', route('admin.memberships.request.change.plan'));
});
Breadcrumbs::register('admin.memberships.billing', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.memberships.index');
    $breadcrumbs->push('Memberships Billing', route('admin.memberships.billing'));
});
Breadcrumbs::register('admin.memberships.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.memberships.index');
    $breadcrumbs->push('Billing information', route('admin.memberships.edit'));
});


// Memberships List (for Admin)
Breadcrumbs::register('admin.memberships.list', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Memberships List', route('admin.memberships.list'));
});
Breadcrumbs::register('admin.memberships.payments', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Memberships Payments', route('admin.memberships.payments'));
});

// Galleries
Breadcrumbs::register('admin.galleries.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Galleries', route('admin.galleries.index'));
});

Breadcrumbs::register('admin.galleries.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.galleries.index');
    $breadcrumbs->push('Create Gallery', route('admin.galleries.create'));
});
Breadcrumbs::register('admin.galleries.edit', function ($breadcrumbs, $gallery) {
    if( roleCheck('gallerist')){
        $breadcrumbs->parent('admin.dashboard');
    } else{
        $breadcrumbs->parent('admin.galleries.index');
    }
    $breadcrumbs->push('Edit Gallery', route('admin.galleries.edit', [$gallery]));
});

// Fairs
Breadcrumbs::register('admin.fairs.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Fairs', route('admin.fairs.index'));
});

Breadcrumbs::register('admin.fairs.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.fairs.index');
    $breadcrumbs->push('Create Fair', route('admin.fairs.create'));
});

Breadcrumbs::register('admin.fairs.edit', function ($breadcrumbs, $fair) {
    if( roleCheck('fair')){
        $breadcrumbs->parent('admin.dashboard');
    } else{
        $breadcrumbs->parent('admin.fairs.index');
    }
    $breadcrumbs->push('Edit Fair', route('admin.fairs.edit', [$fair]));
});

Breadcrumbs::register('admin.fairs.floorplant', function ($breadcrumbs, $fair) {
    $breadcrumbs->parent('admin.fairs.index');
    $breadcrumbs->push('Create Fair', route('admin.fairs.floorplant',[$fair]));
});

// Exhibitions
Breadcrumbs::register('admin.exhibitions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Exhibitions', route('admin.exhibitions.index'));
});

Breadcrumbs::register('admin.exhibitions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.exhibitions.index');
    $breadcrumbs->push('Create Exhibition', route('admin.exhibitions.create'));
});

Breadcrumbs::register('admin.exhibitions.edit', function ($breadcrumbs, $exhibition) {
    $breadcrumbs->parent('admin.exhibitions.index');
    $breadcrumbs->push('Edit Exhibition', route('admin.exhibitions.edit', [$exhibition]));
});

// Magazine Category
Breadcrumbs::register('admin.categories.magazine.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Categories Magazine', route('admin.categories.magazine.index'));
});

Breadcrumbs::register('admin.categories.magazine.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.magazine.index');
    $breadcrumbs->push('Create Category', route('admin.magazine.create'));
});

Breadcrumbs::register('admin.categories.magazine.edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('admin.categories.magazine.index');
    $breadcrumbs->push('Edit Category', route('admin.categories.magazine.edit', [$post]));
});

// Magazine
Breadcrumbs::register('admin.magazine.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Magazine', route('admin.magazine.index'));
});

Breadcrumbs::register('admin.magazine.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.magazine.index');
    $breadcrumbs->push('Create Post', route('admin.magazine.create'));
});

Breadcrumbs::register('admin.magazine.edit', function ($breadcrumbs, $gallery) {
    $breadcrumbs->parent('admin.magazine.index');
    $breadcrumbs->push('Edit Post', route('admin.magazine.edit', [$gallery]));
});


// News Category
Breadcrumbs::register('admin.categories.news.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Categories News', route('admin.categories.news.index'));
});

Breadcrumbs::register('admin.categories.news.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.categories.news.index');
    $breadcrumbs->push('Create Category', route('admin.news.create'));
});

Breadcrumbs::register('admin.categories.news.edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('admin.categories.news.index');
    $breadcrumbs->push('Edit Category', route('admin.categories.news.edit', [$post]));
});

// News
Breadcrumbs::register('admin.news.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('News', route('admin.news.index'));
});

Breadcrumbs::register('admin.news.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.news.index');
    $breadcrumbs->push('Create News', route('admin.news.create'));
});

Breadcrumbs::register('admin.news.edit', function ($breadcrumbs, $gallery) {
    $breadcrumbs->parent('admin.news.index');
    $breadcrumbs->push('Edit News', route('admin.news.edit', [$gallery]));
});


// Pages
Breadcrumbs::register('admin.pages.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Pages', route('admin.pages.index'));
});

Breadcrumbs::register('admin.pages.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('admin.pages.index');
    $breadcrumbs->push('Edit Page', route('admin.pages.edit', [$user]));
});

// Tag & Taxonomies
Breadcrumbs::register('admin.tags.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Tag & Taxonomies', route('admin.tags.index'));
});

Breadcrumbs::register('admin.tags.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.tags.index');
    $breadcrumbs->push('Create Tag', route('admin.tags.create'));
});

Breadcrumbs::register('admin.tags.edit', function ($breadcrumbs, $tag) {
    $breadcrumbs->parent('admin.tags.index');
    $breadcrumbs->push('Edit Tag', route('admin.tags.edit', [$tag]));
});

// Promo codes
Breadcrumbs::register('admin.promo-codes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Promo Codes', route('admin.promo-codes.index'));
});

Breadcrumbs::register('admin.promo-codes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.promo-codes.index');
    $breadcrumbs->push('Create Promo Code', route('admin.promo-codes.create'));
});

Breadcrumbs::register('admin.promo-codes.edit', function ($breadcrumbs, $promoCode) {
    $breadcrumbs->parent('admin.promo-codes.index');
    $breadcrumbs->push('Edit Promo Code', route('admin.promo-codes.edit', [$promoCode]));
});

// Orders
Breadcrumbs::register('admin.orders.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Orders', route('admin.orders.index'));
});
Breadcrumbs::register('admin.orders.not.completed', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Orders Pending', route('admin.orders.not.completed'));
});
Breadcrumbs::register('admin.orders.view', function ($breadcrumbs, $order) {
    $breadcrumbs->parent('admin.orders.index');
    $breadcrumbs->push('View Order', route('admin.orders.view', $order));
});
Breadcrumbs::register('admin.orders.current.invoices.number', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.orders.index');
    $breadcrumbs->push('Current invoices number', route('admin.orders.current.invoices.number'));
});

// Users
Breadcrumbs::register('admin.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Users', route('admin.users.index'));
});

Breadcrumbs::register('admin.users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push('Create User', route('admin.users.create'));
});

Breadcrumbs::register('admin.users.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push('Edit User', route('admin.users.edit', [$user]));
});


// Home Layout
Breadcrumbs::register('admin.homepage.layout.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Edit Home Page Layout', route('admin.homepage.layout.edit'));
});

// Seo Data
Breadcrumbs::register('admin.seo-data.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Seo Data for urls', route('admin.seo-data.index'));
});
Breadcrumbs::register('admin.seo-data.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.seo-data.index');
    $breadcrumbs->push('Create Seo Data for url', route('admin.seo-data.create'));
});
Breadcrumbs::register('admin.seo-data.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.seo-data.index');
    $breadcrumbs->push('Edit Seo Data for url', route('admin.seo-data.edit', [$data]));
});


// Globals Configuration
// Edit Free Shipping Configuration
Breadcrumbs::register('admin.configs.free.shipping', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Edit Free Shipping Configuration', route('admin.configs.free.shipping'));
});
Breadcrumbs::register('admin.configs.gallerist.guide', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Manage Gallerist Guide', route('admin.configs.gallerist.guide'));
});


// Redirect 301
Breadcrumbs::register('admin.redirects.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('301 Redirects', route('admin.redirects.index'));
});
Breadcrumbs::register('admin.redirects.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.redirects.index');
    $breadcrumbs->push('Create 301 Redirects', route('admin.redirects.create'));
});
Breadcrumbs::register('admin.redirects.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('admin.redirects.index');
    $breadcrumbs->push('Edit 301 Redirects', route('admin.redirects.edit', [$data]));
});


// Notifications
Breadcrumbs::register('admin.notifications.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Notifications', route('admin.notifications.index'));
});


// Offers
Breadcrumbs::register('admin.offers.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Artworks\' offers', route('admin.offers.index'));
});


// Bulk Uploads 301 Redirects
Breadcrumbs::register('admin.redirects.bulk.upload', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('301 Redirects Bulk Upload', route('admin.redirects.bulk.upload'));
});

// Bulk Uploads Pages
Breadcrumbs::register('admin.pages.bulk.upload', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Pages Bulk Upload', route('admin.pages.bulk.upload'));
});

// Bulk Uploads Google Shop
Breadcrumbs::register('admin.googleshop.bulk.download', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push('Google Shop Bulk Download', route('admin.googleshop.bulk.download'));
});


//-----------------------------------------------------
// Frontend Breadcumbs
//-----------------------------------------------------


// Artworks Intro
Breadcrumbs::register('artworks.intro', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artworks Intro', route('artworks.intro'));
});
// Artworks Archive All
Breadcrumbs::register('artworks.archive.all', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artworks', route('artworks.archive.all'));
});
// Artworks Archive All
Breadcrumbs::register('artworks.categories.medium', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artworks All', route('artworks.archive.all'));
});
// Artworks Single
Breadcrumbs::register('artworks.single', function ($breadcrumbs, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artwork', route('artworks.single', [$slug_name]));
});


// Galleries Intro
Breadcrumbs::register('galleries.intro', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Galleries Intro', route('galleries.intro'));
});
// Galleries Archive All
Breadcrumbs::register('galleries.archive.all', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Galleries All', route('galleries.archive.all'));
});
// Galleries Single
Breadcrumbs::register('galleries.single', function ($breadcrumbs, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Gallery', route('galleries.single', [$slug_name]));
});
// Galleries Interact
Breadcrumbs::register('galleries.request.subscription', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thank ou for contact us', route('galleries.request.subscription'));
});

// Artists Intro
Breadcrumbs::register('artists.intro', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artists Intro', route('artists.intro'));
});
// Artists Archive All
Breadcrumbs::register('artists.archive.all', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artists All', route('artists.archive.all'));
});
// Artists Single
Breadcrumbs::register('artists.single', function ($breadcrumbs, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Artists', route('artists.single', [$slug_name]));
});


// Fairs Intro
Breadcrumbs::register('fairs.intro', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Fairs Intro', route('fairs.intro'));
});
// Fairs Archive All
Breadcrumbs::register('fairs.archive.all', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Fairs All', route('fairs.archive.all'));
});
// Fairs Archive Category
Breadcrumbs::register('fairs.archive.category', function ($breadcrumbs, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Fairs Category', route('fairs.archive.category', [$slug_name]));
});
// Fairs Single
Breadcrumbs::register('fairs.single', function ($breadcrumbs, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Fairs', route('fairs.single', [$slug_name]));
});


// Posts Archive All
Breadcrumbs::register('posts.archive.all', function ($breadcrumbs, $section) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Posts All', route('posts.archive.all', [$section]));
});
// Posts Single
Breadcrumbs::register('posts.single', function ($breadcrumbs, $section, $slug_name) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Posts', route('posts.single', [$section, $slug_name]));
});


// Exhibitions Archive
Breadcrumbs::register('exhibitions.archive', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Exhibitions', route('exhibitions.archive'));
});

// Exhibitions Single
Breadcrumbs::register('exhibitions.single', function ($breadcrumbs, $exhibitions) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Exhibitions', route('exhibitions.single', $exhibitions));
});


// Users
Breadcrumbs::register('users.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('users.profile'));
});

Breadcrumbs::register('users.collections', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('users.collections'));
});
Breadcrumbs::register('users.favourite.galleries', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('users.favourite.galleries'));
});
Breadcrumbs::register('users.favourite.artists', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('users.favourite.artists'));
});
Breadcrumbs::register('users.favourite.artworks', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('users.favourite.artworks'));
});


// Order Cart / Checkout
Breadcrumbs::register('orders.cart', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cart', route('orders.cart'));
});

Breadcrumbs::register('orders.view.checkout', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Purchase', route('orders.cart'));
});
Breadcrumbs::register('orders.pay.order', function ($breadcrumbs, $token) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Pay Order', route('orders.pay.order', [$token]));
});

// Order completed
Breadcrumbs::register('orders.thank.you', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thank You', route('orders.thank.you'));
});

// Order completed
Breadcrumbs::register('orders.thank.you.without.payment', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thank You', route('orders.thank.you.without.payment'));
});


// Order completed
Breadcrumbs::register('orders.payment.error', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Error Payment', route('orders.payment.error'));
});

// Exhibitions Single
Breadcrumbs::register('pages.intro', function ($breadcrumbs, $slug) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Pages', route('pages.intro', [$slug]));
});

// Tags Single

// Tag & Taxonomies
Breadcrumbs::register('tags.single', function ($breadcrumbs, $tag) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tags', route('tags.single', $tag));
});

// Newsletter subscription thank you page
Breadcrumbs::register('subscription.newsletter.thank.you', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Thanks you!', route('subscription.newsletter.thank.you'));
});

// Newsletter subscription thank you page
Breadcrumbs::register('errors.404', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Page not found!', '');
});