@extends('layouts.admin')
@section('title', 'Admin | Create Artist')
@section('page-header', 'Admin - Create Artist')

@section('content')
    @include('artists.admin.form', ['action' => route('admin.artists.store'), 'method' => 'POST'])
@endsection