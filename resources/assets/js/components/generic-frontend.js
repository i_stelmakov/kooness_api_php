$(function () {
    // console.log("Frontend Scripts loaded");

    /* ==========================================================================
    Generiche frontend
    ========================================================================== */

    // 2Do: reactive for offline
    // DEV ONLY - replace images for test
    // var imgs = document.getElementsByTagName("img");
    // for(var i=0, l=imgs.length; i<l; i++){
    //     imgs[i].src = "/images/placeholder.jpg";
    // }
    // console.log('☢️ all images have been replaced!');


    // Mobile Menu
    $(window).on("load", function () {
        // Variables
        var $ = jQuery;
        var subMenusTogglers = $(".sub-menu-toggler");

        // // click on list filter
        subMenusTogglers.click(function( event ) {
            if ($(window).width() <= untilSmall) {
                // console.log('Less or equal' + untilSmall);
                event.preventDefault();
                // console.log( "default " + event.type + " prevented" );
                $(this).parent().parent().toggleClass('sub-menu-active');
                $(this).find('.fa').toggleClass('fa-chevron-up fa-chevron-down');
            }
        });

        $(window).on('beforeunload', function() {
            if ($("body").hasClass("artworks-archive")) {
                $(window).scrollTop(0);
            }
        });

    });


    // Main Nav
    var stickyTop = $('#main-navbar').offset().top;
    var headerHeight = $("#upper-header").height();
    var navbarHeight = $("#main-navbar").height();
    $("#main-nav li ul").parent().children("a").addClass("child-arrow");
    $(window).on('scroll', function(){
        // responsive
        if ($(window).width() <= untilSmall) {
            // console.log('Less or equal ' + untilSmall);
            $('#main-navbar').removeClass("fixed");
            $('#user-tabs .tab-menu').css("margin-top", "unset");
        }
        else {
            // console.log('More or equal ' + untilSmall);
            if ($(window).scrollTop() >= stickyTop) {
                $('#main-navbar').addClass("fixed");
                $('#user-tabs .tab-menu').css("margin-top", headerHeight + 20);
            }
            else {
                $('#main-navbar').removeClass("fixed");
                $('#user-tabs .tab-menu').css("margin-top", "unset");
            }
        }
    });
    $("#main-nav li").hover(function(){
        $(this).find("ul:first").addClass("show");
    },function() {
        $(this).find("ul:first").removeClass("show");
    });
    $("#main-navbar").on("click","#mobile-icon",function(){
        $("#main-nav").toggleClass("show");
    });

    // Tab
    $(".current-tab-item").find("ul").slideDown("fast");
    if($(window).width() >= fromSmall){
        $("#user-tabs .tab-menu").stick_in_parent();
    }
    $(window).resize(function() {
        $("#user-tabs .tab-menu").trigger("sticky_kit:recalc");
    });
    $(".tab-button").on("click",function() {
        var data_id = $(this).data("id");
        $(".tab").removeClass("tab-display");
        $(this).addClass("tab-display");
        $(".tab").removeClass("tab-display");
        $(".tab[data-id='" + data_id + "']").addClass("tab-display");
        $(".tab-button[data-id='" + data_id + "']").addClass("current-tab-item");
        $(".tab-button[data-id='" + data_id + "']").siblings().removeClass("current-tab-item");
        $(".tab-button[data-id='" + data_id + "']").find("ul").slideDown("fast");
        $(".tab-button[data-id='" + data_id + "']").siblings().find("ul").slideUp("fast");
    });
    $("#user-tabs .tab-button").on("click",function() {
        $('html, body').animate({scrollTop:0});
    });
    $('a[href*="#"]:not([href="#"])').click(function() {
        var headerHeight = ($("#main-header").height() + 50);
        var offset = -headerHeight;
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top + offset
                }, 400);
                return false;
            }
        }
    });

    // K-Experience
    $("#k-experience").on("click",function(){
        $("#archive-sidebar").toggleClass("experience");
        $(".hiding-box-header").children(".hiding-box-trigger").addClass("show");
        $(".hiding-box-header").closest(".hiding-box-header").next("div").slideUp("swing");
        $(".slider-item-txt, #category-filter").slideToggle("swing");
        $(".default-slider-section").toggleClass("experience");
        $(".slider-item").toggleClass("experience");
    });

    // Widget Collapse
    $(".hiding-box-header").on("click",function(){
        var $this = $(this);
        $this.children(".hiding-box-trigger").toggleClass("show");
        $this.closest(".hiding-box-header").next("div").slideToggle("swing");
    });

    // Widget List Collapse
    $(".load-more").on("click",function(){
        var $this = $(this);
        $this.closest(".load-more").toggleClass("show");
        $this.prev(".widget-mask").toggleClass("show");
    });

    // Home Box Options
    $(".fa-cog").on("click",function(){
        var $this = $(this);
        var currentContent = $(this).next(".home-box-options");
        $this.next(".home-box-options").slideToggle("fast");
        $('.home-box-options').not(currentContent).slideUp("fast");
        return false;
    });
    $(document).click(function() {
        $('.home-box-options').slideUp("fast");
    });
    

    // Tab based on url function => if hash is present get the hash parameter and open the correct section
    var actualHash = window.location.hash;
    if(actualHash) {
        var allTabs = $(".tab-button");
        allTabs.removeClass('current-tab-item');
        allTabs.each(function( index ) {
            var thisUrl = $(this).find('a').attr('href');
            var thisDataID = $(this).data("id");
            if(thisUrl === actualHash) {
                $(this).addClass('current-tab-item');
                $(".tab-container").removeClass("tab-display");
                $(".tab-container[data-id='" + thisDataID + "']").addClass("tab-display");
            }

        });
        // console.log(actualHash);
    }


    // Accordion function
    var accordionBox = $('.accordion-box');
    $.each(accordionBox, function (index, thisAccordionBox) {
        var $thisAccordionBox = $(thisAccordionBox);
        $thisAccordionBox.find('.accordion-button').click(function(){
            var panelID = $(this).attr("data-accordion");
            var panelContent = $thisAccordionBox.find("[data-slide='" + panelID + "']");
            $thisAccordionBox.find('.accordion-content').removeClass('active');
            setTimeout(function () {
                panelContent.addClass('active');
            }, 400);
        });
    });


    /* ==========================================================================
    Specifica pagina Gallery
    ========================================================================== */
    if ($('body').hasClass('page-gallery-membership')) {
        // form validation su galleries membership
        $('#gallerist-form').validate({
            submitHandler: function (form, event) {
                form.submit();
            }
        })
    }


    /* ==========================================================================
    Globali per Gestione Currency
    ========================================================================== */
    var selectCurrency = $('select.currency-change');
    selectCurrency.attr("disabled", true);
    var currency = [
        "USD",
        "GBP",
    ];
    fx.base = 'EUR';
    var fx_rates = {
        "EUR": 1
    };
    fx.rates = fx_rates;
    $.each(currency, function (index, value) {
        $.ajax({
            url: "/get/currency/" + value,
            dataType: 'json',
            success: function (response) {
                if (response.result === 'success') {
                    fx_rates[value] = response.value;
                    fx.rates = fx_rates;
                    if (index === currency.length - 1) {
                        selectCurrency.attachCurrencyEvent();
                    }
                }
            }
        });
    });


    /* ==========================================================================
    Funzione Follow generica
    ========================================================================== */
    $('body').on('click', '.follow', function () {
        var element = $(this);
        var hasFollowText = element.find('.follow-unfollow');

        var data = {
            id: $(this).data('id'),
            section: $(this).data('section')
        };
        var url = '/user/ajax/add/to/collection';
        if (element.hasClass('active')) {
            url = '/user/ajax/remove/from/collection';
        }
        element.find('i').removeClass('fa-heart fa-heart-o').addClass('fa-spinner fa-spin');
        $.ajax({
            url: url,
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                // se l'elemento è attualmente attivo lo disattivo
                if (element.hasClass('active')) {
                    element.find('i').removeClass('fa-heart fa-spinner fa-spin').addClass('fa-heart-o');
                    if (hasFollowText.length > 0) {
                        hasFollowText.text('Follow');
                    }
                    element.removeClass('active');
                    toastr.options.timeOut = 1000;
                    toastr.success('Element removed from favourites!');
                }
                // se l'elemento è attualmente disattivo lo attivo
                else {
                    element.find('i').removeClass('fa-heart-o fa-spinner fa-spin').addClass('fa-heart');
                    if (hasFollowText.length > 0) {
                        hasFollowText.text('Unfollow');
                    }
                    element.addClass('active');
                    toastr.options.timeOut = 1000;
                    toastr.success('Element added to favourites!');
                }
            }
        });
    });


    /* ==========================================================================
    Funzione Aggiungi a Collezione
    ========================================================================== */
    $('.add-new-collection').click(function () {
        var $input = $(this).parent().find('input[name=collection_name]');
        if ($input.val() === '') {
            $input.css("border-color", "red")
        } else {
            $input.css("border-color", "var(--main-border-color)");
            $.ajax({
                url: '/user/ajax/create/collection',
                data: {name: $input.val()},
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        $input.val('');
                        var $select = $('.collect-form').find('select[name=collection_id]');
                        if ($select.find('option[value=""]').length) {
                            $select.empty();
                        }
                        $select.append('<option value="' + response.collection.id + '">' + response.collection.name + '</option>');
                        $select.val(response.collection.id);
                        $select.trigger('change');
                    }
                }
            });
        }
    });
    $('.collect-form').find('select[name=collection_id]').on('change', function () {
        if ($(this).val() === '') {
            $(this).css("border-color", "red");
        } else {
            $(this).css("border-color", "var(--main-border-color)");
        }
    });
    $('.collection-save').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $form = $('.collect-form');
        if ($form.find('select[name=collection_id]').val() === "") {
            $form.find('select[name=collection_id]').css("border-color", "red");
            return false;
        }
        $.ajax({
            url: '/user/ajax/add/to/collection',
            data: $form.serialize(),
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    $form.find('.collection-close').trigger('click');
                }
            }
        });
    });


    /* ==========================================================================
    Masonry in Sezione Magazines
    ========================================================================== */
    // console.log('check masonry grid...');

    window.myMasonry = false;
    if ($('#masonry-grid').length > 0) {
        console.log('masonry grid exist!');
        myMasonry = $('#masonry-grid').masonry({
            itemSelector: '.grid-item',
            // use element for option
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer',
            // percentPosition: true
        });

        // layout Masonry after each image loads
        myMasonry.imagesLoaded().progress(function () {
            myMasonry.masonry('layout');
        });

        WebFont.load({
            google: {
                families: ['PT Serif', 'Muli']
            },
            active: function () {
                // console.log('fonts loaded!');
                myMasonry.resize();
            }
        });


        
    }


    /* ==========================================================================
    Funzione generica Load More
    ========================================================================== */
    // $('.btn-load-more').click(function () {
    //     hidePage();
    //
    //     var that = $(this);
    //     var section = $(this).data('section');
    //     var url = '/' + section + '/load/more';
    //     var tagged = $(this).data('tagged');
    //     // console.log(tagged);
    //     if (tagged === undefined) {
    //         if (section === 'news' || section === 'magazine') {
    //             url = '/posts/' + section + '/load/more';
    //         }
    //     } else {
    //         url = '/tags/' + section + '/load/more';
    //     }
    //     var offset = $(this).data('offset');
    //     var limit = $(this).data('limit');
    //     $.ajax({
    //         url: url,
    //         type: 'POST',
    //         dataType: 'json',
    //         data: {
    //             offset: offset,
    //             category: $(that).data('category'),
    //             filter_type: $(that).data('filter-type'),
    //             filter_value: $(that).data('filter-value'),
    //             filter_alpha: $(that).data('filter-alpha'),
    //             filter_location: $(that).data('filter-location'),
    //             filter_category: $(that).data('filter-category'),
    //             tagged: tagged,
    //
    //         },
    //         complete: function (response) {
    //             // console.log('Complete!');
    //         },
    //         error: function (response) {
    //             // console.log('Error!');
    //         },
    //         success: function (response) {
    //             if (response.result === 'success') {
    //                 $.each(response.item_list, function (index, value) {
    //                     var $container = $('.container-load');
    //                     if (section === 'artists') {
    //                         $container.append(getArtistTemplate(value));
    //                     } else if (section === 'galleries') {
    //                         $container.append(getGalleryTemplate(value));
    //                     } else if (section === 'news') {
    //                         if (tagged === undefined) {
    //                             $container.append(getNewsTemplate(value));// Usata in news view archive
    //                         } else {
    //                             $container.append(getMagazineTemplate(value)); // Usata in tag news view archive
    //                         }
    //                     } else if (section === 'magazine') {
    //                         if (tagged === undefined) {
    //                             // $container.append(getMagazineTemplate(value)); // Usata in magazine view archive
    //                             if (myMasonry) {
    //                                 // console.log('masonry grid exist 2!');
    //
    //                                 var array_return = [];
    //                                 var tpl = $(magazineTemplate(value));
    //                                 array_return.push(tpl[0]);
    //
    //                                 var elems = array_return;
    //                                 var $elems = $(elems);
    //                                 myMasonry.append($elems).masonry('appended', $elems);
    //
    //                                 // layout Masonry after each image loads
    //                                 myMasonry.imagesLoaded().progress(function () {
    //                                     myMasonry.masonry('layout');
    //                                 });
    //                             }
    //
    //                         } else {
    //                             // $container.append(getMagazineTemplate(value)); // Usata in tag magazine view archive
    //                             $(magazineTagsTemplate(value)).appendTo($container);
    //                         }
    //                     } else if (section === 'exhibitions') {
    //                         $container.append(getExhibitionTemplate(value));
    //                     } else if (tagged !== undefined && section === 'artworks') {
    //                         $container.append(getArtworkTemplate(value));
    //                     }
    //                 });
    //
    //
    //                 // conditionally hide load more
    //                 if(response.hide_load_more == true) {
    //                     console.log('No more items, hide load more');
    //                     that.hide();
    //                 } else {
    //                     console.log('More items present, show load more');
    //                     that.show();
    //                 }
    //
    //                 that.data('offset', (limit + offset));
    //             }
    //             showPage();
    //         }
    //     })
    // });

    /* ==========================================================================
    Funzione generica View all
    ========================================================================== */
    $('.btn_view_all').click(function () {
        var filter = $(this).data();
        var link = $(this).find('a').attr('href');
        $(this).find('a').on('click', function () {
            return false
        });
        if (filter.alphaFilter !== '' || filter.locationFilter !== '' || filter.categoryFilter !== '') {
            link += '?';
            var filters = [];
            if (filter.alphaFilter !== '') {
                filters.push('startWith=' + filter.alphaFilter);
            }
            if (filter.locationFilter !== undefined && filter.locationFilter !== '') {
                if (filter.categoryFilter === undefined) {
                    filters.push('nationality=' + filter.locationFilter);
                } else {
                    filters.push('location=' + filter.locationFilter);
                }
            }
            if (filter.categoryFilter !== undefined && filter.categoryFilter !== '') {
                filters.push('category=' + filter.categoryFilter);
            }
            var link_query = filters.join('&');
            link += link_query;
        }
        window.location = link;
        return false;
    });

    /* ==========================================================================
    Funzioni di filtraggio in Sezioni Intro
    ========================================================================== */
    $('.galleries-intro select.search-by').on('change', function () {
        filter(this, '/galleries/search/by', $('.galleries-container'), getGalleryTemplate);
    });
    $('.galleries-intro a.search-by').on('click', function () {
        filter(this, '/galleries/search/by', $('.galleries-container'), getGalleryTemplate);
    });
    $('.artists-intro a.search-by').on('click', function () {
        filter(this, '/artists/search/by', $('.artists-container'), getArtistTemplate);
    });
    $('.artists-intro select.search-by').on('change', function () {
        filter(this, '/artists/search/by', $('.artists-container'), getArtistTemplate);
    });

    /* ==========================================================================
    Funzioni di filtraggio in Archivio
    ========================================================================== */
    var $containerGalleryArchive = $('.galleries-archive');
    var $containerArtistArchive = $('.artists-archive');
    $containerGalleryArchive.on('change', 'select.search-by', function () {
        if ($(this).hasClass('location')) {
            $('#archive').data('filter-location', $(this).val());
        } else {
            $('#archive').data('filter-category', $(this).val());
        }
        filterArchive();
    });
    $containerGalleryArchive.on('click', 'a.search-by', function () {
        $('#archive').data('filter-alpha', $(this).data('value'));
        filterArchive();
    });

    $containerArtistArchive.on('click', 'a.search-by', function () {
        $('#archive').data('filter-alpha', $(this).data('value'));
        filterArchive();
    });

    $containerArtistArchive.on('change', 'select.search-by', function () {
        if ($(this).hasClass('location')) {
            $('#archive').data('filter-location', $(this).val());
        }
        filterArchive();
    });

    var filterAlpha = $('#archive').data('filter-alpha');
    if (filterAlpha !== '') {
        $containerGalleryArchive.find('a.search-by[data-value="' + filterAlpha + '"]').addClass('active');
        $containerArtistArchive.find('a.search-by[data-value="' + filterAlpha + '"]').addClass('active');
    }

    /* ==========================================================================
    Funzione per fare Offerta per Opera
    ========================================================================== */
    var makeAnOfferModal = $('[data-remodal-id=modal-make-an-offer]').remodal();
    var buttonMakeAnOffer = $('.make-an-offer');
    buttonMakeAnOffer.click(function () {
        makeAnOfferModal.open();
    });
    var $form = $('.make-an-offer-form');
    $form.validate({
        rules: {
            amount: {
                required: true,
                number: true,
                min: 0.01
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: '/user/ajax/make/offer',
                data: $(form).serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    $(form).find('button[type=submit]').attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i> Sending...');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        makeAnOfferModal.close();
                        $(form).find('.reset').val('');
                        buttonMakeAnOffer.unbind('click').text('OFFER SENT');
                        toastr.success('Message correctly sent!');
                    }
                },
                complete: function () {
                    $(form).find('button[type=submit]').attr('disabled', false).html('Send');
                }
            })
        }
    });





    /* ==========================================================================
    Funzione per Available on Fair
    ========================================================================== */
    var availableInFairModal = $('[data-remodal-id=modal-available-in-fair]').remodal();
    var buttonAvailableInFair = $('.available-in-fair');
    buttonAvailableInFair.click(function () {
        availableInFairModal.open();
    });
    var $form = $('.available-in-fair-form');
    $form.validate({
        rules: {
            amount: {
                required: true,
                number: true,
                min: 0.01
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: '/user/ajax/info/available',
                data: $(form).serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    $(form).find('button[type=submit]').attr('disabled', true).html('<i class="fa fa-spin fa-spinner"></i> Sending...');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        availableInFairModal.close();
                        $(form).find('.reset').val('');
                        buttonAvailableInFair.unbind('click').text('REQUEST SENT');
                        toastr.success('Message correctly sent!');
                    }
                },
                complete: function () {
                    $(form).find('button[type=submit]').attr('disabled', false).html('Send');
                }
            })
        }
    });











    /* ==========================================================================
    Funzione per applicare Codice Promozionale
    ========================================================================== */
    $('.add-promo-code').click(function () {
        var that = $(this);
        if ($('input.promo-code').val()) {
            $.ajax({
                url: '/apply/promo/code',
                type: 'POST',
                dataType: 'json',
                data: {code: $('input.promo-code').val()},
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    // console.log('Error!');
                },
                success: function (response) {
                    var $element = that.parents('#default-sheet-buy').find('h4[data-currency]');
                    var $select = that.parents('#default-sheet-buy').find('select.currency-change');
                    var price = parseFloat($element.data('price'));
                    var current = $element.data('currency');
                    var new_currency = $select.val();
                    var new_val = fx.convert(price, {from: current, to: new_currency});
                    var symbol = '€';
                    if (new_currency === "USD") {
                        symbol = '$';
                    } else if (new_currency === "GBP") {
                        symbol = '£';
                    }
                    new_val = (new_val * (1 - (response.coupon.value / 100)));
                    $element.text(new_val.numberFormat(2, ',', '') + " " + symbol);
                    $element.attr('data-currency', new_currency);
                    var $rowCode = $('.row-code');
                    $('.row-insert-code').hide();
                    $rowCode.find('p').text(response.coupon.name + ": " + response.coupon.value + "%");
                    $rowCode.find('p').data("value", response.coupon.value);
                    $rowCode.show();
                }
            });
        }
    });

    $('.remove-promo-code').click(function () {
        var that = $(this);
        var $element = that.parents('#default-sheet-buy').find('h4[data-currency]');
        var $select = that.parents('#default-sheet-buy').find('select.currency-change');
        var price = parseFloat($element.data('price'));
        var current = $element.data('currency');
        var new_currency = $select.val();
        var new_val = fx.convert(price, {from: current, to: new_currency});
        var symbol = '€';
        if (new_currency === "USD") {
            symbol = '$';
        } else if (new_currency === "GBP") {
            symbol = '£';
        }
        $element.text(new_val.numberFormat(2, ',', '') + " " + symbol);
        $element.attr('data-currency', new_currency);
        var $rowCode = $('.row-code');
        var $rowInsert = $('.row-insert-code');
        $rowCode.hide();
        $rowCode.find('p').text('');
        $rowCode.find('p').data("value", "0");
        $rowInsert.find('input').val('');
        $rowInsert.show()
    });

    $('#order-works-artist').on('change', function () {
        location.href = location.origin + location.pathname + '?works-order-by=' + $(this).val();
    })
});


/* ==========================================================================
Funzione per creazione link filtraggio archvio
========================================================================== */

window.filterArchive = function () {
    var filter = $('#archive').data();
    var link = window.location.pathname;

    if ((filter.filterAlpha !== '' && filter.filterAlpha !== 'all') || filter.filterType !== '' || (filter.filterLocation !== undefined && filter.filterLocation !== '') || (filter.filterCategory !== undefined && filter.filterCategory !== '')) {
        link += '?';
        var filters = [];
        if (filter.filterType !== '') {
            filters.push(filter.filterType + "=" + filter.filterValue);
        }
        if (filter.filterAlpha !== '' && filter.filterAlpha !== 'all') {
            filters.push('startWith=' + filter.filterAlpha);
        }
        if (filter.filterLocation !== undefined && filter.filterLocation !== '') {
            if (filter.filterCategory === undefined) {
                filters.push('nationality=' + filter.filterLocation);
            } else {
                filters.push('location=' + filter.filterLocation);
            }
        }
        if (filter.filterCategory !== undefined && filter.filterCategory !== '') {
            filters.push('category=' + filter.filterCategory);
        }
        var link_query = filters.join('&');
        link += link_query;
    }
    window.location = link;
    return false;
};

/* ==========================================================================
Generatore Template Gallery per LoadMore
========================================================================== */
window.getGalleryTemplate = function (value) {

    var $tpl = '<div data-id="' + value.id + '" class="slider-item one-third-col-with-margin">' +
        '<i title="Remove" class="fa fa-times delete-item"> </i>';

    // se utente è connesso
    if (value.auth_user) {
        // se l'artista è seguito
        if (value.follow_status) {
            $tpl += '<div class="add-item follow active" data-section="galleries" data-id="' + value.id + '"><i class="fa fa-heart"></i></div>';
        }
        // se l'artista NON è seguito
        else {
            $tpl += '<div class="add-item follow " data-section="galleries" data-id="' + value.id + '"><i class="fa fa-heart-o"></i></div>';
        }
    }

    $tpl += '<div class="slider-item-img wide-box">' +
        '<a title="' + value.name + '" href="' + value.url + '">' +
        '<div class="background-cover gallery" style="background-image: url(' + value.image + ')"></div>' +
        '</a>' +
        '</div>' +
        '<div class="slider-item-txt">' +
        '<div class="default-sheet-row slider-item-row">' +
        '<div class="default-sheet-row-cell">' +
        '<h2><a title="' + value.name + '" class="dark-text" href="' + value.url + '">' + value.name + '</a></h2>' +
        '</div></div>' +
        '<div class="default-sheet-row slider-item-row">' +
        '<div class="default-sheet-row-cell">' +
        '<p class="gallery-adress">' + value.city + ', ' + value.address + '</p>' +
        '</div></div>' +
        '<a title="' + value.name + '" class="read-more" href="' + value.url + '">read more</a>' +
        '</div></div>';

    return $tpl;
};


/* ==========================================================================
Generatore Template Artist per LoadMore
========================================================================== */
window.getArtistTemplate = function (value) {
    var $tpl = '<div data-id="' + value.id + '" class="slider-item one-fourth-col-with-margin half-width-mobile">' +
        '<i title="Remove" class="fa fa-times delete-item"></i>';

    // se utente è connesso
    if (value.auth_user) {
        // se l'artista è seguito
        if (value.follow_status) {
            $tpl += '<div class="add-item follow active" data-section="artists" data-id="' + value.id + '"><i class="fa fa-heart"></i></div>';
        }
        // se l'artista NON è seguito
        else {
            $tpl += '<div class="add-item follow " data-section="artists" data-id="' + value.id + '"><i class="fa fa-heart-o"></i></div>';
        }
    }

    $tpl += '<div class="slider-item-img square-box">' +
        '<a title="' + value.name + '" href="' + value.url + '">' +
        '<div class="background-cover gallery" style="background-image: url(' + value.image + ')"></div>\n' +
        '</a></div>' +
        '<div class="slider-item-txt">' +
        '<div class="default-sheet-row slider-item-row">' +
        '<div class="default-sheet-row-cell">' +
        '<h2><a title="' + value.name + '" class="dark-text" href="' + value.url + '">' + value.name + '</a></h2>' +
        '</div></div>';

    if (value.country != null)
        $tpl += '<p class="dark-text provenance">' + value.country + '</p>';

    $tpl += '<p class="exposure">' + value.count + ' Works exhibited</p>' +
        '<a title="Lorem Ipsum" class="read-more" href="' + value.url + '">Read more</a>\n' +
        '</div></div>';

    return $tpl;
};


/* ==========================================================================
Generatore Template Exhibitions per LoadMore
========================================================================== */
// window.getExhibitionTemplate = function (value) {
//     return '<div class="slider-item one-third-col-with-margin">' +
//         '<div class="slider-item-img wide-box">' +
//         '<a title="' + value.name + '" href="' + value.url + '">' +
//         '<div class="background-cover gallery" style="background-image: url(' + value.image + ')"></div>\n' +
//         '</a>' +
//         '</div>' +
//         '<div class="slider-item-txt">' +
//         '<h2><a title="' + value.name + '" class="dark-text" href="' + value.url + '">' + value.name + '</a></h2>' +
//         '<div class="event-date">' +
//         '<p class="event-date-p">' + value.city + ', ' + value.country_code + '</p>\n' +
//         '</div>' +
//         '<div class="default-sheet-row slider-item-row">' +
//         '<div class="default-sheet-row-cell">' +
//         '<p class="dark-text event-adress">From ' + value.start_date + ' to ' + value.end_date + '</p>' +
//         '</div></div>' +
//         '<a title="Read more" class="read-more" href="' + value.url + '">Read more</a>\n' +
//         '</div></div>';
// };


/* ==========================================================================
Generatore Template News per LoadMore
========================================================================== */
// window.getNewsTemplate = function (value) {
//     return '<div class="gallery-news-item col one-half-col-with-margin">' +
//         '<div class="gallery-news-item-img">' +
//         '<a title="' + value.name + '" href="' + value.url + '" class="background-cover square-box" style="background-image: url(' + value.image + ')"></a>\n' +
//         '</div>' +
//         '<div class="gallery-news-item-txt">' +
//         '<div>' +
//         '<p class="news-date">' + value.date + '</p>' +
//         '<div class="default-sheet-row">' +
//         '<h4 class="no-margin"><a title="' + value.name + '" class="dark-text" href="' + value.name + '">' + value.url + '</a></h4>' +
//         '</div>' +
//         '<p>' + value.content + '</p>\n' +
//         '</div>' +
//         '<a title="' + value.name + '" class="read-more" href="' + value.url + '">Read more</a>' +
//         '</div></div>'
// };


/* ==========================================================================
Generatore Template Artwork per LoadMore
========================================================================== */
// window.getArtworkTemplate = function (value) {
//     var $tpl = '<div class="slider-item one-fourth-col-with-margin half-width-mobile">' +
//         '<i title="Remove" class="fa fa-times delete-item"> </i>';
//
//     // se utente è connesso
//     if (value.auth_user) {
//         // se l'artista è seguito
//         if (value.follow_status) {
//             $tpl += '<div class="add-item follow active" data-section="artworks" data-id="' + value.id + '"><i class="fa fa-heart"></i></div>';
//         }
//         // se l'artista NON è seguito
//         else {
//             $tpl += '<div class="add-item follow " data-section="artworks" data-id="' + value.id + '"><i class="fa fa-heart-o"></i></div>';
//         }
//     }
//
//     // old method - used in Tags and others
//     $tpl += '<div class="slider-item-img square-box">' +
//         '<a title="' + value.name + '" href="' + value.url + '">' +
//         '<div class="background-cover gallery" style="background-image: url(' + value.image + ')"></div>' +
//         '</a>' +
//         '</div>' +
//         '<div class="slider-item-txt">' +
//         '<div class="default-sheet-row slider-item-row artist-name">' +
//         '<div class="default-sheet-row-cell">' +
//         '<h3><a title="' + value.artist_name + '" class="dark-text" href="' + value.artist_url + '">' + value.artist_name + '</a></h3>' +
//         '</div></div>' +
//         '<h2><a class="dark-text" href="' + value.url + '">' + value.name + '</a></h2>' +
//         '<p class="artwork-measures">' + value.measure_cm + ' cm</p>' +
//         '<div class="default-sheet-row slider-item-row">' +
//         '<div class="default-sheet-row-cell">' +
//         '<p class="artwork-type">';
//
//     $.each(value.medium, function (index, media) {
//         $tpl += '<a title="' + media.name + '" href="' + media.url + '">' + media.name + '</a>'
//     });
//     $tpl += '</p></div>';
//
//     if (value.price) {
//         $tpl += '<div class="default-sheet-row-cell">' +
//             '<p class="dark-text artwork-price">' + value.price + '</p>' +
//             '</div>';
//     }
//
//     $tpl += '</div></div></div>';
//
//     return $tpl;
// };

var filter_data = {
    location: null,
    category: null,
    alpha: null
};


/* ==========================================================================
Filtraggio elementi per Alfabeto
========================================================================== */
window.filter = function (that, url, $container, templateFunction) {
    if ($(that).is('a')) {
        $(that).parents('#header-secondary-menu').find('a').removeClass('active');
        $(that).addClass('active');
        filter_data.alpha = $(that).data('value');
        $container.parents('.container.boxed-container').find('span.btn_view_all').data('alpha-filter', filter_data.alpha);
    } else if ($(that).is('select')) {
        if ($(that).hasClass('location')) {
            filter_data.location = $(that).val();
            $container.parents('.container.boxed-container').find('span.btn_view_all').data('location-filter', filter_data.location);
        }
        else {
            filter_data.category = $(that).val();
            $container.parents('.container.boxed-container').find('span.btn_view_all').data('category-filter', filter_data.category);
        }
    }

    var current_ids = [];
    $('.slider-item').each(function () {
        if ($(this).data('id') !== undefined)
            current_ids.push($(this).data('id'));
    });

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {current: current_ids, filter_data: filter_data},
        complete: function (response) {
            // console.log('Complete!');
        },
        error: function (response) {
            // console.log('Error!');
        },
        success: function (response) {
            if (response.result === 'success') {
                $.each(response.items.remove_item, function (index, value) {
                    $('.slider-item[data-id="' + value + '"]').remove()
                });

                $.each(response.items.new_item, function (index, value) {
                    $container.append(templateFunction(value))
                })
            }
        }
    });
};


/* ==========================================================================
Number Formatter support function
========================================================================== */
Number.prototype.numberFormat = function (decimals, dec_point, thousands_sep) {
    dec_point = typeof dec_point !== 'undefined' ? dec_point : '.';
    thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ',';

    var parts = this.toFixed(decimals).split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

    return parts.join(dec_point);
};


/* ==========================================================================
Currency Converter
========================================================================== */
$.fn.attachCurrencyEvent = function () {
    $(this).on("change", function () {
        var $element = $(this).parents('.default-sheet-price').find('h4[data-currency]');
        var price = parseFloat($element.data('price'));
        var current = $element.data('currency');
        var new_currency = $(this).val();
        var new_val = fx.convert(price, {from: current, to: new_currency});
        var $rowCode = $('.row-code');
        if ($rowCode.length > 0) {
            var coupon = parseFloat($rowCode.find('p').data('value'));
            // console.log(coupon);
            new_val = (new_val * (1 - (coupon / 100)));
        }
        var symbol = '€';
        if (new_currency === "USD") {
            symbol = '$';
        } else if (new_currency === "GBP") {
            symbol = '£';
        }
        $element.text(new_val.numberFormat(2, ',', '') + " " + symbol);
        $element.attr('data-currency', new_currency);
    }).attr("disabled", false);
}


/* ==========================================================================
Page Loader
========================================================================== */
window.showPage = function () {
    setTimeout(function () {
        document.getElementById("loader-body").classList.add('no-display');
        document.getElementById("loader-body-content").classList.remove('loading-content');
        document.getElementById("loader-body-content").classList.add('content-loaded');
        // console.log('✅ Page Loaded called! (show content, hide loader)');
    }, 400);
}
window.hidePage = function () {
    document.getElementById("loader-body").classList.remove('no-display');
    document.getElementById("loader-body-content").classList.remove('content-loaded');
    document.getElementById("loader-body-content").classList.add('loading-content');
    // console.log('🔄️ Loading Page called! (show loader, hide content)');
}
$(window).on('load', function() {
    // console.log('Window Loaded -> showPage');
    showPage();
});


/* ==========================================================================
Funzione per View in Room
========================================================================== */
$(document).ready(function () {

    /*view in room*/
    var recalcRoom = function () {
        //const
        var oneMeter = 387.5; //1
        var picture = $('#vir-artwork');

        var artworkWidthM = parseFloat(picture.attr('data-width')) / 1000; //1.2
        var artworkWidthPx = (artworkWidthM * oneMeter);

        //ricavo la larghezza dellos schermo
        var windowHeight = $(window).height();

        /*1) zoom dello sfondo*/
        var zoom = windowHeight / 1350;
        var roomBg = $('.vir-room')[0];
        roomBg.style.webkitTransform = "scale(" + zoom + ")";
        roomBg.style.transform = "scale(" + zoom + ")";

        /*change width of artwork based on window height*/
        var newWidth = artworkWidthPx * zoom;
        picture.width(newWidth);

        /*recalc top*/
        var windowHalf = windowHeight / 2;
        if (picture.height() == 0) {
            var calcHeight = (newWidth * picture[0].height) / picture[0].width;
            var diff = windowHalf - calcHeight;
        } else {
            var diff = windowHalf - picture.height();
        }

        var top = diff / 2;
        picture.css('top', top);
    };


    /**
     * Nasconde un popup con del contenuto HTML arbitrario
     * @since 1.0.0
     */
    var hideModal = function () {
        //rimuovi la classe fade-in al nodo scelto come popup/modal (identificato dalla classe "sma-popup")
        $('#sma-popup').removeClass();

        //rimuovi la classe fade-in al nodo scelto come overlay (identificato dalla classe "sma-popup-overlay")
        $('.sma-popup-overlay').removeClass('fade-in');
        $('.sma-popup-overlay').addClass('hidden');

        //tolgo la classe fixed al body per farlo scrollare di nuovo normalmente
        $('body').removeClass('fixed');

        //svuotare l'html del nodo scelto come popup/modal
        $('.sma-popup-content').empty();
    };

    $(document).on('click', '.close-modal', hideModal);

    //$(document).on('click', '.sma-popup-overlay', hideModal);
    $(document).on('click', '.sma-popup', function (evt) {
        evt.stopPropagation();
    });

    var viewInRoom = function () {
        if ($('.vir-room').length) {
            $(window).on('resize', recalcRoom);
            recalcRoom();
        }
        recalcRoom();
        //cerco l'elemento con il data-state
        var roomContainer = $('[data-state]');
        roomContainer.fadeIn('slow');
        $('body').addClass('fixed');
        //$('#main-wrapper').addClass('fixed');

        $(document).on('keyup.viewInRoom', function (evt) {
            if (evt.keyCode == 37) {
                _changeRoom(-1);
            } else if (evt.keyCode == 39) {
                _changeRoom(1);
            }
        });
    };

    var closeRoom = function () {
        var roomContainer = $('[data-state]');
        roomContainer.fadeOut('slow');
        $('body').removeClass('fixed');
        //$('#main-wrapper').removeClass('fixed');

        $(document).off('.viewInRoom');
    };

    $('#view-in-room').on('click', viewInRoom);

    $('.view-in-room-close').on('click', closeRoom);

    var checkDir = function (evt) {
        var dir = $(evt.target).attr('data-dir');
        _changeRoom(parseInt(dir));
    };

    var _changeRoom = function (dir) {
        //definire array degli ambienti disp
        //var rooms=['camino', 'cucina', 'salotto', 'bedroom', 'office'];
        var rooms = ['salotto', 'banner'];

        //ricavare data-room
        var currRoom = $('.vir-room').attr('data-room');
        //'salotto'

        //ricavo l'indice dell'array cercando la posizione data-room (indexOf)
        var currRoomIdx = rooms.indexOf(currRoom);

        //basandomi su dir ricavo l'elemento dell'array prec/successivo (tenendo conto del _circle)
        var newRoomIdx = currRoomIdx + dir;
        newRoomIdx = _circle(rooms.length, newRoomIdx);

        var newRoom = rooms[newRoomIdx];

        //faccio comparire l'overlay
        $('.view-in-room-overlay').fadeIn(300, function () {
            //rimpiazzo data-room attuale con quello che ho ricavato sopra
            $('.vir-room').attr('data-room', newRoom);
        });

        setTimeout(function () {
            $('.view-in-room-overlay').fadeOut('slow');
        }, 300);
    };

    var _circle = function (t, s) {
        return ((t + (s % t)) % t);
    };

    $('.room-btn').on('click', checkDir);

    var flattenObject = function (ob) {
        var toReturn = {};

        for (var i in ob) {
            if (!ob.hasOwnProperty(i))
                continue;

            if ($.isArray(ob[i])) {
                toReturn[i + '.count'] = ob[i].length;
            }

            if ((typeof ob[i]) == 'object') {
                var flatObject = flattenObject(ob[i]);
                for (var x in flatObject) {
                    if (!flatObject.hasOwnProperty(x))
                        continue;

                    toReturn[i + '.' + x] = flatObject[x];
                }
            } else {
                toReturn[i] = ob[i];
            }
        }
        return toReturn;
    };

});


/* ==========================================================================
Warning Modals
========================================================================== */
$(document).ready(function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    // se l'environment attuale è la qa mostra i log degli errori js in toastr
    if (window.envKind == 'qa') {
        console.log('you are in quality assurance environment');
        // Catch error and display in Toastr
        window.onerror = function (msg, url, linenumber) {
            toastr.error('Error message: ' + msg + '<br>Path: ' + url + '<br>Line Number: ' + linenumber);
            return true;
        };
    }
    // esempi di chiamata a toastr
    // toastr.info('No errors found');
    // toastr.success('jQuery & Toastr loaded');

});


/* ==========================================================================
Header Search
========================================================================== */
$(document).ready(function () {
    var headerSearchAccordion = $('.header-search-accordion');
    var xhr;

    // Disable Ajax cache
    $.ajaxSetup({cache: false});

    // Search input keyup function
    $('#search').keyup(function () {
        var searchField = $('#search').val();
        // console.log(searchField.length);
        // se la ricerca è superiore ai tre caratteri
        if (searchField.length >= 3) {
            // lancio lo script di ricerca
            searchFn(searchField);
            // mostro il container dei risultati
            headerSearchAccordion.addClass('active-search');
        }
        // se la ricerca è zero caratteri
        if (searchField.length == 0) {
            // nascondo il container dei risultati
            headerSearchAccordion.removeClass('active-search');
        }
    });

    // Ajax search function
    var ajaxCall = function (searchText) {
        if (xhr && xhr.readyState != 4) {
            // console.log('‼️ Aborted previous request...');
            xhr.abort();
        }
        // console.log('🔄 Ajax call started, wait for results...');
        xhr = $.ajax({
            url: '/search',
            type: 'POST',
            dataType: 'json',
            data: {
                query: searchText
            },
            complete: function (response) {
                // console.log('Complete!');
            },
            error: function (response) {
                // console.log('Error!');
            },
            success: function (response) {
                // console.log('Success! Response => ');
                // console.log(response);

                // Add new items
                var artists = response.artists;
                var galleries = response.galleries;
                var categories = response.categories;
                var artistsCount = 0;
                var galleriesCount = 0;
                var categoriesCount = 0;

                $('#artists-result').html('');
                $('#galleries-result').html('');
                $('#categories-result').html('');

                // insert Artist results
                $.each(artists, function (index, artist) {
                    var artistImage = artist.image_url || '/images/placeholder.png';
                    $('#artists-result').append('<li><a href="' + artist.link + '"><div style="background-image: url(' + artistImage + ');" class="img-result" /></div><span class="text-result">' + artist.text + '</span></li>');
                    artistsCount++;
                });
                $('#artists-result-count').html(artistsCount);
                if (artistsCount == 0) {
                    $('#artists-result').html('No result for this section');
                }

                // insert Galleries results
                $.each(galleries, function (index, gallery) {
                    var galleryImage = gallery.image_url || '/images/placeholder.png';
                    $('#galleries-result').append('<li><a href="' + gallery.link + '"><div style="background-image: url(' + galleryImage + ');" class="img-result" /></div><span class="text-result">' + gallery.text + '</span></li>');
                    galleriesCount++;
                });
                $('#galleries-result-count').html(galleriesCount);
                if (galleriesCount == 0) {
                    $('#galleries-result').html('No result for this section');
                }

                // insert Categories results
                $.each(categories, function (index, category) {
                    var categoryImage = category.image_url || '/images/placeholder.png';
                    $('#categories-result').append('<li><a href="' + category.link + '"><div style="background-image: url(' + categoryImage + ');" class="img-result" /></div><span class="text-result">' + category.text + '</span></li>');
                    categoriesCount++;
                });
                $('#categories-result-count').html(categoriesCount);
                if (categoriesCount == 0) {
                    $('#categories-result').html('No result for this section');
                }

                // automatic positioning on first search result
                console.log(categories);
                if(artistsCount >= 1) {
                    console.log('trovati artisti!'); // es ricerca => 'alessandro'
                    document.getElementById("artist-search-accordion-toggler").click();
                }
                if(artistsCount < 1 && galleriesCount >= 1) {
                    console.log('non trovati artisti, ma trovate gallerie!'); // es ricerca => 'galleria'
                    document.getElementById("galleries-search-accordion-toggler").click();
                }
                if(artistsCount < 1 && galleriesCount < 1 && categoriesCount >= 1) {
                    console.log('non trovati artisti, non trovate gallerie, ma trovate categorie!'); // es ricerca => 'painting'
                    document.getElementById("categories-search-accordion-toggler").click();
                }
                if(artistsCount < 1 && galleriesCount < 1 && categoriesCount < 1) {
                    console.log('non trovati artisti, non trovate gallerie, non trovate categorie... Nessun risultato disponibile per la tua ricerca!'); // es ricerca => 'ksdjh'
                }

            } // END - Success
        }); // END - Ajax
    };

    // Search function
    var searchFn = function (searchText) {
        // console.log('Searching for => ' + searchText);
        // se il campo di ricerca è vuoto nascondo il box dei risultati
        if (searchText == '') {
            headerSearchAccordion.removeClass('active-search');
        }
        // se il campo di ricerca NON è vuoto mostro il box dei risultati e appendo i risultati
        else {
            ajaxCall(searchText);
            headerSearchAccordion.addClass('active-search');
        }
    } // END - searchFn function

    // Accordion function
    var searchAccordionTabs = headerSearchAccordion.find('.header-search-accordion-toggle h4')
    searchAccordionTabs.click(function () {
        var panelID = $(this).attr("data-accordion");
        var panelContent = headerSearchAccordion.find("[data-slide='" + panelID + "']");

        // setto classe attivo/disattivo sui pulsanti
        searchAccordionTabs.parent().removeClass('active');
        $(this).parent().addClass('active');
        // First => Hide all panels
        $.when($(".header-search-accordion-content").removeClass('active')).then(function () {
            // Then => Expand or collapse this panel
            panelContent.addClass('active');
        });

    });

});


/* ==========================================================================
Show More Text
========================================================================== */
$(window).on('load', function() {

    // if content is too short hide "show more" button
    var allShowMoreButtons = $(".show-more-button a");
    allShowMoreButtons.each(function() {
        var $this = $(this);
        var $innerContent = $this.parent().prev(".show-more-content").find(".show-more-content-inner");

        // console.log($innerContent.height());
        if($innerContent.height() <= '100') {
            // console.log('innerContent.height is short, hide read more button');
            // hide show more button
            $this.css("display", "none");
        }
    });


    // add "show more" functionality to buttons
    $(".show-more-button a").on("click", function(e) {
        e.preventDefault();
        var $this = $(this);
        var $content = $this.parent().prev(".show-more-content");
        var $innerContent = $this.parent().prev(".show-more-content").find(".show-more-content-inner");
        var linkText = $this.text().toUpperCase();
        if (linkText === "SHOW MORE") {
            linkText = "Show less";
            $content.addClass('showContent');
        } else {
            linkText = "Show more";
            $content.removeClass('showContent');
        };
        $this.text(linkText);
    });

});


// Slick carousels
jQuery(document).ready(function ($) {
    // generic Slick carousels
    $('.slick-carousel').slick({
        dots: true,
        infinite: false,
        speed: 300
    });

    // header announce Slick carousels
    $('.payoff-slick').slick({
        dots: false,
        arrows: true,
        autoplay: true,
        infinite: true,
        nextArrow: $('.payoff-next'),
        prevArrow: $('.payoff-prev'),
        speed: 300
    });

});

// Funzione di indirizzamento custom generica per Filtri categorie
var actualCategoryFilterRedirect = function() {
    var allCategoryFilters = $('.category-filter-item.active');
    allCategoryFilters.each( function( index, element ){
        $( this ).find("a").click(function(event){
            event.preventDefault();
            // make it selective for the current section 
            if ($("body").hasClass("artworks-archive")) {
                window.location.href = '/artworks'; //relative to domain
            }
            else if ($("body").hasClass("magazine-archive")) {
                window.location.href = '/posts/magazine'; //relative to domain
            }
        });
    });
}

// Funzione di verifica campi per tutti i form di contatto
var validateContactForms = function() {
    $(".contact-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            },
            subject: "required",
            body: "required",
            privacy: "required"
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter a valid email address",
            subject: "Please enter a subject",
            body: "Please enter a message",
            privacy: "Please accept our policy"
        },
        errorPlacement: function( label, element ) {
            if( element.attr( "name" ) === "privacy" ) {
                element.parent().parent().find( ".errorTxt" ).html(label); // custom behaviour => witout this the error will be placed inbetween the radio and the text
            } else {
                label.insertAfter( element ); // standard behaviour
            }
        }
    });
}

// V1 (su pulsante) - Funzione per impedire il doppio submit dei form/caricamenti in ajax
$(document).on('click', '.avoid-multiple-submission', function (){
    var element = $(this);
    element.prop('disabled', true);
    console.log('🔒 lock button to avoid multiple submission');
    setTimeout(function() {
         element.prop('disabled', false);
         console.log('🔓 button unlocked!');
    }, 1500);
});

// V2 (su intero form) - Funzione per impedire il doppio submit dei form/caricamenti in ajax
$(document).on('click', '.avoid-form-multiple-submission', function (){
    console.log('🔒 lock form to avoid multiple submission');
    let submitted = false;

    if(submitted == true) {
        showPage();
        return false;
    } else {
        submitted = true;
        hidePage();
        return true;
    }
});




/* ==========================================================================
Widgets Slick carousels
========================================================================== */
var widgetsSlick = function() {
    $(".widget-slick").slick({
        dots: true
    });
}

var widgetsUnSlick = function() {
    $('.widget-slick').slick('unslick');
}

// re-layout for masonry to fix browser zoom render
var relayoutMasonry = function() {
    if(window.myMasonry) {
        setTimeout(function(){
            console.log('masonry re-layout');
            window.myMasonry.masonry('layout');
        }, 300);    
    } 
}



/* ==========================================================================
function call order
========================================================================== */

// Document Ready
jQuery(document).ready(function ($) {
    actualCategoryFilterRedirect();
    validateContactForms();
});

// Window Load
$(window).on("load", function () {
    // onCheckoutClick();
    relayoutMasonry();

    // Only for mobile
    if ($(window).width() < 768) {
        widgetsSlick();
    }

});

// Window Resize
window.onresize = function(event) {

    // Only for mobile
    if ($(window).width() < 768) {
        // widgetsSlick();
    } else {
        // widgetsUnSlick();
    }

};



