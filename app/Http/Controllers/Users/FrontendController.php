<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Gallery;
use App\Models\Order;
use App\Models\PromoCode;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserCollection;
use App\Models\UserOffer;
use App\Models\UserPayment;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Api4Dem\Api4DemDataParser;
use App\Services\Notification;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class FrontendController extends FrontendExtendedController
{

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function profile()
    {
        $addresses = Auth::user()->addresses()->get();
        $payments = Auth::user()->payments()->get();
        $orders = Order::whereUserId(Auth::user()->id)->get();

        return view('users.frontend.profile', compact( 'addresses', 'payments', 'orders') );
    }

    public function saveAddress(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];

        if ($request->method() == 'POST' && $request->ajax()) {
            $data = Input::all();
            if($data['type'] == 'private'){
                $data['company_name'] = $data['vat_number'] = null;
            } else{
                $data['first_name'] = $data['last_name'] = null;
            }

            if (!isset($data['id'])) {
                $address = new UserAddress($data);
                $address->user_id = Auth::user()->id;
                if ($address->save()) {
                    $response = [
                        'status' => 'success',
                        'action' => 'new',
                        'address' => $address
                    ];
                }
            } else {
                $address = UserAddress::find($data['id']);
                if ($address) {
                    if ($address->update($data)) {
                        $response = [
                            'status' => 'success',
                            'action' => 'edit',
                            'address' => $address
                        ];
                    }
                }
            }

        }

        return response()->json($response);
    }

    public function savePaymentMethod(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];

        if ($request->method() == 'POST' && $request->ajax()) {
            $stripe = Stripe::make(config('services.stripe.secret'));
            if (!Auth::user()->customer_id) {
                $customer = $stripe->customers()->create([
                    'email' => Auth::user()->email
                ]);
                if (isset($customer['id'])) {
                    Auth::user()->customer_id = $customer['id'];
                    Auth::user()->save();
                }
            }
            if (Auth::user()->customer_id) {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'name' => $request->get('owner'),
                        'number' => $request->get('cc'),
                        'exp_month' => $request->get('month'),
                        'exp_year' => $request->get('year'),
                        'cvc' => $request->get('cvv')
                    ],
                ]);
                if (isset($token['id'])) {
                    $card = $stripe->cards()->create(Auth::user()->customer_id, $token['id']);
                    if (isset($card['id'])) {
                        $userPayment = new UserPayment();
                        $userPayment->user_id = Auth::user()->id;
                        $userPayment->card_id = $card['id'];
                        $userPayment->end_with = substr($request->get('cc'), strlen($request->get('cc')) - 4, strlen($request->get('cc')));
                        $userPayment->type = $request->get('type');
                        if ($userPayment->save()) {
                            $response = [
                                'status' => 'success',
                                'payment' => $userPayment
                            ];
                        }
                    }
                }
            }
        }

        return response()->json($response);
    }

    public function deletePaymentMethod(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];

        if ($request->method() == 'POST' && $request->ajax()) {
            $userPayment = UserPayment::find($request->get('id'));
            if ($userPayment) {
                $stripe = Stripe::make(config('services.stripe.secret'));
                $card = $stripe->cards()->delete(Auth::user()->customer_id, $userPayment->card_id);
                if ($card['deleted'] && $userPayment->delete()) {
                    $response = [
                        'status' => 'success'
                    ];
                }
            }
        }

        return response()->json($response);
    }

    public function deleteAddress(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];

        if ($request->method() == 'POST' && $request->ajax()) {
            $address = UserAddress::find($request->get('id'));
            if ($address) {
                if ($address->delete()) {
                    $response = [
                        'status' => 'success'
                    ];
                }
            }
        }

        return response()->json($response);
    }

    public function applyPromoCode(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $code = $request->get('code');
            $promoCode = PromoCode::whereName($code)->first();
            if ($promoCode && $promoCode->status != 3) {
                $this->_cart_instance->promo_code = $promoCode->id;
                $new_amount = $this->_cart_instance->total_price * (1 - ($promoCode->value / 100));
                if ($this->_cart_instance->save()) {
                    $response = [
                        'status' => 'success',
                        'coupon' => $promoCode,
                        'amount' => number_format($new_amount, 2, ',', '') . "€"
                    ];
                }
            }
        }
        return response()->json($response);
    }

    public function removePromoCode(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $this->_cart_instance->promo_code = null;
            if ($this->_cart_instance->save()) {
                $response = [
                    'status' => 'success',
                    'amount' => number_format($this->_cart_instance->total_price, 2, ',', '') . "€"
                ];
            }
        }
        return response()->json($response);
    }

    // User Profile
    public function dashboard()
    {
        return view('users.frontend.profile');
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $isUpdateSuccess = false;

        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $data = $request->all();
            Auth::user()->first_name = $data['first_name'];
            Auth::user()->last_name = $data['last_name'];
            if (isset($data['old_password']) && isset($data['new_password'])) {
                if (Hash::check($data['old_password'], Auth::user()->password)) {
                    Auth::user()->password = Hash::make($data['new_password']);
                    if (Auth::user()->save()) {
                        $response = [
                            'status' => 'success',
                            'user' => Auth::user()
                        ];
                        $isUpdateSuccess = true;
                    }
                } else {
                    $response = [
                        'status' => 'failed',
                        'msg' => 'old_pwd_error',
                    ];
                }
            } else {
                if (Auth::user()->save()) {
                    $response = [
                        'status' => 'success',
                        'user' => Auth::user()
                    ];
                    $isUpdateSuccess = true;
                }
            }
        }

        if($isUpdateSuccess) {

            // 4Dem userSave
            $api4Dem = new Api4Dem();
            $api4Dem->userSave($user);

        }

        return response()->json($response);
    }

    public function createCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $data = $request->all();
            $collection = new UserCollection();
            $collection->user_id = Auth::user()->id;
            $collection->name = $data['name'];
            if ($collection->save()) {
                $response = [
                    'status' => 'success',
                    'collection' => $collection
                ];
            }

        }
        return response()->json($response);
    }

    public function removeCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $collection = UserCollection::find($request->get('id'));
            if ($collection->delete()) {
                $response = [
                    'status' => 'success'
                ];
            }

        }
        return response()->json($response);
    }

    public function addToCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            switch ($request->get('section')) {
                case 'artworks':
                    $artwork = Artwork::find($request->get('id'));
                    if ($artwork) {
                        if (!Auth::user()->collectionArtworks->contains($request->get('id')) && Auth::user()->collectionArtworks()->save($artwork)) {
                            $response = [
                                'status' => 'success'
                            ];
                        }
                    }
                    break;
                case 'artists':
                    $artist = Artist::find($request->get('id'));
                    if ($artist) {
                        if (!Auth::user()->collectionArtists->contains($request->get('id')) && Auth::user()->collectionArtists()->save($artist)) {
                            $response = [
                                'status' => 'success'
                            ];
                        }
                    }
                    break;
                case 'galleries':
                    $gallery = Gallery::find($request->get('id'));
                    if ($gallery) {
                        if (!Auth::user()->collectionGalleries->contains($request->get('id')) && Auth::user()->collectionGalleries()->save($gallery)) {
                            $response = [
                                'status' => 'success'
                            ];
                        }
                    }
                    break;
                default:
                    $collection = UserCollection::find($request->get('collection_id'));
                    if ($collection) {
                        $artwork = Artwork::find($request->get('id'));
                        if ($artwork) {
                            if (!$collection->artworks->contains($request->get('id')) && $collection->artworks()->save($artwork)) {
                                $response = [
                                    'status' => 'success'
                                ];
                            }
                        }
                    }
                    break;
            }
        }
        return response()->json($response);
    }

    public function removeFromCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            switch ($request->get('section')) {
                case 'artworks':
                    if (Auth::user()->collectionArtworks->contains($request->get('id')) && Auth::user()->collectionArtworks()->detach($request->get('id'))) {
                        $response = [
                            'status' => 'success'
                        ];
                    }
                    break;
                case 'artists':
                    if (Auth::user()->collectionArtists->contains($request->get('id')) && Auth::user()->collectionArtists()->detach($request->get('id'))) {
                        $response = [
                            'status' => 'success'
                        ];
                    }
                    break;
                case 'galleries':
                    if (Auth::user()->collectionGalleries->contains($request->get('id')) && Auth::user()->collectionGalleries()->detach($request->get('id'))) {
                        $response = [
                            'status' => 'success'
                        ];
                    }
                    break;
                default:
                    $collection = UserCollection::find($request->get('collection_id'));
                    if ($collection) {
                        if ($collection->artworks->contains($request->get('id'))) {
                            $referer_id = $collection->artworks()->find($request->get('id'))->pivot->id;
//                            if ($collection->artworks()->detach($request->get('id'))) {
                            $response = [
                                'status' => 'success',
                                'referer' => $referer_id
                            ];
//                            }
                        }
                    }
                    break;
            }
        }
        return response()->json($response);
    }

    public function updateCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $collection = UserCollection::find($request->get('collection_id'));
            if ($collection) {
                $collection->name = $request->get('name');
                if ($collection->save()) {
                    $response = [
                        'status' => 'success'
                    ];
                }

            }
        }
        return response()->json($response);
    }

    public function getCollection(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $collection = UserCollection::find($request->get('collection_id'));
            if ($collection) {
                $artworks = [];
                foreach ($collection->artworks()->get() as $artwork) {
                    $artworks[] = [
                        "id" => $artwork->id,
                        "name" => $artwork->title,
                        "img" => $artwork->main_image->url
                    ];
                }
                $response = [
                    'status' => 'success',
                    'collection' => [
                        'name' => $collection->name,
                        'artworks' => $artworks
                    ]
                ];
            }
        }
        return response()->json($response);
    }

    public function collection(){
        $collections = Auth::user()->collections()->get();
        return view('users.frontend.collection', compact('collections'));
    }

    public function favouriteArtworks(){
        $artworks = Auth::user()->collectionArtworks()->get();
        return view('users.frontend.favourite-artworks', compact('artworks'));
    }

    public function favouriteArtists(){
        $artists = Auth::user()->collectionArtists()->get();
        return view('users.frontend.favourite-artists', compact('artists'));
    }

    public function favouriteGalleries(){
        $galleries = Auth::user()->collectionGalleries()->get();
        return view('users.frontend.favourite-galleries', compact('galleries'));
    }

    public function makeAnOffer(Request $request){
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $offer = UserOffer::create($data);
            if($offer){
                $response = [
                    'status' => 'success'
                ];
                $artwork = Artwork::find($data['artwork_id']);
                if($artwork){

                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_NEW_USER_OFFER, 'user_offer', $offer);
                    Notification::addUsers($notify, $users);

                    $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');
                    $name = 'Kooness Proposal';
                    $subject = SUBJECT_MAIL_OFFER_RECEIVED_ADMIN;
                    Mail::send('emails.offers.received-admin', ["artwork" => $artwork, 'user' => Auth::user(), 'msg' => $offer->text], function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });
                }
            }
        }
        return response()->json($response);
    }

    public function requestInfoOnAvailableInFair(Request $request){
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST' && $request->ajax()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $response = [
                'status' => 'success'
            ];
            $artwork = Artwork::find($data['artwork_id']);
            if($artwork){
                $name = 'Kooness Proposal';
                $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');

                $subject = SUBJECT_MAIL_AVAILABLE_RECEIVED_ADMIN;
                Mail::send('emails.available.received-admin', ["artwork" => $artwork, 'user' => Auth::user(), 'msg' => $data['text'] ], function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });
            }
        }
        return response()->json($response);
    }


}
