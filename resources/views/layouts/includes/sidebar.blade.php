<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MENU</li>
    <!-- Optionally, you can add icons to the links -->

    <?php
        $id_artist = null;
        $id_fair = null;
        $id_gallery = null;
        $artist_row = Auth::user()->artist()->first();
        $gallery_row = Auth::user()->gallery()->first();
        $fair_row = Auth::user()->fair()->first();
        if ($artist_row) {
            $id_artist = $artist_row->id;
        }
        if ($gallery_row) {
            $id_gallery = $gallery_row->id;
        }
        if ($fair_row) {
            $id_fair = $fair_row->id;
        }
    ?>

    <!--
    |--------------------------------------------------------------------------
    | Gallery Profile
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('gallerist') && $id_gallery && (isPremiumUser() || isTrialUser()) )
        <li class="treeview {{ Request::is('admin/galleries*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-picture-o"></i> <span>Gallerist</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/galleries*') ? 'display: block' : '' }}">
                @if( roleCheck('gallerist') && ( isPremiumUser() || isTrialUser() ) )
                    <li class="{{ Request::is('admin/galleries*') ? 'active' : '' }}">
                        <a href="{{ route('admin.galleries.edit', [$id_gallery]) }}">
                            <i class="fa fa-edit"></i> <span>Profile</span>
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Fair Profile
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('fair') && $id_fair )
        <li class="{{ Request::is('admin/fairs*') ? 'active' : '' }}">
            <a href="{{ route('admin.fairs.edit', [$id_fair]) }}"><i class="fa fa-th"></i> <span>Fair Profile</span>
            </a>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Artist Profile
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('artist') && $id_artist )
        <li class="{{ Request::is('admin/artists*') ? 'active' : '' }}">
            <a href="{{ route('admin.artists.edit', [$id_artist]) }}"><i class="fa fa-address-book-o"></i> <span>Artist Profile</span>
            </a>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Home Page Layout
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin'))
        <li class="{{ Request::is('admin/homepage*') ? 'active' : '' }}">
            <a href="{{ route('admin.homepage.layout.edit') }}"><i class="fa fa-home"></i> <span>HomePage Layout</span>
            </a>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Memberships (different views for Admin & Gallerist)
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin') || ( roleCheck('gallerist') ) )
        <li class="treeview {{ Request::is('admin/memberships*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-star"></i> <span>Memberships</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/memberships*') ? 'display: block' : '' }}">

                {{--Admin View--}}
                @if(roleCheck('superadmin|admin'))
                    <li class="{{ Request::is('admin/memberships/list') ? 'active' : '' }}">
                        <a href="{{ route('admin.memberships.list', ["galleries"]) }}">
                            <i class="fa fa-list"></i> <span>List</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/memberships/payments') ? 'active' : '' }}">
                        <a href="{{ route('admin.memberships.payments', ["galleries"]) }}">
                            <i class="fa fa-money"></i> <span>Payments</span>
                        </a>
                    </li>
                @endif

                {{--Gallerist View--}}
                @if( roleCheck('gallerist') )
                    <li class="{{ Request::is('admin/memberships*') && !Request::is('admin/memberships/edit') ? 'active' : '' }}">
                        <a href="{{ route('admin.memberships.index') }}">
                            <i class="fa fa-list"></i> <span>My Membership</span>
                        </a>
                    </li>
                    @if(isPremiumUser() && !isTrialUser() )
                        <li class="{{ Request::is('admin/memberships/edit') ? 'active' : '' }}">
                            <a href="{{ route('admin.memberships.edit') }}">
                                <i class="fa fa-info"></i> <span>Billing Information</span>
                            </a>
                        </li>
                    @endif
                @endif


            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Artworks
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|artist|seo') || ( roleCheck('gallerist') && (isPremiumUser() || isTrialUser()) ) )
        <li class="treeview {{ Request::is('admin/artworks*') || Request::is('admin/categories/artworks*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-paint-brush"></i> <span>Artworks</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/artworks*') || Request::is('admin/categories/artworks*')|| Request::is('admin/medium/artworks*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/artworks') ? 'active' : '' }}">
                    <a href="{{ route('admin.artworks.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>

                @if(roleCheck('superadmin|admin|seo'))

                    <li class="{{ Request::is('admin/medium/artworks*') ? 'active' : '' }}">
                        <a href="{{ route('admin.medium.artworks.index') }}">
                            <i class="fa fa-list"></i> <span>Medium</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/categories/artworks*') ? 'active' : '' }}">
                        <a href="{{ route('admin.categories.artworks.index') }}">
                            <i class="fa fa-list"></i> <span>Categories</span>
                        </a>
                    </li>

                    <li class="{{ Request::is('admin/artworks/medium/categories*') ? 'active' : '' }}">
                        <a href="{{ route('admin.artworks.medium.categories.index') }}">
                            <i class="fa fa-list"></i> <span>Medium <small><i class="fa fa-arrow-right"></i></small> Categories</span>
                        </a>
                    </li>
                @endif

                @if(roleCheck('superadmin|admin|gallerist'))
                    <li class="{{ Request::is('admin/artworks/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.artworks.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new artwork</span>
                        </a>
                    </li>
                @endif

            </ul>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Artist
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo') || ( roleCheck('gallerist') && (isPremiumUser() || isTrialUser()) ) )
        <li class="treeview {{ Request::is('admin/artists*') || Request::is('admin/categories/artists*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-address-book-o"></i> <span>Artists</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/artists*') || Request::is('admin/categories/artists*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/artists') ? 'active' : '' }}">
                    <a href="{{ route('admin.artists.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>

                @if(roleCheck('superadmin|admin'))
                    <li class="{{ Request::is('admin/categories/artists') ? 'active' : '' }}">
                        <a href="{{ route('admin.categories.artists.index') }}">
                            <i class="fa fa-list"></i> <span>Categories</span>
                        </a>
                    </li>
                @endif

                @if(roleCheck('superadmin|admin|gallerist'))
                    <li class="{{ Request::is('admin/artists/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.artists.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new artist</span>
                        </a>
                    </li>

                    @if(roleCheck('superadmin|admin'))
                        <li class="{{ Request::is('admin/artists/main/page') ? 'active' : '' }}">
                            <a href="{{ route('admin.main.page', ["artists"]) }}">
                                <i class="fa fa-home"></i> <span>Intro</span>
                            </a>
                        </li>
                    @endif

                @endif

            </ul>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Galleries
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/galleries*') || Request::is('admin/categories/galleries*')  ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-picture-o"></i> <span>Galleries</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/galleries*') || Request::is('admin/categories/galleries*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/galleries') ? 'active' : '' }}">
                    <a href="{{ route('admin.galleries.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                @if(roleCheck('superadmin|admin|gallerist'))
                    <li class="{{ Request::is('admin/categories/galleries*') ? 'active' : '' }}">
                        <a href="{{ route('admin.categories.galleries.index') }}">
                            <i class="fa fa-list"></i> <span>Categories</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/galleries/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.galleries.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new gallery</span>
                        </a>
                    </li>

                    @if(roleCheck('superadmin|admin'))
                        <li class="{{ Request::is('admin/galleries/main/page') ? 'active' : '' }}">
                            <a href="{{ route('admin.main.page', ["galleries"]) }}">
                                <i class="fa fa-home"></i> <span>Intro</span>
                            </a>
                        </li>
                    @endif
                @endif
            </ul>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Exhibitions
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo') || ( roleCheck('gallerist') && (isPremiumUser() || isTrialUser()) ) )
        <li class="treeview {{ Request::is('admin/exhibitions*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-th-large"></i> <span>Exhibitions</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/exhibitions*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/exhibitions') ? 'active' : '' }}">
                    <a href="{{ route('admin.exhibitions.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                @if(roleCheck('superadmin|admin|gallerist'))
                    <li class="{{ Request::is('admin/exhibitions/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.exhibitions.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new exhibitions</span>
                        </a>
                    </li>
                @endif
                {{--<li class="{{ Request::is('admin/exhibitions/main/page') ? 'active' : '' }}">--}}
                    {{--<a href="{{ route('admin.main.page', ["exhibitions"]) }}">--}}
                        {{--<i class="fa fa-home"></i> <span>Intro</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Fairs
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/fairs*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-th"></i> <span>Fairs</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/fairs*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/fairs') ? 'active' : '' }}">
                    <a href="{{ route('admin.fairs.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                @if(roleCheck('superadmin|admin|fair'))
                    <li class="{{ Request::is('admin/fairs/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.fairs.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new fair</span>
                        </a>
                    </li>

                    @if(roleCheck('superadmin|admin'))
                        <li class="{{ Request::is('admin/fairs/main/page') ? 'active' : '' }}">
                            <a href="{{ route('admin.main.page', ["fairs"]) }}">
                                <i class="fa fa-home"></i> <span>Intro</span>
                            </a>
                        </li>
                    @endif

                @endif
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Tags
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/tags*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-tags"></i> <span>Tags</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/tags*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/tags') ? 'active' : '' }}">
                    <a href="{{ route('admin.tags.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                @if(roleCheck('superadmin|admin'))
                    <li class="{{ Request::is('admin/tags/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.tags.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new Tag</span>
                        </a>
                    </li>
                @endif
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Magazine
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|editor|seo'))
        <li class="treeview {{ Request::is('admin/magazine*') || Request::is('admin/categories/magazine*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-book"></i> <span>Magazine</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/magazine*') || Request::is('admin/categories/magazine*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/magazine') ? 'active' : '' }}">
                    <a href="{{ route('admin.magazine.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>

                @if(roleCheck('superadmin|admin|editor'))
                    @if( !roleCheck('editor') )
                        <li class="{{ Request::is('admin/categories/magazine*') ? 'active' : '' }}">
                            <a href="{{ route('admin.categories.magazine.index') }}">
                                <i class="fa fa-list"></i> <span>Categories</span>
                            </a>
                        </li>
                    @endif

                    <li class="{{ Request::is('admin/magazine/create') ? 'active' : '' }}">
                        <a href="{{ route('admin.magazine.create') }}">
                            <i class="fa fa-plus"></i> <span>Add new post</span>
                        </a>
                    </li>
                @endif

            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | News
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|artist|fair|seo') || ( roleCheck('gallerist') && (isPremiumUser() || isTrialUser()) ) )
        <li class="treeview {{ Request::is('admin/news*') || Request::is('admin/categories/news*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-newspaper-o"></i> <span>News</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu"
                style="{{ Request::is('admin/news*') || Request::is('admin/categories/news*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/news') ? 'active' : '' }}">
                    <a href="{{ route('admin.news.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/news/create') ? 'active' : '' }}">
                    <a href="{{ route('admin.news.create') }}">
                        <i class="fa fa-plus"></i> <span>Add news</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Pages
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin'))
        <li class="{{ Request::is('admin/pages*') ? 'active' : '' }}">
            <a href="{{ route('admin.pages.index') }}"><i class="fa fa-file-text-o"></i> <span>Pages</span>
            </a>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Promo Codes
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin'))
        <li class="treeview {{ Request::is('admin/promo-codes*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-percent"></i> <span>Promo Codes</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/promo-codes*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/promo-codes') ? 'active' : '' }}">
                    <a href="{{ route('admin.promo-codes.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/promo-codes/create') ? 'active' : '' }}">
                    <a href="{{ route('admin.promo-codes.create') }}">
                        <i class="fa fa-plus"></i> <span>Add new codes</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Orders
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin'))
        <li class="treeview {{ Request::is('admin/orders*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-shopping-cart"></i> <span>Orders</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/orders*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/orders') ? 'active' : '' }}">
                    <a href="{{ route('admin.orders.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/orders/not/completed') ? 'active' : '' }}">
                    <a href="{{ route('admin.orders.not.completed') }}">
                        <i class="fa fa-list"></i> <span>List Pending</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/orders/current/invoices/number') ? 'active' : '' }}">
                    <a href="{{ route('admin.orders.current.invoices.number') }}">
                        <i class="fa fa-file-text"></i> <span>Current Invoices Number</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Artworks' Offers
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin'))
        <li class="{{ Request::is('admin/offers') ? 'active' : '' }}">
            <a href="{{ route('admin.offers.index') }}">
                <i class="fa fa-money"></i> <span>Artworks' Offers</span>
            </a>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Seo Data
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/seo-data*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-line-chart"></i> <span>Seo Data</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/seo-data*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/seo-data') ? 'active' : '' }}">
                    <a href="{{ route('admin.seo-data.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/seo-data/create') ? 'active' : '' }}">
                    <a href="{{ route('admin.seo-data.create') }}">
                        <i class="fa fa-plus"></i> <span>Add new entry</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | 301 Redirects
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/redirects*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-share-square-o"></i> <span>301 Redirects</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/redirects*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/redirects') ? 'active' : '' }}">
                    <a href="{{ route('admin.redirects.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/redirects/create') ? 'active' : '' }}">
                    <a href="{{ route('admin.redirects.create') }}">
                        <i class="fa fa-plus"></i> <span>Add new entry</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif


    <!--
    |--------------------------------------------------------------------------
    | Notifications
    |--------------------------------------------------------------------------
    -->
    @if(
        roleCheck('superadmin|admin') ||
        ( roleCheck('gallerist') && (isPremiumUser() || isTrialUser()) )
    )
        <li class="treeview {{ Request::is('admin/notifications*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-bell"></i> <span>Notifications</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/notifications*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/notifications') ? 'active' : '' }}">
                    <a href="{{ route('admin.notifications.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Bulk Uploads
    |--------------------------------------------------------------------------
    -->
    @if(roleCheck('superadmin|admin|seo'))
        <li class="treeview {{ Request::is('admin/bulk*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-upload"></i> <span>Bulk Edit</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/bulk*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/bulk/download/googleshop') ? 'active' : '' }}">
                    <a href="{{ route('admin.googleshop.bulk.download') }}">
                        <i class="fa fa-file-excel-o"></i> <span>Google Shop</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/bulk/upload/pages') ? 'active' : '' }}">
                    <a href="{{ route('admin.pages.bulk.upload') }}">
                        <i class="fa fa-file-excel-o"></i> <span>Pages</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/bulk/upload/redirects') ? 'active' : '' }}">
                    <a href="{{ route('admin.redirects.bulk.upload') }}">
                        <i class="fa fa-file-excel-o"></i> <span>301 redirects</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    -->
    @if( roleCheck('superadmin|admin') )
        <li class="treeview {{ Request::is('admin/users*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-users"></i> <span>Users</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/users*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/users') ? 'active' : '' }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-list"></i> <span>List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/users/create') ? 'active' : '' }}">
                    <a href="{{ route('admin.users.create') }}">
                        <i class="fa fa-plus"></i> <span>Add new user</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    <!--
    |--------------------------------------------------------------------------
    | Global Configurations
    |--------------------------------------------------------------------------
    -->
    @if( roleCheck('superadmin|admin') )
        <li class="treeview {{ Request::is('admin/configs*') ? 'menu-open' : '' }}">
            <a href="#"><i class="fa fa-cogs"></i> <span>Global Configs</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" style="{{ Request::is('admin/configs*') ? 'display: block' : '' }}">
                <li class="{{ Request::is('admin/configs/free/shipping') ? 'active' : '' }}">
                    <a href="{{ route('admin.configs.free.shipping') }}">
                        <i class="fa fa-list"></i> <span>Free Shipping</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/configs/gallerist/guide') ? 'active' : '' }}">
                    <a href="{{ route('admin.configs.gallerist.guide') }}">
                        <i class="fa fa-file-pdf-o"></i> <span>Gallerist Guide</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

</ul><!-- END - sidebar-menu -->
