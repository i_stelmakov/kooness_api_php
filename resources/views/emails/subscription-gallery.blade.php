@extends('emails.layout')

@section('title')
    {{SUBJECT_MAIL_GALLERIST_ACTIVATION}}
@endsection

@section('content')
    <h2>Request for registration from the gallery {{ $name }}</h2>
    <p><b>Details below:</b></p>
    <p>Contact: {{ $first_name }} {{ $last_name }}</p>
    <br>
    <p>Gallery's name: {{ $name }}</p>
    <p>Email: {{ $email }}</p>
    <p>Webiste: {{ $website }}</p>
    <p>Phone: {{ $phone }}</p>
    <br>
    <p>Address: {{ $street }}</p>
    <p>City: {{ $city }}</p>
    <p>State: {{ $state }}</p>
    <p>Zip code: {{ $zip }}</p>
    <p>Country: {{ $country }}</p>
    <br>
    <p>Events participations: {{ $events }}</p>
@stop