<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('layout_page_id')->unsigned();
            $table->text('contents');
            $table->timestamps();

            $table->foreign('layout_page_id')
                ->references('id')
                ->on('layout_pages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_sliders');
    }
}
