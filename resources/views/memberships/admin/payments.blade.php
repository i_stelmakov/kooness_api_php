@extends('layouts.admin')
@section('title', 'Admin | Galleries Payments')
@section('page-header', 'Galleries Payments')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-body">
                    <table id="table-galleries" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Serial</th>
                            <th>Gallery Name</th>
                            <th>Plan</th>
                            <th>Total</th>
                            <th>Currency</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>PDF</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table-galleries').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [[ 7, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.memberships.payment.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "serial", "bSortable": false},
                    {"data": "gallery_name"},
                    {"data": "plan", "bSortable": false},
                    {"data": "total", "bSortable": false},
                    {"data": "currency"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "pdf", "bSortable": false},
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    // console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                    }
                ]

            });
        });
    </script>
@endsection