<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtworksImage extends Model
{
    protected $guarded = [];

    public function artwork(){
        return $this->hasOne('App\Models\Artwork', 'artwork_id', 'id');
    }
}
