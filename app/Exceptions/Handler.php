<?php

namespace App\Exceptions;

use App\Http\Controllers\FrontendExtendedController;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];



    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    private $_envKind;
    private $_cart_instance;

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof NotFoundHttpException){
            if(strpos($request->getRequestUri(), '/admin') !== false && !Auth::user() && !roleCheck('superadmin|admin|fair|gallerist|seo|artist|editor')){
                return redirect()->route('register');
            }
            new FrontendExtendedController();
            return response()->view('errors.404', [], 404);
        }
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            if(Auth::user() && strpos($request->getRequestUri(), '/admin') !== false  && $request->getRequestUri() != '/admin'){
                return redirect(route('admin.dashboard'));
            }
            if($request->route()->getName() == 'orders.view.checkout'){
                Session::put("from_checkout", true);
            }
            return redirect(route('register'));
        }

        return parent::render($request, $exception);
    }
}
