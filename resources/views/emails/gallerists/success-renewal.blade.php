{{-- Mail - Successful Membership Renewal --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_SUCCESSFUL_RENEWAL}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
			We can confirm the automatic renewal of the services listed below.<br>
			The invoice is attached to this email.<br>
		</p>

		<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; text-align: left;'>
			Billing to
		</h2>

		<address class="address" style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left; font-style: normal;'>
			<b>Company: </b>{{ $payment->full_name }}<br>
			<b>Vat number: </b>{{ $payment->vat_number }}<br>
			<b>Fiscal Code: </b>{{ $payment->fiscal_code }}<br>
			<b>Address: </b>{{ $payment->address }} - {{ $payment->zip }} - {{ $payment->city }}{{ isset($payment->state)? " (" . $payment->state . ")" : ''   }} - {{ \App\Models\Country::find($payment->country)->name}}</p>
		</address>

		<h2 style='color: #48b6ac; display: block; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; text-align: left;'>
			Your Plan
		</h2>

		<table border="0" cellpadding="0" cellspacing="0" width="100%" style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			<tr>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Plan</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Activation</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Renewal</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Payment</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Amount</th>
			</tr>
			<tr>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ $plan->name }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ date('d/m/Y', strtotime($user->activation_date)) }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ date('d/m/Y', strtotime($user->premium_exp_date)) }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ $payment->card_type }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
					{{$payment->subtotal}}{{ $payment->symbol_currency }}
				</td>
			</tr>
			@if( $payment->tax > 0 )
				<tr>
					<td colspan="3" valign="middle" class="td"></td>
					<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px; font-weight: bold;">
						VAT
					</td>
					<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
						{{ round($payment->tax) }}%
					</td>
				</tr>
			@endif
			<tr>
				<td colspan="3" valign="middle" class="td"></td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px; font-weight: bold;">Total</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
					{{ $payment->total }}{{ $payment->symbol_currency }}
				</td>
			</tr>
		</table>

	</div>
@stop
