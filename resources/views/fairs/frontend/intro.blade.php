@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Fairs')

@section('body-class', 'fairs-intro')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'Fairs' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="featured-section" class="container-vertical container-vertical-padding-bottom">
            <div class="container boxed-container">


                {{--Slider Hero--}}
                <div class="slick-carousel">
                    @foreach($slides as $slide)
                        @if($slide->url_wide_box)
                            {{--Slider Item--}}
                            <div class="slide-item">
                                <div class="col-container-with-offset featured-section-item">
                                    <div class="two-third-col featured-section-item-img">
                                        <div class="slider-item-img wide-box">
                                            <a title="{{ $slide->name }}" href="{{ route('fairs.single', [$slide->slug]) }}">
                                                <div class="background-cover lazy" data-src="{{ $slide->url_wide_box }}"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="one-third-col featured-section-item-txt">
                                        <div class="slider-title-box-txt">
                                            <span class="h1">{{ $slide->name }}</span>
                                            <div class="event-date">
                                                <p>From {{ date('d/m/Y', strtotime($slide->start_date)) }} to {{ date('d/m/Y', strtotime($slide->end_date)) }}</p>
                                            </div>
                                            <p>{!! strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($slide->description, 0, 201))); !!}...</p>
                                            <a title="{{ $slide->name }}" class="black-button" href="{{ route('fairs.single', [$slide->slug]) }}">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--Slider Item--}}
                        @endif
                    @endforeach

                </div>
            </div>
        </section>
        @foreach($widgets as $widget)
            @if($widget['template'] == 'tplA')
                @include('partials.widget-template-a', ['data' => $widget])
            @endif
            @if($widget['template'] == 'tplB')
                @include('partials.widget-template-b', ['data' => $widget])
            @endif
        @endforeach
    </div>
@endsection