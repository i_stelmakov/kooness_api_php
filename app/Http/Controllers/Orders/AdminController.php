<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Models\Artwork;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Http\Response;
use Session;
use Validator;
use View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $not_completed = 0;
        return view('orders.admin.index', compact('not_completed'));
    }

    public function notCompleted()
    {
        $not_completed = 1;
        return view('orders.admin.index', compact('not_completed'));
    }

    public function retrieve(Request $request, $flag)
    {
        $columns = array(
            0 => 'id',
            1 => 'serial',
            2 => 'user',
            3 => 'payment_method',
            4 => 'status',
            5 => 'created_at',
            6 => 'options',
        );

        $totalData = Order::whereIncompleteOrder($flag)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $orders = Order::offset($start)
                ->limit($limit)
                ->whereIncompleteOrder($flag)
                ->orderBy($order, $dir)
                ->get();
        }
//        else {
//            $search = $request->input('search.value');
//
//            $orders = Order::where('name', 'LIKE', "%{$search}%")
//                ->offset($start)
//                ->limit($limit)
//                ->orderBy($order, $dir)
//                ->get();
//
//            $totalFiltered = Tag::where('name', 'LIKE', "%{$search}%")
//                ->count();
//        }

        $data = array();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $edit = null;
                $view = route('admin.orders.view', $order->id);
                // $delete = route('admin.tags.destroy', $order->id);
                $delete = null;

                $user = $order->user()->first();

                $nestedData['id'] = $order->id;
                $nestedData['serial'] = "#" . $order->serial;
                $nestedData['user'] = $user->first_name . " " . $user->last_name;
                $nestedData['payment_method'] = get_payment_method($order->payment_method);
                $nestedData['status'] = get_status_order($order->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($order->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete, 'view' => $view]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function view(Request $request, $order)
    {
        $order = Order::find($order);
        if ($order) {
            return view('orders.admin.view', compact('order'));
        }
    }


    public function updateOrderStatus(Request $request, $order)
    {
        $order = Order::find($order);
        if ($request->method() == 'POST' && $order) {
            $status = $request->get('status');
            $order->status = $status;
            if ($order->save()) {
                $user = $order->user()->first();
                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_ORDER_COMPLETED;
                if ($status == 2) {
                    foreach ($order->items()->get() as $orderItem) {
                        $artwork = Artwork::find($orderItem->artwork_id);
                        if ($artwork) {
                            $artwork->status = 2;
                            $artwork->save();
                        }
                    }
                    if((new FrontendController())->generateInvoice($order)) {
                        Mail::send('emails.orders.completed', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                            $message->to($to, $name)->subject($subject);
                            $message->attach(storage_path("/invoices/{$order->serial}.pdf"));
                        });
                    }
                } else if ($status == 3) {
                    $subject = SUBJECT_MAIL_ORDER_SHIPPED;
                    Mail::send('emails.orders.shipped', ["order" => $order], function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });
                }
            }
        }
        return response()->json([
            'status' => 'success',
            'redirectTo' => route('admin.orders.view', [$order->id])
        ]);
    }

    public function currentInvoicesNumber(Request $request){
        if($request->method() == 'POST'){
            $number = $request->get('current_number');
            $json = json_encode(['current_number' => $number, "year" => date('Y')]);
            file_put_contents(storage_path("/config/invoices_" . date('Y') .".json"), $json);
            return response()->json([
                'status' => 'success',
                'redirectTo' => route('admin.orders.current.invoices.number')
            ]);
        }
        if(File::exists(storage_path("/config/invoices_" . date('Y') .".json")))
            $json = file_get_contents(storage_path("/config/invoices_" . date('Y') .".json"));
        else{
            $json = json_encode(['current_number' => 1, "year" => date('Y')]);
            file_put_contents(storage_path("/config/invoices_" . date('Y') .".json"), $json);
        }
        $data = json_decode($json);
        return  view('orders.admin.invoice-number', compact('data'));
    }

    public function currentInvoicesPDF($order_reference) {
        // echo($order_reference);
        $filename = '/invoices/'.$order_reference.'.pdf';
        $pathToFile = storage_path($filename);
        $headers = array(
            'Content-Type: application/pdf',
          );
        return response()->file($pathToFile, $headers);
     }

     public function resendMail(Request $request, $order_id){
        $order = Order::find($order_id);
        if($order_id && $order){

            $user = $order->user()->first();
            $to = $user->email;
            $name = $user->full_name;

            if($order->status == 1){
                $subject = SUBJECT_MAIL_ORDER_RECEIVED;
                Mail::send('emails.orders.received', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                    $message->to($to, $name)->subject($subject);
                });
            } elseif($order->status == 2){
                $subject = SUBJECT_MAIL_ORDER_COMPLETED;
                Mail::send('emails.orders.completed', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                    $message->to($to, $name)->subject($subject);
                    $message->attach(storage_path("/invoices/{$order->serial}.pdf"));
                });
            } elseif($order->status == 3){
                $subject = SUBJECT_MAIL_ORDER_SHIPPED;
                Mail::send('emails.orders.shipped', ["order" => $order], function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });
            }
        }
        return redirect()->back();
     }

     public function updateShippingOrder(Request $request, $order_id){
        $order = Order::find($order_id);
        if($order){
            $shipment_cost = $request->get('shipping');
            $order->shipping = $shipment_cost;
            $order->incomplete_order = false;
            $order->token = bin2hex(random_bytes(50));
            while(Order::whereToken($order->token)->count() > 0){
                $order->token = bin2hex(random_bytes(50));
            }
            $order->amount += $order->shipping;
            $order->save();

            $user = $order->user()->first();
            $to = $user->email;
            $name = $user->full_name;
            $subject = SUBJECT_MAIL_PAY_ORDER;
            Mail::send('emails.orders.received', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                $message->to($to, $name)->subject($subject);
            });
        }
        return response()->json([
             'status' => 'success',
             'redirectTo' => route('admin.orders.index')
        ]);
     }
}
