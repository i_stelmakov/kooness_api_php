@extends('layouts.app')

@section('title')
    Payment Error
@endsection

@section('body-class')
    payment-error
@endsection

@section('content')
    <div class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1 class="thanks-title">Error Occurred</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-vertical-padding-bottom">
            <div class="container boxed-container">
                <i class="fa fa-close main-color-txt thanks-icon"></i>
                {{-- <div class="default-sheet-row">
                    <p>Your order ID is <a href="/users/profile#orders">#</a></p>
                </div> --}}
                <div class="default-sheet-row">
                    <p>An email receipt containing information about your order will soon follow. Please keep it for records.</p>
                </div>
                <div class="default-sheet-row">
                    <p>Contact us at <a href="mailto:info@kooness.it">info@kooness.it</a></p>
                </div>
                <div class="default-sheet-row">
                    <h3>Thank you for purchase at <a href="{{ route('home') }}">Kooness</a></h3>
                </div>
                <a class="black-button" href="{{ route('home') }}">Back to home</a>
            </div>
        </section>
    </div>
@endsection


