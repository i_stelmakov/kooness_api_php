<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 28/06/18
 * Time: 11:11
 */

namespace App\Services;

use autoloadManager;
use DateTime;
use DHL\Datatype\AM\PieceType;
use DHL\Entity\AM\GetQuote;
use DHL\Client\Web as WebserviceClient;

class DHL
{

    private $config = [];

    private $quote = null;

    private $_fromCity;

    private $_fromZip;

    private $_fromCountry;

    private $_toCity;

    private $_toZip;

    private $_toCountry;

    private $_pieces;

    public function __construct()
    {
        define('DHL_API_DIR', base_path() . '/vendor/alfallouji/dhl_api/');
        require_once(DHL_API_DIR . 'vendor/autoloadManager/autoloadManager.php');
        if (!defined('DHL_CONF_API_DIR')) {
            define('DHL_CONF_API_DIR', base_path() . '/vendor/alfallouji/dhl_api/');
        }

        $scanOption = autoloadManager::SCAN_ONCE;
        $autoloadDir = sys_get_temp_dir() . '/dhl-api-autoload.php';

        $autoloadManager = new AutoloadManager($autoloadDir, $scanOption);
        $autoloadManager->addFolder(DHL_API_DIR . 'vendor');
        $autoloadManager->addFolder(DHL_API_DIR . 'DHL');
        $autoloadManager->register();

        $this->config = config('dhl');

        $this->init();
    }

    private function init()
    {
        $this->quote = new GetQuote();
        $this->quote->SiteID = $this->config['dhl']['id'];
        $this->quote->Password = $this->config['dhl']['pass'];
    }

    public function getQuote($debug = false)
    {
        $this->quote->MessageTime = date('Y-m-d\TH:i:sP');
        $this->quote->MessageReference = $this->randomNumber(31);
        $this->quote->BkgDetails->Date = date('Y-m-d');

        foreach ($this->_pieces as $piece) {
            $this->quote->BkgDetails->addPiece($piece);
        }

        $this->quote->BkgDetails->IsDutiable = 'N';
        $this->quote->BkgDetails->ReadyTime = "PT" . date('H') . "H" . date('i') . "M";
        $this->quote->BkgDetails->ReadyTimeGMTOffset = '+02:00';
        $this->quote->BkgDetails->DimensionUnit = 'CM';
        $this->quote->BkgDetails->WeightUnit = 'KG';
        $this->quote->BkgDetails->PaymentCountryCode = 'IT';

        $this->quote->To->CountryCode = $this->_toCountry;
        $this->quote->To->Postalcode = $this->_toZip;
        $this->quote->To->City = $this->_toCity;

        $this->quote->From->CountryCode = $this->_fromCountry;
        $this->quote->From->Postalcode = $this->_fromZip;
        $this->quote->From->City = $this->_fromCity;

        // Call DHL XML API
        // $start = microtime(true);
//        dd( $this->quote);

        $client = new WebserviceClient('staging');
        $xml = $client->call($this->quote);
        // echo PHP_EOL . 'Executed in ' . (microtime(true) - $start) . ' seconds.' . PHP_EOL;

        // Test on results => http://localhost/test/dhl
        if($debug) {
            // dd($xml);

            // come JSON (per leggibilità)
            $xmlToEncode = simplexml_load_string($xml);
            $json = json_encode($xmlToEncode);
            // $array = json_decode($json,TRUE);
            echo($json);

            // test parsing json to PHP object
            // $parsed_response['test'] = array(
            //     'response' => json_decode( json_encode( $xml ), true )
            // );

            // foreach ($parsed_response['test'] as $key => $value) {
            //     echo "$key => $value<br>";
            // }
        }


        $array = simplexml_load_string($xml);
        return $this->parseQuoteResponse(json_decode(json_encode($array)));
    }

    /**
     * @param mixed $fromCity
     */
    public function setFromCity($fromCity): void
    {
        $this->_fromCity = $fromCity;
    }

    /**
     * @param mixed $fromZip
     */
    public function setFromZip($fromZip): void
    {
        $this->_fromZip = $fromZip;
    }

    /**
     * @param mixed $fromCountry
     */
    public function setFromCountry($fromCountry): void
    {
        $this->_fromCountry = $fromCountry;
    }

    /**
     * @param mixed $toCity
     */
    public function setToCity($toCity): void
    {
        $this->_toCity = $toCity;
    }

    /**
     * @param mixed $toZip
     */
    public function setToZip($toZip): void
    {
        $this->_toZip = $toZip;
    }

    /**
     * @param mixed $toCountry
     */
    public function setToCountry($toCountry): void
    {
        $this->_toCountry = $toCountry;
    }

    private function randomNumber($length)
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    /**
     * @param mixed $pieces
     */
    public function setPieces($pieces): void
    {
        $this->_pieces = [];
        foreach ($pieces as $piece) {
            $piece = new Piece($piece);
            $this->_pieces[] = $piece->getPiece();
        }
    }

    public function parseQuoteResponse($response){
        $quote = null;
        if(isset($response->GetQuoteResponse)){
            if(isset($response->GetQuoteResponse->BkgDetails)){
                if(isset($response->GetQuoteResponse->BkgDetails->QtdShp)){
                    // SE dhl restituisce un solo metodo di spedizione ciclo come valori singoli
                    if(isset($response->GetQuoteResponse->BkgDetails->QtdShp->ProductShortName)){
                        if($response->GetQuoteResponse->BkgDetails->QtdShp->ProductShortName == "EXPRESS WORLDWIDE" && $this->_fromCountry != $this->_toCountry){
                            $quote = $response->GetQuoteResponse->BkgDetails->QtdShp->ShippingCharge;
                        }

                        if($response->GetQuoteResponse->BkgDetails->QtdShp->ProductShortName == "EXPRESS DOMESTIC" && $this->_fromCountry == $this->_toCountry){
                            $quote = $response->GetQuoteResponse->BkgDetails->QtdShp->ShippingCharge;
                        }
                    }
                    // ALTRIMENTI loopo tutte le tipologie di spedizione e per ognuna recupero i parametri
                    else {
                        foreach ($response->GetQuoteResponse->BkgDetails->QtdShp as $shipment) {
                            if($shipment->ProductShortName == "EXPRESS WORLDWIDE" && $this->_fromCountry != $this->_toCountry){
                                $quote = $shipment->ShippingCharge;
                            }

                            if($shipment->ProductShortName == "EXPRESS DOMESTIC" && $this->_fromCountry == $this->_toCountry){
                                $quote = $shipment->ShippingCharge;
                            }
                        }
                    }
                }
            }
        }
        // exit;
        if(!$quote)
            return false;

        return (float)($quote);
    }
}