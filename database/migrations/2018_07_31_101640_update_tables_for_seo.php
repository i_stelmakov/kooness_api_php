<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablesForSeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('artists_categories', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('artworks_medium_categories', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('exhibitions', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('fairs', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('galleries_categories', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('generic_seo_data', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('posts_categories', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->string('h1')->nullable()->after('meta_title');
            $table->string('canonical')->nullable()->after('h1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('artists_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('artworks_medium_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('exhibitions', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('fairs', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('galleries_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('generic_seo_data', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('posts_categories', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->dropColumn('h1');
            $table->dropColumn('canonical');
        });
    }
}
