{{-- Artists - Single --}}

@extends('layouts.app')

@section('title', ($artist && $artist->meta_title) ? $artist->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$artist->full_name}"))

@section('body-class', 'artists-single')

@section('header-og-abstract', ($artist && $artist->meta_description) ? $artist->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($artist && $artist->canonical) ? $artist->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($artist && $artist->meta_keywords) ? $artist->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($artist->url_full_image) ? $artist->url_full_image : null)

@section('content')

    <div class="sections">
        <section class="default-sheet-section">
            <div class="container boxed-container">
                <div class="container col-container">
                    <!-- Product Sheet Image Col !-->
                    <div id="product-img-col" class="col one-half-col">
                        <!-- Product Sheet Image !-->
                        {{--<div class="slider-item-img square-box">--}}
                            {{--<a  href="{{ $artist->url_full_image }}" data-lightbox="example-set">--}}
                                {{--<div class="contain-image" style="background-image: url({{ $artist->url_full_image }})"></div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        <div class="slider-item-img">
                            <a href="{{ $artist->url_full_image }}" data-lightbox="example-set">
                                <img class="lazy" data-src="{{ $artist->url_full_image }}" src=""/>
                            </a>
                        </div>
                    </div>
                    <!-- Artist Sheet Col !-->
                    <div id="default-sheet" class="col one-half-col">
                        <!-- Artist Info !-->
                        <div id="default-sheet-info">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h1 classs="no-margin">{{ ($artist->h1)? $artist->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $artist->full_name) }}</h1>
                                </div>
                                {{--Follow section--}}
                                @if(Auth::user())
                                    <a class="follow secondary-link {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                        @if(Auth::user()->collectionArtists->contains($artist->id))
                                            <i class="fa fa-heart"> </i>
                                            <span class="follow-unfollow">
                                            Unfollow
                                        </span>
                                        @else
                                            <i class="fa fa-heart-o"> </i>
                                            <span class="follow-unfollow">
                                            Follow
                                        </span>
                                        @endif
                                    </a>
                                @endif
                            </div>
                            <div class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <p>
                                        <span class="dark-text">{{ $artist->date_of_birth }} 
                                        @if($artist->date_of_death)
                                            - {{ $artist->date_of_death }} 
                                        @endif
                                        </span>@if($artist->city_of_birth){{ $artist->city_of_birth }},@endif {{ $artist->countryBirth()->first()->name }}
                                    </p>
                                </div>
                                <div class="default-sheet-row">
                                    <p>
                                        {{ $artist->artworks()->count() }} Works exhibited </p>
                                </div>
                            </div>
                            <div class="default-sheet-box">
                                @if($artist->current_location)
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">
                                                <strong>Current location</strong>
                                            </p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <p>{{ $artist->current_location }}</p>
                                        </div>
                                    </div>
                                @endif
                                @if($artist->galleries()->count())
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">
                                                <strong>Represented by</strong>
                                            </p>
                                        </div>
                                        <div id="default-sheet-category" class="default-sheet-row-cell">
                                            <ul>
                                                @foreach($artist->galleries()->get() as $gallery)
                                                    <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!-- Gallery Sheet Category/Tags !-->
                            <div id="default-sheet-category" class="default-sheet-box">
                                @if($artist->categories()->count())
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">
                                                <strong>Category</strong>
                                            </p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <ul>
                                                <li>
                                                    @foreach($artist->categories()->get() as $category_key=>$category)
                                                        <a title="{{ $category->name }}" href="{{ route('artists.single', [$category->slug]) }}">
                                                            {{ $category->name }}
                                                        </a>@if (!$loop->last), @endif
                                                    @endforeach
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                                @if($artist->tags()->count())
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">
                                                <strong>Tags</strong>
                                            </p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <ul>
                                                @foreach($artist->tags()->get() as $tag_key=>$tag)
                                                    <li>
                                                        <a title="{{ $tag->name }}" href="{{ route('tags.single', [$tag->slug]) }}">{{ $tag->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!-- Gallery Sheet Social !-->
                            <div id="default-sheet-info" class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Share</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <div class="icon-container">
                                            <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                            <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                            <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                            <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                            <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($artist->about_the_artist, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container boxed-container">
            <hr class="divider"/>
        </div>
        <!-- Description !-->
        <section class="tab-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin container-vertical-padding-top container-vertical-padding-bottom">
                    <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                        <ul>
                            @if($artist->about_the_artist)
                                <li class="current-tab-item tab-button" data-id="1">About the Artist</li>
                            @endif
                            @if($artist->meet_the_artist)
                                <li class="tab-button" data-id="6">Meet the Artist</li>
                            @endif
                            @if($artist->curriculum_vitae)
                                <li class="tab-button" data-id="2">Curriculum Vitae</li>
                            @endif
                            @if($artist->bibliography)
                                <li class="tab-button" data-id="3">Bibliography</li>
                            @endif
                            @if($artist->awards)
                                <li class="tab-button" data-id="4">Awards</li>
                            @endif
                            @if($artist->news()->whereStatus(1)->count())
                                <li class="tab-button" data-id="5">News</li>
                            @endif
                        </ul>
                    </div>
                    <!-- Tab 1 !-->
                    <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $artist->about_the_artist !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 2 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $artist->curriculum_vitae !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 3 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $artist->bibliography !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                      </div>
                      <!-- Tab 4 !-->
                      <div class="tab-container four-fifths-col-with-margin tab" data-id="4">
                          <div class="show-more-text-container">
                              <div class="show-more-content">
                                  <div class="show-more-content-inner">
                                      {!! $artist->awards !!}
                                  </div>
                              </div>
                              <div class="show-more-button">
                                  <a href="#">Show more</a>
                              </div>
                          </div>​
                      </div>
                      <!-- Tab 5 !-->
                      @if($artist->news()->whereStatus(1)->count())
                          <!-- Tab 2 !-->
                          <div class="tab-container four-fifths-col-with-margin tab" data-id="5">
                              <section id="featured-news">
                                  <div class="container boxed-container">
                                      <div class="default-slider-header">
                                          <span class="h1">Featured News</span>
                                          <p>
                                              <a class="read-more" href="{{ route('posts.archive.all', ["news"]) }}">All featured news</a>
                                          </p>
                                      </div>
                                      <div class="container col-container-with-offset-and-margin">
                                          @foreach($artist->news()->whereStatus(1)->limit(3)->get() as $news)
                                              <div class="slider-item one-third-col-with-margin">
                                                  <div class="slider-item-img wide-box">
                                                      <a title="{{ $news->title }}" href="{{ route('posts.single', ["news", $news->slug]) }}">
                                                          <div class="background-cover gallery lazy" data-src="{{ $news->url_wide_box }}"></div>
                                                      </a>
                                                  </div>
                                                  <div class="featured-news-item-txt">
                                                      <p class="news-date">{{  date('d F Y', strtotime($news->date)) }}</p>
                                                      <div class="default-sheet-row">
                                                          <h2>
                                                              <a class="dark-text" href="{{ route('posts.single', ["news", $news->slug]) }}">{{ $news->title }}</a>
                                                          </h2>
                                                      </div>
                                                      <div class="default-sheet-row-cell">
                                                          <p class="dark-text news-author">By <a>{{ $news->author }}</a>
                                                          <p>
                                                      </div>
                                                  </div>
                                              </div>
                                          @endforeach
                                      </div>
                                  </div>
                              </section>
                          </div>
                      @endif
                  <!-- Tab 6 !-->
                      <div class="lama-videos tab-container four-fifths-col-with-margin tab" data-id="6">
                          <?php
                              if ($artist->meet_the_artist) {

                                  $curl = curl_init();
                                  curl_setopt_array($curl, array(
                                      // CURLOPT_URL => "https://lama-app.com/api/v4/requests?user_id=113021", // working with video answers
                                      CURLOPT_URL => "https://lama-app.com/api/v4/requests?user_id=$artist->meet_the_artist", // working with automatic id retrieve
                                      CURLOPT_RETURNTRANSFER => true,
                                      CURLOPT_ENCODING => "",
                                      CURLOPT_MAXREDIRS => 10,
                                      CURLOPT_TIMEOUT => 30,
                                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                      CURLOPT_CUSTOMREQUEST => "GET",
                                      CURLOPT_HTTPHEADER => array(
                                          "Authorization: Token 0051c52ca26e115bbfeb2f4a1cae10d1",
                                          "Cache-Control: no-cache",
                                          "Content-Type: application/json",
                                          "Postman-Token: 36c47ae5-2c86-464d-9548-05c40bb1a4d4"
                                      ),
                                  ));

                                  $response = curl_exec($curl);
                                  $err = curl_error($curl);

                                  curl_close($curl);

                                  if ($err) {
                                        //  echo "cURL Error #:" . $err;
                                      echo "no video";

                                  } else {
                                      $responseObject = json_decode($response);
                                      //var_dump($responseObject);
                                      echo('<div class="container col-container-with-offset-and-margin">');
                                      // foreach video in response
                                      foreach($responseObject->requests as $item) {
                                          if($item->answer && $item->answer->thumbnail) {
                                          ?>
                                          <div class="one-third-col-with-margin">
                                              <div class="video-item">
                                                  <video width="100%" controls poster="{{ $item->answer->thumbnail }}">
                                                      <source src="{{ $item->answer->video }}" type="video/mp4">
                                                      Your browser does not support HTML5 video.
                                                  </video>
                                                  <div class="slider-item-txt">
                                                      <h2>{{$item->text}}</h2>
                                                  </div>
                                              </div>
                                          </div>
                                      <?php
                                          }
                                      }
                                  echo('</div>');
                                  }
                              }
                          ?>
                        {{--<div style="position:relative;padding-top:56.25%;">--}}
                            {{--<iframe src="https://lama-app.com/story/{!! $artist->meet_the_artist !!}/widget/videos" frameborder="0" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>--}}
                        {{--</div>--}}
{{--                        {{ $artist->meet_the_artist }}--}}

                      </div>
                </div>
            </div>
        </section>
        <!-- Archive !-->
        <section id="archive" class="artist-archive">
            <div class="container boxed-container">
                <div class="container col-container">
                    <div id="archive-col" class="col three-fourth-col">
                        <!-- Archive Header !-->
                        <div id="archive-header">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h3>Works by {{ $artist->first_name }} {{ $artist->last_name }}</h3>
                                </div>
                                <div class="default-sheet-row-cell">
                                    <p>Order by</p>
                                    <form>
                                        <select class="search-by" id="order-works-artist">
                                            <option value="recent" selected="selected">Most recent</option>
                                            <option value="higher-price">Higher price</option>
                                            <option value="lower-price">Lower price</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>


                            <section>
                                <div class="container boxed-container">
                    
                                    <!-- Masonry Grid !-->
                                    <div id="masonry-grid" class="container-vertical-padding-top container-load">
                                        <div class="grid-sizer"></div>
                                        <div class="gutter-sizer"></div>
                                        <!-- Masonry Item !-->

                                        @foreach($artist->artworks()->whereRaw("status IN (1,2)")->orderBy("$order_by", $order_dir)->get() as $artwork_key=>$artwork)
                                            <div class="masonry-item grid-item">
                                                @if(Auth::user())
                                                    <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                                        @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                                            <i class="fa fa-heart"></i>
                                                        @else
                                                            <i class="fa fa-heart-o"></i>
                                                        @endif
                                                    </div>
                                                @endif
                                                <a class="masonry-item-img-link"
                                                   href="{{ route('artworks.single', [$artwork->slug]) }}">
                                                    <img class="lazy" data-src="{!! ($artwork->main_image->url) ? $artwork->main_image->url : '/images/default-cover.jpg' !!}" src="">
                                                </a>
                                                <div class="masonry-item-txt">
                                                    <div class="default-sheet-row">
                                                        <h2>
                                                            <a class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a>
                                                        </h2>
                                                    </div>
                                                    <div class="slider-item-txt">
                                                        <p class="artwork-date">{{ $artwork->year }}</p>
                                                        <p class="artwork-measures">{{ $artwork->measure_cm }} cm</p>
                                                        <div class="default-sheet-row slider-item-row">
                                                            <div class="default-sheet-row-cell">
                                                                <p class="artwork-type">
                                                                    @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $ArtworkMedium)
                                                                        <a title="{{ $ArtworkMedium->name }}" href="{{ route('artworks.single', [$ArtworkMedium->slug]) }}">{{ $ArtworkMedium->name }}</a>
                                                                        <br>
                                                                    @endforeach
                                                                </p>
                                                            </div>
                                                            {{-- SE artwork non in fiera OPPURE artwork impone visualizzazione prezzo --}}
                                                            @if(!$artwork->available_in_fair || $artwork->show_price)
                                                                <div class="default-sheet-row-cell">
                                                                    {{-- SE artwork segnato come disponibile E artwork non è in nessuna fiera --}}
                                                                    @if( $artwork->status == 1 && !$artwork->available_in_fair)
                                                                        <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                                                                    @else
                                                                        <p class="dark-text artwork-price">SOLD OUT</p>
                                                                    @endif
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <!-- Masonry Item !-->
                                    </div>
                                    <!-- Masonry Grid !-->
                    
                                </div>
                            </section>
                    

                        {{-- Precedente versione - NO masonry script --}}
                        {{-- <section id="product-masonry" class="default-slider-section">
                            <div class="product-masonry-container masonry-container">
                                <!-- Item 1 -->
                                <div class="grid">
                                    @foreach($artist->artworks()->whereRaw("status IN (1,2)")->get() as $artwork_key=>$artwork)
                                        <div class="slider-item">

                                            @if(Auth::user())
                                                <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                                    @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                                        <i class="fa fa-heart"></i>
                                                    @else
                                                        <i class="fa fa-heart-o"></i>
                                                    @endif
                                                </div>
                                            @endif

                                            <div class="slider-item-img">
                                                <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                                                    <img class="lazy" data-src="{{ $artwork->main_image->url }}" src=""> </a>
                                            </div>
                                            <div class="slider-item-txt">
                                                <h2>
                                                    <a class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a>
                                                </h2>
                                                <p class="artwork-date">{{ $artwork->year }}</p>
                                                <p class="artwork-measures">{{ $artwork->measure_cm }} cm</p>
                                                <div class="default-sheet-row slider-item-row">
                                                    <div class="default-sheet-row-cell">
                                                        <p class="artwork-type">
                                                            @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $ArtworkMedium)
                                                                <a title="{{ $ArtworkMedium->name }}" href="{{ route('artworks.single', [$ArtworkMedium->slug]) }}">{{ $ArtworkMedium->name }}</a>
                                                                <br>
                                                            @endforeach
                                                        </p>
                                                    </div>
                                                    @if(!$artwork->available_in_fair || $artwork->show_price)
                                                        <div class="default-sheet-row-cell">
                                                            <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section> --}}
                        {{-- .Precedente versione - NO masonry script --}}

                    </div>
                </div>
            </div>
        </section>
        <!-- Post Navigation !-->
        <div class="container boxed-container">
            <div id="post-nav" class="container col-container-with-offset">
                @if($prev)
                    <div class="col one-third-col prev">
                        <div class="default-sheet-col">
                            <a class="prev-a" href="{{ route('artists.single', [$prev->slug]) }}">Prev Artist</a>
                            <h3>
                                <a href="{{ route('artists.single', [$prev->slug]) }}">{{ $prev->first_name }} {{ $prev->last_name }}</a>
                            </h3>
                        </div>
                    </div>
                @endif
                @if($next)
                    <div class="col one-third-col next">
                        <div class="default-sheet-col">
                            <a class="next-a" title="{{ $next->first_name }} {{ $next->last_name }}" href="{{ route('artists.single', [$next->slug]) }}">Next Artist</a>
                            <h3>
                                <a title="{{ $next->first_name }} {{ $next->last_name }}" href="{{ route('artists.single', [$next->slug]) }}">{{ $next->first_name }} {{ $next->last_name }}</a>
                            </h3>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection