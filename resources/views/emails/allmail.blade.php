{{-- Mail - All Order Mail --}}
<!doctype html>
    <html lang="it">
    <head>
        <meta charset="utf-8">
        <title>All Mail templates Demo</title>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css" type="text/css" >
        <style>
            body {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
                font-weight: 300;
            }
            .resp-container {
                position: relative;
                /*overflow: hidden;*/
                padding-top: 240%;
                width: 130%;

                -ms-zoom: 0.70;
                -moz-transform: scale(0.7);
                -moz-transform-origin: 0 0;
                -o-transform: scale(0.7);
                -o-transform-origin: 0 0;
                -webkit-transform: scale(0.7);
                -webkit-transform-origin: 0 0;
                /* border: 1px solid red; */
            }

            .resp-iframe {
                position: absolute;
                top: 0;
                left: 0;
                width: 130%;
                height: 150%;
                border: 0;
                /*overflow:hidden !important;*/
                /* border: 1px solid blue; */
            }

            * {
                /*box-shadow: 0 0 0 1px inset aqua;*/
            }

            ::-webkit-scrollbar {
                display: none;
            }

        </style>
    </head>
    <body>

        {{-- Order Mail --}}
        <h1>Order Mail</h1>
        <div class="row">
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/order/order-received-admin/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/order/order-received/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/order/order-completed/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/order/order-shipped/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        {{-- Offer Mail --}}
        <h1>Offer Mail</h1>
        <div class="row">
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/offer/offer-received-admin/3973" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/offer/offer-accepted/3799" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/offer/offer-declined/3799" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        {{-- Registration Mail --}}
        <h1>Registration Mail</h1>
        <div class="row">
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/registration/user-registered/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/registration/activation/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/registration/reset-password/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>

        {{-- Info Request Mail --}}
        <h1>Info Request Mail</h1>
        <div class="row">
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/contact-form/info-request/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>


        {{-- Gallerist Mail --}}
        <h1>Gallerist Mail</h1>
        <div class="row">
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/activation/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/cancellation/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/card-expiration/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/pre-renewal/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/renewal-not-successful/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/success-renewal/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/trial-deadline/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="resp-container">
                    <iframe class="resp-iframe" src="/test/mail/gallerists/change-plan/1" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>

    </body>
</html>