@extends('layouts.admin')
@section('title', 'Admin | Create Gallery')
@section('page-header', 'Admin - Create Gallery')

@section('content')
    @include('galleries.admin.form', ['action' => route('admin.galleries.store'), 'method' => 'POST'])
@endsection