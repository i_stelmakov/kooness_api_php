<style>
    form .error {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $code->id }}" data-referer="code">
            {{ method_field($method) }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Name (*)</label>
                            <input id="name" class="form-control" type="text" placeholder="Name" name="name" value="{{ old('name', $code->{'name'}) }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="value">Value (*)</label>
                            <input id="value" class="form-control" type="number" placeholder="0.00" name="value" value="{{ old('value', $code->{'value'}) }}" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="status">Status (*)</label>
                            <select class="form-control" id="status" name="status">
                                <option value="1" {{ ($code->{'status'} == 1)  ? 'selected="selected"' : ''  }}>Activated for Newsletter subscription</option>
                                <option value="2" {{ ($code->{'status'} == 2)  ? 'selected="selected"' : ''  }}>Active and applicable via MailChimp</option>
                                <option value="3" {{ ($code->{'status'} == 3)  ? 'selected="selected"' : ''  }}>Not active</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>