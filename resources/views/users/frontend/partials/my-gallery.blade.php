<div class="slider-item one-third-col-with-margin" data-collection-id="{{ $collection->id }}">
    @foreach($collection->artworks()->get() as $artwork)
        @if($loop->first)
            <div class="slider-item-img square-box" data-referer="{{ $artwork->pivot->id }}">
                <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                    <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                </a>
            </div>
        @endif
        @if($loop->count > 1 && $loop->first)
            <div class="col-container-with-offset">
        @endif
        @if(!$loop->first)
            <div class="slider-item-img square-box one-fourth-col" data-referer="{{ $artwork->pivot->id }}">
                <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                    <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                </a>
            </div>
        @endif
        @if($loop->count > 1 && $loop->last)
            </div>
        @endif
    @endforeach
    <div class="slider-item-txt">
        <h2><a class="dark-text my-gallery-title">{{ $collection->name }}</a></h2>
        <div class="default-sheet-row slider-item-row">
            <div class="default-sheet-row-cell">
                <p class="artwork-type">
                    <a class="modal-my-gallery-trigger" data-modal-id="{{ $collection->id }}">Edit</a>
                </p>
            </div>
            <div class="default-sheet-row-cell">
                <p class="dark-text artwork-price">
                    <a  data-collection-id="{{ $collection->id }}" class="remove-collection">Delete</a>
                </p>
            </div>
        </div>
    </div>
</div>