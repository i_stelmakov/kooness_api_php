@extends('layouts.app')
@section('title', 'Change your password')

@section('content')
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>Change your password</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container boxed-container">
        <form method="POST" action="{{ route('password.request') }}" class="col-container-with-offset-and-margin k-form">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <input id="email" class="one-half-col-with-margin" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required />
            <input id="password" class="full-col-with-margin {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Enter your password"  name="password" value="" required/>
            <input id="password_confirmation" class="full-col-with-margin {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" placeholder="Repeat your password" autocomplete="false" name="password_confirmation" value="" required/>
            <div class="full-col-with-margin">
                <input class="default-input-button" type="submit" value="Reset Password" />
            </div>
        </form>
    </div>
@endsection

