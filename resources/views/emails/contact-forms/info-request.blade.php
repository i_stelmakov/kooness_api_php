{{-- Mail - Offer Accepted --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_REGISTRATION_INFO_REQUEST}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
		You have received a request for information from the site, below the details of the message.<br><br>
		Posted by: {{ $name }}<br>
		Email: {{ $email }}<br>
		Subject: {{ $subject }}<br>
		Message: {{ $body }}<br>
	</p>
@stop