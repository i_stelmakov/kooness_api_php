<?php

namespace App\Console\Commands;

use App\Models\MbrPlan;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MbrTrialAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:trial-alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereRaw('(DATE(NOW()) = DATE_ADD(`trial_activation_date`, INTERVAL 23 DAY) AND `deactivate_premium_request_date` IS NULL AND `is_trial`')->get();
        foreach ($users as $user) {

            $to = $user->email;
            $name = $user->full_name;
            $subject = SUBJECT_MAIL_MEMBERSHIP_TRIAL_DEADLINE;
            $extras = [
                "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
            ];
            Mail::send('emails.gallerists.trial-deadline', $extras, function ($message) use ($to, $name, $subject) {
                $message->to($to, $name)->subject($subject);
            });
        }
    }
}
