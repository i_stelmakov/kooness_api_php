@extends('layouts.admin')
@section('title', 'Admin | Create Exhibition')
@section('page-header', 'Admin - Create Exhibition')

@section('content')
    @include('exhibitions.admin.form', ['action' => route('admin.exhibitions.store'), 'method' => 'POST'])
@endsection