<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\HomeSection;
use App\Models\LayoutPage;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Spatie\GoogleTagManager\GoogleTagManagerFacade as GoogleTagManager;

class FrontendController extends FrontendExtendedController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Google Tag Manager
        GoogleTagManager::set('pageType', 'home');

        $blocksRows = HomeSection::all();
        $blocks = [];
        foreach ($blocksRows as $blocksRow) {
            $tmp = [
                "title" => null,
                "view_all" => null,
                "view_all_label" => null,
                "view_all_label_2" => null,
                "bg" => null,
                "links" => [],
                "caption" => ''
            ];
            $content = json_decode($blocksRow->content);
            if (!count((array)$content))
                continue;
            if ($content->sections == 'artworks' || $content->sections == 'categories') {
                if ($content->sections == 'artworks') {
                    $tmp['title'] = "Artworks";
                    $tmp['view_all'] = route('artworks.archive.all');
                    $tmp['view_all_label'] = "All Artworks";
                    $tmp['view_all_label_2'] = "Browse Artworks";
                } else {
                    $tmp['title'] = "Categories";
                    $tmp['view_all'] = route('artworks.archive.all');
                    $tmp['view_all_label'] = "All Artworks";
                    $tmp['view_all_label_2'] = "Browse Artworks";
                }

                $artwork = Artwork::find($content->{'bg-image'});
                if ($artwork) {
                    $image = $artwork->images()->whereLabel(1)->first();
                    if ($image) {
                        $tmp['bg'] = $image->url;
                    }
                    if(!isset($content->{'caption'}) || $content->{'caption'} == ''){
                        $tmp['caption'] = $artwork->title;
                    } else{
                        $tmp['caption'] = $content->{'caption'};
                    }
                }
                $key = 0;
                foreach ($content->link as $link) {
                    if ($link) {
                        $artworkCategory = ArtworksCategory::find($link);
                        if ($artworkCategory) {
                            $tmp['links'][$key]['name'] = $artworkCategory->name;
                            if ($content->sections == 'artworks')
                                $tmp['links'][$key]['link'] = route('artworks.single', [$artworkCategory->slug]);
                            else
                                $tmp['links'][$key]['link'] = route('artworks.single', [$artworkCategory->slug]);
                            $key++;
                        }
                    }
                }
            } else if ($content->sections == 'artists') {
                $tmp['title'] = "Artists";
                $tmp['view_all'] = route('artists.archive.all');
                $tmp['view_all_label'] = "All Artists";
                $tmp['view_all_label_2'] = "Browse Artists";
                $artist = Artist::find($content->{'bg-image'});
                if ($artist && $artist->url_full_image) {
                    $tmp['bg'] = $artist->url_full_image;
                }
                $key = 0;
                if(!isset($content->{'caption'}) || $content->{'caption'} == ''){
                    $tmp['caption'] = $artist->full_name;
                } else{
                    $tmp['caption'] = $content->{'caption'};
                }
                foreach ($content->link as $link) {
                    if ($link) {
                        $artist = Artist::find($link);
                        if ($artist) {
                            $tmp['links'][$key]['name'] = $artist->last_name . " " . $artist->first_name;
                            $tmp['links'][$key]['link'] = route('artists.single', [$artist->slug]);
                            $key++;
                        }
                    }
                }
            } else if ($content->sections == 'galleries') {
                $tmp['title'] = "Galleries";
                $tmp['view_all'] = route('galleries.archive.all');
                $tmp['view_all_label'] = "All Galleries";
                $tmp['view_all_label_2'] = "Browse Galleries";
                $gallery = Gallery::find($content->{'bg-image'});
                if ($gallery && $gallery->url_full_image) {
                    $tmp['bg'] = $gallery->url_full_image;
                }
                $key = 0;

                if(!isset($content->{'caption'}) || $content->{'caption'} == ''){
                    $tmp['caption'] = $gallery->name;
                } else{
                    $tmp['caption'] = $content->{'caption'};
                }
                // If user has some favourites get these...
//                if(Auth::user()->collectionGalleries()->count()) {
//                    foreach (Auth::user()->collectionGalleries()->limit(4)->get() as $gallery) {
//                        $tmp['links'][$gallery->id]['name'] = $gallery->name.'🔵';
//                        $tmp['links'][$gallery->id]['link'] = route('galleries.single', [$gallery->slug]);
//                        $key++;
//                    }
//                }

                // ...then show the standard categories
                foreach ($content->link as $link) if (count($tmp['links']) < 4) {
                    if ($link) {
                        $gallery = Gallery::find($link);
                        if ($gallery && !isset($tmp['links'][$gallery->id]) ) {
                            $tmp['links'][$gallery->id]['name'] = $gallery->name;
                            $tmp['links'][$gallery->id]['link'] = route('galleries.single', [$gallery->slug]);
                            $key++;
                        }
                    }
                }

            } else if ($content->sections == 'fairs') {
                $tmp['title'] = "Fairs";
                $tmp['view_all'] = route('fairs.archive.all');
                $tmp['view_all_label'] = "All Fairs";
                $tmp['view_all_label_2'] = "Browse Fairs";
                $fair = Fair::find($content->{'bg-image'});
                if ($fair && $fair->url_full_image) {
                    $tmp['bg'] = $fair->url_full_image;
                }
                if(!isset($content->{'caption'}) || $content->{'caption'} == ''){
                    $tmp['caption'] = $fair->name;
                } else{
                    $tmp['caption'] = $content->{'caption'};
                }
                $key = 0;
                foreach ($content->link as $link) {
                    if ($link) {
                        $fair = Fair::find($link);
                        if ($fair) {
                            $tmp['links'][$key]['name'] = $fair->name;
                            $tmp['links'][$key]['link'] = route('fairs.single', [$fair->slug]);
                            $key++;
                        }
                    }
                }
            }
            $blocks[$blocksRow->pointer] = $tmp;
        }

        $layout = LayoutPage::whereLabel('home')->first();
        $widgets = [];
        if ($layout) {
            $result = $layout->widgets()->orderBy('order', 'ASC')->get();
            foreach ($result as $row) {
                $tmp = [
                    'title' => $row->title,
                    'subtitle' => $row->subtitle,
                    'description' => $row->description,
                    'section' => $row->section,
                    'letter' => 'a',
                    'link' => null,
                    'link_title' => "View all",
                    'template' => $row->template,
                    'items' => []
                ];
                $items = json_decode($row->items);
                foreach ($items as $key=>$item) {
                    if ($row->section == 'artworks') {
                        $rows = Artwork::find($item);
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artworks";
                    } else if ($row->section == 'artists') {
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artists";
                        $rows = Artist::find($item);
                    } else if ($row->section == 'galleries') {
                        $tmp['letter'] = 'g';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured galleries";
                        $rows = Gallery::find($item);
                    } else if ($row->section == 'exhibitions') {
                        if ($row->template == 'tplC')
                            $tmp['link_title'] = "All featured events";
                        $rows = Exhibition::find($item);
                    } else if ($row->section == 'fairs') {
                        $tmp['letter'] = 'f';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured events";
                        $rows = Fair::find($item);
                    } else if ($row->section == 'magazine') {
                        $tmp['letter'] = 'f';
                        if ($row->template == 'tplCa')
                            $tmp['link_title'] = "All featured post";
                        $rows = Post::find($item);
                    }
                    $tmp['link'] = $row->link;
                    if (isset($rows) && $rows) {
                        $tmp['items'][$key] = $rows;
                    }
                }
                $widgets[] = $tmp;
            }
        }
        return view('home.frontend.index', compact('blocks', 'widgets'));
    }
}
