{{-- User - Collections --}}

@extends('layouts.app')

@section('title')
    User - Collections
@endsection

@section('title', 'Collections')

@section('body-class')
    user-collections
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/collections.js"></script>
@endsection

@section('content')

    {{-- Gallery Modal--}}
    <div class="remodal" data-remodal-id="modal-my-gallery" data-remodal-options="hashTracking: false">
        <div id="loader-modal"></div>
        <div class="content-loader" id="loader-modal-content">

            <button data-remodal-action="close" class="remodal-close"></button>

            <form class="collect-form">
                <h2 class="collect-form-header">Collection Name:</h2>
                <input type="text" name="collection-name" placeholder="Collection name">
                <span class="horizontal-space-md"></span> <a class="change-collection-name save-collection-name">
                    <i class="fa fa-floppy-o"></i> Save </a>

                <h3 class="collect-form-header">Artworks list:</h3>

                <div class="gallery-items-list">

                </div>

                <div>
                    <button class="collection-close small-input-button" data-remodal-action="cancel">Done</button>
                </div>
            </form>

        </div>
    </div>
    {{-- Gallery Modal --}}

    <section id="my-galleries-slider" class="default-slider-section slider-border">
        <div class="container boxed-container">
            <div class="default-slider-header">
                <h1>My Collections</h1>
            </div>
            <div class="container col-container-with-offset-and-margin gallery-container">
                @if(Auth::user()->collections()->count())
                    @foreach(Auth::user()->collections()->get() as $collection)
                        @include('users.frontend.partials.my-gallery', ['collection' => $collection])
                    @endforeach
                @else
                    No favourite
                @endif
            </div>
        </div>
    </section>

@endsection