@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Exhibitions')

@section('body-class', 'exhibitions-archive')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)


@section('content')
    <div class="sections">
        <!-- exhibition Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">

                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'Exhibitions' }}</h1>
                        </div>
                    </div>
                    <div class="page-header-filter">
                        <div class="default-sheet-row-cell">
                            <p>Filter by</p>
                            <form>
                                <select class="search-by category" onchange="window.location.href=window.location.pathname + ((this.value !== '') ? '?filtered_by=' + this.value : '')">
                                    <option value="">All</option>
                                    <option value="past" {{ ($filter_by == 'past')? 'selected="selected"': '' }}>Past</option>
                                    <option value="current" {{ ($filter_by == 'current')? 'selected="selected"': '' }}>Current</option>
                                    <option value="future" {{ ($filter_by == 'future')? 'selected="selected"': '' }}>Future</option>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-container-with-offset">
                    <div class="category-filter-container container-load">
                        @foreach($exhibitions as $exhibition_key=>$exhibition)
                            <div class="slider-item one-third-col-with-margin">
                                <div class="slider-item-img wide-box">
                                    <a title="{{ $exhibition->name }}" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $exhibition->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <h2><a title="{{ $exhibition->name }}" class="dark-text" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">{{ $exhibition->name }}</a></h2>
                                    <div class="default-sheet-row slider-item-row" style="margin-top: 5px">
                                        <div class="default-sheet-row-cell">
                                            <h6><a href="{{ route('galleries.single', [$exhibition->gallery()->first()->slug]) }}">{{ $exhibition->gallery()->first()->name }}</a></h6>
                                        </div>
                                    </div>
                                    <div class="event-date">
                                        <p class="event-date-p">{{ $exhibition->city }}, {{ $exhibition->country()->first()->code }}</p>
                                    </div>
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($exhibition->start_date)) }} to {{ date('d/m/Y', strtotime($exhibition->end_date)) }}</p>
                                        </div>
                                    </div>
                                    <a title="Read more" class="read-more" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">Read more</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                {!! $exhibitions->links('layouts.includes.pagination') !!}
                </div>
            </div>
        </section>
    </div>
@endsection