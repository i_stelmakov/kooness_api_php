<div class="col-md-12 col-xs-12">


    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            Thumbnails
        </div>
        <div class="box-body">
            <div class="col-md-12">
                @if($square_box_active)
                    <div class="col-md-12">
                        <p>If no cropped image is inserted will be used the first image from above section</p>
                    </div>
                    <div class="col-md-4">
                        <label>Square Box (usato nei widget)<br>
                            <small>&nbsp;</small>
                        </label>
                        @if(!$item->url_square_box)
                            <img src="/images/placeholder-square-box.jpg" class="img-responsive img-preview-square-box">
                        @else
                            <img src="{{ asset($item->url_square_box) }}" class="img-responsive img-preview-square-box">
                        @endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Upload new image…
                                        <input type="file"
                                               class="img-uploader"
                                               data-label="square_box"
                                               data-preview=".img-preview-square-box"
                                               data-minWidth="270"
                                               data-minHeight="270"
                                               data-maxWidth="2048"
                                               data-maxHeight="2048"
                                               data-cropper="true"
                                               data-aspectRatio="1">
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                @endif
                @if($wide_box_active)
                    <div class="col-md-4">
                        <label>Wide Box (usato nei recents)<br>
                            <small>&nbsp;</small>
                        </label>
                        @if(!$item->url_wide_box)
                            <img src="/images/placeholder-wide-box.jpg" class="img-responsive img-preview-wide-box">
                        @else
                            <img src="{{ asset($item->url_wide_box) }}" class="img-responsive img-preview-wide-box">
                        @endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Upload new image…
                                        <input type="file"
                                               class="img-uploader"
                                               data-label="wide_box"
                                               data-preview=".img-preview-wide-box"
                                               data-minWidth="800"
                                               data-minHeight="450"
                                               data-maxWidth="2048"
                                               data-maxHeight="2048"
                                               data-cropper="true"
                                               data-aspectRatio="1.7777777778">
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                @endif
                @if($vertical_box_active)
                    <div class="col-md-4">
                        <label>Vertical Box<br>
                            <small>&nbsp;</small>
                        </label>
                        @if(!$item->url_vertical_box)
                            <img src="/images/placeholder-vertical-box.jpg" class="img-responsive img-preview-vertical-box">
                        @else
                            <img src="{{ asset($item->url_vertical_box) }}" class="img-responsive img-preview-vertical-box">
                        @endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Upload new image…
                                        <input type="file"
                                               class="img-uploader"
                                               data-label="vertical_box"
                                               data-preview=".img-preview-vertical-box"
                                               data-minWidth="208"
                                               data-minHeight="312"
                                               data-maxWidth="2048"
                                               data-maxHeight="2048"
                                               data-cropper="true"
                                               data-aspectRatio="0.6666666667">
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>


</div>

