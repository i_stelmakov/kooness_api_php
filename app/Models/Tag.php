<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function artworks(){
        return $this->belongsToMany('App\Models\Artwork', 'artworks_tags', 'tag_id', 'artwork_id')->whereRaw('status IN (1,2)');
    }

    public function artists(){
        return $this->belongsToMany('App\Models\Artist', 'artists_tags', 'tag_id', 'artist_id')->whereStatus(1);
    }

    public function galleries(){
        return $this->belongsToMany('App\Models\Gallery', 'galleries_tags', 'tag_id', 'gallery_id')->whereStatus(1);
    }

    public function news(){
        return $this->belongsToMany('App\Models\Post', 'posts_tags', 'tag_id', 'post_id')->whereType('news')->whereStatus(1);
    }

    public function magazine(){
        return $this->belongsToMany('App\Models\Post', 'posts_tags', 'tag_id', 'post_id')->whereType('magazine')->whereStatus(1);
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
