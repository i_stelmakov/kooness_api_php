@extends('layouts.admin')

@if(Request::is('admin/categories/magazine*'))
    @section('title', 'Create Magazine Category')
@else
    @section('title', 'Create News Category')
@endif
@section('page-header', 'Admin - Create Category')

@section('content')
    @include('posts.categories.form', ['action' => (Request::is('admin/categories/magazine*'))? route('admin.categories.magazine.store'):route('admin.categories.news.store') , 'method' => 'POST'])
@endsection