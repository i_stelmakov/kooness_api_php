<div class="slider-item-txt">
    <div class="default-sheet-row slider-item-row">
        <div class="default-sheet-row-cell">
            <h2><a title="{{ $item->name }}" class="dark-text" href="{{ route('galleries.single', [$item->slug]) }}">{{ $item->name }}</a></h2>
        </div>
    </div>
    <div class="default-sheet-row slider-item-row">
        <div class="default-sheet-row-cell">
            <p class="gallery-adress">{{ $item->city }}, {{ $item->address }}</p>
        </div>
    </div>
    <a title="{{ $item->name }}" class="read-more" href="{{ route('galleries.single', [$item->slug]) }}">read more</a>
</div>