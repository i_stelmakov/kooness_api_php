{{-- User - Favourite Galleries --}}

@extends('layouts.app')

@section('title')
    User - Favourite Galleries
@endsection

@section('title', 'Favourite Galleries')

@section('body-class')
    user-favourite-galleries
@endsection

@section('content')
    <section id="gallery-slider" class="default-slider-section slider-border">
        <div id="saved-galleries" class="container boxed-container">
            <div class="default-slider-header">
                <h1>Saved Galleries</h1>
            </div>
            <div class="col-container">
                @if(Auth::user()->collectionGalleries()->count())
                    @foreach(Auth::user()->collectionGalleries()->get() as $gallery)
                        @include('users.frontend.partials.gallery', ['gallery' => $gallery])
                    @endforeach
                @else
                    No favourite
                @endif
            </div>
        </div>
    </section>
@endsection