var autocomplete;
var componentForm = {
    street_number: {
        get: 'short_name',
        input: '.number'
    },
    route: {
        get: 'long_name',
        input: '.address'
    },
    locality: {
        get: 'long_name',
        input: '.city'
    },
    administrative_area_level_2: {
        get: 'short_name',
        input: '.state'
    },
    country: {
        get: 'long_name',
        input: '.country',
        get2: 'short_name',
        input2: '.country_code',
    },
    postal_code: {
        get: 'short_name',
        input: '.zip'
    }
};

function initGMaps() {
    $(function () {
        $('input.address-autocomplete').each(function () {
            $(this).initAutocomplete();
        })
    })
}

$.fn.initAutocomplete = function () {
    var element = this;
    element.on('keyup', function () {
        if($(this).val().length > 10){
            var autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById($(this)[0].id)),
                {types: ['geocode']});
            autocomplete.addListener('place_changed', function () {
                fillInAddress(element, autocomplete);
            });
        }
    });
    $(this).on('change', function () {
        var val = $(this).val();
        $(this).val('');
        $(this).val(val);
        google.maps.event.trigger(autocomplete, 'place_changed');
    })

};

function fillInAddress(element, autocomplete) {
    var place = autocomplete.getPlace();
    console.log(place);
    for (var component in componentForm) {
        $(element).parent().find(componentForm[component].input).val('')
    }

    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType].get];
            $(element).parent().find(componentForm[addressType].input).val(val);
            if(componentForm[addressType].get2 !== 'undefined'){
                var val2 = place.address_components[i][componentForm[addressType].get2];
                $(element).parent().find(componentForm[addressType].input2).val(val2);
            }
        }
    }

    $(element).blur();
}
