<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fair extends Model
{
    protected $guarded = [];

    protected $appends = ['gallery_ids', 'country', 'contact_country'];

    protected $fillable = [
        'name',
        'slug',
        'description',
        'url_picture',
        'url_cover',
        'country_id',
        'city',
        'address',
        'zip',
        'phone',
        'start_date',
        'end_date',
        'times',
        'tickets',
        'map',
        'contact_name',
        'contact_country_id',
        'contact_city',
        'contact_address',
        'contact_zip',
        'contact_email',
        'contact_website',
        'status',
        'user_id',
        'created_by',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description'
    ];

    public function galleries()
    {
        return $this->belongsToMany('App\Models\Gallery', 'fairs_galleries', 'fair_id', 'gallery_id');
    }

    public function artworks()
    {
        return $this->belongsToMany('App\Models\Artwork', 'artworks_fairs', 'fair_id', 'artwork_id');
    }

    public function getGalleryIdsAttribute()
    {
        return $this->galleries->pluck('id')->toArray();
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_id');
    }

    public function contactCountry()
    {
        return $this->hasOne('App\Models\Country', 'id', 'contact_country_id');
    }

    public function news()
    {
        return $this->belongsToMany('App\Models\Post', 'posts_fairs', 'fair_id', 'post_id');
    }

    public function getCountryAttribute()
    {
        if ($this->country()->first())
            return $this->country()->first()->name;
        else
            return "";

    }

    public function getContactCountryAttribute()
    {
        if ($this->contactCountry()->first())
            return $this->contactCountry()->first()->name;
        else
            return "";
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-square-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-wide-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
