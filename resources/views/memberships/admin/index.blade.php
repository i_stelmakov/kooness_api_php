@extends('layouts.admin')
@section('title', 'Admin | Memberships')
@section('page-header', 'Memberships')
@section('styles')
    <style>
        table:not(#table-payment-history) {
            width: 100%;
        }
        table:not(#table-payment-history) td, table:not(#table-payment-history) tr, table:not(#table-payment-history) th {
            padding: 10px;
        }
        table:not(#table-payment-history) td {
            border: 1px solid silver;
        }

    </style>
@endsection

@section('content')
    @if(Auth::user()->renewal_failed_date)
        <div class="alert alert-warning" role="alert">
            <strong>There was problems with the renewal of membership. Please proceed to the payment by {{ date('d/m/Y', strtotime(Auth::user()->renewal_failed_date . " +30 Days")) }}  otherwise your account will be deactivated.  </strong>
        </div>
    @endif
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <p>Below you can monitor and change the status of your membership</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <table>
                        <tr>
                            <th>Membership Plan</th>
                            @if(isPremiumUser() && !isTrialUser())
                                <th>Amount</th>
                            @endif
                            @if(isPremiumUser() && !isTrialUser())
                                <th>Fee</th>
                            @endif
                            <th>Activation Date</th>
                            <th>{{ Auth::user()->renewal_failed_date? 'Payment Failed On' : (Auth::user()->deactivate_premium_request_date ? 'Deactivation Date' : (isTrialUser()? 'Expiration' : 'Renewal')) }}</th>
                            <th>Payment</th>
                        </tr>
                        <tr>
                            @if(Auth::user()->renewal_failed_date)
                                <td>{{ $plan->name }}</td>
                                <td>{{ $plan->fixed_fee }}{{ $plan->getSymbolCurrency() }}</td>
                                <td>{{ $plan->success_fee }}%</td>
                                <td>{{ date('d/m/Y', strtotime($activation_date)) }}</td>
                                <td class="error">{{ date('d/m/Y', strtotime(Auth::user()->premium_exp_date)) }}</td>
                                <td>
                                    <form action="{{ route('admin.memberships.pay.now') }}" class="action-submit" role="form" method="POST" data-attachment="validate">
                                        @csrf
                                        <input type="hidden" name="payment_id" value="{{ Auth::user()->renewal_failed_payment_id }}">
                                        <button class="btn btn-primary btn-xs">PAY NOW!</button>
                                    </form>
                                </td>
                            @elseif(isPremiumUser() && !isTrialUser())
                                <td>{{ $plan->name }}</td>
                                <td>{{ $plan->fixed_fee }}{{ $plan->getSymbolCurrency() }}</td>
                                <td>{{ $plan->success_fee }}%</td>
                                <td>{{ date('d/m/Y', strtotime($activation_date)) }}</td>
                                <td class="{{ date('Y-m-d', strtotime(Auth::user()->premium_exp_date . " - 7 Days")) <= date('Y-m-d') ? 'error' : '' }}">
                                    {{ date('d/m/Y', strtotime(Auth::user()->premium_exp_date)) }}
                                </td>
                                <td>{{ $cardType }}</td>
                            @else
                                <td>Trial Version</td>
                                <td>{{ date('d/m/Y', strtotime(Auth::user()->trial_activation_date)) }}</td>
                                <td class="{{ (date('Y-m-d', strtotime(Auth::user()->trial_activation_date . " + 23 Days")) < date('Y-m-d')) ? 'error' : '' }}">
                                    {{ date('d/m/Y', strtotime(Auth::user()->trial_activation_date . " +30 Days")) }}
                                </td>
                                <td>Free</td>
                            @endif
                        </tr>
                    </table>
                    @if(isPremiumUser())
                        <br>
                        <p class="text-danger">Warning! In case of a change in the membership plan, the change will take place according to the contractual conditions</p>
                    @endif
                </div>
                <div class="box-footer">
                    @if(!Auth::user()->deactivate_premium_request_date)
                        @if(isPremiumUser())
                            <div class="pull-right">
                                <form action="{{ route('admin.memberships.deactivate') }}" class="action-submit inline delete-membership-button" role="form" method="POST" onsubmit="return alert(this)">
                                    @csrf
                                    <button class="btn btn-primary">Cancel Subscription</button>
                                </form>
                                @if(!Auth::user()->have_pending_request())
                                    <a href="{{ route('admin.memberships.plans') }}">
                                        <button class="btn btn-primary">Modify plan</button>
                                    </a>
                                @endif
                            </div>
                        @else
                            <div class="pull-right">
                                <a href="{{ route('admin.memberships.plans') }}">
                                    <button class="btn btn-primary">Buy a plan</button>
                                </a>
                            </div>
                        @endif
                    {{--@else--}}
                        {{--<div class="pull-right">--}}
                            {{--<form action="{{ route('admin.memberships.reactivate') }}" class="action-submit inline" role="form" method="POST" data-attachment="validate">--}}
                                {{--@csrf--}}
                                {{--<button class="btn btn-primary">Reactivate Subscription</button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    @endif
                </div>

            </div>
        </div>
    </div>

    @if(Auth::user()->have_pending_request())

        <div class="row">
            <section class="content-header">
                <h1>Pending Request for change plan</h1>
            </section>
            <section class="content container-fluid">
                <div class="box box-primary">
                    <div class="box-body">
                        <table>
                            <tr>
                                <th>Membership Plan</th>
                                <th>Amount</th>
                                <th>Fee</th>
                                <th>Activation Date</th>
                            </tr>
                            <tr>
                                <td>{{ $pending_request_plan->name }}</td>
                                <td>{{ $pending_request_plan->fixed_fee }}{{ $pending_request_plan->getSymbolCurrency() }}</td>
                                <td>{{ $pending_request_plan->success_fee }}%</td>
                                <td>{{ date('d/m/Y', strtotime(Auth::user()->premium_exp_date)) }}</td>
                            </tr>
                        </table>
                    </div>
                    {{--<div class="box-footer">--}}
                        {{--<div class="pull-right">--}}
                            {{--<form action="{{ route('admin.memberships.undo.request.change.plan') }}" class="action-submit inline delete-membership-button" role="form" method="POST" onsubmit="return alert_request(this)">--}}
                                {{--@csrf--}}
                                {{--<button class="btn btn-primary">Cancel Request</button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </section>
        </div>
    @endif

    @if(isPremiumUser() && !isTrialUser())
        <div class="row">
            <section class="content-header">
                <h1>Payment History</h1>
            </section>
            <section class="content container-fluid">
                <div class="box box-primary">
                    <div class="box-body">
                        <table id="table-payment-history" class="table table-striped dataTables" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Reference</th>
                                <th>Plan</th>
                                <th>Referer to (period)</th>
                                <th>Total</th>
                                <th>Payment date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    @endif

    <script>
        function alert(form) {
            swal({
                    title: 'Are you sure?',
                    text: 'Warning! in order to reactivate a membership you will have to wait until the current plan expires',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, cancel subscription!',
                    cancelButtonText: 'Cancel'
                },
                function () {
                    $(form)[0].submit();
                });
            return false;
        }

        function alert_request(form) {
            swal({
                    title: 'Are you sure?',
                    text: 'Warning! If you cancel the request at the next renewal, your plan will not be changed\n',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, cancel request!',
                    cancelButtonText: 'Cancel'
                },
                function () {
                    $(form)[0].submit();
                });
            return false;
        }

        $(document).ready(function () {
            $('#table-payment-history').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [[ 5, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.memberships.get.history.payment') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id","bSortable": false},
                    {"data": "reference","bSortable": false},
                    {"data": "plan","bSortable": false},
                    {"data": "referer_to","bSortable": false},
                    {"data": "total","bSortable": false},
                    {"data": "updated_at","bSortable": false},
                    {"data": "options","bSortable": false}
                ],
            });
        });
    </script>

@endsection