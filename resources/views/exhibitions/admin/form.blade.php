@if( roleCheck('superadmin|admin|gallerist|artist|seo') )
    <!-- Form -->
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $exhibition->id }}" data-referer="exhibition">
    {{ method_field($method) }}
    @csrf

        {{-- Section intro --}}
        <div class="col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        @if(!roleCheck('seo'))
            {{-- Associations --}}
            <div class='col-md-12 col-xs-12'>
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Associations
                    </div>
                    <div class="box-body">

                        <div class="col-md-12 col-xs-12 {{ ( roleCheck('gallerist') ) ? 'hidden' : '' }}">
                            <div class="form-group">
                                <label for="gallery_id">Gallery</label>
                                <select class="select2 form-control" name="gallery_id">
                                    @foreach($galleries as $gallery)
                                        <option value="{{ $gallery->id }}" {{ $gallery->id == $exhibition->gallery_id ? 'selected="selected"' : '' }}>{{ $gallery->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="artists">Search artist</label>
                                <div class="input-group">
                                    <select id="artists" class="select2 form-control" name="artists[]" multiple>
                                        @foreach($artists as $artist)
                                            <option value="{{ $artist->id }}" {{ (in_array($artist->id, $exhibition->artist_ids))? 'selected="selected"' : '' }}>{{ $artist->first_name }} {{  $artist->last_name }}</option>
                                        @endforeach
                                    </select> <span class="input-group-btn">
                                        <button class="btn btn-success add-new-artist" style="padding: 5px 12px;"><i class="fa fa-plus"></i> Add</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Exhibition Information --}}
        <div class='col-md-12 col-xs-12'>

            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Exhibition Information
                </div>
                <div class="box-body">
                    <div class="col-md-12  col-xs-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input 
                                id="generated_url" 
                                class="form-control" 
                                type="text" 
                                data-prefix="{{ url('') }}/exhibitions/"
                                value="{{ ($exhibition->{'slug'})? url('') . '/exhibitions/' . $exhibition->{'slug'} : '' }}"
                                disabled
                            >
                        </div>
                    </div>
                    <!-- Name -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Name (*)</label>
                            <input 
                                id="name" 
                                class="form-control" 
                                type="text" 
                                placeholder="Name" 
                                name="name" 
                                data-slug="#slug" 
                                value="{{ old('name', $exhibition->{'name'}) }}" 
                                {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                            >
                        </div>
                    </div>
                    <!-- Slug -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="slug">Slug (*)</label>
                            <input 
                                id="slug" 
                                class="form-control" 
                                type="text" 
                                placeholder="Slug" 
                                name="slug" 
                                value="{{ old('slug', $exhibition->{'slug'}) }}" data-url="#generated_url" 
                                {{ ( roleCheck('gallerist|seo') ) ? 'readonly' : 'required' }}
                            >
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        <!-- Status -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="0" {{ (!$exhibition->{'status'})  ? 'selected="selected"' : ''  }}>
                                        Hidden
                                    </option>
                                    <option value="1" {{ ($exhibition->{'status'})  ? 'selected="selected"' : ''  }}>
                                        Visible
                                    </option>
                                </select>
                            </div>
                        </div>
                        <!-- Description -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea id="description" rows="5" class="form-control ck-editor" type="text" placeholder="Description" name="description">
                                    {{ old('description', $exhibition->{'description'}) }}
                                </textarea>
                            </div>
                        </div>
                        <!-- Address -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">Address (*)</label>
                                <input id="address" class="form-control" type="text" placeholder="Address" name="address" value="{{ old('address', $exhibition->{'address'}) }}" required>
                            </div>
                        </div>
                        <!-- Country -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country_id">Country</label>
                                <select class="select2 form-control" name="country_id">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $country->id  == $exhibition->country_id ? 'selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- City -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">City (*)</label>
                                <input id="city" class="form-control" type="text" placeholder="City" name="city" value="{{ old('city', $exhibition->{'city'}) }}" required>
                            </div>
                        </div>
                        <!-- Zip -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zip">Zip code (*)</label>
                                <input id="zip" class="form-control" type="text" placeholder="Zip Code" name="zip" value="{{ old('zip', $exhibition->{'zip'}) }}" required>
                            </div>
                        </div>
                        <!-- Start Date -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="start_date">Start Date (*)</label>
                                <input id="start_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="start_date" value="{{ old('start_date', $exhibition->{'start_date'}) }}" required>
                            </div>
                        </div>
                        <!-- End Date -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="end_date">End Date (*)</label>
                                <input id="end_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="end_date" value="{{ old('end_date', $exhibition->{'end_date'}) }}" required>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        @if(!roleCheck('seo'))
            {{-- Main Images --}}
            @include('partials.image-default', ['item' => $exhibition])

            @if( !roleCheck('gallerist|artist') )
                {{-- Thumbnail Images --}}
                @include('partials.images', ['item' => $exhibition, 'square_box_active' => false, 'wide_box_active' => true,'vertical_box_active' => false ])
            @endif
        @endif

        @if(!roleCheck('gallerist'))
            {{-- SEO Management --}}
            <div class='col-md-12 col-xs-12'>
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        SEO Management
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_title">SEO - title</label>
                                    <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $exhibition->{'meta_title'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_keywords">SEO - keywords</label>
                                    <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $exhibition->{'meta_keywords'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="h1">SEO - H1</label>
                                    <input class="form-control" name="h1" type="text" placeholder="SEO - H1" id="h1" value="{{ $exhibition->{'h1'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="canonical">SEO - Canonical Url</label>
                                    <input class="form-control" name="canonical" type="text" placeholder="SEO - Canonical Url" id="canonical" value="{{ $exhibition->{'canonical'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_description">SEO - Description</label>
                                    <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $exhibition->{'meta_description'} }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Footer & Submit --}}
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </form>
@endif

@include('exhibitions.admin.partials.new-artist-modal')