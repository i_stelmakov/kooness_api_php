<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| ☢️ Test Zone
|--------------------------------------------------------------------------
*/

Route::group( ['middleware' => ['permission:admin-level']], function() {
    Route::get('/test/isotope', 'Test\FrontendController@isotope')->name('test.isotope');
    Route::get('/test/isotope-sorting', 'Test\FrontendController@isotopeSorting')->name('test.isotope.sorting');
    Route::get('/test/dhl', 'Test\FrontendController@dhl')->name('test.dhl');
    Route::get('/test/grid', 'Test\FrontendController@grid')->name('test.grid');
    
    Route::get('/test/api', 'Test\FrontendController@api')->name('test.api');
    Route::post('/test/api', 'Test\FrontendController@api')->name('test.api');
    Route::post('/test/api2', 'Test\FrontendController@api2')->name('test.api2');
    
    Route::get('/test/mail/{type}/{mail_kind}/{id}', 'Test\FrontendController@mail');
    Route::get('/test/allmail/', 'Test\FrontendController@allmail')->name('all.mail');
    
    Route::get('/test/search', 'Test\FrontendController@search')->name('test.search');
    
});


/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------
*/

// Home
Route::get('/', 'Home\FrontendController@index')->name('home');
Route::get('/home', 'Home\FrontendController@index')->name('home');

// Artworks
//Route::get('/artworks/view/intro', 'Artworks\FrontendController@intro')->name('artworks.intro'); // Artworks Intro
Route::get('/artworks', 'Artworks\FrontendController@archive')->name('artworks.archive.all'); // Artworks Archive All
Route::get('/artworks/{slug_name}', 'Artworks\FrontendController@single')->name('artworks.single'); // Artworks Single
Route::get('/artworks/{slug_medium}/{slug_category}', 'Artworks\FrontendController@archive')->name('artworks.categories.medium'); // Artworks Single
Route::post('/artworks/get/filtered/items', 'Artworks\FrontendController@getFilteredItems')->name('artworks.get.filtered.items'); // Artworks filter

// Galleries
Route::get('/galleries', 'Galleries\FrontendController@intro')->name('galleries.intro'); // Artworks Intro
Route::get('/galleries/archive', 'Galleries\FrontendController@archive')->name('galleries.archive.all'); // Artworks Archive All
Route::post('/galleries/load/more', 'Galleries\FrontendController@loadMore')->name('galleries.load.more'); // Artworks Archive All
Route::get('/galleries/{slug_name}', 'Galleries\FrontendController@single')->name('galleries.single'); // Artworks Single
Route::post('/galleries/search/by', 'Galleries\FrontendController@searchBy')->name('galleries.search.by'); // Artworks Archive All
Route::post('/galleries/request/subscription', 'Galleries\FrontendController@requestSubscription')->name('galleries.request.subscription');
// Artists
Route::get('/artists', 'Artists\FrontendController@intro')->name('artists.intro'); // Artworks Intro
Route::get('/artists/archive', 'Artists\FrontendController@archive')->name('artists.archive.all'); // Artworks Archive All
Route::post('/artists/load/more', 'Artists\FrontendController@loadMore')->name('artists.load.more'); // Artworks Archive All
Route::get('/artists/{slug_name}', 'Artists\FrontendController@single')->name('artists.single'); // Artworks Single
Route::post('/artists/search/by', 'Artists\FrontendController@searchBy')->name('artists.search.by'); // Artworks Archive All

// Fairs
Route::get('/fairs', 'Fairs\FrontendController@intro')->name('fairs.intro'); // Fairs Intro
Route::get('/fairs/archive', 'Fairs\FrontendController@archive')->name('fairs.archive.all'); // Fairs Archive All
Route::get('/fairs/{slug_name}', 'Fairs\FrontendController@single')->name('fairs.single'); // Fairs Single

// Posts Magazine / News
Route::get('/posts/{section}', 'Posts\FrontendController@archive')->name('posts.archive.all'); // Posts Archive All
Route::get('/posts/{section}/{slug_name}', 'Posts\FrontendController@single')->name('posts.single'); // Posts Single
Route::post('/posts/{section}/load/more', 'Posts\FrontendController@loadMore')->name('posts.load.more'); // Posts Single

// Static Pages
Route::get('/pages/{slug_name}', 'Pages\FrontendController@pages')->name('pages.intro'); // Users profile

// Exhibitions
Route::get('/exhibitions', 'Exhibitions\FrontendController@archive')->name('exhibitions.archive'); // Archive
Route::get('/exhibitions/{slug_name}', 'Exhibitions\FrontendController@single')->name('exhibitions.single'); // Single
Route::post('/exhibitions/load/more', 'Exhibitions\FrontendController@loadMore')->name('exhibitions.load.more'); // Posts Single

// Tags
Route::get('/tags/{slug_name}/{type?}', 'Tags\FrontendController@single')->name('tags.single'); // Tags Single
Route::post('/tags/{section}/load/more', 'Tags\FrontendController@loadMore')->name('tags.load.more'); // Tags Single

Route::post('/search', 'FrontendController@search')->name('search'); // Posts Single
Route::get('/get/currency/{currency}', 'FrontendController@getCurrency')->name('get.currency'); // Posts Single
Route::post('/apply/promo/code', 'FrontendController@applyPromoCode')->name('apply.promo.code');
Route::post('/remove/promo/code', 'FrontendController@removePromoCode')->name('remove.promo.code');

// Access User
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login.get');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('/activate/{token}', 'Auth\RegisterController@verifyUser')->name('verify_user');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/auth/{provider}', 'Auth\SocialController@redirectToProvider')->name('auth.social');
Route::get('/auth/{provider}/callback', 'Auth\SocialController@handleProviderCallback')->name('auth.social.callback');

// User Routes
//Route::group(['middleware' => ['permission:user-level']], function () {
//    Route::get('/user/page', 'Users\FrontendController@profile')->name('user.profile');
//    Route::post('/user/page', 'Users\FrontendController@profile')->name('user.profile');
//});

Route::post('/add/to/cart', 'Orders\FrontendController@addToCart')->name('orders.add.to.cart'); // Add to cart
Route::post('/remove/from/cart', 'Orders\FrontendController@removeFromCart')->name('orders.remove.from.cart'); // Remove from cart
Route::get('/cart', 'Orders\FrontendController@cart')->name('orders.cart'); // Add to cart
Route::get('/paypal', 'Orders\FrontendController@payPalReturn')->name('orders.paypal.callback'); // Thank you page

Route::post('/subscribe/to/newsletter', 'FrontendController@subscribeToNewsletter')->name('subscribe.to.newsletter');
Route::get('/subscription/newsletter/thank-you', 'FrontendController@subscriptionNewsletterThankYou')->name('subscription.newsletter.thank.you');

Route::post('/send/contact/message', 'FrontendController@sendContactMessage')->name('send.contact.message');
Route::get('/pay/order/{token}', 'Orders\FrontendController@payOrder')->name('orders.pay.order'); // Checkout


Route::get('/google/shopping/feed.xml', 'FrontendController@googleFeed')->name('google.feed'); // Checkout

Route::group(['middleware' => ['permission:user-level']], function () {
    //Orders
    Route::post('/get/shipping/rate', 'Orders\FrontendController@getShippingRate')->name('orders.get.shipping.rate');
    Route::get('/checkout', 'Orders\FrontendController@viewCheckout')->name('orders.view.checkout'); // Checkout
    Route::post('/checkout', 'Orders\FrontendController@checkout')->name('orders.checkout'); // Checkout
    Route::post('/checkout/pay', 'Orders\FrontendController@checkoutPay')->name('orders.checkout.pay'); // Checkout
    Route::get('/thank-you', 'Orders\FrontendController@thankYouPage')->name('orders.thank.you'); // Thank you page
    Route::get('/order/pending/thank-you', 'Orders\FrontendController@thankYouPageWithoutPayment')->name('orders.thank.you.without.payment'); // Thank you page
    Route::get('/payment/error', 'Orders\FrontendController@paymentError')->name('orders.payment.error'); // Thank you page

    Route::post('/user/ajax/save/address', 'Users\FrontendController@saveAddress')->name('users.save.address');
    Route::post('/user/ajax/save/payment', 'Users\FrontendController@savePaymentMethod')->name('users.save.payment');
    Route::post('/user/ajax/delete/address', 'Users\FrontendController@deleteAddress')->name('users.delete.address');
    Route::post('/user/ajax/delete/payment', 'Users\FrontendController@deletePaymentMethod')->name('users.delete.payment');
    Route::post('/user/ajax/promo/code', 'Users\FrontendController@applyPromoCode')->name('users.apply.promo.code');
    Route::post('/user/ajax/promo/code/remove', 'Users\FrontendController@removePromoCode')->name('users.remove.promo.code');
    Route::post('/user/ajax/add/to/collection', 'Users\FrontendController@addToCollection')->name('users.add.to.collection');
    Route::post('/user/ajax/remove/from/collection', 'Users\FrontendController@removeFromCollection')->name('users.remove.from.collection');
    Route::post('/user/ajax/create/collection', 'Users\FrontendController@createCollection')->name('users.create.collection');
    Route::post('/user/ajax/remove/collection', 'Users\FrontendController@removeCollection')->name('users.remove.collection');
    Route::post('/user/ajax/get/collection', 'Users\FrontendController@getCollection')->name('users.get.collection');
    Route::post('/user/ajax/update/collection', 'Users\FrontendController@updateCollection')->name('users.update.collection');
    Route::post('/user/ajax/update/profile', 'Users\FrontendController@updateProfile')->name('users.update.profile');
    Route::post('/user/ajax/make/offer', 'Users\FrontendController@makeAnOffer')->name('users.make.an.offer');
    Route::post('/user/ajax/info/available', 'Users\FrontendController@requestInfoOnAvailableInFair')->name('users.info.on.available');

    // Users Dashboard
    Route::get('/users/profile', 'Users\FrontendController@profile')->name('users.profile'); // Users profile
    Route::get('/users/collections', 'Users\FrontendController@collection')->name('users.collections'); // Users profile
    Route::get('/users/favourite/artworks', 'Users\FrontendController@favouriteArtworks')->name('users.favourite.artworks'); // Users profile
    Route::get('/users/favourite/artists', 'Users\FrontendController@favouriteArtists')->name('users.favourite.artists'); // Users profile
    Route::get('/users/favourite/galleries', 'Users\FrontendController@favouriteGalleries')->name('users.favourite.galleries'); // Users profile

});


/*
|--------------------------------------------------------------------------
| Backend
|--------------------------------------------------------------------------
*/

// Common for All
Route::group(['middleware' => ['permission:fair-level|gallerist-level|artist-level|editor-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Dashboard Generics
    Route::get('/', "AdminController@index")->name('dashboard'); // dashboard iniziale
    Route::post('/users/check/email', ['laroute' => true, 'uses' => "AdminController@userCheckEmail"])->name('user.check.email'); // check email
    Route::post('/users/check/username', ['laroute' => true, 'uses' => "AdminController@userCheckUsername"])->name('user.check.username'); // check username
    Route::post('/ajax/check/slug/availability', ['laroute' => true, 'uses' => "AdminController@checkSlugAvailability"])->name('check.slug.availability'); // check slug
    Route::post('/ajax/upload/images', "AdminController@uploadImages")->name('ajax.upload.images'); // image uploader generico 

});

// Only for SuperAdmin
Route::group(['middleware' => ['permission:superadmin-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Order
    Route::get('orders',  'Orders\AdminController@index')->name('orders.index');
    Route::get('orders/not/completed',  'Orders\AdminController@notCompleted')->name('orders.not.completed');
    Route::get('orders/view/{order}',  'Orders\AdminController@view')->name('orders.view');
    Route::post('orders/update/status/{order}',  'Orders\AdminController@updateOrderStatus')->name('orders.update.status');
    Route::post('/ajax/orders/retrieve/{flag}', 'Orders\AdminController@retrieve')->name('orders.retrieve');
    Route::any('orders/current/invoices/number', 'Orders\AdminController@currentInvoicesNumber')->name('orders.current.invoices.number');
    Route::get('orders/{order_id}/resend/current/mail', 'Orders\AdminController@resendMail')->name('orders.resend.current.mail');
    Route::post('orders/{order_id}/update/shipping', 'Orders\AdminController@updateShippingOrder')->name('orders.update.shipping');

    // Offers
    Route::get('/offers', "Offers\AdminController@index")->name('offers.index');
    Route::get('/offers/{offer}/change/status/{status}', "Offers\AdminController@changeStatus")->name('offers.change.status');
    Route::post('/ajax/offers/retrieve', 'Offers\AdminController@retrieve')->name('offers.retrieve');

    // Memberships (for Admin)
    Route::get('memberships/list', 'Galleries\AdminController@membershipsList')->name('memberships.list');
    Route::get('memberships/payments', 'Galleries\AdminController@membershipsPayments')->name('memberships.payments');
    Route::post('/ajax/memberships/list/retrieve', 'Galleries\AdminController@membershipsRetrieve')->name('memberships.list.retrieve');
    Route::post('/ajax/memberships/payments/retrieve', 'Galleries\AdminController@membershipsPaymentRetrieve')->name('memberships.payment.retrieve');

});

// Only for Admin
Route::group(['middleware' => ['permission:admin-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Pages
    Route::resource('pages', 'Pages\AdminController');
    Route::post('/ajax/pages/retrieve', 'Pages\AdminController@retrieve')->name('pages.retrieve');

    // Promo Codes
    Route::resource('promo-codes', 'PromoCodes\AdminController');
    Route::post('/ajax/promo/codes/retrieve', 'PromoCodes\AdminController@retrieve')->name('promo-codes.retrieve');

    // Homepage Builder
    Route::get('/homepage', "Home\AdminController@editLayout")->name('homepage.layout.edit');
    Route::post('/homepage/layout', "Home\AdminController@saveLayout")->name('homepage.layout.save');
    Route::post('/homepage/section', "Home\AdminController@saveSections")->name('homepage.sections.save');
    Route::post('/homepage/links', "Home\AdminController@saveLinks")->name('homepage.links.save');
    Route::post('/homepage/section/retrieve/data', ['laroute' => true, 'uses' => "Home\AdminController@retrieveDataSection"])->name('homepage.section.retrieve.data');
    Route::post('/homepage/search/items', ['laroute' => true, 'uses' => "Home\AdminController@searchItems"])->name('homepage.layout.search.items');

    // User
    Route::resource('users', 'Users\AdminController');
    Route::post('/ajax/users/retrieve', 'Users\AdminController@retrieve')->name('users.retrieve');

    // Global System Configurations
    Route::any('/configs/free/shipping', 'AdminController@configFreeShipping')->name('configs.free.shipping');
    Route::any('/configs/gallerist/guide', 'AdminController@galleristGuide')->name('configs.gallerist.guide');

    // Generate and download Orders PDF
    Route::any('orders/current/invoices/pdf/{order}', 'Orders\AdminController@currentInvoicesPDF')->name('orders.current.invoices.pdf');

});

// Only for Admin / Seo
Route::group(['middleware' => ['permission:admin-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Dashboard
    Route::get('/{section}/main/page', 'AdminController@mainPage')->name('main.page'); // controller delle intro
    Route::post('/{section}/main/page', 'AdminController@saveMainPage')->name('main.page.save'); // controller delle intro

    // Tag & Taxonomies
    Route::resource('tags', 'Tags\AdminController');
    Route::post('/ajax/tags/retrieve', 'Tags\AdminController@retrieve')->name('tags.retrieve');
    Route::post('/tags/check/if/exist', 'Tags\AdminController@checkTagIfExist')->name('tags.check.if.exist');

    // Seo Data
    Route::resource('seo-data', 'SeoData\AdminController');
    Route::post('/ajax/seo-data/retrieve', 'SeoData\AdminController@retrieve')->name('seo-data.retrieve');

    // 301 Redirects
    Route::resource('redirects', 'Redirects\AdminController');
    Route::post('/ajax/redirects/retrieve', 'Redirects\AdminController@retrieve')->name('redirects.retrieve');

    // Bulk Sections
    Route::get('bulk/upload/redirects', 'Redirects\AdminController@bulkUpload')->name('redirects.bulk.upload');
    Route::get('bulk/upload/pages', 'Bulk\AdminController@bulkUpload')->name('pages.bulk.upload');
    Route::get('bulk/download/googleshop', 'Bulk\AdminController@bulkGoogleShop')->name('googleshop.bulk.download');

    // Bulk generics XLS calls
    Route::post('bulk/upload/xls', 'Bulk\AdminController@uploadFile')->name('bulk.upload.upload.xls');
    Route::post('bulk/uploads/verify/xls/{xls_name}/type/{type}', 'Bulk\AdminController@verifyXls')->name('bulk.upload.verify.xls');
    Route::post('bulk/uploads/process/xls/{xls_name}/type/{type}', 'Bulk\AdminController@processXls')->name('bulk.upload.process.xls');
    Route::get('bulk/uploads/download/xls/{type}', 'Bulk\AdminController@downloadXls')->name('bulk.download.process.xls');
    Route::post('bulk/uploads/process/xls/{xls_name}/status', 'Bulk\AdminController@getStateProcessXls')->name('bulk.upload.process.xls.status');

    // Galleries
    Route::get('/galleries', 'Galleries\AdminController@index')->name('galleries.index');
    Route::get('/galleries/create', 'Galleries\AdminController@create')->name('galleries.create');
    Route::post('/galleries', 'Galleries\AdminController@store')->name('galleries.store');
    Route::delete('/galleries/{id}', 'Galleries\AdminController@destroy')->name('galleries.destroy');
    Route::post('/ajax/galleries/retrieve', 'Galleries\AdminController@retrieve')->name('galleries.retrieve');

    // Fairs
    Route::get('/fairs', 'Fairs\AdminController@index')->name('fairs.index');
    Route::get('/fairs/create', 'Fairs\AdminController@create')->name('fairs.create');
    Route::post('/fairs', 'Fairs\AdminController@store')->name('fairs.store');
    Route::delete('/fairs/{id}', 'Fairs\AdminController@destroy')->name('fairs.destroy');
    Route::post('/ajax/fairs/retrieve', 'Fairs\AdminController@retrieve')->name('fairs.retrieve');
    Route::any('/ajax/fairs/{id}/pdf/floorplant', 'Fairs\AdminController@floorplantManagement')->name('fairs.floorplant');

    // Categories
    Route::group(['prefix' => "categories", 'as' => 'categories.'], function () {
        Route::resource('magazine', 'Posts\CategoriesController');
        Route::post('/ajax/magazine/retrieve', 'Posts\CategoriesController@retrieve')->name('magazine.retrieve');

        Route::resource('news', 'Posts\CategoriesController');
        Route::post('/ajax/news/retrieve', 'Posts\CategoriesController@retrieve')->name('news.retrieve');

        Route::resource('galleries', 'Galleries\CategoriesController');
        Route::post('/ajax/galleries/retrieve', 'Galleries\CategoriesController@retrieve')->name('galleries.retrieve');

        Route::resource('artists', 'Artists\CategoriesController');
        Route::post('/ajax/artists/retrieve', 'Artists\CategoriesController@retrieve')->name('artists.retrieve');

        Route::resource('artworks', 'Artworks\CategoriesController');
        Route::get('artworks/create/{is_main?}', 'Artworks\CategoriesController@create')->name('artworks.create');
        Route::post('/ajax/artworks/retrieve/{is_main?}', 'Artworks\CategoriesController@retrieve')->name('artworks.retrieve');
    });

    // Medium (Category variation for Artwork)
    Route::group(['prefix' => "medium", 'as' => 'medium.'], function () {
        Route::resource('artworks', 'Artworks\MediumController');
        Route::post('/ajax/artworks/retrieve', 'Artworks\MediumController@retrieve')->name('artworks.retrieve');
    });

    // Medium / Categories relations
    Route::group(['prefix' => "artworks/medium", 'as' => 'artworks.medium.'], function () {
        Route::resource('categories', 'Artworks\CategoriesMediumController');
        Route::post('categories/ajax/retrieve', 'Artworks\CategoriesMediumController@retrieve')->name('categories.retrieve');
    });
});

// Common for Admin / Gallerist / Artist / Seo
Route::group(['middleware' => ['permission:admin-level|gallerist-level|artist-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Artworks
    Route::resource('artworks', 'Artworks\AdminController');
    Route::post('/ajax/artworks/retrieve', 'Artworks\AdminController@retrieve')->name('artworks.retrieve');

    //Artists
    Route::get('/artists/{id}/edit', 'Artists\AdminController@edit')->name('artists.edit');
    Route::patch('/artists/{id}', 'Artists\AdminController@update')->name('artists.update');

});

// Common for Admin / Gallerist / Artist / Fair / Seo
Route::group(['middleware' => ['permission:admin-level|gallerist-level|fair-level|artist-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // News
    Route::resource('news', 'Posts\AdminController');
    Route::post('/ajax/news/retrieve', 'Posts\AdminController@retrieve')->name('news.retrieve');

});

// Common for Admin / Gallerist / Seo
Route::group(['middleware' => ['permission:admin-level|gallerist-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Galleries
    Route::get('/galleries/{id}/edit', 'Galleries\AdminController@edit')->name('galleries.edit');
    Route::patch('/galleries/{id}', 'Galleries\AdminController@update')->name('galleries.update');

    // Artists
    Route::get('/artists', 'Artists\AdminController@index')->name('artists.index');
    Route::get('/artists/create', 'Artists\AdminController@create')->name('artists.create');
    Route::post('/artists', 'Artists\AdminController@store')->name('artists.store');
    Route::delete('/artists/{id}', 'Artists\AdminController@destroy')->name('artists.destroy');
    Route::post('/ajax/artists/retrieve', 'Artists\AdminController@retrieve')->name('artists.retrieve');

    // Exhibitions
    Route::resource('exhibitions', 'Exhibitions\AdminController');
    Route::post('/ajax/exhibitions/retrieve', 'Exhibitions\AdminController@retrieve')->name('exhibitions.retrieve');

});

// Common for Admin / Gallerist
Route::group(['middleware' => ['permission:admin-level|gallerist-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Notifications
    Route::get('notifications', 'Notifications\AdminController@index')->name('notifications.index');
    Route::get('notifications/mark/all/as/read', 'Notifications\AdminController@markAllAsRead')->name('notifications.mark.all.as.read');
    Route::post('/ajax/notifications/retrieve', 'Notifications\AdminController@retrieve')->name('notifications.retrieve');

    // Memberships for Gallerist
    Route::get('memberships', 'Memberships\AdminController@index')->name('memberships.index');
    Route::get('memberships/plans', 'Memberships\AdminController@plans')->name('memberships.plans');
    Route::any('memberships/billing', 'Memberships\AdminController@billing')->name('memberships.billing');
    Route::any('memberships/card', 'Memberships\AdminController@card')->name('memberships.card');
    Route::any('memberships/payment/action', 'Memberships\AdminController@paymentAction')->name('memberships.payment.action');
    Route::any('memberships/edit', 'Memberships\AdminController@edit')->name('memberships.edit');
    Route::post('memberships/deactivate', 'Memberships\AdminController@deactivate')->name('memberships.deactivate');
    Route::post('memberships/reactivate', 'Memberships\AdminController@reactivate')->name('memberships.reactivate');
    Route::post('memberships/pay/now', 'Memberships\AdminController@payNow')->name('memberships.pay.now');
    Route::any('memberships/request/change/plan', 'Memberships\AdminController@requestChangePlan')->name('memberships.request.change.plan');
    Route::post('memberships/undo/request/change/plan', 'Memberships\AdminController@undoRequestChangePlan')->name('memberships.undo.request.change.plan');
    Route::post('memberships/get/history/payment', 'Memberships\AdminController@getHistoryPayment')->name('memberships.get.history.payment');
    Route::get('memberships/view/invoice/{serial}', 'Memberships\AdminController@viewInvoice')->name('memberships.view.invoice');

    // Associate artist to itself called from gallerist
    Route::post('/ajax/associate/artist/to/me', 'Artists\AdminController@associateArtistToGallery')->name('associate.artist.to.me');

    // Download User Guide PDF
    Route::get('download/guide', 'Galleries\AdminController@downloadGuide')->name('download.guide.pdf');

});

// Common for Admin / Fairs / Seo
Route::group(['middleware' => ['permission:admin-level|fair-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Fairs
    Route::get('/fairs/{id}/edit', 'Fairs\AdminController@edit')->name('fairs.edit');
    Route::patch('/fairs/{id}', 'Fairs\AdminController@update')->name('fairs.update');

});

// Common for Admin / Editor / Seo
Route::group(['middleware' => ['permission:admin-level|editor-level|seo-level'], 'prefix' => "admin", 'as' => 'admin.'], function () {

    // Magazines
    Route::resource('magazine', 'Posts\AdminController');
    Route::post('/ajax/magazine/retrieve', 'Posts\AdminController@retrieve')->name('magazine.retrieve');

});

