@if(roleCheck('superadmin|admin|fair|seo'))

    <!-- Form -->
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $fair->id }}" data-referer="fair">
    {{ method_field($method) }}
    @csrf

    {{-- Section intro --}}
    <div class="col-sm-12">
        <p>The fields marked with asterisk (*) are required </p>
    </div>

    @if(!roleCheck('seo'))
            {{-- Associations --}}
            <div class='col-md-12 col-xs-12'>
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Associations
                    </div>
                    <div class="box-body">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="galleries">Galleries</label>
                                <select class="select2 form-control" name="galleries[]" multiple="multiple">
                                    @foreach($galleries as $gallery)
                                        <option value="{{ $gallery->id }}" {{ (in_array($gallery->id, $fair->gallery_ids))? 'selected="selected"' : '' }}>{{ $gallery->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Fair Information --}}
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Fair Information
                </div>
                <div class="box-body">
                    <div class="row">
                        <!-- Url generated -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="first_name">Url generated</label>
                                <input 
                                    id="generated_url" 
                                    class="form-control" 
                                    type="text" 
                                    data-prefix="{{ url('') }}/fairs/" 
                                    value="{{ ($fair->{'slug'})? url('') . '/fairs/' . $fair->{'slug'} : '' }}" 
                                    disabled
                                    {{ ( roleCheck('seo') ) ? 'disabled' : '' }}
                                >
                            </div>
                        </div>
                        <!-- Name -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">Name (*)</label>
                                <input 
                                    id="name" 
                                    class="form-control" 
                                    type="text" 
                                    placeholder="Name" 
                                    name="name" 
                                    data-slug="#slug" 
                                    value="{{ old('name', $fair->{'name'}) }}" 
                                    {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                                >
                            </div>
                        </div>
                        <!-- Slug -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="slug">Slug (*)</label>
                                <input 
                                    id="slug" 
                                    class="form-control" 
                                    type="text" 
                                    placeholder="Slug" 
                                    name="slug" 
                                    value="{{ old('slug', $fair->{'slug'}) }}" data-url="#generated_url" 
                                    {{ ( roleCheck('seo') ) ? 'readonly' : 'required' }}
                                >
                            </div>
                        </div>
                        @if(!roleCheck('seo'))
                            <!-- Status -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="0" {{ (!$fair->{'status'})  ? 'selected="selected"' : ''  }}>
                                            Hidden
                                        </option>
                                        <option value="1" {{ ($fair->{'status'})  ? 'selected="selected"' : ''  }}>
                                            Visible
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <!-- Description -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" rows="5" class="form-control ck-editor" type="text" placeholder="Description" name="description">
                                    {{ old('description', $fair->{'description'}) }}
                                </textarea>
                                </div>
                            </div>
                            <!-- Address -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="address">Address (*)</label>
                                    <input id="address" class="form-control" type="text" placeholder="Address" name="address" value="{{ old('address', $fair->{'address'}) }}" required>
                                </div>
                            </div>
                            <!-- Country -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country_id">Country</label>
                                    <select class="select2 form-control" name="country_id">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ $country->id  == $fair->country_id ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- City -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="city">City (*)</label>
                                    <input id="city" class="form-control" type="text" placeholder="City" name="city" value="{{ old('city', $fair->{'city'}) }}" required>
                                </div>
                            </div>
                            <!-- Zip -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="zip">Zip code (*)</label>
                                    <input id="zip" class="form-control" type="text" placeholder="Zip Code" name="zip" value="{{ old('zip', $fair->{'zip'}) }}" required>
                                </div>
                            </div>
                            <!-- Start Date -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_date">Start Date (*)</label>
                                    <input id="start_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="start_date" value="{{ old('start_date', $fair->{'start_date'}) }}" required>
                                </div>
                            </div>
                            <!-- End Date -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_date">End Date (*)</label>
                                    <input id="end_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="end_date" value="{{ old('end_date', $fair->{'end_date'}) }}" required>
                                </div>
                            </div>
                            <!-- Empty for alignment -->
                            <div class="col-md-6 col-md-offset-6">
                            </div>
                            <!-- Tickets -->
                            <div class="col-md-6">
                                <div class="box box-default box-solid">
                                    <div class="box-header with-border">
                                        Tickets
                                    </div>
                                    <div class="box-body">
                                        <div class="form-group">
                                        <textarea id="tickets" rows="5" class="form-control ck-editor" type="text" placeholder="Tickets" name="tickets">
                                            {{ old('tickets', $fair->{'tickets'}) }}
                                        </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Contact data -->
                            <div class="col-md-6">
                                <div class="box box-default box-solid" style="padding-bottom: 23px;">
                                    <div class="box-header with-border">
                                        Contact
                                    </div>
                                    <div class="box-body row">
                                        <!-- Contact name -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="contact_name">Contact name</label>
                                                <input id="contact_name" class="form-control" type="text" placeholder="Contact name" name="contact_name" value="{{ old('contact_name', $fair->{'contact_name'}) }}">
                                            </div>
                                        </div>
                                        <!-- Contact city -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_city">Contact city</label>
                                                <input id="contact_city" class="form-control" type="text" placeholder="Contact city" name="contact_city" value="{{ old('contact_city', $fair->{'contact_city'}) }}">
                                            </div>
                                        </div>
                                        <!-- Contact country id -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_country_id">Contact country</label>
                                                <select class="select2 form-control" name="contact_country_id">
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->id }}" {{ $country->id  == $fair->contact_country_id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Contact address -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_address">Contact address</label>
                                                <input id="contact_address" class="form-control" type="text" placeholder="Contact address" name="contact_address" value="{{ old('contact_address', $fair->{'contact_address'}) }}">
                                            </div>
                                        </div>
                                        <!-- Contact zip -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_zip">Contact zip</label>
                                                <input id="contact_zip" class="form-control" type="text" placeholder="Contact zip" name="contact_zip" value="{{ old('contact_zip', $fair->{'contact_zip'}) }}">
                                            </div>
                                        </div>
                                        <!-- Contact email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_email">Contact email</label>
                                                <input id="contact_email" class="form-control" type="text" placeholder="Contact email" name="contact_email" value="{{ old('contact_email', $fair->{'contact_email'}) }}">
                                            </div>
                                        </div>
                                        <!-- Contact website -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contact_website">Contact website</label>
                                                <input id="contact_website" class="form-control" type="text" placeholder="Contact website" name="contact_website" value="{{ old('contact_website', $fair->{'contact_website'}) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Opening time -->
                            <div class="col-md-12">
                                <div class="box box-default box-solid" style="padding-bottom: 23px;">
                                    <div class="box-header with-border">
                                        Opening time
                                    </div>
                                    <div class="box-body widget-time-repeater">
                                        <ul data-repeater-list="widget-time" class="widget-time-repeater-container">
                                            @if($fair->times && count($fair->times))
                                                @foreach($fair->times as $time)
                                                    @include('fairs.admin.partials.widget-time', ['time' => $time])
                                                @endforeach
                                            @else
                                                @include('fairs.admin.partials.widget-time', ['time' => null])
                                            @endif
                                        </ul>
                                        <input data-repeater-create type="button" value="Add"/>
                                    </div>
                                </div>
                            </div>
                            <!-- Map -->
                            <div class="col-md-12">
                                <div class="box box-default box-solid" style="padding-bottom: 23px;">
                                    <div class="box-header with-border">
                                        Map
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-10">
                                        <textarea id="map" rows="3" class="form-control" type="text" placeholder="Iframe map" name="map">
                                            {{ old('map', $fair->{'map'}) }}
                                        </textarea>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <button class="btn btn-default render-map">Render</button>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12" style="height: 143px">
                                            <div class="render-container">
                                                @if($fair->{'map'})
                                                    {!! $fair->{'map'} !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>

        @if(!roleCheck('seo'))

            {{-- Fair Director Information --}}
            <div class='col-md-12 col-xs-12'>
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Fair Director Information
                    </div>
                    <div class="box-body">

                        {{-- Se utente è già stato attivato almeno una volta non mostrare più opzione di attivazione--}}
                        @if(!$fair->user_id)

                            {{--Create Credential--}}
                            <div class="col-md-12">
                                <label for="created_user">Create user credential</label>
                                <div class="form-group">
                                    <input class="iCheck-input form-check-input" type="radio" name="created_user" id="created_user_yes" value="1" {{ (!$fair->id && !$fair->user_id)? 'checked' : '' }}> Yes &nbsp;&nbsp;
                                    <input class="iCheck-input form-check-input" type="radio" name="created_user" id="created_user_no" value="0" {{ ($fair->id && !$fair->user_id)? 'checked' : '' }}> No
                                </div>
                            </div>

                            {{--First Name--}}
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="first_name">First name (*)</label>
                                    <input id="first_name" class="form-control" type="text" placeholder="First Name" name="first_name" value="{{ old('first_name', $fair->{'first_name'}) }}" data-for-user="true" required>
                                </div>
                            </div>

                            {{--Last Name--}}
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="last_name">Last name (*)</label>
                                    <input id="last_name" class="form-control" type="text" placeholder="Last Name" name="last_name" value="{{ old('last_name', $fair->{'last_name'}) }}" data-for-user="true" required>
                                </div>
                            </div>

                            {{--Email--}}
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email (*)</label>
                                    <input id="email" class="form-control" type="email" placeholder="Email" name="email" value="{{ old('email', $fair->{'email'}) }}" data-for-user="true">
                                </div>
                            </div>

                        @endif

                        {{--User Recap--}}
                        @if( $fair->user_id )
                            <div class="col-md-12 col-xs-12">
                                <p>
                                    <b>Associated User data (uneditable)</b><br>
                                    User: {{ $fair->user()->first()->{'username'} }}<br>
                                    User First Name: {{ $fair->user()->first()->{'first_name'} }}<br>
                                    User Last Name: {{ $fair->user()->first()->{'last_name'} }}<br>
                                    User Email: {{ $fair->user()->first()->{'email'} }}
                                </p>
                            </div>
                        @endif

                        {{--Enable--}}
                        {{--Se utente è già stato attivato almeno una volta non mostrare più opzione di attivazione--}}
                        @if( ($fair->user()->first() && $fair->user()->first()->{'user_first_enable'} == 0 ) || !$fair->user_id )
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="fair_status">Enable</label>
                                    <select class="select2 form-control" name="fair_status" data-for-user="true">
                                        <option value="0">
                                            No
                                        </option>
                                        <option value="1">
                                            Yes
                                        </option>
                                    </select>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            {{-- Main Images --}}
            @include('partials.image-default', ['item' => $fair])

            @if( !roleCheck('gallerist|artist') )
                {{-- Thumbnail Images --}}
                @include('partials.images', ['item' => $fair, 'square_box_active' => false, 'wide_box_active' => true,'vertical_box_active' => false ])
            @endif

        @endif {{--!roleCheck('seo')--}}

        {{-- SEO Management --}}
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    SEO Management
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_title">SEO - title</label>
                                <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $fair->{'meta_title'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_keywords">SEO - keywords</label>
                                <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $fair->{'meta_keywords'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="h1">SEO - H1</label>
                                <input class="form-control" name="h1" type="text" placeholder="SEO - H1" id="h1" value="{{ $fair->{'h1'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="canonical">SEO - Canonical Url</label>
                                <input class="form-control" name="canonical" type="text" placeholder="SEO - Canonical Url" id="canonical" value="{{ $fair->{'canonical'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meta_description">SEO - Description</label>
                                <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">
                                {{ $fair->{'meta_description'} }}
                            </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Footer & Submit --}}
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endif