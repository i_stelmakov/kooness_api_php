{{-- User - Profile --}}

@extends('layouts.app')

@section('title')
    User - Profile
@endsection
@section('title', 'Home')

@section('body-class')
    user-profile
@endsection

@section('maps')
    <script type="text/javascript" src="/js/map-dependencies.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxzde8RaVZH-pv5ISqx402r4vR9IDWGsI&libraries=places&callback=initGMaps"></script>
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/user.js"></script>
    <script type="text/javascript" src="/js/collections.js"></script>
@endsection

@section('content')

    {{-- Gallery Modal--}}
    <div class="remodal" data-remodal-id="modal-my-gallery" data-remodal-options="hashTracking: false">
        <div id="loader-modal"></div>
        <div class="content-loader" id="loader-modal-content">

            <button data-remodal-action="close" class="remodal-close"></button>

            <form class="collect-form">
                <h2 class="collect-form-header">Collection Name:</h2>
                <input type="text" name="collection-name" placeholder="Collection name">
                <span class="horizontal-space-md"></span> <a class="change-collection-name save-collection-name">
                    <i class="fa fa-floppy-o"></i> Save </a>

                <h3 class="collect-form-header">Artworks list:</h3>

                <div class="gallery-items-list">

                </div>

                <div>
                    <button class="collection-close small-input-button" data-remodal-action="cancel">Done</button>
                </div>
            </form>

        </div>
    </div>
    {{-- Gallery Modal --}}

    {{-- Profile Modal--}}
    <div class="remodal" data-remodal-id="modal-profile" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h2 class="collect-form-header">Personal Information:</h2>
        <form class="user-form k-form container col-container-with-offset-and-margin">
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="text" id="first_name" name="first_name" placeholder="First Name" value="{{ Auth::user()->first_name }}" required>
            </div>
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="text" id="last_name"  name="last_name" placeholder="Last Name" value="{{ Auth::user()->last_name }}" required>
            </div>
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="email" id="email" name="email" placeholder="Email" value="{{ Auth::user()->email }}" disabled>
            </div>
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="password" id="old_password" name="old_password" placeholder="Old Password">
            </div>
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="password" id="new_password" name="new_password" placeholder="New Password">
            </div>
            {{--third--}}
            <div class="one-third-col-with-margin">
                <input class="full-col" type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password"><br>
            </div>
            <div>
                <input class="small-input-button" type="submit" value="Save">
                <button class="small-input-button" data-remodal-action="cancel">Cancel</button>
            </div>
        </form>
    </div>
    {{-- Profile Modal --}}

    {{-- Address Modal--}}
    <div class="remodal" data-remodal-id="modal-address" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <span class="h1">Addresses</span>
        <p>Add or edit all your addresses for shipping o billing.</p>
        <a class="accordion" href="#"><i class="fa fa-plus"></i> Add New Address</a>
        <div class="panel">
            <form id="addressForm0" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="addresses-editing" data-id="">
                {{--full--}}
                <div class="full-col-with-margin">
                    <select class="full-col" id="address_type_select" name="type">
                        <option value="private">Address type: <b>Private</b></option>
                        <option value="company">Address type: <b>Organization</b></option>
                    </select>
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin for-private">
                    <input class="full-col" type="text" name="first_name" placeholder="First Name" required="">
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin for-private">
                    <input class="full-col" type="text" name="last_name" placeholder="Last Name" required="">
                </div>
                {{--full--}}
                <div class="full-col-with-margin for-company" style="display: none">
                    <input class="full-col" type="text" name="company_name" placeholder="Company name" required="" disabled>
                </div>
                {{--full--}}
                <div class="full-col-with-margin">
                    <input class="full-col" type="text" name="fiscal_code" placeholder="Fiscal Code">
                </div>
                {{--full--}}
                <div class="full-col-with-margin for-company" style="display: none">
                    <input class="full-col" type="text" name="vat_number" placeholder="Vat Number" required="" disabled>
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col" type="text" name="email" placeholder="Email" required="">
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col" type="tel" name="phone" placeholder="Phone" required="">
                </div>
                {{--full--}}
                <div class="full-col-with-margin">
                    <input id="autocomplete-0" class="full-col address-autocomplete" type="text" name="autocomplete-0" placeholder="Viale Europa, 28/30, Castiglione delle Stiviere, MN, Italia" required="">
                    <input class="address" type="hidden" name="address">
                    <input class="number" type="hidden" name="number">
                    <input class="city" type="hidden" name="city">
                    <input class="state" type="hidden" name="state">
                    <input class="zip" type="hidden" name="zip_code">
                    <input class="country" type="hidden" name="country">
                    <input class="country_code" type="hidden" name="country_code">
                </div>
                {{--hidden--}}
                <input class="small-input-button" type="submit" value="Add New">
            </form>
        </div>
        @foreach($addresses as $address)
            <div class="address-item">
                <hr>
                <div class="address-text">{{ $address->label }}</div>
                <a class="accordion" href="#"><i class="fa fa-edit"></i> Edit Address</a>
                <div class="panel">
                    <form id="addressForm{{ $address->id }}" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="addresses-editing" data-id="{{ $address->id }}">
                        {{--full--}}
                        <div class="full-col-with-margin">
                            <select class="full-col" id="address_type_select" name="type">
                                <option value="private" {{ $address->type == 'private' ? 'selected="selected"' : '' }}>Address type: <b>Private</b></option>
                                <option value="company" {{ $address->type == 'company' ? 'selected="selected"' : '' }}>Address type: <b>Organization</b></option>
                            </select>
                        </div>
                        {{--half--}}
                        <div class="one-half-col-with-margin for-private" style="{{ $address->type == 'company' ? 'display:none' : '' }}">
                            <input class="full-col" type="text" name="first_name" placeholder="First Name" required="" value="{{ $address->first_name }}" {{ $address->type == 'company' ? 'disabled' : '' }}>
                        </div>
                        {{--half--}}
                        <div class="one-half-col-with-margin for-private" style="{{ $address->type == 'company' ? 'display:none' : '' }}">
                            <input class="full-col" type="text" name="last_name" placeholder="Last Name" required="" value="{{ $address->last_name }}" {{ $address->type == 'company' ? 'disabled' : '' }}>
                        </div>
                        {{--full--}}
                        <div class="full-col-with-margin for-company" style="{{ $address->type == 'private' ? 'display:none' : '' }}">
                            <input class="full-col" type="text" name="company_name" placeholder="Company name" required="" value="{{ $address->company_name }}" {{ $address->type == 'private' ? 'disabled' : '' }}>
                        </div>
                        {{--full--}}
                        <div class="full-col-with-margin">
                            <input class="full-col" type="text" name="fiscal_code" placeholder="Fiscal Code" value="{{ $address->fiscal_code }}">
                        </div>
                        {{--full--}}
                        <div class="full-col-with-margin for-company" style="{{ $address->type == 'private' ? 'display:none' : '' }}">
                            <input class="full-col" type="text" name="vat_number" placeholder="Vat Number" required=""  value="{{ $address->vat_number }}" {{ $address->type == 'private' ? 'disabled' : '' }}>
                        </div>

                        <div class="one-half-col-with-margin">
                            <input class="full-col" type="email" name="email" placeholder="Email" required="" value="{{ $address->email }}">
                        </div>
                        {{--half--}}
                        <div class="one-half-col-with-margin">
                            <input class="full-col" type="tel" name="phone" placeholder="Phone" required="" value="{{ $address->phone }}">
                        </div>
                        {{--full--}}
                        <div class="full-col-with-margin">
                            <input id="autocomplete-{{ $address->id }}" class="full-col address-autocomplete" type="text" name="autocomplete-{{ $address->id }}" placeholder="Viale Europa, 28/30, Castiglione delle Stiviere, MN, Italia" required="" value="{{ $address->label }}">
                            <input class="address" type="hidden" name="address" value="{{ $address->address  }}">
                            <input class="number" type="hidden" name="number" value="{{ $address->number }}">
                            <input class="city" type="hidden" name="city" value="{{ $address->city }}">
                            <input class="state" type="hidden" name="state" value="{{ $address->state }}">
                            <input class="zip" type="hidden" name="zip_code" value="{{ $address->zip }}">
                            <input class="country" type="hidden" name="country" value="{{ $address->country }}">
                            <input class="country_code" type="hidden" name="country_code" value="{{ $address->country_code }}">
                        </div>
                        {{--hidden--}}
                        <input class="small-input-button" type="submit" value="Save Address">
                        <button class="small-input-button delete-address">Delete Address</button>
                    </form>
                </div>
            </div>
        @endforeach
        <hr>
        <br>
        {{-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> --}}
        <button data-remodal-action="confirm" class="remodal-confirm">Close</button>
    </div>
    {{-- Address Modal--}}

    {{-- Card Modal--}}
    <div class="remodal" data-remodal-id="modal-cards" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <span class="h1">Cards</span>
        <p>Add or edit all your cards for shipping o billing.</p>
        <a class="accordion" href="#"><i class="fa fa-plus"></i> Add New Card</a>
        <div class="panel">
            <form id="cardForm0" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="cards-editing" data-id="">
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col" type="text" name="card_owner" placeholder="Owner" required="" value="">
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col card-number" type="text" name="card_number" placeholder="Number" required="" value="">
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <select class="full-col" name="card_exp_month" required>
                        <option value="01">January</option>
                        <option value="02">February</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <select class="full-col" name="card_exp_year" required>
                        @for($i = date('Y'); $i < date('Y') + 10; $i++)
                            <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col" type="text" name="card_cvv" placeholder="CVV" required="" value="">
                </div>
                {{--half--}}
                <div class="one-half-col-with-margin">
                    <input class="full-col" type="text" name="card_type" placeholder="Type" required="" readonly value="">
                </div>
                <input class="small-input-button" type="submit" value="Add New">
            </form>
        </div>
        @foreach($payments as $payment)
            <div class="card-item" data-id="{{ $payment->id }}">
                <hr>
                <div class="card-text"><img src="/images/credit_cards/light/{{ strtolower($payment->type) }}.png">
                    <p>{{ $payment->type }} - end in ***{{ $payment->end_with }}</p></div>
                <a class="delete-card"><i class="fa fa-close"></i> Delete Card</a>
            </div>
        @endforeach
        <hr>
        <br>
        {{-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> --}}
        <button data-remodal-action="confirm" class="remodal-confirm">Close</button>
    </div>
    {{-- Card Modal--}}

    {{-- Linked Modal--}}
    <div class="remodal" data-remodal-id="modal-linked" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h2 class="collect-form-header">Linked Accounts:</h2>
        <form class="user-form k-form container col-container-with-offset-and-margin">
            {{--half--}}
            <div class="one-half-col-with-margin">
                <input class="full-col" type="text" name="facebook" placeholder="Facebook link" required="" value="">
            </div>
            {{--half--}}
            <div class="one-half-col-with-margin">
                <input class="full-col" type="text" name="twitter" placeholder="Twitter link" required="" value="">
            </div>
            {{--half--}}
            <div class="one-half-col-with-margin">
                <input class="full-col" type="text" name="linkedin" placeholder="Linkedin link" required="" value="">
            </div>
            {{--half--}}
            <div class="one-half-col-with-margin">
                <input class="full-col" type="text" name="google" placeholder="Google link" required="" value="">
            </div>
            <div>
                <input class="small-input-button" type="submit" value="Save">
                <button class="small-input-button" data-remodal-action="cancel">Cancel</button>
            </div>
        </form>
    </div>
    {{-- Linked Modal --}}



    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>User page</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="user-tabs" class="tab-section">
        <div class="container boxed-container">
            <div class="container col-container-with-offset-and-margin container-vertical-padding-bottom">
                <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                    <ul>
                        <li class="tab-button tab-display current-tab-item" data-id="1"><a href="#collection">Collection</a>
                            <ul style="display: block;">
                                <li><a href="#my-galleries-slider">My Collections</a></li>
                                <li><a href="#saved-artworks">Saved Artworks</a></li>
                                <li><a href="#saved-artists">Saved Artists</a></li>
                                <li><a href="#saved-galleries">Saved Galleries</a></li>
                            </ul>
                        </li>
                        <li class="tab-button tab-display" data-id="4"><a href="#orders">Orders</a>
                        </li>
                        <li class="tab-button tab-display" data-id="2"><a href="#profile">Profile</a>
                        </li>
                        <li class="tab-button tab-display" data-id="3"><a href="#accounts">Linked Accounts</a></li>
                    </ul>
                </div>

                <!-- Tab 1 !-->
                <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">

                        <section id="my-galleries-slider" class="default-slider-section slider-border">
                            <div class="container boxed-container">
                                <div class="default-slider-header">
                                    <span class="h1">My Collections</span>
                                    <p>
                                        <a class="read-more" href="/users/collections">All my collections</a>
                                    </p>
                                </div>
                                <div class="container col-container-with-offset-and-margin gallery-container">
                                    @if(Auth::user()->collections()->count())
                                        @foreach(Auth::user()->collections()->limit(3)->get() as $collection)
                                            @include('users.frontend.partials.my-gallery', ['collection' => $collection])
                                        @endforeach
                                    @else
                                        No favourite
                                    @endif
                                </div>
                            </div>
                        </section>

                        <section id="product-slider" class="default-slider-section slider-border">
                            <div id="saved-artworks" class="container boxed-container">
                                <div class="default-slider-header">
                                    <span class="h1">Saved Artworks</span>
                                    <p>
                                        <a class="read-more" href="/users/favourite/artworks">All saved Artworks</a>
                                    </p>
                                </div>
                                <div class="container col-container-with-offset-and-margin">
                                    @if(Auth::user()->collectionArtworks()->count())
                                        @foreach(Auth::user()->collectionArtworks()->limit(8)->get() as $artwork)
                                            @include('users.frontend.partials.artwork', ['artwork' => $artwork])
                                        @endforeach
                                    @else
                                        No favourite
                                    @endif
                                </div>
                            </div>
                        </section>

                        <section id="artist-slider" class="default-slider-section slider-border">
                            <div id="saved-artists" class="container boxed-container">
                                <div class="default-slider-header">
                                    <span class="h1">Saved Artists</span>
                                    <p>
                                        <a class="read-more" href="/users/favourite/artists">All saved Artists</a>
                                    </p>
                                </div>
                                <div class="col-container">
                                    @if(Auth::user()->collectionArtworks()->count())
                                        @foreach(Auth::user()->collectionArtists()->limit(8)->get() as $artist)
                                            @include('users.frontend.partials.artist', ['artist' => $artist])
                                        @endforeach
                                    @else
                                        No favourite
                                    @endif
                                </div>
                            </div>
                        </section>

                        <section id="gallery-slider" class="default-slider-section slider-border">
                            <div id="saved-galleries" class="container boxed-container">
                                <div class="default-slider-header">
                                    <span class="h1">Saved Galleries</span>
                                    <p>
                                        <a class="read-more" href="/users/favourite/galleries">All saved galleries</a>
                                    </p>
                                </div>
                                <div class="col-container">
                                    @if(Auth::user()->collectionGalleries()->count())
                                        @foreach(Auth::user()->collectionGalleries()->limit(6)->get() as $gallery)
                                            @include('users.frontend.partials.gallery', ['gallery' => $gallery])
                                        @endforeach
                                    @else
                                        No favourite
                                    @endif
                                </div>
                            </div>
                        </section>
                </div>
                <!-- Tab 2 !-->
                <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                    <div id="about-you" class="container-vertical-padding-bottom">
                        <h2>About You</h2>
                        <h4>Personal Information</h4>
                        <div id="default-sheet">
                            <div class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Name</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell first-name-result">
                                        <p>{{ Auth::user()->first_name }}</p>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Surname</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell last-name-result">
                                        <p>{{ Auth::user()->last_name }}</p>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Email</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell email-result">
                                        <p>{{ Auth::user()->email }}</p>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text"><strong>Password</strong></p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <p>*******************</p>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p><a id="modal-profile-trigger" data-modal-id="0">Edit</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--Addresses Section--}}
                    <div id="my-addresses" class="container col-container">
                        <div class="col full-col">
                            <h4>Addresses</h4>
                            <p>Here below your Addresses:</p>
                            <div id="billing-address">
                                @if(count($addresses))
                                    @foreach($addresses as $address)
                                        <p id="{{ $address->id }}"> {{ $address->label }}</p>
                                    @endforeach
                                @else
                                    <p class="empty">No address found. Add new one.</p>
                                @endif

                            </div>
                            <br>
                            <a data-remodal-target="modal-address" href="#"><i class="fa fa-edit"></i> Edit Address list</a>
                        </div>
                    </div>

                    <div id="payment-methods">

                        {{--Credit Cards Section--}}
                        <div id="credit-cards" class="container col-container">
                            <div class="col full-col">
                                <h4>Credit Cards</h4>
                                <p>Here below your Credit Cards:</p>
                                @if(count($payments))
                                    @foreach($payments as $payment)
                                        <p class="card-item-account" id="card-{{ $payment->id }}" style="background-image: url('/images/credit_cards/light/{{ strtolower($payment->type) }}.png')"> {{ $payment->type }} - end in ***{{ $payment->end_with }}</p>
                                    @endforeach
                                @else
                                    <p class="empty">No cards found. Add new one.</p>
                                @endif
                                <a data-remodal-target="modal-cards" href="#"><i class="fa fa-edit"></i> Edit Credit Card list</a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Tab 3 !-->
                <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h4><i class="fa fa-facebook"></i>Facebook</h4>
                                </div>
                            </div>
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h4><i class="fa fa-twitter"></i>Twitter</h4>
                                </div>
                            </div>
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h4><i class="fa fa-linkedin"></i>Linkedin</h4>
                                </div>
                            </div>
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h4><i class="fa fa-google-plus"></i>Google +</h4>
                                </div>
                            </div>
                            <a id="modal-linked-trigger">Edit</a>
                        </div>
                    </div>
                </div>
                <!-- Tab 4 !-->
                <div id="linked-accounts" class="tab-container four-fifths-col-with-margin tab" data-id="4">
                    <div id="default-sheet">
                        <div class="default-sheet-box">
                            <div class="default-sheet-row">
                                <table class="user-orders-table" cellspacing=0 cellpadding=0>
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Reference</th>
                                            <th>Title</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Total Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr class="orders-table-separator">
                                            <td colspan="6"></td>
                                        </tr>

                                        <tr class="table-border-top">
                                            <td class="image">

                                            @foreach($order->items()->limit(4)->get() as $item)
                                                @if(count($order->items()->get()) == 1)
                                                    <div class="ratio-container-1-1">
                                                        <div class="ratio-content cover-image lazy" data-src="{{$item->artwork()->first()->main_image->url }}"></div>
                                                    </div>
                                                @else
                                                    <div class=" four-box-division lazy" data-src="{{$item->artwork()->first()->main_image->url }}">
                                                    </div>
                                                @endif
                                            @endforeach
                                            </td>
                                            <td colspan="4" class="items-box">
                                                {{--===== INNER ORDER ITEMS TABLE--}}
                                                <table class="user-orders-items-table">
                                                @foreach($order->items()->get() as $item)
                                                    <tr>
                                                        <td class="reference">
                                                            <p>{{ $item->artwork()->first()->reference }}</p>
                                                        </td>
                                                        <td class="title">
                                                            <p>{{ $item->artwork()->first()->title }}</p>
                                                        </td>
                                                        <td class="quantity">
                                                            <p>{{ $item->quantity }}</p>
                                                        </td>
                                                        <td class="unit-price">
                                                            <p>{{ $item->subtotal }} €</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </table>
                                                {{--===== INNER ORDER ITEMS TABLE--}}
                                            </td>
                                            <td class="total-price">
                                                <span>{{ $order->amount }} €</span> for {{count($order->items()->get())}} items<br>
                                                <span class="order-id">#{{ $order->serial }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection