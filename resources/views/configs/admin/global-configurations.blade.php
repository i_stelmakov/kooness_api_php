@extends('layouts.admin')
@section('title', 'Admin | Edit Free Shipping Configuration')
@section('page-header', 'Admin - Edit Free Shipping Configuration')

@section('content')
<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="action-submit" role="form" method="POST" data-attachment="validate" data-id="" data-referer="">
            {{ method_field('POST') }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Config Free Shipping
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <!-- Activate Free Shipping -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><input class="iCheck-input" type="checkbox" name="free_shipping_enabled" {{ ($data->{'free_shipping_enabled'}? 'checked="checked"' : '') }}>
                                    Activate Free Shipping for entire site</label>
                            </div>
                        </div>
                        <!-- Start Date -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="free_shipping_start_date">Start Date (*)</label>
                                <input id="free_shipping_start_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="free_shipping_start_date" value="{{ old('free_shipping_start_date', $data->{'free_shipping_start_date'}) }}" required>
                            </div>
                        </div>
                        <!-- End Date -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="free_shipping_end_date">End Date (*)</label>
                                <input id="free_shipping_end_date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="free_shipping_end_date" value="{{ old('free_shipping_end_date', $data->{'free_shipping_end_date'}) }}" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection