@extends('layouts.admin')
@section('title', 'Admin | Google Shop Bulk Download')
@section('page-header', 'Google Shop Bulk Download')

@section('content')
    <style>
        #modal-upload .modal-title i {
            vertical-align: middle;
            font-size: 21px;
            margin-left: 3px;
            color: steelblue;
            cursor: pointer;
        }

        #modal-upload-info .modal-dialog {
            width: 80%
        }

        table.excel {
            margin: 10px 0 15px 0;
            width: 100%;
            text-align: left;
        }

        table.excel thead tr th,
        table.excel tfoot tr th {
            background-color: #eeeeee;
            border: 1px solid #000;
            font-weight: bold;
            color: #333333;
            white-space: nowrap;
            text-align: left;
            padding: 5px;
        }

        table.excel tbody td {
            color: #333333;
            background-color: #FFF;
            vertical-align: middle;
            border: 1px solid #000;
            padding: 5px;
        }
        .display-none{
            display: none;
        }
    </style>
    <div class="row">
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    File Format
                </div>
                <div class="box-body">
                    <div class="row no-margin margin-bottom">
                        <div class="pull-right">
                            <button class="btn btn-primary download-btn" data-type="{{ $type }}">Download current file</button>
                        </div>
                    </div>
                    <div class="row" style="overflow: hidden; padding: 0 15px">
                        <div class="col-lg-12" style="padding: 0;;overflow: hidden;overflow-x: scroll;">

                            <table width="100%" class="excel no-wrap" cellspacing="1">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>title</th>
                                        <th>description</th>
                                        <th>product_category</th>
                                        <th>product_type</th>
                                        <th>link</th>
                                        <th>image_link</th>
                                        <th>availability_status</th>
                                        <th>price</th>
                                        <th>brand</th>
                                        <th>size</th>
                                        <th>identifier_exists</th>
                                        <th>custom_label_0</th>
                                        <th>custom_label_1</th>
                                        <th>shipping</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($artworks as $artwork)
                                        <tr>
                                            <td>{{ $artwork->reference }}</td>
                                            <td>{{ $artwork->artist->full_name }} - {{ $artwork->title }} - {{ $artwork->year }}</td>
                                            <td class="truncate">{!! $artwork->about_the_work !!}</td>
                                            <td>Home &amp; Garden &gt; Decor &gt; Artwork &gt; Posters, Prints, &amp; Visual Artwork</td>
                                            <td>{{join(', ',$artwork->medium->pluck('name')->toArray())}}</td>
                                            <td>{{ route('artworks.single', [$artwork->slug]) }}</td>
                                            <td>{{ $artwork->main_image->url }}</td>
                                            <td>
                                                @if($artwork->status == 1)
                                                    in stock
                                                @else
                                                    out of stock
                                                @endif
                                            </td>
                                            <td>{{ number_format($artwork->price, 2, '.','') }} EUR</td>
                                            <td>{{ $artwork->artist->full_name }}</td>
                                            <td>{{ $artwork->measure_inc }} in</td>
                                            <td>no</td>
                                            <td>{{ $artwork->gallery_name }}</td>
                                            <td>{{ $artwork->year }}</td>
                                            <td>IT:::0 EUR</td>
                                        </tr>
                                    @endforeach

                                    </tr>
                                    <tr>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                        <td>...</td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('script-bulk')
@endsection