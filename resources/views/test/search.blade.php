{{-- Test - Search --}}

@extends('layouts.app')

@section('title')
    Test - Search
@endsection

@section('body-class')
    test-search
@endsection

@section('scripts')
@endsection

@section('styles')
    <style>
        .sections * {
            box-shadow: 0 0 0 1px aqua;
        }
        #result {
            position: absolute;
            width: 100%;
            max-width:870px;
            max-width: none;
            cursor: pointer;
            overflow-y: auto;
            max-height: 400px;
            box-sizing: border-box;
            z-index: 1001;
        }
        .link-class:hover{
            background-color:#f1f1f1;
        }

        .accordion-toggle {cursor: pointer;}
        .accordion-content {display: none;}
        .accordion-content.default {display: block;}

    </style>
@endsection

@section('content')
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">

                {{--TEST CONTENT--}}

                <input type="text" name="search" id="search" placeholder="Search Employee Details" class="form-control" /><i title="Search" class="fa fa-search"> </i>

                <div id="accordion" style="display: block; width: 100%; position:relative;">
                    <div style="display: inline-block; width: 33%; position:relative;">
                        <h4 class="accordion-toggle" data-accordion="1">Artists<span id="artists-result-count"></span></h4>
                    </div>
                    <div style="display: inline-block; width: 33%; position:relative;">
                        <h4 class="accordion-toggle" data-accordion="2">Galleries<span id="galleries-result-count"></span></h4>
                    </div>
                    <div style="display: inline-block; width: 33%; position:relative;">
                        <h4 class="accordion-toggle" data-accordion="3">Categories<span id="categories-result-count"></span></h4>
                    </div>
                    <div style="display: inline-block; width: 100%; position:relative;">
                        <div id="#panel-1" class="accordion-content default" data-slide="1">
                            <ul id="artists-result"></ul>
                        </div>
                        <div id="#panel-2" class="accordion-content" data-slide="2">
                            <ul id="galleries-result"></ul>
                        </div>
                        <div id="#panel-3" class="accordion-content" data-slide="3">
                            <ul id="categories-result"></ul>
                        </div>
                    </div>
                </div>

                {{--TEST CONTENT--}}
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function(){

            // Ajax JSON search
            $.ajaxSetup({ cache: false });
            $('#search').keyup(function(){
                $('#result').html('');
                var searchField = $('#search').val();
                // console.log(searchField);
                ajaxCall(searchField);

            });


            // Ajax input search
            var ajaxCall = function (searchText) {
                console.log('ajax call started, wait for results...');
                console.log('Searching for => ' + searchText);

                $.ajax({
                    url: '/search',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        query: searchText
                    },
                    complete: function (response) {
                        console.log('Complete!');
                    },
                    error: function (response) {
                        console.log('Error!');
                    },
                    success: function (response) {
                        console.log('Success! Response => ');
                        console.log(response);

                        // Add new items
                        var artists = response.artists;
                        var galleries = response.galleries;
                        var categories = response.categories;
                        var artistsCount = 0;
                        var galleriesCount = 0;
                        var categoriesCount = 0;

                        $('#artists-result').html('');
                        $('#galleries-result').html('');
                        $('#categories-result').html('');

                        // insert Artist results
                        $.each(artists, function (index, artist) {
                            $('#artists-result').append('<li><a href="'+ artist.link +'"><img src="'+ artist.image_url +'" height="40" width="40" class="img-thumbnail" /> '+ artist.text +'</li>');
                            artistsCount++;
                        });
                        $('#artists-result-count').html(artistsCount);
                        if (artistsCount == 0) {$('#artists-result').html('No result for this section')}

                        // insert Galleries results
                        $.each(galleries, function (index, gallery) {
                            $('#galleries-result').append('<li><a href="'+ gallery.link +'"><img src="'+ gallery.image_url +'" height="40" width="40" class="img-thumbnail" /> '+ gallery.text +'</li>');
                            galleriesCount++;
                        });
                        $('#galleries-result-count').html(galleriesCount);
                        if (galleriesCount == 0) {$('#galleries-result').html('No result for this section')}

                        // insert Categories results
                        $.each(categories, function (index, category) {
                            $('#categories-result').append('<li><a href="'+ category.link +'"><img src="'+ category.image_url +'" height="40" width="40" class="img-thumbnail" /> '+ category.text +'</li>');
                            categoriesCount++;
                        });
                        $('#categories-result-count').html(categoriesCount);
                        if (categoriesCount == 0) {$('#categories-result').html('No result for this section')}


                    } // END - Success
                }); // END - Ajax
            } // END - ajaxCall function

            $('#btn_search').click(function () {
                console.log('button clicked');
                ajaxCall()
            });


            $('#accordion').find('.accordion-toggle').click(function(){

                var panelID = $(this).attr("data-accordion");
                var panelContent = $('#accordion').find("[data-slide='" + panelID + "']");
                // console.log(panelID);
                // console.log(panelContent);

                // First => Hide all panels
                $.when( $(".accordion-content").fadeOut()  ).then(function(  ) {
                    // Then => Expand or collapse this panel
                    panelContent.fadeIn();
                });


            });

        });
    </script>
@endsection