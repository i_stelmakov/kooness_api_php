@extends('layouts.admin')
@section('title', "Admin | Edit Gallery Category `{$category->name}`")
@section('page-header', "Admin - Edit Gallery Category `{$category->name}`")

@section('content')
    @include('galleries.categories.form', ['action' => route('admin.categories.galleries.update', [$category->id]), 'method' => 'PATCH'])
@endsection