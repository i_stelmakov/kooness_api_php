<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Post;
use App\Models\PostsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class FrontendController extends FrontendExtendedController
{

    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Archive All
    public function archive(Request $request, $section, $slug_name = null)
    {
        $filtered_by = $request->get('filtered_by');
        $categories = PostsCategory::all();
        $category = null;
        if($slug_name){
            $category = PostsCategory::whereSlug($slug_name)->first();
        }
        if ($section == 'news') {
            $posts_query = Post::whereRaw("posts.type = 'news'")->whereStatus(1)->orderBy('date', 'DESC');
            $posts_query->select('posts.*');
            if ($filtered_by) {
                $posts_query->whereRaw("YEAR(date) = '$filtered_by'");
            }
            $posts = $posts_query->paginate(18);
            $posts->appends($request->all());

            return view('posts.frontend.news-archive', compact('posts', 'section', 'filtered_by', 'hide_load_more'));
        } else if ($section == 'magazine') {
            $posts_query = Post::whereRaw("posts.type = 'magazine'")->whereStatus(1)->orderBy('date', 'DESC');
            $posts_query->select('posts.*');
            if ($filtered_by) {
                $posts_query->whereRaw("YEAR(date) = '$filtered_by'");
            }
            if($category){
                $posts_query->join('posts_posts_categories', 'posts_posts_categories.post_id', '=', 'posts.id')
                    ->join('posts_categories', 'posts_categories.id', '=', 'posts_posts_categories.category_id')
                    ->where('posts_categories.slug', '=', $category->slug);
            }
            $posts = $posts_query->paginate(18);
            $posts->appends($request->all());

            return view('posts.frontend.magazines-archive', compact('posts', 'section', 'categories', 'filtered_by', 'category', 'hide_load_more'));
        } else {
            abort(404);
        }
    }

    // Posts Single
    public function single(Request $request, $section, $slug_name)
    {
        $category = PostsCategory::whereSlug($slug_name)->first();
        if ($category) {
            return $this->archive($request, $section, $slug_name);
        }
        $post = Post::whereSlug($slug_name)->first(); // select * from posts (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)

        if ($post) {
            if ($section == 'magazine' && $post->type == 'news') {
                abort(404);
            } else if ($section == 'news' && $post->type == 'magazine') {
                abort(404);
            } else if($section != 'news' && $section != 'magazine'){
                abort(404);
            }

            $prev = Post::whereStatus(1)->where("id", "<", $post->id)->whereType($section)->orderBy('date', "ASC")->first();
            $next = Post::whereStatus(1)->where("id", ">", $post->id)->whereType($section)->orderBy('date', "ASC")->first();
            $other_posts = Post::whereStatus(1)->where("id", "!=", $post->id)->whereType($section)->orderBy('date', "ASC")->limit(3)->get();
            $categories = $post->categories()->get();
            $all_categories = PostsCategory::all();
            $post_tags = $post->tags()->get();
            $years = $post->getYears($section);

            if ($post->type == 'magazine') {
                return view('posts.frontend.magazines-single', compact('post', 'slug_name', 'categories', 'all_categories', 'post_tags', 'years', 'section', 'prev', 'next', 'other_posts'));
            } else {
                return view('posts.frontend.news-single', compact('post', 'slug_name', 'categories', 'all_categories', 'post_tags', 'years', 'section', 'prev', 'next', 'other_posts'));
            }
        } else {
            return redirect(route('posts.archive.all', [$section]));
        }
    }

//    public function loadMore(Request $request, $section)
//    {
//        if ($request->ajax()) {
//            $offset = $request->get('offset');
//            $filtered_by = $request->get('filtered_by');
//            $posts_query = Post::offset($offset)->limit(19)->whereStatus(1)->whereRaw("posts.type = '$section'")->orderBy('date', "DESC");
//            $posts_query->select('posts.*');
//            $category_id = $request->get('category');
//            $category = null;
//            if ($category_id) {
//                $category = PostsCategory::whereId($category_id)->first();
//            }
//            if ($filtered_by) {
//                $posts_query->whereRaw("YEAR(date) = '$filtered_by'");
//            }
//            if($category){
//                $posts_query->join('posts_posts_categories', 'posts_posts_categories.post_id', '=', 'posts.id')
//                    ->join('posts_categories', 'posts_categories.id', '=', 'posts_posts_categories.category_id')
//                    ->where('posts_categories.slug', '=', $category->slug);
//            }
//            $posts = $posts_query->get();
//
//            $hide_load_more = false;
//            if(count($posts) <= 18){
//                $hide_load_more = true;
//            } else{
//                $posts->pop();
//            }
//
//            $return = [];
//            foreach ($posts as $key => $post) {
//                $return[] = [
//                    "id" => $post->id,
//                    "name" => $post->title,
//                    "url" => route('posts.single', [$section, $post->slug]),
//                    "image" => $post->url_full_image,
//                    "date" => date('d F Y', strtotime($post->date)),
//                    "content" => ($section == 'news') ? '' : ''
//                ];
//            }
//
//            return response()->json([
//                "result" => "success",
//                "item_list" => $return,
//                "hide_load_more" => $hide_load_more
//            ]);
//        }
//    }
}
