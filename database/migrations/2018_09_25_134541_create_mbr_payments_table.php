<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMbrPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('serial');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->string('transaction_id')->nullable();

            $table->decimal('subtotal', 10,2)->default(0);
            $table->decimal('tax', 10,2)->default(0);
            $table->decimal('total', 10,2)->default(0);
            $table->enum('currency', ['EUR', 'USD', 'GBP']);
            $table->string('full_name');
            $table->string('vat_number'); // p.iva
            $table->string('fiscal_code'); // cf
            // $table->string('phone');
            $table->string('address');
            $table->integer('country')->unsigned()->nullable();
            $table->string('city');
            $table->string('state')->nullable();
            $table->string('zip');

            $table->string('card_type');

            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('plan_id')
                ->references('id')
                ->on('mbr_plans')
                ->onDelete('set null');

            $table->foreign('country')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_payments');
    }
}
