<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LayoutPage extends Model
{
    protected $guarded = [];

    public function widgets(){
        return $this->hasMany("App\Models\WidgetTemplate", 'layout_page_id', 'id');
    }

    public function slider(){
        return $this->hasOne("App\Models\WidgetSlider", 'layout_page_id', 'id');
    }
}
