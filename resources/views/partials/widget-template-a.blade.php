<section id="artist-slider" class="artist-slider widget-box default-slider-section slider-border">
    <div class="container boxed-container">
        <div class="container col-container-with-offset">
            {{-- Primo box - intro --}}
            <div class="one-fourth-col-with-margin slider-title-box">
                <div class="slider-title-box-txt">
                    <span class="h1">{{ $data['title'] }}
                        <span class="main-color-txt slider-title-span">{{ $data['subtitle'] }}</span>
                    </span>
                    <p>{{ $data['description'] }}</p>
                    <a title="{{ $data['link_title'] }}" class="black-button" href="{{ $data['link'] }}">{{ $data['link_title'] }}</a>
                </div>
                <div class="slider-title-drop-caps">
                    <h6 style="background-image: url(/images/opera-3.jpg)">k</h6>
                </div>
            </div>

            {{-- Slick container --}}
            <div class="three-fourth-col-with-margin">
                {{-- Slick --}}
                <div class='widget-slick-deactivated container col-container-with-offset-and-margin'>
            
                    {{-- Items list --}}
                    @foreach($data['items'] as $item)
                        <?php
                            $link = null;
                            if ($data['section'] == 'artworks') {
                                $link = route('artworks.single', [$item->slug]);
                                $img = $item->url_square_box;
                                $title = $item->title;
                                $class = '';
                                if (Auth::user())
                                    $class = (Auth::user()->collectionArtworks->contains($item->id)) ? 'active' : '';
                            } else if ($data['section'] == 'artists') {
                                $link = route('artists.single', [$item->slug]);
                                $title = $item->first_name . " " . $item->last_name;
                                $img = $item->url_square_box;
                                $class = '';
                                if (Auth::user())
                                    $class = (Auth::user()->collectionArtists->contains($item->id)) ? 'active' : '';

                            } else if ($data['section'] == 'galleries') {
                                $link = route('galleries.single', [$item->slug]);
                                $title = $item->name;
                                $img = $item->url_wide_box;
                                $class = '';
                                if (Auth::user())
                                    $class = (Auth::user()->collectionGalleries->contains($item->id)) ? 'active' : '';
                            }
                        ?>

                        <div class="slider-item one-third-col-with-margin">
                            <i title="Remove" class="fa fa-times delete-item"></i>

                            @if(Auth::user())
                                <div class="add-item follow {{$class}}" data-section="{{ $data['section'] }}" data-id="{{$item->id}}">
                                    @if($class == 'active')
                                        <i class="fa fa-heart"></i>
                                    @else
                                        <i class="fa fa-heart-o"></i>
                                    @endif
                                </div>
                            @endif

                            <div class="slider-item-img square-box">
                                <a title="{{ $title }}" href="{{ $link }}">
                                    <div class="background-cover gallery lazy" data-src="{{$img}}"></div>
                                </a>
                            </div>
                            @if($data['section'] == 'artworks')
                                @include('partials.includes.box-artwork', ["item" => $item])
                            @elseif($data['section'] == 'artists')
                                @include('partials.includes.box-artist', ["item" => $item])
                            @elseif($data['section'] == 'galleries')
                                @include('partials.includes.box-gallery', ["item" => $item])
                            @endif
                        </div>
                    @endforeach
                    {{-- Items - contenuti --}}

                </div>{{-- Slick --}}
            </div>{{-- Slick container --}}

        </div>
    </div>
</section>


