@extends('layouts.admin')
@section('title', "Admin | Edit Exhibition `{$exhibition->name}`")
@section('page-header', "Admin - Edit Exhibition `{$exhibition->name}`")

@section('content')
    @include('exhibitions.admin.form', ['action' => route('admin.exhibitions.update', [$exhibition->id]), 'method' => 'PATCH'])
@endsection