<?php

namespace App\Http\Controllers\Offers;

use App\Http\Controllers\Controller;
use App\Models\UserOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('offers.index');
    }

    public function retrieve(\Illuminate\Http\Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'user',
            2 => 'artwork',
            3 => 'amount',
            4 => 'price',
            5 => 'text',
            6 => 'status',
            7 => 'created_at',
            8 => 'action',
        );

        $totalData = UserOffer::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $usersOffers = UserOffer::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
//            $search = $request->input('search.value');
//
//            $users = User::where('first_name', 'LIKE', "%{$search}%")
//                ->orWhere('last_name', 'LIKE', "%{$search}%")
//                ->orWhere('email', 'LIKE', "%{$search}%")
//                ->offset($start)
//                ->limit($limit)
//                ->orderBy($order, $dir)
//                ->get();
//
//            $totalFiltered = User::where('first_name', 'LIKE', "%{$search}%")
//                ->orWhere('last_name', 'LIKE', "%{$search}%")
//                ->orWhere('email', 'LIKE', "%{$search}%")
//                ->count();
        }

        $data = array();
        if (!empty($usersOffers)) {
            foreach ($usersOffers as $offer) {
                $edit = null;
                $delete = null;
                $accept = route('admin.offers.change.status', [$offer->id, 2]);
                $reject = route('admin.offers.change.status', [$offer->id, 3]);
                if ($offer->status == 2) {
                    $accept = null;
                }
                if ($offer->status == 3) {
                    $reject = null;
                }

                $nestedData['id'] = $offer->id;
                $nestedData['user'] = $offer->user()->first()->first_name . " " . $offer->user()->first()->last_name;
                $nestedData['artwork'] = $offer->artwork()->first()->title;
                $nestedData['amount'] = number_format($offer->amount, 2, ',', '') . " €";
                $nestedData['price'] = $offer->artwork()->first()->pretty_price;
                $nestedData['text'] = $offer->text;
                $nestedData['status'] = get_offer_status($offer->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($offer->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['accept' => $accept, 'reject' => $reject, 'edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function changeStatus(Request $request, $offer, $status)
    {
        $offer = UserOffer::find($offer);
        if ($offer) {
            $offer->status = $status;
            if ($offer->save()) {
                if ($offer->status == 2){
                    $to = $offer->user()->first()->email;
                    $name = $offer->user()->first()->first_name . " " . $offer->user()->first()->last_name ;
                    $subject = SUBJECT_MAIL_OFFER_ACCEPTED;
                    Mail::send('emails.offers.accepted', ["artwork" => $offer->artwork()->first()], function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });
                    $request->session()->flash("success", "Proposal successfully accepted");
                }
                else{
                    $to = $offer->user()->first()->email;
                    $name = $offer->user()->first()->first_name . " " . $offer->user()->first()->last_name ;
                    $subject = SUBJECT_MAIL_OFFER_DECLINED;
                    Mail::send('emails.offers.declined', ["artwork" => $offer->artwork()->first()], function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });
                    $request->session()->flash("success", "Proposal successfully rejected");
                }
            }
        }
        return redirect()->route('admin.offers.index');
    }
}
