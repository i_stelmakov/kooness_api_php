<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $_one_to_one = 'url_square_box';
    private $_sixteen_nine = 'url_wide_box';
    private $_vertical = 'url_vertical_box';

    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('url_picture');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
            $table->renameColumn('url_picture', 'url_full_image');
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('slug');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
        });

        Schema::table('exhibitions', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('url_cover');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
            $table->renameColumn('url_cover', 'url_full_image');
        });

        Schema::table('fairs', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('url_picture');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
            $table->renameColumn('url_picture', 'url_full_image');
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('url_cover');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
            $table->renameColumn('url_cover', 'url_full_image');
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->string($this->_one_to_one)->nullable()->after('url_picture');
            $table->string($this->_sixteen_nine)->nullable()->after($this->_one_to_one);
            $table->string($this->_vertical)->nullable()->after($this->_sixteen_nine);
            $table->renameColumn('url_picture', 'url_full_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->renameColumn('url_full_image', 'url_picture');
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });

        Schema::table('artworks', function (Blueprint $table) {
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });

        Schema::table('exhibitions', function (Blueprint $table) {
            $table->renameColumn('url_full_image', 'url_cover');
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });

        Schema::table('fairs', function (Blueprint $table) {
            $table->renameColumn('url_full_image', 'url_picture');
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });

        Schema::table('galleries', function (Blueprint $table) {
            $table->renameColumn('url_full_image', 'url_cover');
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->renameColumn('url_full_image', 'url_picture');
            $table->dropColumn($this->_one_to_one);
            $table->dropColumn($this->_sixteen_nine);
            $table->dropColumn($this->_vertical);
        });
    }
}
