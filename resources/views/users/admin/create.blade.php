@extends('layouts.admin')
@section('title', 'Admin | Create User')
@section('page-header', 'Admin - Create User')

@section('content')
    @include('users.admin.form', ['action' => route('admin.users.store'), 'method' => 'POST'])
@endsection