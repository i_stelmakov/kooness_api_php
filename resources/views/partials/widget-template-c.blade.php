<section id="event-slider" class="event-slider widget-box default-slider-section slider-border">
    <div class="container boxed-container">
        <div class="default-slider-header">
            <span class="h1">{{ $data['title'] }}</span>
            <p>
                <a title="{{ $data['link_title'] }}" class="read-more" href="{{ $data['link'] }}">{{ $data['link_title'] }}</a>
            </p>
        </div>
        <div class="container col-container-with-offset-and-margin widget-slick-deactivated">
            @foreach($data['items'] as $key=>$item)
                <div class="slider-item one-third-col-with-margin">
                    <i title="Remove"  class="fa fa-times delete-item">
                    </i>
                    <div class="on-going-ticket">
                        <h4> {{ ucfirst($key) }}</h4>
                    </div>
                    <div class="slider-item-img wide-box">
                        <a href="{{ route('exhibitions.single', [$item->slug]) }}">
                            <div class="background-cover gallery lazy" data-src="{{ $item->url_wide_box }}"></div>
                        </a>
                    </div>
                    <div class="slider-item-txt">
                        <h2>
                            <a title="{{ $item->name }}" class="dark-text" href="{{ route('exhibitions.single', [$item->slug]) }}">{{ $item->name }}</a>
                        </h2>
                        <div class="event-date">
                            <p class="event-date-p">{{ $item->city }}, {{ $item->country()->first()->code }}</p>
                        </div>
                        <div class="default-sheet-row slider-item-row">
                            <div class="default-sheet-row-cell">
                                <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($item->start_date)) }} to {{ date('d/m/Y', strtotime($item->end_date)) }}</p>
                            </div>
                        </div>
                        <a title="Read more" class="read-more" href="{{ route('exhibitions.single', [$item->slug]) }}">Read more</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>