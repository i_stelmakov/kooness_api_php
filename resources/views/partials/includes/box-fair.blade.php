<div class="slider-item-txt">
    <h2><a title="{{ $item->name }}" class="dark-text" href="{{ route('fairs.single', [$item->slug]) }}">{{ $item->name }}</a></h2>
    <div class="event-date">
        <p class="event-date-p">{{ $item->city }}, {{ $item->country()->first()->code }}</p>
    </div>
    <div class="default-sheet-row slider-item-row">
        <div class="default-sheet-row-cell">
            <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($item->start_date)) }} to {{ date('d/m/Y', strtotime($item->end_date)) }}</p>
        </div>
    </div>
    <a title="Read more" class="read-more" href="{{ route('fairs.single', [$item->slug]) }}">Read more</a>
</div>