@extends('layouts.admin')
@section('title', 'Admin | Create Tag')
@section('page-header', 'Admin - Create Tag')

@section('content')
    @include('tags.admin.form', ['action' => route('admin.tags.store'), 'method' => 'POST'])
@endsection