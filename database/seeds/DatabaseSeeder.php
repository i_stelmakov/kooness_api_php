<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'superadmin-level']); // ID1 Test SuperAdmin (super)
        Permission::create(['name' => 'admin-level']); // ID2 TestAdmin (admin-level@notomia.com)
        Permission::create(['name' => 'fair-level']); // ID3 TestFair (fair-level@notomia.com)
        Permission::create(['name' => 'gallerist-level']); // ID4 TestGallerist (gallerist-level@notomia.com)
        Permission::create(['name' => 'artist-level']); // ID5 TestArtist (artist-level@notomia.com)
        Permission::create(['name' => 'editor-level']); // ID6 TestEditor (editor-level@notomia.com)
        Permission::create(['name' => 'seo-level']); // ID7 TestSeo (seo-level@notomia.com) TestPsw-1234
        Permission::create(['name' => 'user-level']); // ID8 TestUser (user-level@notomia.com)

        $role = Role::create(['name' => 'superadmin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('admin-level', 'fair-level', 'gallerist-level', 'artist-level', 'editor-level', 'user-level');

        $role = Role::create(['name' => 'fair']);
        $role->givePermissionTo('fair-level', 'user-level');

        $role = Role::create(['name' => 'gallerist']);
        $role->givePermissionTo('gallerist-level', 'user-level');

        $role = Role::create(['name' => 'artist']);
        $role->givePermissionTo('artist-level', 'user-level');

        $role = Role::create(['name' => 'editor']);
        $role->givePermissionTo('editor-level', 'user-level');

        $role = Role::create(['name' => 'seo']);
        $role->givePermissionTo('seo-level', 'user-level');

        $role = Role::create(['name' => 'user']);
        $role->givePermissionTo('user-level');

        $user = User::create([
            'username' => 'superadmin',
            'first_name' => str_random(10),
            'last_name' => str_random(10),
            'email' => 'kooness@notomia.com',
            'password' => bcrypt('Notomia2018'),
            'status' => 1
        ]);

        $user->assignRole('superadmin');
    }
}
