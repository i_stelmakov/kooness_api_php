@extends('layouts.admin')
@section('title', "Admin | Edit Artist Category `{$category->name}`")
@section('page-header', "Admin - Edit Artist Category `{$category->name}`")

@section('content')
    @include('artists.categories.form', ['action' => route('admin.categories.artists.update', [$category->id]), 'method' => 'PATCH'])
@endsection