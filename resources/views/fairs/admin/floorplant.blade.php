@extends('layouts.admin')
@section('title', 'Admin | Fair Floorplant')
@section('page-header', 'Admin - Fair Floorplant')

@section('scripts')
    <script src="/js/pdf.js?v=1537869937"></script>
    <script src="/js/pdf.worker.js?v=1537869937"></script>
    <script>
        $(function () {
            var _PDF_DOC,
                _CANVAS = document.querySelector('.pdf-preview'),
                _OBJECT_URL;

            function showPDF(pdf_url) {
                PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
                    _PDF_DOC = pdf_doc;

                    // Show the first page
                    showPage(1);

                    // destroy previous object url
                    URL.revokeObjectURL(_OBJECT_URL);
                }).catch(function(error) {
                    // trigger Cancel on error
                    document.querySelector(".cancel-pdf").click();

                    // error reason
                    alert(error.message);
                });
            }

            function showPage(page_no) {
                // fetch the page
                _PDF_DOC.getPage(page_no).then(function(page) {
                    // set the scale of viewport
                    var scale_required = _CANVAS.width / page.getViewport(1).width;

                    // get viewport of the page at required scale
                    var viewport = page.getViewport(scale_required);

                    // set canvas height
                    _CANVAS.height = viewport.height;

                    var renderContext = {
                        canvasContext: _CANVAS.getContext('2d'),
                        viewport: viewport
                    };

                    // render the page contents in the canvas
                    page.render(renderContext).then(function() {
                        document.querySelector(".pdf-preview").style.display = 'inline-block';
                        document.querySelector(".pdf-loader").style.display = 'none';
                    });
                });
            }


            /* Show Select File dialog */
            document.querySelector(".upload-dialog").addEventListener('click', function() {
                document.querySelector(".pdf-file").click();
            });

            /* Selected File has changed */
            document.querySelector(".pdf-file").addEventListener('change', function() {
                // user selected file
                var file = this.files[0];

                // allowed MIME types
                var mime_types = [ 'application/pdf' ];

                // Validate whether PDF
                if(mime_types.indexOf(file.type) === -1) {
                    alert('Error : Incorrect file type');
                    return;
                }

                // validate file size
                if(file.size > 10*1024*1024) {
                    alert('Error : Exceeded size 10MB');
                    return;
                }

                // validation is successful

                // hide upload dialog button

                // show cancel and upload buttons now
                document.querySelector(".upload-button").disabled = false;

                // Show the PDF preview loader
                document.querySelector(".pdf-loader").style.display = 'inline-block';

                // object url of PDF
                _OBJECT_URL = URL.createObjectURL(file);

                // send the object url of the pdf to the PDF preview function
                showPDF(_OBJECT_URL);
            });

            /* Upload file to server */
            document.querySelector(".upload-button").addEventListener('click', function() {
                waitingDialog.show('Saving data...', {
                    dialogSize: 'm',
                    progressType: 'success'
                });

                var previewImageCode = _CANVAS.toDataURL("image/jpeg");
                var previewImageInput = document.querySelector('input[name="fair_floorplant_preview"]');
                previewImageInput.value = previewImageCode;
                // AJAX request to server
                document.querySelector('.form-upload').submit();
            });


        });
    </script>
@endsection

@section('styles')
    <style>

        .preview-canvas-container{
            position: relative;
            margin-bottom: 10px;
            display: inline-block;
            background-image: url(/images/placeholder.png);
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;
        }
        .pdf-loader {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            vertical-align: middle;
            color: #cccccc;
            font-size: 12px;
            text-align: center;
            height: 24px;
        }

        .pdf-preview {
            vertical-align: middle;
            border: 1px solid rgba(0,0,0,0.2);
            border-radius: 2px;
        }

        /*Pulsante scegli file*/
        .upload-dialog {
            padding: 5px;
            border: 1px solid #336699;
            background-color: white;
            color: #336699;
            background: none;
            font-size: inherit;
            font-family: inherit;
            outline: none;
            display: inline-block;
            vertical-align: middle;
            cursor: pointer;
        }

        .pdf-file {
            display: none !important;
        }

        .cancel-pdf {
            display: none;
            vertical-align: middle;
            padding: 0;
            border: none;
            color: #777777;
            background-color: white;
            font-size: inherit;
            font-family: inherit;
            outline: none;
            cursor: pointer;
            margin: 0 0 0 15px;
        }
        .previous-guide-box {
            margin: 10px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class='col-md-12 col-xs-12'>
            <form class="form-upload" role="form" method="POST" onsubmit="return false;" enctype="multipart/form-data">
                @csrf
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Upload new floorplant
                    </div>
                    @if( $pdf_preview )
                        <div class="previous-floorplant-box pull-right"><b>Previously inserted floorplant:</b><br><img src="{{$pdf_preview}}"></div>
                    @endif
                    <div class="box-body">
                        <div class="preview-container">
                            <div>
                                <div class="preview-canvas-container">
                                    <canvas class="pdf-preview" width="250"></canvas>
                                    <div class="pdf-loader">Loading Preview...</div>
                                </div>
                            </div>
                            <button class="upload-dialog">Choose PDF</button>
                            <input type="file" class="pdf-file" name="pdf_floorplant" accept="application/pdf" />
                            <br>
                            <input type="hidden" class="pdf-file-preview" name="fair_floorplant_preview" value=""/>
                        </div>
                    </div>
                </div>

                <!-- Form Footer & Submit -->
                <div class="box box-primary box-solid">
                    <div class="box-footer">
                        <div class="col-md-12">
                            <div class="pull-right ">
                                <input class="btn btn-primary upload-button" type="submit" value="Save" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

