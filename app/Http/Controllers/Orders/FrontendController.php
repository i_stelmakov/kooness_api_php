<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artwork;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\PromoCode;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserPayment;
use App\Models\Config;
use App\Services\Api4Dem\Api4Dem;
use App\Services\DHL;
use App\Services\ExpressCheckout;
use App\Services\Notification;
use Barryvdh\DomPDF\Facade as PDF;
use Cartalyst\Stripe\Stripe;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Spatie\GoogleTagManager\GoogleTagManagerFacade as GoogleTagManager;


class FrontendController extends FrontendExtendedController
{

    use RegistersUsers;
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function addToCart(Request $request)
    {
        if ($request->method() == 'POST') {
            $is_new_4dem = false;
            if(!$this->_cart_instance->id){
                $this->_cart_instance->save();

                // se loggato & il carrello appena salvato appartiene all'utente
                if(Auth::user() && $this->_cart_instance->user_id == Auth::user()->id) {
                    // se non ha dato di 4dem associato
                    if (!$this->_cart_instance->api4dem_referer_id) {
                        // creo istanza 4dem
                        $api4Dem = new Api4Dem();
                        $api4Dem->cartCreate($this->_cart_instance, Auth::user());
                    }
                    foreach ($this->_cart_instance->items()->get() as $item) {
                        if (!$item->api4dem_success_send) {
                            $api4Dem = new Api4Dem();
                            $api4Dem->cartArtworkAdd($item, $this->_cart_instance);
                        }
                    }
                }
            }
            $id = $request->get('id');
            if ($id) {
                $artwork = Artwork::find($id);
                if ($artwork) {
                    if( !$this->_cart_instance->items()->whereProductId($id)->count() ) {
                        $line = $this->_cart_instance->addItem([
                            'product_id' => $id,
                            'unit_price' => $artwork->price,
                            'quantity' => 1
                        ]);
                        if(Auth::user() && $this->_cart_instance->user_id == Auth::user()->id && $this->_cart_instance->api4dem_referer_id) {
                            $api4Dem = new Api4Dem();
                            $api4Dem->cartArtworkAdd($line, $this->_cart_instance);
                        }
                    }
                }
            }
        }
        $request->session()->flash("msg_added_to_cart", "Artwork with title `{$artwork->title}` added to cart!");
        return redirect()->route('orders.view.checkout');
    }

    public function removeFromCart(Request $request)
    {
        if ($request->method() == 'POST') {
            $id = $request->get('id');
            if ($id) {
                $artwork = Artwork::find($id);
                if ($artwork) {
                    $line = $this->_cart_instance->items()->where(['product_id' => $id])->first();
                    $this->_cart_instance->removeItem([
                        'product_id' => $id
                    ]);
                    if(Auth::user() && $this->_cart_instance->user_id == Auth::user()->id && $this->_cart_instance->api4dem_referer_id) {
                        $api4Dem = new Api4Dem();
                        $api4Dem->cartArtworkRemove($this->_cart_instance, $line);
                    }
                }
            }
        }
        return redirect()->back();
    }

    public function cart()
    {
        // ☢️ START - test zone for thank you page Google ecommerce Tracking
        // Session::put("order_id", 3);
        // return redirect(route('orders.thank.you'));
        // ☢️ END - test zone for thank you page Google ecommerce Tracking
        $promo = null;
        if ($this->_cart_instance->promo_code) {
            $promo = PromoCode::find($this->_cart_instance->promo_code);
        }
        return view('orders.frontend.cart', compact('addresses', 'payments', 'promo'));
    }

    public function viewCheckout()
    {
        $addresses = Auth::user()->addresses()->get();
        $payments = Auth::user()->payments()->get();
        $promo = null;
        $remove = false;
        $hidePromoCode = false;
        foreach ($this->_cart_instance->items()->get() as $item) {
            if(Auth::user()->offers()->whereArtworkId($item->product_id)->whereStatus(2)->first()){
                $remove = true;
            }
        }
        if($remove){
            $this->_cart_instance->promo_code = null;
            $this->_cart_instance->save();
            $hidePromoCode = true;
        }
        if ($this->_cart_instance->promo_code) {
            $promo = PromoCode::find($this->_cart_instance->promo_code);
        }
        return view('orders.frontend.checkout', compact('addresses', 'payments', 'promo', 'hidePromoCode'));
    }

    public function getShippingRate(Request $request)
    {
        $free_shipping_start_date = Config::where('key', '=', 'free_shipping_start_date')->first(); // recupero dal DB la chiave indicata (o NULL oppure intera Row)
        $free_shipping_end_date = Config::where('key', '=', 'free_shipping_end_date')->first();
        $free_shipping_enabled = Config::where('key', '=', 'free_shipping_enabled')->first();

        // SE sono attivate le spese di spedizione gratuite a livello globale...
        if($free_shipping_enabled) {
            // verifico che il valore della key indicata sia uno
            if($free_shipping_enabled->value == '1') {
                // dd('free_shipping_enabled value exist AND it is equal to 1!');

                $todayDate = date('Y-m-d');
                $todayDate=date('Y-m-d', strtotime($todayDate));
                $freeShippingDateBegin = date('Y-m-d', strtotime($free_shipping_start_date->value));
                $freeShippingDateEnd = date('Y-m-d', strtotime($free_shipping_end_date->value));
                
                if (($todayDate >= $freeShippingDateBegin) && ($todayDate <= $freeShippingDateEnd)){
                    // dd('is between');
                    return response()->json([
                        'status' => 'success',
                        'tax_amount' => 0,
                        'is_free' => true,
                        'total' => $this->_cart_instance->total_price
                    ]);
        
                }
            }
        }

        // ...altrimenti passa la richiesta a DHL
        if ($request->ajax()) {
            $quote = 0;
            $dhl = new DHL();
            $user_address = Auth::user()->addresses()->find($request->get('address'));
            if ($user_address) {
                $dhl->setToCity($user_address->city);
                $dhl->setToZip($user_address->zip);
                $dhl->setToCountry($user_address->country_code);
                foreach ($this->_cart_instance->items()->get() as $item) {
                    $dhl->setPieces($item->product()->get());
                    $artwork = $item->product()->first();
                    $dhl->setFromZip($artwork->current_zip);
                    $dhl->setFromCity($artwork->current_city);
                    $dhl->setFromCountry($artwork->country()->first()->code);
                    $tmp = $dhl->getQuote();
                    // se DHL non riesce a recuperare le spese di spedizione...
                    if(!$tmp){
                        // restituisco errore dalla chiamata
                        return response()->json([
                            'status' => 'error',
                        ]);
                    }
                    $quote += $tmp;
                }
            }

            return response()->json([
                'status' => 'success',
                'tax_amount' => $quote,
                'is_free' => false,
                'total' => $this->_cart_instance->total_price + $quote
            ]);
        }
    }

    public function checkout(Request $request)
    {
        if ($request->method() == 'POST') {
            $provider = new ExpressCheckout;
            $stripe = Stripe::make(config('services.stripe.secret'));
            $data = Input::all();

            $payPalData = [
                'items' => [],
                'invoice_id' => null,
                'invoice_description' => null,
                'return_url' => route('orders.paypal.callback'),
                'cancel_url' => route('orders.cart'),
                'discount_amount' => 0,
                'total' => 0
            ];

            $billing_address = UserAddress::find($data['billing-address']);
            $shipping_address = UserAddress::find($data['shipping-address']);


            $free_shipping_start_date = Config::where('key', '=', 'free_shipping_start_date')->first(); // recupero dal DB la chiave indicata (o NULL oppure intera Row)
            $free_shipping_end_date = Config::where('key', '=', 'free_shipping_end_date')->first();
            $free_shipping_enabled = Config::where('key', '=', 'free_shipping_enabled')->first();
    
            $quote = 0;
            $dhl_calc = true;
            // SE sono attivate le spese di spedizione gratuite a livello globale...
            if($free_shipping_enabled) {
                // verifico che il valore della key indicata sia uno
                if($free_shipping_enabled->value == '1') {
                    // dd('free_shipping_enabled value exist AND it is equal to 1!');
    
                    $todayDate = date('Y-m-d');
                    $todayDate=date('Y-m-d', strtotime($todayDate));
                    $freeShippingDateBegin = date('Y-m-d', strtotime($free_shipping_start_date->value));
                    $freeShippingDateEnd = date('Y-m-d', strtotime($free_shipping_end_date->value));
                    
                    if (($todayDate >= $freeShippingDateBegin) && ($todayDate <= $freeShippingDateEnd)){
                        $dhl_calc = false;
                    }
                }
            }

            if($dhl_calc && isset($data['payment_method'])){
                $dhl = new DHL();
                if ($shipping_address) {
                    $dhl->setToCity($shipping_address->city);
                    $dhl->setToZip($shipping_address->zip);
                    $dhl->setToCountry($shipping_address->country_code);
                    foreach ($this->_cart_instance->items()->get() as $item) {
                        $dhl->setPieces($item->product()->get());
                        $artwork = $item->product()->first();
                        $dhl->setFromZip($artwork->current_zip);
                        $dhl->setFromCity($artwork->current_city);
                        $dhl->setFromCountry($artwork->country()->first()->code);
                        $tmp = $dhl->getQuote();
                        if (!$tmp) {
                            return response()->json([
                                'status' => 'error',
                            ]);
                        }
                        $quote += $tmp;
                    }
                }
            }

            $metadata = null;

            $order = new Order();
            $order->setBillingAddress($billing_address);
            $order->setShippingAddress($shipping_address);

            $order->serial = strtoupper(substr(sha1(time()), 0, 10));
            $order->cart_id = $this->_cart_instance->id;
            $order->amount = $this->_cart_instance->total_price + $quote;
            $order->subtotal = $this->_cart_instance->total_price;
            if(isset($data['payment_method'])) {
                $order->payment_method = ($data['payment_method'] != 'paypal' && $data['payment_method'] != 'bank_transfer') ? 'stripe' : $data['payment_method'];
            }else{
                $order->incomplete_order = true;
            }
            $order->tax = 0;
            $order->shipping = $quote;
            $order->shipping_method = 'delivery';
            $order->currency = 'eur';
            $order->note = $data['note'];
            $order->status = 1;
            $promo = null;
            if ($this->_cart_instance->promo_code) {
                $order->promo_code = $this->_cart_instance->promo_code;
                $promo = PromoCode::find($this->_cart_instance->promo_code);
                if ($promo) {
                    $order->amount = $this->_cart_instance->total_price * (1 - ($promo->value / 100));
                    $metadata = [
                        "coupon_code" => $promo->name,
                        "coupon_discount" => $promo->value . "%"
                    ];
                }
            }
            if (Auth::user())
                $order->user_id = Auth::user()->id;
            if ($order->save()) {
                foreach ($this->_cart_instance->items()->get() as $item) {
                    $orderItem = new OrderItem();
                    $orderItem->order_id = $order->id;
                    $orderItem->artwork_id = $item->product->id;
                    $orderItem->quantity = $item->quantity;
                    $orderItem->subtotal = $item->unit_price;
                    $orderItem->tax = 0.00;
                    $orderItem->amount = $item->quantity * $item->unit_price;
                    $orderItem->save();
                    if ($order->payment_method && $order->payment_method == 'paypal') {
                        $payPalData['items'][] = [
                            'name' => Artwork::find($orderItem->artwork_id)->title,
                            'price' => $orderItem->subtotal,
                            'qty' => $orderItem->quantity
                        ];
                    }
                }
                if ($order->payment_method == 'paypal' && $promo) {
                    $payPalData['items'][] = [
                        'name' => $promo->name . " " . $promo->value . "%",
                        'price' => -($order->subtotal * $promo->value) / 100,
                        'qty' => $orderItem->quantity
                    ];
                }
            }

            $to = Auth::user()->email;
            $name = Auth::user()->full_name;
            $subject = SUBJECT_MAIL_ORDER_RECEIVED;
            Mail::send('emails.orders.received', ["order" => $order], function ($message) use ($to, $name, $subject) {
                $message->to($to, $name)->subject($subject);
            });

            $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');
            $name = 'Kooness Order';
            $subject = SUBJECT_MAIL_ORDER_RECEIVED_ADMIN;
            Mail::send('emails.orders.received-admin', ["order" => $order], function ($message) use ($to, $name, $subject) {
                $message->to($to, $name)->subject($subject);
            });

            $api4Dem = new Api4Dem();
            $api4Dem->cartClose($this->_cart_instance);
            $this->_cart_instance->checkout();

            if(!isset($data['payment_method'])){
                foreach ($order->items()->get() as $orderItem) {
                    $artwork = Artwork::find($orderItem->artwork_id);
                    if ($artwork) {
                        $artwork->status = 2;
                        $artwork->save();
                    }
                }
                Session::put("order_id", $order->id);

                $users = User::permission('superadmin-level')->get();
                $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ORDER_WITHOUT_SHIPPING, 'order_without_shipping', $order);
                Notification::addUsers($notify, $users);

                return redirect(route('orders.thank.you.without.payment'));
            }

            if ($order->payment_method == 'paypal') {
                $payPalData['invoice_id'] = $order->id;
                $payPalData['shipping'] = $order->shipping;
                $payPalData['invoice_description'] = "Order #" . $order->serial;
                $payPalData['subtotal'] = $order->amount - $order->tax;
                $payPalData['total'] = $order->amount;
                try {
                    $response = $provider->setExpressCheckout($payPalData);
                    return redirect($response['paypal_link']);
                } catch (\Exception $e) {
                }
            } else if ($order->payment_method == 'bank_transfer') {
                foreach ($order->items()->get() as $orderItem) {
                    $artwork = Artwork::find($orderItem->artwork_id);
                    if ($artwork) {
                        $artwork->status = 2;
                        $artwork->save();
                    }
                }
            } else {
                $userPayment = UserPayment::find($data['payment_method']);
                $charge_data = [
                    'source' => $userPayment->card_id,
                    'customer' => Auth::user()->customer_id,
                    'currency' => 'EUR',
                    'amount' => $order->amount + $order->shipping,
                    'description' => "Order #" . $order->serial,
                ];
                if ($metadata) {
                    $charge_data['metadata'] = $metadata;
                }
                $charge = $stripe->charges()->create($charge_data);
                if ($charge['status'] !== 'succeeded') {
                    Session::put("order_id", $order->id);
                    return redirect(route('orders.payment.error'));
                }
                $order->status = 2;
                $order->transaction_id = $charge['id'];
                $order->save();
                $this->_cart_instance->complete();

                foreach ($order->items()->get() as $orderItem) {
                    $artwork = Artwork::find($orderItem->artwork_id);
                    if ($artwork) {
                        $artwork->status = 2;
                        $artwork->save();
                    }
                }

                if($this->generateInvoice($order)) {
                    $to = Auth::user()->email;
                    $name = Auth::user()->full_name;
                    $subject = SUBJECT_MAIL_ORDER_COMPLETED;
                    Mail::send('emails.orders.completed', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                        $message->to($to, $name)->subject($subject);
                        $message->attach(storage_path("/invoices/{$order->serial}.pdf"));
                    });
                }

            }
            Session::put("order_id", $order->id);


            $users = User::permission('superadmin-level')->get();
            $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ORDER, 'order', $order);
            Notification::addUsers($notify, $users);
        }
        return redirect(route('orders.thank.you'));
    }

    public function thankYouPage(Request $request)
    {
        $id = $request->session()->get("order_id");
        // se non esiste un id ordine
        if (!$id) {
            // se non sono in produzione
            if($this->_envKind != 'prod'){
                // recupero e mostro comunque l'ultimo ordine
                $order = Order::first();
                if($order){
                   $id = $order->id; 
                } else {
                    die('no order found');
                }
            } else {
                // altrimenti rimando alla home
                return redirect(route('home'));
            }
        }

        Session::remove("order_id");
        $order = Order::find($id);

        
        // Google Tag Manager for Order
        GoogleTagManager::set('pageType', 'TransactionPage');

        GoogleTagManager::set('transactionId', $order->serial); // identificativo di transazione unico (obbligatorio)
        GoogleTagManager::set('transactionAffiliation', 'Kooness'); // Partner o negozio (facoltativo)
        GoogleTagManager::set('transactionTotal', $order->amount); // Valore totale della transazione (obbligatorio)
        // GoogleTagManager::set('transactionTax', $order->amount); // Importo delle imposte per la tranzazione (facoltativo)
        GoogleTagManager::set('transactionShipping', $order->shipping); // Importo di spedizione per la tranzazione (facoltativo)
        $products = []; 
        foreach ($order->items()->get() as $orderItem) {
            $products[] = [
                "sku" => $orderItem->artwork()->first()->reference,
                "name" => $orderItem->artwork()->first()->title,
                "category" => join(', ',$orderItem->artwork()->first()->categories()->whereIsMedium(1)->pluck('name')->toArray()),
                "price" => $orderItem->subtotal,
                "quantity" => $orderItem->quantity,
            ];
        }
        GoogleTagManager::set('transactionProducts', $products); // elenco di articoli acquistati (facoltativo)

        //  RESULT EXAMPLE
        // 'transactionId': '1234',
        // 'transactionAffiliation': 'Moda 1980',
        // 'transactionTotal': 38.26,
        // 'transactionTax': 1.29,
        // 'transactionShipping': 5,
        // 'transactionProducts': [{
        //     'sku': 'DD44',
        //     'name': 'T-Shirt',
        //     'category': 'abbigliamento',
        //     'price': 11.99,
        //     'quantity': 1
        // }


        return view('orders.frontend.thank-you', compact('order'));
    }

    public function thankYouPageWithoutPayment(Request $request)
    {
        $id = $request->session()->get("order_id");
        // se non esiste un id ordine
        if (!$id) {
            // se non sono in produzione
            if($this->_envKind != 'prod'){
                // recupero e mostro comunque l'ultimo ordine
                $order = Order::first();
                if($order){
                    $id = $order->id;
                } else {
                    die('no order found');
                }
            } else {
                // altrimenti rimando alla home
                return redirect(route('home'));
            }
        }

        Session::remove("order_id");
        $order = Order::find($id);

        return view('orders.frontend.thank-you-without-payment', compact('order'));
    }

    public function payPalReturn(Request $request)
    {
        $provider = new ExpressCheckout;
        $token = $request->get("token");
        $payerId = $request->get("PayerID");
        if (!$token || !$payerId)
            return redirect(route('home'));

        $payPalData = [
            'items' => [],
            'invoice_id' => null,
            'invoice_description' => null,
            'return_url' => route('orders.paypal.callback'),
            'cancel_url' => route('orders.cart'),
            'total' => 0
        ];

        $response = $provider->getExpressCheckoutDetails($token);
        Log::channel('paypal')->debug($response);

        if (isset($response['BILLINGAGREEMENTACCEPTEDSTATUS'])) {
            $order = Order::find($response['INVNUM']);
            if ($order) {
                if ($order->status != 1)
                    return redirect(route('home'));
                $orderItems = $order->items()->get();
                foreach ($orderItems as $orderItem) {
                    $payPalData['items'][] = [
                        'name' => Artwork::find($orderItem->artwork_id)->title,
                        'price' => $orderItem->subtotal,
                        'qty' => $orderItem->quantity
                    ];
                }
                if ($order->promo_code) {
                    $promo = PromoCode::find($order->promo_code);
                    if ($promo) {
                        $payPalData['items'][] = [
                            'name' => $promo->name . " " . $promo->value . "%",
                            'price' => -($order->subtotal * $promo->value) / 100,
                            'qty' => 1
                        ];
                    }
                }

                $payPalData['invoice_id'] = $order->id;
                $payPalData['shipping'] = $order->shipping;
                $payPalData['invoice_description'] = "Order #" . $order->serial;
                $payPalData['subtotal'] = $order->amount - $order->tax;
                $payPalData['total'] = $order->amount;

                $payment_status = $provider->doExpressCheckoutPayment($payPalData, $token, $payerId);
                Log::channel('paypal')->debug($payment_status);
                $status = $payment_status['ACK'];
                if (!strcasecmp($status, 'Success')) {

                    $order->status = 2;
                    $order->transaction_id = $payment_status['PAYMENTINFO_0_TRANSACTIONID'];
                    $order->save();
                    $this->_cart_instance->complete();

                    foreach ($order->items()->get() as $orderItem) {
                        $artwork = Artwork::find($orderItem->artwork_id);
                        if ($artwork) {
                            $artwork->status = 2;
                            $artwork->save();
                        }
                    }

                    if($this->generateInvoice($order)) {

                        if (Auth::user()) {
                            $to = Auth::user()->email;
                            $name = Auth::user()->full_name;
                        } else {
                            $user = $order->user()->first();
                            $to = $user->email;
                            $name = $user->full_name;
                        }
                        $subject = SUBJECT_MAIL_ORDER_COMPLETED;
                        Mail::send('emails.orders.completed', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                            $message->to($to, $name)->subject($subject);
                            $message->attach(storage_path("/invoices/{$order->serial}.pdf"));
                        });
                    }

                    Session::put("order_id", $order->id);
                    return redirect(route('orders.thank.you'));
                } else {
                    Session::put("order_id", $order->id);
                    return redirect(route('orders.payment.error'));
                }
            } else {
                return redirect(route('home'));
            }
        } else {
            return redirect(route('home'));
        }
    }

    public function paymentError(Request $request)
    {
        $id = $request->session()->get("order_id");
        if (!$id) {
            return redirect(route('home'));
        }
        Session::remove("order_id");
        $order = Order::find($id);
        return view('orders.frontend.payment-error', compact('order'));
    }

    public function generateInvoice($order){
        if(File::exists(storage_path("/config/invoices_" . date('Y') .".json")))
            $json = file_get_contents(storage_path("/config/invoices_" . date('Y') .".json"));
        else{
            $json = json_encode(['current_number' => 1, "year" => date('Y')]);
            file_put_contents(storage_path("/config/invoices_" . date('Y') .".json"), $json);
        }
        $data = json_decode($json);
        $pdf = PDF::loadView('pdf.invoice', ['order' => $order, 'invoice_number' => $data->current_number]);
        $pdf->save(storage_path("/invoices/{$order->serial}.pdf"));
        $data->current_number++;
        $json = json_encode($data);
        file_put_contents(storage_path("/config/invoices_" . date('Y') .".json"), $json);
        return true;
    }

    public function payOrder($token){
        $order = Order::whereToken($token)->first();
        if($order){
            $user = $order->user()->first();
            $payments = $user->payments()->get();
            $promo = $order->promo()->first();
            if($user){
                if(Auth::user() && Auth::user()->id != $user->id){
                    $this->guard()->logout();
                }
                $this->guard()->login($user);
                return view('orders.frontend.pay-checkout', compact('order', 'payments', 'promo'));
            }
        } else{
            return redirect()->route('home');
        }
    }

    public function checkoutPay(Request $request){
        if ($request->method() == 'POST' && $request->has('order_token') && $request->get('order_token')) {
            $order = Order::whereToken($request->get('order_token'))->first();
            if($order){
                $user = $order->user()->first();
                $provider = new ExpressCheckout;
                $stripe = Stripe::make(config('services.stripe.secret'));
                $data = Input::all();

                $payPalData = [
                    'items' => [],
                    'invoice_id' => null,
                    'invoice_description' => null,
                    'return_url' => route('orders.paypal.callback'),
                    'cancel_url' => route('orders.cart'),
                    'discount_amount' => 0,
                    'total' => 0
                ];

                $metadata = null;

                $order->payment_method = ($data['payment_method'] != 'paypal' && $data['payment_method'] != 'bank_transfer') ? 'stripe' : $data['payment_method'];
                $order->token = null;
                $order->save();
                if ($order->promo_code) {
                    $promo = $order->promo()->first();
                    if($promo){
                        $metadata = [
                            "coupon_code" => $promo->name,
                            "coupon_discount" => $promo->value . "%"
                        ];
                    }
                }

                if($order->payment_method == 'paypal') {
                    foreach ($order->items()->get() as $orderItem) {
                        $payPalData['items'][] = [
                            'name' => Artwork::find($orderItem->artwork_id)->title,
                            'price' => $orderItem->subtotal,
                            'qty' => $orderItem->quantity
                        ];
                        if ($promo) {
                            $payPalData['items'][] = [
                                'name' => $promo->name . " " . $promo->value . "%",
                                'price' => -($order->subtotal * $promo->value) / 100,
                                'qty' => $orderItem->quantity
                            ];
                        }
                    }
                }

//              TODO::vedere se cambiare mail
                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_ORDER_RECEIVED;
                Mail::send('emails.orders.received', ["order" => $order], function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });

//              TODO::vedere se cambiare mail
                $to = env('MAIL_TO_ADMIN_ADDRESS', 'm.fusco@notomia.com');
                $name = 'Kooness Order';
                $subject = SUBJECT_MAIL_ORDER_RECEIVED_ADMIN;
                Mail::send('emails.orders.received-admin', ["order" => $order], function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });

                if ($order->payment_method == 'paypal') {
                    $payPalData['invoice_id'] = $order->id;
                    $payPalData['shipping'] = $order->shipping;
                    $payPalData['invoice_description'] = "Order #" . $order->serial;
                    $payPalData['subtotal'] = $order->amount - $order->tax;
                    $payPalData['total'] = $order->amount;
                    try {
                        $response = $provider->setExpressCheckout($payPalData);
                        return redirect($response['paypal_link']);
                    } catch (\Exception $e) {
                    }
                } else if ($order->payment_method == 'bank_transfer') {
                    foreach ($order->items()->get() as $orderItem) {
                        $artwork = Artwork::find($orderItem->artwork_id);
                        if ($artwork) {
                            $artwork->status = 2;
                            $artwork->save();
                        }
                    }
                } else {
                    $userPayment = UserPayment::find($data['payment_method']);
                    $charge_data = [
                        'source' => $userPayment->card_id,
                        'customer' => $user->customer_id,
                        'currency' => 'EUR',
                        'amount' => $order->amount + $order->shipping,
                        'description' => "Order #" . $order->serial,
                    ];
                    if ($metadata) {
                        $charge_data['metadata'] = $metadata;
                    }
                    $charge = $stripe->charges()->create($charge_data);
                    if ($charge['status'] !== 'succeeded') {
                        Session::put("order_id", $order->id);
                        return redirect(route('orders.payment.error'));
                    }
                    $order->status = 2;
                    $order->transaction_id = $charge['id'];
                    $order->save();
                    $order->cart();

                    if($this->generateInvoice($order)) {
                        $to = Auth::user()->email;
                        $name = Auth::user()->full_name;
                        $subject = SUBJECT_MAIL_ORDER_COMPLETED;
                        Mail::send('emails.orders.completed', ["order" => $order], function ($message) use ($to, $name, $subject, $order) {
                            $message->to($to, $name)->subject($subject);
                            $message->attach(storage_path("/invoices/{$order->serial}.pdf"));
                        });
                    }

                }
                Session::put("order_id", $order->id);
                return redirect(route('orders.thank.you'));
            }
        }
        return redirect(route('home'));
    }
}
