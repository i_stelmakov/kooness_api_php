@extends('layouts.admin')
@section('title', 'Admin | Create Artwork')
@section('page-header', 'Admin - Create Artwork')

@section('content')
    @include('artworks.admin.form', ['action' => route('admin.artworks.store'), 'method' => 'POST'])
@endsection