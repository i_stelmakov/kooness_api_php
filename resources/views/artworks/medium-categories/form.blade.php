<style>
    form .error {
        color: #ff0000;
    }
</style>
<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="" data-referer="">
            {{ method_field($method) }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Association
                </div>
                <div class="box-body">
                    <div class="col-md-12  col-xs-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input id="generated_url" class="form-control" type="text" data-prefix="{{ url('') }}/artworks/" value="" disabled>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="media">Medium (*)</label>
                            <select class="select2 form-control media-category" name="media">
                                @foreach($medium as $media)
                                    <option data-slug="{{ $media->slug }}" value="{{ $media->id }}" {{ ($media->id == $categoryMedium->medium_id)? 'selected="selected"' : '' }}>{{ $media->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="category">Category (*)</label>
                            <select class="select2 form-control media-category" name="category">
                                @foreach($categories as $category)
                                    <option data-slug="{{ $category->slug }}" value="{{ $category->id }}" {{ ($category->id == $categoryMedium->category_id)? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control ck-editor" placeholder="Description" name="description" rows="5">{{ $categoryMedium->{'description'} }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    SEO Management
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_title">SEO - title</label>
                                <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $categoryMedium->{'meta_title'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_keywords">SEO - keywords</label>
                                <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $categoryMedium->{'meta_keywords'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="h1">SEO - H1</label>
                                <input class="form-control"
                                       name="h1" type="text"
                                       placeholder="SEO - H1"
                                       id="h1"
                                       value="{{ $categoryMedium->{'h1'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="canonical">SEO - Canonical Url</label>
                                <input class="form-control"
                                       name="canonical" type="text"
                                       placeholder="SEO - Canonical Url"
                                       id="canonical"
                                       value="{{ $categoryMedium->{'canonical'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meta_description">SEO - Description</label>
                                <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $categoryMedium->{'meta_description'} }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>