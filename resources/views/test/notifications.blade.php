{{-- Test - Intro --}}

@extends('layouts.app')

@section('title')
Test - Grid
@endsection

@section('body-class')
test-grid
@endsection

@section('scripts')
@endsection

@section('styles')
    <style>
        .sections * {
            box-shadow: 0 0 0 1px aqua;
        }
        .inner-element {
            background: rgba(0,0,255,0.2);
            display: block;
            height: 100%;
            width: 100%;
            position: absolute;
            text-align: center;
        }
    </style>
@endsection

@section('content')
<div class="container boxed-container">
<h1>Test Grid ☢</h1>️

    <div class="container col-container-with-offset-and-margin">
        <div class="one-fifths-col-with-margin">
            <div class="inner-element">Element</div>
        </div>
        <div class=" one-fifths-col-with-margin">ELEMENT</div>
        <div class=" one-fifths-col-with-margin">ELEMENT</div>
        <div class=" one-fifths-col-with-margin">ELEMENT</div>
        <div class=" one-fifths-col-with-margin">ELEMENT</div>
    </div>
</div>
@endsection