<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairsGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fairs_galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fair_id')->unsigned();
            $table->integer('gallery_id')->unsigned();
            $table->timestamps();

            $table->foreign('fair_id')
                ->references('id')
                ->on('fairs')
                ->onDelete('CASCADE');

            $table->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fairs_galleries');
    }
}
