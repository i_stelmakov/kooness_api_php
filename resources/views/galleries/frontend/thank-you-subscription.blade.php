@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Thank you for the request')

@section('body-class', 'thank-you')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('title')

@section('content')
    <div class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1 class="thanks-title">{{ ($seo_data && $seo_data->h1)? $seo_data->h1: 'Thank You' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="container-vertical-padding-bottom">
            <div class="container boxed-container">
                <i class="fa fa-check main-color-txt thanks-icon"></i>
                <div class="default-sheet-row">
                    <p>
                    <h3>Thank you for your interest in Kooness!</h3></p>
                </div>
                <div class="default-sheet-row full-col">
                    <p>Your subscription request has been successfully sent to us and we'll get in touch with you as soon as possible.</p>
                </div>
                <div class="default-sheet-row full-col">
                    <p>Go to <a href="{{ route('galleries.intro') }}"><u>our galleries selection.</u></a></p>
                </div>
            </div>
        </section>
    </div>
@endsection


