@extends('layouts.admin')
@section('title', 'Edit HomePage Layout')
@section('page-header', 'Admin - Edit HomePage Layout')

@section('body-class')
    home-layout
@endsection

@section('content')
    <form id="form-link" class="ajax-form widget-repeater-link" role="form" method="POST" action="{{ route('admin.homepage.links.save') }}">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Header Link
            </div>
            <div class="box-body">
                <ul data-repeater-list="widget-link" class="widget-repeater-link-container">
                    @if(count($links))
                        @foreach($links as $link)
                            @include('home.admin.partials.widget-link', ['link' => $link])
                        @endforeach
                    @endif
                </ul>
                <input data-repeater-create class="btn btn-primary" type="button" value="Add"/>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-footer">
                <div class="col-md-12">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form class="ajax-form" role="form" method="POST" action="{{ route('admin.homepage.sections.save') }}">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Home page Section
            </div>
            <div class="box-body">
                <div class="row home-section">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block1">
                                    <input type="hidden" class="pointer" value="block1">
                                    <input type="hidden" class="input-section" name="sections[block1]" value="{{ $sections['block1'] }}">
                                    <input type="hidden" class="input-data" value="{}">
                                    <h4 class="title">Title</h4><br>
                                    <ul>
                                        <li>Element 1</li>
                                        <li>Element 2</li>
                                        <li>Element 3</li>
                                        <li>Element 4</li>
                                        <li>Element 5</li>
                                    </ul>
                                    <button class="block-edit btn btn-xs">
                                        <i class="fa fa-pencil-square-o"></i> Edit section
                                    </button>
                                    <div class="block-loader">
                                        <div class="block-loader__icon-container">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <span class="caption-home-sections">Caption</span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="block block2">
                                    <input type="hidden" class="pointer" value="block2">
                                    <input type="hidden" class="input-section" name="sections[block2]" value="{{ $sections['block2'] }}">
                                    <input type="hidden" class="input-data" value="{}">
                                    <h4 class="title">Title</h4><br>
                                    <ul>
                                        <li>Element 1</li>
                                        <li>Element 2</li>
                                        <li>Element 3</li>
                                        <li>Element 4</li>
                                        <li>Element 5</li>
                                    </ul>
                                    <button class="block-edit btn btn-xs">
                                        <i class="fa fa-pencil-square-o"></i> Edit section
                                    </button>
                                    <div class="block-loader">
                                        <div class="block-loader__icon-container">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <span class="caption-home-sections">Caption</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block block3">
                                    <input type="hidden" class="pointer" value="block3">
                                    <input type="hidden" class="input-section" name="sections[block3]" value="{{ $sections['block3'] }}">
                                    <input type="hidden" class="input-data" value="{}">
                                    <h4 class="title">Title</h4><br>
                                    <ul>
                                        <li>Element 1</li>
                                        <li>Element 2</li>
                                        <li>Element 3</li>
                                        <li>Element 4</li>
                                        <li>Element 5</li>
                                    </ul>
                                    <button class="block-edit btn btn-xs">
                                        <i class="fa fa-pencil-square-o"></i> Edit section
                                    </button>
                                    <div class="block-loader">
                                        <div class="block-loader__icon-container">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <span class="caption-home-sections">Caption</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block block4">
                                    <input type="hidden" class="pointer" value="block4">
                                    <input type="hidden" class="input-section" name="sections[block4]" value="{{ $sections['block4'] }}">
                                    <input type="hidden" class="input-data" value="{}">
                                    <h4 class="title">Title</h4><br>
                                    <ul>
                                        <li>Element 1</li>
                                        <li>Element 2</li>
                                        <li>Element 3</li>
                                        <li>Element 4</li>
                                        <li>Element 5</li>
                                    </ul>
                                    <button class="block-edit btn btn-xs">
                                        <i class="fa fa-pencil-square-o"></i> Edit section
                                    </button>
                                    <div class="block-loader">
                                        <div class="block-loader__icon-container">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <span class="caption-home-sections">Caption</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="block block5">
                                    <input type="hidden" class="pointer" value="block5">
                                    <input type="hidden" class="input-section" name="sections[block5]" value="{{ $sections['block5'] }}">
                                    <input type="hidden" class="input-data" value="{}">
                                    <h4 class="title">Title</h4><br>
                                    <ul>
                                        <li>Element 1</li>
                                        <li>Element 2</li>
                                        <li>Element 3</li>
                                        <li>Element 4</li>
                                        <li>Element 5</li>
                                    </ul>
                                    <button class="block-edit btn btn-xs btn-default">
                                        <i class="fa fa-pencil-square-o"></i> Edit section
                                    </button>
                                    <div class="block-loader">
                                        <div class="block-loader__icon-container">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <span class="caption-home-sections">Caption</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input class="btn btn-primary" type="submit" value="Save"/>
            </div>
        </div>
    </form>
    <form class="ajax-form widget-repeater" role="form" method="POST" action="{{ route('admin.homepage.layout.save') }}" data-attachment="validate" data-id="" data-referer="">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Home page Widgets
            </div>
            <div class="box-body">
                @csrf
                <input type="hidden" name="label" value="home">
                <ul data-repeater-list="widget" class="widget-repeater-container">
                    @if(count($widgets))
                        @foreach($widgets as $widget)
                            @include('home.admin.partials.widget', ['widget' => $widget])
                        @endforeach
                    @endif
                </ul>
                <input data-repeater-create class="btn btn-primary" type="button" value="Add"/>
            </div>
        </div>
        <div class="box box-primary box-solid">
            <div class="box-footer">
                <div class="col-md-12">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @include('home.admin.partials.modal-section')
@endsection