<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToArtworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artworks', function (Blueprint $table) {
            $table->boolean('show_price')->default(0)->after('available_in_fair');
            $table->float('taxable')->default(0.00)->after('price');
            $table->float('vat')->default(0.00)->after('taxable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artworks', function (Blueprint $table) {
            $table->dropColumn('show_price');
            $table->dropColumn('taxable');
            $table->dropColumn('vat');
        });
    }
}
