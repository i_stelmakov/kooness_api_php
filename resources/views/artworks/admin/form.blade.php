@if( roleCheck('superadmin|admin|gallerist|artist|seo') )
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $artwork->id }}" data-referer="artwork">
        {{ method_field($method) }}
        <div class="col-md-6 col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        {{-- @if(!auth()->user()->hasRole('seo')) --}}
            {{-- Associations --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Associations
                    </div>
                    <div class="box-body row">

                        @if(!roleCheck('seo'))
                            <div class="col-md-6 col-xs-12
                                {{ ( roleCheck('artist') ) ? 'hide' : '' }}
                            ">
                                <div class="form-group">
                                    <label for="artist_id">Artist (*)</label>
                                    <select class="select2 form-control artworks-slug-trigger" name="artist_id" required>
                                        @foreach($artists as $artist)
                                            <option value="{{ $artist->id }}" {{ ($artist->id == $artwork->artist_id)? 'selected="selected"' : '' }}> {{ $artist->first_name }} {{  $artist->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12
                                {{ ( roleCheck('gallerist') ) ? 'hide' : '' }}
                            ">
                                <div class="form-group">
                                    <label for="gallery_id">Gallery (*)</label>
                                    <select class="select2 form-control" name="gallery_id" required>
                                        @foreach($galleries as $gallery)
                                            <option value="{{ $gallery->id }}" {{ ($gallery->id == $artwork->gallery_id)? 'selected="selected"' : '' }}>{{ $gallery->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="medium">Medium</label>
                                <select class="select2 form-control artworks-slug-trigger" name="medium[]" multiple="multiple">
                                    @foreach($medium as $media)
                                        <option value="{{ $media->id }}" {{ (in_array($media->id, $artwork->category_ids))? 'selected="selected"' : '' }}>{{ $media->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="categories">Categories (*)</label>
                                <select
                                    class="select2 form-control required validatemultiple"
                                    multiple="multiple"
                                    name="categories[]"
                                    required
                                >
                                <option disabled value="">Choose one or more Categories</option>
                                @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ (in_array($category->id, $artwork->category_ids))? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        @if(!roleCheck('seo'))
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="tags">Tags</label>
                                    <select class="select2 form-control" name="tags[]" multiple="multiple">
                                        @foreach($tags as $tag)
                                            <option value="{{ $tag->id }}" {{ (in_array($tag->id, $artwork->tag_ids))? 'selected="selected"' : '' }}>{{ $tag->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        {{-- @endif --}}
    
        {{-- General Information --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    General Information
                </div>
                <div class="box-body row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input 
                                id="generated_url" 
                                class="form-control" 
                                type="text" 
                                data-prefix="{{ url('') }}/artworks/" 
                                value="{{ ($artwork->{'slug'})? url('') . '/artworks/' . $artwork->{'slug'} : '' }}"
                                disabled
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="title">Title (*)</label>
                            <input 
                                id="title" 
                                class="form-control artworks-slug-trigger"
                                type="text" 
                                placeholder="Title" 
                                name="title" 
                                value="{{ old('title', $artwork->{'title'}) }}" 
                                {{--data-slug="#slug" --}}
                                {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="slug">Slug (*)</label>
                            <input 
                                id="slug" 
                                class="form-control" 
                                type="text" 
                                placeholder="Slug" 
                                name="slug" 
                                value="{{ old('slug', $artwork->{'slug'}) }}"
                                data-url="#generated_url"
                                {{ ( roleCheck('seo|gallerist|artist') ) ? 'readonly' : 'required' }}
                            >
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="from_the_series">From the series</label>
                                <input id="from_the_series" class="form-control" type="text" placeholder="From the series" name="from_the_series" value="{{ old('from_the_series', $artwork->{'from_the_series'}) }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="year">Year (*)</label>
                                <input id="year" class="form-control date-year year-mask" type="text" placeholder="YYYY" name="year" value="{{ old('year', $artwork->{'year'}) }}" required>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="title">Width(*) -
                                    <small>cm</small>
                                </label>
                                <input id="width" class="form-control" type="number" placeholder="Width" name="width" value="{{ old('width', $artwork->{'width'}) }}" required>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="height">Height(*) -
                                    <small>cm</small>
                                </label>
                                <input id="height" class="form-control" type="number" placeholder="Height" name="height" value="{{ old('height', $artwork->{'height'}) }}" required>
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="depth">Depth -
                                    <small>cm</small>
                                </label>
                                <input id="depth" class="form-control" type="number" placeholder="Depth" name="depth" value="{{ old('depth', $artwork->{'depth'}) }}">
                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="form-group">
                                <label for="weight">Weight -
                                    <small>kg</small>
                                </label>
                                <input id="weight" class="form-control" type="number" placeholder="Weight" name="weight" value="{{ old('weight', $artwork->{'weight'}) }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="box box-default box-solid">
                                <div class="box-header with-border">
                                    Features
                                </div>
                                <div class="box-body row">
                                    <div class="col-md-6 col-xs-12">
                                        <div>
                                            <label><input class="iCheck-input" type="checkbox" name="single_piece" {{ ($artwork->{'single_piece'}? 'checked="checked"' : '') }}>
                                                <i class="fa fa-puzzle-piece" aria-hidden="true"></i> Single Piece</label>
                                        </div>
                                        <div>
                                            <label><input class="iCheck-input" type="checkbox" name="signed" {{ ($artwork->{'signed'}? 'checked="checked"' : '') }}>
                                                <i class="fa fa-pencil" aria-hidden="true"></i> Signed</label>
                                        </div>
                                        <div>
                                            <label><input class="iCheck-input" type="checkbox" name="dated" {{ ($artwork->{'dated'}? 'checked="checked"' : '') }}>
                                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i> Dated</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div>
                                            <label><input class="iCheck-input" type="checkbox" name="titled" {{ ($artwork->{'titled'}? 'checked="checked"' : '') }}>
                                                <i class="fa fa-tag" aria-hidden="true"></i> Titled</label>
                                        </div>
                                        <div>
                                            <label><input class="iCheck-input" type="checkbox" name="framed" {{ ($artwork->{'framed'}? 'checked="checked"' : '') }}>
                                                <i class="fa fa-object-group" aria-hidden="true"></i> Framed</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="about_the_work">About the work</label>
                                <textarea id="about_the_work" class="form-control ck-editor" placeholder="About the work" name="about_the_work" rows="3">{{ $artwork->{'about_the_work'} }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="news">Bibliography</label>
                                <textarea id="news" class="form-control ck-editor" placeholder="News" name="news" rows="3">{{ $artwork->{'news'} }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" id="status" name="status">
                                    @if( !roleCheck('gallerist|artist') )
                                        <option value="1" {{ ($artwork->{'status'} == 1)  ? 'selected="selected"' : ''  }}>Visible</option>
                                    @endif
                                    <option value="4" {{ ($artwork->{'status'} == 4)  ? 'selected="selected"' : ''  }}>Ready to be published</option>
                                    @if(!roleCheck('artist') ))
                                        <option value="2" {{ ($artwork->{'status'} == 2)  ? 'selected="selected"' : ''  }}>Sold (in the gallery)</option>
                                    @endif
                                    <option value="3" {{ ($artwork->{'status'} == 3)  ? 'selected="selected"' : ''  }}>Suspended</option>
                                </select>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        @if( !roleCheck('seo|artist')  )
            {{-- Fair --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Fair
                    </div>
                    <div class="box-body row">
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="available_in_fair">Available on fair (*)</label>
                                <select class="form-control" id="available_in_fair" name="available_in_fair">
                                    <option value="1" {{ ($artwork->{'available_in_fair'})  ? 'selected="selected"' : ''  }}>
                                        Yes
                                    </option>
                                    <option value="0" {{ (!$artwork->{'available_in_fair'})  ? 'selected="selected"' : ''  }}>
                                        No
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            <div class="form-group">
                                <label for="show_price">Show Price</label>
                                <select class="form-control" id="show_price" name="show_price" {{ !$artwork->{'available_in_fair'}? 'disabled="disabled"':''}}>
                                    <option value="1" {{ ($artwork->{'show_price'})  ? 'selected="selected"' : ''  }}>
                                        Yes
                                    </option>
                                    <option value="0" {{ (!$artwork->{'show_price'})  ? 'selected="selected"' : ''  }}>
                                        No
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <div class="form-group">
                                <label for="tags">Fairs</label>
                                <select class="select2 form-control" name="fairs[]" multiple="multiple" id="fairs">
                                    @foreach($fairs as $fair)
                                        <option value="{{ $fair->id }}" {{ (in_array($fair->id, $artwork->fair_ids))? 'selected="selected"' : '' }}>{{ $fair->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(!roleCheck('seo'))
            {{-- Main Images --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Images
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Pictures <br> </label>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-12">
                                        @if(!$artwork->images()->whereLabel(1)->first())
                                            <img src="/images/placeholder-square-box.jpg" class="img-responsive img-preview1">
                                        @else
                                            <img src="{{ asset($artwork->images()->whereLabel(1)->first()->url) }}" class="img-responsive img-preview1">
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Upload new image… <input type="file" class="img-uploader" data-label="1" data-preview=".img-preview1" data-maxWidth="2048" data-maxHeight="2048" data-cropper="false">
                                            </span>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-12">
                                        @if(!$artwork->images()->whereLabel(2)->first())
                                            <img src="/images/placeholder-square-box.jpg" class="img-responsive img-preview2">
                                        @else
                                            <img src="{{ asset($artwork->images()->whereLabel(2)->first()->url) }}" class="img-responsive img-preview2">
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Upload new image… <input type="file" class="img-uploader" data-label="2" data-preview=".img-preview2" data-maxWidth="2048" data-maxHeight="2048" data-cropper="false">
                                            </span>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-12">
                                        @if(!$artwork->images()->whereLabel(3)->first())
                                            <img src="/images/placeholder-square-box.jpg" class="img-responsive img-preview3">
                                        @else
                                            <img src="{{ asset($artwork->images()->whereLabel(3)->first()->url) }}" class="img-responsive img-preview3">
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Upload new image… <input type="file" class="img-uploader" data-label="3" data-preview=".img-preview3" data-maxWidth="2048" data-maxHeight="2048" data-cropper="false">
                                            </span>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-12">
                                        @if(!$artwork->images()->whereLabel(4)->first())
                                            <img src="/images/placeholder-square-box.jpg" class="img-responsive img-preview4">
                                        @else
                                            <img src="{{ asset($artwork->images()->whereLabel(4)->first()->url) }}" class="img-responsive img-preview4">
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Upload new image… <input type="file" class="img-uploader" data-label="4" data-preview=".img-preview4" data-maxWidth="2048" data-maxHeight="2048" data-cropper="false">
                                            </span>
                                        </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if( !roleCheck('seo|gallerist|artist') )
            {{-- Thumbnails Images --}}
            @include('partials.images', ['item' => $artwork, 'square_box_active' => true, 'wide_box_active' => false,'vertical_box_active' => true ])
        @endif

        @if( !roleCheck('seo|artist') )
            {{-- Selling Information --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Selling Information
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6  col-xs-12">
                                <div class="form-group">
                                    <label for="reference">Reference ID (*)
                                        <small class="alert-danger">this field is only for reference, it will be automatically generated by the system only after the successfull submission of the form</small>
                                    </label>
                                    <input disabled id="reference" class="form-control" type="text" placeholder="Reference" name="reference" value="{{ old('reference', $artwork->{'reference'}) }}" required>
                                </div>
                            </div>
                            <div class="col-md-6  col-xs-12">
                                <div class="form-group">
                                    <label for="price">Final price (*)</label>
                                    <input id="price" class="form-control" type="number" step="0.01" placeholder="Price" name="price" value="{{ old('price', $artwork->{'price'}) }}" required>
                                </div>
                            </div>
                            <div class="col-md-6  col-xs-12">
                                <div class="form-group">
                                    <label for="taxable">Price (exc. VAT)</label>
                                    <input id="taxable" class="form-control" type="number" step="0.01" placeholder="Price (exc. VAT)" name="taxable" value="{{ old('taxable', $artwork->{'taxable'}) }}">
                                </div>
                            </div>
                            <div class="col-md-6  col-xs-12">
                                <div class="form-group">
                                    <label for="vat">VAT</label>
                                    <input id="vat" class="form-control" type="number" step="0.01" placeholder="Vat" name="vat" value="{{ old('vat', $artwork->{'vat'}) }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(!roleCheck('seo'))
            {{-- Artwork's current location --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Artwork's current location
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label for="current_city">City</label>
                                    <input id="current_city" class="form-control" type="text" placeholder="Current city" name="current_city" value="{{ old('current_city', $artwork->{'current_city'}) }}">
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label for="current_zip">Current zip</label>
                                    <input id="current_zip" class="form-control" type="text" placeholder="Current zip" name="current_zip" value="{{ old('current_zip', $artwork->{'current_zip'}) }}">
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label for="current_country">Country</label>
                                    <select class="select2 form-control" name="current_country">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}" {{ ($country->id == $artwork->current_country)? 'selected="selected"' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if( !roleCheck('gallerist|artist|seo') )
        {{-- SEO Management --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        SEO Management
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_title">SEO - title</label>
                                    <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $artwork->{'meta_title'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_keywords">SEO - keywords</label>
                                    <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $artwork->{'meta_keywords'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="h1">SEO - H1</label>
                                    <input class="form-control" name="h1" type="text" placeholder="SEO - H1" id="h1" value="{{ $artwork->{'h1'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="canonical">SEO - Canonical Url</label>
                                    <input class="form-control" name="canonical" type="text" placeholder="SEO - Canonical Url" id="canonical" value="{{ $artwork->{'canonical'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_description">SEO - Description</label>
                                    <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $artwork->{'meta_description'} }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Form Footer & Submit --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endif
