@extends('layouts.admin')
@section('title', "Admin | Edit Promo Code `{$code->name}`")
@section('page-header', "Admin - Edit Promo Code `{$code->name}`")

@section('content')
    @include('promo-codes.admin.form', ['action' => route('admin.promo-codes.update', [$code->id]), 'method' => 'PATCH'])
@endsection