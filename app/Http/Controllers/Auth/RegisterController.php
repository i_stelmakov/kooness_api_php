<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artwork;
use App\Models\User;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Api4Dem\Api4DemDataParser;
use App\Services\MailUp;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Mail;

class RegisterController extends FrontendExtendedController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        if(Session::has('from_checkout')){
            View::share('from_checkout', true);
            Session::remove('from_checkout');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|regex:/^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).*$/|confirmed',
            'honey_name-register' => 'honeypot',
            'honey_time-register' => 'required|honeytime:5'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        // dd($data);
        $activation_code = hash_hmac('sha256', str_random(40), config('app.key'));
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => 1,
            'activation_code' => $activation_code
        ]);

        $user->assignRole('user');

        if(isset($data['from_checkout'])){
            Session::put("postLogin", "checkout");
        }

        if(isset($data['from_offer_id'])){
            Session::put("artworksIdReferer", base64_decode($data['from_offer_id']));
        }

        if(isset($data['from_available_id'])){
            Session::put("artworksIdReferer", base64_decode($data['from_available_id']));
        }

        $to = $user->email;
        $name = $user->full_name;
        $subject = SUBJECT_MAIL_REGISTRATION_USER_REGISTERED;
        // $link = url("activate/$activation_code");
        $extras = array(
           'user' => $user,
        //    'activation_url' => $link
        );

       Mail::send('emails.auth.registered', $extras, function ($message) use ($to, $name, $subject) {
           $message->to($to, $name)->subject($subject);
       });

        if(isset($data['newsletter'])){

            $mailUp = new MailUp();
            if($mailUp->subscribeUser($user)){
                $user->newsletter = 1;
                $user->save();

                // 4Dem userSubscribe
                $api4Dem = new Api4Dem();
                $api4Dem->userSubscribe($user);


            }
        }

        // 4Dem userSave
        $api4Dem = new Api4Dem();
        $api4Dem->userSave($user);

        return $user;
    }

    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function verifyUser($activation_code = null)
    {
        if (!$activation_code)
            return redirect()->route('register');

        $verifyUser = User::where('activation_code', $activation_code)->first();
        if (isset($verifyUser)) {
            $verifyUser->activation_code = null;
            $verifyUser->status = 1;
            if ($verifyUser->save()) {
                return redirect()->route('register');
            } else {
                return redirect()->route('register');
            }
        } else {
            return redirect()->route('register');
        }
    }

    protected function registered(Request $request, $user)
    {
        if(Session::has('postLogin')){
            Session::remove('postLogin');
            $this->guard()->login($user);
            return redirect()->route('orders.view.checkout');
        }

        if(Session::has('artworksIdReferer')){
            $id = Session::get('artworksIdReferer');
            Session::remove('artworksIdReferer');
            $artwork = Artwork::find($id);
            if($artwork){
                $this->guard()->login($user);
                return redirect()->route('artworks.single', [$artwork->slug]);
            }
        }

        return redirect()->route('register')->with('registered', 'success');
    }
}
