<?php

namespace App\Http\Controllers\Fairs;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Fair;
use App\Models\FairsGallery;
use App\Models\Gallery;
use App\Models\LayoutPage;
use Request;
use Validator;

class FrontendController extends FrontendExtendedController
{
    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Artists Intro
    public function intro()
    {
        $slides = [];
        $widgets = [];
        $layout = LayoutPage::whereLabel('fairs')->first();
        if ($layout) {
            $slider = $layout->slider()->first();
            if ($slider) {
                $contents = json_decode($slider->contents);
                foreach ($contents as $content) {
                    $artwork = Fair::find($content);
                    if ($artwork) {
                        $slides[] = $artwork;
                    }
                }
            }
            $result = $layout->widgets()->orderBy('order', "ASC")->get();
            foreach ($result as $row) {
                $tmp = [
                    'title' => $row->title,
                    'subtitle' => $row->subtitle,
                    'description' => $row->description,
                    'section' => $row->section,
                    'letter' => 'a',
                    'link' => null,
                    'link_title' => "View all",
                    'template' => $row->template,
                    'items' => []
                ];
                $items = json_decode($row->items);
                foreach ($items as $item) {
                    if ($row->section == 'artworks') {
                        $rows = Artwork::find($item);
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artworks";
                    } else if ($row->section == 'artists') {
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured artists";
                        $rows = Artist::find($item);
                    } else if ($row->section == 'galleries') {
                        $tmp['letter'] = 'g';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All featured galleries";
                        $rows = Gallery::find($item);
                    } else if ($row->section == 'fairs') {
                        $tmp['letter'] = 'f';
                        if ($row->template == 'tplB')
                            $tmp['link_title'] = "All Fairs";
                        $rows = Fair::find($item);
                    }
                    $tmp['link'] =$row->link;
                    if (isset($rows) && $rows) {
                        $tmp['items'][] = $rows;
                    }
                }
                $widgets[] = $tmp;
            }
        }
        return view('fairs.frontend.intro', compact('slides', 'widgets'));
    }
    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Archive All
    public function archive()
    {
        $categories = FairsGallery::all();
        $fairCount = Fair::all()->count();
        $fairs = Fair::whereStatus(1)->orderBy('created_at', "DESC")->paginate(18);
        return view('fairs.frontend.archive', compact('fairs', 'categories', 'fairCount'));
    }


    // Fairs Single
    public function single($slug_name)
    {
        $fair = Fair::whereSlug($slug_name)->first(); // select * from fairs (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)
        if ($fair) {
            $galleries = $fair->galleries()->whereStatus(1)->orderByRaw("RAND()")->limit(3)->get();
            $artists = [];
            $artworks = $fair->artworks()->whereRaw('status IN (1,2)')->orderByRaw("RAND()")->limit(3)->get();

            $other_artworks = $fair->artworks()->whereRaw('status IN (1,2)')->orderByRaw("RAND()")->limit(3)->get();
            foreach ($other_artworks as $other_artwork) {
                $artists[] = $other_artwork->artist()->first();
            }
            return view('fairs.frontend.single', compact('fair', 'slug_name', 'galleries', 'artists','artworks'));
        } else {
            return redirect(route('fairs.archive.all'));
        }
    }

}
