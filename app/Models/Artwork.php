<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Artwork extends Model
{
    protected $guarded = [];

    protected $appends = ['tag_ids', 'category_ids', 'fair_ids', 'gallery_name', 'artist_name', 'pretty_price'];

    protected $fillable = [
        'artist_id',
        'gallery_id',
        'created_by',
        'reference',
        'title',
        'slug',
        'from_the_series', 
        'year',
        'width',
        'height',
        'depth',
        'weight',
        'single_piece',
        'signed',
        'dated',
        'titled',
        'framed',
        'available_in_fair',
        'show_price',
        'about_the_work',
        'news',
        'status',
        'price',
        'taxable',
        'vat',
        'current_city',
        'current_zip',
        'current_country',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'artworks_tags', 'artwork_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\ArtworksCategory', 'artworks_artworks_categories', 'artwork_id', 'category_id');
    }


    public function medium()
    {
        return $this->belongsToMany('App\Models\ArtworksCategory', 'artworks_artworks_categories', 'artwork_id', 'category_id');
    }

    public function fairs()
    {
        return $this->belongsToMany('App\Models\Fair', 'artworks_fairs', 'artwork_id', 'fair_id');
    }

    public function artist()
    {
        return $this->hasOne('App\Models\Artist', 'id', 'artist_id');
    }

    public function gallery()
    {
        return $this->hasOne('App\Models\Gallery', 'id', 'gallery_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ArtworksImage', 'artwork_id', 'id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'id', 'current_country');
    }

    public function news(){
        return $this->belongsToMany('App\Models\Post', 'posts_artworks', 'artwork_id', 'post_id');
    }

    public function getTagIdsAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function getFairIdsAttribute()
    {
        return $this->fairs->pluck('id')->toArray();
    }

    public function getGalleryNameAttribute()
    {
        return $this->gallery()->first()->name;
    }

    public function getArtistNameAttribute()
    {
        $artist = $this->artist()->first();
        return $artist->last_name . " " . $artist->first_name;
    }

    public function getMediumStringAttribute()
    {
        $medium = $this->categories()->whereIsMedium(1)->pluck('name')->toArray();
        return implode(', ', $medium);
    }

    public function getMeasureCmAttribute()
    {
        return $this->width . ' x ' . $this->height . (($this->depth ? ' x ' . $this->depth : ''));
    }

    public function getMeasureIncAttribute()
    {
        return number_format(round($this->width/2.54, 2 ),2,'.',''). ' x ' . number_format(number_format(round($this->height/2.54, 2 ),2,'.','')) . (($this->depth ? ' x ' . number_format(round($this->depth/2.54, 2 ),2,'.','') : ''));
    }

    public function getNewsAttribute($value)
    {
        return $value;
    }

    public function getPrettyPriceAttribute()
    {
        $request = request();
        if(!$request->is('admin*')){
            if(Auth::user()) {
                $offer = Auth::user()->offers()->whereArtworkId($this->id)->whereStatus(2)->first();
                if($offer){
                    return number_format($offer->amount, '2', ',', '') . " €";
                }
            }
        }
        return number_format($this->price, '2', ',', '') . " €";
    }

    public function getPriceAttribute($value)
    {
        $request = request();
        if(!$request->is('admin*')){
            if(Auth::user()) {
                $offer = Auth::user()->offers()->whereArtworkId($this->id)->whereStatus(2)->first();
                if($offer){
                    return $offer->amount;
                }
            }
        }
        return $value;
    }

    public function getMainImageAttribute(){
        $image = $this->images()->orderBy('label', 'ASC')->first();
        if(!$image){
            $image = new ArtworksImage();
            $image->url = url('/images/placeholder-square-box.jpg');

            return $image;
        }
        return $image;
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            $main = $this->getMainImageAttribute();
            if(!$main->id){
                return '/images/placeholder-square-box.jpg';
            }
            return $main->url;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            $main = $this->getMainImageAttribute();
            if(!$main->id){
                return '/images/placeholder-wide-box.jpg';
            }
            return $main->url;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            $main = $this->getMainImageAttribute();
            if(!$main->id){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $main->url;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
