<?php

namespace App\Handlers;

use Illuminate\Support\Facades\Auth;

class LfmConfigHandler extends \UniSharp\LaravelFilemanager\Handlers\ConfigHandler
{
    public function userField()
    {
        switch (Auth::user()->role){
            case 'superadmin':
            case 'admin':
                return 'archive';
                break;
            case 'fair':
                return 'archive/fairs/' . Auth::user()->id;
                break;
            case 'gallerist':
                return 'archive/galleries/' . Auth::user()->id;
                break;
            case 'artist':
                return 'archive/artists/' . Auth::user()->id;
                break;
            case 'editor':
                return 'archive/editors/' . Auth::user()->id;
                break;
        }
    }
}
