<div id="edit-section-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit section</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="form-modal-section">
                    <input type="hidden" class="block-selector" value="">
                    <div class="form-group">
                        <label for="tags">Section</label>
                        <select id="sections" class="select2-modal form-control sections" name="sections" required>
                            <option value="artworks">Artworks</option>
                            <option value="artists">Artists</option>
                            <option value="categories">Artworks Categories</option>
                            <option value="galleries">Galleries</option>
                            <option value="fairs">Fairs</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Background Image</label>
                        <select id="bg-image" class="select2-modal-bg form-control bg-image" name="bg-image" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Caption</label>
                        <input id="caption" class="form-control caption" type="text" name="caption">
                    </div>
                    <div class="form-group">
                        <label for="tags">Link 1</label>
                        <select class="select2-modal-link form-control link" name="link">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Link 2</label>
                        <select class="select2-modal-link form-control link" name="link">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Link 3</label>
                        <select class="select2-modal-link form-control link" name="link">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Link 4</label>
                        <select class="select2-modal-link form-control link" name="link">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tags">Link 5</label>
                        <select class="select2-modal-link form-control link" name="link">
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary submit-section">Save section</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>