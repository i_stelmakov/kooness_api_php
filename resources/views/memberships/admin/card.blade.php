@extends('layouts.admin')

@if(!$change_plan)
    @section('title', 'Admin | Memberships Card')
    @section('page-header', 'Memberships Card')
@else
    @section('title', 'Admin | Memberships Change Plan')
    @section('page-header', 'Memberships Change Plan (Summary)')
@endif

@section('styles')
    <style>
        .card-item-account {
            background-size: auto 100%;
            background-repeat: no-repeat;
            background-position: 4px center;
            height: auto;
            width: auto;
            display: block;
            padding: 0 0 0 44px;
        }
        table {
            width: 100%;
        }
        table td, table tr, table th {
            padding: 10px;
        }
        table td {
            border: 1px solid silver;
        }
        .card-edit-box {
            overflow: hidden;
            max-height: 1800px;
            -webkit-transition: max-height 0.3s;
            -moz-transition: max-height 0.3s;
            -ms-transition: max-height 0.3s;
            -o-transition: max-height 0.3s;
            transition: max-height 0.3s;
        }
        .card-edit-box.collapsed {
            max-height: 0;
        }
    </style>
@endsection



@section('content')
    @if(isset($change_plan) && $change_plan)
        <div class="alert alert-warning" role="alert">
            <strong>Attention! The changes to your plan will be made effective after your next renewal!</strong>
        </div>
    @endif
    @if( roleCheck('gallerist') )

        <form id="card-edit" class="action-submit row no-margin" role="form" method="POST" action="{{ (!$change_plan) ? route('admin.memberships.payment.action') : '' }}" data-attachment="validate" data-kind="cards-editing">
            @csrf

            {{-- Section intro --}}
            @if(!$change_plan)
                <div class="">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            @endif

            {{-- Details --}}
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="row">

                        {{-- Personal Data Recap --}}
                        <div class="col-md-6">
                            <div class="recap">
                                <h3>Your personal data</h3>
                                <p>{{ $billing_data->{'full_name'} }}</p>
                                <p>Fiscal Code: {{ $billing_data->{'fiscal_code'} }}</p>
                                @if(isset($billing_data->{'vat_number'}))
                                    <p>VAT Code: {{ $billing_data->{'vat_number'} }}</p>
                                @endif
                                <p>Address: {{ $billing_data->{'address'} }} - {{ $billing_data->{'zip'} }} - {{ $billing_data->{'city'} }}{{ isset($billing_data->{'state'})? " (" . $billing_data->{'state'} . ")" : ''   }} - {{ \App\Models\Country::find($billing_data->{'country'})->name}}</p>
                                @if(!$change_plan)
                                    <a class="btn btn-primary" href="{{ route('admin.memberships.billing') }}">Edit</a>
                                @endif
                            </div>
                        </div>

                        {{-- Plan Recap --}}
                        <div class="col-md-6">
                            <div class="recap">
                                <h3>Your selected plan</h3>
                                <p>{{ $plan->name }}</p>
                                <p>Fixed Fee: {{ $plan->fixed_fee }}{{ $plan->getSymbolCurrency() }}/month</p>
                                <p>Success Fee: {{ $plan->success_fee }}%</p>
                                @if(!$change_plan)
                                    <a class="btn btn-primary" href="{{ route('admin.memberships.plans') }}">Edit</a>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12 spacer"></div>
                        @if(!$change_plan)
                            <form class="action-submit" id="cardForm0" class="row" data-kind="cards-editing" data-id="">
                                {{--Card Owner--}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="card_owner">Card Owner</label>
                                        <input class="form-control" type="text" name="card_owner" placeholder="Owner" value="{{ old('card_owner') }}" required="" value="">
                                    </div>
                                </div>
                                {{--Card Number--}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="card_number">Card Number</label>
                                        <input class="form-control card-number" type="text" name="card_number" placeholder="Number" value="{{ old('card_number') }}" required="" value="">
                                    </div>
                                </div>
                                {{--Card expire year--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_exp_year">Card expire year</label>
                                        <select class="form-control" name="card_exp_year" id="card_exp_year" required onchange="$('#card_exp_month').trigger('change');">
                                            @for($i = date('Y'); $i < date('Y') + 10; $i++)
                                                <option value="{{ $i }}"  {{ (collect(old('card_exp_year'))->contains($i)) ? 'selected':'' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                {{--Card expire month--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_exp_month">Card expire month</label>
                                        <select id="card_exp_month" class="form-control" name="card_exp_month" onchange="$('#card_exp_year').trigger('change');" required>
                                            <option value="01" {{ (collect(old('card_exp_month'))->contains("01")) ? 'selected':'' }}>January</option>
                                            <option value="02" {{ (collect(old('card_exp_month'))->contains("02")) ? 'selected':'' }}>February</option>
                                            <option value="03" {{ (collect(old('card_exp_month'))->contains("03")) ? 'selected':'' }}>March</option>
                                            <option value="04" {{ (collect(old('card_exp_month'))->contains("04")) ? 'selected':'' }}>April</option>
                                            <option value="05" {{ (collect(old('card_exp_month'))->contains("05")) ? 'selected':'' }}>May</option>
                                            <option value="06" {{ (collect(old('card_exp_month'))->contains("06")) ? 'selected':'' }}>June</option>
                                            <option value="07" {{ (collect(old('card_exp_month'))->contains("07")) ? 'selected':'' }}>July</option>
                                            <option value="08" {{ (collect(old('card_exp_month'))->contains("08")) ? 'selected':'' }}>August</option>
                                            <option value="09" {{ (collect(old('card_exp_month'))->contains("09")) ? 'selected':'' }}>September</option>
                                            <option value="10" {{ (collect(old('card_exp_month'))->contains("10")) ? 'selected':'' }}>October</option>
                                            <option value="11" {{ (collect(old('card_exp_month'))->contains("11")) ? 'selected':'' }}>November</option>
                                            <option value="12" {{ (collect(old('card_exp_month'))->contains("12")) ? 'selected':'' }}>December</option>
                                        </select>
                                    </div>
                                </div>
                                {{--Card CVV--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_cvv">Card CVV</label>
                                        <input class="form-control" type="text" name="card_cvv" placeholder="CVV" required="" >
                                    </div>
                                </div>
                                {{--Card Type--}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="card_type">Card Type</label>
                                        <input class="form-control" type="text" name="card_type" placeholder="Type" required="" readonly value="{{ old('card_type') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                            <label>
                                                <input type="checkbox" name="agree" value="1" id="agree" {{ old('agree')? 'checked="checked"' : '' }} required>
                                                I agree with the terms and conditions of the subscribed membership agreement
                                            </label>
                                        <br>
                                        <span id="accept-error"></span>
                                    </div>
                                </div>
                            </form>
                        @else
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <th>Credit Card Circuit</th>
                                        <th>Credit Card Number</th>
                                        <th>Name on card</th>
                                        <th>Expires on</th>
                                    </tr>
                                    <tr>
                                        @if($card_data)
                                            <td>
                                                <span class="card-item-account" id="card-2" style="background-image: url('/images/credit_cards/light/{{ strtolower($card_data->type) }}.png')">{{ $card_data->type }}</span></td>
                                            <td>
                                                end in ***{{ $card_data->end_with }}
                                            </td>
                                            <td>{{ $card_data->name }}</td>
                                            <td>{{ date('m/Y', strtotime($card_data->expire_date )) }}</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" form="card-edit" value="{{ (!$change_plan) ? 'Pay Now' : 'Send change request' }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <script>
            window.addEventListener("load", function () {

                // controlli su Numero carta e  CVV
                $('form[data-kind="cards-editing"]').each(function () {
                    var $cardNumber = $(this).find('input[name=card_number]');
                    var $cardType = $(this).find('input[name=card_type]');
                    // inserisco tipologia carta in campo
                    $cardNumber.payform('formatCardNumber');
                    $cardNumber.keyup(function () {
                        var cardType = $.payform.parseCardType($cardNumber.val());
                        if (cardType && $.inArray(cardType, ['visa', 'mastercard', 'amex', 'visaelectron', 'maestro']) > -1) {
                            $cardType.val(cardType.charAt(0).toUpperCase() + cardType.slice(1));
                        } else {
                            $cardType.val('')
                        }
                    });
                    // formatta CVV
                    $(this).find('input[name=card_cvv]').payform('formatCardCVC');
                });

                // aggiunge regola a Validate per verificare le date
                jQuery.validator.addMethod("CCExp", function (value, element, params) {
                    var minMonth = new Date().getMonth() + 1;
                    var minYear = new Date().getFullYear();

                    var formMonth = $(element).parents('form').find('select[name="card_exp_month"]');
                    var formYear = $(element).parents('form').find('select[name="card_exp_year"]');

                    var month = parseInt(formMonth.val(), 10);
                    var year = parseInt(formYear.val(), 10);

                    if ((year > minYear) || ((year === minYear) && (month >= minMonth))) {
                        return true;
                    } else {
                        return false;
                    }
                }, "Your Credit Card Expiration date is invalid.");

                // controlli validate su Numero carta e required
                jQuery.validator.addClassRules('card-number', {
                    required: true,
                    creditcard: true
                });

            }); // END - jQuery window load

        </script>

    @endif
@endsection