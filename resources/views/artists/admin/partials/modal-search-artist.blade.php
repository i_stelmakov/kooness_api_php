<div class="modal fade" id="basicModal" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Search an artist or add new one!</h4>
            </div>
            <div class="modal-body">
                <h3>Search an artist:</h3>
                <select class="select2 form-control" name="artist">
                    @foreach($artists as $artist)
                        <option value="{{ $artist->id }}">{{ $artist->full_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary associate-artist">Associate to me</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Add new one</button>
                <button type="button" class="btn btn-primary" id="close-artist-association-modal">Close & Return</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        var $modal = $("#basicModal");
        $modal.find('.associate-artist').click(function () {
            $.ajax({
                url: '{{ route('admin.associate.artist.to.me') }}',
                dataType: 'json',
                type: 'POST',
                data: { artist_id: $modal.find('select[name=artist]').val()},
                beforeSend: function () {
                    $modal.modal('hide');
                    waitingDialog.show('Associate artist...', {
                        dialogSize: 'm',
                        progressType: 'success'
                    });
                },
                success: function (response) {
                    if (response.redirectTo)
                        window.location.href = response.redirectTo;
                }
            });
        });
        $modal.modal({
            backdrop: 'static',
            keyboard: false
        });
        $modal.on('click','button#close-artist-association-modal',function(e) {
            window.location = "{{ route('admin.artists.index') }}";
        });
    });
</script>