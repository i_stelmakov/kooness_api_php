/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


// Disable all JS logs only for production
// if(window.envKind == 'prod') {
//     console.log = function() { };
// }

// Media Queries (as in scss files)
window.untilSmall = 767;
window.untilMedium = 991;
window.untilLarge = 1199;

window.fromSmall = 768;
window.fromMedium = 992;
window.fromLarge = 1200;


// Vendors
function initGMaps() {}
import jquery from 'jquery';
window.jQuery = jquery;
window.$ = jquery;
window.jquery = jquery;
window.$ = $.extend(require('jquery-ui-bundle'));
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
window.fx = require("money"); // libreria per conversioni di valute
window.WebFont = require('webfontloader'); // libreria per riconoscere il caricamento completato dei fonts
window.jQueryBridget = require('jquery-bridget'); // activate jQueryBridget -> libreria per riassociare corretto scope JS dopo Webpack
window.imagesLoaded = require('imagesloaded'); // libreria per verificare caricamento completato delle immagini
imagesLoaded.makeJQueryPlugin( $ ); // provide jQuery argument
window.Masonry = require('masonry-layout'); // libreria per masonry
jQueryBridget( 'masonry', Masonry, $ ); // pass to jQuery through jQueryBridge
window.toastr = require('toastr'); // libreria per verificare caricamento completato delle immagini
require('payform');
require('payform/dist/jquery.payform');
require('remodal'); // libreria per modals
require('slick-carousel'); // libreria per carousel
require('sweetalert'); // libreria per warnings
require('sticky-kit/dist/sticky-kit.js'); // puntamento errato del "main script" da NPM, quindi puntamento manuale
require('./vendors/jquery.validate.js'); // "jquery-validation": "^1.13.0",
require('jquery-validation/dist/additional-methods');
require('./vendors/lightbox');

// Modules
require('./components/generic-frontend.js');

// Templates
// window.magazineTemplate = require('../../views/templates/magazine-template.hbs')
// window.magazineTagsTemplate = require('../../views/templates/magazine-tags-template.hbs')