# BACKEND

## Artwork Archive 
- [ ] task uno
- [x] task uno
- [x] task tre



# FRONTEND

## Header (generico)
- [ ] Funzione ajax search
- [ ] Nascondere pulsante "App"

## Widgets (generico)
- [ ] lettera principale con background clip come "K" sempre

## User Profile
- [ ] splittare il box di inserimento carte / indirizzi / info personali su 3 colonne su Desktop MD

## Artwork Intro 
- [ ] Lorem

## Artist Single
- [ ] Integrare servizio API Video

## Artworks Archive 
- [ ] i box di scelta Categoria in alto devono contenere un background random tra i contenuti ricavati
- [ ] una volta selezionato uno dei filtri dai box, all'entrata nel template archivio filtrato, chiudere il blocco di filtraggio 
- [ ] mostriamo in verde quella selezionata (categoria di filtraggio attuale)

## Tags Page
- [ ] mostrare come archivio, struttura simile ai widget
- [ ] dividere con row per ognuna delle tre sezioni dei tags: artworks, artist, gallery

## Tags richiamati dalle altre pagine
- [ ] mostrarli in camel case con spazi

## Footer Newsletter
- [ ] Agganciare API (parlare con Lab Villa)

## Versione mobile
- [ ] Stand-by: Lab Villa ci fornirà un mockup per la visualizzazione
- [ ] in Artworks Archive prevedere possibilità di fare apparire la barra dei filtri come menu fixed che appare da sinistra













# Frontend

## Header (generico)
### Barra Search
• come funziona il search?
R: [ ]in fase di realizzazione
### Barra News
vorremo che sia uno spazio per inserimento di testo libero + link da associare. Possiamo inserirla in backend?
R: [ ]implementato come singola
### Menu
• nascondere momentaneamente pulsante "App"
R: [ ]implementiamo
### Claim
• scritta “follow” in colore Kooness, non nero
R: [ ]a noi risulta verde kooness, puoi girarci uno screenshot di come la vedi?
• “The most innovative art galleries network” => se non richiede effort, fare una prova modificando in “To Dream, to Collect” (se possibile mantenerlo provvisorio) no problem
R: [x] modificata
## Footer (generico)
### Social box
- gli stessi social media dell’header vanno inseriti anche nel footer della homepage (vedi mockup) 
R: [x] trasportati
### Newsletter
- Testo del sottoparagrafo ancora da definire
R: [ ]stand-by per testi
### Link a sezioni
About Us
Seguire il layout del mockup. Testi ancora da definire per: 
Who We Are
The Team
Press
Careers
Contact Us
Special Projects (da tenere momentaneamente invisibile poichè non abbiamo contenuti da mostrare)
Privacy Policy
R: [ ]Implementato funzionamento di recupero pagine statiche, attendiamo testi definitivi per ogni sezione
## Pagine Statiche (generico)
### Pagina Our Services
Seguire il layout del mockup, MA eliminare la parte relativa ai testimonials. Testi ancora da definire. 
R: [ ]ricreata pagina statica come da mockup, procediamo ad eliminare sezione "Testimonials", stand-by testi
### Gallery Application
Seguire il layout del mockup, aggiungendo dopo le n.6 “features” un form da compilare per la domanda come il sito vecchio (vedi qui). Testi ancora da definire. 
R: [ ]attendiamo istruzioni per il form ed i suoi campi
### Support Center
Applicare lo stesso layout  della sezione About Us del mockup, seguendo le stesse FAQ del sito vecchio.
R: [ ]replichiamo
## Artworks Archive
### Singolo Item
- ridurre lo spazio tra le voci di ogni artwork presente nella lista
R: [x] ridotto spazio
- nome e cognome dell’artista completo
R: [x] impostato come da mockup
- ridurre la dimensione del font relativo al titolo dell’opera
R: [x] impostato come da mockup
- prezzo allineato a destra
R: [x] impostato come da mockup
- LAYOUT DA RIPETERE IN TUTTE LE MINIATURE DEGLI ARTWOKRS
R: [x] implementato per tutti gli items. Sotto esempio del nuovo
Review QA Kooness.pngSchermata 2018-07-13 alle 17.15.49.jpg
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
## Artwork Single
### Box Currency
Possibilità di switchare tra £,$ e €. (come vecchio sito) 
R: [ ]funzionalità da implementare 
### Box About the Artist
Non mostrare così tanto testo su “About the Artist”ma tenere solo le prime 6 righe e poi “Read more”
R: [x] limitato a 140 caratteri => il testo si puo basare sul numero di caratteri ma non sulle righe, perché le righe cambiano in base alle dimensioni dello schermo 
### About the Gallery
Non mostrare così tanto testo su “About the Gallery”ma tenere solo le prime 6 righe e poi “Read more”. In generale da applicare sempre regola di n.6 righe e poi “Read more”
R: [x] Come sopra 
### Box Similar Works
manca la parte relativa ai “Similar Works” (vedi mockup). 
R: [x] implementato
## Artists Archive All
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
## Artist Single
### Box Follow
- - il cuoricino in alto a dx sta per “follow” ma non in senso di seguire la sua pagina facebook quanto invece di “preferito” (un artista che si vuole salvare come preferito e seguire).
R: [ ]Funzione da finire di agganciare 
### Box About the Artist
- Non mostrare così tanto testo su “About the Artist” ma tenere solo le prime 6 righe e poi “Read more” 
R: [ ]da definire funzionamento (read more potrebbe semplicemente espandere la sezione, altrimenti limitata in altezza) 
## Galleries Archive All
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
### Testi generici
[x]MAGAZINES intro => Magazine al singolare
Mostra di più

## Header (generico)
### Barra Search
• come funziona il search?
R: [ ]in fase di realizzazione
### Barra News
vorremo che sia uno spazio per inserimento di testo libero + link da associare. Possiamo inserirla in backend?
R: [ ]implementato come singola
### Menu
• nascondere momentaneamente pulsante "App"
R: [ ]implementiamo
### Claim
• scritta “follow” in colore Kooness, non nero
R: [ ]a noi risulta verde kooness, puoi girarci uno screenshot di come la vedi?
• “The most innovative art galleries network” => se non richiede effort, fare una prova modificando in “To Dream, to Collect” (se possibile mantenerlo provvisorio) no problem
R: [x] modificata
## Footer (generico)
### Social box
- gli stessi social media dell’header vanno inseriti anche nel footer della homepage (vedi mockup) 
R: [x] trasportati
### Newsletter
- Testo del sottoparagrafo ancora da definire
R: [ ]stand-by per testi
### Link a sezioni
About Us
Seguire il layout del mockup. Testi ancora da definire per: 
Who We Are
The Team
Press
Careers
Contact Us
Special Projects (da tenere momentaneamente invisibile poichè non abbiamo contenuti da mostrare)
Privacy Policy
R: [ ]Implementato funzionamento di recupero pagine statiche, attendiamo testi definitivi per ogni sezione
## Pagine Statiche (generico)
### Pagina Our Services
Seguire il layout del mockup, MA eliminare la parte relativa ai testimonials. Testi ancora da definire. 
R: [ ]ricreata pagina statica come da mockup, procediamo ad eliminare sezione "Testimonials", stand-by testi
### Gallery Application
Seguire il layout del mockup, aggiungendo dopo le n.6 “features” un form da compilare per la domanda come il sito vecchio (vedi qui). Testi ancora da definire. 
R: [ ]attendiamo istruzioni per il form ed i suoi campi
### Support Center
Applicare lo stesso layout  della sezione About Us del mockup, seguendo le stesse FAQ del sito vecchio.
R: [ ]replichiamo
## Artworks Archive
### Singolo Item
- ridurre lo spazio tra le voci di ogni artwork presente nella lista
R: [x] ridotto spazio
- nome e cognome dell’artista completo
R: [x] impostato come da mockup
- ridurre la dimensione del font relativo al titolo dell’opera
R: [x] impostato come da mockup
- prezzo allineato a destra
R: [x] impostato come da mockup
- LAYOUT DA RIPETERE IN TUTTE LE MINIATURE DEGLI ARTWOKRS
R: [x] implementato per tutti gli items. Sotto esempio del nuovo
Review QA Kooness.pngSchermata 2018-07-13 alle 17.15.49.jpg
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
## Artwork Single
### Box Currency
Possibilità di switchare tra £,$ e €. (come vecchio sito) 
R: [ ]funzionalità da implementare 
### Box About the Artist
Non mostrare così tanto testo su “About the Artist”ma tenere solo le prime 6 righe e poi “Read more”
R: [x] limitato a 140 caratteri => il testo si puo basare sul numero di caratteri ma non sulle righe, perché le righe cambiano in base alle dimensioni dello schermo 
### About the Gallery
Non mostrare così tanto testo su “About the Gallery”ma tenere solo le prime 6 righe e poi “Read more”. In generale da applicare sempre regola di n.6 righe e poi “Read more”
R: [x] Come sopra 
### Box Similar Works
manca la parte relativa ai “Similar Works” (vedi mockup). 
R: [x] implementato
## Artists Archive All
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
## Artist Single
### Box Follow
- - il cuoricino in alto a dx sta per “follow” ma non in senso di seguire la sua pagina facebook quanto invece di “preferito” (un artista che si vuole salvare come preferito e seguire).
R: [ ]Funzione da finire di agganciare 
### Box About the Artist
- Non mostrare così tanto testo su “About the Artist” ma tenere solo le prime 6 righe e poi “Read more” 
R: [ ]da definire funzionamento (read more potrebbe semplicemente espandere la sezione, altrimenti limitata in altezza) 
## Galleries Archive All
### Load More button
- load more piu in evidenza (magari in grassetto) e allineato al centro (non a sinistra come ora)
R: [x] stilato come pulsante standard e centrato 
### Testi generici
[x]MAGAZINES intro => Magazine al singolare

[ ]REGISTER page => Kooness con due S
[ ]Recent viewed => Recently viewed
[ ]Ready for publication => Ready to be published
R: [ ]da variare


## Artworks Archive All

### Categorie di filtraggio in alto
- Immagine di sfondo delle categorie (es. landscape, conceptual, Pop Art etc.)
R: [x] implementata immagine con inserimento manuale 


# Backend


## Artwork Edit

### Box Fairs
Insieme al flag Available on Fair (Available on fair) YES/NO vorremmo inserire la voce “Show Price” YES/NO. La ragione di questo è che in futuro vorremmo che le opere che fanno parte di una fiera possano essere disponibili in vendita su Kooness. Attualmente, se un’opera è flaggata come available on fair allora il prezzo automaticamente non è visibile sul sito e dunque non acquistabile online. La voce “show price” deve necessariamente però essere abilitata solo quando si seleziona Available on Fair  YES, come diretta conseguenza. (Per dubbi chiedete pure.)
Ricapitolando:
SE Available on Fair  YES => verrà fuori il pulsante “Show Price” YES/NO
SE Available on Fair  NO => Il pulsante “Show Price” YES/NO non verrà fuori. 1gg

R: [ ] creati campi per immissione nel backend. Ora dobbiamo intendere se volete applicare la differenziazione di visualizzazione in frontend già da adesso o volete aspettare
a
### Box Selling information
Inserire campo “Price (excl. VAT)” e un altro campo “VAT/Taxes” a parte. Avevamo già affrontato il discorso a voce se non erro. Per dubbi chiedete pure.
R: [x] implementata