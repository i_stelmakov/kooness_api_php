@extends('layouts.admin')
@section('title', "Admin | Edit Artwork Category `{$category->name}`")
@section('page-header', "Admin - Edit Artwork Category `{$category->name}`")

@section('content')
    @include('artworks.categories.form', ['action' => route('admin.categories.artworks.update', [$category->id]), 'method' => 'PATCH'])
@endsection