@extends('layouts.admin')
@section('title', 'Admin | Artworks')
@section('page-header', 'Artworks')
@section('styles')
    <style>

        table{
            table-layout: fixed;
        }
        th, td {
            width: 100px;
        }
        th:nth-child(1), td:nth-child(1) {
            width: 60px;
        }
        th:nth-child(2), td:nth-child(2) {
            width: 80px;
        }
        th:nth-child(3), td:nth-child(3) {
            width: 160px;
        }
        th:nth-child(4), td:nth-child(4) {
            width: 160px;
        }
        th:nth-child(5), td:nth-child(5) {
            width: 160px;
        }
        th:nth-child(6), td:nth-child(6) {
            width: 80px;
        }
        th:nth-child(7), td:nth-child(7) {
            width: 140px;
        }
        th:nth-child(8), td:nth-child(8) {
            width: 140px;
        }
        th:nth-child(9), td:nth-child(9) {
            width: 60px;
        }
        th:nth-child(10), td:nth-child(10) {
            width: 60px;
        }
        th:nth-child(11), td:nth-child(11) {
            width: 100px;
        }
        th:nth-child(12), td:nth-child(12) {
            width: 140px;
        }
        th input, td input {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                @if(!roleCheck('seo') && !roleCheck('artist'))
                    <div class="box-header">
                        <div class="pull-right">
                            <a href="{{ route('admin.artworks.create') }}">
                                <button class="btn btn-primary">Add new artwork</button>
                            </a>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <table id="table-galleries" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Reference</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Medium</th>
                            <th>Price</th>
                            <th>Artist</th>
                            <th>Gallery</th>
                            <th>Sold</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {

                // Setup - add a text input to each footer cell
                $('#table-galleries thead tr').clone(true).appendTo( '#table-galleries thead' );
                $('#table-galleries thead tr:eq(1) th').each( function (i) {
                    var title = $(this).text();

                    if(title == 'Category' || title == 'Medium') {
                        $(this).html( '<input id="'+title.toLowerCase()+'" type="text" placeholder="Search '+title+'" />' );

                        $( 'input', this ).on( 'keyup change', function () {
                            if ( table.column(i).search() !== this.value ) {

                                console.log('passa!');
                                table
                                    .column(i)
                                    .search( this.value )
                                    .draw();
                            }
                        } );

                    } else {
                        $(this).html( '' );
                    }

                } );

            var table = $('#table-galleries').DataTable({
                orderCellsTop: true,
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "bStateSave": true,
                "fnStateLoaded": function (oSettings, oData) {
                    if(oData.columns !== undefined){
                        var categorySearch = oData.columns[3].search.search;
                        var mediumSearch = oData.columns[4].search.search;
                        $('#category').val(categorySearch);
                        $('#medium').val(mediumSearch);
                    }
                },
                "fnStateLoad": function (oSettings) {
                },
                "order": [[ 10, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.artworks.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "reference"},
                    {"data": "title"},
                    {"data": "category"},
                    {"data": "medium"},
                    {"data": "price"},
                    {"data": "artist", "bSortable": false},
                    {"data": "gallery", "bSortable": false},
                    {"data": "sold",  "bSortable": false},
                    {"data": "status"},
                    {"data": "created_at", },
                    {"data": "options", "bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    // console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                                // esempio di attivazione di attributi CSS personalizzati per una singola cella, in base al valore
                                // if ( cellData < 1 ) {
                                //     $(td).css('color', 'red')
                                // }
                            }
                        }
                    }
                ]
            });
        });
    </script>
@endsection