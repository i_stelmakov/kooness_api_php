<?php

namespace App\Console\Commands;

use App\Models\MbrPlan;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class mbrRenewalAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:renewal-alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all membership where the renewal date is under 1 week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereRaw('(DATE(NOW()) = DATE_SUB(`premium_exp_date`, INTERVAL 7 DAY)) AND `deactivate_premium_request_date` IS NULL')->get();
        foreach ($users as $user) {
            if ($user->is_premium && $user->current_plan_id) {
                $plan = MbrPlan::find($user->current_plan_id);
                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_MEMBERSHIP_AUTOMATIC_RENEWAL;
                $extras = [
                    "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                    "plan" => $plan, //	name
                ];
                Mail::send('emails.gallerists.pre-renewal', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });
            }
        }
    }
}
