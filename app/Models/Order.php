<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id', 'id');
    }

    public function user(){
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

    public function promo(){
        return $this->hasOne('App\Models\PromoCode','id', 'promo_code');
    }

    public function cart(){
        return $this->hasOne('App\Services\Cart','id', 'cart_id');
    }

    public function setBillingAddress(UserAddress $userAddress){
        if($userAddress->type == 'private'){
            $this->billing_first_name = $userAddress->first_name;
            $this->billing_last_name = $userAddress->last_name;
        } else{
            $this->	billing_company_name = $userAddress->company_name;
            $this->billing_vat_number = $userAddress->vat_number;
        }
        $this->billing_fiscal_code = $userAddress->fiscal_code;
        $this->billing_email = $userAddress->email;
        $this->billing_phone = $userAddress->phone;
        $this->billing_address = $userAddress->address_1;
        $this->billing_country = $userAddress->country;
        $this->billing_country_code = $userAddress->country_code;
        $this->billing_city = $userAddress->city;
        $this->billing_state = $userAddress->state;
        $this->billing_zip = $userAddress->zip;
    }

    public function setShippingAddress(UserAddress $userAddress){
        $this->shipping_first_name = $userAddress->first_name;
        $this->shipping_last_name = $userAddress->last_name;
        $this->shipping_email = $userAddress->email;
        $this->shipping_phone = $userAddress->phone;
        $this->shipping_address = $userAddress->address_1;
        $this->shipping_country = $userAddress->country;
        $this->shipping_country_code = $userAddress->country_code;
        $this->shipping_city = $userAddress->city;
        $this->shipping_state = $userAddress->state;
        $this->shipping_zip = $userAddress->zip;
    }
}
