<div class="slider-item one-fourth-col-with-margin">
    <i title="Remove" class="fa fa-times delete-item" data-id="{{ $artist->id }}" data-section="artists"></i>
    <div class="slider-item-img square-box">
        <a href="{{ route('artists.single', [$artist->slug]) }}">
            <div class="background-cover gallery lazy" data-src="{{ $artist->url_square_box }}"></div>
        </a>
    </div>
    <div class="slider-item-txt">
        <div class="default-sheet-row slider-item-row">
            <div class="default-sheet-row-cell">
                <h2><a title="{{ $artist->countryBirth()->first()->name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a></h2>
            </div>
        </div>
        <!--
        <p class="birth">1918 - 2011</p>
        !-->
        <p class="dark-text provenance">{{ $artist->countryBirth()->first()->name }}</p>
        <p class="exposure">{{ $artist->artworks()->count() }} Works exhibited</p>
        <a class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
    </div>
</div>