<li class="item-drop" data-repeater-item>
    <i class="fa fa-arrows"></i>
    <i class="fa fa-times pull-right text-danger" data-repeater-delete style="cursor: pointer"></i>
    <input class="id" type="hidden" name="id" value="{{ $widget->{'id'} }}">
    <input class="order" type="hidden" name="order" value="{{ ($widget->{'order'}) ? $widget->{'order'} : 0 }}">
    <input class="form-control" type="text" placeholder="Title" name="title" value="{{ $widget->{'title'} }}" required>
    <input class="form-control" type="text" placeholder="Link" name="link" value="{{ $widget->{'link'} }}" required>
    <input class="form-control not-for-c not-for-b" type="{{ ($section != 'galleries' && $section != 'fairs' && $section != 'exhibitions')? 'text' : 'hidden' }}" placeholder="Subtitle" name="subtitle" value="{{ $widget->{'subtitle'} }}">
    <input class="form-control not-for-c not-for-b" type="{{ ($section != 'galleries' && $section != 'fairs' && $section != 'exhibitions')? 'text' : 'hidden' }}" placeholder="Description" name="description" value="{{ $widget->{'description'} }}">
    <select class="form-control select-type not-for-c {{ ($section != 'galleries' && $section != 'artists' && $section != 'fairs' && $section != 'artworks'  && $section != 'exhibitions')? 'select2' : 'hidden' }}" name="section">
        @if($section != 'galleries' && $section != 'artists' && $section != 'fairs')
            <option value="artworks" {{ $widget->{'section'} == 'artworks' ? "selected" : "" }} >Artworks</option>
        @endif
        @if($section != 'galleries' && $section != 'artworks' && $section != 'fairs')
            <option value="artists" {{ $widget->{'section'} == 'artists' ? "selected" : "" }} >Artists</option>
        @endif
        @if($section != 'artists' && $section != 'artworks' && $section != 'fairs')
            <option value="galleries" {{ $widget->{'section'} == 'galleries' ? "selected" : "" }} >Galleries</option>
        @endif
        @if($section != 'artists' && $section != 'artworks' && $section != 'galleries')
            <option value="fairs" {{ $widget->{'section'} == 'fairs' ? "selected" : "" }} >Fairs</option>
        @endif
        @if(!$section)
            <option value="magazine" {{ $widget->{'section'} == 'magazine' ? "selected" : "" }} >Magazine</option>
        @endif
    </select>
    <select class="form-control select-tpl {{ ($section != 'galleries' && $section != 'artists' && $section != 'fairs' && $section != 'artworks'  && $section != 'exhibitions')? 'select2' : 'hidden' }}" name="template">
        @if($section != 'galleries' && $section != 'fairs' && $section != 'exhibitions')
            <option data-template="widget-template-a" value="tplA" {{ $widget->{'template'} == 'tplA' ? "selected" : "" }}>Template A (Artworks / Artists)</option>
        @endif
        @if($section != 'artists' && $section != 'artworks'  && $section != 'exhibitions')
            @if($section != 'fair')
                <option data-template="widget-template-b" value="tplB" {{ $widget->{'template'} == 'tplB' ? "selected" : "" }}>Template B (Galleries)</option>
            @endif
            @if($section)
                <option data-template="widget-template-b" value="tplB" {{ $widget->{'template'} == 'tplB' ? "selected" : "" }}>Template C (Fairs)</option>
            @else
                <option data-template="widget-template-b" value="tplBf" {{ $widget->{'template'} == 'tplBf' ? "selected" : "" }}>Template C (Fairs)</option>
                <option data-template="widget-template-ca" value="tplCa" {{ $widget->{'template'} == 'tplCa' ? "selected" : "" }}>Template C (Magazine)</option>
            @endif
        @endif
        @if($section != 'galleries' && $section != 'artists' && $section != 'artworks' && $section != 'fairs' )
            <option data-template="widget-template-c" value="tplC" {{ $widget->{'template'} == 'tplC' ? "selected" : "" }}>Template C (Exhibitions with past, current, feature)</option>
        @endif
    </select> <select class="select2-widget form-control widget-items not-for-c" name="items" multiple>
        @if($widget->{'items'} && $widget->{'template'} != 'tplC')
            @foreach( $widget->{'items'} as $item)
                <option value="{{ $item->{'id'} }}" selected>{{ $item->{'text'} }}</option>
            @endforeach
        @endif
    </select>

    <label for="item_past">Past</label>
    <select class="select2-widget-exhibition form-control widget-items-exhibition for-c" name="item_past">
        @if(isset($widget->{'items'}['past']))
            <option value="{{ $widget->{'items'}['past']->{'id'} }}" selected>{{ $widget->{'items'}['past']->{'text'} }}</option>
        @endif
    </select>

    <label for="item_current">Current</label>
    <select class="select2-widget-exhibition form-control widget-items-exhibition for-c" name="item_current">
        @if(isset($widget->{'items'}['current']))
            <option value="{{ $widget->{'items'}['current']->{'id'} }}" selected>{{ $widget->{'items'}['current']->{'text'} }}</option>
        @endif
    </select>

    <label for="item_upcoming">Upcoming</label>
    <select class="select2-widget-exhibition form-control widget-items-exhibition for-c" name="item_upcoming">
        @if(isset($widget->{'items'}['upcoming']))
            <option value="{{ $widget->{'items'}['upcoming']->{'id'} }}" selected>{{ $widget->{'items'}['upcoming']->{'text'} }}</option>
        @endif
    </select>
</li>