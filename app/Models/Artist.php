<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $guarded = [];

    protected $appends = ['tag_ids', 'category_ids', 'gallery_ids', 'gallery_names'];

    protected $fillable = [
        'first_name',
        'last_name',
        'slug',
        'website',
        'phone',
        'url_cover',
        'about_the_artist',
        'meet_the_artist',
        'bibliography',
        'curriculum_vitae',
        'awards',
        'date_of_birth',
        'date_of_death',
        'city_of_birth',
        'country_of_birth',
        'current_location',
        'featured',
        'status',
        'user_id',
        'created_by',
        'create_from_exhibition',
        'meta_title',
        'h1',
        'canonical',
        'meta_keywords',
        'meta_description',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'artists_tags', 'artist_id', 'tag_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\ArtistsCategory', 'artists_artists_categories', 'artist_id', 'category_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function galleries()
    {
        return $this->belongsToMany('App\Models\Gallery', 'artists_galleries', 'artist_id', 'gallery_id');
    }

    public function artworks(){
        return $this->hasMany('App\Models\Artwork', 'artist_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function news(){
        return $this->belongsToMany('App\Models\Post', 'posts_artists', 'artist_id', 'post_id');
    }

    public function countryBirth()
    {
        return $this->hasOne('App\Models\Country', 'id', 'country_of_birth');
    }

    public function getTagIdsAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function getGalleryIdsAttribute()
    {
        return $this->galleries->pluck('id')->toArray();
    }

    public function getGalleryNamesAttribute()
    {
        return implode(', ',$this->galleries->pluck('name')->toArray());
    }

    public function getUrlSquareBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-square-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlWideBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-wide-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getUrlVerticalBoxAttribute($value)
    {
        if(!$value){
            if(!$this->url_full_image){
                return '/images/placeholder-vertical-box.jpg';
            }
            return $this->url_full_image;
        }
        return $value;
    }

    public function getSlugAttribute($value){
        return trim($value);
    }
}
