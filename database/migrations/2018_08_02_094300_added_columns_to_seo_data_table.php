<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedColumnsToSeoDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_seo_data', function (Blueprint $table) {
            $table->boolean('no_index')->default(0)->after('canonical');
            $table->boolean('no_follow')->default(0)->after('no_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generic_seo_data', function (Blueprint $table) {
            $table->dropColumn('no_index');
            $table->dropColumn('no_follow');
        });
    }
}
