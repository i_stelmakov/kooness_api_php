<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\User;
use App\Services\Api4Dem\Api4Dem;
use App\Services\Api4Dem\Api4DemDataParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Session;
use Spatie\Permission\Models\Role;
use View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.admin.index');
    }

    public function retrieve(\Illuminate\Http\Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'email',
            4 => 'role',
            5 => 'status',
            6 => 'created_at',
            7 => 'action',
        );

        $totalData = User::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $users = User::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $users = User::where('first_name', 'LIKE', "%{$search}%")
                ->orWhere('last_name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = User::where('first_name', 'LIKE', "%{$search}%")
                ->orWhere('last_name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($users)) {
            foreach ($users as $user) {
                $edit = route('admin.users.edit', $user->id);
                $delete = route('admin.users.destroy', $user->id);
                if($user->role == 'superadmin'){
                    $edit = null;
                    $delete = null;
                }
                $nestedData['id'] = $user->id;
                $nestedData['first_name'] = $user->first_name;
                $nestedData['last_name'] = $user->last_name;
                $nestedData['email'] = $user->email;
                $nestedData['role'] = get_user_role_name($user->role);
                $nestedData['status'] = get_state_status($user->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($user->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $roles = Role::all();
        $user = new User();
        $galleries = Gallery::all();
        $fairs = Fair::all();
        $artists = Artist::all();
        return view('users.admin.create', compact('roles', 'user', 'galleries', 'fairs', 'artists'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $password = $data['password'];
            $data['password'] = Hash::make($data['password']);
            $data['user_first_enable'] = 1; // creato da Admin quindi imposto il valore di 'user_first_enable' a 1

            if(isset($data['membership_expiration'])){
                $expiration_date = date('Y-m-d', strtotime(str_replace('/', '-', $data['membership_expiration'])));
                $data['trial_activation_date'] = date('Y-m-d', strtotime(date("Y-m-d", strtotime($expiration_date)) . " -1 month"));
            }

            $user = User::create($data);

            if ($user) {

                // associa condizionalmente gli id della nuova utenza a fiera/gallery/artist
                if (isset($data['fair'])) {
                    $fair = Fair::find($data['fair']);
                    if ($fair) {
                        $user->fair()->save($fair);
                    }
                }

                if (isset($data['gallery'])) {
                    $gallery = Gallery::find($data['gallery']);
                    if ($gallery) {
                        $user->gallery()->save($gallery);
                    }
                }

                if (isset($data['artist'])) {
                    $artist = Artist::find($data['artist']);
                    if ($artist) {
                        $user->artist()->save($artist);
                    }
                }

                $user->assignRole($data['role']);
                if ($data['email_activation']) {
                    $activation_code = hash_hmac('sha256', str_random(40), config('app.key'));
                    $user->activation_code = $activation_code;
                    if ($user->save()) {
                        $to = $user->email;
                        $name = $user->full_name;
                        $subject = "Hi {$user->full_name}! Activate your account on Kooness.com";
                        $link = url("activate/$activation_code");
                        $user_kind_with_article = 'An Administrator';
                        $extras = array(
                            'username' => $user->username,
                            'activation_url' => $link,
                            'password' => $password,
                            'user_kind_with_article' => $user_kind_with_article
                        );

                        Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                            $message->to($to, $name)->subject($subject);
                        });
                    }
                }

                // 4Dem userSave
                $api4Dem = new Api4Dem();
                $api4Dem->userSave($user);

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.users.index')
                ];
                $request->session()->flash("success", "User `{$user->full_name}` is successful created!");
            }
        }
        return response()->json($response);
    }

    public function edit($user)
    {
        $user = User::find($user);
        $roles = Role::all();
        $galleries = Gallery::all();
        $fairs = Fair::all();
        $artists = Artist::all();
        return view('users.admin.edit', compact('user', 'roles', 'galleries', 'fairs', 'artists'));
    }

    public function update(Request $request, $user)
    {

        $user = User::find($user);
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'PATCH' && $user) {
            $data = Input::all();

            $password = false;

            if(isset($data['membership_expiration'])){
                $expiration_date = date('Y-m-d', strtotime(str_replace('/', '-', $data['membership_expiration'])));
                $data['trial_activation_date'] = date('Y-m-d', strtotime(date("Y-m-d", strtotime($expiration_date)) . " -1 month"));
            }

            if ($data['password'] != '') {
                $password = $data['password'];
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }
            if ($user->update($data)) {

                // se user_first_enable = 0 e enable user = yes (durante l'invio)
                if($user->user_first_enable == 0 && $data['status'] == 1) {
                    // se password non è settata...
                    if (!$password) {
                        // generane una casuale
                        $password = str_random(10);
                        $user->password = Hash::make($password);
                    }
                    $user->user_first_enable = 1;
                    $user->save();

                    // ##### manda una mail all'utente
                    // identifico il tipo di utenza che ha creato il nuovo utente
                    $user_kind_with_article = 'An Administrator';
                    $to = $user->email;
                    $name = $user->full_name;
                    $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                    $extras = array(
                        'username' => $user->username,
                        'password' => $password,
                        'user_kind_with_article' => $user_kind_with_article
                    );
                    // invio la mail
                    Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });

                }


                if (isset($data['fair'])) {
                    $fair = Fair::find($data['fair']);
                    $old_fair = $user->fair()->first();
                    if ($fair && $fair != $old_fair) {
                        if ($old_fair) {
                            $old_fair->user_id = null;
                            $old_fair->save();
                        }
                        $user->fair()->save($fair);
                    }
                } else {
                    $fair = $user->fair()->first();
                    if ($fair) {
                        $fair->user_id = null;
                        $fair->save();
                    }
                }

                if (isset($data['gallery'])) {
                    $gallery = Gallery::find($data['gallery']);
                    $old_gallery = $user->gallery()->first();
                    if ($gallery && $gallery != $old_gallery) {
                        if ($old_gallery) {
                            $old_gallery->user_id = null;
                            $old_gallery->save();
                        }
                        $user->gallery()->save($gallery);
                    }
                } else {
                    $gallery = $user->gallery()->first();
                    if ($gallery) {
                        $gallery->user_id = null;
                        $gallery->save();
                    }
                }

                if (isset($data['artist'])) {
                    $artist = Artist::find($data['artist']);
                    $old_artist = $user->artist()->first();
                    if ($artist && $artist != $old_artist) {
                        if ($old_artist) {
                            $old_artist->user_id = null;
                            $old_artist->save();
                        }
                        $user->artist()->save($artist);
                    }
                } else {
                    $artist = $user->artist()->first();
                    if ($artist) {
                        $artist->user_id = null;
                        $artist->save();
                    }
                }

                if ($user->role != $data['role']) {
                    $user->removeRole($user->role);
                    $user->assignRole($data['role']);
                }

                // 4Dem userSave
                $api4Dem = new Api4Dem();
                $api4Dem->userSave($user);

                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.users.index')
                ];
                $request->session()->flash("success", "User `{$user->full_name}` is successful saved!");
            }
        }
        return response()->json($response);
    }

    public function destroy($user)
    {
        $user = User::find($user);
        $user->delete();
        Session::flash('success', "User `{$user->full_name}` is successful removed!");

        // 4Dem userDelete
        $api4Dem = new Api4Dem();
        $api4Dem->userDelete($user);

        return redirect(route('admin.users.index'));
    }
}
