{{-- News - Single --}}
@extends('layouts.app')

@section('title', ($post && $post->meta_title) ? $post->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$post->name}"))

@section('body-class', 'magazine-single')

@section('header-og-abstract', ($post && $post->meta_description) ? $post->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($post && $post->canonical) ? $post->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($post && $post->meta_keywords) ? $post->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($post->url_full_image) ? $post->url_full_image : null)

@section('header-og-url', Request::url())

@section('content')
    <section id="news-container">
        <div class="container boxed-container">
            <div class="container col-container">
                <section id="page-header" class="col">
                    <div class="default-sheet-row">
                        <div class="default-sheet-col">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <p class="news-date">26 October 2019</p>
                                </div>
                            </div>
                            <div class="default-sheet-row title-wrap">
                                <h1>{{ ($post->h1)? $post->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $post->name) }}</h1>
                            </div>
                            <div id="default-sheet-category">
                                <div class="default-sheet-row">
                                    <ul>
                                        @foreach($post->artworks()->get() as $post_artwork)
                                            <li>
                                                <a title="{{ $post_artwork->title }}" href="{{ route('artworks.single', [$post_artwork->slug]) }}">{{ $post_artwork->title }}</a>
                                            </li>
                                        @endforeach
                                        @foreach($post->galleries()->get() as $post_gallery)
                                            <li>
                                                <a title="{{ $post_gallery->name }}" href="{{ route('galleries.single', [$post_gallery->slug]) }}">{{ $post_gallery->name }}</a>
                                            </li>
                                        @endforeach
                                        @foreach($post->artists()->get() as $post_artist)
                                            <li>
                                                <a title="{{ $post_artist->first_name }} {{ $post_artist->last_name }}" href="{{ route('artists.single', [$post_artist->slug]) }}">{{ $post_artist->first_name }} {{ $post_artist->last_name }}</a>
                                            </li>
                                        @endforeach
                                        @foreach($post->fairs()->get() as $post_fair)
                                            <li>
                                                <a title="{{ $post_fair->name }}" href="{{ route('fairs.single', [$post_fair->slug]) }}">{{ $post_fair->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="col three-fourth-col">
                    <div class="news-content">
                        <div class="news-content">
                            <div class="container boxed-container">
                                <div class="ratio-container-1-1">
                                    <div class="ratio-content cover-image lazy" data-src="{{ $post->url_full_image }}"></div>
                                </div>
                                {{--<img src="{{ $post->url_full_image }}">--}}
                            </div>

                            {{--<img class="news-featured-image" src="{{ $post->url_full_image }}">--}}
                            {!! $post->content !!}
                        </div>
                    </div>
                    <div id="default-sheet-info" class="default-sheet-box">
                        <div class="default-sheet-row">
                            <div class="default-sheet-row-cell">
                                <p class="dark-text"><strong>Share</strong></p>
                            </div>
                            <div class="default-sheet-row-cell">
                                <div class="icon-container">
                                    <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                    <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                    <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                    <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                    <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($post->content, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="news-sidebar" class="col one-fourth-col sidebar">
                    @if(count($post_tags))
                        <div class="widget">
                            <div class="widget-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Keywords</h4>
                                </div>
                            </div>
                            <div class="widget-container tagcloud">
                                @foreach($post_tags as $post_tag)
                                    <a title="{{ $post_tag->name }}" href="{{ route('tags.single', ['tags', $post_tag->slug]) }}">{{ $post_tag->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if(count($years))
                        <div class="widget">
                            <div class="widget-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Archive</h4>
                                </div>
                            </div>
                            <div class="widget-container">
                                <ul>
                                    @foreach($years as $year)
                                        <li>
                                            <a href="{{ route('posts.archive.all', [$section, "filtered_by" => $year->year]) }}">{{$year->year}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <div class="container boxed-container">
        <div id="post-nav" class="container col-container-with-offset">
            @if($prev)
                <div class="col one-third-col prev">
                    <div class="default-sheet-col">
                        <a class="prev-a" href="{{ route('posts.single', [$section, $prev->slug]) }}">Prev News</a>
                        <h3><a href="{{ route('posts.single', [$section, $prev->slug]) }}">{{ $prev->title }}</a></h3>
                    </div>
                </div>
            @endif
            @if($next)
                <div class="col one-third-col next">
                    <div class="default-sheet-col">
                        <a class="next-a" href="{{ route('posts.single', [$section, $next->slug]) }}">Next News</a>
                        <h3><a href="{{ route('posts.single', [$section, $next->slug]) }}">{{ $next->title }}</a></h3>
                    </div>
                </div>
            @endif
        </div>
    </div>

    @if(count($other_posts))
        <section id="featured-news">
            <div class="container boxed-container">
                <div class="default-slider-header">
                    <span class="h1">Featured News</span>
                    <p><a class="read-more" href="{{ route('posts.archive.all', [$section]) }}">All featured news</a></p>
                </div>
                <div class="container col-container-with-offset-and-margin">
                    @foreach($other_posts as $other_post)
                        <div class="slider-item one-third-col-with-margin">
                            <div class="slider-item-img wide-box">
                                <a href="{{ route('posts.single', [$section, $other_post->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $other_post->url_wide_box }}"></div>
                                </a>
                            </div>
                            <div class="featured-news-item-txt">
                                <p class="news-date">{{  date('d F Y', strtotime($other_post->date)) }}</p>
                                <div class="default-sheet-row">
                                    <h2>
                                        <a class="dark-text" href="{{ route('posts.single', [$section, $other_post->slug]) }}">{{ $other_post->title }}</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection