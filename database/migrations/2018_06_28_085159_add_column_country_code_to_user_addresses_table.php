<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCountryCodeToUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->string('country_code')->after('country');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('billing_country_code')->after('billing_country');
            $table->string('shipping_country_code')->after('shipping_country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->dropColumn("country_code");
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('billing_country_code');
            $table->dropColumn('shipping_country_code');
        });
    }
}
