<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 12/09/18
 * Time: 08:50
 */

namespace App\Services;

// use App\Services\Notification

use App\Models\User;
use App\Models\Notification as NotifyEntity;

class Notification
{


    const NOTIFICATION_USER_NEW_ARTWORK_BY_ARTIST = 'A new Artwork with name %artwork_name% has been created by the Artist %artist_name%';
    const NOTIFICATION_USER_NEW_ARTWORK_BY_GALLERY = 'A new Artwork with name %artwork_name% has been created by the Gallery %gallery_name%';
    const NOTIFICATION_USER_EDIT_ARTWORK_BY_ARTIST = 'The Artwork with name %artwork_name% has been edited by the Artist %artist_name%';
    const NOTIFICATION_USER_EDIT_ARTWORK_BY_GALLERY = 'The Artwork with name %artwork_name% has been edited by the Gallery %gallery_name%';
    const NOTIFICATION_USER_NEW_ARTIST = 'A new Artist with name %artist_name% has been created by the Gallery %gallery_name%';
    const NOTIFICATION_USER_EDIT_ARTIST = 'The Artist with name %artist_name% has been edited by the Gallery %gallery_name%';
    const NOTIFICATION_USER_EDIT_ARTIST_PROFILE = 'The Artist with name %artist_name% has been edited by itself';
    const NOTIFICATION_USER_EDIT_GALLERY_PROFILE = 'The Gallery with name %gallery_name% has been edited by itself';
    const NOTIFICATION_USER_NEW_EXHIBITION = 'A new Exhibition with name %exhibition_name% has been created by the Gallery %gallery_name%';
    const NOTIFICATION_USER_EDIT_EXHIBITION = 'A new Exhibition with name %exhibition_name% has been created by the Gallery %gallery_name%';
    const NOTIFICATION_USER_NEW_POST_MAGAZINE = 'A new Post with name %post_name% has been created by the Editor %editor_name%';
    const NOTIFICATION_USER_EDIT_POST_MAGAZINE = 'The Post with name %post_name% has been edited by the Editor %editor_name%';
    const NOTIFICATION_USER_NEW_POST_NEWS_BY_GALLERY = 'A new News with name %post_name% has been created by the Gallery %gallery_name%';
    const NOTIFICATION_USER_NEW_POST_NEWS_BY_FAIR = 'A new News with name %post_name% has been created by the Fair %fair_name%';
    const NOTIFICATION_USER_NEW_POST_NEWS_BY_ARTIST = 'A new News with name %post_name% has been created by the Artist %artist_name%';
    const NOTIFICATION_USER_EDIT_POST_NEWS_BY_GALLERY = 'The News with name %post_name% has been edited by the Gallery %gallery_name%';
    const NOTIFICATION_USER_EDIT_POST_NEWS_BY_FAIR = 'The News with name %post_name% has been edited by the Artist %fair_name%';
    const NOTIFICATION_USER_EDIT_POST_NEWS_BY_ARTIST = 'The News with name %post_name% has been edited by the Artist %artist_name%';
    const NOTIFICATION_USER_NEW_ORDER = 'A new Order with id %order_id% has been created by the User %user_name%';
    const NOTIFICATION_USER_NEW_ORDER_WITHOUT_SHIPPING = 'Shipping quote required for order %order_id%';
    const NOTIFICATION_USER_NEW_USER_OFFER = 'A new Offer has been created by the User %user_name% for the Artwork %artwork_name%';
    const NOTIFICATION_USER_NEW_MEMBERSHIP = 'A new membership has been subscribed by %user_name% by joining the plan: %plan_name%';
    const NOTIFICATION_USER_REQUEST_DEACTIVATE_MEMBERSHIP = 'The user %user_name% has requested the deactivation of the %plan_name% membership that will take place on %deactivation_date%';
    const NOTIFICATION_USER_REQUEST_CHANGE_MEMBERSHIP = 'The user %user_name% has requested the change of the %plan_name% with the new membership %new_plan_name%';
    const NOTIFICATION_USER_FAILED_PAYMENT_MEMBERSHIP = 'There was an error during the renewal of membership for the user %user_name%';

    public function __construct()
    {

    }

    // Notification::getAll((int)$user_id) retrieve di tutte le notifiche dell'utente corrente loggato
    // return array di risultati
    public static function getAll($user_id, $offset = null, $limit = null, $order = null, $dir = null, $search = null)
    {
        $user = User::find($user_id);
        if (!$user) {
            return null;
        }

        $query = $user->notifications();
        if($search){
            $query->where('content', 'LIKE', "%{$search}%");
        }
        if($offset){
            $query->offset($offset);
        }
        if($limit){
            $query->limit($limit);
        }
        if($order && $dir){
            $query->orderBy($order, $dir);
        }
        return $query->get();
    }

    public static function countAll($user_id, $search = null){
        $user = User::find($user_id);
        if (!$user) {
            return 0;
        }
        $query = $user->notifications();
        if($search){
            $query->where('content', 'LIKE', "%{$search}%");
        }
        return $query->count();
    }

    public static function getUnreadCount($user_id){
        $user = User::find($user_id);
        if (!$user) {
            return null;
        }
        return $user->notifications()->whereRead(0)->count();
    }

    public static function getUnreadIds($user_id){
        $user = User::find($user_id);
        if (!$user) {
            return null;
        }
        return $user->notifications()->whereRead(0)->pluck('notifications.id')->toArray();
    }

    // Notification::getUnread((int)$user_id) retrieve di tutte le notifiche non lette dell'utente corrente loggato
    //return array di risultati
    public static function getUnread($user_id)
    {
        $user = User::find($user_id);
        if (!$user) {
            return null;
        }
        return $user->notifications()->whereRead(0)->orderBy('created_at', 'DESC')->get();
    }

    // Notification::getRead((int)$user_id) retrieve di tutte le notifiche lette dell'utente corrente loggato
    // return array di risultati
    public static function getRead($user_id)
    {
        $user = User::find($user_id);
        if (!$user) {
            return null;
        }
        return $user->notifications()->whereRead(1)->get();
    }

    // Notification::getNotification((int)$notification_id) retrieve singola notifica by id
    // return singola notifica
    public static function getNotification($id){
         return NotifyEntity::find($id);
    }

    // Notification::setRead((int)$user_id, (array)[id1,id2,...]) set read una lista di notifiche se solo 1 array con un elemento
    // return boolean (true|false)
    public static function setRead($user_id = null, $notifications_ids = [])
    {
        if (!count($notifications_ids)) {
            return true;
        }
        if (!$user_id) {
            return false;
        }
        $user = User::find($user_id);
        foreach ($notifications_ids as $notifications_id) {
            $notify = $user->notifications()->whereNotificationId($notifications_id)->first();
            if ($notify) {
                $notify->pivot->read = 1;
                if (!$notify->pivot->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    // Notification::createNotification((string)null, (string)Notification::NOTIFICATION_USER_EDIT_ARTWORK_BY_ARTIST|...,  (string)'artwork|artist|gallery...', Row di riferimento|gallery)set read una lista di notifiche se solo 1 array con un elemento
    // return $notify class;
    public static function createNotification($title = null, $text = null, $section = null, $row = null)
    {
        $notify = new NotifyEntity();
        $notify->title = $title;
        if ($section != null && $row != null) {
            switch ($section) {
                case 'artwork':
                    if ($text == Notification::NOTIFICATION_USER_NEW_ARTWORK_BY_ARTIST || $text == Notification::NOTIFICATION_USER_EDIT_ARTWORK_BY_ARTIST) {
                        $artist = auth()->user()->artist()->first();
                        if ($artist) {
                            $text = str_replace('%artist_name%', $artist->full_name, $text);
                        }
                    } else {
                        $gallery = auth()->user()->gallery()->first();
                        if ($gallery) {
                            $text = str_replace('%gallery_name%', $gallery->name, $text);
                        }
                    }
                    $text = str_replace('%artwork_name%', $row->title, $text);
                    $notify->content = $text;
                    $notify->artwork_id = $row->id;
                    break;

                case 'artist':
                    if ($text != Notification::NOTIFICATION_USER_EDIT_ARTIST_PROFILE) {
                        $gallery = auth()->user()->gallery()->first();
                        if ($gallery) {
                            $text = str_replace('%gallery_name%', $gallery->name, $text);
                        }
                    }
                    $text = str_replace('%artist_name%', $row->full_name, $text);
                    $notify->content = $text;
                    $notify->artist_id = $row->id;
                    break;

                case 'gallery':
                    $text = str_replace('%gallery_name%', $row->name, $text);
                    $notify->content = $text;
                    $notify->gallery_id = $row->id;
                    break;

                case 'exhibition':
                    $gallery = auth()->user()->gallery()->first();
                    if ($gallery) {
                        $text = str_replace('%gallery_name%', $gallery->name, $text);
                    }
                    $text = str_replace('%exhibition_name%', $row->name, $text);
                    $notify->content = $text;
                    $notify->exhibition_id = $row->id;
                    break;

                case 'post':
                    if ($row->type == 'magazine') {
                        $user = auth()->user();
                        if ($user) {
                            $text = str_replace('%editor_name%', $user->full_name, $text);
                        }
                    } else {
                        if ($text == Notification::NOTIFICATION_USER_NEW_POST_NEWS_BY_ARTIST || $text == Notification::NOTIFICATION_USER_EDIT_POST_NEWS_BY_ARTIST) {
                            $artist = auth()->user()->artist()->first();
                            if ($artist) {
                                $text = str_replace('%artist_name%', $artist->full_name, $text);
                            }
                        } elseif ($text == Notification::NOTIFICATION_USER_NEW_POST_NEWS_BY_GALLERY || $text == Notification::NOTIFICATION_USER_EDIT_POST_NEWS_BY_GALLERY) {
                            $gallery = auth()->user()->gallery()->first();
                            if ($gallery) {
                                $text = str_replace('%gallery_name%', $gallery->name, $text);
                            }
                        } else {
                            $fair = auth()->user()->fair()->first();
                            if ($fair) {
                                $text = str_replace('%fair_name%', $fair->name, $text);
                            }
                        }
                    }
                    $text = str_replace('%post_name%', $row->title, $text);
                    $notify->content = $text;
                    $notify->post_id = $row->id;
                    break;

                case 'user_offer':
                    $user = auth()->user();
                    if ($user) {
                        $text = str_replace('%user_name%', $user->full_name, $text);
                    }
                    $artwork = $row->artwork()->first();
                    if($artwork){
                        $text = str_replace('%artwork_name%', $artwork->title, $text);
                    }
                    $notify->content = $text;
                    $notify->user_offers_id = $row->id;
                    break;

                case 'membership':
                    if($text == Notification::NOTIFICATION_USER_REQUEST_CHANGE_MEMBERSHIP){
                        $change_request = $row->pending_request()->first();
                        if($change_request){
                            $new_plan = $change_request->plan()->first();
                            if($new_plan){
                                $text = str_replace('%new_plan_name%', $new_plan->name, $text);
                            }
                        }
                    }
                    if ($row) {
                        $text = str_replace('%user_name%', $row->full_name, $text);
                        $text = str_replace('%deactivation_date%', date('d/m/Y', strtotime($row->premium_exp_date)), $text);
                    }
                    $plan = $row->mbr_plan()->first();
                    if ($row) {
                        $text = str_replace('%plan_name%', $plan->name, $text);
                    }

                    $notify->content = $text;
                    $notify->user_id = $row->id;
                    break;

                case 'order':
                case 'order_without_shipping':
                    if($text == Notification::NOTIFICATION_USER_NEW_ORDER_WITHOUT_SHIPPING){
                        $text = str_replace('%order_id%', $row->serial, $text);
                        $user = $row->user()->first();
                        if($user){
                            $text = str_replace('%user_name%', $user->full_name, $text);
                            $notify->content = $text;
                            $notify->order_incomplete_id = $row->id;
                        }
                    }
                    if($text == Notification::NOTIFICATION_USER_NEW_ORDER){
                        $text = str_replace('%order_id%', $row->serial, $text);
                        $user = $row->user()->first();
                        if($user){
                            $text = str_replace('%user_name%', $user->full_name, $text);
                            $notify->content = $text;
                            $notify->order_id = $row->id;
                        }
                    }
                    break;
            }
        }
        if ($notify->save()) {
            return $notify;
        } else {
            return false;
        }

    }

    // Notification::addUsers(NotifyEntit, array = [user, user])
    // return boolean true | false
    public static function addUsers(NotifyEntity $notify = null, $users = [])
    {
        if ($notify) {
            if (count($users)) {
                foreach ($users as $user)
                    if (!$notify->users->contains($user->id)) {
                        $notify->users()->attach($user->id);
                    }
            } else {
                return true;
            }

        } else {
            return false;
        }
    }
}