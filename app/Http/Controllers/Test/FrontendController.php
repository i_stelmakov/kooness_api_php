<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artwork;
use App\Models\User;
use App\Models\Order;
use Request;
use Validator;
use App\Services\DHL;

class FrontendController extends FrontendExtendedController
{

    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Test Isotope
    public function isotope()
    {
        return view('test.isotope');
    }
    public function isotopeSorting()
    {
        return view('test.isotope-sorting');
    }

    // Test Grid
    public function grid()
    {
        return view('test.grid');
    }

    // Test DHL
    public function dhl()
    {
        $dhl = new DHL();
        $dhl->setFromCity('Milano');
        $dhl->setFromZip('20124');
        $dhl->setFromCountry('IT');
        $dhl->setToCity('Genova');
        $dhl->setToZip('16121');
        $dhl->setToCountry('IT');
        $dhl->setPieces(Artwork::whereId(4)->get());
        $dhl->getQuote(true);
    }

    // Test Search
    public function search()
    {
        return view('test.search');
    }

    public function api(\Illuminate\Http\Request $request)
    {
        // recupero attuali filtri ed items passati dalla POST request
        $actual_item_list = $request->input('actual_item_list');
        $filter_color = $request->input('filter_color');
        $filter_min_price = $request->input('filter_min_price');
        $filter_max_price = $request->input('filter_max_price');
        $filter_min_height = $request->input('filter_min_height');
        $filter_max_height = $request->input('filter_max_height');
        $filter_min_width = $request->input('filter_min_width');
        $filter_max_width = $request->input('filter_max_width');

        // creo array per risultati
        $results_array = [];

        // effettuo la query

//         $query = Artwork::where("status", "=", "1")
//             ->orWhere("status", "=", "2")
//             ->where(function ($query2) {
//                 $query2->where('price', '>=', '10')
//                 ->orWhere('price', '<=', '50');
//             });
        
        $query = Artwork::whereRaw("(status = 1 OR status = 2)");

        if($filter_min_price)
            $query->whereRaw("price >= '$filter_min_price'");

        if($filter_max_price)
            $query->whereRaw("price <= '$filter_max_price'");

        // ⚠️ Dump & Die Test
        // dd($query->toSql());
        $query->orderBy("created_at", "DESC");
        $result = $query->get();

        $artworks = [];
        $toRemove = $actual_item_list;
        foreach ($result as $row){
            // check if already present in previous, than apply a marker to remove
            if (!empty($actual_item_list)) {
                if(in_array($row->slug, $actual_item_list)){
                    $index = array_search($row->slug, $toRemove);
                    if($index > -1){
                        unset($toRemove[$index]);
                    }
                    continue;
                }
            }

            $tmp = [
                "id" => null,
                "name" => null,
                "status" => null,
                "image" => null,
                "link" => null,
                "year" => null,
                "price" => null,
                "measure_cm" => null,
                "measure_inc" => null,
                "artist_id" => null,
                "artist_name" => null,
                "artist_link" => null,
                "medium" => [],
            ];

            // recupero dati generici
            $tmp['id'] = $row->id;
            $tmp['name'] = $row->title;
            $tmp['status'] = $row->status;
            $tmp['image'] = $row->main_image->url;
            $tmp['link'] = $row->slug;
            $tmp['year'] = $row->year;
            $tmp['price'] = $row->price;
            $tmp['measure_cm'] = $row->width;
            $tmp['measure_inc'] = $row->height;

            // recupero artist connesso
            $artist = \DB::table('artists')->where('id', $row->artist_id)->first();
            $tmp['artist_id'] = $row->artist_id;
            $tmp['artist_name'] = $artist->first_name;
            $tmp['artist_link'] = $artist->slug;

            // recupero medium connessi
            $medium = $row->categories()->where("is_medium", "=", "1")->get();
            if($medium){
                foreach ($medium as $media) {
                    $tmp['medium'][] = [
                        "id" => $media->id,
                        "name" => $media->name,
                        "link" => route('artworks.single', [$media->slug])
                    ];
                }
            }

            $artworks[] = $tmp;
        }
        // ⚠️ Dump & Die Test
        // dd($artworks);


        $results_array = [
            "query" => $query->toSql(),
            "query_bindings" => $query->getBindings(),
            "actual_item_list" => $actual_item_list,
            "filter_color" => $filter_color,
            "filter_min_price" => $filter_min_price,
            "filter_max_price" => $filter_max_price,
            "filter_min_height" => $filter_min_height,
            "filter_max_height" => $filter_max_height,
            "filter_min_width" => $filter_min_width,
            "filter_max_width" => $filter_max_width,
            "new_item_list" => $artworks,
            "old_item_list" => $toRemove
        ];

        return response()->json(['response' => $results_array]);
    }


    public function api2(\Illuminate\Http\Request $request)
    {
        // recupero attuali filtri ed items passati dalla POST request
        return response()->json(['response' => $request->all()]);
    }


    public function mail($type, $mail_kind, $id)
    {
        // Mail per ordini
        if($type == 'order'){
            if($id) {
                $order = Order::find($id);

                // Mail ordine Ricevuto - verso Admin
                if ($mail_kind == 'order-received-admin') {
                    return view('emails.orders.received-admin', compact('order'));
                }
                // Mail ordine ricevuto - verso User
                if ($mail_kind == 'order-received') {
                    return view('emails.orders.received', compact('order'));
                }
                // Mail ordine Completato - verso User
                if ($mail_kind == 'order-completed') {
                    return view('emails.orders.completed', compact('order'));
                }
                // Mail ordine Spedito - verso User
                if ($mail_kind == 'order-shipped') {
                    return view('emails.orders.shipped', compact('order'));
                }
            }
        }
        // Mail per offerte
        else if($type == 'offer') {
            $artwork = Artwork::find($id);
            // Mail offerta Ricevuta - verso Admin
            if($mail_kind == 'offer-received-admin') {
                $user = User::find(1);
                $msg = 'test';
                return view('emails.offers.received-admin', compact('artwork', 'user', 'msg'));
            }
            // Mail offerta Accettata - verso User
            if($mail_kind == 'offer-accepted') {
                return view('emails.offers.accepted', compact('artwork'));
            }
            // Mail offerta Rifiutata - verso User
            if($mail_kind == 'offer-declined') {
                return view('emails.offers.declined', compact('artwork'));
            }
        }
        // Mail per registrazioni
        else if($type == 'registration') {
            // Mail registrazione avvenuta - verso User
            if($mail_kind == 'user-registered') {
                $user = User::find(1);
                return view('emails.auth.registered', compact('user'));
            }
            if ($mail_kind == 'reset-password') {
                $link = 'link';
                return view('emails.auth.reset-password', compact('link'));
            }
            if($mail_kind == 'activation') {
                $user = User::find(1);
                $username = 'pinco';
                $user_kind_with_article = 'Mrs. Smith';
                $password = 'Test_12345';
                $user_kind_with_article = 'Mrs. Smith';
                return view('emails.auth.activation-admin', compact('user', 'username', 'user_kind_with_article', 'password'));
            }

        }
        // Mail per form contatto (richieste utenti)
        else if($type == 'contact-form') {
            // Mail richiesta ricevuta - verso Admin
            if($mail_kind == 'info-request') {
                $name = 'Pinco';
                $email = 'pinco@pallino.com';
                $subject = 'I need help!';
                $body = 'Lorem ipsum sit amet';
                return view('emails.contact-forms.info-request', compact('name', 'email', 'subject', 'body'));
            }
        }
        // Mail per Gallerist
        else if($type == 'gallerists') {

            if($mail_kind == 'activation') {
                $user = User::find(3012);
                $plan = $user->mbr_plan()->first();
                $payment = $user->mbr_payments()->first();

                return view('emails.gallerists.activation', compact('user', 'plan', 'payment'));
            }

            if($mail_kind == 'cancellation') {
                $user = User::find(3012);
                return view('emails.gallerists.cancellation', compact('user'));
            }

            if($mail_kind == 'card-expiration') {
                $user = User::find(3012);
                $card = $user->mbr_card()->first();
                $plan = $user->mbr_plan()->first();

                return view('emails.gallerists.card-expiration', compact('user', 'plan', 'card'));
            }

            if($mail_kind == 'pre-renewal') {
                $user = User::find(3012);
                return view('emails.gallerists.pre-renewal', compact('user'));
            }

            if($mail_kind == 'renewal-not-successful') {
                $user = User::find(3012);
                $card = $user->mbr_card()->first();
                $plan = $user->mbr_plan()->first();
                $payment = $user->mbr_payments()->first();
                $error_message = 'Errore';

                return view('emails.gallerists.renewal-not-successful', compact('user', 'plan', 'card', 'payment', 'error_message'));
            }

            if($mail_kind == 'success-renewal') {
                $user = User::find(3012);
                $card = $user->mbr_card()->first();
                $plan = $user->mbr_plan()->first();
                $payment = $user->mbr_payments()->first();

                return view('emails.gallerists.success-renewal', compact('user', 'plan', 'card', 'payment'));
            }

            if($mail_kind == 'trial-deadline') {
                $user = User::find(3012);
                $card = $user->mbr_card()->first();
                $plan = $user->mbr_plan()->first();
                $payment = $user->mbr_payments()->first();

                $user = User::find(3012);
                return view('emails.gallerists.trial-deadline', compact('user', 'plan', 'card', 'payment'));
            }

            if($mail_kind == 'change-plan') {
                $user = User::find(3012);
                $card = $user->mbr_card()->first();
                $plan = $user->mbr_plan()->first();
                $payment = $user->mbr_payments()->first();

                $user = User::find(3012);
                return view('emails.gallerists.change-plan', compact('user', 'plan', 'card', 'payment'));
            }

        }


    }

    public function allmail()
    {
        return view('emails.allmail');
    }

}