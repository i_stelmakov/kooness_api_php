<div id="new-artist-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">New Artist</h4>
            </div>
            <div class="modal-body">
                <form class="ajax-form" role="form" method="POST" action="{{ route('admin.artists.store') }}"   data-attachment="validate" data-id="" data-referer="artist">
                    @csrf
                    <input type="hidden" name="not_in_kooness" value="true">
                    <div class="col-md-12 col-xs-12">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                Artist Main Information
                            </div>
                            <div class="box-body">
                                <div class="col-md-6  col-xs-12">
                                    <div class="form-group">
                                        <label for="first_name">First name (*)</label>
                                        <input id="first_name" class="form-control" type="text" placeholder="First Name" name="first_name" required>
                                    </div>
                                </div>
                                <div class="col-md-6  col-xs-12">
                                    <div class="form-group">
                                        <label for="last_name">Last name (*)</label>
                                        <input id="last_name" class="form-control" type="text" placeholder="Last Name" name="last_name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                Images
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Picture</label>
                                    </div>
                                    <div class="col-md-6">
                                        <img src="/images/placeholder-wide-box.jpg" class="img-responsive img-preview-artist">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Upload new image… <input type="file" class="img-uploader" data-label="cover" data-preview=".img-preview-artist" data-minWidth="600" data-minHeight="200" data-maxWidth="2048" data-maxHeight="2048" data-cropper="true" data-aspectRatio="1">
                                        </span>
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary submit-new-artist">Add new artist</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </form>
            </div>
        </div>
    </div>
</div>