<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->get('from_checkout')) {
            Session::put("postLogin", "checkout");
        }

        if ($request->get('from_offer_id')) {
            Session::put("artworksIdReferer", base64_decode($request->get('from_offer_id')));
        }

        if ($request->get('from_available_id')) {
            Session::put("artworksIdReferer", base64_decode($request->get('from_available_id')));
        }


        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    }
}
