@extends('layouts.admin')
@section('title', 'Admin | 301 Redirects Bulk Upload')
@section('page-header', '301 Redirects Bulk Upload')

@section('content')
    <style>
        #modal-upload .modal-title i {
            vertical-align: middle;
            font-size: 21px;
            margin-left: 3px;
            color: steelblue;
            cursor: pointer;
        }

        #modal-upload-info .modal-dialog {
            width: 80%
        }

        table.excel {
            margin: 10px 0 15px 0;
            width: 100%;
            text-align: left;
        }

        table.excel thead tr th,
        table.excel tfoot tr th {
            background-color: #eeeeee;
            border: 1px solid #000;
            font-weight: bold;
            color: #333333;
            white-space: nowrap;
            text-align: left;
            padding: 5px;
        }

        table.excel tbody td {
            color: #333333;
            background-color: #FFF;
            vertical-align: middle;
            border: 1px solid #000;
            padding: 5px;
        }
        .display-none{
            display: none;
        }
    </style>
    <div class="row">
        <div class='col-md-12 col-xs-12'>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    File Format
                </div>
                <div class="box-body">
                    <div class="alert alert-warning">Attention: the previous data will be completely replaced, any unspecified url will be lost. Please download the current file and make changes directly to it!</div>

                    <div class="margin-bottom pull-right">
                        <button class="btn btn-primary download-btn" data-type="{{ $type }}">Download current file</button>
                    </div>
                    <fieldset>
                        <ul>
                            <li>
                                <strong>Remove formatting</strong> of the excel file before uploading
                            </li>
                            <li>
                                <strong>Remove any formula or reference </strong> to other cells or files while benefiting from the command
                                <kbd>PASTE SPECIAL: VALUES</kbd></li>
                            <li>The header of the columns is fundamental, must comply with the terms below (CASE-INSENSITIVE: upper and lower case are not binding)</li>
                            <li>The order of the columns is not binding</li>
                            <li>Columns with (<strong> * </strong>) are required</li>
                            <li>The domain name of the url is not binding (The part http: //domain.ext will be ignored)</li>
                        </ul>
                    </fieldset>
                    <div class="row" style="overflow: hidden; padding: 0 15px">
                        <div class="col-lg-12" style="padding: 0;;overflow: hidden;overflow-x: scroll;">
                            <table width="100%" class="excel" cellspacing="1">
                                <thead>
                                <tr>
                                    <th>url (*)</th>
                                    <th>redirect_to (*)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>http://www.kooness.com/artworks/hunt-slonem-untitled-27-painting</td>
                                    <td>http://www.kooness.com/artworks/</td>
                                </tr>
                                <tr>
                                    <td>...</td>
                                    <td>...</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
            @csrf
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Uploads
                </div>
                <div class="box-body">
                    <div class="alert alert-danger error-upload display-none"></div>
                    <form  method="POST" action="{{ route('admin.bulk.upload.upload.xls') }}" accept-charset="UTF-8" enctype="multipart/form-data" class="well form-upload" data-type="{{ $type }}">
                        @csrf
                        <div class="form-group">
                            <label for="file">Select file to upload:</label>
                            <input class="form-control bulk-upload" id="file" name="file" type="file">
                            <p class="help-block">The type field is only binding .xls file with maximum size of
                                <b> 50M </b></p>
                        </div>
                    </form>
                    <fieldset class="fieldset-upload display-none">
                        <legend>Stato Avanzamento</legend>
                        <div class="upload-div display-none-not-important">
                            Upload file on server:
                            <div class="progress progress-upload">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                        </div>
                        <div class="upload-verify-div display-none-not-important">
                            Integrity check :
                            <div class="progress progress-upload-verify">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                        </div>
                        <div class="upload-process-div display-none-not-important">
                            Elaborazione File:
                            <div class="progress progress-upload-process">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                        </div>
                    </fieldset>
                </div>

            </div>
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <button class="btn btn-primary upload-btn">Start Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('script-bulk')
@endsection