@if ($paginator->hasPages())
    <div class="pagination">
        <p>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <a class="disabled"><i class="fa fa-angle-left"></i> Prev</a>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="fa fa-angle-left"></i> Prev</a>
            @endif

            @if($paginator->hasMorePages())
                @section('rel_next')
                    <link rel="next" href="{{ $paginator->nextPageUrl() }}">
                @endsection
            @endif

            @if(!$paginator->onFirstPage())
                @section('canonical'){{ $paginator->url($paginator->currentPage())}}@overwrite
            @endif

            @if(!$paginator->onFirstPage())
                @section('page_title')
                    Result Page {{ $paginator->currentPage() }} for
                @endsection
                @section('rel_prev')
                    <link rel="prev" href="{{ $paginator->previousPageUrl() }}">
                @endsection
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)

                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a class="disabled"><span>{{ $element }}</span></a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="disabled"><span>{{ $page }}</span></a>
                        @else
                            <a href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif

            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a  href="{{ $paginator->nextPageUrl() }}" rel="next">Next <i class="fa fa-angle-right"></i></a>
            @else
                <a class="disabled">Next <i class="fa fa-angle-right"></i></a>
            @endif
        </p>
    </div>
@else
    <div class="pagination">
        <p>
            <a class="disabled"><i class="fa fa-angle-left"></i> Prev</a>
            <a class="disabled"><span>1</span></a>
            <a class="disabled">Next <i class="fa fa-angle-right"></i></a>
        </p>
    </div>
@endif
