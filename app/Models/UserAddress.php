<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $guarded = [];

    protected $appends = ['label', 'number', 'address'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getLabelAttribute()
    {
        return ($this->type == 'private' ? $this->first_name . " " . $this->last_name : $this->company_name) . " - " . $this->address_1 . ", " . $this->zip . " " . $this->city . ($this->state ? ", {$this->state }," : "") . " " . $this->country;
    }

    public function getNumberAttribute()
    {
        return trim(substr($this->address_1, strripos($this->address_1, ',') + 1, strlen($this->address_1)));
    }

    public function getAddressAttribute()
    {
        return trim(substr($this->address_1, 0, strripos($this->address_1, ',')));
    }
}
