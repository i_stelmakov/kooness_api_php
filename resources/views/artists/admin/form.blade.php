@if( roleCheck('superadmin|admin|gallerist|artist|seo') )
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $artist->id }}" data-referer="artist">
        {{ method_field($method) }}
        @csrf

        {{-- Section intro --}}
        <div class="col-md-6 col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        {{-- Associations --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Associations
                </div>
                <div class="box-body row">

                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="categories">Categories</label>
                            <select class="select2 form-control" name="categories[]" multiple="multiple">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ (in_array($category->id, $artist->category_ids))? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <select class="select2 form-control" name="tags[]" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}" {{ (in_array($tag->id, $artist->tag_ids))? 'selected="selected"' : '' }}>{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-xs-12 {{ ( roleCheck('gallerist') ) ? 'hide' : '' }} ">
                            <div class="form-group">
                                <label for="galleries">Represented by</label>
                                <select 
                                    class="select2 form-control" 
                                    name="galleries[]" 
                                    multiple="multiple"
                                >
                                    @foreach($galleries as $gallery)
                                        <option value="{{ $gallery->id }}" {{ (in_array($gallery->id, $artist->gallery_ids) || $gallery_id == $gallery->id )? 'selected="selected"' : '' }}>{{ $gallery->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    @endif

                </div>
            </div>
        </div>

        {{-- Artist Main Information --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Artist Main Information
                </div>
                <div class="box-body row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label for="first_name">Url generated</label>
                            <input
                                id="generated_url"
                                class="form-control"
                                type="text"
                                data-prefix="{{ url('') }}/artists/"
                                value="{{ ($artist->{'slug'})? url('') . '/artists/' . $artist->{'slug'} : '' }}"
                                disabled
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="first_name">First name (*)</label>
                            <input id="first_name"
                                class="form-control"
                                type="text"
                                data-slug="#slug"
                                placeholder="First Name" name="first_name"
                                value="{{ old('first_name', $artist->{'first_name'}) }}"
                                {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                            >
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="last_name">Last name (*)</label>
                            <input id="last_name"
                                class="form-control"
                                type="text"
                                data-slug="#slug"
                                placeholder="Last Name" name="last_name"
                                value="{{ old('last_name', $artist->{'last_name'}) }}"
                                {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                            >
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12{{ ($artist->{'create_from_exhibition'})? ' hide' : '' }} artist-slug-container">
                            <div class="form-group">
                                <label for="slug">Slug (*)</label>
                                <input id="slug"
                                    class="form-control artist-slug"
                                    type="text"
                                    data-url="#generated_url"
                                    placeholder="Slug" name="slug"
                                    value="{{ old('slug', $artist->{'slug'}) }}"
                                    {{ ( roleCheck('seo|gallerist|artist') || $artist->{'create_from_exhibition'} ) ? 'readonly' : 'required' }}
                                    {{ ($artist->{'create_from_exhibition'})? 'disabled' : '' }}
                                >
                            </div>
                        </div>
                    @if(roleCheck('admin|superadmin') && $artist->{'create_from_exhibition'})

                        <div class="col-md-6 col-xs-12 not-in-kooness-container" style="margin-bottom: 20px">
                            <div>
                                <label><input id="not_in_kooness" class="iCheck-input" type="checkbox" name="titled" checked="checked">
                                    Artist not in kooness</label>
                            </div>
                        </div>
                    @endif
                        @if(!roleCheck('seo|artist'))

                            {{--Se non esiste l'utente mostra il box--}}
                            @if(!$artist->user_id)

                                {{--Create Credential--}}
                                <div class="col-md-12">
                                    <label for="created_user">Create user credential</label>
                                    <div class="form-group">
                                        <input
                                            class="iCheck-input form-check-input"
                                            type="radio"
                                            name="created_user"
                                            id="created_user_yes"
                                            value="1"
                                            {{ (!$artist->id && !$artist->user_id)? 'checked' : '' }}
                                        > Yes &nbsp;&nbsp;
                                        <input
                                            class="iCheck-input form-check-input"
                                            type="radio" name="created_user"
                                            id="created_user_no"
                                            value="0" {{ ($artist->id && !$artist->user_id)? 'checked' : '' }}
                                        > No
                                    </div>
                                </div>

                                {{--Email--}}
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email"
                                            class="form-control"
                                            type="email"
                                            data-for-user="true"
                                            placeholder="Email" name="email"
                                            value="{{ old('email', $artist->{'email'}) }}"
                                        >
                                    </div>
                                </div>

                            @endif {{--END - !$artist->user_id--}}

                            {{--User Recap--}}
                            @if( $artist->user_id )
                                {{--User Recap--}}
                                <div class="col-md-12 col-xs-12">
                                    <p>
                                        <b>Associated User data (uneditable)</b><br>
                                        User: {{ $artist->user()->first()->{'username'} }}<br>
                                        User First Name: {{ $artist->user()->first()->{'first_name'} }}<br>
                                        User Last Name: {{ $artist->user()->first()->{'last_name'} }}<br>
                                        User Email: {{ $artist->user()->first()->{'email'} }}
                                    </p>
                                </div>
                            @endif

                            {{-- Se utente è già stato attivato almeno una volta non mostrare più opzione di attivazione--}}
                            @if( ($artist->user()->first() && $artist->user()->first()->{'user_first_enable'} == 0 ) || !$artist->user_id )
                                {{--Enable--}}
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="artist_status">Enable</label>
                                        <select
                                                class="select2 form-control"
                                                name="artist_status"
                                                data-for-user="true"
                                        >
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>
                            @endif {{--END - Se utente è già stato attivato almeno una volta--}}

                        @endif {{--END - !roleCheck('seo|artist')--}}


                </div>
            </div>
        </div>

        @if(!roleCheck('seo'))

            {{-- Artist General Information --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        Artist General Information
                    </div>
                    <div class="box-body row">

                        @if(!roleCheck('artist|gallerist') )
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <input 
                                        id="website"
                                        class="form-control"
                                        type="text"
                                        placeholder="http://www.website.com" 
                                        name="website"
                                        value="{{ old('website', $artist->{'website'}) }}"
                                    >
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Year of birth</label>
                                <input id="date_of_birth"
                                        class="form-control date-year year-mask"
                                        type="text"
                                        placeholder="YYYY" name="date_of_birth"
                                        value="{{ old('date_of_birth', $artist->{'date_of_birth'}) }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_death">Year of death (optional)</label>
                                <input id="date_of_death"
                                        class="form-control date-year"
                                        type="text"
                                        placeholder="YYYY" name="date_of_death"
                                        value="{{ old('date_of_death', $artist->{'date_of_death'}) }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="box box-default box-solid">
                                <div class="box-header with-border">
                                    Place of birth
                                </div>
                                <div class="box-body row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="city_of_birth">City</label>
                                            <input id="city_of_birth"
                                                    class="form-control"
                                                    type="text"
                                                    placeholder="City of birth" name="city_of_birth"
                                                    value="{{ old('city_of_birth', $artist->{'city_of_birth'}) }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country_of_birth">Country</label>
                                            <select class="select2 form-control" name="country_of_birth">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" {{ ($country->id == $artist->country_of_birth)? 'selected="selected"' : '' }}>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="current_location">Current location</label>
                                <input id="current_location"
                                        class="form-control"
                                        type="text"
                                        placeholder="Current location" name="current_location"
                                        value="{{ old('current_location', $artist->{'current_location'}) }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="about_the_artist">About the artist</label>
                                <textarea id="about_the_artist"
                                            class="form-control ck-editor"
                                            placeholder="Content" name="about_the_artist"
                                            rows="3">{{ $artist->{'about_the_artist'} }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meet_the_artist">Insert you Lama App User ID (used for Meet the artist section)</label>
                                {{-- <textarea id="meet_the_artist" class="form-control" placeholder="Meet the artist" name="meet_the_artist" rows="5">{{ $artist->{'meet_the_artist'} }}</textarea> --}}
                                <input type="number" name="meet_the_artist" id="meet_the_artist" class="form-control" placeholder="12345" value="{{ $artist->{'meet_the_artist'} }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="bibliography">Bibliography</label>
                                <textarea id="bibliography"
                                            class="form-control ck-editor"
                                            placeholder="Content" name="bibliography"
                                            rows="3">{{ $artist->{'bibliography'} }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="curriculum_vitae">Curriculum Vitae</label>
                                <textarea id="curriculum_vitae"
                                            class="form-control ck-editor"
                                            placeholder="Content" name="curriculum_vitae"
                                            rows="3">{{ $artist->{'curriculum_vitae'} }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="awards">Awards</label>
                                <textarea id="awards"
                                            class="form-control ck-editor"
                                            placeholder="Content" name="awards"
                                            rows="3">{{ $artist->{'awards'} }}</textarea>
                            </div>
                        </div>
                        
                        @if( !roleCheck('gallerist|artist') )
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="featured">Featured</label>
                                    <div class="form-group">
                                        <input class="iCheck-input form-check-input" type="radio" name="featured"
                                                id="featured_yes"
                                                value="1" {{ ($artist->{'featured'}) ? 'checked="checked"' : '' }}> Yes
                                        &nbsp;&nbsp;
                                        <input class="iCheck-input form-check-input" type="radio" name="featured"
                                                id="featured_no"
                                                value="0" {{ (!$artist->{'featured'}) ? 'checked="checked"' : '' }}> No
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if( (!auth()->user()->hasRole('gallerist') && !auth()->user()->hasRole('artist')) || (auth()->user()->hasRole('gallerist') && $artist->{'status'} != 1 ) )
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="2" {{ ($artist->{'status'} == 2)  ? 'selected="selected"' : ''  }}>Pending</option>
                                        <option value="0" {{ (!$artist->{'status'})  ? 'selected="selected"' : ''  }}>Ready to be Published</option>
                                        @if(!auth()->user()->hasRole('gallerist'))
                                            <option value="1" {{ ($artist->{'status'} == 1)  ? 'selected="selected"' : ''  }}>Visible</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            {{-- Main Images --}}
            @include('partials.image-default', ['item' => $artist])

            @if( !roleCheck('gallerist|artist') )
                {{-- Thumbnail Images --}}
                @include('partials.images', ['item' => $artist, 'square_box_active' => true, 'wide_box_active' => false,'vertical_box_active' => false ])
            @endif
        @endif

        @if( !roleCheck('gallerist|artist') )
            {{-- SEO Management --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        SEO Management
                    </div>
                    <div class="box-body row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_title">SEO - title</label>
                                <input class="form-control"
                                        name="meta_title" type="text"
                                        placeholder="SEO - title"
                                        id="meta_title"
                                        value="{{ $artist->{'meta_title'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_keywords">SEO - keywords</label>
                                <input class="form-control"
                                        name="meta_keywords" type="text"
                                        placeholder="SEO - keywords"
                                        id="meta_keywords"
                                        value="{{ $artist->{'meta_keywords'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="h1">SEO - H1</label>
                                <input class="form-control"
                                        name="h1" type="text"
                                        placeholder="SEO - H1"
                                        id="h1"
                                        value="{{ $artist->{'h1'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="canonical">SEO - Canonical Url</label>
                                <input class="form-control"
                                        name="canonical" type="text"
                                        placeholder="SEO - Canonical Url"
                                        id="canonical"
                                        value="{{ $artist->{'canonical'} }}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meta_description">SEO - Description</label>
                                <textarea id="meta_description"
                                            class="form-control"
                                            placeholder="SEO Description" name="meta_description"
                                            rows="5">{{ $artist->{'meta_description'} }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Footer & Submit --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="pull-right ">
                        <input class="btn btn-primary" type="submit" value="Save"/>
                    </div>
                </div>
            </div>
        </div>
        
    </form>

    @if(Auth::user()->hasRole('gallerist') && isset($artists))
        @include('artists.admin.partials.modal-search-artist')
    @endif
@endif
