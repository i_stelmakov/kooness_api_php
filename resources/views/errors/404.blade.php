@extends('layouts.app')

@section('title', "Kooness | 404")

@section('body-class', 'page')

@section('index_opt', 'noindex')

@section('follow_opt', 'nofollow')

@section('content')
{{--GENERIC--}}

<section id="page-header">
    <div class="container boxed-container">
        <div class="col-container-with-offset">
            <div class="one-half-col-with-margin">
                <h1 class="accent-color max-title text-center">404</h1>
                <p class="text-center">Sorry, the page you are looking for could not be found...</p>
            </div>
            <div class="one-half-col-with-margin">
                {{--Banksy Box--}}
                <div class="banksy-box full-col-with-margin">
                    <div class="banksy-container">
                        <div class="banksy-cover-without-shadow-container">
                            <img class="banksy-cover-without-shadow-sizer" src="/images/404-banksy/banksy-cover-without-shadow.png">
                            <img class="banksy-cover-shadow" src="/images/404-banksy/banksy-cover-shadow-inner.png">
                            <img class="banksy-cover-without-shadow" src="/images/404-banksy/banksy-cover-without-shadow.png">
                            <img class="banksy-artwork-full" src="/images/404-banksy/banksy-artwork-full.png">
                        </div>
                        <img class="banksy-cover-shadow" src="/images/404-banksy/banksy-cover-shadow-outer.png">
                        <div class="banksy-cover-without-shadow-container-2">
                            <img class="banksy-cover-without-shadow-sizer" src="/images/404-banksy/banksy-cover-without-shadow.png">
                            <img class="banksy-artwork-cut" src="/images/404-banksy/banksy-artwork-cut.png">
                        </div>
                        <img class="banksy-background-sizer" src="/images/404-banksy/banksy-base-dimension.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
