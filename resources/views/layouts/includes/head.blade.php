<head>
    {{--Google Tag Manager--}}
    @if( env('APP_ENVIRONMENT_KIND') == 'prod' )
        @include('googletagmanager::head')
    @endif

    <title>@yield('page_title')@yield('title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="@yield('canonical', url()->current())" />

    {{--Next Page--}}
    @yield('rel_next')

    {{--Prev Page--}}
    @yield('rel_prev')

    {{--Favicons & Co--}}
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <link rel="manifest" href="/images/site.webmanifest">
    <link rel="mask-icon" href="/images/safari-pinned-tab.svg" color="#ffffff">
    <link rel="shortcut icon" href="/images/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Kooness">
    <meta name="application-name" content="Kooness">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/images/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <meta name="robots" content="@yield('index_opt', 'index'),@yield('follow_opt', 'follow')">
    {{-- <meta name="robots" content="noindex, nofollow"> --}}
    <!-- Search Engine -->
    <meta name="description" content="@yield('header-og-abstract', 'Buy Art Online')">
    <meta name="image" content="@yield('header-og-image', url('/images/logo.png'))">
    <meta name="keywords" content="@yield('seo-keywords', 'Art, Buy, Kooness')">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="@yield('header-og-title', 'Kooness')">
    <meta itemprop="description" content="@yield('header-og-abstract', 'Buy Art Online')">
    <meta itemprop="image" content="@yield('header-og-image', url('/images/logo.png'))">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="@yield('header-og-title', 'Kooness')">
    <meta name="twitter:description" content="@yield('header-og-abstract', 'Buy Art Online')">
    <meta name="twitter:site" content="@KoonessOfficial">
    <meta name="twitter:creator" content="@KoonessOfficial">
    <meta name="twitter:image:src" content="@yield('header-og-image', url('/images/logo.png'))">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="@yield('header-og-title', 'Kooness')">
    <meta name="og:description" content="@yield('header-og-abstract', 'Buy Art Online')">
    <meta property="og:image" content="@yield('header-og-image', url('/images/logo.png'))">
    <meta property="og:image:width" content="279">
    <meta property="og:image:height" content="279">
    <meta name="og:url" content="@yield('header-og-url', url(''))">
    <meta name="og:site_name" content="@yield('header-og-title', 'Kooness')">
    <meta name="fb:admins" content="764898470223261">
    <meta name="fb:app_id" content="741496009317839">
    <meta name="og:type" content="website">

    <script>
        @if(env('APP_DEBUG', false))
            window.env = true;
        @else
            window.env = false;
        @endif
        
        @if( env('APP_ENVIRONMENT_KIND', false) )
            window.envKind = '{{ env('APP_ENVIRONMENT_KIND') }}';
        @endif
    </script>



    <!-- Generic Script -->
    <script src="/js/main-frontend.js?v={{ get_asset_rev() }}"></script>

    <!-- cdnjs -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.10/jquery.lazy.plugins.min.js"></script>
    <script type="text/javascript">
        $(function($) {
            $("img.lazy, div.lazy, h6.lazy").lazy({
                afterLoad: function(element) {
                    element.addClass('animation-in');
                    if( $('.grid').length > 0 && $('.grid').isotope){
                        $('.grid').isotope('layout');
                    }
                    if($('#masonry-grid').length > 0 && $('#masonry-grid').masonry){
                        $('#masonry-grid').masonry('layout');
                    }
                }
            });
        });
    </script>
    <style>
        .lazy {
            opacity: 0;
            transition: all 750ms ease-in-out;
        }
        .lazy.animation-in {
            opacity: 1;
        }
    </style>

    <!-- Specific Page Scripts -->
    @yield('scripts')
    @yield('maps')

    <!-- Generic Style (Main) -->
    <style>
        #loader-body,
        #loader-modal {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 75px;
            height: 75px;
            margin: -34.5px 0 0 -37.5px;
            border: 8px solid #ececec;
            border-radius: 50%;
            border-top: 8px solid #333333;
            width: 60px;
            height: 60px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        #loader-body {
            position: fixed;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
                        transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                        transform: rotate(360deg);
            }
        }

        /* Add animation to "page content" */

        .content-loader {
            position: relative;
            opacity: 0;
        }

        .content-loader.content-loaded {
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
            opacity: 1;
        }

        @-webkit-keyframes animatebottom {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }

        @keyframes animatebottom {
            from {
                opacity: 0;
            }

            to {
                opacity: 1;
            }
        }
    </style>

    <!-- Generic Style (Deferred) -->
    <noscript id="deferred-styles">
        {{-- <link rel="stylesheet" type="text/css" href="small.css"/> --}}
        <link rel="stylesheet" type="text/css" href="/css/main-frontend.css?v={{ get_asset_rev() }}">
    </noscript>
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>

    <!-- Specific Page Style -->
    @yield('styles')
    
    <!-- range-touch.js file to enable slider on touch devices -->
    {{-- <script type="text/javascript" src="https://cdn.rawgit.com/dwyl/range-touch/master/range-touch.min.js"></script> --}}

    @if($envKind == 'prod')

        <!-- iubenda cookie banner -->
        <style type="text/css">
            #iubenda-cs-banner {
                bottom: 0px !important;
                left: 0px !important;
                position: fixed !important;
                width: 100% !important;
                z-index: 99999998 !important;
                background-color: #48b6ac;
            }
            .iubenda-cs-content {
                display: block;
                margin: 0 auto;
                padding: 20px;
                width: auto;
                font-family: Helvetica,Arial,FreeSans,sans-serif;
                font-size: 14px;
                background: #48b6ac;
                color: #fff;}
            .iubenda-cs-rationale {
                max-width: 900px;
                position: relative;
                margin: 0 auto;
            }
            .iubenda-banner-content > p {
                font-family: Helvetica,Arial,FreeSans,sans-serif;
                line-height: 1.5;
                color: white;
            }
            .iubenda-cs-close-btn {
                margin:0;
                color: #fff;
                text-decoration: none;
                font-size: 14px;
                position: absolute;
                top: 0;
                right: 0;
                border: none;
            }
            .iubenda-cs-cookie-policy-lnk {
                text-decoration: underline;
                color: #fff;
                font-size: 14px;
                font-weight: 900;
            }
        </style>
        <script type="text/javascript">
            var _iub = _iub || [];
            _iub.csConfiguration = {"consentOnScroll":false,"priorConsent":false,"lang":"en","siteId":1364845,"cookiePolicyId":66328477, "banner":{ "slideDown":false,"applyStyles":false,"textColor":"#ffffff","backgroundColor":"#4cb6ac" } };
        </script>
        <script type="text/javascript" src="//cdn.iubenda.com/cookie_solution/safemode/iubenda_cs.js" charset="UTF-8" async></script>

        <!--google analytics -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', '{{ env('GOOGLE_ANALYTICS_ID', false) }}', 'auto');
            ga('send', 'pageview');
        </script>

        <!-- Facebook Pixel -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '{{ env('FACEBOOK_PIXEL_ID', false) }}');
            fbq('track', "PageView");
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=741496009317839&amp;ev=PageView&amp;noscript=1" /></noscript>

        <!-- Hotjar Tracking -->
        <script>
            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)};
                h._hjSettings = {hjid: {{ env('HOTJAR_ID', false) }}, hjsv: 5};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>

        <!-- Zendesk Chat Script-->
        <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="https://v2.zopim.com/?2eE0Dq9UFg6doIZ2soia0dUPpkJRevDy";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
        </script>

    @endif

</head>
