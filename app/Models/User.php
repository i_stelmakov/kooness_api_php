<?php

namespace App\Models;

use App\Notifications\MailResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;

    use HasRoles;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'first_name', 'last_name', 'status', 'activation_code', 'is_premium', 'trial_activation_date', 'is_trial', 'premium_exp_date', 'user_first_enable'
    ];

    protected $appends = [
        'full_name',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getRoleAttribute()
    {
        if ($this->roles()->first())
            return $this->roles()->first()->name;
    }

    public function gallery()
    {
        return $this->hasOne('App\Models\Gallery', 'user_id', 'id');
    }

    public function artist()
    {
        return $this->hasOne('App\Models\Artist', 'user_id', 'id');
    }


    public function fair()
    {
        return $this->hasOne('App\Models\Fair', 'user_id', 'id');
    }

    public function addresses(){
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id');
    }

    public function payments(){
        return $this->hasMany('App\Models\UserPayment', 'user_id', 'id');
    }

    public function collectionArtworks(){
        return $this->belongsToMany('App\Models\Artwork', 'users_artworks', 'user_id' ,'artwork_id');
    }

    public function collectionArtists(){
        return $this->belongsToMany('App\Models\Artist', 'users_artists', 'user_id' ,'artist_id');
    }

    public function collectionGalleries(){
        return $this->belongsToMany('App\Models\Gallery', 'users_galleries', 'user_id' ,'gallery_id');
    }

    public function collections(){
        return $this->hasMany('App\Models\UserCollection', 'user_id', 'id');
    }

    public function offers(){
        return $this->hasMany('App\Models\UserOffer', 'user_id', 'id');
    }

    public function notifications(){
        return $this->belongsToMany('App\Models\Notification', 'user_notifications', 'user_id' ,'notification_id')->withPivot(['read']);
    }

    public function mbr_address(){
        return $this->hasOne('App\Models\MbrAddress', 'user_id', 'id');
    }

    public function mbr_card(){
        return $this->hasOne('App\Models\MbrCard', 'user_id', 'id');
    }

    public function mbr_plan(){
        return $this->hasOne('App\Models\MbrPlan', 'id', 'current_plan_id');
    }

    public function mbr_payments(){
        return $this->hasMany('App\Models\MbrPayment', 'user_id', 'id');
    }

    public function mbr_failed_payments(){
        return $this->hasMany('App\Models\MbrPayment', 'id', 'renewal_failed_payment_id');
    }

    public function getActivationDateAttribute(){
        $firstPayment = $this->mbr_payments()->whereStatus(1)->orderBy('created_at', 'ASC')->first();
        if($firstPayment) {
            return $firstPayment->created_at;
        }
        return null;
    }

    public function have_pending_request(){
        return $this->hasMany('App\Models\MbrChangePlanRequest', 'user_id', 'id')->count();
    }

    public function pending_request(){
        return $this->hasMany('App\Models\MbrChangePlanRequest', 'user_id', 'id');
    }


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }
}
