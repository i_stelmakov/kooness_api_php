@extends('layouts.admin')
@if(Request::is('admin/magazine*'))
    @section('title', "Admin | Edit Post `{$post->title}`")
    @section('page-header', "Admin - Edit Post `{$post->title}`")
@else
    @section('title', "Admin | Edit News `{$post->title}`")
    @section('page-header', "Admin - Edit News `{$post->title}`")
@endif
@section('content')
    @include('posts.admin.form', ['action' => (Request::is('admin/magazine*')) ? route('admin.magazine.update', [$post->id]) : route('admin.news.update', [$post->id]), 'method' => 'PATCH'])
@endsection