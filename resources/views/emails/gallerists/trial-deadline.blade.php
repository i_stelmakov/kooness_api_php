{{-- Mail - Trial Membership Deadline --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_TRIAL_DEADLINE}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
			We remind you that your trial period is about to expire.<br>
			We invite you to buy the membership plan so as not to lose the ability to edit your content on the site.
		</p>

	</div>
@stop
