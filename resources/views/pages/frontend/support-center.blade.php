@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : "Kooness | {$page->title}")

@section('body-class', 'page')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
{{--SERVICES--}}
    <section id="page-header">
        <div class="container boxed-container">
            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : $page->title }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section id="user-tabs" class="tab-section">
    <div class="container boxed-container">

        <div class="container col-container-with-offset-and-margin container-vertical-padding-bottom">

            <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                <ul>
                    <li class="tab-button current-tab-item" data-id="1"><a href="#my-user-page">My User page</a></li>
                    <li class="tab-button" data-id="2"><a href="#buying-on-kooness">Buying on Kooness</a></li>
                    <li class="tab-button" data-id="3"><a href="#artworks-on-kooness">Artworks on Kooness</a></li>
                    <li class="tab-button" data-id="4"><a href="#payments">Payments</a></li>
                    <li class="tab-button" data-id="5"><a href="#make-an-offer">Make an Offer</a></li>
                    <li class="tab-button" data-id="6"><a href="#shippings-and-returns">Shippings and Returns</a></li>
                </ul>
            </div>

            <!-- Tab 1 !-->
            <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <h2>My user page</h2>
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">How do I manage my user profile?</h4>
                            <div class="accordion-content active" data-slide="1">
                                By creating an account on Kooness, you have access to numerous advantages. Once you are logged with your ID and password, you have the chance to create your digital art collection and customize your browsing preferences. Here you can select your favorite artworks, artists and galleries and add them to your virtual collection. On your profile settings you can see your personal information and edit them whenever you want. Here you can add or edit your own address and credit cards details and also have a track of your orders. As you are logged and want to share your virtual art collection with some friends, you can also add one or more social media accounts such as Facebook and Twitter. Finally, by signing up to our newsletter you can get advanced access to the latest contemporary artworks each week directly in your inbox. Plus, learn more about artists and artworks you love most, stay updated with all breaking news, exhibition openings and events from the contemporary art world.
                            </div>
                            <h4 class="accordion-button" data-accordion="2">I forgot my Kooness password. How can I log in?</h4>
                            <div class="accordion-content" data-slide="2">
                                    To reset your user password, click the “Login” link located in the site header and then click “Forgot Your Password.” You will be required to enter your email address to receive a temporary password via email, which you can use to log in to the site. You can then visit the “User Page” to officially change your password.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab 2 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <h2>Buying on Kooness</h2>
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">Why buy on Kooness?</h4>
                            <div class="accordion-content active" data-slide="1">
                                We select the best contemporary art galleries in the world in order to offer you the largest and most various choice of artworks. By purchasing on Kooness, you can discover many works of different categories, genres and sizes made by selected emerging and established contemporary artists, offered by quality partners chosen by us. Moreover, Kooness was founded to offer you an uncommon browsing experience: each gallery and each artist have a personalized page on which you can go to explore their works and see their artworks. Each work of art is presented with high quality images and well explained. The website is very interactive, you can share artworks and artists on your social network, follow the gallery you like, create your virtual art collection and read all latest news from the art world.
                            </div>
                            <h4 class="accordion-button" data-accordion="2">How do I search for artworks, artists, or galleries?</h4>
                            <div class="accordion-content" data-slide="2">
                                You can search for a specific artist, artwork or gallery by typing your terms in the search box located at the top of the site and click what you are looking for. To explore all available artworks click the “Artworks” link in the black navigation bar. On the Artworks page you will be given the option to sort and filter results by category, medium, price, size, artist and gallery. To view all artists, click the “Artists” link in the black navigation bar and filter them by category or country of origin. Likewise, clicking the “Galleries” link in the black navigation bar will enable you to browse all of the galleries from our network.
                            </div>
                            <h4 class="accordion-button" data-accordion="3">Can I view the artwork in person prior to purchase?</h4>
                            <div class="accordion-content" data-slide="3">
                                Our inventory is housed all over the world, so we are unable to accommodate viewing sessions prior to purchase. Please use our “Zoom” and “View in a Room” features for a more detailed look at an object. These tools are denoted on the item detail page by the magnifying glass and eye icons.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab 3 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <h2>Artworks on Kooness</h2>
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">What does the price of the artworks sold on Kooness correspond to?</h4>
                            <div class="accordion-content active" data-slide="1">
                                All the artworks featured on the website are sold at the public price. You, as buyer, do not pay any commission. Shipping costs are not part of the final price, but they are visible when you select the destination country for the shipping. For Extra European Country different taxation rules and additional charges may apply. The prices are DDU (Duty Delivery Unpaid). You may need to pay import duties upon receipt of the products. Neither we nor the galleries have any control over these charges and we cannot predict their amount. You will be responsible for payment of any such import duties and taxes that are not included.
                            </div>
                            <h4 class="accordion-button" data-accordion="2">How can I know if a work of art is authentic?</h4>
                            <div class="accordion-content" data-slide="2">
                                Each work exhibited on the website is delivered to you with the authenticity certificate.
                            </div>
                            <h4 class="accordion-button" data-accordion="3">How do I know if an artwork is available?</h4>
                            <div class="accordion-content" data-slide="3">
                                All artworks are available for sale unless indicated as “Sold”. If you encounter a sold item and you wish to buy it or something similar please send an e-mail at info@kooness.com and our team will help you in find what you are looking for.
                            </div>
                            <h4 class="accordion-button" data-accordion="4">How do I know when artists or galleries I like add new artworks?</h4>
                            <div class="accordion-content" data-slide="4">
                                You will receive notifications in your personal profile when items by the artists or galleries you “follow” are added to the site. To follow an artist or a gallery, visit the artist’s bio page or gallery’s page and click the heart icon.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab 4 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="4">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <h2>Payments</h2>
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">What payment methods does Kooness accept?</h4>
                            <div class="accordion-content active" data-slide="1">
                                Once you click on the artwork you love most and desire to buy it, you have different payments options showed on you cart. Our online checkout system accepts PayPal, all major credit cards (American Express, Visa, MasterCard, Maestro) and bank transfer. Our prices are listed in EUR, USD and GBP.
                            </div>
                            <h4 class="accordion-button" data-accordion="2">Bank transfer</h4>
                            <div class="accordion-content" data-slide="2">
                                once you validate your order, we verify the availability of the artwork to the seller and you will be immediately noticed by email. Our IBAN is already specified. The work of art will be sent by the seller as soon as we receive your transfer. Thus, each seller reserves the artwork ordered exclusively to you within 5 working days from the date of the validation of availability. If we do not receive your bank transfer within this period of time, the order will be cancelled.
                            </div>
                            <h4 class="accordion-button" data-accordion="3">Credit card</h4>
                            <div class="accordion-content" data-slide="3">
                                you can pay with both credit and debit card, all the major ones are accepted. If you are logged, you can select your credit card and details from the list, if you are logged, or add new ones.
                            </div>
                            <h4 class="accordion-button" data-accordion="4">PayPal</h4>
                            <div class="accordion-content" data-slide="4">
                                if you have a PayPal account you can simply pay entering your username and password without any card or bank credentials.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <!-- Tab 5 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="5">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">Make an Offer</h4>
                            <div class="accordion-content active" data-slide="1">
                                Are you a newbie art collector? We are here to help you in starting and building your own art collection. For this reason, our users can have the chance to propose the desired price for the artworks they love most. Once you click on the “make an offer” button, an automatic email box will be generated to submit your request. Here you can write us and submit all your questions in details. We will be responsible of contacting the sellers and immediately submit your offer to them. Meanwhile, the artwork you are interest in will be automatically considered as pending. In case they accept, the final price will change and updated on the website. Don’t be shy, just try.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tab 6 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="6">
                <div class="container col-container-with-margin">
                    <div class="col full-col-with-margin">
                        <div class="accordion-box">
                            <h4 class="accordion-button" data-accordion="1">How are the works delivered?</h4>
                            <div class="accordion-content active" data-slide="1">
                                Once you have validated your order, Kooness will send to the gallery/seller the shipping information. The seller has a precise number of days (depending on each work) to drop off the work at a transporter authorized to deliver artworks. You will receive an email from us with the name of transporter, the package number and the approximate date of delivery so you will be able to track your order.
                            </div>
                            <h4 class="accordion-button" data-accordion="2">How are shipping costs determined?</h4>
                            <div class="accordion-content" data-slide="2">
                                Shipping costs are determined based on the price, dimensions, and weight of the purchased artworks, as well as by the item’s origin and the shipping destination. All shipping costs include packaging, handling, and insurance fees. To estimate shipping costs on your order, select your shipping address and location on the checkout page.
                            </div>
                            <h4 class="accordion-button" data-accordion="3">How is shipping insurance calculated?</h4>
                            <div class="accordion-content" data-slide="3">
                                Insurance is automatically included in shipping fees and covers door-to-door handling.
                            </div>
                            <h4 class="accordion-button" data-accordion="4">Do I need to sign for my package upon delivery?</h4>
                            <div class="accordion-content" data-slide="4">
                                Yes, all Kooness packages must be accepted and signed for upon delivery. Due to the valuable nature of artworks and design objects, packages should not be left unattended.
                            </div>
                            <h4 class="accordion-button" data-accordion="5">Can I return an artwork if I change my mind?</h4>
                            <div class="accordion-content" data-slide="5">
                                For any purchase from one of our galleries established in the European Economic Area (European Union countries plus Iceland, Norway and Liechtenstein), if you are a resident of this area, you have a period of 14 days from the date of the reception of your order to exercise your right of revocation to Kooness, without having to justify the reason. For purchases made from a gallery that is not established in the European Economic Area or if you are not a resident of this area this right of cancellation shall not apply. You can use your revocation right directly to Kooness by sending an email to <a href="mailto:shop@kooness.com">shop@kooness.com</a>. Kooness team will get in touch with you and will explain the return process. In case of exercise of the revocation right in the period indicated, only the price of the artwork(s) bought will be refunded by the seller; shipping and return costs are charged to the buyer. The artwork must be returned in perfect conditions and in its original packaging. For the artworks you received in damaged conditions, you will have to notice Kooness as soon as you receive the package at shop@kooness.com. The reimbursement will occur with the same payment method of the artwork's purchase within 5 working days from the day that Kooness will receive the artwork.
                            </div>
                            <h4 class="accordion-button" data-accordion="6">What happens if I do not receive my artwork or if it is damaged?</h4>
                            <div class="accordion-content" data-slide="6">
                                Every buyer has the chance to signal any claim regarding the orders within 14 days from the reception of the artwork. You can just send an email to <a href="mailto:shop@kooness.com">shop@kooness.com</a> with detailed photographs of the damage along with name, order number and artwork’s title. We are here to support you and work with you to order a replacement.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
</section>


<section id="info">
    <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
        <div class="container col-container-with-offset-and-margin">
            <div class="full-col-with-margin">
                @if(Session::get('send') && Session::get('send') == 'success')
                    <div class="in-page-warning success">
                        Message successful sent! We will reply as soon as possible.
                    </div>
                @endif
                <h2>Send a message</h2>
                <form class="contact-form col-container-with-offset-and-margin k-form" method="POST" action="{{ route('send.contact.message') }}">
                    {!! Honeypot::generate('honey_name', 'honey_time') !!}
                    @csrf
                    <div class="one-half-col-with-margin">
                        <input type="text" name="name" placeholder="Name" required>
                    </div>
                    <div class="one-half-col-with-margin">
                        <input type="email" name="email" placeholder="Email" required>
                    </div>
                    <div class="full-col-with-margin">
                        <input type="text" name="subject" placeholder="Subject" required>
                    </div>
                    <div class="full-col-with-margin">
                        <textarea name="body" placeholder="Message" rows="6" required></textarea>
                    </div>
                    <div class="full-col-with-margin form-privacy">
                        <p>
                            <input name="privacy" type="checkbox" required> I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                            <p class="errorTxt"></p>
                        </p>
                    </div>
                    <div class="full-col-with-margin">
                        <input class="default-input-button" type="submit" value="Submit">
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>

@endsection
