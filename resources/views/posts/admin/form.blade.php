@if(roleCheck('superadmin|admin|editor|gallerist|artist|fair|seo'))
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $post->id }}" data-referer="posts">
        {{ method_field($method) }}
        @csrf

        {{-- Section intro --}}
        <div class="col-md-6 col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        {{-- Details --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12  col-xs-12">
                            <div class="form-group">
                                <label for="first_name">Url generated</label>
                                <input 
                                    id="generated_url" 
                                    class="form-control" 
                                    type="text" 
                                    data-prefix="{{ url('') }}/posts/{{ Request::is('admin/magazine*')? 'magazine' : 'news' }}/" 
                                    value="{{ ($post->{'slug'})? url('') . '/posts/' . ( Request::is('admin/magazine*')? 'magazine' : 'news') . "/" .  $post->{'slug'} : '' }}" 
                                    disabled
                                >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title (*)</label>
                                <input 
                                    id="title" 
                                    class="form-control" 
                                    type="text" 
                                    placeholder="Title" 
                                    name="title" 
                                    value="{{ old('title', $post->{'title'}) }}" 
                                    data-slug="#slug" 
                                    {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                                >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="slug">Slug (*)</label>
                                <input 
                                    id="slug" 
                                    class="form-control" 
                                    type="text" 
                                    placeholder="Slug" 
                                    name="slug" 
                                    value="{{ old('name', $post->{'slug'}) }}" 
                                    data-url="#generated_url" 
                                    {{ ( roleCheck('gallerist|seo|artist|fair|editor') ) ? 'readonly' : 'required' }}
                                >
                            </div>
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        <div class="row">

                            @if( !roleCheck('gallerist|artist|fair') )
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="author">Author</label>
                                        <input id="author" class="form-control" type="text" placeholder="Author" name="author" value="{{ old('author', $post->{'author'}) }}">
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input id="date" class="form-control datepicker date-mask" type="text" placeholder="DD/MM/AAAA" name="date" value="{{ old('name', $post->{'date'}) }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="content">Content (*)</label>
                                    <textarea id="content" class="form-control ck-editor" placeholder="Content" name="content" rows="5" required>{{ $post->content }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="0" {{ (!$post->{'status'})  ? 'selected="selected"' : ''  }}>Draft</option>
                                        @if ( !roleCheck('editor') )
                                            <option value="1" {{ ($post->{'status'} == 1)  ? 'selected="selected"' : ''  }}>Published</option>
                                        @endif
                                        <option value="2" {{ ($post->{'status'} == 2)  ? 'selected="selected"' : ''  }}>Featured</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        @if(!roleCheck('seo'))
            {{-- Refer to --}}
            @if(Request::is('admin/news*'))
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            Referer to
                        </div>
                        <div class="box-body">
                            <div class="row">

                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="artworks">Artworks</label>
                                        <select class="select2 select-for-news form-control" name="artworks[]" multiple="multiple">
                                            @foreach($artworks as $artwork)
                                                <option value="{{ $artwork->id }}" {{ (in_array($artwork->id, $post->artwork_ids))? 'selected="selected"' : '' }}>{{ $artwork->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12 {{ ( roleCheck('artist') ) ? 'hide' : '' }} ">
                                    <div class="form-group">
                                        <label for="artists">Artists</label>
                                        <select class="select2 select-for-news form-control" name="artists[]" multiple="multiple">
                                            @foreach($artists as $artist)
                                                <option value="{{ $artist->id }}" {{ (in_array($artist->id, $post->artist_ids) || $artist_id == $artist->id )? 'selected="selected"' : '' }}>{{ $artist->first_name }} {{ $artist->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                @if( !roleCheck('artist')  )
                                    <div class="col-md-6 col-xs-12 {{ ( roleCheck('gallerist') ) ? 'hide' : '' }} ">
                                        <div class="form-group">
                                            <label for="galleries">Galleries</label>
                                            <select class="select2 select-for-news form-control" name="galleries[]" multiple="multiple">
                                                @foreach($galleries as $gallery)
                                                    <option 
                                                        value="{{ $gallery->id }}" 
                                                        {{ (in_array($gallery->id, $post->gallery_ids) || $gallery_id == $gallery->id ) ? 'selected="selected"' : '' }}
                                                    >
                                                    {{ $gallery->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif

                                @if( !roleCheck('gallerist|artist') )
                                    <div class="col-md-6 col-xs-12 {{ ( roleCheck('fair') ) ? 'hide' : '' }} ">
                                        <div class="form-group">
                                            <label for="fairs">Fairs</label>
                                            <select class="select2 select-for-news form-control" name="fairs[]" multiple="multiple">
                                                @foreach($fairs as $fair)
                                                    <option value="{{ $fair->id }}" {{ (in_array($fair->id, $post->fair_ids) || $fair_id == $fair->id ) ? 'selected="selected"' : '' }}>{{ $fair->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        {{-- Associations --}}
        @if((Request::is('admin/magazine*') && roleCheck('seo')) || !roleCheck('seo') )
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                            Associations
                    </div>
                    <div class="box-body">
                        <div class="row">
                            @if(!roleCheck('seo'))
                                <div class="col-md-{{ Request::is('admin/magazine*')? '6':'12' }} col-xs-12">
                                    <div class="form-group">
                                        <label for="tags">Tags</label>
                                        <select class="select2 form-control" name="tags[]" multiple="multiple">
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}" {{ (in_array($tag->id, $post->tag_ids))? 'selected="selected"' : '' }}>{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(Request::is('admin/magazine*'))
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label for="categories">Categories</label>
                                    <select class="select2 form-control" name="categories[]" multiple="multiple">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ (in_array($category->id, $post->category_ids))? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(!roleCheck('seo'))
            {{-- Main Images --}}
            @include('partials.image-default', ['item' => $post])

            @if( !roleCheck('seo|gallerist|artist') )
                {{-- Thumbnail Images --}}
                @include('partials.images', ['item' => $post, 'square_box_active' => false, 'wide_box_active' => true,'vertical_box_active' => false ])
            @endif
        @endif

        @if( !roleCheck('gallerist|artist|editor') )
            {{-- SEO Management --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        SEO Management
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_title">SEO - title</label>
                                    <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $post->{'meta_title'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_keywords">SEO - keywords</label>
                                    <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $post->{'meta_keywords'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="h1">SEO - H1</label>
                                    <input class="form-control"
                                            name="h1" type="text"
                                            placeholder="SEO - H1"
                                            id="h1"
                                            value="{{ $post->{'h1'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="canonical">SEO - Canonical Url</label>
                                    <input class="form-control"
                                            name="canonical" type="text"
                                            placeholder="SEO - Canonical Url"
                                            id="canonical"
                                            value="{{ $post->{'canonical'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_description">SEO - Description</label>
                                    <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $post->{'meta_description'} }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Footer & Submit --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endif