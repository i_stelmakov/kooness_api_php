@extends('layouts.app')

@section('title', ($category && $category->meta_title) ? $category->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : 'Kooness | Artists'))

@section('body-class', 'artists-archive')

@section('header-og-abstract', ($category && $category->meta_description) ? $category->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($category && $category->canonical) ? $category->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($category && $category->meta_keywords) ? $category->meta_keywords : ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Artist Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span>
                                @if($category && $category->h1)
                                    <h1>{{  $category->h1 }}</h1>
                                @elseif(($seo_data && $seo_data->h1))
                                    <h1>{{  $seo_data->h1 }}</h1>
                                @elseif(isset($category))
                                    <h1>All {{ $category->name }} Artists</h1>
                                @else
                                    @if(isset($fair) && $fair)
                                        <h1>Artists in fair {{ $fair->name }}</h1>
                                    @elseif(isset($gallery) && $gallery)
                                        <h1>Artists in gallery {{ $gallery->name }}</h1>
                                    @elseif(isset($exhibition) && $exhibition)
                                        <h1>Artists in exhibition {{ $exhibition->name }}</h1>
                                    @else
                                        <h1>Artists</h1>
                                    @endif
                                @endif

                                @if( $category && $category->description )

                                <div class="show-more-text-container">
                                    <div class="show-more-content">
                                        <div class="show-more-content-inner">
                                            {!! $category->description !!}
                                        </div>
                                    </div>
                                    <div class="show-more-button">
                                        <a href="#">Show more</a>
                                    </div>
                                </div>​

                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="page-header-filter">
                        <div class="default-sheet-row-cell">
                            <p>Nationality</p>
                            <form>
                                <select class="search-by location">
                                    <option value="">All</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->code }}" {{ (app('request')->input('nationality') == $city->code )? 'selected="selected"' : ''}}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="header-secondary-menu">
            <a class="search-by" data-value="all">All</a>
            <a class="search-by" data-value="number">0-9</a>
            <a class="search-by" data-value="a">A</a> <a class="search-by" data-value="b">B</a>
            <a class="search-by" data-value="c">C</a> <a class="search-by" data-value="d">D</a>
            <a class="search-by" data-value="e">E</a> <a class="search-by" data-value="f">F</a>
            <a class="search-by" data-value="g">G</a> <a class="search-by" data-value="h">H</a>
            <a class="search-by" data-value="i">I</a> <a class="search-by" data-value="j">J</a>
            <a class="search-by" data-value="k">K</a> <a class="search-by" data-value="l">L</a>
            <a class="search-by" data-value="m">M</a> <a class="search-by" data-value="n">N</a>
            <a class="search-by" data-value="o">O</a> <a class="search-by" data-value="p">P</a>
            <a class="search-by" data-value="q">Q</a> <a class="search-by" data-value="r">R</a>
            <a class="search-by" data-value="s">S</a> <a class="search-by" data-value="t">T</a>
            <a class="search-by" data-value="u">U</a> <a class="search-by" data-value="v">V</a>
            <a class="search-by" data-value="w">W</a> <a class="search-by" data-value="x">X</a>
            <a class="search-by" data-value="y">Y</a> <a class="search-by" data-value="z">Z</a>
        </div>
        <section id="archive" class="galleries-page"
                 data-filter-type="{{ (isset($filter_type) ? $filter_type : '') }}"
                 data-filter-value="{{ (isset($filter_value) ? $filter_value : '') }}"
                 data-filter-alpha="{{ app('request')->input('startWith') }}"
                 data-filter-location="{{ app('request')->input('nationality') }}">
            <div class="container boxed-container">
                <section id="product-masonry" class="default-slider-section">
                    <div class="container col-container-with-offset container-load">
                        @foreach($artists as $artist_key=>$artist)
                            {{--START - Item--}}
                            <div class="slider-item one-fourth-col-with-margin half-width-mobile">
                                <i title="Remove" class="fa fa-times delete-item"></i>
                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                        @if(Auth::user()->collectionArtists->contains($artist->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif
                                <div class="slider-item-img square-box">
                                    <a title="{{ $artist->full_name }}" href="{{  route('artists.single', [$artist->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $artist->url_square_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <h2>
                                                <a title="{{ $artist->full_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->full_name }}</a>
                                            </h2>
                                        </div>
                                    </div>
                                    @if($artist->countryBirth()->count())
                                        <p class="dark-text provenance">{{ $artist->countryBirth()->first()->name }}</p>
                                    @endif
                                    <p class="exposure">{{ $artist->artworks()->count() }} Works exhibited</p>
                                    <a title="{{ $artist->full_name }}" class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
                                </div>
                            </div>
                            {{--END - Item--}}
                        @endforeach
                    </div>
                </section>
            </div>
            @if(isset($paginate))
                {!! $paginate->links('layouts.includes.pagination') !!}
            @else
                {!! $artists->links('layouts.includes.pagination') !!}
            @endif
        </section>
    </div>
@endsection