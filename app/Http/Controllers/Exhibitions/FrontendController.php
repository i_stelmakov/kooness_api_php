<?php

namespace App\Http\Controllers\Exhibitions;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Exhibition;
use Illuminate\Http\Request;
use Validator;

class FrontendController extends FrontendExtendedController
{

    /**
     *
     * @return \Illuminate\Http\Response
     */
    // Archive All
    public function archive(Request $request)
    {
        $filter_by = $request->get('filtered_by');
        $query = Exhibition::whereStatus(1)->orderBy('start_date', "DESC");
        if($filter_by == 'past'){
            $query->whereRaw("end_date < CURDATE()");
        }else if($filter_by == 'current'){
            $query->whereRaw("start_date < CURDATE() AND end_date > CURDATE()");
        }else if($filter_by == 'future'){
            $query->whereRaw("start_date > CURDATE()");
        }
        $exhibitions = $query->paginate(18);
        $exhibitions->appends($request->all());

        return view('exhibitions.frontend.archive', compact('exhibitions', 'filter_by', 'hide_load_more'));
    }


    // Exhibitions Single
    public function single($slug_name)
    {
        $exhibition = Exhibition::whereSlug($slug_name)->first(); // select * from exhibitions (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)
        if ($exhibition) {
            $exhibitions = [
                'past' => $exhibition->gallery()->first()->exhibitions()->whereRaw("id != '{$exhibition->id}' AND end_date < CURDATE()")->orderBy('end_date', "DESC")->first(),
                'current' => $exhibition->gallery()->first()->exhibitions()->whereRaw("id != '{$exhibition->id}' AND start_date < CURDATE() AND end_date > CURDATE()")->orderBy('end_date', "ASC")->first(),
                'future' => $exhibition->gallery()->first()->exhibitions()->whereRaw("id != '{$exhibition->id}' AND start_date > CURDATE()")->orderBy('start_date', "ASC")->first()
            ];
            return view('exhibitions.frontend.single', compact('exhibition', 'slug_name', 'exhibitions'));
        } else {
            return redirect(route('exhibitions.archive.all'));
        }
    }

//    public function loadMore(Request $request)
//    {
//        if ($request->ajax()) {
//            $offset = $request->get('offset');
//            $filter_by = $request->get('filtered_by');
//            $query = Exhibition::offset($offset)->limit(19)->whereStatus(1)->orderBy('created_at', "DESC");
//            if($filter_by == 'past'){
//                $query->whereRaw("end_date < CURDATE()");
//            }else if($filter_by == 'current'){
//                $query->whereRaw("start_date < CURDATE() AND end_date > CURDATE()");
//            }else if($filter_by == 'future'){
//                $query->whereRaw("start_date > CURDATE()");
//            }
//            $exhibitions = $query->get();
//
//            $return = [];
//            $hide_load_more = false;
//            if(count($exhibitions) <= 18){
//                $hide_load_more = true;
//            } else{
//                $exhibitions->pop();
//            }
//            foreach ($exhibitions as $key => $exhibition) {
//                $return[] = [
//                    "id" => $exhibition->id,
//                    "name" => $exhibition->name,
//                    "url" => route('exhibitions.single', [$exhibition->slug]),
//                    "image" => $exhibition->url_wide_box,
//                    "city" => $exhibition->city,
//                    "country_code" => $exhibition->country()->first()->code,
//                    "start_date" => date('m/d/Y', strtotime($exhibition->start_date)),
//                    "end_date" =>date('m/d/Y', strtotime($exhibition->end_date))
//                ];
//            }
//
//            return response()->json([
//                "result" => "success",
//                "item_list" => $return,
//                "hide_load_more" => $hide_load_more
//            ]);
//        }
//    }
}
