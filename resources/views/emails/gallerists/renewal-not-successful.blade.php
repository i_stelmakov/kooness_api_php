{{-- Mail - Unsuccessful Memebership Renewal --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_MEMBERSHIP_UNSUCCESSFUL_RENEWAL}}
@endsection

@section('content')
	<div style="margin: 16px;">

		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			Dear {{ $user->first_name }},<br>
			There were problems during the renewal of the services listed below.
		</p>

		<table border="0" cellpadding="0" cellspacing="0" width="100%" style='color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			<tr>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Plan</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Activation</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Renewal</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Payment</th>
				<th class="td" scope="col" style="border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; padding: 12px;">Amount</th>
			</tr>
			<tr>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ $plan->name }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ date('d/m/Y', strtotime($user->activation_date)) }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ date('d/m/Y', strtotime($user->premium_exp_date)) }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">{{ $payment->card_type }}</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
					{{$payment->subtotal}}{{ $payment->symbol_currency }}
				</td>
			</tr>
			@if( $payment->tax > 0 )
				<tr>
					<td colspan="3" valign="middle" class="td"></td>
					<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px; font-weight: bold;">
						VAT
					</td>
					<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
						{{ round($payment->tax) }}%
					</td>
				</tr>
			@endif
			<tr>
				<td colspan="3" valign="middle" class="td"></td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px; font-weight: bold;">Total</td>
				<td  valign="middle" class="td" style="text-align: left; vertical-align: middle; border-top: 0px; border-right: 0px; border-bottom: 1px solid #eee; border-left: 0px; word-wrap: break-word; color: #636363; padding: 12px;">
					{{ $payment->total }}{{ $payment->symbol_currency }}
				</td>
			</tr>
		</table>


		<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
			The error returned from Stripe is: "{{$error_message}}".<br>
			We kindly ask you to verify and proceed with the payment again to keep your membership active.
		</p>

	</div>

@stop
