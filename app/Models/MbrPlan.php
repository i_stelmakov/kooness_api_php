<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MbrPlan extends Model
{
    protected $guarded = [];

    private $_currency = '';

    private $_user = null;

    public function getFixedFeeAttribute(){
        $user = null;
        if(Auth::user() && roleCheck('gallerist')){
            $user = Auth::user();
        } elseif($this->_user){
            $user = $this->_user;
        }
        if($user && $user->hasRole('gallerist')){
            $gallery = $user->gallery()->first();
            if($gallery){
                if($gallery->country()->first()->code == 'GB'){
                    $this->_currency = 'GBP';
                    return $this->fixed_fee_gbp;
                }
                else{
                    $type = country_to_continent($gallery->country()->first()->code);
                    if($type == 'Europe'){
                        $this->_currency = 'EUR';
                        return $this->fixed_fee_eur;
                    } else{
                        $this->_currency = 'USD';
                        return $this->fixed_fee_usd;
                    }
                }
            } else{
                $this->_currency = 'EUR';
                return $this->fixed_fee_eur;
            }
        } else{
            $this->_currency = 'EUR';
            return $this->fixed_fee_eur;
        }
    }

    /**
     * @return string
     */
    public function getSymbolCurrency(): string
    {
        if($this->_currency == 'EUR') {
            return '€';
        }
        elseif($this->_currency == 'USD') {
            return '$';
        }
        elseif($this->_currency == 'GBP') {
            return '£';
        }
    }

    public function getCurrency(): string{
        return $this->_currency;
    }

    /**
     * @param null $user
     */
    public function setUser($user): void
    {
        $this->_user = $user;
    }
}
