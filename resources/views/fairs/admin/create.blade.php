@extends('layouts.admin')
@section('title', 'Admin | Create Fair')
@section('page-header', 'Admin - Create Fair')

@section('content')
    @include('fairs.admin.form', ['action' => route('admin.fairs.store'), 'method' => 'POST'])
@endsection