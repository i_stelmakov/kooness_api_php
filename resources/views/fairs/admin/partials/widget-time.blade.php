<li class="item-time" data-repeater-item>
    <div class="row">
        <i class="fa fa-arrows"></i>
        <i class="fa fa-times pull-right text-danger" data-repeater-delete style="cursor: pointer"></i>
    </div>
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <input type="text" placeholder="DD/MM/YYYY" name="day" class="form-control datepicker date-mask" value="{{ ($time && $time->day) ? $time->day : '' }}">
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="input-group">
            <span class="input-group-addon">
               <b>From</b>
            </span>
                <input type="text" class="form-control timepicker" name="from" value="{{ ($time && $time->from) ?  $time->from : '00:00'}}">
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="input-group">
            <span class="input-group-addon">
                <b>To</b>
            </span>
                <input type="text" class="form-control timepicker" name="to" value="{{ ($time && $time->to) ? $time->to : '00:00'}}">
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="input-group">
            <span class="input-group-addon">
                <b>Note</b>
            </span>
                <input type="text" placeholder="Note" class="form-control" name="note" value="{{ ($time && $time->note) ? $time->note : ''}}">
            </div>
        </div>
    </div>
</li>