@extends('layouts.admin')
@section('title', 'Admin | Create Promo Code')
@section('page-header', 'Admin - Create Promo Code')

@section('content')
    @include('promo-codes.admin.form', ['action' => route('admin.promo-codes.store'), 'method' => 'POST'])
@endsection