@extends('layouts.admin')
@section('title', 'Admin | Memberships Billing information')
@section('page-header', 'Memberships Billing information')

@section('content')
    @if( roleCheck('gallerist') )

        <form id="address-edit" class="action-submit row no-margin" role="form" method="POST" action="{{ route('admin.memberships.card') }}" data-attachment="validate">
            @csrf

            {{-- Section intro --}}
            <div class="">
                <p>The fields marked with asterisk (*) are required </p>
            </div>

            {{-- Details --}}
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="row">

                        {{-- Nome/Cognome o Ragione sociale --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="full_name">Name/Surname or Business name (*)</label>
                                <input 
                                    id="full_name" 
                                    class="form-control" 
                                    type="text" 
                                    data-slug="#slug" 
                                    placeholder="Name/Surname o Business name"
                                    name="full_name" 
                                    {{--value="{{ old('full_name', $address->{'full_name'}) }}"  --}}
                                    required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Codice Fiscale --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fiscal_code">Fiscal Code (*)</label>
                                <input 
                                    id="fiscal_code" 
                                    class="form-control" 
                                    type="text" 
                                    data-slug="#slug" 
                                    placeholder="Fiscal Code"
                                    name="fiscal_code" 
                                    {{-- value="{{ old('fiscal_code', $address->{'fiscal_code'}) }}"  --}}
                                    required
                                >
                            </div>
                        </div>

                        {{-- Partita IVA --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="vat_number">VAT number (*)</label>
                                <input
                                    id="vat_number"
                                    class="form-control"
                                    type="text"
                                    data-slug="#slug"
                                    placeholder="VAT numbers"
                                    name="vat_number"
                                    required
                                    {{-- value="{{ old('vat_number', $address->{'vat_number'}) }}"  --}}
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Country --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="country">Country</label>
                                <select class="select2 form-control" name="country">
                                    @foreach($countries as $country)
                                        <option
                                                value="{{ $country->id }}"
                                                {{-- {{ $country->id  == $gallery->country_id ? 'selected' : '' }} --}}
                                        >{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{-- Address --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Address (*)</label>
                                <input
                                        id="address"
                                        class="form-control"
                                        type="text"
                                        placeholder="Address"
                                        name="address"
                                        {{-- value="{{ old('address', $gallery->{'address'}) }}"  --}}
                                        required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- City --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="city">City (*)</label>
                                <input
                                        id="city"
                                        class="form-control"
                                        type="text"
                                        placeholder="City"
                                        name="city"
                                        {{-- value="{{ old('city', $gallery->{'city'}) }}"  --}}
                                        required
                                >
                            </div>
                        </div>

                        {{-- State --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="state">State (*)</label>
                                <input
                                        id="state"
                                        class="form-control"
                                        type="text"
                                        placeholder="State"
                                        name="state"
                                        {{-- value="{{ old('city', $gallery->{'city'}) }}"  --}}
                                        required
                                >
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        {{-- Zip --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="zip">Zip code (*)</label>
                                <input
                                        id="zip"
                                        class="form-control"
                                        type="text"
                                        placeholder="Zip Code"
                                        name="zip"
                                        {{-- value="{{ old('zip', $gallery->{'zip'}) }}"  --}}
                                        required
                                >
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" form="address-edit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    @endif
@endsection