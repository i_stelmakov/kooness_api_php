<section id="gallery-slider" class="gallery-slider widget-box default-slider-section slider-border">
    <div class="container boxed-container">
        <div class="default-slider-header">
            <span class="h1">{{ $data['title'] }}</span>
            <p>
                <a title="{{ $data['link_title'] }}" class="read-more" href="{{ $data['link'] }}">{{ $data['link_title'] }}</a>
            </p>
        </div>
            {{-- Different container class if item in list single or multiple (then add slick) --}}
            @if( count($data['items']) > 1 )
                <div class="container col-container-with-offset widget-slick-deactivated">
                @else
                <div class="container col-container-with-offset">
            @endif
            {{-- Items loop --}}
            @foreach($data['items'] as $item)
                <div class="slider-item one-third-col-with-margin">
                    @if(Auth::user() && $data['section'] != 'fairs' )
                        <div class="add-item follow {{ (Auth::user()->collectionGalleries->contains($item->id)) ? 'active' : '' }}" data-section="galleries" data-id="{{$item->id}}">
                            @if(Auth::user()->collectionGalleries->contains($item->id))
                                <i class="fa fa-heart"></i>
                            @else
                                <i class="fa fa-heart-o"></i>
                            @endif
                        </div>
                    @endif

                    <?php
                    $link = null;
                    if ($data['section'] == 'artworks') {
                        $link = route('artworks.single', [$item->slug]);
                        $img = $item->url_square_box;
                        $title = $item->title;
                    } else if ($data['section'] == 'artists') {
                        $link = route('artists.single', [$item->slug]);
                        $title = $item->first_name . " " . $item->last_name;
                        $img = $item->url_square_box;
                    } else if ($data['section'] == 'galleries') {
                        $link = route('galleries.single', [$item->slug]);
                        $title = $item->name;
                        $img = $item->url_wide_box;
                    } else if ($data['section'] == 'fairs') {
                        $link = route('fairs.single', [$item->slug]);
                        $title = $item->name;
                        $img = $item->url_wide_box;
                    }
                    ?>
                    <div class="slider-item-img wide-box">
                        <a title="{{ $title }}" href="{{ $link }}">
                            <div class="background-cover gallery lazy" data-src="{{$img}}"></div>
                        </a>
                    </div>
                    @if($data['section'] == 'artworks')
                        @include('partials.includes.box-artwork', ["item" => $item])
                    @elseif($data['section'] == 'artists')
                        @include('partials.includes.box-artist', ["item" => $item])
                    @elseif($data['section'] == 'galleries')
                        @include('partials.includes.box-gallery', ["item" => $item])
                    @elseif($data['section'] == 'fairs')
                        @include('partials.includes.box-fair', ["item" => $item])
                    @endif
                </div>
            @endforeach
            
        </div>
    </div>
</section>