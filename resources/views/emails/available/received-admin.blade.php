{{-- Mail - Admin Available Received --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_AVAILABLE_RECEIVED_ADMIN}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: center;'>
		<b>You have received a new request for an artwork available on fair:</b><br>
		"{{ $artwork->title }}"<br>
		<b>by the user:</b><br>
		{{ $user->first_name }} {{ $user->last_name }}<br>
		<b>with the message:</b><br>
		"{{ $msg}}"
	</p>
@stop