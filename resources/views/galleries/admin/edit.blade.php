@extends('layouts.admin')
@section('title', "Admin | Edit Gallery `{$gallery->name}`")
@section('page-header', "Admin - Edit Gallery `{$gallery->name}`")

@section('content')
    @include('galleries.admin.form', ['action' => route('admin.galleries.update', [$gallery->id]), 'method' => 'PATCH'])
@endsection