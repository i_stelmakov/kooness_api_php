<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 12/06/18
 * Time: 14:29
 */

namespace App\Http\Controllers\Home;


use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\Gallery;
use App\Models\HeaderLink;
use App\Models\HomeSection;
use App\Models\LayoutPage;
use App\Models\Post;
use App\Models\WidgetTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function editLayout()
    {
        $links = HeaderLink::orderBy('order', 'ASC')->get();
        if (!count($links)) {
            $links = [];
            $links[] = new HeaderLink();
        }
        $layoutPage = LayoutPage::whereLabel('home')->first();
        $result = WidgetTemplate::whereLayoutPageId($layoutPage->id)->orderBy('order', 'ASC')->get();
        $widgets = [];
        foreach ($result as $row) {
            $row->items = json_decode($row->items);
            $items = (array)$row->items;
            foreach ($items as $key => $item) {
                $element = null;
                if ($row->section == 'artists') {
                    $element = Artist::selectRaw('id as id, CONCAT_WS(" ", last_name, first_name) as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'artworks') {
                    $element = Artwork::selectRaw('id as id, title as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'galleries') {
                    $element = Gallery::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'fairs') {
                    $element = Fair::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'exhibitions') {
                    $element = Exhibition::selectRaw('id as id, name as text')
                        ->whereId($item)
                        ->first();
                } else if ($row->section == 'magazine') {
                    $element = Post::selectRaw('id as id, title as text')
                        ->whereId($item)
                        ->first();
                }
                if ($element) {
                    $items[$key] = (object)[
                        "id" => $element->id,
                        "text" => $element->text
                    ];
                }
                $row->items = $items;
            }
            $widgets[] = $row;
        }
        if (!count($widgets)) {
            $widgets[] = new WidgetTemplate();
        }
        $sections = [];
        $sectionsArray = HomeSection::all();
        foreach ($sectionsArray as $section) {
            $sections[$section->pointer] = $section->content;
        }
        $section = null;

        return view('home.admin.edit-layout', compact('widgets', 'sections', 'section', 'links'));
    }

    public function saveLinks(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $toRemove = HeaderLink::pluck('id')->toArray();
            if($request->get('widget-link')){
                foreach ($request->get('widget-link') as $link){
                    $headerLink = new HeaderLink();
                    if($link['id']){
                        if (in_array($link['id'], $toRemove)) {
                            $index = array_search($link['id'], $toRemove);
                            if ($index > -1) {
                                unset($toRemove[$index]);
                            }
                        }
                        $headerLink = HeaderLink::find($link['id']);
                    }
                    $headerLink->title = $link['title'];
                    $headerLink->link = $link['link'];
                    $headerLink->order = $link['order'];
                    if ($headerLink->save()) {
                        $response = [
                            'status' => 'success',
                            'redirectTo' => route('admin.homepage.layout.edit')
                        ];
                    }
                }
            }

            if (count($toRemove)) {
                HeaderLink::destroy($toRemove);
                $response = [
                    'status' => 'success',
                    'redirectTo' => route('admin.homepage.layout.edit')
                ];
            }
        }
        return response()->json($response);
    }

    public function saveLayout(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $layoutPage = LayoutPage::whereLabel('home')->first();
        $toRemove = WidgetTemplate::whereLayoutPageId($layoutPage->id)->pluck('id')->toArray();
        if ($request->method() == 'POST') {
            $data = Input::all();
            $label = $data['label'];
            $layoutPage = LayoutPage::whereLabel($label)->first();
            if ($layoutPage) {
                if (isset($data['widget']))
                    foreach ($data['widget'] as $widget) {
                        if (isset($widget['items']) && $widget['template'] != 'tplC') {
                            $widget['items'] = json_encode($widget['items']);
                            if ($widget['id']) {
                                if (in_array($widget['id'], $toRemove)) {
                                    $index = array_search($widget['id'], $toRemove);
                                    if ($index > -1) {
                                        unset($toRemove[$index]);
                                    }
                                }
                                $widgetTemplate = WidgetTemplate::find($widget['id']);
                                if ($widgetTemplate->update($widget)) {
                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.homepage.layout.edit')
                                    ];
                                }
                            } else {
                                $widgetTemplate = WidgetTemplate::create($widget);
                                if ($widgetTemplate) {
                                    $layoutPage->widgets()->save($widgetTemplate);

                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.homepage.layout.edit')
                                    ];
                                }
                            }
                        } else {
                            if ($widget['id']) {
                                if (in_array($widget['id'], $toRemove)) {
                                    $index = array_search($widget['id'], $toRemove);
                                    if ($index > -1) {
                                        unset($toRemove[$index]);
                                    }
                                }
                                $widgetTemplate = WidgetTemplate::find($widget['id']);
                            } else {
                                $widgetTemplate = new WidgetTemplate();
                            }
                            if ($widget['template'] == 'tplC') {
                                $widgetTemplate->title = $widget['title'];
                                $widgetTemplate->link = $widget['link'];
                                $widgetTemplate->subtitle = null;
                                $widgetTemplate->description = null;
                                $widgetTemplate->template = 'tplC';
                                $widgetTemplate->section = 'exhibitions';
                                $widgetTemplate->order = $widget['order'];
                                $items = [];
                                if (isset($widget['item_past'])) {
                                    $items['past'] = $widget['item_past'];
                                }
                                if (isset($widget['item_current'])) {
                                    $items['current'] = $widget['item_current'];
                                }
                                if (isset($widget['item_upcoming'])) {
                                    $items['upcoming'] = $widget['item_upcoming'];
                                }
                                $widgetTemplate->items = json_encode($items);
                                if ($widgetTemplate->save()) {
                                    $response = [
                                        'status' => 'success',
                                        'redirectTo' => route('admin.homepage.layout.edit')
                                    ];
                                }
                            }
                        }
                    }

                if (count($toRemove)) {
                    WidgetTemplate::destroy($toRemove);
                    $response = [
                        'status' => 'success',
                        'redirectTo' => route('admin.homepage.layout.edit')
                    ];
                }
            }
            $request->session()->flash("success", "Home page is successful updated!");
        }

        return response()->json($response);
    }

    public function saveSections(Request $request)
    {

        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            foreach ($data['sections'] as $key => $section) {
                $row = HomeSection::wherePointer($key)->first();
                if ($row) {
                    $row->content = $section;
                    $row->save();
                }
            }
            $response = [
                'status' => 'success',
                'redirectTo' => route('admin.homepage.layout.edit')
            ];
            $request->session()->flash("success", "Home page is successful updated!");

        }
        return response()->json($response);
    }

    public function searchItems(Request $request)
    {
        $response = [];
        if ($request->method() == 'POST') {
            $type = $request->get('type');
            $result = [];
            if ($type == 'artists') {
                $result = Artist::selectRaw('id as id, CONCAT_WS(" ", last_name, first_name) as text')
                    ->whereRaw("first_name LIKE '{$request->get('search')}%'")
                    ->orWhereRaw("last_name LIKE '{$request->get('search')}%'")
                    ->get();
            } else if ($type == 'artworks') {
                if ($request->get('modal') && !$request->get('bg')) {
                    $result = ArtworksCategory::selectRaw('id as id, name as text')
                        ->whereRaw("name LIKE '{$request->get('search')}%'")
                        ->where("is_medium", "=", "1")
                        ->get();
                } else {
                    $result = Artwork::selectRaw('id as id, title as text')
                        ->whereRaw("title LIKE '{$request->get('search')}%'")
                        ->get();
                }
            } else if ($type == 'galleries') {
                $result = Gallery::selectRaw('id as id, name as text')
                    ->whereRaw("name LIKE '{$request->get('search')}%'")
                    ->get();
            } else if ($type == 'categories') {
                if ($request->get('bg')) {
                    $result = Artwork::selectRaw('id as id, title as text')
                        ->whereRaw("title LIKE '{$request->get('search')}%'")
                        ->get();
                } else {
                    $result = ArtworksCategory::selectRaw('id as id, name as text')
                        ->whereRaw("name LIKE '{$request->get('search')}%'")
                        ->where("is_medium", "=", "0")
                        ->get();
                }
            } else if ($type == 'fairs') {
                $result = Fair::selectRaw('id as id, name as text')
                    ->whereRaw("name LIKE '{$request->get('search')}%'")
                    ->get();
            } else if ($type == 'exhibitions') {
                $result = Exhibition::selectRaw('id as id, name as text')
                    ->whereRaw("name LIKE '{$request->get('search')}%'")
                    ->get();
            } else if ($type == 'magazine') {
                $result = Post::selectRaw('id as id, title as text')
                    ->whereRaw("title LIKE '{$request->get('search')}%'")
                    ->whereType("magazine")
                    ->get();
            }
            foreach ($result as $row) {
                $tmp = [
                    'id' => $row->id,
                    'text' => $row->text
                ];

                if ($type == 'artworks' && $request->get('modal') && $request->get('bg')) {
                    $tmp['image'] = $row->main_image->url;
                }

                $response[] = $tmp;
            }
        }

        return response()->json($response);
    }

    public function retrieveDataSection(Request $request)
    {
        $response = [];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $response['sections'] = $data['sections'];
            $response['caption'] = (isset($data['caption'])) ? $data['caption'] : '';
            switch ($data['sections']) {
                case 'artworks':
                case 'categories':
                    if ($data['sections'] == 'artworks')
                        $response['title'] = "Artworks";
                    else
                        $response['title'] = "Categories";

                    if (isset($data['bg-image'])) {
                        $artwork = Artwork::find($data['bg-image']);
                        $response['id_bg'] = $artwork->id;
                        $response['text_bg'] = $artwork->title;
                        $image = $artwork->images()->whereLabel(1)->first();
                        if ($image) {
                            $response['bg'] = $image->url;
                        }
                    }
                    if($response['caption'] == ''){
                        $response['caption'] = $artwork->title;
                    }
                    $response['link'] = [];
                    $key = 0;
                    foreach ($data['link'] as $link) {
                        if ($link) {
                            $artworkCategory = ArtworksCategory::find($link);
                            $response['link'][$key]['id'] = $artworkCategory->id;
                            $response['link'][$key]['name'] = $artworkCategory->name;
                            $response['link'][$key]['link'] = route('artworks.single', [$artworkCategory->slug]);
                            $key++;
                        }
                    }
                    break;
                case 'artists':
                    $response['title'] = "Artists";
                    if (isset($data['bg-image'])) {
                        $artist = Artist::find($data['bg-image']);
                        $response['id_bg'] = $artist->id;
                        $response['text_bg'] = $artist->first_name . " " . $artist->last_name;
                        if ($artist->url_full_image) {
                            $response['bg'] = $artist->url_full_image;
                        }
                    }
                    if($response['caption'] == ''){
                        $response['caption'] = $artist->full_name;
                    }
                    $response['link'] = [];
                    $key = 0;
                    foreach ($data['link'] as $link) {
                        if ($link) {
                            $singleArtist = Artist::find($link);
                            $response['link'][$key]['id'] = $singleArtist->id;
                            $response['link'][$key]['name'] = $singleArtist->first_name. " " . $singleArtist->last_name ;
                            $response['link'][$key]['link'] = route('artworks.single', [$singleArtist->slug]);
                            $key++;
                        }
                    }
                    break;
                case 'fairs':
                    $response['title'] = "Fairs";
                    if (isset($data['bg-image'])) {
                        $fair = Fair::find($data['bg-image']);
                        $response['id_bg'] = $fair->id;
                        $response['text_bg'] = $fair->name;
                        if ($fair->url_full_image) {
                            $response['bg'] = $fair->url_full_image;
                        }
                    }
                    if($response['caption'] == ''){
                        $response['caption'] = $fair->name;
                    }
                    $response['link'] = [];
                    $key = 0;
                    foreach ($data['link'] as $link) {
                        if ($link) {
                            $singleFair = Fair::find($link);
                            $response['link'][$key]['id'] = $singleFair->id;
                            $response['link'][$key]['name'] = $singleFair->name;
                            $response['link'][$key]['link'] = route('artworks.single', [$singleFair->slug]);
                            $key++;
                        }
                    }
                    break;
                case 'galleries':
                    $response['title'] = "Galleries";
                    if (isset($data['bg-image'])) {
                        $gallery = Gallery::find($data['bg-image']);
                        $response['id_bg'] = $gallery->id;
                        $response['text_bg'] = $gallery->name;
                        if ($gallery->url_full_image) {
                            $response['bg'] = $gallery->url_full_image;
                        }
                    }
                    if($response['caption'] == ''){
                        $response['caption'] = $gallery->name;
                    }
                    $response['link'] = [];
                    $key = 0;
                    foreach ($data['link'] as $link) {
                        if ($link) {
                            $singleGallery = Gallery::find($link);
                            $response['link'][$key]['id'] = $singleGallery->id;
                            $response['link'][$key]['name'] = $singleGallery->name;
                            $response['link'][$key]['link'] = route('artworks.single', [$singleGallery->slug]);
                            $key++;
                        }
                    }
                    break;
            }

            return response()->json([
                "result" => "success",
                "data" => $response
            ]);

        }
    }
}