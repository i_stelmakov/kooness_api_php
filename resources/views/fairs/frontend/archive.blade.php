@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | Fairs')

@section('body-class', 'fairs-archive')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
<div class="sections">
    <!-- Fair Sheet Header !-->
    <section id="page-header">
        <div class="container boxed-container">

            <div class="col-container-with-offset">
                <div class="default-sheet-row">
                    <div class="default-sheet-row-cell">
                        <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'Fairs' }}</h1>
                    </div>
                </div>
            </div>

            <br>

            <div class="col-container-with-offset">
                <div class="category-filter-container">
                    @foreach($fairs as $fair_key=>$fair)
                        <div class="slider-item one-third-col-with-margin">
                            <div class="slider-item-img wide-box">
                                <a title="{{ $fair->name }}" href="{{ route('fairs.single', [$fair->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $fair->url_wide_box }}"></div>
                                </a>
                            </div>
                            <div class="slider-item-txt">
                                <h2><a title="{{ $fair->name }}" class="dark-text" href="{{ route('fairs.single', [$fair->slug]) }}">{{ $fair->name }}</a></h2>
                                <div class="event-date">
                                    <p class="event-date-p">{{ $fair->city }}, {{ $fair->country()->first()->code }}</p>
                                </div>
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($fair->start_date)) }} to {{ date('d/m/Y', strtotime($fair->end_date)) }}</p>
                                    </div>
                                </div>
                                <a title="Read more" class="read-more" href="{{ route('fairs.single', [$fair->slug]) }}">Read more</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        {!! $fairs->links('layouts.includes.pagination') !!}
    </section>
</div>
@endsection