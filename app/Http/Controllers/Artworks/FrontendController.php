<?php

namespace App\Http\Controllers\Artworks;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\ArtworksMediumCategory;
use App\Models\Fair;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Validator;

class FrontendController extends FrontendExtendedController
{
    /**
     *
     * @return \Illuminate\Http\Response
     */

    // Archive All (dipende da -> queryFilter)
    public function archive(Request $request, $slug_name = null, $slug_extra = null)
    {
        $has_filter = false;
        $this_category = null;
        $this_medium = null;
        $meta_title = ($this->_seo_data && $this->_seo_data->meta_title) ? $this->_seo_data->meta_title : 'Archive';
        $meta_keywords = ($this->_seo_data && $this->_seo_data->meta_keywords) ? $this->_seo_data->meta_keywords : null;
        $meta_description = ($this->_seo_data && $this->_seo_data->meta_description) ? $this->_seo_data->meta_description : null;
        $canonical = ($this->_seo_data && $this->_seo_data->canonical) ? $this->_seo_data->canonical : null;
        $h1 = ($this->_seo_data && $this->_seo_data->h1) ? $this->_seo_data->h1 : 'Archive';
        if ($slug_extra) {
            $slug_extra = preg_replace("/-{$slug_name}/i", '', $slug_extra);
            $this_medium = ArtworksCategory::whereSlug($slug_name)->first();
            $this_category = ArtworksCategory::whereSlug($slug_extra)->first();
            if ($this_category && $this_medium) {
                $relation = ArtworksMediumCategory::whereMediumId($this_medium->id)->whereCategoryId($this_category->id)->first();
                if ($relation) {
                    $meta_title = ($relation->meta_title) ? $relation->meta_title : "All {$this_medium->name} {$this_category->name} artworks";
                    $meta_keywords = ($relation->meta_keywords) ? $relation->meta_keywords : null;
                    $meta_description = ($relation->meta_description) ? $relation->meta_description : null;
                    $canonical = ($relation->canonical) ? $relation->canonical : null;
                    $h1 = ($relation->h1) ? $relation->h1 : "All {$this_medium->name} {$this_category->name} artworks";
                    $description = ($relation->description) ? $relation->description : ($this_medium->description ? $this_medium->description : ($this_category->description ? $this_category->description : null));
                } else {
                    return abort(404);
                }
            } else {
                return abort(404);
            }
        } else {
            if ($slug_name) {
                $this_category = ArtworksCategory::whereSlug($slug_name)->first();

                $meta_title = ($this_category->meta_title) ? $this_category->meta_title : "All {$this_category->name} artworks";
                $meta_keywords = ($this_category->meta_keywords) ? $this_category->meta_keywords : null;
                $meta_description = ($this_category->meta_description) ? $this_category->meta_description : null;
                $canonical = ($this_category->canonical) ? $this_category->canonical : null;
                $h1 = ($this_category->h1) ? $this_category->h1 : "All {$this_category->name} artworks";
                $description = ($this_category->description) ? $this_category->description : null;
                if ($this_category->is_medium) {
                    $this_medium = $this_category;
                    $this_category = null;
                }
            }
        }
        $filter_min_price = $request->input('min_price');
        $filter_max_price = $request->input('max_price');
        $filter_fair = $request->input('fair');
        $fair = null;
        if ($filter_fair) {
            $fair = Fair::whereSlug($filter_fair)->first();
            if ($fair) {
                $h1 = 'Artworks in fair ' . $fair->name;
            }
            $filter_min_price = null;
            $filter_max_price = null;
        }
        $filter_min_height = $request->input('min_height');
        $filter_max_height = $request->input('max_height');
        $filter_min_width = $request->input('min_width');
        $filter_max_width = $request->input('max_width');
        $filter_medium = $request->input('medium');
        $filter_artists = $request->input('artists');
        $filter_galleries = $request->input('galleries');

        $filter_order_by = $request->input('order_by');

        if($filter_min_price || $filter_max_price || $filter_fair || $filter_min_height || $filter_max_height || $filter_min_width || $filter_max_width || $filter_medium || $filter_artists || $filter_galleries)
            $has_filter = true;

        if ($filter_medium) {

            $has_filter = true;
            $filter_medium = explode(",", $filter_medium);
        }
        if ($filter_artists) {
            $filter_artists = explode(",", $filter_artists);
        }
        if ($filter_galleries) {
            $filter_galleries = explode(",", $filter_galleries);
        }

        // retrieve all filters in URL (query string)
        $filters = $request->all();
        // convert them in json format for js
        $filters_to_json = json_encode((array)$filters);

        $order_by = 'created_at';
        $order_dir = 'DESC';
        if($filter_order_by) {
            if($filter_order_by == 'recent') {
                $order_by = "created_at";
            }
            elseif($filter_order_by == 'higher-price') {
                $order_by = "price";
                $order_dir = 'DESC';
            }
            elseif($filter_order_by == 'lower-price') {
                $order_by = "price";
                $order_dir = 'ASC';
            }
        }

        $paginate = $this->queryFilter(
            $filter_min_price,
            $filter_max_price,
            $filter_min_height,
            $filter_max_height,
            $filter_min_width,
            $filter_max_width,
            $filter_medium,
            $filter_artists,
            $filter_galleries,
            $filter_fair,
            false, [],
            19,
            $this_category,
            $this_medium,
            $order_by,
            $order_dir
        );

        $paginate->appends($request->all());
        $artworks = Artwork::hydrate($paginate->items());
//        $artworks = $artworks->shuffle();

        $medium = ArtworksCategory::whereIsMedium(1)->get();
        if (!$this_medium && !$this_category){
            $categories = ArtworksCategory::whereIsMedium(0)->whereIsMain(1)->limit(10)->get();
        } elseif(!$this_medium  && $this_category){
            $categories = ArtworksCategory::whereIsMedium(0)->whereIsMain(1)->limit(10)->get();
        } else {
            if ($this_medium) {
                $categories = $this_medium->categories()->limit(10)->get();
            } else {
                $categories = [];
            }
        }
        $artists = Artist::where("status", "=", "1")->get();
        $galleries = Gallery::where("status", "=", "1")->get();
        $artworkCount = Artwork::where("status", "=", "1")->count();
        return view('artworks.frontend.archive', compact('artworks', 'medium', 'categories', 'artists', 'galleries', 'artworkCount', 'filters_to_json', 'fair', 'this_category', 'this_medium', 'meta_title', 'meta_description', 'meta_keywords', 'canonical', 'h1', 'hide_load_more', 'description', 'paginate', 'has_filter'));
    }

    // Artworks Single
    public function single(Request $request, $slug_name)
    {
        $category = ArtworksCategory::whereSlug($slug_name)->first();
        if ($category) {
            return $this->archive($request, $slug_name);
        }
        $artwork = Artwork::whereSlug($slug_name)->first(); // select * from artworks (tabella DB) where slug (nome campo) = '$slug_name' limit 1 (first = per non ottenere un array multidimensionale)
        if ($artwork) {
            $artist = $artwork->artist()->first();
            $mediums = $artwork->categories()->whereIsMedium(1)->get();
            $categories = $artwork->categories()->whereIsMedium(0)->get();
            $images = $artwork->images()->orderBy('label', 'ASC')->get();
            $gallery = $artwork->gallery()->first();
            $tags = $artwork->tags()->get();


            $recentView = Cookie::get("kooness_recent");
            if (!$recentView) {
                $recentView = Cookie::make("kooness_recent", Crypt::encrypt(json_encode([$artwork->id])), 3600);
            } else {
                $array = json_decode(Crypt::decrypt($recentView));
                if (!in_array($artwork->id, $array))
                    array_unshift($array, $artwork->id);
                $array = array_slice($array, 0, 5);
                $recentView = Cookie::make("kooness_recent", Crypt::encrypt(json_encode($array)), 3600);
            }

            return (new Response(view('artworks.frontend.single', compact('artwork', 'artist', 'mediums', 'categories', 'images', 'gallery', 'tags', 'fair'))))->withCookie($recentView);
        } else {
            return abort(404);
        }
    }

    // Artworks Ajax Call (per Load More & Filtri, dipende da -> queryFilter)
//    public function getFilteredItems(Request $request)
//    {
//        $actual_item_list = $request->input('actual_item_list');
////        $filter_color = $request->input('filter_color');
//        $filter_min_price = $request->input('filter_min_price');
//        $filter_max_price = $request->input('filter_max_price');
//        $filter_fair = $request->input('fair');
//        if ($filter_fair) {
//            $filter_min_price = null;
//            $filter_max_price = null;
//        }
//        $filter_min_height = $request->input('filter_min_height');
//        $filter_max_height = $request->input('filter_max_height');
//        $filter_min_width = $request->input('filter_min_width');
//        $filter_max_width = $request->input('filter_max_width');
//        $filter_medium = $request->input('filter_medium');
//        $filter_artists = $request->input('filter_artists');
//        $filter_galleries = $request->input('filter_galleries');
//        $load_more = $request->input('load_more');
//        $this_medium = $request->input('filter_this_medium');
//        $this_category = $request->input('filter_this_category');
//        $filter_order_by = $request->input('filter_order_by');
//
//        if ($this_medium) {
//            $this_medium = ArtworksCategory::find($this_medium);
//        } else {
//            $this_medium = null;
//        }
//
//        if ($this_category) {
//            $this_category = ArtworksCategory::find($this_category);
//        } else {
//            $this_category = null;
//        }
//
//        $order_by = 'created_at';
//        $order_dir = 'DESC';
//        if($filter_order_by) {
//            if($filter_order_by == 'recent') {
//                $order_by = "created_at";
//            }
//            elseif($filter_order_by == 'higher-price') {
//                $order_by = "price";
//                $order_dir = 'DESC';
//            }
//            elseif($filter_order_by == 'lower-price') {
//                $order_by = "price";
//                $order_dir = 'ASC';
//            }
//        }
//
//        $result = $this->queryFilter($filter_min_price, $filter_max_price, $filter_min_height, $filter_max_height, $filter_min_width, $filter_max_width, $filter_medium, $filter_artists, $filter_galleries, $filter_fair, $load_more, $actual_item_list, 19, $this_category, $this_medium, $order_by, $order_dir);
//
//        $artworks = [];
//        $toRemove = ($load_more == 'false') ? $actual_item_list : [];
////        dd($load_more);
//        $hydrate = Artwork::hydrate($result);
//        foreach ($hydrate as $row) {
//            // check if already present in previous, than apply a marker to remove
//            if (!empty($actual_item_list)) {
//                if (in_array($row->id, $actual_item_list)) {
//                    $index = array_search($row->id, $toRemove);
//                    if ($index > -1) {
//                        unset($toRemove[$index]);
//                    }
//                    continue;
//                }
//            }
//
//            $tmp = [
//                "id" => null,
//                "name" => null,
//                "status" => null,
//                "image" => null,
//                "link" => null,
//                "year" => null,
//                "price" => null,
//                "measure_cm" => null,
//                "measure_inc" => null,
//                "artist_id" => null,
//                "artist_name" => null,
//                "artist_link" => null,
//                "medium" => [],
//            ];
//
//            // recupero dati generici
//            $tmp['id'] = $row->id;
//            $tmp['name'] = $row->title;
//            $tmp['status'] = $row->status;
//            $tmp['image'] = $row->main_image->url;
//            $tmp['link'] = $row->slug;
//            $tmp['year'] = $row->year;
//            $tmp['price'] = $row->pretty_price;
//            $tmp['measure_cm'] = $row->measure_cm;
//            $tmp['measure_inc'] = $row->measure_inc;
//            $tmp['auth_user'] = (Auth::user()) ? true : false;
//            $tmp['favourite'] = (Auth::user() && Auth::user()->collectionArtworks->contains($row->id)) ? true : false;
//            $tmp['available_in_fair'] = (($row->available_in_fair) ? ($row->show_price ? 0 : 1) : 0);
//            $tmp['show_price'] = $row->show_price;
//
//
//            // recupero artist connesso
//            $artist = \DB::table('artists')->where('id', $row->artist_id)->first();
//            $tmp['artist_id'] = $row->artist_id;
//            $tmp['artist_name'] = $artist->first_name;
//            $tmp['artist_lastname'] = $artist->last_name;
//            $tmp['artist_link'] = route('artists.single', [$artist->slug]);
//
//            // recupero medium connessi
//            $medium = $row->categories()->where("is_medium", "=", "1")->get();
//            if ($medium) {
//                foreach ($medium as $media) {
//                    $tmp['medium'][] = [
//                        "id" => $media->id,
//                        "name" => $media->name,
//                        "link" => route('artworks.single', [$media->slug])
//                    ];
//                }
//            }
//
//            $artworks[] = $tmp;
//        }
//
//        $hide_load_more = false;
//        if(count($hydrate) <= 18){
//            $hide_load_more = true;
//        } else{
//            array_pop($artworks);
//        }
//
//        shuffle($artworks);
//        $results_array = [
//            "new_item_list" => $artworks,
//            "old_item_list" => $toRemove,
//            "hide_load_more" => $hide_load_more
//        ];
//
//        return response()->json(['response' => $results_array]);
//    }

    // Artworks Filter (generico)
    private function queryFilter(
        $min_price = null,
        $max_price = null,
        $min_height = null,
        $max_height = null,
        $min_width = null,
        $max_width = null,
        $medium = null,
        $artists = null,
        $galleries = null,
        $fair = null,
        $load_more = false,
        $actual_item_list = [],
        $limit = 18,
        $this_category = null,
        $this_medium = null,
        $order_by = 'created_at',
        $order_dir = 'DESC'
    )
    {
//        dd($filter_order_by);

        $query = DB::table('artworks')->selectRaw('artworks.*')->whereRaw("(artworks.status = 1 OR artworks.status = 2)")->distinct();

        if ($min_price)
            $query->whereRaw("price >= '$min_price'");

        if ($max_price)
            $query->whereRaw("price <= '$max_price'");

        if ($min_height)
            $query->whereRaw("height >= '$min_height'");

        if ($max_height)
            $query->whereRaw("height <= '$max_height'");

        if ($min_width)
            $query->whereRaw("width >= '$min_width'");

        if ($max_width)
            $query->whereRaw("width <= '$max_width'");

        if ($medium) {
            $query->leftJoin('artworks_artworks_categories as p1', "artworks.id", "=", "p1.artwork_id");
            $query->whereRaw("p1.category_id IN (" . implode(',', $medium) . ")");
        }

        if ($this_medium) {
            $query->leftJoin('artworks_artworks_categories as p2', "artworks.id", "=", "p2.artwork_id");
            $query->whereRaw("p2.category_id = '{$this_medium->id}'");
        }

        if ($this_category) {
            $query->leftJoin('artworks_artworks_categories as p3', "artworks.id", "=", "p3.artwork_id");
            $query->whereRaw("p3.category_id = '{$this_category->id}'");
        }

        if ($artists) {
            $query->whereRaw("artworks.artist_id IN (" . implode(',', $artists) . ")");
        }

        if ($galleries) {
            $query->whereRaw("artworks.gallery_id IN (" . implode(',', $galleries) . ")");
        }

        if ($load_more == 'true') {
            $query->whereRaw("artworks.id NOT IN (" . implode(',', $actual_item_list) . ")");
        }

        if ($fair) {
            $query->leftJoin('artworks_fairs', "artworks.id", "=", "artworks_fairs.artwork_id");
            $query->leftJoin('fairs', "artworks_fairs.fair_id", "=", "fairs.id");
            $query->whereRaw("fairs.slug LIKE '$fair'");
        } else {
            $query->whereRaw("NOT available_in_fair");
        }
        // se passato filtro per l'ordinamento

        $query->orderBy("artworks.$order_by", $order_dir);

        return $query->paginate(18);
    }
}