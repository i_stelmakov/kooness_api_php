@if(!Request::is('/') && !Request::is('home'))
<p>
    @foreach (Breadcrumbs::generate() as $breadcrumb)
        @if($loop->first)
            <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
        @elseif (!$loop->last)
            <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
        @else
            {{ $breadcrumb->title }}
        @endif
    @endforeach
</p>
@endif