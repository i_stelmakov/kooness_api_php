@extends('layouts.admin')
@section('title', "Admin | Edit Fair `{$fair->name}`")
@section('page-header', "Admin - Edit Fair `{$fair->name}`")

@section('content')
    @include('fairs.admin.form', ['action' => route('admin.fairs.update', [$fair->id]), 'method' => 'PATCH'])
@endsection