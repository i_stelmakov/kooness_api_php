@if(roleCheck('superadmin|admin|gallerist|artist|seo'))
    <form class="ajax-form row" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="{{ $gallery->id }}" data-referer="gallery">
        {{ method_field($method) }}

        {{-- Section intro --}}
        <div class="col-md-6 col-sm-12">
            <p>The fields marked with asterisk (*) are required </p>
        </div>

        {{-- Associations --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Associations
                </div>
                <div class="box-body">

                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="categories">Categories</label>
                            <select class="select2 form-control" name="categories[]" multiple="multiple" id="categories">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ (in_array($category->id, $gallery->category_ids))? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <select class="select2 form-control" name="tags[]" multiple="multiple" id="tags">
                                    @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}" {{ (in_array($tag->id, $gallery->tag_ids))? 'selected="selected"' : '' }}>{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label for="fairs">Fairs</label>
                                <select class="select2 form-control" name="fairs[]" multiple="multiple" id="fairs">
                                    @foreach($fairs as $fair)
                                        <option value="{{ $fair->id }}" {{ (in_array($fair->id, $gallery->fair_ids))? 'selected="selected"' : '' }}>{{ $fair->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>

        {{-- Gallery Information --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Gallery Information
                </div>
                <div class="box-body">
                    <div class="col-md-12  col-xs-12">
                        <div class="form-group">
                            <label for="generated_url">Url generated</label>
                            <input 
                                id="generated_url" 
                                class="form-control" 
                                type="text" 
                                data-prefix="{{ url('') }}/galleries/" 
                                value="{{ ($gallery->{'slug'})? url('') . '/galleries/' . $gallery->{'slug'} : '' }}" 
                                disabled
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Gallery name (*)</label>
                            <input 
                                id="name" 
                                class="form-control" 
                                type="text" 
                                data-slug="#slug" 
                                placeholder="Gallery Name" 
                                name="name" 
                                value="{{ old('name', $gallery->{'name'}) }}" 
                                {{ ( roleCheck('seo') ) ? 'disabled' : 'required' }}
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="slug">Slug (*)</label>
                            <input 
                                id="slug" 
                                class="form-control" 
                                type="text" 
                                placeholder="Slug" 
                                name="slug" 
                                value="{{ old('slug', $gallery->{'slug'}) }}" 
                                data-url="#generated_url" 
                                {{ ( roleCheck('seo|gallerist') ) ? 'readonly' : 'required' }}
                            >
                        </div>
                    </div>

                    @if(!roleCheck('seo'))
                        {{--<div class="row">--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="about_gallery">About the gallery</label>
                                    <textarea id="about_gallery" class="form-control" placeholder="About the gallery" name="about_gallery" rows="5">{{ $gallery->{'about_gallery'} }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meet_gallerist">Meet the gallerist</label>
                                    <textarea id="meet_gallerist" class="form-control" placeholder="Meet the gallerist" name="meet_gallerist" rows="5">{{ $gallery->{'meet_gallerist'} }}</textarea>
                                </div>
                            </div>
                        {{--</div>--}}
                        <div class="col-md-12">
                            <div class="box box-default box-solid" style="padding-bottom: 23px;">
                                <div class="box-header with-border">
                                    Address 1
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country_id">Country</label>
                                                <select class="select2 form-control" name="country_id">
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->id }}" {{ $country->id  == $gallery->country_id ? 'selected' : '' }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">City (*)</label>
                                                <input id="city" class="form-control" type="text" placeholder="City" name="city" value="{{ old('city', $gallery->{'city'}) }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address">Address (*)</label>
                                                <input id="address" class="form-control" type="text" placeholder="Address" name="address" value="{{ old('address', $gallery->{'address'}) }}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zip">Zip code (*)</label>
                                                <input id="zip" class="form-control" type="text" placeholder="Zip Code" name="zip" value="{{ old('zip', $gallery->{'zip'}) }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-default box-solid collapsed-box" style="padding-bottom: 23px;">
                                <div class="box-header with-border">
                                    Address 2
                                    <div class="box-tools pull-right">
                                        <!-- Collapse Button -->
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country_id_2">Country</label>
                                                <select class="select2 form-control" name="country_id_2">
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->id }}" {{ $country->id  == $gallery->country_id_2 ? 'selected' : '' }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city_2">City (*)</label>
                                                <input id="city_2" class="form-control" type="text" placeholder="City" name="city_2" value="{{ old('city_2', $gallery->{'city_2'}) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address_2">Address (*)</label>
                                                <input id="address_2" class="form-control" type="text" placeholder="Address" name="address_2" value="{{ old('address_2', $gallery->{'address_2'}) }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zip_2">Zip code (*)</label>
                                                <input id="zip_2" class="form-control" type="text" placeholder="Zip Code" name="zip_2" value="{{ old('zip_2', $gallery->{'zip_2'}) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-default box-solid collapsed-box" style="padding-bottom: 23px;">
                                <div class="box-header with-border">
                                    Address 3
                                    <div class="box-tools pull-right">
                                        <!-- Collapse Button -->
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country_id_3">Country</label>
                                                <select class="select2 form-control" name="country_id_3">
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->id }}" {{ $country->id  == $gallery->country_id_3 ? 'selected' : '' }}>{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city_3">City (*)</label>
                                                <input id="city_3" class="form-control" type="text" placeholder="City" name="city_3" value="{{ old('city_3', $gallery->{'city_3'}) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address_3">Address (*)</label>
                                                <input id="address_3" class="form-control" type="text" placeholder="Address" name="address_3" value="{{ old('address', $gallery->{'address_3'}) }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zip_3">Zip code (*)</label>
                                                <input id="zip_3" class="form-control" type="text" placeholder="Zip Code" name="zip_3" value="{{ old('zip_3', $gallery->{'zip_3'}) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       @if(!auth()->user()->hasRole('gallerist') || (auth()->user()->hasRole('gallerist') && $gallery->{'status'} != 1) )
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="0" {{ (!$gallery->{'status'})  ? 'selected="selected"' : ''  }}>
                                                Hidden
                                            </option>
                                            <option value="2" {{ ($gallery->{'status'} == 2)  ? 'selected="selected"' : ''  }}>
                                                Ready to be Published
                                            </option>
                                            @if(!auth()->user()->hasRole('gallerist'))
                                                <option value="1" {{ ($gallery->{'status'} == 1)  ? 'selected="selected"' : ''  }}>
                                                    Visible
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!roleCheck('gallerist'))
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="featured">Featured</label>
                                        <div class="form-group">
                                            <input class="iCheck-input form-check-input" type="radio" name="featured" id="featured_yes" value="1" {{ ($gallery->{'featured'}) ? 'checked="checked"' : '' }}> Yes &nbsp;&nbsp;
                                            <input class="iCheck-input form-check-input" type="radio" name="featured" id="featured_no" value="0" {{ (!$gallery->{'featured'}) ? 'checked="checked"' : '' }}> No
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    @endif
                </div>
            </div>
        </div>

        {{-- Gallerist Information --}}
        @if(!roleCheck('seo') )
                <div class="col-md-12 col-xs-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            Gallerist Information
                        </div>
                        <div class="box-body">

                            {{-- Se utente è già stato attivato almeno una volta non mostrare più opzione di attivazione--}}
                            @if(!$gallery->user_id)

                                {{--Create Credential--}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="created_user">Create user credential</label>
                                        <div class="form-group">
                                            <input
                                                    class="iCheck-input form-check-input"
                                                    type="radio"
                                                    name="created_user"
                                                    id="created_user_yes"
                                                    value="1"
                                            > Yes &nbsp;&nbsp;
                                            <input
                                                    class="iCheck-input form-check-input"
                                                    type="radio"
                                                    name="created_user"
                                                    id="created_user_no"
                                                    value="0"
                                                    checked
                                            > No
                                        </div>
                                    </div>
                                </div>

                                {{--First Name--}}
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="first_name">First name (*)</label>
                                        <input
                                                id="first_name"
                                                class="form-control"
                                                type="text"
                                                placeholder="First Name"
                                                name="first_name"
                                                data-for-user="true"
                                                value="{{ old('first_name', $gallery->{'first_name'}) }}"
                                                required
                                        >
                                    </div>
                                </div>

                                {{--Last Name--}}
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="last_name">Last name (*)</label>
                                        <input
                                                id="last_name"
                                                class="form-control"
                                                type="text"
                                                placeholder="Last Name"
                                                name="last_name"
                                                data-for-user="true"
                                                value="{{ old('last_name', $gallery->{'last_name'}) }}"
                                                required
                                        >
                                    </div>
                                </div>

                                {{--Email--}}
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">Email (*)</label>
                                        <input
                                                id="email"
                                                class="form-control"
                                                type="email"
                                                placeholder="Email"
                                                name="email"
                                                data-for-user="true"
                                                value="{{ old('email', $gallery->{'email'}) }}"
                                        >
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="email">Membership expired on (*)</label>
                                        <input
                                                id="membership_expiration"
                                                class="form-control datepicker date-mask"
                                                type="text"
                                                name="membership_expiration"
                                                placeholder="DD/MM/AAAA"
                                                data-for-user="true"
                                                value="{{  date('d/m/Y', strtotime(date("Y-m-d", time()) . " +1 month")) }}"
                                        >
                                    </div>
                                </div>

                            @endif

                            {{--User Recap--}}
                            @if( $gallery->user_id )
                                <div class="col-md-12 col-xs-12">
                                    <p>
                                        <b>Associated User data (uneditable)</b><br>
                                        User: {{ $gallery->user()->first()->{'username'} }}<br>
                                        User First Name: {{ $gallery->user()->first()->{'first_name'} }}<br>
                                        User Last Name: {{ $gallery->user()->first()->{'last_name'} }}<br>
                                        User Email: {{ $gallery->user()->first()->{'email'} }}<br>
                                        User Trial Expiration: {{ date('d/m/Y',strtotime(date("Y-m-d", strtotime($gallery->user()->first()->{'trial_activation_date'})) . " +1 month")) }}
                                    </p>
                                </div>
                            @endif

                            {{--Enable--}}
                            {{--Se utente è già stato attivato almeno una volta non mostrare più opzione di attivazione--}}
                            @if( ($gallery->user()->first() && $gallery->user()->first()->{'user_first_enable'} == 0 ) || !$gallery->user_id )
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="gallerist_status">Enable</label>
                                        <select class="select2 form-control" name="gallerist_status" data-for-user="true">
                                            <option value="0">
                                                No
                                            </option>
                                            <option value="1">
                                                Yes
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            {{--@endif--}}

        @endif {{--!roleCheck('seo')--}}

        {{-- Images --}}
        @if(!roleCheck('seo'))
            {{-- Main Images --}}
            @include('partials.image-default', ['item' => $gallery])
            @if( !roleCheck('gallerist|artist') )
                {{-- Thumbnail Images --}}
                @include('partials.images', ['item' => $gallery, 'square_box_active' => false, 'wide_box_active' => true,'vertical_box_active' => false ])
            @endif
        @endif

        @if(!roleCheck('gallerist'))
            {{-- SEO Management --}}
            <div class="col-md-12 col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        SEO Management
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_title">SEO - title</label>
                                    <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $gallery->{'meta_title'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="meta_keywords">SEO - keywords</label>
                                    <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $gallery->{'meta_keywords'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="h1">SEO - H1</label>
                                    <input class="form-control"
                                        name="h1" type="text"
                                        placeholder="SEO - H1"
                                        id="h1"
                                        value="{{ $gallery->{'h1'} }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="canonical">SEO - Canonical Url</label>
                                    <input class="form-control"
                                        name="canonical" type="text"
                                        placeholder="SEO - Canonical Url"
                                        id="canonical"
                                        value="{{ $gallery->{'canonical'} }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="meta_description">SEO - Description</label>
                                    <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $gallery->{'meta_description'} }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- Footer & Submit --}}
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endif