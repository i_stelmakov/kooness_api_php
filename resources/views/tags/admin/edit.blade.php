@extends('layouts.admin')
@section('title', "Admin | Edit Tag `{$tag->name}`")
@section('page-header', "Admin - Edit Tag `{$tag->name}`")

@section('content')
    @include('tags.admin.form', ['action' => route('admin.tags.update', [$tag->id]), 'method' => 'PATCH'])
@endsection