@extends('layouts.admin')
@section('title', 'Admin | Artists')
@section('page-header', 'Artists')

@section('content')
    <div class="callout callout-warning" style="background: #fff!important; color: #333!important;">
        <h4>Reminder!</h4>
        <p>
            if you see some content marked as "NOT EDITABLE" it is because your content is common with other galleries so, if you need some modification ask directly to kooness staff at <a href="mailto:info@kooness?subject=Artist mod request">info@kooness</a> .
        </p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                @if(!roleCheck('seo'))
                    <div class="box-header">
                        <div class="pull-right">
                            <a href="{{ route('admin.artists.create') }}">
                                <button class="btn btn-primary">Add new artist</button>
                            </a>
                        </div>
                    </div>
                @endif
    
                <div class="box-body">
                    <table id="table-tags" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Slug</th>
                            <th>Represented by</th>
                            <th>Visible</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table-tags').DataTable({
                "scrollX": true,
                "processing": true,
                "stateSave": true,
                "serverSide": true,
                "order": [[ 6, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.artists.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "first_name"},
                    {"data": "last_name"},
                    {"data": "slug"},
                    {"data": "galleries", "bSortable": false},
                    {"data": "status", "bSortable": false},
                    {"data": "created_at"},
                    {"data": "options","bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                    if(data.has_multiple_gallery){
                        $(row).addClass('disabled-because-is-multi-gallery');
                    }
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                        // esempio di attivazione di attributi CSS personalizzati per una singola cella 
                        // if ( cellData < 1 ) {
                        //     $(td).css('color', 'red')
                        // }
                    }
                ]

            });
        });
    </script>
@endsection