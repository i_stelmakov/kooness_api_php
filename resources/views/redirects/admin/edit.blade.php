@extends('layouts.admin')
@section('title', "Admin | Edit 301 Redirects")
@section('page-header', "Admin -  301 Redirects")

@section('content')
    @include('redirects.admin.form', ['action' => route('admin.redirects.update', [$redirect->id]), 'method' => 'PATCH'])
@endsection