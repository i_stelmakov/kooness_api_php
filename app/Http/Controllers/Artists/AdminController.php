<?php

namespace App\Http\Controllers\Artists;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Country;
use App\Models\Gallery;
use App\Models\Redirect301;
use App\Models\SiteMap;
use App\Models\Tag;
use App\Models\User;
use App\Services\Notification;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Session;
use Validator;
use View;

class AdminController extends Controller
{

    private $_directory_images = 'artists';

    private $_type_url = 'Artist';

    private $_url_prefix = '/artists/';

    // variabili private dell'istanza della classe
    private $gallery_id = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // recupero eventuale gallerist ID che sta accedendo al dato
        $gallery = auth()->user()->gallery()->first();
        if ($gallery) {
            $this->gallery_id = $gallery->id;
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('artists.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'last_name',
            3 => 'slug',
            4 => 'galleries',
            5 => 'status',
            6 => 'created_at',
            7 => 'options',
        );

        // recupero eventuale gallerist ID che sta accedendo al dato
        $gallery_id = 0;
        $gallery = auth()->user()->gallery()->first();
        if ($gallery) {
            $gallery_id = $gallery->id;
        }

        // creazione di una nuova query basata su model Artist
        $totalData_query = new Artist();

        // creazione di una nuova query partendo dall'istanza di Artist, per integrare le condizioni sotto
        $totalData_query = $totalData_query->newQuery();

        // se non è admin evita l'estrazione delle righe senza slug
        if(!roleCheck('admin')){
            $totalData_query->whereRaw('slug IS NOT NULL');
        }

        // se è gallerist recupera solo i suoi associati
        if(roleCheck('gallerist')){
            $totalData_query->whereRaw("(created_by = ". auth()->user()->id ." OR
                artists.id
                IN (
                    SELECT ag1.`artist_id`
                    FROM `artists_galleries` AS ag1
                    WHERE ag1.`gallery_id` = $gallery_id
                )
            )");

        }

        // conteggia gli elementi trovati
        $totalData = $totalData_query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        // se non c'è una ricerca
        if (empty($request->input('search.value'))) {
            $artists_query = Artist::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            // se non è admin evita l'estrazione delle righe senza slug
            if( !roleCheck('admin') ){
                $artists_query->whereRaw('slug IS NOT NULL');
            }
            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $artists_query->whereRaw("(created_by = ". auth()->user()->id ." OR
                    artists.id
                    IN (
                        SELECT ag1.`artist_id`
                        FROM `artists_galleries` AS ag1
                        WHERE ag1.`gallery_id` = $gallery_id
                    )
                )");

            }
            $artists = $artists_query->get();
        } // se c'è una ricerca
        else {
            $search = $request->input('search.value');

            $artists_query = Artist::whereRaw("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%')")
                ->select('artists.*')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            // se non è admin evita l'estrazione delle righe senza slug
            if( roleCheck('gallerist') ){
                $artists_query->whereRaw('slug IS NOT NULL');
            }
            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $artists_query->whereRaw("(created_by = ". auth()->user()->id ." OR
                    artists.id
                    IN (
                        SELECT ag1.`artist_id`
                        FROM `artists_galleries` AS ag1
                        WHERE ag1.`gallery_id` = $gallery_id
                    )
                )");

            }
            // dd($artists_query->toSql());
            $artists = $artists_query->get();
            $totalFiltered_query = Artist::whereRaw("(first_name LIKE '%{$search}%' OR last_name LIKE '%{$search}%')");
            // se non è admin evita l'estrazione delle righe senza slug
            if( roleCheck('gallerist') ){
                $totalFiltered_query->whereRaw('slug IS NOT NULL');
            }
            // se è gallerist recupera solo i suoi associati
            if( roleCheck('gallerist') ){
                $totalFiltered_query->whereRaw("(created_by = ". auth()->user()->id ." OR
                    artists.id
                    IN (
                        SELECT ag1.`artist_id`
                        FROM `artists_galleries` AS ag1
                        WHERE ag1.`gallery_id` = $gallery_id
                    )
                )");

            }
            // risultati della ricerca
            $totalFiltered = $totalFiltered_query->count();
        }

        $data = array();
        if (!empty($artists)) {
            foreach ($artists as $artist) {
                $has_multiple = 0;
                if(Auth::user()->hasRole('gallerist') && check_if_artist_has_multiple_gallery($artist->id)){
                    $edit = null;
                    $delete = null;
                    $has_multiple = 1;
                } else{
                    $edit = route('admin.artists.edit', $artist->id);
                    $delete = route('admin.artists.destroy', $artist->id);
                }

                $nestedData['id'] = $artist->id;
                $nestedData['first_name'] = $artist->first_name;
                $nestedData['last_name'] = $artist->last_name;
                $nestedData['slug'] = $artist->slug;
                $nestedData['galleries'] = $artist->gallery_names;
                $nestedData['status'] = ($artist->status == 2) ? 'No' : get_state_status($artist->status);
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($artist->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete, 'not_editable' => $has_multiple]))->render();
                $nestedData['has_multiple_gallery'] = $has_multiple;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $artist = new Artist();
        $countries = Country::all();
        $categories = ArtistsCategory::select("id", "name")->get();
        $gallery_id = $this->gallery_id;
        $artists = Artist::whereStatus(1)->get();

        // se gallerista mostra nel select di associazione galleria solo il suo
        if (roleCheck('gallerist')) {
            $galleries = Gallery::select("id", "name")->where('id', '=', $this->gallery_id)->get();
        } else {
            $galleries = Gallery::select("id", "name")->get();
        }
        $tags = Tag::select("id", "name")->get();
        return view('artists.admin.create', compact('artist', 'countries', 'categories', 'tags', 'galleries', 'gallery_id', 'artists'));
    }

    public function store(Request $request)
    {
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        if ($request->method() == 'POST') {
            $data = Input::all();
            $data['created_by'] = Auth::user()->id;
            if (isset($data['not_in_kooness'])) {
                $data['status'] = 1;
                $data['create_from_exhibition'] = 1;
            }
            $artist = Artist::create($data);
            if ($artist) {

                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $artist, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $artist, '\App\Models\ArtistsCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $artist, '\App\Models\Gallery', 'galleries', 'gallery_ids');

                $this->createUser($artist, $data);

                if (isset($data['not_in_kooness'])) {
                    $response = [
                        'status' => 'success',
                        'directory' => $this->_directory_images,
                        'id_row' => $artist->id,
                        'label' => $artist->last_name . " " . $artist->first_name,
                        'redirectTo' => null
                    ];
                } else {
                    SiteMap::create([
                        "url" => $this->_url_prefix . $artist->slug,
                        "type" => $this->_type_url,
                        "referer_id" => $artist->id
                    ]);

                    if(roleCheck('gallerist')){
                        $users = User::permission('admin-level')->get();
                        $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_ARTIST, 'artist', $artist);
                        Notification::addUsers($notify, $users);
                    }

                    $response = [
                        'status' => 'success',
                        'directory' => $this->_directory_images,
                        'id_row' => $artist->id,
                        'redirectTo' => route('admin.artists.index')
                    ];
                    $request->session()->flash("success", "Artist with name `{$artist->first_name} {$artist->last_name}` is successful created!");
                }
            }
        }
        return response()->json($response);
    }

    public function edit($artist)
    {
        if(roleCheck('artist')){
            $artist_user = Auth::user()->artist()->first();
            if ($artist != $artist_user->id) {
                return redirect()->route('admin.dashboard');
            }
        }
        if(Auth::user()->hasRole('gallerist') && check_if_artist_has_multiple_gallery($artist)){
            return redirect()->route('admin.dashboard');
        }
        $artist = Artist::find($artist);
        $countries = Country::all();
        $categories = ArtistsCategory::select("id", "name")->get();
        $gallery_id = $this->gallery_id;

        // se gallerista mostra nel select di associazione galleria solo il suo
        if (roleCheck('gallerist')) {
            $galleries = Gallery::select("id", "name")->where('id', '=', $this->gallery_id)->get();
        } else {
            $galleries = Gallery::select("id", "name")->get();
        }
        $tags = Tag::select("id", "name")->get();
        return view('artists.admin.edit', compact('artist', 'countries', 'categories', 'tags', 'galleries', 'gallery_id'));
    }

    public function update(Request $request, $artist)
    {
        if(roleCheck('artist')){
            $artist_user = Auth::user()->artist()->first();
            if ($artist != $artist_user->id) {
                return redirect()->route('admin.dashboard');
            }
        }
        if(Auth::user()->hasRole('gallerist') && check_if_artist_has_multiple_gallery($artist)){
            return redirect()->route('admin.dashboard');
        }
        $response = [
            'status' => 'failed',
            'msg' => 'Ops! Something went wrong!',
        ];
        $artist = Artist::find($artist);
        if ($request->method() == 'PATCH' && $artist) {
            $data = Input::all();
            if ($artist->update($data)) {
                /**** TAG ASSOCIATION ****/
                manyToManyAssociation((isset($data['tags'])) ? $data['tags'] : null, $artist, '\App\Models\Tag', 'tags', 'tag_ids');
                manyToManyAssociation((isset($data['categories'])) ? $data['categories'] : null, $artist, '\App\Models\ArtistsCategory', 'categories', 'category_ids');
                manyToManyAssociation((isset($data['galleries'])) ? $data['galleries'] : null, $artist, '\App\Models\Gallery', 'galleries', 'gallery_ids');

                $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($artist->id)->first();
                if (!$site_map) {
                    $site_map = SiteMap::create([
                        "url" => $this->_url_prefix . $artist->slug,
                        "type" => $this->_type_url,
                        "referer_id" => $artist->id
                    ]);
                } else {
                    $site_map->url = $this->_url_prefix . $artist->slug;
                    $site_map->save();
                }

                // associo l'utenza salvata
                $user = $artist->user()->first();
                if( !$user ) {
                    $this->createUser($artist, $data);
                }
                else {
                    // abilitazione utenza se box checcato e con valore
                    if( $request->has('artist_status') && $request->get('artist_status') == 1 ) {
                        $user->status = 1;
                        $user->user_first_enable = 1;
                        // all'update, se si seleziona la creazione dell'utenza si deve riassociare la psw
                        $password = str_random(10);
                        $user->password = bcrypt($password);
                        $user->save();

                        // identifico il tipo di utenza che ha creato il nuovo utente
                        if( roleCheck('gallerist') ) {
                            $user_kind_with_article = 'A Gallerist';
                        } elseif( roleCheck('fair') ) {
                            $user_kind_with_article = 'A Fair';
                        } else {
                            $user_kind_with_article = 'An Administrator';
                        }

                        // mando mail
                        if ($user && $user->status == 1 ) {

                            $to = $user->email;
                            $name = $user->full_name;
                            $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                            $extras = array(
                                'username' => $user->username,
                                'password' => $password,
                                'user_kind_with_article' => $user_kind_with_article
                            );

                            Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                                $message->to($to, $name)->subject($subject);
                            });
                        }
                    }
                }

                if(roleCheck('gallerist')){
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null,Notification::NOTIFICATION_USER_EDIT_ARTIST, 'artist', $artist);
                    Notification::addUsers($notify, $users);
                }

                if(roleCheck('artist')){
                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_EDIT_ARTIST_PROFILE, 'artist', $artist);
                    Notification::addUsers($notify, $users);
                }

                $response = [
                    'status' => 'success',
                    'directory' => $this->_directory_images,
                    'id_row' => $artist->id,
                    'redirectTo' => route('admin.artists.index')
                ];
                $request->session()->flash("success", "Artist with name `{$artist->first_name} {$artist->last_name}` is successful updated!");
            }
        }

        return response()->json($response);
    }

    public function destroy($artist)
    {
        if(Auth::user()->hasRole('gallerist') && check_if_artist_has_multiple_gallery($artist)){
            return redirect()->route('admin.dashboard');
        }
        $artist = Artist::find($artist);
        if (Storage::disk()->exists(public_path("uploads/artists/{$artist->id}"))) {
            Storage::disk()->deleteDirectory(public_path("uploads/artists/{$artist->id}"));
        }
        $artist->user()->delete();
        $artist->delete();

        $site_map = SiteMap::whereType($this->_type_url)->whereRefererId($artist->id)->first();
        $site_map->delete();

        if($artist->slug)
            create_redirect_301_on_delete(parse_url(route('artists.single', [$artist->slug]), PHP_URL_PATH), parse_url(route('artists.archive.all'), PHP_URL_PATH));

        Session::flash('success', "Artist with name `{$artist->first_name} {$artist->last_name}` is successful removed!");
        return redirect(route('admin.artists.index'));
    }

    private function createUser($artist, $data)
    {
        if (isset($data['created_user']) && $data['created_user'] == '1') {
            $password = str_random(10);
            $username = strtolower($data['first_name'] . "." . $data['last_name']);
            $check = User::whereUsername($username)->count();
            if ($check > 0) {
                $username .= "." . str_random(6);
            }

            $activation_code = hash_hmac('sha256', str_random(40), config('app.key'));
            $user = User::create([
                'username' => strtolower($username),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($password),
                'activation_code' => $activation_code,
                'status' => ($data['artist_status']) ? 1 : 0,
                'user_first_enable' => ($data['artist_status']) ? 1 : 0
            ]);

            $user->assignRole('artist');
            $user->artist()->save($artist);
            $user_kind_with_article = '';

            // identifico il tipo di utenza che ha creato il nuovo utente
            if( roleCheck('gallerist') ) {
                $user_kind_with_article = 'A Gallerist';
            } elseif( roleCheck('fair') ) {
                $user_kind_with_article = 'A Fair';
            } else {
                $user_kind_with_article = 'An Administrator';
            }

            if ($user && $user->status == 1 ) {

                $to = $user->email;
                $name = $user->full_name;
//                $subject = "Hi {$user->full_name}! Welcome to Kooness.com";
                $subject = SUBJECT_MAIL_REGISTRATION_USER_ACTIVATED;
                $link = url("activate/$activation_code");
                $extras = array(
                    'username' => $user->username,
                    'password' => $password,
                    'user_kind_with_article' => $user_kind_with_article
                );

                Mail::send('emails.auth.activation-admin', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });
            }
        }
    }

    public function associateArtistToGallery(Request $request)
    {
        if ($request->has('artist_id')) {
            $gallery = auth()->user()->gallery()->first();
            $artist = Artist::find($request->get('artist_id'));
            if ($gallery && $artist) {
                if (!$gallery->artists->contains($artist->id)) {
                    $gallery->artists()->attach($artist->id);
                    $request->session()->flash("success", "Artist with name `{$artist->first_name} {$artist->last_name}` is successful added to your gallery!");
                } else {
                    $request->session()->flash("error", "Artist with name `{$artist->first_name} {$artist->last_name}` is already added to your gallery!");
                }
            } else {
                $request->session()->flash("error", "Whoops, looks like something went wrong.");
            }
        }
        return response()->json([
            'redirectTo' => route('admin.artists.index')
        ]);
    }
}
