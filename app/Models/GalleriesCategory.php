<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleriesCategory extends Model
{
    protected $guarded = [];

    public function parent(){
        return $this->hasOne("App\Models\GalleriesCategory","id", 'parent_id');
    }

    public function galleries(){
        return $this->belongsToMany('App\Models\Gallery', 'galleries_galleries_categories', 'category_id', 'gallery_id');
    }

}
