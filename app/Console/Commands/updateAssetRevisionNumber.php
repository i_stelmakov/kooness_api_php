<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class updateAssetRevisionNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:revision:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the timestamp of asset in file not versioned called asset.rev';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = time();
        $file = storage_path('config/asset.rev');
        file_put_contents($file, $time);

        echo "New Asset Revision created ($time)\n";
    }
}
