@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : 'Kooness | News')

@section('body-class', 'news-archive')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : 'News' }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="gallery-news-archive">
            <div class="boxed-container container-vertical-padding-top container-vertical-padding-bottom">
                <div class="container col-container-with-offset-and-margin">
                    <!-- Loop Item !-->
                    @foreach($posts as $post_key=>$post)
                        {{--START - Item--}}
                        <div class="gallery-news-item col one-half-col-with-margin">
                            <div class="gallery-news-item-img">
                                <a title="{{ $post->title }}" href="{{ route('posts.single', ['news', $post->slug]) }}" class="background-cover square-box lazy" data-src="{{ $post->url_wide_box }}"></a>
                            </div>
                            <div class="gallery-news-item-txt">
                                <div>
                                    <p class="news-date">{{  date('d F Y', strtotime($post->date)) }}</p>
                                    <div class="default-sheet-row">
                                        <h4 class="no-margin">
                                            <a title="{{ $post->title }}" class="dark-text" href="{{ route('posts.single', ['news', $post->slug]) }}">{{ $post->title }}</a>
                                        </h4>
                                    </div>
                                    <p>{!! strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr( $post->content , 0, 201))); !!}...</p>
                                </div>
                                <a title="{{ $post->title }}" class="read-more" href="{{ route('posts.single', ['news', $post->slug]) }}">Read more</a>
                            </div>
                        </div>
                    {{--END - Item--}}
                @endforeach
                <!-- Loop Item !-->
                </div>
                {!! $posts->links('layouts.includes.pagination') !!}
            </div>
        </section>
@endsection