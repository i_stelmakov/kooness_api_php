<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOffer extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->hasOne('App\Models\User', 'id','user_id');
    }

    public function artwork(){
        return $this->hasOne('App\Models\Artwork', 'id','artwork_id');
    }
}
