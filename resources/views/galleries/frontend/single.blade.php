@extends('layouts.app')

@section('title', ($gallery && $gallery->meta_title) ? $gallery->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$gallery->name}"))

@section('body-class', 'galleries-single')

@section('header-og-abstract', ($gallery && $gallery->meta_description) ? $gallery->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($gallery && $gallery->canonical) ? $gallery->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($gallery && $gallery->meta_keywords) ? $gallery->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($gallery->url_full_image) ? $gallery->url_full_image : null)

@section('header-og-url', Request::url())

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($gallery->h1)? $gallery->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $gallery->name) }}</h1>
                        </div>

                        @if(Auth::user())
                            <a class="follow secondary-link {{ (Auth::user()->collectionGalleries->contains($gallery->id)) ? 'active' : '' }}" data-section="galleries" data-id="{{$gallery->id}}">
                                @if(Auth::user()->collectionGalleries->contains($gallery->id))
                                    <i class="fa fa-heart"> </i>
                                    <span class="follow-unfollow">
                                            Unfollow
                                        </span>
                                @else
                                    <i class="fa fa-heart-o"> </i>
                                    <span class="follow-unfollow">
                                            Follow
                                        </span>
                                @endif
                            </a>
                        @endif

                    </div>
                </div>
            </div>
        </section>
        <!-- Gallery Sheet !-->
        <section class="default-sheet-section">
            <div class="container boxed-container">
                <div class="container col-container">
                    <!-- Gallery Sheet Col !-->
                    <div id="default-sheet" class="col one-third-col">
                        <!-- Gallery Info !-->
                        <div id="default-sheet-info">
                            <div class="default-sheet-box">
                                <div class="default-sheet-row flex-wrap">
                                    <div class="default-sheet-row">
                                        <p class="dark-text">
                                            <strong>Address</strong>
                                        </p>
                                    </div>
                                    <div id="event-adress" class="default-sheet-row">
                                        <h4>{{ $gallery->city }}, {{ $country->name }}</h4>
                                    </div>
                                    <div class="default-sheet-row">
                                        <p>{{ $gallery->address }} {{ $gallery->address2 }} {{ $gallery->address3 }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Gallery Sheet Category/Tags !-->
                            <div id="default-sheet-category" class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Category</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <ul>
                                            <li>
                                                @foreach($categories as $category_key=>$category)
                                                    <a title="{{ $category->name }}" href="{{ route('galleries.single', [$category->slug]) }}">
                                                        {{ $category->name }}
                                                    </a>@if (!$loop->last), @endif
                                                @endforeach
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Tags</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <ul>
                                            @foreach($tags as $tag_key=>$tag)
                                                <li>
                                                    <a title="{{ $tag->name }}" href="{{ route('tags.single', [$tag->slug]) }}">{{ $tag->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- Gallery Sheet Social !-->
                            <div id="default-sheet-info" class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Share</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <div class="icon-container">
                                            <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                            <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                            <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                            <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                            <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($gallery->about_gallery, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Product Sheet Image Col !-->

                    <div id="product-img-col" class="col two-third-col">
                        <!-- Product Sheet Image !-->
                        <div class="slider-item-img wide-box">
                            <a href="{{ $gallery->url_full_image }}" data-lightbox="example-set">
                                <div class="background-cover lazy" data-src="{{ $gallery->url_wide_box }}"></div>
                            </a>
                        </div>
                        {{--<div class="slider-item-img">--}}
                            {{--<a href="{{ $gallery->url_full_image }}" data-lightbox="example-set">--}}
                                {{--<img src="{{ $gallery->url_wide_box }}"/>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </section>
        <div class="container boxed-container">
            <hr class="divider"/>
        </div>
        <!-- Description !-->
        <section class="tab-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin container-vertical-padding-top container-vertical-padding-bottom">
                    <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                        <ul>
                            @if($gallery->about_gallery)
                                <li class="current-tab-item tab-button" data-id="1">About the Gallery</li>
                            @endif
                            @if($gallery->meet_gallerist)
                                <li class="tab-button" data-id="2">Meet the Gallery</li>
                            @endif
                            @if($gallery->news()->whereStatus(1)->count())
                                <li class="tab-button" data-id="3">News</li>
                            @endif
                            @if($gallery->city)
                                <li class="tab-button" data-id="4">Address</li>
                            @endif
                        </ul>
                    </div>
                    <!-- Tab 1 !-->
                    <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $gallery->about_gallery !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 2 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $gallery->meet_gallerist !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 3 !-->
                    @if($gallery->news()->whereStatus(1)->count())
                        <!-- Tab 2 !-->
                        <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                            <section id="featured-news">
                                <div class="container boxed-container">
                                    <div class="default-slider-header">
                                        <span class="h1">Featured News</span>
                                        <p>
                                            <a class="read-more" href="{{ route('posts.archive.all', ["news"]) }}">All featured news</a>
                                        </p>
                                    </div>
                                    <div class="container col-container-with-offset-and-margin">
                                        @foreach($gallery->news()->whereStatus(1)->limit(3)->get() as $news)
                                            <div class="slider-item one-third-col-with-margin">
                                                <div class="slider-item-img wide-box">
                                                    <a title="{{ $news->title }}" href="{{ route('posts.single', ["news", $news->slug]) }}">
                                                        <div class="background-cover gallery lazy" data-src="{{ $news->url_wide_box }}"></div>
                                                    </a>
                                                </div>
                                                <div class="featured-news-item-txt">
                                                    <p class="news-date">{{  date('d F Y', strtotime($news->date)) }}</p>
                                                    <div class="default-sheet-row">
                                                        <h2>
                                                            <a class="dark-text" href="{{ route('posts.single', ["news", $news->slug]) }}">{{ $news->title }}</a>
                                                        </h2>
                                                    </div>
                                                    <div class="default-sheet-row-cell">
                                                        <p class="dark-text news-author">By <a>{{ $news->author }}</a>
                                                        <p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endif
                <!-- Tab 4 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="4">
                        @if($gallery->city)
                            <h2>Address 1</h2>
                            <div id="default-sheet-info">
                                <div class="default-sheet-box">
                                    <div class="default-sheet-row flex-wrap">
                                        <div class="default-sheet-row">
                                            <p class="dark-text">
                                                <strong>Address</strong>
                                            </p>
                                        </div>
                                        <div id="event-adress" class="default-sheet-row">
                                            <h4>{{ $gallery->address }}<br/>{{ $gallery->zip }} {{ $gallery->city }}
                                                <br/>{{ $gallery->country }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($gallery->city_2 && $gallery->zip_2 && $gallery->address_2)
                            <h2>Address 2</h2>
                            <div id="default-sheet-info">
                                <div class="default-sheet-box">
                                    <div class="default-sheet-row flex-wrap">
                                        <div class="default-sheet-row">
                                            <p class="dark-text">
                                                <strong>Address</strong>
                                            </p>
                                        </div>
                                        <div id="event-adress" class="default-sheet-row">
                                            <h4>{{ $gallery->address_2 }}
                                                <br/>{{ $gallery->zip_2 }} {{ $gallery->city_2 }}
                                                <br/>{{ $gallery->country2 }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($gallery->city_3 && $gallery->zip_3 && $gallery->address_3)
                            <h2>Address 3</h2>
                            <div id="default-sheet-info">
                                <div class="default-sheet-box">
                                    <div class="default-sheet-row flex-wrap">
                                        <div class="default-sheet-row">
                                            <p class="dark-text">
                                                <strong>Address</strong>
                                            </p>
                                        </div>
                                        <div id="event-adress" class="default-sheet-row">
                                            <h4>{{ $gallery->address_3 }}
                                            <br/>{{ $gallery->zip_3 }} {{ $gallery->city_3 }}
                                            <br/>{{ $gallery->country3 }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>

        @if($gallery->artists()->whereStatus(1)->count())
            <section id="represented-artists-slider" class="default-slider-section ">
                <div class="container boxed-container">
                    <div class="default-slider-header">
                        <span class="h1">Represented Artists</span>
                        <p>
                            <a class="read-more" href="{{ route('artists.archive.all', ["gallery" => $gallery->slug]) }}">All represented Artists</a>
                        </p>
                    </div>
                    <div class="container col-container-with-offset">
                        @foreach($gallery->artists()->whereStatus(1)->limit(5)->get() as $artist)
                            <div class="slider-item one-fifths-col-with-margin half-width-mobile">

                                <div class="slider-item-img square-box">
                                    <a href="{{ route('artists.single', [$artist->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $artist->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <h2>
                                                <a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                            </h2>
                                        </div>

                                        @if($artist->slug)
                                            @if(Auth::user())
                                                <div class="add-item follow {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                                    @if(Auth::user()->collectionArtists->contains($artist->id))
                                                        <i class="fa fa-heart"></i>
                                                    @else
                                                        <i class="fa fa-heart-o"></i>
                                                    @endif
                                                </div>
                                            @endif
                                        @endif

                                    </div>
                                    <a title="Read more" class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">Read more</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

        @if(count(array_filter($exhibitions)))
            <section id="event-slider" class="default-slider-section slider-border">
                <div class="container boxed-container">
                    <div class="default-slider-header">
                        <span class="h1">Featured events</span>
                        <p>
                            <a class="read-more" href="{{ route('exhibitions.archive') }}">All featured events</a>
                        </p>
                    </div>
                    <div class="container col-container-with-offset-and-margin">
                        @foreach($exhibitions AS $key=>$exhibition)
                            @if(!$exhibition)
                                @continue
                            @endif
                            <div class="slider-item one-third-col-with-margin">
                                <div class="on-going-ticket">
                                    <h4>{{ ucfirst($key) }}</h4>
                                </div>
                                <div class="slider-item-img wide-box">
                                    <a title="{{ $exhibition->name }}" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">
                                        <div class="background-cover gallery lazy" data-src="{{ $exhibition->url_wide_box }}"></div>
                                    </a>
                                </div>
                                <div class="slider-item-txt">
                                    <h2><a title="{{ $exhibition->name }}" class="dark-text" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">{{ $exhibition->name }}</a></h2>
                                    <div class="event-date">
                                        <p class="event-date-p">{{ $exhibition->city }}, {{ $exhibition->country()->first()->code }}</p>
                                    </div>
                                    <div class="default-sheet-row slider-item-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text event-adress">From {{ date('d/m/Y', strtotime($exhibition->start_date)) }} to {{ date('d/m/Y', strtotime($exhibition->end_date)) }}</p>
                                        </div>
                                    </div>
                                    <a title="Read more" class="read-more" href="{{ route('exhibitions.single', [$exhibition->slug]) }}">Read more</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif
        <!-- Post Navigation !-->
        <div class="container boxed-container">
            <div id="post-nav" class="container col-container-with-offset">
                @if($prev)
                    <div class="col one-third-col prev">
                        <div class="default-sheet-col">
                            <a class="prev-a" href="{{ route('galleries.single', [$prev->slug]) }}">Prev galley</a>
                            <h3>
                                <a href="{{ route('galleries.single', [$prev->slug]) }}">{{ $prev->name }}</a>
                            </h3>
                        </div>
                    </div>
                @endif
                @if($next)
                    <div class="col one-third-col next">
                        <div class="default-sheet-col">
                            <a class="next-a" href="{{ route('galleries.single', [$next->slug]) }}">Next gallery</a>
                            <h3>
                                <a href="{{ route('galleries.single', [$next->slug]) }}">{{ $next->name }}</a>
                            </h3>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection