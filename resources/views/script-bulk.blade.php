<script>
    var ajaxProgressCall = null;
    var errorBeforeCall = false;
    var xhr = null;

    function resetUploader() {
        $('.bulk-upload').prop('disabled', false);
        $('.upload-div').hide();
        $('.fieldset-upload').hide();
        $('.progress-upload .progress-bar').css('width', '0%').html('');
        $('.upload-verify-div .progress-bar').css('width', '0%').html('').removeClass('progress-bar-danger');
        $('.progress-upload-process .progress-bar').css('width', '0%').html('');
        $('.upload-btn').prop('disabled', true);
        $('.upload-verify-div').hide();
        $('.upload-process-div').hide();
        $('.continue-btn').unbind();
        ajaxProgressCall = null;
    }

    function verifyXls(fileName, type) {
        $.ajax({
            url: '/admin/bulk/uploads/verify/xls/' + fileName + '/type/' + type,
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('.upload-verify-div').slideDown();
            },
            success: function (data) {
                var progress_bar = $('.upload-verify-div .progress-bar');
                progress_bar.css("width", "100%").html("100%");
                if (data.result === 'success') {
                    processXls(data.file, type);
                } else {
                    progress_bar.addClass("progress-bar-danger");
                }
            }
        })
    }

    function processXls(fileName, type) {
        $.ajax({
            url: '/admin/bulk/uploads/process/xls/' + fileName + '/type/' + type,
            type: 'POST',
            dataType: 'json',
            async: true,
            beforeSend: function () {
                errorBeforeCall = false;
                $('.upload-process-div').slideDown();
                setTimeout(function () {
                    statusProgress(fileName);
                }, 1000);
            },
            success: function (data) {
                if (data.result == 'success') {
                    statusProgress(fileName);
                    setTimeout(function () {
                        window.location.reload(true);
                    }, 1500);
                }

            },
            error: function () {
                errorBeforeCall = true;
                if (ajaxProgressCall == null)
                    ajaxProgressCall.abort();
            }
        })
    }

    function statusProgress(fileName) {
        if (errorBeforeCall)
            return;
        ajaxProgressCall = $.ajax({
            url: '/admin/bulk/uploads/process/xls/' + fileName + '/status',
            type: 'POST',
            dataType: 'json',
            async: true,
            success: function (data) {
                $('.progress-upload-process .progress-bar').css('width', parseInt(data.progress) + "%").html(data.progress + "%");
                if (parseInt(data.progress) !== 100)
                    setTimeout(function () {
                        statusProgress(fileName);
                    }, 5000);

            }
        })
    }

    $(function () {
        var upload_btn = $('.upload-btn');
        upload_btn.prop('disabled', true);
        upload_btn.click(function () {
            $(this).prop('disabled', true);
            $('.form-upload').submit();
        });
        var input = $('.bulk-upload');
        input.click(function () {
            $('.error-upload').slideUp();
        });
        input.change(function () {
            resetUploader();
            var fileName = $(this).val();
            var ext = fileName.substr(fileName.length - 3, fileName.length);
            if (ext !== 'xls') {
                $('.error-upload').html('Attention: the selected file is not an xls file').slideDown();
                $(this).val('');
                upload_btn.prop('disabled', true);
            } else {
                upload_btn.prop('disabled', false);
            }
        });

        $('.form-upload').submit(function (e) {
            var type = $(this).data('type');
            e.preventDefault();
            var fd = new FormData(this);
            $.ajax({
                url: $(this).attr('action'),
                xhr: function () {

                    var xhr = new XMLHttpRequest();
                    var total = 0;

                    $.each(document.getElementById('file').files, function (i, file) {
                        total += file.size;
                    });

                    xhr.upload.addEventListener("progress", function (evt) {
                        var loaded = (evt.loaded / total).toFixed(2) * 100;
                        if (loaded > 100)
                            loaded = 100;
                        $('.progress-upload .progress-bar').css('width', loaded + '%').html(loaded + '%');
                    }, false);

                    return xhr;
                },
                type: 'post',
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    $('.upload-div').show();
                    $('.fieldset-upload').slideDown();
                },
                success: function (data) {
                    ajaxProgressCall = null;
                    input.val('');
                    input.prop('disabled', true);
                    verifyXls(data.file, type);
                }
            });
        });

        $('.download-btn').click(function () {
            $(this).attr('disabled', true);
            window.location.href = '/admin/bulk/uploads/download/xls/' + $(this).data('type');
        });
    });
</script>