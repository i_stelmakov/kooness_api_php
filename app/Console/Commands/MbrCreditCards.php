<?php

namespace App\Console\Commands;

use App\Models\MbrCard;
use App\Models\MbrPlan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class mbrCreditCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'membership:credit-card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check that the credit card expiration month is subsequent to the current one (execute only on first day of every month)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cards = MbrCard::all();
        foreach ($cards as $card) {
            $card_check_date = date('Y-m-d', mktime(0, 0, 0, date('m', strtotime($card->expire_date)) - 1 , 1, date('Y', strtotime($card->expire_date))));
            $now =
//                date('Y-m-d');
            date('Y-m-d', mktime(0, 0, 0, date('m') , 1, date('Y')));
            if($card_check_date == $now){
                $user = $card->user()->first();
                if ($user->is_premium && $user->current_plan_id) {
                    $plan = MbrPlan::find($user->current_plan_id);
                    $to = $user->email;
                    $name = $user->full_name;
                    $subject = SUBJECT_MAIL_MEMBERSHIP_CARD_EXPIRATION;
                    $extras = [
                        "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                        "plan" => $plan, //	name
                        "card" => $card
                    ];
                    Mail::send('emails.gallerists.card-expiration', $extras, function ($message) use ($to, $name, $subject) {
                        $message->to($to, $name)->subject($subject);
                    });
                }
            }
        }
    }
}
