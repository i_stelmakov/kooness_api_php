@extends('layouts.admin')
@section('title', "Admin | Edit Artwork Medium Category `{$categoryMedium->medium()->first()->name} - {$categoryMedium->category()->first()->name}`")
@section('page-header', "Admin - Edit Artwork Category `{$categoryMedium->medium()->first()->name} - {$categoryMedium->category()->first()->name}`")

@section('content')
    @include('artworks.medium-categories.form', ['action' => route('admin.artworks.medium.categories.update', [$categoryMedium->id]), 'method' => 'PATCH'])
@endsection