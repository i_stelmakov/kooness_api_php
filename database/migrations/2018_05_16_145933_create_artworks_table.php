<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artworks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id')->unsigned();
            $table->integer('gallery_id')->unsigned();
            $table->string('reference');
            $table->string('title');
            $table->string('slug');
            $table->string('from_the_series')->nullable();
            $table->year('year');
            $table->float('width');
            $table->float('height');
            $table->float('depth')->nullable();
            $table->boolean('single_piece')->default(0);
            $table->boolean('signed')->default(0);
            $table->boolean('dated')->default(0);
            $table->boolean('titled')->default(0);
            $table->text('about_the_work')->nullable();
            $table->text('news')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->float('price')->default(0);
            $table->string('current_city')->nullable();
            $table->integer('current_country')->unsigned()->nullable();

            $table->integer('created_by')->unsigned()->nullable();

            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->timestamps();

            $table->foreign('artist_id')
                ->references('id')
                ->on('artists')
                ->onDelete('CASCADE');

            $table->foreign('gallery_id')
                ->references('id')
                ->on('galleries')
                ->onDelete('CASCADE');

            $table->foreign('current_country')
                ->references('id')
                ->on('countries')
                ->onDelete('CASCADE');

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artworks');
    }
}
