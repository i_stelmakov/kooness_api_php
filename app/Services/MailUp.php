<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 20/08/18
 * Time: 09:23
 */

namespace App\Services;

use App\Models\User;
use Caereservices\Mailup\MailupStatus;
use Caereservices\Mailup\MailupException;
use Caereservices\Mailup\MailupClient;


class MailUp
{

    protected $_client_id;

    protected $_client_secret;

    protected $_user;

    protected $_password;

    protected $_group;

    protected $_redirect_uri;

    protected $_instance;

    public function __construct()
    {
        $this->_client_id = config('services.mailup.client_id', null);
        $this->_client_secret = config('services.mailup.client_secret', null);
        $this->_user = config('services.mailup.user', null);
        $this->_password = config('services.mailup.pwd', null);
        $this->_group = config('services.mailup.group', null);
        $this->_redirect_uri = config('services.mailup.redirect', null);
        $this->init();
    }

    public function init(){
        $this->_instance = null;
        try {
            $this->_instance = new MailupClient($this->_client_id, $this->_client_secret, $this->_redirect_uri);
            if( $this->_instance ) {
                $result = $this->_instance->login($this->_user, $this->_password);
                if( $result == MailupStatus::OK ) {
                }
            }
        } catch (MailupException $e) {
        }
    }

    public function subscribeUser(User $user){
        $result = $this->_instance->addUserToGroup([
            'mail' => $user->email,
            'name' => $user->first_name,
            'surname' => $user->last_name,
        ], $this->_group);

        if($result == MailupStatus::OK){
            return $this->_redirect_uri;
        } else{
            return false;
        }
    }
}