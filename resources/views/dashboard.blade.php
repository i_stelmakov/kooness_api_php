@extends('layouts.admin')
@section('content')

    <?php
        // recupero lista ultimi 12 mesi
        $months_years = [];
        for ($i = 1; $i <= 12; $i++) {
            $months_years[$i]['month_name'] = date("F", strtotime(date('Y-m-01') . " -$i months"));
            $months_years[$i]['month'] = date("m", strtotime(date('Y-m-01') . " -$i months"));
            $months_years[$i]['year'] = date("Y", strtotime(date('Y-m-01') . " -$i months"));
        }
        $months_years = array_reverse($months_years);

        // recupero Ordini per mese
        $orders_per_months = \App\Models\Order::selectRaw('count(*) as counter, MONTHNAME(created_at) as month, MONTH(created_at) as month_number, YEAR(created_at) as year')->where('status', '>=', '2')->whereRaw('created_at >= DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 1 YEAR)')->groupBy(DB::raw('MONTHNAME(created_at), MONTH(created_at), YEAR(created_at)'))->orderBy(DB::raw('MONTH(created_at)'))->get();
        $max = max(array_column($orders_per_months->toArray(), 'counter'));

        // recupero Top Artworks
        $top_artworks = \App\Models\Artwork::selectRaw('artworks.id, title, slug, COUNT(*) AS counter')
            ->join(
                DB::raw('( SELECT ua.artwork_id, ua.user_id FROM users_artworks as ua UNION DISTINCT SELECT uca.artwork_id, uc.user_id FROM user_collection_artworks as uca JOIN user_collections as uc ON uca.collection_id = uc.id ) as t'),
                    'artworks.id',
                    '=',
                    't.artwork_id'
            )
            ->where('artworks.status', '<=', '2') // 1 VISIBLE, 4 READY, 2 SOLD, 3 SUSPENDED
            ->groupBy('artwork_id')
            ->orderBy('counter', 'DESC')
            ->limit(5)
            ->get();

        // recupero Top Galleries
        $top_galleries = \App\Models\Gallery::selectRaw('galleries.id, name, slug, COUNT(*) AS counter')
            ->join(
                DB::raw('users_galleries'),
                'galleries.id',
                '=',
                'users_galleries.gallery_id'
            )
            ->groupBy('gallery_id')
            ->orderBy('counter', 'DESC')
            ->limit(5)
            ->get();

        // recupero Top Artists
        $top_artists = \App\Models\Artist::selectRaw('artists.id, first_name, last_name, slug, COUNT(*) AS counter')
            ->join(
                DB::raw('users_artists'),
                'artists.id',
                '=',
                'users_artists.artist_id'
            )
            ->groupBy('artist_id')
            ->orderBy('counter', 'DESC')
            ->limit(5)
            ->get();

        // recupero Latest Artists
        $latest_artworks = \App\Models\Artwork::selectRaw('artworks.id, title, slug, created_at')
            ->orderBy('created_at', 'DESC')
            ->limit(5)
            ->get();

        // recupero Latest Users
        $latest_users = \App\Models\User::selectRaw('users.id, username, created_at')
            ->orderBy('created_at', 'DESC')
            ->limit(5)
            ->get();

        // conto Users
        $count_users = \App\Models\User::count();

        // conto Artworks
        $count_artworks = \App\Models\Artwork::count();

        // conto Artist
        $count_artists = \App\Models\Artist::count();

        // conto Galleries
        $count_galleries = \App\Models\Gallery::count();

        // conto Post
        $count_posts = \App\Models\Post::count();


    ?>


    <section class="content">

        @role('superadmin')
            <h1>Superadmin Dashboard</h1>
        @endrole
        @role('admin')
            <h1>Admin Dashboard</h1>
        @endrole
        @role('seo')
            <h1>Seo Dashboard</h1>
        @endrole
        @role('fair')
            <h1>Fair Dashboard</h1>
        @endrole
        @role('gallerist')
            <h1>Gallerist Dashboard</h1>
        @endrole
        @role('artist')
            <h1>Artist Dashboard</h1>
        @endrole
        @role('editor')
            <h1>Editor Dashboard</h1>
        @endrole

        @if(roleCheck('superadmin|admin'))
            <div class="row">
                {{-- Box 1 - Sales per Country --}}
                <div class="col-md-6">
                    {{-- Box Content --}}
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sales per Country</h3>
                        </div>
                        <div class="box-body">
                            <div id="worldmap">
                                {{--worldmap canvas container--}}
                            </div>
                        </div>
                    </div>
                    {{-- .Box Content --}}
                </div>
                {{-- Box 2 - Sales per Month --}}
                <div class="col-md-6">
                    {{-- Box Content --}}
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sales withing last Month</h3>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <!-- line chart canvas element -->
                                <canvas id="buyers" width="600" height="320"></canvas>
                            </div>
                        </div>
                    </div>
                    {{-- .Box Content --}}
                </div>
            </div>
            <div class="row">
                {{-- Box 3 - Latest Published --}}
                <div class="col-md-6">
                    {{-- Box Content --}}
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest inserted</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Counters</h4>
                                    <p>
                                        <b>Users: </b>{{$count_users}}<br>
                                        <b>Artworks: </b>{{$count_artworks}}<br>
                                        <b>Arists: </b>{{$count_artists}}<br>
                                        <b>Galleries: </b>{{$count_galleries}}<br>
                                        <b>Posts: </b>{{$count_posts}}<br>
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <h4>Latest Artworks</h4>
                                    <p>
                                        @foreach ($latest_artworks as $latest_artwork)
                                            <a href='{{ route('artworks.single', [$latest_artwork->slug]) }}'>
                                                {{$latest_artwork->title}}
                                            </a><br>
                                        @endforeach
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <h4>Latest Subscribers</h4>
                                    <p>
                                        @foreach ($latest_users as $latest_user)
                                            <a href='{{ route('admin.users.edit', [$latest_user->id]) }}'>
                                                {{$latest_user->username}}
                                            </a><br>
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- .Box Content --}}
                </div>
                {{-- Box 4 - Top Five --}}
                <div class="col-md-6">
                    {{-- Box Content --}}
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Top Five</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Top Artworks</h4>
                                    <p>
                                        @foreach ($top_artworks as $top_artwork)
                                            <a href='{{ route('artworks.single', [$top_artwork->slug]) }}'>
                                                {{$top_artwork->title}} ({{$top_artwork->counter}})
                                            </a><br>
                                        @endforeach
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <h4>Top Galleries</h4>
                                    <p>
                                        @foreach ($top_galleries as $top_gallery)
                                            <a href='{{ route('galleries.single', [$top_gallery->slug]) }}'>
                                                {{$top_gallery->name}} ({{$top_gallery->counter}})
                                            </a><br>
                                        @endforeach
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <h4>Top Artists</h4>
                                    <p>
                                        @foreach ($top_artists as $top_artist)
                                            <a href='{{ route('artists.single', [$top_artist->slug]) }}'>
                                                {{$top_artist->first_name}} {{$top_artist->last_name}} ({{$top_artist->counter}})
                                            </a><br>
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- .Box Content --}}
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <p>Welcome to Kooness!<br>
                        From the left bar you can manage the sections that compete with you.
                    </p>
                </div>
            </div>
        @endif

    </section>

    @if(roleCheck('superadmin|admin'))
        <script src="js/jquery.twism.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
        <script>
            $(document).ready(function () {

                // #############################################
                // ### Sales per Country
                // #############################################

                // standard colours
                var colorFountainBlue = '#48b6ac';
                var colorCodGray = '#101010';
                var colorWhite = '#ffffff';
                var colorBlack = '#000000';
                var colorTwine = '#c2a661';
                var colorMineShaft = '#222222';
                var colorGray1 = '#888888';
                var colorGray2 = '#eeeeee';
                var colorMercury = '#e5e5e5';
                // best sell colours
                var colorCrimsonLow = '#ffcccc';
                var colorCrimsonMedium = '#ff6666';
                var colorCrimsonHigh = '#e60000';

                $('#worldmap').twism("create",
                    {
                        map: "world",
                        border: colorFountainBlue,
                        color: colorGray2,
                        borderWidth: 1,
                        backgroundColor: colorWhite,
                        hoverColor: colorFountainBlue,
                        hoverBorder: colorGray1,
                        customMap: '/images/world-map.svg',
                        individualCountrySettings: [
                                @foreach(\App\Models\Order::selectRaw('count(*) as sell_counter, billing_country_code as country_code')->where('status', '>=', '2')->groupBy('billing_country_code')->get() as $row )
                            {
                                name: "{{strtolower($row->country_code)}}",
                                color: {{ ($row->sell_counter >= 3) ? 'colorCrimsonHigh' : (($row->sell_counter == 2) ? 'colorCrimsonMedium' : 'colorCrimsonLow') }}
                            },
                            @endforeach
                        ]
                    }
                );


                // #############################################
                // ### Sales per Month
                // #############################################
                var buyerData = {
                    labels: [
                        <?php
                        // loop per mostrare mese per mese
                        foreach ($months_years as $month_year) {
                            echo '"' . $month_year['month_name'] . ' ' . $month_year['year'] . '",';
                        }
                        ?>

                    ],
                    datasets: [
                        {
                            fillColor: "rgba(172,194,132,0.4)",
                            strokeColor: "#ACC26D",
                            pointColor: "#fff",
                            pointStrokeColor: "#9DB86D",
                            data: [
                                <?php
                                // loop per mostrare mese per mese
                                foreach ($months_years as $month_year) {
                                    // loop per mostrare elementi venduti questo mese
                                    $month_order_sum = 0;
                                    foreach ($orders_per_months as $order_per_month) {
                                        if ($order_per_month['month_number'] == $month_year['month']) {
                                            $month_order_sum += $order_per_month['counter'];
                                        }
                                    }
                                    echo($month_order_sum . ',');
                                }
                                ?>

                            ]
                        }
                    ]
                }
                {{$max = 50}}
                // get line chart canvas
                var buyers = document.getElementById('buyers').getContext('2d');
                // draw line chart
                new Chart(buyers).Line(buyerData, {
                    scaleOverride: true,
                    scaleSteps: 10,
                    scaleStepWidth: 1,
                    scaleStartValue: 0
                });

            });
        </script>
    @endif

@endsection