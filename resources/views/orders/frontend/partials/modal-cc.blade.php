<style>
    .k-form .transparent {
        opacity: 0.2;
    }
</style>
<div id="ccModal" data-remodal-id="ccModal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div>
        <h2 id="modal1Title"></h2>
        <div id="modal1Desc" class="k-form">
            <div class="col-container-with-offset-and-margin">
                <input type="text" class="full-col-with-margin" id="owner" placeholder="Owner">
                <input type="text" class="full-col-with-margin" id="cvv" placeholder="CVV">
                <input type="text" class="full-col-with-margin" id="cardNumber" placeholder="Card Number">
             <select class="two-third-col-with-margin" id="cardMonth">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select> <select class="one-third-col-with-margin" id="cardYear">
                    @for($i = date('Y'); $i < date('Y') + 10; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group" id="credit_cards">
                <img src="/images/visa.jpg" id="visa">
                <img src="/images/mastercard.jpg" id="mastercard">
                <img src="/images/amex.jpg" id="amex">
                <img src="/images/maestro.jpg" id="maestro">
            </div>
        </div>
        <br>
        <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
        <button data-remodal-action="confirm" class="remodal-confirm" id="confirm-cc">OK</button>
    </div>
</div>