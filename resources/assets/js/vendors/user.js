$(window).on("load", function () {
    /* ==========================================================================
    Generics
    ========================================================================== */
    console.log('User JS');

    // Variables
    var $ = jQuery;

    var inst2 = $('[data-remodal-id=modal-profile]').remodal();
    var inst3 = $('[data-remodal-id=modal-linked]').remodal();

    $("#modal-profile-trigger").click(function () {
        var $form = $('.user-form');
        $form.find('input:not([type=submit])').val('');
        $form.find('#first_name').val($('.first-name-result').find('p').text());
        $form.find('#last_name').val($('.last-name-result').find('p').text());
        $form.find('#email').val($('.email-result').find('p').text());
        inst2.open();
    });

    $("#modal-linked-trigger").click(function () {
        inst3.open();
    });

    jQuery.validator.addMethod("pwcheck", function (value) {
        return /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/.test(value);
    }, 'The password must be at least 8 characters and contain a minimum of one lower case character, one upper case character, one digit and one special character.');

    $('.user-form').validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            new_password: {
                pwcheck: function () {
                    return $('#old_password').val() !== '';
                },
                required: function () {
                    return $('#old_password').val() !== '';
                }
            },
            confirm_password: {
                required: function () {
                    return $('#new_password').val() !== '';
                },
                equalTo: "#new_password"
            },
        },
        submitHandler: function (form) {
            $.ajax({
                url: '/user/ajax/update/profile',
                data: $(form).serialize(),
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        $('.first-name-result').find('p').text(response.user.first_name);
                        $('.last-name-result').find('p').text(response.user.last_name);
                        inst2.close();
                    } else {
                        if (response.msg === 'old_pwd_error') {
                            //TODO::error password old
                            console.log('Your old password is not the same');
                        }
                    }
                }
            });
        }
    });


    // Edit Addresses and Cards
    function sendData(event) {

        // click event
        var thisEvent = event;
        var thisForm = event.target; // target => the form where start click event

        // Form ID
        var thisFormID = thisForm.id;

        // Form Data-kind
        var thisFormDataKind = thisForm.dataset.kind;

        // Form Data-id
        var thisFormDataID = thisForm.dataset.id;

        // create a new associative array for contain data
        var data = {};

        // if form is for cards
        if (thisFormDataKind === 'cards-editing') {
            console.log('cards-editing');
            console.log('form id => ' + thisFormDataID);

            // Retrieve id if edit, or empty if new
            if (thisFormDataID) {
                data.id = thisFormDataID;
            }

            // Retrieve form inputs values for Cards
            data.owner = event.target.querySelector('input[name="card_owner"]').value;
            // console.log( '🔵' + form_data_id );

            data.cc = event.target.querySelector('input[name="card_number"]').value;
            data.month = event.target.querySelector('select[name="card_exp_month"]').value;
            data.year = event.target.querySelector('select[name="card_exp_year"]').value;
            data.cvv = event.target.querySelector('input[name="card_cvv"]').value;
            data.type = event.target.querySelector('input[name="card_type"]').value;

            $.ajax({
                url: '/user/ajax/save/payment',
                type: 'POST',
                dataType: 'json',
                data: data,
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        var $form = $('#cardForm0');
                        $form.parents('.accordion').toggle("active");
                        $form.parents('.panel').css("maxHeight", "0");

                        var $template = $('<div class="card-item" data-id="' + response.payment.id + '"><hr><div class="card-text"></div></div>');
                        $template.find('.card-text').append('<img src="/images/credit_cards/light/' + response.payment.type.toLowerCase() + '.png">');
                        $template.find('.card-text').append('<p>' + response.payment.type + '- end in ***' + response.payment.end_with + '</p></div>')
                        var $delete = $('<a class="delete-card"><i class="fa fa-edit"></i> Delete Card</a>');
                        $delete.deletePayment();
                        $template.find('.card-text').append($delete);



                        if ($('div[data-remodal-id="modal-cards"]').find('.card-item').length)
                            $template.insertAfter($('div[data-remodal-id="modal-cards"]').find('.card-item:last'));
                        else
                            $template.insertAfter($('div[data-remodal-id="modal-cards"]').find('.panel'));


                        // seleziono il blocco delle carte
                        var creditCards = $('#credit-cards');

                        // se trovo il testo "nessun indirizzo"
                        if (creditCards.find('.empty')) {
                            console.log('elimino la dicitura nessuna carta');
                            // elimino la dicitura
                            creditCards.find('.empty').remove();
                        }
                        // aggiorna la lista degli indirizzi a fondo pagina
                        console.log('appendo nuova carta');

                        // raggruppo tutti i dati in un contenitore per appenderli
                        var $tpl = $('<p class="card-item-account" id="card-' + response.payment.id + '" style="background-image: url(\'/images/credit_cards/light/' + response.payment.type.toLowerCase() + '.png\')">' + response.payment.type + ' - end in ***' + response.payment.end_with + '</p>')
                        // ✅️ se esistono altri elementi
                        if ($('#credit-cards').find('.card-item-account').length) {
                            console.log('esistevano altre carte, appendo');
                            // appendi l'elemento all'ultimo della lista
                            $tpl.insertAfter($('#credit-cards').find('.card-item-account:last'));
                        }
                        // ✅️️ • se non esistono altri elementi
                        else {
                            console.log('non esistevano altre carte, inserisco');
                            // appendi l'elemento come primo della lista
                            $tpl.insertAfter($('#credit-cards').find('p'));
                        }




                    }
                }
            });
        }

        // if form is for addresses
        if (thisFormDataKind === 'addresses-editing') {

            // Retrieve id if edit, or empty if new
            if (thisFormDataID) {
                data.id = thisFormDataID;
            }

            // Retrieve form inputs values for Addresses
            data.type = event.target.querySelector('select[name="type"]').value;
            data.first_name = event.target.querySelector('input[name="first_name"]').value;
            data.last_name = event.target.querySelector('input[name="last_name"]').value;
            data.company_name = event.target.querySelector('input[name="company_name"]').value;
            data.fiscal_code = event.target.querySelector('input[name="fiscal_code"]').value;
            data.vat_number = event.target.querySelector('input[name="vat_number"]').value;
            data.email = event.target.querySelector('input[name="email"]').value;
            data.phone = event.target.querySelector('input[name="phone"]').value;
            data.address_1 = event.target.querySelector('input[name="address"]').value + ", " + event.target.querySelector('input[name="number"]').value;
            data.city = event.target.querySelector('input[name="city"]').value;
            data.state = event.target.querySelector('input[name="state"]').value;
            data.zip = event.target.querySelector('input[name="zip_code"]').value;
            data.country = event.target.querySelector('input[name="country"]').value;
            data.country_code = event.target.querySelector('input[name="country_code"]').value;

            $.ajax({
                url: '/user/ajax/save/address',
                type: 'POST',
                dataType: 'json',
                data: data,
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        // se aggiunta nuovo indirizzo...
                        if (response.action === 'new') {
                            var $form = $('#addressForm0');
                            $form.parents('.accordion').toggle("active");
                            $form.parents('.panel').css("maxHeight", "0");

                            var $row = $(' <div class="address-item"><hr><div class="address-text">' + response.address.label + '</div><a class="accordion" href="#"><i class="fa fa-edit"></i> Edit Address</a><div class="panel"></div></div>');

                            var $template = $form.clone();
                            $template.parents('.accordion').toggle("active");
                            $template.parents('.panel').css("maxHeight", "0");
                            $template.attr("data-id", response.address.id);
                            $template.attr("id", "addressForm" + response.address.id);
                            $.each(response.address, function (index, value) {
                                if (index === 'label') {
                                    var $input = $template.find('#autocomplete-0');
                                    $input.attr("id", "autocomplete-" + response.address.id);
                                    $input.attr("name", "autocomplete-" + response.address.id);
                                    $input.initAutocomplete();
                                    $input.val(value);
                                }
                                if ($template.find('input[name=' + index + ']').length) {
                                    $template.find('input[name=' + index + ']').val(value);
                                }
                            });

                            var $delete = $('<button class="small-input-button delete-address">Delete Address</button>');
                            $delete.deleteAddress();
                            $template.append($delete);

                            $template.find('input[type=submit]').attr('value', 'Save Address');

                            $template.validate({
                                submitHandler: function (form, event) {
                                    sendData(event);
                                }
                            });

                            $row.find('.panel').append($template);
                            $row.find('.accordion')[0].addEventListener("click", function () {
                                this.classList.toggle("active");
                                var panel = this.nextElementSibling;
                                if (panel.style.maxHeight === "2000px") {
                                    panel.style.maxHeight = "0px";
                                } else {
                                    panel.style.maxHeight = "2000px";
                                }
                            });

                            // se nel modal degli indirizzi sono presenti elementi (.address-item length )
                            if ($('div[data-remodal-id="modal-address"]').find('.address-item').length) {
                                // appendi l'elemento "$row" all'ultimo degli indirizzi inseriti
                                $row.insertAfter($('div[data-remodal-id="modal-address"]').find('.address-item:last'));
                            }
                            // se nel modal degli indirizzi non sono presenti elementi
                            else {
                                // appendi l'elemento "$row" al pannello di inserimento di un nuovo indirizzo
                                $row.insertAfter($('div[data-remodal-id="modal-address"]').find('.panel'));
                            }

                            // seleziono il blocco degli indirizzi
                            var addressList = $('#billing-address');
                            // se trovo il testo "nessun indirizzo"
                            if (addressList.find('.empty')) {
                                console.log('elimino la dicitura nessun indirizzo');
                                // elimino la dicitura
                                addressList.find('.empty').remove();
                            }
                            // aggiorna la lista degli indirizzi a fondo pagina
                            console.log('appendo nuovo indirizzo');
                            addressList.append('<p id="' + response.address.id + '">' + response.address.label + '</p>')

                        }
                        // se modifica vecchio indirizzo...
                        else {
                            console.log('modifica vecchio indirizzo');

                            var $form = $('#addressForm' + response.address.id);
                            $form.parents('.accordion').toggle("active");
                            $form.parents('.panel').css("maxHeight", "0");
                            $form.parents('.address-item').find('.address-text').text(response.address.label);
                            var $select = $('.form-validate').find('select.address-select');
                            $select.find('option[value="' + response.address.id + '"]').text(response.address.label)
                        }
                    }
                }
            }); // END - Ajax
        }
    }

    // Accordion
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight === "2000px") {
                panel.style.maxHeight = "0px";
            } else {
                panel.style.maxHeight = "2000px";
            }
        });
    }

    $('form[data-kind="cards-editing"]').each(function () {
        var $cardNumber = $(this).find('input[name=card_number]');
        var $cardType = $(this).find('input[name=card_type]');
        $cardNumber.payform('formatCardNumber');
        $cardNumber.keyup(function () {
            var cardType = $.payform.parseCardType($cardNumber.val());
            if (cardType && $.inArray(cardType, ['visa', 'mastercard', 'amex', 'visaelectron', 'maestro']) > -1) {
                $cardType.val(cardType.charAt(0).toUpperCase() + cardType.slice(1));
            } else {
                $cardType.val('')
            }
        });
        $(this).find('input[name=card_cvv]').payform('formatCardCVC');
    });

    jQuery.validator.addMethod("CCExp", function (value, element, params) {
        var minMonth = new Date().getMonth() + 1;
        var minYear = new Date().getFullYear();

        var formMonth = $(element).parents('form').find('select[name="card_exp_month"]');
        var formYear = $(element).parents('form').find('select[name="card_exp_year"]');

        var month = parseInt(formMonth.val(), 10);
        var year = parseInt(formYear.val(), 10);

        if ((year > minYear) || ((year === minYear) && (month >= minMonth))) {
            return true;
        } else {
            return false;
        }
    }, "Your Credit Card Expiration date is invalid.");

    jQuery.validator.addClassRules('address-autocomplete', {
        required: true,
        address: true
    });

    jQuery.validator.addClassRules('card-number', {
        required: true,
        creditcard: true
    });

    jQuery.validator.addMethod("selectCheck", function (value, element) {
        return value !== '';
    });

    jQuery.validator.addMethod("address", function (value, element) {
            var address = $(element).parent().find('.address').val();
            var number = $(element).parent().find('.number').val();
            var city = $(element).parent().find('.city').val();
            var country = $(element).parent().find('.country').val();
            var country_code = $(element).parent().find('.country_code').val();
            var zip = $(element).parent().find('.zip').val();
            if (address !== '' && number !== '' && city !== '' && country !== '' && zip !== '' && country_code !== '') {
                return true;
            } else {
                return false;
            }
        }, "Please enter a valid address."
    );

    jQuery.fn.deleteAddress = function () {
        $(this).click(function (e) {
            var id = $(this).parents('form').data('id');
            var element = $(this).parents('.address-item');
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                url: '/user/ajax/delete/address',
                type: 'POST',
                dataType: 'json',
                data: {id: id},
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                success: function (response) {
                    var $select = $('.form-validate').find('select.address-select');
                    if (response.status === 'success') {

                        // remove this element from page
                        element.remove();

                        var addressList = $('#billing-address');

                        // rimuovo l'indirizzo eliminato dall'elenco degli indirizzi
                        console.log('rimuovo l\'indirizzo eliminato dall\'elenco"');
                        addressList.find('#' + id).remove();

                        // se non è più presente alcun indirizzo
                        if (!addressList.find('p').length) {
                            console.log('nessun indirizzo precedentemente presente, aggiungo la dicitura "nessun indirizzo"');
                            // aggiungo la dicitura nessun indirizzo
                            addressList.append('<p class="empty">No address found. Add new one.</p>');
                        }
                    }
                }
            });
        })
    };

    jQuery.fn.deletePayment = function () {
        $(this).click(function (e) {
            var id = $(this).parents('.card-item').data('id');
            var element = $(this).parents('.card-item');
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                url: '/user/ajax/delete/payment',
                type: 'POST',
                dataType: 'json',
                data: {id: id},
                complete: function (response) {
                    // console.log('Complete!');
                },
                error: function (response) {
                    console.log('Error!');
                },
                success: function (response) {
                    if (response.status === 'success') {
                        // rimuovo la carta dal modal
                        console.log('card removed from modal');
                        element.remove();

                        // seleziono la lista degli indirizzi
                        var cardsList = $('#credit-cards');

                        // ✅️ rimuovo la carta dalla lista
                        console.log('card removed from list');
                        cardsList.find('#card-' + id).remove();

                        // ✅️ se non è più presente alcuna carta
                        if (!cardsList.find('.card-item-account').length) {
                            // aggiungo la dicitura nessuna carta
                            console.log('nessuna carta precedentemente presente, aggiungo la dicitura "nessuna carta"');
                            var $p = $('<p class="empty">No cards found. Add new one.</p>');
                            $p.insertAfter(cardsList.find('p:first'));
                        }

                    }
                }
            });
        })
    };

    $(".add-edit-form").each(function (index, this_form) {
        // ...and take over its submit event.
        $(this_form).validate({
            rules: {
                card_exp_month: {
                    CCExp: true
                },
                card_exp_year: {
                    CCExp: true
                }
            },
            submitHandler: function (form, event) {
                event.preventDefault();
                sendData(event);
            }
        });
    });

    $('body.user-profile').on('change', 'select[name=type]', function () {
        var $container = $(this).closest('form');
        if($(this).val() === 'private'){
            $container.find('.for-company').hide();
            $container.find('.for-company>input').attr("disabled", true);
            $container.find('.for-private').show();
            $container.find('.for-private>input').attr("disabled", false);
        } else{
            $container.find('.for-private').hide();
            $container.find('.for-private>input').attr("disabled", true);
            $container.find('.for-company').show();
            $container.find('.for-company>input').attr("disabled", false);
        }
    });

    $('.delete-address').each(function () {
        $(this).deleteAddress();
    });

    $('.delete-card').each(function () {
        $(this).deletePayment();
    });

    $('.form-validate').validate({
        rules: {
            'shipping-address': {
                required: true,
                selectCheck: true
            },
            'billing-address': {
                required: true,
                selectCheck: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });


}); // END - jQuery window load


