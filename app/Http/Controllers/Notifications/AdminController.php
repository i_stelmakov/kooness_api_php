<?php

namespace App\Http\Controllers\Notifications;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Artwork;
use App\Models\Exhibition;
use App\Models\Gallery;
use App\Models\Post;
use App\Models\Redirect301;
use App\Models\UserOffer;
use App\Services\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('notifications.admin.index');
    }

    public function retrieve(Request $request)
    {
        $columns = array(
            1 => 'id',
            2 => 'content',
            3 => 'referer_to',
            4 => 'created_at',
            5 => 'options',
        );

        $user_id = auth()->user()->id;
        $totalData = Notification::countAll($user_id);
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $notifications = Notification::getAll($user_id, $start, $limit, $order, $dir);
        } else {
            $search = $request->input('search.value');

            $notifications = Notification::getAll($user_id, $start, $limit, $order, $dir, $search);

            $totalFiltered = Notification::countAll($user_id, $search);
        }

        $data = array();
        if (!empty($notifications)) {
            foreach ($notifications as $notification) {
                $edit = null;
                $delete = null;

                $nestedData['id'] = $notification->id;
                $nestedData['content'] = $notification->content;
                $referer = '';
                if($notification->artwork_id){
                    $artwork = Artwork::find($notification->artwork_id);
                    if($artwork){
                        $referer = $artwork->title;
                    }
                }
                if($notification->artist_id){
                    $artist = Artist::find($notification->artist_id);
                    if($artist){
                        $referer = $artist->full_name;
                    }
                }
                if($notification->gallery_id){
                    $gallery = Gallery::find($notification->gallery_id);
                    if($gallery){
                        $referer = $gallery->name;
                    }
                }
                if($notification->exhibition_id){
                    $exhibition = Exhibition::find($notification->exhibition_id);
                    if($exhibition){
                        $referer = $exhibition->name;
                    }
                }
                if($notification->post_id){
                    $post = Post::find($notification->post_id);
                    if($post){
                        $referer = $post->title;
                    }
                }
                if($notification->post_id){
                    $offer = UserOffer::find($notification->user_offers_id);
                    if($offer){
                        $artwork = $offer->artwork()->first();
                        $referer = $artwork->title;
                    }
                }
                $nestedData['referer_to'] = $referer;
                $nestedData['read'] = $notification->pivot->read;
                $nestedData['created_at'] = date('j M Y h:i a', strtotime($notification->created_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => $edit, 'delete' => $delete]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function markAllAsRead(){
        Notification::setRead(auth()->user()->id, Notification::getUnreadIds(auth()->user()->id));

        return redirect()->back();
    }
}
