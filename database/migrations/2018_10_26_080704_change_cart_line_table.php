<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCartLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_lines', function (Blueprint $table) {
            $table->dropColumn('api4dem_referer_id');
            $table->boolean('api4dem_success_send')->after('unit_price')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_lines', function (Blueprint $table) {
            $table->integer('api4dem_referer_id')->after('cart_id')->nullable();
            $table->dropColumn('api4dem_success_send');
        });
    }
}
