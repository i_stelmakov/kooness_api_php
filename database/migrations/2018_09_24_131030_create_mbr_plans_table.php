<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMbrPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('fixed_fee_eur');
            $table->float('fixed_fee_gbp');
            $table->float('fixed_fee_usd');
            $table->float('success_fee');
            $table->boolean('enabled')->default(0);
            $table->timestamps();
        });

        DB::statement("INSERT INTO `mbr_plans` (`id`, `name`, `fixed_fee_eur`, `fixed_fee_gbp`, `fixed_fee_usd`, `success_fee`, `created_at`, `updated_at`)  VALUES 
(NULL, 'Membership 1', 50, 50, 60, 20, '2018-09-26 07:32:27', '2018-09-26 07:32:27'),
(NULL, 'Membership 2', 100, 100, 150, 10, '2018-09-26 07:32:27', '2018-09-26 07:32:27'),
(NULL, 'Membership 3', 200, 200, 250, 5, '2018-09-26 07:32:27', '2018-09-26 07:32:27')
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_plans');
    }
}
