@extends('layouts.admin')
@section('title', 'Admin | Create Artwork Category')
@section('page-header', 'Admin - Create Category')

@section('content')
    @include('artworks.categories.form', ['action' => route('admin.categories.artworks.store'), 'method' => 'POST'])
@endsection