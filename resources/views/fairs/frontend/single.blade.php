@extends('layouts.app')

@section('title', ($fair && $fair->meta_title) ? $fair->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : "Kooness | {$fair->name}"))

@section('body-class', 'fairs-single')

@section('header-og-abstract', ($fair && $fair->meta_description) ? $fair->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($fair && $fair->canonical) ? $fair->canonical : ($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($fair && $fair->meta_keywords) ? $fair->meta_keywords :  ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('header-og-image', ($fair->url_full_image) ? $fair->url_full_image : null)

@section('header-og-url', Request::url())

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>{{ ($fair->h1)? $fair->h1 : ($seo_data && $seo_data->h1 ? $seo_data->h1 : $fair->name) }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Event Sheet !-->
        <section id="event-sheet" class="default-sheet-section">
            <div class="container boxed-container">
                <div class="container col-container">
                    <!-- Event Sheet Col !-->
                    <div id="default-sheet" class="col one-third-col">
                        <!-- Event Info !-->
                        <div id="default-sheet-info">
                            <div class="default-sheet-box">
                                <div class="event-date">
                                    <p>{{ $fair->city }}</p>
                                </div>
                                <div class="default-sheet-row flex-wrap">
                                    <div id="event-adress" class="default-sheet-row">
                                        <h4>From {{ date('d/m/Y', strtotime($fair->start_date)) }} to {{ date('d/m/Y', strtotime($fair->end_date)) }}</h4>
                                    </div>
                                    <div class="default-sheet-row">
                                        <p class="dark-text">
                                            <strong>Location</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row">
                                        <p>{{ $fair->city }}, {{ $fair->address }}<br>{{ $fair->zip }}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Gallery Sheet Social !-->
                            <div id="default-sheet-info" class="default-sheet-box">
                                <div class="default-sheet-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="dark-text">
                                            <strong>Share</strong>
                                        </p>
                                    </div>
                                    <div class="default-sheet-row-cell">
                                        <div class="icon-container">
                                            <a title="facebook" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($fair->description, 0, 140))).'...')->facebook() }}"><i class="fa fa-facebook"></i></a>
                                            <a title="twitter" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($fair->description, 0, 140))).'...')->twitter() }}"><i class="fa fa-twitter"></i></a>
                                            <a title="pinterest" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($fair->description, 0, 140))).'...')->pinterest() }}"><i class="fa fa-pinterest"></i></a>
                                            <a title="tumblr" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($fair->description, 0, 140))).'...')->tumblr() }}"><i class="fa fa-tumblr"></i></a>
                                            <a title="envelope" href="{{ Share::load( Request::url() , strip_tags(preg_replace('/\s+?(\S+)?$/', '', substr($fair->description, 0, 140))).'...')->email() }}"><i class="fa fa-envelope"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Product Sheet Image Col !-->
                    <div id="product-img-col" class="col two-third-col">
                        <!-- Product Sheet Image !-->
                        {{-- <div class="slider-item-img wide-box">
                            <a href="{{ $fair->url_full_image }}" data-lightbox="example-set">
                                <div class="background-cover lazy" data-src="{{ $fair->url_full_image  }}"></div>
                            </a>
                        </div> --}}
                        <div class="slider-item-img">
                            <a href="{{ $fair->url_full_image }}" data-lightbox="example-set">
                                <img data-src="{{ $fair->url_full_image }}" src="" class="lazy"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container boxed-container">
            <hr class="divider"/>
        </div>
        <!-- Description !-->
        <section class="tab-section">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin container-vertical-padding-top container-vertical-padding-bottom">
                    <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                        <ul>
                            @if($fair->description)
                                <li class="current-tab-item tab-button" data-id="1">About the fair</li>
                            @endif
                            @if($fair->news()->whereStatus(1)->count())
                                <li class="tab-button" data-id="2">News</li>
                            @endif
                            @if($fair->tickets)
                                <li class="tab-button" data-id="3">Tickets & Floorplant</li>
                            @endif
                            {{--@if($fair->news()->whereStatus(1)->count())--}}
                                {{--<li class="tab-button" data-id="7">Sections</li>--}}
                            {{--@endif--}}
                            @if($fair->times)
                                <li class="tab-button" data-id="4">Visitor info</li>
                            @endif
                        </ul>
                    </div>
                    <!-- Tab 1 !-->
                    <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">
                                    {!! $fair->description !!}
                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 2 !-->
                    @if($fair->news()->whereStatus(1)->count())
                        <!-- Tab 2 !-->
                        <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                            <section id="featured-news">
                                <div class="container boxed-container">
                                    <div class="default-slider-header">
                                        <span class="h1">Featured News</span>
                                        <p>
                                            <a class="read-more" href="{{ route('posts.archive.all', ["news"]) }}">All featured news</a>
                                        </p>
                                    </div>
                                    <div class="container col-container-with-offset">
                                        @foreach($fair->news()->whereStatus(1)->limit(3)->get() as $news)
                                            <div class="slider-item one-third-col-with-margin">
                                                <div class="slider-item-img wide-box">
                                                    <a title="{{ $news->title }}" href="{{ route('posts.single', ["news", $news->slug]) }}">
                                                        <div class="background-cover gallery lazy" data-src="{{ $news->url_wide_box }}"></div>
                                                    </a>
                                                </div>
                                                <div class="featured-news-item-txt">
                                                    <p class="news-date">{{  date('d F Y', strtotime($news->date)) }}</p>
                                                    <div class="default-sheet-row">
                                                        <h2>
                                                            <a class="dark-text" href="{{ route('posts.single', ["news", $news->slug]) }}">{{ $news->title }}</a>
                                                        </h2>
                                                    </div>
                                                    <div class="default-sheet-row-cell">
                                                        <p class="dark-text news-author">By <a>{{ $news->author }}</a>
                                                        <p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endif
                    <!-- Tab 3 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="3">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner">

                                    <h2>Tickets</h2>
                                    {!! $fair->tickets !!}

                                    <h2>Floorplant</h2>
                                    <a style="border: 1px solid #444" href="{!! $fair->floorplant_pdf_url !!}" target="_blank">
                                        <img data-src="{!! $fair->floorplant_preview_url !!}" src="" class="lazy">
                                    </a>

                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​
                    </div>
                    <!-- Tab 7 !-->
                    <div class="tab-container four-fifths-col-with-margin tab" data-id="4">
                        <div class="show-more-text-container">
                            <div class="show-more-content">
                                <div class="show-more-content-inner" id="test">

                                    @if($fair->times)
                                        <?php
                                        $times = json_decode($fair->times);
                                        ?>
                                        @if($times)
                                            <h2>Opening times</h2>
                                            @foreach($times as $day=>$time)
                                                <div>
                                                    <span>{{ date('l', strtotime(str_replace('/', '-', $time->day)))}}, {{ $time->day  }}</span>&emsp;
                                                    <span>{{ $time->from }} - {{ $time->to }}</span>&emsp;
                                                    @if($time->note)
                                                        <span>{{ $time->note }}</span>
                                                    @endif
                                                </div>
                                            @endforeach
                                        @endif
                                    @endif
                                    @if($fair->contact_name)
                                        <h2>Contact info</h2>
                                        <div class="default-sheet-row">
                                            <p>{{ $fair->contact_name }} <br>
                                                {{ $fair->contact_address }}<br>
                                                {{ $fair->contact_city }}, {{ $fair->contact_zip }}<br>
                                                {{ $fair->contactCountry()->first()->name }}<br>
                                                @if($fair->contact_email)
                                                    <a href="mailto:{{ $fair->contact_email }}">{{ $fair->contact_email }}</a><br>
                                                @endif
                                                @if($fair->contact_website)
                                                    <a href="{{ $fair->contact_website }}">{{ $fair->contact_website }}</a>
                                                @endif
                                            </p>
                                        </div>
                                    @endif

                                </div>
                            </div>
                            <div class="show-more-button">
                                <a href="#">Show more</a>
                            </div>
                        </div>​

                    </div>
                </div>
            </div>
        </section>

        {{-- Browse by title section --}}
        <div class="container boxed-container">
            <hr class="divider"/>
            <div class="inner-title-container">
                <span class="inner-title-alternative">Browse <span class="main-color-txt">by</span></span>
            </div>
        </div>

        {{-- Featured Artworks --}}
        <section id="product-slider" class="default-slider-section slider-border">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin">
                    <div class="one-fourth-col-with-margin slider-title-box">
                        <div class="slider-title-box-txt">
                            <span class="h1">Featured<span class="main-color-txt slider-title-span">Artworks</span></span>
                            <a class="black-button" href="{{ route('artworks.archive.all', ["fair" => $fair->slug]) }}">View all</a>
                        </div>
                        <div class="slider-title-drop-caps">
                            <h6 style="background-image: url(/images/opera-3.jpg)">k</h6>
                        </div>
                    </div>
                    @foreach($artworks as $artwork)
                        <div class="slider-item one-fourth-col-with-margin half-width-mobile">

                            @if(Auth::user())
                                <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                    @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                        <i class="fa fa-heart"></i>
                                    @else
                                        <i class="fa fa-heart-o"></i>
                                    @endif
                                </div>
                            @endif

                            <div class="slider-item-img square-box">
                                <a href="{{ route('artworks.single', [$artwork->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $artwork->main_image->url }}"></div>
                                </a>
                            </div>
                            <div class="slider-item-txt">
                                <h2>
                                    <a class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a>
                                </h2>
                                <p class="artwork-measures">{{ $artwork->measure_cm }} cm</p>
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="artwork-type">
                                            @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $ArtworkMedium)
                                                <a title="{{ $ArtworkMedium->name }}" href="{{ route('artworks.single', [$ArtworkMedium->slug]) }}">{{ $ArtworkMedium->name }}</a>
                                                <br>
                                            @endforeach
                                        </p>
                                    </div>
                                    {{-- SE artwork non in fiera OPPURE artwork impone visualizzazione prezzo --}}
                                    @if(!$artwork->available_in_fair || $artwork->show_price)
                                        <div class="default-sheet-row-cell">
                                            {{-- SE artwork segnato come disponibile E artwork non è in nessuna fiera --}}
                                            @if( $artwork->status == 1 && !$artwork->available_in_fair)
                                                <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                                            @else
                                                <p class="dark-text artwork-price">SOLD OUT</p>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </section>
        
        {{-- Featured Galleries --}}
        <section id="gallery-slider" class="default-slider-section slider-border">
            <div class="container boxed-container">
                <div class="default-slider-header">
                    <span class="h1">Featured galleries</span>
                    <p>
                        <a class="read-more" href="{{ route('galleries.archive.all', ["fair" => $fair->slug]) }}">All featured galleries</a>
                    </p>
                </div>
                <div class="container col-container-with-offset-and-margin">
                    @foreach($galleries as $gallery)
                        <div class="slider-item one-third-col-with-margin half-width-mobile">
                            <i title="Remove" class="fa fa-times delete-item"> </i>
                            @if(Auth::user())
                                <div class="add-item follow {{ (Auth::user()->collectionGalleries->contains($gallery->id)) ? 'active' : '' }}" data-section="galleries" data-id="{{$gallery->id}}">
                                    @if(Auth::user()->collectionGalleries->contains($gallery->id))
                                        <i class="fa fa-heart"></i>
                                    @else
                                        <i class="fa fa-heart-o"></i>
                                    @endif
                                </div>
                            @endif
                            <div class="slider-item-img wide-box">
                                <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $gallery->url_wide_box }}"></div>
                                </a>
                            </div>
                            <div class="slider-item-txt">
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <h2>
                                            <a title="{{ $gallery->name }}" class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                        </h2>
                                    </div>
                                </div>
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <p class="gallery-adress">{{ $gallery->city }}, {{ $gallery->address }}</p>
                                    </div>
                                </div>
                                <a title="{{ $gallery->name }}" class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">read more</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

       {{-- Featured Artists --}}
        <section id="artist-slider" class="default-slider-section slider-border">
            <div class="container boxed-container">
                <div class="container col-container-with-offset-and-margin">
                    <div class="one-fourth-col-with-margin slider-title-box">
                        <div class="slider-title-box-txt">
                            <span class="h1">Featured<span class="main-color-txt slider-title-span">Artists</span></span>
                            <a class="black-button" href="{{ route('artists.archive.all', ["fair" => $fair->slug]) }}">View all</a>
                        </div>
                        <div class="slider-title-drop-caps">
                            <h6 style="background-image: url(/images/opera-3.jpg)">k</h6>
                        </div>
                    </div>
                    @foreach($artists as $artist)
                        <div class="slider-item one-fourth-col-with-margin half-width-mobile">
                            <i title="Remove" class="fa fa-times delete-item"></i>
                            @if($artist->slug)
                                @if(Auth::user())
                                    <div class="add-item follow {{ (Auth::user()->collectionArtists->contains($artist->id)) ? 'active' : '' }}" data-section="artists" data-id="{{$artist->id}}">
                                        @if(Auth::user()->collectionArtists->contains($artist->id))
                                            <i class="fa fa-heart"></i>
                                        @else
                                            <i class="fa fa-heart-o"></i>
                                        @endif
                                    </div>
                                @endif
                            @endif
                            <div class="slider-item-img square-box">
                                <a title="{{ $artist->first_name . " " . $artist->last_name }}" href="{{  route('artists.single', [$artist->slug]) }}">
                                    <div class="background-cover gallery lazy" data-src="{{ $artist->url_square_box }}"></div>
                                </a>
                            </div>
                            <div class="slider-item-txt">
                                <div class="default-sheet-row slider-item-row">
                                    <div class="default-sheet-row-cell">
                                        <h2>
                                            <a title="{{ $artist->first_name }} {{ $artist->last_name }}" class="dark-text" href="{{ route('artists.single', [$artist->slug]) }}">{{ $artist->first_name }} {{ $artist->last_name }}</a>
                                        </h2>
                                    </div>
                                </div>
                                @if($artist->countryBirth()->count())
                                    <p class="dark-text provenance">{{ $artist->countryBirth()->first()->name }}</p>
                                @endif
                                <p class="exposure">{{ $artist->artworks()->count() }} Works exhibited</p>
                                <a class="read-more" href="{{ route('artists.single', [$artist->slug]) }}">View artist page</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

    </div>
@endsection