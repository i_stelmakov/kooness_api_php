<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageColumnToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists_categories', function (Blueprint $table) {
            $table->string('url_cover')->after('slug')->nullable();
        });

        Schema::table('artworks_categories', function (Blueprint $table) {
            $table->string('url_cover')->after('slug')->nullable();
        });

        Schema::table('galleries_categories', function (Blueprint $table) {
            $table->string('url_cover')->after('slug')->nullable();
        });

        Schema::table('posts_categories', function (Blueprint $table) {
            $table->string('url_cover')->after('slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
