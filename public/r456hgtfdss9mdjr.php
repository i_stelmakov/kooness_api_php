<?php
/*******EDIT LINES 3-8*******/
$DB_Server = "kooness-mysql.cypvlecd01pf.eu-west-1.rds.amazonaws.com"; //MySQL Server
$DB_Username = "kooness_prod"; //MySQL Username
$DB_Password = "Jyf8XQsu935CsPys";             //MySQL Password
$DB_DBName = "www_kooness_com";         //MySQL Database Name
$DB_TBLName = "artists"; //MySQL Table Name
$filename = "artist";         //File Name
/*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/
//create MySQL connection
$sql = "SELECT * FROM $DB_TBLName";
$dbh_prod = new PDO("mysql:host={$DB_Server};dbname={$DB_DBName}", $DB_Username, $DB_Password);
//select database
$stmt = $dbh_prod->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll();

$file_ending = "xls";
//header info for browser
//header("Content-Type: application/xls");
//header("Content-Disposition: attachment; filename=$filename.xls");
//header("Pragma: no-cache");
//header("Expires: 0");
/*******Start of Formatting for Excel*******/

echo '<html>';
echo '<body>';
echo '<table border="1">';
//make the column headers what you want in whatever order you want
echo '<tr>
        <th>id</th>
        <th>first_name</th>
        <th>last_name</th>
        <th>slug</th>
        <th>website</th>
        <th>url_full_image</th>
        <th>url_square_box</th>
        <th>url_wide_box</th>
        <th>url_vertical_box</th>
        <th>url_cover</th>
        <th>about_the_artist</th>
        <th>meet_the_artist</th>
        <th>bibliography</th>
        <th>awards</th>
        <th>curriculum_vitae</th>
        <th>date_of_birth</th>
        <th>date_of_death</th>
        <th>city_of_birth</th>
        <th>country_of_birth</th>
        <th>current_location</th>
        <th>featured</th>
        <th>status</th>
        <th>user_id</th>
        <th>created_by</th>
        <th>create_from_exhibition</th>
        <th>meta_title</th>
        <th>h1</th>
        <th>canonical</th>
        <th>meta_keywords</th>
        <th>meta_description</th>
        <th>created_at</th>
        <th>updated_at</th>
       </tr>';
//loop the query data to the table in same order as the headers
foreach ($result as $item) {
    echo '<tr>';
    echo '<td>';
    echo $item['id'];
    echo '</td>';
    echo '<td>';
    echo $item['first_name'];
    echo '</td>';
    echo '<td>';
    echo $item['last_name'];
    echo '</td>';
    echo '<td>';
    echo $item['slug'];
    echo '</td>';
    echo '<td>';
    echo $item['website'];
    echo '</td>';
    echo '<td>';
    echo $item['url_full_image'];
    echo '</td>';
    echo '<td>';
    echo $item['url_square_box'];
    echo '</td>';
    echo '<td>';
    echo $item['url_wide_box'];
    echo '</td>';
    echo '<td>';
    echo $item['url_vertical_box'];
    echo '</td>';
    echo '<td>';
    echo $item['url_cover'];
    echo '</td>';
    echo '<td>';
    echo $item['about_the_artist'];
    echo '</td>';
    echo '<td>';
    echo $item['meet_the_artist'];
    echo '</td>';
    echo '<td>';
    echo $item['bibliography'];
    echo '</td>';
    echo '<td>';
    echo $item['awards'];
    echo '</td>';
    echo '<td>';
    echo $item['curriculum_vitae'];
    echo '</td>';
    echo '<td>';
    echo $item['date_of_birth'];
    echo '</td>';
    echo '<td>';
    echo $item['date_of_death'];
    echo '</td>';
    echo '<td>';
    echo $item['city_of_birth'];
    echo '</td>';
    echo '<td>';
    echo $item['country_of_birth'];
    echo '</td>';
    echo '<td>';
    echo $item['current_location'];
    echo '</td>';
    echo '<td>';
    echo $item['featured'];
    echo '</td>';
    echo '<td>';
    echo $item['status'];
    echo '</td>';
    echo '<td>';
    echo $item['user_id'];
    echo '</td>';
    echo '<td>';
    echo $item['created_by'];
    echo '</td>';
    echo '<td>';
    echo $item['create_from_exhibition'];
    echo '</td>';
    echo '<td>';
    echo $item['meta_title'];
    echo '</td>';
    echo '<td>';
    echo $item['h1'];
    echo '</td>';
    echo '<td>';
    echo $item['canonical'];
    echo '</td>';
    echo '<td>';
    echo $item['meta_keywords'];
    echo '</td>';
    echo '<td>';
    echo $item['meta_description'];
    echo '</td>';
    echo '<td>';
    echo $item['created_at'];
    echo '</td>';
    echo '<td>';
    echo $item['updated_at'];
    echo '</td>';
    echo '</tr>';
}
echo '</table>';
echo '</body>';
echo '</html>';

?>