#### Importante!!!
#su server non lanciare gst ma:
git --git-dir=../.git status

#### Checkout Manuale
```console
da dentro public_html (cd /var/www/www.kooness.com/public_html)
git --git-dir=../.git fetch origin --verbose
git --git-dir=../.git rebase
OPZIONALE composer install #in caso di variazioni alle dipendenze
OPZIONALE php artisan migrate #in caso di variazioni al DB
OPZIONALE composer dump-autoload #in caso di aggiunta di classi PHP
OPZIONALE npm run prod
OPZIONALE sudo touch /var/cache/mod_pagespeed/cache.flush # !!!SOLO se PROD
```

#### 4dem Batch
```console
da dentro public_html (cd /var/www/www.kooness.com/public_html)
screen #per non fare cadere la connessione durante il batch
php artisan api:4dem:batch:products # Batch command for load all products
php artisan api:4dem:batch:users #Batch command for load all users
```


# Entire install procedure
```console
git clone https://David-Notomia@bitbucket.org/smadevelopers/kooness-v2.git
docker-compose up -d
.env.example in .env e correggere i parametri
docker exec -it kooness-docker_php_1 bash
php composer.phar install
npn install
npm run dev
php artisan migrate
php artisan db:seed
```

#Test sulle memberships
```console
php artisan membership:renewal
```

# Git work multibranch per Theming / Dev / QA
```console
1 - theming -> commit
2 - theming -> push
3 - qa -> checkout (checkout AS al primo giro)
4 - qa -> pull
6 - theming -> merge into current
6 - qa -> push
7 - theming -> checkout (checkout AS al primo giro)
Riassunto: Qa merg into theming (contenitore dove metterle, mergiare la branch dalla quale arrivano le m)
```

# Dopo modifiche alla PRO
```console
posizionarsi in
/src
lanciare il comando
php artisan cache:forget spatie.permission.cache
```


# 1 - Rollback ultima Migrazione tabelle
```console
php artisan migrate:rollback
```

# 2 - Migrazione tabelle
```console
php artisan migrate
```

# Dopo GIT sync con DEV
```console
composer install
artisan migrate
```

# Password
```console
utenti prova => TestPsw-1234
```

# Full install procedure
```console
git clone https://David-Notomia@bitbucket.org/smadevelopers/kooness-v2.git
docker-compose up -d
.env.example in .env e correggere i parametri
docker exec -it kooness-docker_php_1 bash
php composer.phar install
npn install
npm run dev
php artisan migrate
php artisan db:seed
```

# Git work multibranch per Theming / Dev
```console
1 - theming -> commit
2 - theming -> push (*opzionale)
3 - dev -> checkout (checkout AS al primo giro)
4 - dev -> pull
5 - theming -> checkout
6 - dev -> merge into current
6.b - theming -> push (se è stata effettuata una merge)
```

# Git work multibranch per Theming / QA
```console
1 - theming -> commit
2 - theming -> push
3 - qa -> checkout (checkout AS al primo giro)
4 - qa -> pull
6 - theming -> merge into current
6 - qa -> push
7 - theming -> checkout (checkout AS al primo giro)
Riassunto: Qa merg into theming (contenitore dove metterle, mergiare la branch dalla quale arrivano le m)
```

# Dopo modifiche alla PRO
```console
posizionarsi in
/src
lanciare il comando
php artisan cache:forget spatie.permission.cache
```

# Per portare poi le variazioni da QA/DEV/Master dentro theming
```console
1 - QA / DEV / Master merge into current
```

# Per lanciare recupero git e npm run dev + install vari dalla QA
```console
Andare nella cartella => cd /var/www/qa.kooness.com
dentro di essa risiederà lo script
Lanciare comando => ./checkout.sh
```

# Sequenza operazioni compiute da .checkout
```console
/.checkout
	GIT pull
	composer install
	YES - NO / CTRL+C
		php artisan migrate
	        reset cache (spatie permission)
	        npm install
	        npm run dev
se si esegue CTRL+C vengono esclusi gli ultimi 3 passaggi
```

# in caso di conflitti SULLA MASTER per checkout (il server restituisce 'cannot rebase')
```console
cd www.kooness.com/
ll
./checkout.sh
cd public_html/
git --git-dir=../.git status
git --git-dir=../.git reset origin/master --hard
git --git-dir=../.git status
```

# in caso di conflitti SULLA QA per checkout (il server restituisce 'cannot rebase')
```console
cd www.kooness.com/
ll
./checkout.sh
cd public_html/
git --git-dir=../.git status
git --git-dir=../.git reset origin/qa --hard
git --git-dir=../.git status
```

# Comandi post Git per allineamento
```console
#comandi da eseguire da SRC:
composer install
npm install

#comandi da eseguire da MACCHINA:
docker exec -it kooness-docker_php_1 bash
php composer.phar install
php artisan migrate (edited)
```

# Git stash mods (From IDE)
```console
1 - vcs -> stash changes
2 - dev -> checkout
3 - dev -> pull
4 - theming -> checkout
5 - dev -> merge into current
6 - theming -> push
7 - vcs -> unstash changes -> apply stash
```

# Avvia la checkout di Git sul server
#### posizionarsi sul percorso
```console
/var/www/qa.kooness.com
```

#### Checkout Automatica
```console
./checkout.sh
```

## nel caso desse errore 'cannot rebase' lanciare il comando
```console
cd public_html/
git --git-dir=../.git reset origin/qa --hard
```

#### deve restituire il messaggio di successo:
First, rewinding head to replay your work on top of it...
Fast-forwarded qa to refs/remotes/origin/qa.

# Avvia npm sul server
#### posizionarsi sul percorso
```console
/var/www/qa.kooness.com/public/html
```
#### lanciare
```console
npm install
```

# Git utils
```console
## Creare un nuovo branch
gst
git pull
#### 1 - crea nuovo branch in locale
git checkout -b theming
#### 2 - committa sulla nuova branch
git add-commit -m "added fairs sections"
#### 3 - pusha sulla nuova branch
git push

## Creare un nuovo branch
#### aggiorna le branch locali con le remote (verifica se sono stati creati nuovi branch)
git fetch
#### fa il checkout in locale della nuova branch indicata
git checkout theming
#### lista branch
git branch -l

## 👍 Unire due branch
git cheeckout dev
git pull
git checkout theming
git merge dev
:q
git push
```

# Operate with containers
```console
cd /kooness-docker/src
git clone https://David-Notomia@bitbucket.org/smadevelopers/kooness-v2.git
git checkout origin-dev
docker-compose up -d
```

### Install Laravel components
```console
docker exec -it kooness-docker_php_1 bash
php composer.phar install
docker ls
docker ps
cd public
bower install
```

# Production scripts
```console
npm update -g
npm install
npm run dev
npm run production
npm run watch
```

#### flush della cache di Google Page Speed
```console
sudo touch /var/cache/mod_pagespeed/cache.flush
RICARICA UNA QUALSIASI PAGINA!!!
sudo rm /var/cache/mod_pagespeed/cache.flush
```

#### per eliminare traccia modifiche da shell
```console
uscire das qualsiasi shell connessa
rimuovere file '.bash_history' dalla cartella dell'utenza => '/home/ec2-user/.bash_history'
```

### Structure

#### Routes
routes/web.php
#### Breadcumbs
routes/breadcrumbs.php

#### Backend Page Layout
resources/views/layouts/admin.blade.php
#### Backend Sidebar
resources/views/layouts/includes/sidebar.blade.php

#### Fair Create
resources/views/fairs/admin/create.blade.php
#### Fair Form
resources/views/fairs/admin/form.blade.php
#### Fair Controller
app/Http/Controllers/Fairs/AdminController.php
#### Fair Page Layout
resources/views/fairs/admin/index.blade.php

#### Backend Sass Variables
resources/assets/sass/_variables.scss
#### Backend Sass Style
resources/assets/sass/app.scss

#### Frontend Head
resources/views/layouts/includes/head.blade.php
#### Frontend Sass Variables
resources/assets/admin/_variables.scss
#### Frontend Sass Style
resources/assets/admin/app.scss

#### Production
webpack.mix.js


# Laravel

## Creare una nuova pagina, elementi da settare
routes/web.php
app/Http/Controllers/Artists/FrontendController.php
routes/breadcrumbs.php
resources/views/layouts/app.blade.php
resources/views/layouts/includes/head.blade.php
resources/views/artworks/frontend/index.blade.php
resources/views/layouts/includes/footer.blade.php

## Laravel refresh cache
php composer.phar dump-autoload
