
$(window).on("load", function () {
    /* ==========================================================================
    Generics
    ========================================================================== */
    console.log('collections JS');

    // Variables
    var $ = jQuery;
    var inst = $('[data-remodal-id=modal-my-gallery]').remodal();

    // Loader reveal
    function loadedModalContent() {
        document.getElementById("loader-modal").style.display = "none";
        document.getElementById("loader-modal-content").classList.add('content-loaded');
    }
    function unloadModalContent() {
        document.getElementById("loader-modal").style.display = "block";
        document.getElementById("loader-modal-content").classList.remove('content-loaded');
    }

    var openGalleryModal = function () {
        var id = $(this).data('modal-id');
        var $form = $('.collect-form');
        unloadModalContent();

        inst.open();
        $.ajax({
            url: '/user/ajax/get/collection',
            data: {'collection_id': id},
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    var $input = $form.find('input[name=collection-name]');
                    $input.val(response.collection.name);
                    $form.find('.save-collection-name').unbind('click').click(function () {
                        if ($input.val() === '') {
                            $input.css("border-color", "red")
                        } else {
                            $input.css("border-color", "var(--main-border-color)");
                            $.ajax({
                                url: '/user/ajax/update/collection',
                                data: {name: $input.val(), 'collection_id': id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (response) {
                                    if (response.status === 'success') {
                                        $('.slider-item[data-collection-id=' + id + ']').find('.my-gallery-title').text($input.val());
                                        $input.css("border-color", "lightgreen");
                                        setTimeout(function () {
                                            $input.css("border-color", "var(--main-border-color)");
                                        }, 1000)
                                    }
                                }
                            });
                        }
                    });
                    $form.find('.gallery-items-list').empty();
                    $.each(response.collection.artworks, function (index, value) {
                        var $item = $('<div class="gallery-item"></div>');
                        var $img = $('<span class="gallery-item-image" style="background-image: url(' + value.img + ')"></span>');
                        var $title = $('<span class="gallery-item-title">' + value.name + '</span> <span class="horizontal-space-md"></span>');
                        var $delete = $('<a class="remove-from-collection"><i class="fa fa-close"></i> Delete</a><hr>');

                        $delete.on('click', function () {
                            $.ajax({
                                url: '/user/ajax/remove/from/collection',
                                data: {'collection_id': id, 'id': value.id},
                                type: 'POST',
                                dataType: 'json',
                                success: function (response) {
                                    if (response.status === 'success') {
                                        $item.remove();
                                        var $itemToRemove = $('.gallery-container').find('.slider-item-img[data-referer="' + response.referer + '"]');
                                        console.log($itemToRemove)
                                        if ($itemToRemove.length) {
                                            if (!$itemToRemove.hasClass('one-fourth-col')) {
                                                var $newMain = $itemToRemove.parents('.slider-item').find('.slider-item-img.one-fourth-col:first');
                                                $newMain.detach();
                                                $newMain.removeClass('one-fourth-col');
                                                $itemToRemove.parent('.slider-item').prepend($newMain);
                                            }
                                            $itemToRemove.remove();
                                        }
                                    }
                                }
                            });
                        });

                        $item.append($img);
                        $item.append($title);
                        $item.append($delete);

                        $form.find('.gallery-items-list').append($item);

                    })
                }
                loadedModalContent();

            }
        });
    };

    $('.modal-my-gallery-trigger').each(function(i, obj) {
        obj.addEventListener("click", openGalleryModal, false);
    });

    $('.delete-item').each(function () {
        $(this).on('click', function () {
            hidePage();
            var $element = $(this);
            $.ajax({
                url: '/user/ajax/remove/from/collection',
                data: {'id': $element.data('id'), 'section': $element.data('section')},
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        window.location.reload(true)
                    }
                }
            });
        });
    });

    $('.remove-collection').each(function () {
        $(this).on('click', function () {
            hidePage();
            var $element = $(this);
            $.ajax({
                url: '/user/ajax/remove/collection',
                data: {'id': $element.data('collection-id')},
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    if (response.status === 'success') {
                        window.location.reload(true)
                    }
                }
            });
        });
    });


}); // END - jQuery window load


