@extends('layouts.admin')
@section('title', 'Admin | Current invoices number')
@section('page-header', 'Current invoices number')

@section('content')
    <div class="row">
        <div class='col-md-12 col-xs-12'>
            <form role="form" method="POST" action="{{ route('admin.orders.current.invoices.number') }}" data-attachment="validate" data-id="" data-referer="">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>The fields marked with asterisk (*) are required </p>
                    </div>
                </div>
                <br>
                <div class="box box-primary box-solid">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="current_number">Current Number (*)</label>
                                <input id="current_number" class="form-control" type="number" placeholder="Current number" name="current_number" value="{{ $data->{'current_number'} }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Form Footer & Submit -->
                <div class="box box-primary box-solid">
                    <div class="box-footer">
                        <div class="col-md-12">
                            <div class="pull-right ">
                                <input class="btn btn-primary" type="submit" value="Save"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection