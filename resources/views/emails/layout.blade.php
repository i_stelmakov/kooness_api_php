<!DOCTYPE html>
<html lang="it-IT">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Kooness</title>
</head>
<style>
	.style-map {
		--main-color: #48b6ac;
		--main-navbar-bg: #101010;
		--glut-nav-link-color: #fff;
		--glut-nav-link-color-hover: #c2a661;
		--title-color: #222;
		--p-color: #888;
		--secondary-link-color: #c2a661;
		--form-input-bg: #eee;
		--main-border-color: #e5e5e5;
		--font-family: "PT Serif", serif
		--font-family: "Open Sans", serif
		--font-family: "Libre Baskerville", serif
	}
</style>
<!-- Style Map
-->
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper" dir="ltr" style="background-color: #101010; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
		<tr>
			<td align="center" valign="top">
				<div id="template_header_image"></div>
				<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="background-color: #ffffff; border: 1px solid #101010; border-radius: 3px !important;">
					<tr>
						<td align="center" valign="top">
							<!-- Header -->
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style='background-color: #48b6ac; border-radius: 3px 3px 0 0 !important; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;'>
								<tr>
									<td id="header_wrapper" style="padding: 36px 48px; display: block;">
										<div id="big-logo-container">
											<img src="{{url('images/logo-white.png')}}">
										</div><br>
										<h1 style='color: #ffffff; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left;'>@yield('title')</h1>
									</td>
								</tr>
							</table>
							<!-- End Header -->
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">

						{{--CONTENT--}}
						@yield('content')

						{{--FOOTER--}}
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
							<!-- Footer -->
							<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
								<tr>
									<td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
										<table border="0" cellpadding="10" cellspacing="0" width="100%">
											<tr>
												<td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #48b6ac; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
													<p>
														Kooness<br>
														Via Cesare Battisti 49 - 46043 Castiglione delle siviere (MN)<br>
														VAT number 02439970209<br>
														<a href="mailto:info@kooness.com" style="text-decoration: none; color: #7c7c7c">info@kooness.com</a>
													</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<!-- End Footer -->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>