@extends('layouts.admin')
@if(Request::is('admin/magazine*'))
    @section('title', 'Admin | Create Post')
    @section('page-header', 'Admin - Create Post')
@else
    @section('title', 'Admin | Create News')
    @section('page-header', 'Admin - Create News')
@endif

@section('content')
    @include('posts.admin.form', ['action' => (Request::is('admin/magazine*')) ? route('admin.magazine.store'): route('admin.news.store'), 'method' => 'POST'])
@endsection