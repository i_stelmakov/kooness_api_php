@extends('layouts.admin')
@section('title', 'Admin | Create Artwork Medium - Category')
@section('page-header', 'Admin - Create Medium Category Relation')

@section('content')
    @include('artworks.medium-categories.form', ['action' => route('admin.artworks.medium.categories.store'), 'method' => 'POST'])
@endsection