<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworksImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artworks_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artwork_id')->unsigned();
            $table->string('url');
            $table->string('label')->nullable();
            $table->timestamps();

            $table->foreign('artwork_id')
                ->references('id')
                ->on('artworks')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artworks_images');
    }
}
