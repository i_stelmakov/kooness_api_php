@extends('layouts.admin')
@if(Request::is('admin/magazine*'))
    @section('title', 'Magazine Post')
    @section('page-header', 'Magazine Post')
@else
    @section('title', 'News')
    @section('page-header', 'News')
@endif

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                @if(!roleCheck('seo'))
                    <div class="box-header">
                        <div class="pull-right">
                            @if(Request::is('admin/magazine*'))
                                <a href="{{ route('admin.magazine.create') }}">
                                    <button class="btn btn-primary">Add new post</button>
                                </a>
                            @else
                                <a href="{{ route('admin.news.create') }}">
                                    <button class="btn btn-primary">Add news</button>
                                </a>
                            @endif
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <table id="table-posts" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Author</th>
                            <th>Visible</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table-posts').DataTable({
                "scrollX": true,
                "processing": true,
                "stateSave": true,
                "serverSide": true,
                "order": [[ 5, "desc" ]],
                "ajax": {
                    "url": "{{ (Request::is('admin/magazine*'))?  route('admin.magazine.retrieve') : route('admin.news.retrieve') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "title"},
                    {"data": "slug"},
                    {"data": "author"},
                    {"data": "status", "bSortable": false},
                    {"data": "created_at"},
                    {"data": "options", "bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                        // esempio di attivazione di attributi CSS personalizzati per una singola cella 
                        // if ( cellData < 1 ) {
                        //     $(td).css('color', 'red')
                        // }
                    }
                ]

            });
        });
    </script>
@endsection