(function ($) {
    $.imageUploader = function (element, options) {
        let defaults = {
            preview: false,
            cropper: false,
            maxWidth: 2048,
            maxHeight: 2048,
            minWidth: 600,
            minHeight: 200,
            aspectRatio: 1,
            maxFileSize: 2000,
            placeholderImg: '',
            textFileInput: false,
            virtualPath: null,
            fileName: null,
            label: 0
        };

        let plugin = this;


        plugin.settings = {};

        plugin.uuid = Math.random().toString(5).substring(2)
            + (new Date()).getTime().toString(5);

        let $element = $(element);

        plugin.init = function () {
            plugin.settings = $.extend({}, defaults, options);
            plugin.hideInput();
            if (plugin.settings.placeholderImg && plugin.settings.preview) {
                plugin.setPlaceholder();
            }
            if (plugin.settings.textFileInput && !plugin.settings.cropper) {
                plugin.createTextInput();
            } else if (plugin.settings.textFileInput && $(plugin.settings.textFileInput).length) {
                plugin.bindTextInput($(plugin.settings.textFileInput));
            } else {
                $element.on('change', function (e) {
                    let input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    if(!plugin.settings.fileName){
                        plugin.settings.fileName = label;
                    }
                    if (plugin.settings.preview) {
                        plugin.readFile(e.target);
                    }
                });
            }
        };

        plugin.hideInput = function () {
            element.style.opacity = 0;
            element.style.position = 'absolute';
            element.style.top = 0;
            element.style.left = 0;
            element.style.width = '100%';
            element.style.height = '100%';
        };

        plugin.setPlaceholder = function () {
            let $imagePreview = $(plugin.settings.preview);
            $imagePreview.attr('src', plugin.settings.placeholderImg);
        };

        plugin.bindTextInput = function (el) {
            $element.on('change', function (e) {
                let input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                if(!plugin.settings.fileName){
                    plugin.settings.fileName = label;
                }
                input.trigger('fileselect', [label]);
                if (plugin.settings.preview) {
                    plugin.readFile(e.target);
                }
            }).on('fileselect', function (event, label) {
                if (el.length) {
                    el.val(label);
                } else {
                    if (label) alert(label);
                }
            });
        };

        plugin.createTextInput = function () {
            let $input = $('<input type="text" class="form-control" readonly>');
            plugin.bindTextInput($input);
            $element.parents('.input-group').append($input);
        };

        plugin.readFile = function (input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    let $imagePreview = $(plugin.settings.preview);
                    let blob = new Blob([event.target.result]);
                    window.URL = window.URL || window.webkitURL;
                    let blobURL = window.URL.createObjectURL(blob);

                    let image = new Image();
                    image.src = blobURL;
                    image.onload = function () {
                        // have to wait till it's loaded
                        let canvas = plugin.resizeMe(image); // send it to canvas
                        if (!plugin.settings.cropper) {
                            $imagePreview.attr('src', canvas.toDataURL("image/jpeg", 0.8));
                            let $canvas = $('canvas[data-for="' + plugin.uuid + '"]');
                            if ($canvas.length) {
                                $canvas.remove();
                            }
                            $(canvas).attr("data-role", "imageUploader");
                            $(canvas).attr("data-for", plugin.uuid);
                            $(canvas).attr("data-virtualPath", plugin.settings.virtualPath);
                            $(canvas).attr("data-fileName", plugin.settings.fileName);
                            $(canvas).attr("data-label", plugin.settings.label);
                            $(canvas).css("display", "none");
                            $element.parent().append(canvas);
                        } else {
                            plugin.initCropper(canvas.toDataURL("image/jpeg", 0.8));
                        }
                    }
                };
                reader.readAsArrayBuffer(input.files[0]);
            }
        };

        plugin.initCropper = function (stream) {
            let $modalContainer = $('<div />');
            $modalContainer.addClass("modal fade");
            $modalContainer.attr("id", "modalCropper");
            $modalContainer.attr("role", "dialog");
            $modalContainer.attr("tabindex", "-1");

            let $modalDialog = $('<div />');
            $modalDialog.addClass("modal-dialog");

            let $modalContent = $('<div />');
            $modalContent.addClass("modal-content");

            let $modalHeader = $('<div />');
            $modalHeader.addClass("modal-header");

            let $modalTimesClose = $('<button />');
            $modalTimesClose.addClass("close");
            $modalTimesClose.attr("type", "button");
            $modalTimesClose.attr("data-dismiss", "modal");
            $modalTimesClose.attr("aria-label", "Close");
            $modalTimesClose.append('<span aria-hidden="true">&times;</span>');
            $modalHeader.append($modalTimesClose);

            let $modalTitle = $('<h5 />');
            $modalTitle.addClass("modal-title");
            $modalTitle.text("Crop Image");
            $modalHeader.append($modalTitle);

            let $modalBody = $('<div />');
            $modalBody.addClass("modal-body");

            let $img = $('<img />');
            $img.addClass("img-responsive");
            $img.attr("src", stream);

            $modalBody.append($img);

            let $modalFooter = $('<div />');
            $modalFooter.addClass("modal-footer");
            let $buttonClose = $('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $modalFooter.append($buttonClose);
            let $buttonSave = $('<button type="button" class="btn btn-primary">Save crop</button>');
            $buttonSave.on('click', function () {
                let canvas = $img.cropper('getCroppedCanvas');
                let cropping = canvas.toDataURL("image/jpeg", 0.8);
                let $imagePreview = $(plugin.settings.preview);
                $imagePreview.attr('src', cropping);
                let $canvas = $('canvas[data-for="' + plugin.uuid + '"]');
                if ($canvas.length) {
                    $canvas.remove();
                }
                $(canvas).attr("data-role", "imageUploader");
                $(canvas).attr("data-for", plugin.uuid);
                $(canvas).attr("data-virtualPath", plugin.settings.virtualPath);
                $(canvas).attr("data-fileName", plugin.settings.fileName);
                $(canvas).attr("data-label", plugin.settings.label);
                $(canvas).css("display", "none");
                $element.parent().append(canvas);

                $modalContainer.modal('hide');
            });
            $modalFooter.append($buttonSave);

            $modalContent.append($modalHeader);
            $modalContent.append($modalBody);
            $modalContent.append($modalFooter);
            $modalDialog.append($modalContent);
            $modalContainer.append($modalDialog);

            $('body').append($modalContainer);
            let modalIn = $('.modal.fade.in');

            $modalContainer.on('shown.bs.modal\t', function () {
                $img.cropper({
                    aspectRatio: plugin.settings.aspectRatio,
                    minCropBoxWidth: plugin.settings.minWidth,
                    minCropBoxHeight: plugin.settings.minHeight,
                    movable: false,
                    viewMode: 1,
                    rotatable: false,
                    scalable: false,
                    zoomable: false,
                });
            });
            $modalContainer.on('hidden.bs.modal', function () {
                $modalContainer.remove();
                if(modalIn.length > 0) {
                    modalIn.modal('show')
                }
            });

            if(modalIn.length > 0){
                modalIn.on('hidden.bs.modal', function () {
                    $modalContainer.modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    modalIn.unbind('hidden.bs.modal');
                });
                modalIn.modal('hide');
            } else{
                $modalContainer.modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
        };

        plugin.resizeMe = function (img) {
            let canvas = document.createElement('canvas');

            let width = img.width;
            let height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > plugin.settings.maxWidth) {
                    //height *= max_width / width;
                    height = Math.round(height *= plugin.settings.maxWidth / width);
                    width = plugin.settings.maxWidth;
                }
            } else {
                if (height > plugin.settings.maxHeight) {
                    //width *= max_height / height;
                    width = Math.round(width *= plugin.settings.maxHeight / height);
                    height = plugin.settings.maxHeight;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            let ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            // do the actual resized preview

            return canvas// get the data from canvas as 70% JPG (can be also PNG, etc.)

        };
        plugin.init();
    };

    $.fn.imageUploader = function (options) {
        return this.each(function () {
            if (undefined === $(this).data('imageUploader')) {
                let opt = {
                    preview: $(this).data('preview') || false,
                    minWidth: (parseInt($(this).data('minwidth')) || 600),
                    minHeight: (parseInt($(this).data('minheight')) || 200),
                    maxWidth: (parseInt($(this).data('maxwidth')) || 2048),
                    maxHeight: (parseInt($(this).data('maxheight')) || 2048),
                    cropper: (Boolean($(this).data('cropper')) || false),
                    aspectRatio: (parseFloat($(this).data('aspectratio')) || 1),
                    virtualPath: ($(this).data('virtualpath') || null),
                    fileName: ($(this).data('filename') || null),
                    label: ($(this).data('label') || ''),
                };
                if (options) {
                    opt = $.extend({}, opt, options);
                }
                let plugin = new $.imageUploader(this, opt);
                $(this).data('imageUploader', plugin);
            }
        });
    }
})(jQuery);