<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\SocialAccount;
use App\Models\User;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends FrontendExtendedController
{
    public function redirectToProvider($provider)
    {
        if ($provider == 'facebook') {
            return Socialite::driver($provider)->fields([
                'first_name',
                'last_name',
                'email'
            ])->scopes([
                'email',  'user_birthday'
            ])->redirect();
        }
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        if ($provider == 'facebook') {
            $socialiteUser = Socialite::driver($provider)->fields([
                'first_name',
                'last_name',
                'email'
            ])->user();
        } else{
            $socialiteUser = Socialite::driver($provider)->user();
        }

        $user = $this->findOrCreateUser($provider, $socialiteUser);

        auth()->login($user, true);

        return redirect('/');
    }

    public function findUserBySocialId($provider, $id)
    {
        $socialAccount = SocialAccount::where('provider', $provider)
            ->where('provider_id', $id)
            ->first();

        return $socialAccount ? $socialAccount->user : false;
    }

    public function findUserByEmail($provider, $email)
    {
        return User::where('email', $email)->first();
    }

    public function addSocialAccount($provider, $user, $socialiteUser)
    {
        SocialAccount::create([
            'user_id' => $user->id,
            'provider' => $provider,
            'provider_id' => $socialiteUser->getId(),
            'token' => $socialiteUser->token,
        ]);
    }

    public function findOrCreateUser($provider, $socialiteUser)
    {
        if ($user = $this->findUserBySocialId($provider, $socialiteUser->getId())) {
            return $user;
        }

        if ($user = $this->findUserByEmail($provider, $socialiteUser->getEmail())) {
            $this->addSocialAccount($provider, $user, $socialiteUser);

            return $user;
        }

        if ($provider == 'facebook') {
            $data = $socialiteUser->getRaw();
        } else {
            $name = explode(' ', $socialiteUser->getName());
            $data = [
                'first_name' => $name[0],
                'last_name' => $name[1],
            ];
        }
        $username = strtolower($data['first_name'] . "." . $data['last_name']);
        $check = User::whereUsername($username)->count();
        if ($check > 0) {
            $username .= "." . str_random(6);
        }


        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'username' => $username,
            'email' => $socialiteUser->getEmail(),
            'password' => bcrypt(str_random(25)),
            'status' => 1,
        ]);

        if($user){
            $user->assignRole('user');
        }

        $this->addSocialAccount($provider, $user, $socialiteUser);

        return $user;
    }
}
