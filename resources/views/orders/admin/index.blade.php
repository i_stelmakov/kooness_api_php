@extends('layouts.admin')
@section('title', 'Admin | Orders')
@section('page-header', 'Orders')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <table id="table-orders" class="table table-striped dataTables" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Serial</th>
                            <th>User</th>
                            <th>Payment Method</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#table-orders').DataTable({
                "scrollX": true,
                "processing": true,
                "serverSide": true,
                "searching": false,
                "stateSave": true,
                "order": [[ 5, "desc" ]],
                "ajax": {
                    "url": "{{ route('admin.orders.retrieve', [$not_completed]) }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "id"},
                    {"data": "serial","bSortable": false},
                    {"data": "user","bSortable": false},
                    {"data": "payment_method","bSortable": false},
                    {"data": "status","bSortable": false},
                    {"data": "created_at","bSortable": false},
                    {"data": "options","bSortable": false}
                ],
                // aggiungo meccanismo per settare classe di ogni cella come titolo colonna
                'createdRow': function( row, data, dataIndex ) {
                    $(row).attr('id', 'row-' + dataIndex);
                },
                'columnDefs': [
                    {
                        'targets': '_all', // punto tutte le colonne
                        // 'targets': 1, // punto la seconda colonna
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            var valueIndex = 0;
                            for (var k in rowData){
                                if (rowData.hasOwnProperty(k)) {
                                    console.log("Key is " + k + " index " + valueIndex);
                                    if(valueIndex == col) {
                                        $(td).attr('class', 'cell-' + k);
                                    }
                                    valueIndex++;
                                }
                            }
                        }
                        // esempio di attivazione di attributi CSS personalizzati per una singola cella 
                        // if ( cellData < 1 ) {
                        //     $(td).css('color', 'red')
                        // }
                    }
                ]
            });
        });
    </script>
@endsection