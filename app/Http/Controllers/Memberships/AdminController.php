<?php

namespace App\Http\Controllers\Memberships;

use App\Models\MbrAddress;
use App\Models\MbrCard;
use App\Models\MbrChangePlanRequest;
use App\Models\MbrPayment;
use App\Models\MbrPlan;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\User;
use App\Services\Notification;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if (roleCheck('gallerist') && !isPremiumUser() && !isTrialUser()) {
            return redirect()->route('admin.memberships.plans')->send();
        }
        $activation_date = null;
        $cardType = 'Not defined';
        $plan = null;
        if (isPremiumUser()) {
            $first_payment = MbrPayment::whereUserId(Auth::user()->id)->whereStatus(1)->orderBy('created_at', 'ASC')->first();
            $last_payment = MbrPayment::whereUserId(Auth::user()->id)->orderBy('created_at', 'DESC')->first();
            if ($first_payment) {
                $activation_date = $first_payment->created_at;
            }
            if ($last_payment) {
                $plan = $last_payment->plan()->first();
            }

            $card = Auth::user()->mbr_card()->first();
            if ($card) {
                $cardType = $card->type;
            }
            $pending_request = Auth::user()->pending_request()->first();
            $pending_request_plan = null;
            if($pending_request){
                $pending_request_plan = $pending_request->plan()->first();
            }
        }
        return view('memberships.admin.index', compact('plan', 'activation_date', 'cardType', 'pending_request', 'pending_request_plan'));
    }

    public function plans()
    {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $plans = MbrPlan::all();
        $currency = 'eur';
        $current_plan = null;
        if (isPremiumUser()) {
            $last_payment = MbrPayment::whereUserId(Auth::user()->id)->whereStatus(1)->orderBy('created_at', 'DESC')->first();
            if ($last_payment) {
                $current_plan = $last_payment->plan()->first();
            }
        }
        return view('memberships.admin.plans', compact('plans', 'currency', 'current_plan'));
    }

    public function billing(Request $request)
    {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if ($request->method() == 'POST') {
            if($request->has('request-change-plan')){
                $membership_id = $request->get('membership-plan');
                $request->session()->put('membership_request_plan_id', $membership_id);
                return redirect()->route('admin.memberships.request.change.plan');
            }
            $membership_id = $request->get('membership-plan');
            $request->session()->put('membership_plan_id', $membership_id);
        } else {
            if (!$request->session()->get('membership_plan_id')) {
                return redirect()->route('admin.memberships.plans');
            }
        }
        $countries = Country::all();
        return view('memberships.admin.billing', compact('countries'));
    }

    public function card(Request $request)
    {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if ($request->method() == 'POST') {
            $billing_data = $request->all();
            unset($billing_data['_token']);
            $billing_data = (object)$billing_data;
            $request->session()->put('membership_plan_billing', json_encode($billing_data));
        } else {
            if (!$request->session()->get('membership_plan_billing')) {
                return redirect()->route('admin.memberships.billing');
            }
            $billing_data = json_decode($request->session()->get('membership_plan_billing'));
        }
        if (!$request->session()->get('membership_plan_id')) {
            return redirect()->route('admin.memberships.plans');
        } else {
            $plan = MbrPlan::find($request->session()->get('membership_plan_id'));
        }

        $change_plan = false;
        $card_data = false;
        return view('memberships.admin.card', compact('billing_data', 'plan', 'change_plan', 'card_data'));
    }

    public function requestChangePlan(Request $request){
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if ($request->method() == 'POST') {
            $mbrRequest = new MbrChangePlanRequest();
            $mbrRequest->user_id = Auth::user()->id;
            $mbrRequest->plan_id = $request->session()->get('membership_request_plan_id');
            $mbrRequest->save();
            $request->session()->flash('success', "Membership request change plan completed with success!");

            $user = Auth::user();
            $to = $user->email;
            $name = $user->full_name;
            $subject = SUBJECT_MAIL_MEMBERSHIP_CHANGE_PLAN;
            $extras = [
                "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
            ];

            Mail::send('emails.gallerists.change-plan', $extras, function ($message) use ($to, $name, $subject) {
                $message->to($to, $name)->subject($subject);
            });

            $users = User::permission('admin-level')->get();
            $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_REQUEST_CHANGE_MEMBERSHIP, 'membership', $user);
            Notification::addUsers($notify, $users);

            return redirect()->route('admin.memberships.index');
        } else{
            if (!$request->session()->get('membership_request_plan_id')) {
                return redirect()->route('admin.memberships.plans');
            }
        }
        $billing_data = Auth::user()->mbr_address()->first();
        $card_data = Auth::user()->mbr_card()->first();
        $plan = MbrPlan::find($request->session()->get('membership_request_plan_id'));
        $change_plan = true;

        return view('memberships.admin.card', compact('billing_data', 'plan', 'change_plan', 'card_data'));
    }

    public function undoRequestChangePlan(Request $request){
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $pending_request = Auth::user()->pending_request()->first();
        $pending_request->delete();
        $request->session()->flash('success', "Membership request successfully removed!");
        return redirect()->route('admin.memberships.index');
    }

    public function edit(Request $request) {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $countries = Country::all();
        $user = Auth::user();
        if ($request->method() == 'POST') {
            if ($request->has('address_id')) {
                $data = $request->all();
                $mbrAddress = MbrAddress::find($request->get('address_id'));
                unset($data['_token']);
                unset($data['address_id']);
                if ($mbrAddress) {
                    if ($mbrAddress->update($data)) {
                        $request->session()->flash('success', "Address successful saved!");
                        return redirect()->back();
                    } else {
                        return redirect()->back()->with("error", "si sono presentati  problemi durante il salvataggio dell'indirizzo")->withInput($request->all());
                    }
                } else {
                    return redirect()->back()->with("error", "si sono presentati  problemi durante il salvataggio dell'indirizzo")->withInput($request->all());
                }
            } else {
                $mbrCard = null;
                $stripe = Stripe::make(config('services.stripe.secret'));
                $mbrCard = Auth::user()->mbr_card()->first();
                if (Auth::user()->customer_id) {
                    $token = $stripe->tokens()->create([
                        'card' => [
                            'name' => $request->get('card_owner'),
                            'number' => $request->get('card_number'),
                            'exp_month' => $request->get('card_exp_month'),
                            'exp_year' => $request->get('card_exp_year'),
                            'cvc' => $request->get('card_cvv')
                        ],
                    ]);
                    if (isset($token['id'])) {
                        try {
                            $card = $stripe->cards()->create(Auth::user()->customer_id, $token['id']);
                        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                            $card = null;
                        }
                        if (isset($card['id'])) {
                            if ($mbrCard) {
                                try {
                                    $stripe->cards()->delete(Auth::user()->customer_id, $mbrCard->token);
                                } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                                    return;
                                }
                                $mbrCard->user_id = Auth::user()->id;
                                $mbrCard->token = $card['id'];
                                $mbrCard->name = $card['name'];
                                $mbrCard->end_with = substr($request->get('card_number'), strlen($request->get('card_number')) - 4, strlen($request->get('card_number')));
                                $mbrCard->expire_date = $request->get('card_exp_year') . "-" . $request->get('card_exp_month') . "-01";
                                $mbrCard->type = $request->get('card_type');
                                if (!$mbrCard->save()) {
                                    return redirect()->back()->with("error", "ci sono stati problemi al salvataggio della carta in stripe")->withInput($request->all());
                                } else {
                                    $request->session()->flash('success', "Card successful saved!");
                                    return redirect()->back();
                                }
                            } else {
                                return redirect()->back()->with("error", "si sono presentati problemi durante il recupero della vecchia carta")->withInput($request->all());
                            }
                        } else {
                            return redirect()->back()->with("error", "si sono presentati problemi durante la restituzione del token da parte di stripe")->withInput($request->all());
                        }
                    } else {
                        return redirect()->back()->with("error", "si sono presentati problemi durante la restituzione del token da parte di stripe")->withInput($request->all());
                    }
                }
            }
        }
        $address = $user->mbr_address()->first();
        $card = $user->mbr_card()->first();
        return view('memberships.admin.billing-card-edit', compact('countries', 'address', 'card'));
    }

    public function paymentAction(Request $request) {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if ($request->method() == 'POST') {
            $stripe = Stripe::make(config('services.stripe.secret'));

            $mbrCard = null;
            if (!Auth::user()->customer_id) {
                $customer = $stripe->customers()->create([
                    'email' => Auth::user()->email
                ]);
                if (isset($customer['id'])) {
                    Auth::user()->customer_id = $customer['id'];
                    Auth::user()->save();
                } else {
                    return redirect()->back()->with("error", "si sono presentati problemi durante la creazione del customer ID")->withInput($request->all());
                }
            }

            if (Auth::user()->customer_id) {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'name' => $request->get('card_owner'),
                        'number' => $request->get('card_number'),
                        'exp_month' => $request->get('card_exp_month'),
                        'exp_year' => $request->get('card_exp_year'),
                        'cvc' => $request->get('card_cvv')
                    ],
                ]);
                if (isset($token['id'])) {
                    try {
                        $card = $stripe->cards()->create(Auth::user()->customer_id, $token['id']);
                    } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
                        $card = null;
                    }
                    if (isset($card['id'])) {
                        $mbrCard = new MbrCard();
                        $mbrCard->user_id = Auth::user()->id;
                        $mbrCard->token = $card['id'];
                        $mbrCard->name = $card['name'];
                        $mbrCard->end_with = substr($request->get('card_number'), strlen($request->get('card_number')) - 4, strlen($request->get('card_number')));
                        $mbrCard->expire_date = $request->get('card_exp_year') . "-" . $request->get('card_exp_month') . "-01";
                        $mbrCard->type = $request->get('card_type');
                        if (!$mbrCard->save()) {
                            return redirect()->back()->with("error", "ci sono stati problemi al salvataggio della carta in stripe");
                        }
                    }
                    if (!$card) {
                        return redirect()->back()->with("error", "si sono presentati problemi durante la restituzione del token da parte di stripe")->withInput($request->all());
                    }
                } else {
                    return redirect()->back()->with("error", "si sono presentati problemi durante la restituzione del token da parte di stripe")->withInput($request->all());
                }

            }

            $mbrAddress = null;
            if ($request->session()->get('membership_plan_billing')) {
                $data = json_decode($request->session()->get('membership_plan_billing'), true);
                $mbrAddress = new MbrAddress($data);
                $mbrAddress->user_id = Auth::user()->id;
                if (!$mbrAddress->save()) {
                    $mbrCard->delete();
                    return redirect()->back()->with("error", "si sono presentati  problemi durante il salvataggio dell'indirizzo")->withInput($request->all());
                }
            } else {
                $mbrCard->delete();
                return redirect()->route('admin.memberships.plans');
            }

            $plan = null;
            if ($request->session()->get('membership_plan_id')) {
                $plan = MbrPlan::find($request->session()->get('membership_plan_id'));
                if (!$plan) {
                    return redirect()->back()->with("error", "si sono presentati problemi durante il salvataggio della membership")->withInput($request->all());
                }
                $gallery =  Auth::user()->gallery()->first();
                $is_italian = false;
                if($gallery){
                    if($gallery->country()->first()->code == 'IT'){
                        $is_italian = true;
                    }
                }
                $payment = new MbrPayment();
                $payment->serial = strtoupper(substr(sha1(time()), 0, 10));
                $payment->user_id = Auth::user()->id;
                $payment->plan_id = $plan->id;
                $payment->subtotal = $plan->fixed_fee;
                $country = Country::find($mbrAddress->country);
                $payment->total = $plan->fixed_fee;
                if ($is_italian) {
                    $payment->tax = 22;
                    $payment->total = $plan->fixed_fee * 1.22;
                }
                $payment->currency = $plan->getCurrency();
                $payment->full_name = $mbrAddress->full_name;
                $payment->vat_number = $mbrAddress->vat_number;
                $payment->fiscal_code = $mbrAddress->fiscal_code;
                $payment->address = $mbrAddress->address;
                $payment->country = $mbrAddress->country;
                $payment->city = $mbrAddress->city;
                $payment->state = $mbrAddress->state;
                $payment->zip = $mbrAddress->zip;
                $payment->card_type = $mbrCard->type;
                if (!$payment->save()) {
                    $mbrAddress->delete();
                    $mbrCard->delete();
                    return redirect()->back()->with("error", "problemi durante il salvataggio del pagamento")->withInput($request->all());
                }

                $charge_data = [
                    'source' => $mbrCard->token,
                    'customer' => Auth::user()->customer_id,
                    'currency' => $payment->currency,
                    'amount' => $payment->total,
                    'description' => "Mambership #" . $payment->serial,
                ];
                $charge = $stripe->charges()->create($charge_data);
                if ($charge['status'] == 'succeeded') {
                    $payment->status = 1;
                    $payment->transaction_id = $charge['id'];
                    $payment->save();
                    Auth::user()->is_premium = 1;
                    Auth::user()->is_trial = 0;
                    Auth::user()->current_plan_id = $plan->id;
                    if (date('d') == '01') {
                        Auth::user()->premium_exp_date = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 1, date('Y')));
                    } else {
                        Auth::user()->premium_exp_date = date('Y-m-d', mktime(0, 0, 0, date('m') + 2, 1, date('Y')));
                    }
                    Auth::user()->save();
                    $request->session()->remove('membership_plan_billing');
                    $request->session()->remove('membership_plan_id');

                    $user = Auth::user();
                    if($this->generateInvoice($payment, $user, $user->gallery()->first(), $plan)) {
                        $to = $user->email;
                        $name = $user->full_name;
                        $subject = SUBJECT_MAIL_MEMBERSHIP_ACTIVATION;
                        $extras = [
                            "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                            "plan" => $plan, //	name
                            "payment" => $payment,  // subtotal, tax, total, currency, full_name, vat_number, fiscal_code, address, country(ID riferimento tabella country), city, state, zip, card_type
                        ];
                        Mail::send('emails.gallerists.activation', $extras, function ($message) use ($to, $name, $subject, $payment) {
                            $message->to($to, $name)->subject($subject);
                            $message->attach(storage_path("/invoices/memberships/{$payment->serial}.pdf"));
                        });
                    }

                    $users = User::permission('admin-level')->get();
                    $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_NEW_MEMBERSHIP, 'membership', $user);
                    Notification::addUsers($notify, $users);

                } else {
                    $mbrAddress->delete();
                    $mbrCard->delete();
                    return redirect()->back()->with("error", "pagamento stripe fallito")->withInput($request->all());
                }
            } else {
                return redirect()->route('admin.memberships.plans')->withInput($request->all());
            }
            $request->session()->remove('membership_plan_id');
            $request->session()->remove('membership_plan_billing');

            $request->session()->flash('success', "Membership subscription completed with success!");
            return redirect()->route('admin.memberships.index');
        }
    }

    public function deactivate(Request $request) {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $user = Auth::user();
        $plan = $user->mbr_plan()->first();
        if ($plan) {
            $user->deactivate_premium_request_date = date('Y-m-d');
            if ($user->save()) {
                if($user->have_pending_request()){
                    $change_request = $user->pending_request()->first();
                    $change_request->delete();
                }
                $to = $user->email;
                $name = $user->full_name;
                $subject = SUBJECT_MAIL_MEMBERSHIP_CANCELLATION;
                $extras = [
                    "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                    "plan" => $plan, //	name
                ];
                Mail::send('emails.gallerists.cancellation', $extras, function ($message) use ($to, $name, $subject) {
                    $message->to($to, $name)->subject($subject);
                });

                $users = User::permission('admin-level')->get();
                $notify = Notification::createNotification(null, Notification::NOTIFICATION_USER_REQUEST_DEACTIVATE_MEMBERSHIP, 'membership', $user);
                Notification::addUsers($notify, $users);

            }
        }
        return redirect()->route('admin.memberships.index');
    }

    public function reactivate() {
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $user = Auth::user();
        $plan = $user->mbr_plan()->first();
        if ($plan) {
            $user->deactivate_premium_request_date = null;
            $user->save();
        }
        return redirect()->route('admin.memberships.index');
    }

    public function payNow(Request $request){
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        if($request->has('payment_id')){
            $payment = MbrPayment::find($request->get('payment_id'));
            $stripe = Stripe::make(config('services.stripe.secret'));
            if($payment){
                $plan = $payment->plan()->first();
                $mbrCard = Auth::user()->mbr_card()->first();
                $charge_data = [
                    'source' => $mbrCard->token,
                    'customer' => Auth::user()->customer_id,
                    'currency' => $payment->currency,
                    'amount' => $payment->total,
                    'description' => "Mambership #" . $payment->serial,
                ];

                $error_message = 'generic_error';
                try{
                    $charge = $stripe->charges()->create($charge_data);
                }catch (\Cartalyst\Stripe\Exception\CardErrorException $e){
                    $array_error = $e->getRawOutput();
                    if(isset($array_error['error']) && isset($array_error['error']['message'])){
                        $error_message = $array_error['error']['message'];
                    }
                    $charge = ['status' => "failed"];
                }

                $user = Auth::user();
                if ($charge['status'] == 'succeeded') {
                    $payment->status = 1;
                    $payment->transaction_id = $charge['id'];
                    $payment->save();
                    $user->premium_exp_date = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, 1, date('Y')));
                    $user->renewal_failed_payment_id = null;
                    $user->renewal_failed_date = null;
                    $user->save();

                    if($this->generateInvoice($payment, $user, $user->gallery()->first(), $plan)) {
                        $to = $user->email;
                        $name = $user->full_name;
                        $subject = SUBJECT_MAIL_MEMBERSHIP_SUCCESSFUL_RENEWAL;
                        $extras = [
                            "user" => $user, // username, email, first_name, last_name, is_premium, is_trial, trial_activation_date, deactivate_premium_request_date, premium_exp_date
                            "plan" => $plan, //	name
                            "payment" => $payment,  // subtotal, tax, total, currency, full_name, vat_number, fiscal_code, address, country(ID riferimento tabella country), city, state, zip, card_type
                        ];
                        Mail::send('emails.gallerists.success-renewal', $extras, function ($message) use ($to, $name, $subject, $payment) {
                            $message->to($to, $name)->subject($subject);
                            $message->attach(storage_path("/invoices/memberships/{$payment->serial}.pdf"));
                        });
                    }
                    return redirect()->route('admin.memberships.index');
                } else{
                    return redirect()->route('admin.memberships.index')->with("error", "Stripe Error: $error_message");
                }
            }
        }
    }

    public function generateInvoice($payment, $user, $gallery, $plan) {
        if (File::exists(storage_path("/config/invoices_" . date('Y') . ".json")))
            $json = file_get_contents(storage_path("/config/invoices_" . date('Y') . ".json"));
        else {
            $json = json_encode(['current_number' => 1, "year" => date('Y')]);
            file_put_contents(storage_path("/config/invoices_" . date('Y') . ".json"), $json);
        }
        $data = json_decode($json);
        $pdf = PDF::loadView('pdf.membership-invoice', ['payment' => $payment, "user" => $user, 'gallery' => $gallery, 'plan' => $plan, 'invoice_number' => $data->current_number]);
        $pdf->save(storage_path("/invoices/memberships/{$payment->serial}.pdf"));
        $data->current_number++;
        $json = json_encode($data);
        file_put_contents(storage_path("/config/invoices_" . date('Y') . ".json"), $json);
        return true;
    }

    public function getHistoryPayment(Request $request){
        if(!roleCheck('gallerist')){
            return redirect()->route('admin.dashboard')->send();
        }

        $columns = array(
            0 => 'id',
            1 => 'reference',
            2 => 'plan',
            3 => 'referer_to',
            4 => 'total',
            5 => 'updated_at',
            6 => 'options',
        );

        $totalData = MbrPayment::whereUserId(Auth::user()->id)->whereStatus(1)->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $payments =  MbrPayment::whereUserId(Auth::user()->id)
            ->whereStatus(1)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();

        $data = array();
        if (!empty($payments)) {
            foreach ($payments as $payment) {
                $invoice = route('admin.memberships.view.invoice', [$payment->serial]);
                $nestedData['id'] = $payment->id;
                $nestedData['reference'] = $payment->serial;
                $nestedData['plan'] = $payment->plan()->first()->name;
                $nestedData['referer_to'] = date('d/m/Y', strtotime($payment->created_at));
                $nestedData['referer_to'] .= " - ";
                if (date('d', strtotime($payment->created_at)) == '01') {
                    $nestedData['referer_to'] .= date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($payment->created_at)) + 1, 1, date('Y', strtotime($payment->created_at))));
                } else {
                    $nestedData['referer_to'] .= date('d/m/Y', mktime(0, 0, 0, date('m', strtotime($payment->created_at)) + 2, 1, date('Y', strtotime($payment->created_at))));
                }

                $nestedData['total'] = $payment->total . " " . $payment->getSymbolCurrencyAttribute();
                $nestedData['updated_at'] = date('j M Y h:i a', strtotime($payment->updated_at));
                $nestedData['options'] = (View::make('partials.datatable-options', ['edit' => null, 'delete' => null ,'invoice' => $invoice]))->render();
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);

    }

    public function viewInvoice(Request $request, $serial){
        $filename = '/invoices/memberships/'.$serial.'.pdf';
        $pathToFile = storage_path($filename);
        if(!File::exists($pathToFile)){
            return redirect()->route('admin.memberships.index');
        }
        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->file($pathToFile, $headers);
    }
}