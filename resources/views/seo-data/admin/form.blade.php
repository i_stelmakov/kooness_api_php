<div class="row">
    <div class='col-md-12 col-xs-12'>
        <form class="ajax-form" role="form" method="POST" action="{{ $action }}" data-attachment="validate" data-id="" data-referer="">
            {{ method_field($method) }}
            @csrf
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>The fields marked with asterisk (*) are required </p>
                </div>
            </div>
            <br>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    Details
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="url">Url (*)</label>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon3">{{ url('') }}</span>
                                <input id="url" class="form-control" type="text" placeholder="Url" name="url" value="{{ old('url', $data->{'url'}) }}" required>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    SEO Management
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div>
                                <label><input class="iCheck-input" type="checkbox" name="no_index" {{ ($data->{'no_index'}? 'checked="checked"' : '') }}> No Index</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div>
                                <label><input class="iCheck-input" type="checkbox" name="no_follow" {{ ($data->{'no_follow'}? 'checked="checked"' : '') }}> No follow</label>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_title">SEO - title</label>
                                <input class="form-control" name="meta_title" type="text" placeholder="SEO - title" id="meta_title" value="{{ $data->{'meta_title'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="meta_keywords">SEO - keywords</label>
                                <input class="form-control" name="meta_keywords" type="text" placeholder="SEO - keywords" id="meta_keywords" value="{{ $data->{'meta_keywords'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="h1">SEO - H1</label>
                                <input class="form-control"
                                       name="h1" type="text"
                                       placeholder="SEO - H1"
                                       id="h1"
                                       value="{{ $data->{'h1'} }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="canonical">SEO - Canonical Url</label>
                                <input class="form-control"
                                       name="canonical" type="text"
                                       placeholder="SEO - Canonical Url"
                                       id="canonical"
                                       value="{{ $data->{'canonical'} }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="meta_description">SEO - Description</label>
                                <textarea id="meta_description" class="form-control" placeholder="SEO Description" name="meta_description" rows="5">{{ $data->{'meta_description'} }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Form Footer & Submit -->
            <div class="box box-primary box-solid">
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right ">
                            <input class="btn btn-primary" type="submit" value="Save"/>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>