<div class="slider-item-txt">
    <div class="default-sheet-row slider-item-row">
        <div class="default-sheet-row-cell">
            <h2><a title="{{ $item->first_name }} {{ $item->last_name }}" class="dark-text" href="{{ route('artists.single', [$item->slug]) }}">{{ $item->first_name }} {{ $item->last_name }}</a></h2>
        </div>
    </div>
    @if($item->countryBirth()->count())
        <p class="dark-text provenance">{{ $item->countryBirth()->first()->name }}</p>
    @endif
    <p class="exposure">{{ $item->artworks()->count() }} Works exhibited</p>
    <a class="read-more" href="{{ route('artists.single', [$item->slug]) }}">View artist page</a>
</div>