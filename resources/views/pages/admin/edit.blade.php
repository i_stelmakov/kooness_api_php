@extends('layouts.admin')
@section('title', "Admin | Edit Page `{$page->title}`")
@section('page-header', "Admin - Edit Page `{$page->title}`")

@section('content')
    @include('pages.admin.form', ['action' => route('admin.pages.update', [$page->id]), 'method' => 'PATCH'])
@endsection