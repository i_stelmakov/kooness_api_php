<?php
/**
 * Created by IntelliJ IDEA.
 * User: Marco
 * Date: 31/07/18
 * Time: 09:53
 */

namespace App\Services;

use App\Models\Redirect301;
use Spatie\MissingPageRedirector\Redirector\Redirector;
use Symfony\Component\HttpFoundation\Request;

class ConfigurationRedirector implements Redirector
{
    public function getRedirectsFor(Request $request): array
    {
        $data = Redirect301::all();
        $return = [];
        foreach ($data as $datum) {
            $return[$datum->old_url] = $datum->new_url;
        }
        return $return;
    }
}
