<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitionsArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibitions_artists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exhibition_id')->unsigned();
            $table->integer('artist_id')->unsigned();

            $table->timestamps();

            $table->foreign('exhibition_id')
                ->references('id')
                ->on('exhibitions')
                ->onDelete('CASCADE');

            $table->foreign('artist_id')
                ->references('id')
                ->on('artists')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibitions_artists');
    }
}
