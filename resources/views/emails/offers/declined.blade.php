{{-- Mail - Offer Declined --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_OFFER_DECLINED}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: center;'>
		We are sorry, your offer proposal for<br>
		"{{ $artwork->title }}"<br>
		has been rejected.<br>
		Please, look for other artworks:<br>
		<a title="View all" style="background-color: rgb(16, 16, 16); border: none; box-sizing: border-box; color: rgb(255, 255, 255); cursor: pointer; display: inline-block; font-family: Muli, sans-serif; font-size: 20px; font-stretch: 100%; font-style: normal; font-weight: 400; height: 44px; line-height: 20px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 10px; padding-bottom: 12px; padding-left: 20px; padding-right: 20px; padding-top: 12px; text-decoration-line: none; text-decoration: none; text-decoration-style: solid; vertical-align: baseline; white-space: normal; -webkit-font-smoothing: antialiased;"
		href="{{ route('artworks.archive.all') }}">Browse Artworks</a>
</p>
@stop