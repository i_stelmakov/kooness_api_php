@extends('layouts.app')

@section('title', ($category && $category->meta_title) ? $category->meta_title : ($seo_data && $seo_data->meta_title ? $seo_data->meta_title : 'Kooness | Galleries'))

@section('body-class', 'galleries-archive')

@section('header-og-abstract', ($category && $category->meta_description) ? $category->meta_description : ($seo_data && $seo_data->meta_description ? $seo_data->meta_description : null))

@section('canonical', ($category && $category->canonical) ? $category->canonical :($seo_data && $seo_data->canonical ? $seo_data->canonical : null))

@section('seo-keywords', ($category && $category->meta_keywords) ? $category->meta_keywords : ($seo_data && $seo_data->meta_keywords ? $seo_data->meta_keywords : null))

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    <div class="sections">
        <!-- Gallery Sheet Header !-->
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span>
                                @if($category && $category->h1)
                                    <h1>{{ $category->h1 }}</h1>
                                @elseif(($seo_data && $seo_data->h1))
                                    <h1>{{  $seo_data->h1 }}</h1>
                                @elseif($category)
                                    <h1>All {{ $category->name }} Galleries</h1>
                                @else
                                    <h1>Galleries{{ ($fair)? ' in fair ' . $fair->name : '' }}</h1>
                                @endif

                                @if( $category && $category->description )
                                    {!! $category->description !!}
                                @endif
                            </span>
                        </div>
                    </div>
                    <div style="display: block; text-align: right; width: 100%; position: relative;">
                        <div class="page-header-filter">
                            <div style="margin-left: auto;" class="default-sheet-row-cell">
                                <p>Location</p>
                                <form>
                                    <select class="search-by location">
                                        <option value="">All</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->code }}" {{ (app('request')->input('location') == $city->code )? 'selected="selected"' : ''}}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                            <div class="default-sheet-row-cell">
                                <p>Category</p>
                                <form>
                                    <select class="search-by category">
                                        <option value="">All</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->slug }}" {{ (app('request')->input('category') == $category->slug )? 'selected="selected"' : ''}}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="header-secondary-menu">
            <a class="search-by" data-value="all">All</a> 
            <a class="search-by" data-value="number">0-9</a>
            <a class="search-by" data-value="a">A</a> <a class="search-by" data-value="b">B</a>
            <a class="search-by" data-value="c">C</a> <a class="search-by" data-value="d">D</a>
            <a class="search-by" data-value="e">E</a> <a class="search-by" data-value="f">F</a>
            <a class="search-by" data-value="g">G</a> <a class="search-by" data-value="h">H</a>
            <a class="search-by" data-value="i">I</a> <a class="search-by" data-value="j">J</a>
            <a class="search-by" data-value="k">K</a> <a class="search-by" data-value="l">L</a>
            <a class="search-by" data-value="m">M</a> <a class="search-by" data-value="n">N</a>
            <a class="search-by" data-value="o">O</a> <a class="search-by" data-value="p">P</a>
            <a class="search-by" data-value="q">Q</a> <a class="search-by" data-value="r">R</a>
            <a class="search-by" data-value="s">S</a> <a class="search-by" data-value="t">T</a>
            <a class="search-by" data-value="u">U</a> <a class="search-by" data-value="v">V</a>
            <a class="search-by" data-value="w">W</a> <a class="search-by" data-value="x">X</a>
            <a class="search-by" data-value="y">Y</a> <a class="search-by" data-value="z">Z</a>
        </div>
        <section id="archive" class="galleries-page"
                 data-filter-alpha="{{ app('request')->input('startWith') }}"
                 data-filter-location="{{ app('request')->input('location') }}"
                 data-filter-category="{{ app('request')->input('category') }}"
                 data-filter-type="{{ (isset($filter_type) ? $filter_type : '') }}"
                 data-filter-value="{{ (isset($filter_value) ? $filter_value : '') }}">
            <div class="container boxed-container">
                <div class="container col-container">
                    <section id="product-masonry" class="default-slider-section">
                        <div class="container col-container-with-offset container-load">
                            @foreach($galleries as $gallery)
                                <div class="slider-item one-third-col-with-margin">
                                    <i title="Remove" class="fa fa-times delete-item"> </i>
                                    @if(Auth::user())
                                        <div class="add-item follow {{ (Auth::user()->collectionGalleries->contains($gallery->id)) ? 'active' : '' }}" data-section="galleries" data-id="{{$gallery->id}}">
                                            @if(Auth::user()->collectionGalleries->contains($gallery->id))
                                                <i class="fa fa-heart"></i>
                                            @else
                                                <i class="fa fa-heart-o"></i>
                                            @endif
                                        </div>
                                    @endif
                                    <div class="slider-item-img wide-box">
                                        <a title="{{ $gallery->name }}" href="{{ route('galleries.single', [$gallery->slug]) }}">
                                            <div class="background-cover gallery lazy" data-src="{{ $gallery->url_wide_box }}"></div>
                                        </a>
                                    </div>
                                    <div class="slider-item-txt">
                                        <div class="default-sheet-row slider-item-row">
                                            <div class="default-sheet-row-cell">
                                                <h2>
                                                    <a title="{{ $gallery->name }}" class="dark-text" href="{{ route('galleries.single', [$gallery->slug]) }}">{{ $gallery->name }}</a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="default-sheet-row slider-item-row">
                                            <div class="default-sheet-row-cell">
                                                <p class="gallery-adress">{{ $gallery->city }}, {{ $gallery->address }}</p>
                                            </div>
                                        </div>
                                        <a title="{{ $gallery->name }}" class="read-more" href="{{ route('galleries.single', [$gallery->slug]) }}">read more</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </section>
        {!! $galleries->links('layouts.includes.pagination') !!}
        </div>
    </div>
@endsection