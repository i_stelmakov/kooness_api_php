{{-- Mail - User Registered --}}

@extends('emails.layout')

@section('title')
	{{SUBJECT_MAIL_REGISTRATION_USER_REGISTERED}}
@endsection

@section('content')
	<br>
	<p style='margin: 16px; color: #636363; font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>
		Dear {{ $user->first_name }},<br>
		Thank you for signing up to our service. You can now connect with your credential.
	</p>
@stop