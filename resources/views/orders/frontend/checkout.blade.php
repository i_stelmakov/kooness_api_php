@extends('layouts.app')

@section('title')
    Checkout
@endsection

@section('body-class')
    checkout
@endsection

@section('maps')
    <script type="text/javascript" src="/js/map-dependencies.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxzde8RaVZH-pv5ISqx402r4vR9IDWGsI&libraries=places&callback=initGMaps"></script>
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/checkout.js"></script>
@endsection

@section('content')
    <section class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <h1>Purchase</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="cart">
            <div class="boxed-container">
                <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                    @if($cart->items()->count())
                        @foreach($cart->items()->get() as $item)
                            <?php
                            $product = $item->product;
                            $casual_transfer[] = $item->product->title;
                            ?>
                            <div class="full-col-with-margin cart-item">
                                <div class="col-container">
                                    <div class="one-third-col-with-margin">
                                        <div class="cart-product-img">
                                            <a href="{{ route('artworks.single', [$product->slug]) }}" class="cart-thumb">
                                                <img data-src="{{ $product->main_image->url }}" src="" class="lazy"> </a>
                                        </div>
                                    </div>
                                    <div class="two-third-col-with-margin">
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <h3 class="no-margin">
                                                        <a title="{{ $product->artist_name }}" class="dark-text" href="{{ route('artists.single', [$product->artist->slug]) }}">{{ $product->artist_name }}</a>
                                                    </h3>
                                                </div>
                                                <a class="follow secondary-link"><i class="fa fa-heart-o"></i> Follow</a>
                                            </div>
                                            <div class="default-sheet-row">
                                                <span class="h1" class="no-margin">{{ $product->title }}</span>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box">
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Reference</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->reference }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Year</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->year }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Medium</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->medium_string }}</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row">
                                                <div class="default-sheet-row-cell">
                                                    <p class="dark-text"><strong>Size</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>{{ $product->measure_cm }} cm<br>{{ $product->measure_inc }} in
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="default-sheet-box default-sheet-price">
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Price</p>
                                                <div class="default-sheet-row flex-align-center">
                                                    <div class="default-sheet-row-cell">
                                                        <h4 data-price="{{ $product->price }}" data-currency="EUR">{{ $product->pretty_price }}</h4>
                                                    </div>
                                                    <div class="default-sheet-row-cell">
                                                        <select class="currency-change no-margin">
                                                            <option value="EUR" selected="selected">€</option>
                                                            <option value="USD">$</option>
                                                            <option value="GBP">£</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="default-sheet-price" class="default-sheet-col">
                                                <p>Artwork offered by</p>
                                                <div class="default-sheet-row">
                                                    <div class="default-sheet-row-cell">
                                                        <h4>
                                                            <a href="{{ route('galleries.single', [$product->gallery->slug]) }}">{{ $product->gallery->name }}</a>, {{ $product->gallery->city }}
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="full-col-with-margin cart-item">
                            <div class="col-container">
                                <p>Your cart is empty.</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        {{-- Address Modal--}}
        <div class="remodal" data-remodal-id="modal-address" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>
            <span class="h1">Addresses</span>
            <p>Add or edit all your addresses for shipping o billing.</p>
            <a class="accordion" href="#"><i class="fa fa-plus"></i> Add New Address</a>
            <div class="panel">
                <form id="addressForm0" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="addresses-editing" data-id="">
                    {{--full--}}
                    <div class="full-col-with-margin">
                        <select class="full-col" id="address_type_select" name="type">
                            <option value="private">Address type: <b>Private</b></option>
                            <option value="company">Address type: <b>Organization</b></option>
                        </select>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin for-private">
                        <input class="full-col" type="text" name="first_name" placeholder="First Name" required="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin for-private">
                        <input class="full-col" type="text" name="last_name" placeholder="Last Name" required="">
                    </div>
                    {{--full--}}
                    <div class="full-col-with-margin for-company" style="display: none">
                        <input class="full-col" type="text" name="company_name" placeholder="Company name" required="" disabled>
                    </div>
                    {{--full--}}
                    <div class="full-col-with-margin">
                        <input class="full-col" type="text" name="fiscal_code" placeholder="Fiscal Code">
                    </div>
                    {{--full--}}
                    <div class="full-col-with-margin for-company" style="display: none">
                        <input class="full-col" type="text" name="vat_number" placeholder="Vat Number" required="" disabled>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="email" placeholder="Email" required="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="tel" name="phone" placeholder="Phone" required="">
                    </div>
                    {{--full--}}
                    <div class="full-col-with-margin">
                        <input id="autocomplete-0" class="full-col address-autocomplete" type="text" name="autocomplete-0" placeholder="Viale Europa, 28/30, Castiglione delle Stiviere, MN, Italia" required="">
                        {{--hidden--}}
                        <input class="address" type="hidden" name="address">
                        <input class="number" type="hidden" name="number">
                        <input class="city" type="hidden" name="city"> <input class="state" type="hidden" name="state">
                        <input class="zip" type="hidden" name="zip_code">
                        <input class="country" type="hidden" name="country">
                        <input class="country_code" type="hidden" name="country_code">
                    </div>
                    <input class="small-input-button" type="submit" value="Add New">
                </form>
            </div>
            @foreach($addresses as $address)
                <div class="address-item">
                    <hr>
                    <div class="address-text">{{ $address->label }}</div>
                    <a class="accordion" href="#"><i class="fa fa-edit"></i> Edit Address</a>
                    <div class="panel">
                        <form id="addressForm{{ $address->id }}" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="addresses-editing" data-id="{{ $address->id }}">
                            {{--full--}}
                            <div class="full-col-with-margin">
                                <select class="full-col" id="address_type_select" name="type">
                                    <option value="private" {{ $address->type == 'private' ? 'selected="selected"' : '' }}>Address type: <b>Private</b></option>
                                    <option value="company" {{ $address->type == 'company' ? 'selected="selected"' : '' }}>Address type: <b>Organization</b></option>
                                </select>
                            </div>
                            {{--half--}}
                            <div class="one-half-col-with-margin for-private" style="{{ $address->type == 'company' ? 'display:none' : '' }}">
                                <input class="full-col" type="text" name="first_name" placeholder="First Name" required="" value="{{ $address->first_name }}" {{ $address->type == 'company' ? 'disabled' : '' }}>
                            </div>
                            {{--half--}}
                            <div class="one-half-col-with-margin for-private" style="{{ $address->type == 'company' ? 'display:none' : '' }}">
                                <input class="full-col" type="text" name="last_name" placeholder="Last Name" required="" value="{{ $address->last_name }}" {{ $address->type == 'company' ? 'disabled' : '' }}>
                            </div>
                            {{--full--}}
                            <div class="full-col-with-margin for-company" style="{{ $address->type == 'private' ? 'display:none' : '' }}">
                                <input class="full-col" type="text" name="company_name" placeholder="Company name" required="" value="{{ $address->company_name }}" {{ $address->type == 'private' ? 'disabled' : '' }}>
                            </div>
                            {{--full--}}
                            <div class="full-col-with-margin">
                                <input class="full-col" type="text" name="fiscal_code" placeholder="Fiscal Code" value="{{ $address->fiscal_code }}">
                            </div>
                            {{--full--}}
                            <div class="full-col-with-margin for-company" style="{{ $address->type == 'private' ? 'display:none' : '' }}">
                                <input class="full-col" type="text" name="vat_number" placeholder="Vat Number" required=""  value="{{ $address->vat_number }}" {{ $address->type == 'private' ? 'disabled' : '' }}>
                            </div>

                            <div class="one-half-col-with-margin">
                                <input class="full-col" type="email" name="email" placeholder="Email" required="" value="{{ $address->email }}">
                            </div>
                            {{--half--}}
                            <div class="one-half-col-with-margin">
                                <input class="full-col" type="tel" name="phone" placeholder="Phone" required="" value="{{ $address->phone }}">
                            </div>
                            {{--full--}}
                            <div class="full-col-with-margin">
                                <input id="autocomplete-{{ $address->id }}" class="full-col address-autocomplete" type="text" name="autocomplete-{{ $address->id }}" placeholder="Viale Europa, 28/30, Castiglione delle Stiviere, MN, Italia" required="" value="{{ $address->label }}">
                                {{--hidden--}}
                                <input class="address" type="hidden" name="address" value="{{ $address->address  }}">
                                <input class="number" type="hidden" name="number" value="{{ $address->number }}">
                                <input class="city" type="hidden" name="city" value="{{ $address->city }}">
                                <input class="state" type="hidden" name="state" value="{{ $address->state }}">
                                <input class="zip" type="hidden" name="zip_code" value="{{ $address->zip }}">
                                <input class="country" type="hidden" name="country" value="{{ $address->country }}">
                                <input class="country_code" type="hidden" name="country_code" value="{{ $address->country_code }}">
                            </div>
                            <input class="small-input-button" type="submit" value="Save Address">
                            <button class="small-input-button delete-address">Delete Address</button>
                        </form>
                    </div>
                </div>
            @endforeach
            <hr>
            <br>
            {{-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> --}}
            <button data-remodal-action="confirm" class="remodal-confirm">Close</button>
        </div>
        {{-- Address Modal--}}

        {{-- Card Modal--}}
        <div class="remodal" data-remodal-id="modal-cards" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>
            <span class="h1">Cards</span>
            <p>Add or edit all your cards for shipping o billing.</p>
            <a class="accordion" href="#"><i class="fa fa-plus"></i> Add New Card</a>
            <div class="panel">
                <form id="cardForm0" class="k-form col-container-with-offset-and-margin add-edit-form" data-kind="cards-editing" data-id="">
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_owner" placeholder="Owner" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col card-number" type="text" name="card_number" placeholder="Number" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <select class="full-col" name="card_exp_month" required>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <select class="full-col" name="card_exp_year" required>
                            @for($i = date('Y'); $i < date('Y') + 10; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_cvv" placeholder="CVV" required="" value="">
                    </div>
                    {{--half--}}
                    <div class="one-half-col-with-margin">
                        <input class="full-col" type="text" name="card_type" placeholder="Type" required="" readonly value="">
                    </div>
                    <input class="small-input-button" type="submit" value="Add New">
                </form>
            </div>
            @foreach($payments as $payment)
                <div class="card-item" data-id="{{ $payment->id }}">
                    <hr>
                    <div class="card-text"><img src="/images/credit_cards/light/{{ strtolower($payment->type) }}.png">
                        <p>{{ $payment->type }} - end in ***{{ $payment->end_with }}</p></div>
                    <a class="delete-card"><i class="fa fa-close"></i> Delete Card</a>
                </div>
            @endforeach
            <hr>
            <br>
            {{-- <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button> --}}
            <button data-remodal-action="confirm" class="remodal-confirm">Close</button>
        </div>
        {{-- Card Modal--}}

        @if($cart->items()->count())
            <form method="POST" action="{{ route('orders.checkout') }}" class="form-validate">
                @csrf
                <section class="cart-payment">
                    <div class="boxed-container">

                        <div class="in-page-warning error-dhl error" style="display: none">To obtain the final shipping quote for this artwork, please continue your order. You'll receive a quote for approval within 2 working days.</div>
                        <div class="in-page-warning error-shipping-address error" style="display: none">No shipping address entered, it is impossible to calculate shipping costs and proceed with the purchase. Please enter a valid address first.</div>

                        <div class="col-container container-vertical-padding-bottom container-vertical-padding-top">
                            <div class="one-half-col-with-margin">


                                {{--Billing Address Section--}}
                                <div id="billing-addresses" class="container col-container">
                                    <div class="col full-col">
                                        <h4>Billing details</h4>
                                        <p>Select your billing address from this list, or add a new one</p>
                                        <select id="billing-address" name="billing-address" class="address-select">
                                            @if(count($addresses))
                                                @foreach($addresses as $address)
                                                    <option value="{{ $address->id }}"> {{ $address->label }}</option>
                                                @endforeach
                                            @else
                                                <option value="">No address found. Add new one.</option>
                                            @endif

                                        </select> <br>
                                        <a data-remodal-target="modal-address" href="#"><i class="fa fa-edit"></i> Edit Address list</a>
                                    </div>
                                </div>
                                {{--Shipping Address Section--}}
                                <div id="shipping-addresses" class="container col-container">
                                    <div class="col full-col">
                                        <h4>Shipping details</h4>
                                        <p>Select your shipping address from this list, or add a new one</p>
                                        <select id="shipping-address" name="shipping-address" class="address-select">
                                            @if(count($addresses))
                                                @foreach($addresses as $address)
                                                    <option value="{{ $address->id }}"> {{ $address->label }}</option>
                                                @endforeach
                                            @else
                                                <option value="">No address found. Add new one.</option>
                                            @endif
                                        </select> <br>
                                        <a data-remodal-target="modal-address" href="#"><i class="fa fa-edit"></i> Edit Address list</a>
                                    </div>
                                </div>
                                <div id="note" class="container col-container">
                                    <div class="col full-col">
                                        <h4>Shipping Note</h4>
                                        <p>If you have further notes enter them here</p>
                                        <textarea name="note" id="note" style="resize: none" rows="7" placeholder="Notes..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="one-half-col-with-margin">
                                <!-- Cart Summary !-->
                                <div class="default-sheet-box cart-box">
                                    <h4>Summary</h4>
                                    @if(!$hidePromoCode)
                                        <div class="default-sheet-row">
                                            <div class="default-sheet-row-cell">
                                                <p class="dark-text"><strong>Promo code</strong></p>
                                            </div>
                                            <div class="default-sheet-row row-code" style="display: {{ ($cart->promo_code)? 'flex' : 'none' }};">
                                                <p class="dark-text">KOONESS: 15.00%</p>
                                                <a title="Remove promo code" class="remove-promo-code text-left-space">Remove discount</a>
                                            </div>
                                            <div class="default-sheet-row-cell row-insert-code" style="display: {{ (!$cart->promo_code)? 'flex' : 'none' }};">
                                                <form>
                                                    <input type="text" class="promo-code" placeholder="Example: AXCF1245">
                                                </form>
                                                <a title="Add Promo Code" class="add-promo-code text-left-space">Apply discount</a>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Subtot</strong></p>
                                        </div>
                                        <div class="default-sheet-row-cell cart-subtotal">
                                            @if($promo)
                                                <p>{{ number_format($cart->total_price * (1 - ($promo->value / 100)), 2, ',', '') }} €</p>
                                            @else
                                                <p>{{ number_format($cart->total_price, 2, ',', '') }} €</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Delivery</strong></p>
                                        </div>
                                        <div class="default-sheet-row-cell delivery-result">
                                            {{-- <p>00,00 €</p> --}}
                                            <p>
                                                <span><i class="fa fa fa-spinner fa-spin"></i> Calculating Shippings</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Tot</strong></p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            @if($promo)
                                                <p class="dark-text cart-price cart-total" data-total="{{ $cart->total_price * (1 - ($promo->value / 100)) }}">
                                                    <strong>{{ number_format($cart->total_price * (1 - ($promo->value / 100)), 2, ',', '') }} €</strong>
                                                </p>
                                            @else
                                                <p class="dark-text cart-price cart-total" data-total="{{ $cart->total_price }}">
                                                    <strong>{{ number_format($cart->total_price, 2, ',', '') }} €</strong>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!-- Payment Method !-->
                                <div class="default-sheet-box cart-box payment-box">
                                    <h4>Payment method</h4>
                                    <span id="payment-error"></span>

                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Paypal</strong></p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input type="radio" name="payment_method" value="paypal" required>
                                            <img src="/images/paypal.png">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Credit Cards</strong></p>
                                        </div>

                                        <div id="credit-cards" class="container col-container">
                                            <div class="col full-col">

                                                <div>
                                                    {{--<h4 style="display: inline;">Credit Cards</h4>--}}
                                                    <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/mastercard.png">
                                                    <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/amex.png">
                                                    <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/maestro.png">
                                                    <img style="display: inline; vertical-align: middle" src="/images/credit_cards/light/visa.png">
                                                </div>

                                                <p>Select your credit card from this list, or add a new one</p>
                                                @foreach($payments as $payment)
                                                    <div class="target-card">
                                                        <input id="card-{{ $payment->id }}" type="radio" class="card" name="payment_method" value="{{ $payment->id }}">
                                                        <label for="card-1" style="background-image: url('/images/credit_cards/light/{{ strtolower($payment->type) }}.png')"> {{ $payment->type }} - end in ***{{ $payment->end_with }}</label>
                                                        <br>
                                                    </div>
                                                @endforeach
                                                <br>
                                                <a data-remodal-target="modal-cards" href="#"><i class="fa fa-edit"></i> Edit Credit Card list</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="default-sheet-row align-top">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text"><strong>Bank transfer</strong></p>
                                        </div>
                                        <div class="default-sheet-col">
                                            <div class="default-sheet-row-cell">
                                                <input type="radio" name="payment_method" value="bank_transfer" required>
                                                <p class=""><strong>Pay with bank transfer</strong></p>
                                            </div>

                                            {{-- <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>Company name</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>U-art S.r.l.</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>Bank name</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>Unicredit</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>Bank branch</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>Castiglione delle Stiviere<br>Via Europa, 28/30<br>46043 - Castiglione Delle Stiviere (MN)
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>CC</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>N° 000103346275</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>BIC/SWIFT</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>UNCRITM1Q62</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>IBAN</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>IT53 U020 0857 5700 0010 3346 275</p>
                                                </div>
                                            </div>
                                            <div class="default-sheet-row align-top">
                                                <div class="default-sheet-row-cell">
                                                    <p class=""><strong>Causal transfer</strong></p>
                                                </div>
                                                <div class="default-sheet-row-cell">
                                                    <p>Purchase {{ implode(", ", $casual_transfer) }}</p>
                                                </div>
                                            </div>
                                            --}}

                                            <div class="default-sheet-row align-top">
                                                <p class="small-p">
                                                        Contact us at <a href="mailto:info@kooness.com">info@kooness.com</a> to get our bank details. For any questions you can also visit our <u><a href="pages/support-center">FAQ</a></u> page
                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="default-sheet-box">
                                    <div class="default-sheet-row">
                                        <p>
                                            <input id="privacy_policy" type="checkbox" name="privacy_policy" value="privacy_policy" required="">
                                            I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                                            <br>
                                            <span id="accept-error"></span>
                                        </p>
                                    </div>
                                </div>
                                <button id="checkout-btn" class="black-button" type="submit" disabled>Checkout</button>
                            </div>
                        </div>
                    </div>
                </section>
                @include('orders.frontend.partials.modal-cc')
            </form>
        @endif
    </section>

@endsection