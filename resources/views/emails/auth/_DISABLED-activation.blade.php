{{-- Mail - User Registered (UNUSED!) --}}
@extends('emails.layout')

@section('content')
		<h2>Activate your account</h2>

		<div>
            Hi {{ $username }}.<br>
            Thank you for registering at our site. Please <a href="{{ $activation_url }}"  style="text-decoration: none; color: #7c7c7c">click here</a> to confirm your registration and to activate your account .<br/>	<br>
            After visiting the link above you can log into Kooness!<br><br>		
			
			Enjoy!
		</div>
@stop