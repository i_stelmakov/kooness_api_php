<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 25/10/18
 * Time: 12:26
 */

namespace App\Services\Api4Dem;


use App\Models\Artwork;
use App\Models\User;
use App\Services\Cart;
use Hassansin\DBCart\Models\CartLine;
use Illuminate\Support\Facades\Log;

class Api4DemActions
{

    // User Add & Edit
    function userSave(User $user)
    {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseUserSave($user);
            $user_args = $class->getArg();
            // Creazione nuovo utente => se ne database non è settato l'id di 4dem
            if(!$user->api4dem_referer_id) {
                $id = $this->subscribeContact(env('4DEM_RECIPIENT_ECOMMERCE_ID'), $user_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            }
            // Editing utente esistente => se ne database è settato l'id di 4dem
            else {
                $id = $this->updateContact(env('4DEM_RECIPIENT_ECOMMERCE_ID'), $user->api4dem_referer_id, $user_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            }
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n" . '#################### User Add & Edit' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The User with id ' . $user_args['user_id'] . ' cannot be saved on 4DEM, here the User data:' . "\n";
                $api4DemLogs .= print_r($user_args,true); ; // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The User with id ' . $user_args['user_id'] . ' has correctly saved on 4DEM, with association id of ' . $id . ', here the User data:' . "\n";
                $api4DemLogs .= ' User email_address => ' . $user_args['email_address'] . "\n";
                $api4DemLogs .= ' User name => ' . $user_args['custom_fields'][0]['value'] . "\n";
                $api4DemLogs .= ' User surname => ' . $user_args['custom_fields'][1]['value'] . "\n";

                // Creazione nuovo utente => se ne database non è settato l'id di 4dem
                if(!$user->api4dem_referer_id) {
                    // passaggio id di ritorno a Kooness
                    $user->api4dem_referer_id = $id;
                    $user->save(); // salvataggio id restituito da 4dem in kooness
                }

            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $user->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
    }

    // User Delete
    function userDelete(User $user)
    {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseUserUnsubscribe($user);
            $user_args = $class->getArg();
            $id = $this->unsubscribeContact(env('4DEM_RECIPIENT_ECOMMERCE_ID'), $user_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)

            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### User Delete'."\n";
            if( is_array($id) ) {
                $api4DemLogs .= '=== ‼️ The User with 4DEM id '.$user_args['subscriber_id'].' cannot be deactivated on 4DEM, here the User data:'."\n";
                $api4DemLogs .= ' User email_address => '.$user_args['subscriber_id']."\n";
                $api4DemLogs .= ' Request ip => '.$user_args['ip']."\n";
                $api4DemLogs .= '=== Here the reason:'."\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The User with 4DEM id '.$user_args['subscriber_id'].' has correctly deactivated on 4DEM, here the User data:'."\n";
                $api4DemLogs .= ' User email_address => '.$user_args['subscriber_id']."\n";
                $api4DemLogs .= ' Request ip => '.$user_args['ip']."\n";
            }
        }
        catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $user->id ."\n";
            $api4DemLogs .= $e->getMessage() ."\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
    }

    // User Subscribe To Newsletter
    function userSubscribe(User $user)
    {
        $result = false;

        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseUserSubscribe($user);
            $user_args = $class->getArg();
            $id = $this->subscribeContact(env('4DEM_RECIPIENT_NESWLETTER_ID'), $user_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)

            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### User Subscribe To Newsletter'."\n";
            if( is_array($id) ) {
                $api4DemLogs .= '=== ‼️ The User with 4DEM id '.$user_args['email_address'].' cannot be subscribed on 4DEM, here the User data:'."\n";
                $api4DemLogs .= ' User email_address => '.$user_args['email_address']."\n";
                $api4DemLogs .= ' Request ip => '.$user_args['subscription']['ip']."\n";
                $api4DemLogs .= '=== Here the reason:'."\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The User with 4DEM id '.$user_args['email_address'].' has correctly subscribed on 4DEM, here the User data:'."\n";
                $api4DemLogs .= ' User email_address => '.$user_args['email_address']."\n";
                $api4DemLogs .= ' Request ip => '.$user_args['subscription']['ip']."\n";
                $result = true;
            }
        }
        catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $user->id ."\n";
            $api4DemLogs .= $e->getMessage() ."\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
        return $result;
    }

    // Product Add & Edit
    function artworkSave(Artwork $artwork)
    {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseArtworkSave($artwork);
            $artwork_args = $class->getArg();
            // Creazione nuovo Artwork => se ne database non è settato l'id di 4dem
            if(!$artwork->api4dem_referer_id) {
                $id = $this->createStoreProduct(env('4DEM_STORE_ID'), $artwork_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            }
            // Editing Artwork esistente => se ne database è settato l'id di 4dem
            else {
                $id = $this->updateProductInformation(env('4DEM_STORE_ID'), $artwork->reference, $artwork_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            }
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Product Add & Edit' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The Artwork with id ' . $artwork_args['row_id'] . ' cannot be saved on 4DEM, here the Artwork data:' . "\n";
                $api4DemLogs .= print_r($artwork_args, true); // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Artwork with id ' . $artwork_args['row_id'] . ' has correctly saved on 4DEM, with association id of ' . $id . ', here the Artwork data:' . "\n";
                $api4DemLogs .= print_r($artwork_args, true); // dati prodotto passati

                // Creazione nuovo Artwork => se ne database non è settato l'id di 4dem
                if(!$artwork->api4dem_referer_id) {
                    // passaggio id di ritorno a Kooness
                    $artwork->api4dem_referer_id = $id;
                    $artwork->save(); // salvataggio id restituito da 4dem in kooness
                }

            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $artwork->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
    }

    // Product Delete
    function artworkDelete(Artwork $artwork)
    {
        $api4DemLogs = ''; // per raccolta errori
        try {

            $id = $this->deleteStoreProduct(env('4DEM_STORE_ID'), $artwork->reference); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)

            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= '#################### Product Delete' . "\n";
            if( is_array($id) ) {
                $api4DemLogs .= '=== ‼️ The Artwork with 4DEM id '.$artwork->reference.' cannot be deleted on 4DEM, here the Artwork data:'."\n";
                $api4DemLogs .= ' Artwork title => '.$artwork->title."\n";
                $api4DemLogs .= '=== Here the reason:'."\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Artwork with 4DEM id '.$artwork->reference.' has correctly deleted on 4DEM, here the Artwork data:'."\n";
                $api4DemLogs .= ' Artwork title => '.$artwork->title."\n";
            }
        }
        catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $artwork->id ."\n";
            $api4DemLogs .= $e->getMessage() ."\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
    }

    // Cart Create
    function cartCreate(Cart $cart, User $user) {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseCartCreate($cart, $user);
            $cart_args = $class->getArg();
            $id = $this->createStoreCart(env('4DEM_STORE_ID'), $cart_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Cart Create' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The Cart with id ' . $cart_args['cart_id'] . ' cannot be saved on 4DEM, here the Cart data:' . "\n";
                $api4DemLogs .= print_r($cart_args, true); // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Cart with id ' . $cart_args['cart_id'] . ' has correctly saved on 4DEM, with association id of ' . $id . ', here the Cart data:' . "\n";
                $api4DemLogs .= print_r($cart_args, true); // dati prodotto passati
                // passaggio id di ritorno a Kooness
                $cart->api4dem_referer_id = $id;
                $cart->save(); // salvataggio id restituito da 4dem in kooness
                foreach ($cart->items()->get() as $cartLine){
                    $cartLine->api4dem_success_send = true; // imposto manualmente nel ddb che il dato è passato correttamente
                    $cartLine->save();
                }
            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $user->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
    }

    // Cart Add Artwork
    function cartArtworkAdd(CartLine $cartLine, Cart $cart) {
        $api4DemLogs = ''; // per raccolta errori
        $is_success = false;
        try {
            $class = new Api4DemDataParser();
            $class->parseCartArtworkAdd($cartLine);
            $line_args = $class->getArg();
            $id = $this->createCartProduct(env('4DEM_STORE_ID'), $cart->id, $line_args); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Cart Add Artwork' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The Cart Line with id ' . $line_args['line_id'] . ' cannot be saved on 4DEM, here the Line data:' . "\n";
                $api4DemLogs .= print_r($line_args,true); // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Cart Line id ' . $line_args['line_id'] . ' has correctly saved on 4DEM, with association id of ' . $id . ', here the Line data:' . "\n";
                $api4DemLogs .= print_r($line_args,true); // dati prodotto passati

                // passaggio id di ritorno a Kooness
                $cartLine->api4dem_success_send = true; // imposto manualmente nel ddb che il dato è passato correttamente
                $cartLine->save(); // salvataggio id restituito da 4dem in kooness
                $is_success = true;
            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $cartLine->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
        if($is_success){
            $this->cartTotalUpdate($cart);
        }
    }

    // Cart Remove Artwork
    function cartArtworkRemove(Cart $cart, CartLine $cartLine) {
        $api4DemLogs = ''; // per raccolta errori
        $is_success = false;
        try {
            $id = $this->deleteCartProduct(env('4DEM_STORE_ID'), $cart->id, $cartLine->id); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)

            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Cart Remove Artwork' . "\n";
            if( is_array($id) ) {
                $api4DemLogs .= '=== ‼️ The Cart Line with id '.$cartLine->id.' cannot be deleted on 4DEM'."\n";
                $api4DemLogs .= '=== Here the reason:'."\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Cart Line with id '.$cartLine->id.' has correctly deleted on 4DEM'."\n";
                $is_success = true;
            }
        }
        catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $cartLine->id ."\n";
            $api4DemLogs .= $e->getMessage() ."\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);
        if($is_success){
            $this->cartTotalUpdate($cart);
        }
    }

    // Cart Close
    function cartClose(Cart $cart) {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseCartClose($cart);
            $cart_args = $class->getArg();
            $id = $this->updateCart(env('4DEM_STORE_ID'), $cart_args, $cart->id); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Cart Update' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The Cart with id ' . $cart_args['cart_id'] . ' cannot be saved on 4DEM, here the Cart data:' . "\n";
                $api4DemLogs .= serialize($cart_args); // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Cart with id ' . $cart_args['cart_id'] . ' has correctly saved on 4DEM, here the Cart data:' . "\n";
                $api4DemLogs .= serialize($cart_args); // dati prodotto passati
            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $cart->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);

    }


    // Cart Total Update
    function cartTotalUpdate(Cart $cart) {
        $api4DemLogs = ''; // per raccolta errori
        try {
            $class = new Api4DemDataParser();
            $class->parseCartTotalUpdate($cart);
            $cart_args = $class->getArg();
            $id = $this->updateCart(env('4DEM_STORE_ID'), $cart_args, $cart->id); // effettivo salvataggio dati su 4dem e restituzione risposta (id se inserito correttamente, array in caso di errore)
            // se inserimento su 4DEM restituisce errore (restituisce array)
            $api4DemLogs .= "\n".'#################### Cart Total Update' . "\n";
            if (is_array($id)) {
                $api4DemLogs .= '=== ‼️ The Cart with id ' . $cart->id . ' cannot be updated on 4DEM with new total, here the data:' . "\n";
                $api4DemLogs .= print_r($cart_args,true); // dati prodotto passati
                $api4DemLogs .= '=== Here the reason:' . "\n";
                $api4DemLogs .= print_r($id,true); // array con errore
            }
            // se inserimento su 4DEM va a buon fine (restituisce id)
            else {
                $api4DemLogs .= '=== ✅ The Cart with id ' . $cart->id . ' has correctly updated on 4DEM with new total, with association id of ' . $id . ', here the data:' . "\n";
                $api4DemLogs .= print_r($cart_args,true); // dati prodotto passati
            }
        } catch (\Exception $e) {
            $api4DemLogs = "Error on id:" . $cart->id . "\n";
            $api4DemLogs .= $e->getMessage() . "\n";
        }
        Log::channel('4dem')->debug($api4DemLogs);

    }






} // .Api4DemActions