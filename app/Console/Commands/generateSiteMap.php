<?php

namespace App\Console\Commands;

use App\Models\Artist;
use App\Models\ArtistsCategory;
use App\Models\Artwork;
use App\Models\ArtworksCategory;
use App\Models\ArtworksMediumCategory;
use App\Models\Exhibition;
use App\Models\Fair;
use App\Models\GalleriesCategory;
use App\Models\Gallery;
use App\Models\Page;
use App\Models\Post;
use App\Models\PostsCategory;
use App\Models\Tag;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class generateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create sitemap generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create(config('app.url'));

        $sitemap->add(Url::create("/artworks"));
        $sitemap->add(Url::create("/galleries"));
        $sitemap->add(Url::create("/galleries/archive"));
        $sitemap->add(Url::create("/artists"));
        $sitemap->add(Url::create("/artists/archive"));
        $sitemap->add(Url::create("/fairs"));
        $sitemap->add(Url::create("/fairs/archive"));
        $sitemap->add(Url::create("/posts/magazine"));
        $sitemap->add(Url::create("/posts/news"));
        $sitemap->add(Url::create("/exhibitions"));
        $sitemap->add(Url::create("/register"));

        $artists = Artist::whereRaw("slug IS NOT NULL")->whereRaw("status IN (1)")->get();
        $artists->each(function (Artist $artist) use ($sitemap) {
            $sitemap->add(Url::create("/artists/{$artist->slug}"));
        });


        $artists_categories = ArtistsCategory::all();
        $artists_categories->each(function (ArtistsCategory $artistsCategory) use ($sitemap) {
            $sitemap->add(Url::create("/artists/{$artistsCategory->slug}"));
        });

        $artworks = Artwork::whereRaw("status IN (1,2)")->get();
        $artworks->each(function (Artwork $artwork) use ($sitemap) {
            $sitemap->add(Url::create("/artworks/{$artwork->slug}"));
        });

        $artworks_categories = ArtworksCategory::all();
        $artworks_categories->each(function (ArtworksCategory $artworksCategory) use ($sitemap) {
            $sitemap->add(Url::create("/artworks/{$artworksCategory->slug}"));
        });

        $artworks_medium_categories = ArtworksMediumCategory::all();
        $artworks_medium_categories->each(function (ArtworksMediumCategory $artworksMediumCategory) use ($sitemap) {
            $medium = $artworksMediumCategory->medium()->first();
            $category = $artworksMediumCategory->category()->first();
            $sitemap->add(Url::create("/artworks/{$medium->slug}/{$category->slug}"));
        });

        $exhibitions = Exhibition::whereRaw("status IN (1)")->get();
        $exhibitions->each(function (Exhibition $exhibition) use ($sitemap) {
            $sitemap->add(Url::create("/exhibition/{$exhibition->slug}"));
        });

        $fairs = Fair::whereRaw("status IN (1)")->get();
        $fairs->each(function (Fair $fair) use ($sitemap) {
            $sitemap->add(Url::create("/fairs/{$fair->slug}"));
        });

        $galleries = Gallery::whereRaw("status IN (1)")->get();
        $galleries->each(function (Gallery $gallery) use ($sitemap) {
            $sitemap->add(Url::create("/galleries/{$gallery->slug}"));
        });

        $galleries_categories = GalleriesCategory::all();
        $galleries_categories->each(function (GalleriesCategory $galleriesCategory) use ($sitemap) {
            $sitemap->add(Url::create("/galleries/{$galleriesCategory->slug}"));
        });

        $posts_magazine = Post::whereType('magazine')->whereRaw("status IN (1)")->get();
        $posts_magazine->each(function (Post $post) use ($sitemap) {
            $sitemap->add(Url::create("/posts/magazine/{$post->slug}"));
        });

        $posts_news = Post::whereType('news')->whereRaw("status IN (1)")->get();
        $posts_news->each(function (Post $post) use ($sitemap) {
            $sitemap->add(Url::create("/posts/news/{$post->slug}"));
        });

        $posts_categories = PostsCategory::all();
        $posts_categories->each(function (PostsCategory $postsCategory) use ($sitemap) {
            $sitemap->add(Url::create("/posts/magazine/{$postsCategory->slug}"));
        });

        $posts_categories = PostsCategory::all();
        $posts_categories->each(function (PostsCategory $postsCategory) use ($sitemap) {
            $sitemap->add(Url::create("/posts/magazine/{$postsCategory->slug}"));
        });

//        $tags = Tag::all();
//        foreach ($tags as $tag) {
//            $add = false;
//            $artworks = false;
//            $artists = false;
//            $galleries = false;
//            $magazine = false;
//            $news = false;
//            if($tag->artworks()->count()){
//                $add = true;
//                $artworks = true;
//            }
//            if($tag->artists()->count()){
//                $add = true;
//                $artists = true;
//            }
//            if($tag->galleries()->count()){
//                $add = true;
//                $galleries = true;
//            }
//            if($tag->news()->count()){
//                $add = true;
//                $magazine = true;
//            }
//            if($tag->magazine()->count()){
//                $add = true;
//                $news = true;
//            }
//
//            if($add){
//                $sitemap->add(Url::create("/tags/{$tag->slug}"));
//                if($artworks){
//                    $sitemap->add(Url::create("/tags/{$tag->slug}/artworks"));
//                }
//                if($artists){
//                    $sitemap->add(Url::create("/tags/{$tag->slug}/artists"));
//                }
//                if($galleries){
//                    $sitemap->add(Url::create("/tags/{$tag->slug}/galleries"));
//                }
//                if($magazine){
//                    $sitemap->add(Url::create("/tags/{$tag->slug}/magazine"));
//                }
//                if($news){
//                    $sitemap->add(Url::create("/tags/{$tag->slug}/news"));
//                }
//            }
//        }


        $pages = Page::all();
        $pages->each(function (Page $page) use ($sitemap) {
            $sitemap->add(Url::create("/pages/{$page->slug}"));
        });

        $sitemap->writeToFile(public_path('sitemap.xml'));

    }
}
