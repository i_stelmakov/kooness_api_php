{{-- Artworks - Archive --}}

@extends('layouts.app')

@section('title', $meta_title)

@section('body-class', 'artworks-archive')

@section('header-og-abstract', $meta_description)

@section('canonical', $canonical)

@section('seo-keywords', $meta_keywords)

@section('body-class')

@endsection

@section('scripts')
    <script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="/js/artworks.js"></script>
@endsection

@section('styles')
@endsection

@section('content')
    <div class="sections">
        <section id="page-header">
            <div class="container boxed-container">
                <div class="col-container-with-offset">
                    <div class="default-sheet-row">
                        <div class="default-sheet-row-cell">
                            <span>
                                <h1>{{ $h1 }}</h1>
                                @if( isset($description))
                                    <div class="show-more-text-container">
                                    <div class="show-more-content">
                                        <div class="show-more-content-inner">
                                            {!! $description !!}
                                        </div>
                                    </div>
                                    <div class="show-more-button">
                                        <a href="#">Show more</a>
                                    </div>
                                </div>​
                                @endif
                            </span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="container boxed-container">
                <div class="artwoks-filter-button">
                    <span class="small-input-button">&lt; Filters</span>
                </div>
            </div>

        </section>
        <!-- Archive !-->
        <section id="archive" class="artworks-page">
            <div class="container boxed-container">
                <div class="container col-container">
                    <div id="archive-col" class="col three-fourth-col">
                        @if(!$fair && count($categories))
                            <section id="category-filter" class="show">
                                <!-- Hiding Box Header !-->
                                <div class="hiding-box-header">
                                    <div class="default-sheet-row-cell">
                                        <h3>Featured subjects</h3>
                                    </div>
                                    <div title="open/close" class="hiding-box-trigger"></div>
                                </div>
                                <div class="col-container-with-offset">
                                    <div class="category-filter-container">
                                        @foreach($categories as $category)
                                            @if(!$this_medium)
                                                <div class="slider-item-img wide-box category-filter-item {{ ($this_category && $this_category->id == $category->id)? 'active':'' }}">
                                                    <a href="{{ route('artworks.single', [$category->slug]) }}">
                                                        <div class="category-filter-item-img background-cover lazy" data-src="{{($category->url_cover)? $category->url_cover : $category->randomImage()}}">
                                                            <h4>{{ $category->name }}</h4>
                                                        </div>
                                                    </a>

                                                </div>
                                            @else
                                                <div class="slider-item-img wide-box category-filter-item {{ ($this_category && $this_category->id == $category->id)? 'active':'' }}">
                                                    <a href="{{ route('artworks.categories.medium', [$this_medium->slug, join('-', [$category->slug, $this_medium->slug])]) }}">
                                                        <div class="category-filter-item-img background-cover lazy" data-src="{{($category->url_cover)? $category->url_cover : $category->randomImage()}}">
                                                            <h4>{{ $category->name }}</h4>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </section>
                        @endif
                        <div id="archive-header">
                            <div class="default-sheet-row">
                                <div class="default-sheet-row-cell">
                                    <h3></h3>
                                </div>
                                    <div class="default-sheet-row-cell">
                                        <p>Order by</p>
                                        <form onsubmit="return false;">
                                            <select name="order_by" id="order_by" class="search-by filter_select">
                                                <option value="recent" selected="selected">Most recent</option>
                                                @if(!$fair)
                                                    <option value="higher-price">Higher price</option>
                                                    <option value="lower-price">Lower price</option>
                                                @endif
                                            </select>
                                        </form>
                                    </div>
                            </div>
                        </div>
                        <section id="product-masonry" class="default-slider-section">
                            <div class="grid">
                                @foreach($artworks as $artwork_key=>$artwork)
                                    <?php
                                    $artist_name = null;
                                    $artist_link = null;
                                    $artist = $artwork->artist()->first();
                                    if ($artist) {
                                        $artist_name = $artist->first_name . " " . $artist->last_name;
                                        $artist_link = route('artists.single', [$artist->slug]);
                                    }
                                    ?>
                                    <div id="{{ $artwork->id }}" class="item">
                                        <span class="content">
                                            @if(Auth::user())
                                                <div class="add-item follow {{ (Auth::user()->collectionArtworks->contains($artwork->id)) ? 'active' : '' }}" data-section="artworks" data-id="{{$artwork->id}}">
                                                @if(Auth::user()->collectionArtworks->contains($artwork->id))
                                                        <i class="fa fa-heart"></i>
                                                    @else
                                                        <i class="fa fa-heart-o"></i>
                                                    @endif
                                                </div>
                                            @endif
                                            <div class="slider-item-img"><a title="{{ $artwork->title }}" href="/artworks/{{ $artwork->slug }}"><img class="lazy" data-src="{{ $artwork->main_image->url }}" src=""></a></div>

                                            <div class="slider-item-txt">
                                                <div class="default-sheet-row slider-item-row artist-name">
                                                    <div class="default-sheet-row-cell">
                                                        <h3><a title="{{ $artist_name }}" class="dark-text" href="{{ $artist_link }}">{{ $artist_name }}</a></h3>
                                                    </div>
                                                </div>
                                                <h2><a title="{{ $artwork->title }}" class="dark-text" href="{{ route('artworks.single', [$artwork->slug]) }}">{{ $artwork->title }}</a></h2>
                                                <p class="artwork-measures">{{ $artwork->measure_cm }} cm</p>
                                                <div class="default-sheet-row slider-item-row">
                                                    <div class="default-sheet-row-cell">
                                                        <p class="artwork-type">
                                                            @foreach($artwork->categories()->where("is_medium", "=", "1")->get() as $ArtworkMedium)
                                                                <a title="{{ $ArtworkMedium->name }}" href="{{ route('artworks.single', [$ArtworkMedium->slug]) }}">{{ $ArtworkMedium->name }}</a>@if(!$loop->last), @endif
                                                            @endforeach
                                                        </p>
                                                    </div>

                                                    {{-- SE artwork non in fiera OPPURE artwork impone visualizzazione prezzo --}}
                                                    @if(!$artwork->available_in_fair || $artwork->show_price)
                                                        <div class="default-sheet-row-cell">
                                                            {{-- SE artwork segnato come disponibile E artwork non è in nessuna fiera --}}
                                                            @if( $artwork->status == 1 && !$artwork->available_in_fair)
                                                                <p class="dark-text artwork-price">{{ $artwork->pretty_price }}</p>
                                                            @else
                                                                <p class="dark-text artwork-price">SOLD OUT</p>
                                                            @endif
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>

                                        </span>
                                    </div>
                                @endforeach
                            </div>
                            {!! $paginate->links('layouts.includes.pagination') !!}
                        </section>
                    </div>
                    <div id="archive-sidebar" class="col one-fourth-col sidebar">

                        <div class="artwoks-filter-button right-big-button">
                            <i class="fa fa-close"> </i>
                        </div>

                        @if($this_medium)
                            <input type="hidden" name="this_medium" value="{{ $this_medium->id }}">
                        @endif

                        @if($this_category)
                            <input type="hidden" name="this_category" value="{{ $this_category->id }}">
                        @endif

                        <div class="widget">
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Medium</h4>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="widget-container">
                                <div class="widget-mask">
                                    <ul data-filter="medium">
                                        @foreach($medium as $key=>$item)
                                            @if(isset($this_medium) && $this_medium->id == $item->id)
                                                @continue
                                            @endif
                                            <li data-order="{{ $key }}" class="item-list li-filter" data-id="{{ $item->id }}">
                                                <a title="{{ $item->name }}">{{ $item->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <a class="load-more">Mediums</a>
                            </div>
                        </div>
                        <!-- Filter WidgetTemplate 3 !-->
                        <div class="widget" style="{{ ($fair)? 'display:none' : 'display:block' }}">
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Price</h4>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="widget-container">
                                <div class="range-slider">
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">From</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="min-price" class="range-number number" name="min-price" min="0" max="50000" step="100" type="number" value="0"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>€</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">€</option>--}}
                                        {{--<option>$</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row">
                                        <input data-filter="min-price" class="range-number range" name="min-price" type="range" min="0" max="50000" step="100" value="0"/>
                                    </div>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">to</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="max-price" class="range-number number" name="max-price" min="0" max="50000" step="100" type="number" value="50000"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>€</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">€</option>--}}
                                        {{--<option>$</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row space-between-row">
                                        <input data-filter="max-price" class="range-number range" name="max-price" type="range" min="0" max="50000" step="100" value="50000"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Filter WidgetTemplate 8 !-->
                        <div class="widget">
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Size</h4>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="widget-container">
                                <div class="range-slider">
                                    <p>Width</p>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">from</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="min-width" class="range-number number" name="min-width" min="0" max="400" step="10" type="number" value="0"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>cm</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">cm</option>--}}
                                        {{--<option>in</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row">
                                        <input data-filter="min-width" class="range-number range" name="min-width" type="range" min="0" max="400" step="10" value="0"/>
                                    </div>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">to</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="max-width" class="range-number number" name="max-width" min="0" max="400" step="10" type="number" value="400"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>cm</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">cm</option>--}}
                                        {{--<option>in</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row space-between-row">
                                        <input data-filter="max-width" class="range-number range" name="max-width" type="range" min="0" max="400" step="10" value="400"/>
                                    </div>
                                    <p>Height</p>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">from</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="min-height" class="range-number number" name="min-height" min="0" max="400" step="10" type="number" value="0"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>cm</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">cm</option>--}}
                                        {{--<option>in</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row">
                                        <input data-filter="min-height" class="range-number range" name="min-height" type="range" min="0" max="400" step="10" value="0"/>
                                    </div>
                                    <div class="default-sheet-row">
                                        <div class="default-sheet-row-cell">
                                            <p class="dark-text">to</p>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <input data-filter="max-height" class="range-number number" name="max-height" min="0" max="400" step="10" type="number" value="400"/>
                                        </div>
                                        <div class="default-sheet-row-cell">
                                            <span>cm</span>
                                        </div>
                                        {{--<div class="default-sheet-row-cell">--}}
                                        {{--<select class="search-by">--}}
                                        {{--<option selected="selected">cm</option>--}}
                                        {{--<option>in</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="default-sheet-row space-between-row">
                                        <input data-filter="max-height" class="range-number range" name="max-height" type="range" min="0" max="400" step="10" value="400"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Artist</h4>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="widget-container">
                                <form class="default-sheet-row" onsubmit="return false;">
                                    <input type="search" class="filter-input" placeholder="Search Artist"/>
                                    <i class="fa fa-search search-button"> </i>
                                </form>
                                <div class="widget-mask">
                                    <ul data-filter="artists">
                                        @foreach($artists as  $key=>$item)
                                            <li data-order="{{ $key }}" class="item-list li-filter" data-id="{{ $item->id }}">
                                                <a title="{{  $item->first_name . " " . $item->last_name }}">{{  $item->last_name . " " . $item->first_name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <a class="load-more">Artists</a>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="hiding-box-header">
                                <div class="default-sheet-row-cell">
                                    <h4>Gallery</h4>
                                </div>
                                <div title="open/close" class="hiding-box-trigger"></div>
                            </div>
                            <div class="widget-container">
                                <form class="default-sheet-row" onsubmit="return false;">
                                    <input type="search" class="filter-input" placeholder="Search gallery"/>
                                    <i class="fa fa-search search-button"> </i>
                                </form>
                                <div class="widget-mask">
                                    <ul data-filter="galleries">
                                        @foreach($galleries as $key=>$item)
                                            <li data-order="{{ $key }}" class="item-list li-filter" data-id="{{ $item->id }}">
                                                <a title="{{  $item->name }}">{{  $item->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <a class="load-more">Galleries</a>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-container">
                                <div class="full-col">
                                    <button class="apply-filters default-input-button full-col">Apply filters</button>
                                </div>
                                @if(isset($has_filter) && $has_filter)
                                    <div class="full-col">
                                        <button class="remove-filters default-input-button full-col">Remove filters</button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        // global variables
        const medium = [];
        const artists = [];
        const galleries = [];

        var minPriceInput = $("input[name='min-price']");
        var maxPriceInput = $("input[name='max-price']");
        var minWidthInput = $("input[name='min-width']");
        var maxWidthInput = $("input[name='max-width']");
        var minHeightInput = $("input[name='min-height']");
        var maxHeightInput = $("input[name='max-height']");
        var thisMediumInput = $("input[name='this_medium']");
        var thisCategoryInput = $("input[name='this_category']");
        var thisOrderBy = $("select[name='order_by']");
        // console.log('order_by => '+orderBy);

        var pathName = window.location.pathname + window.location.search;

        var activeFiltersFromPHP = JSON.parse('{!!$filters_to_json!!}'); // special data retrieve from PHP
        // console.log('activeFiltersFromPHP =>');
        console.log(activeFiltersFromPHP);
        var fair = '{!! ($fair)? $fair->slug : null !!}'; // special data retrieve from PHP

    </script>
@endsection