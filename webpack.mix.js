let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.(handlebars|hbs)$/,
                use: [{
                    loader: "handlebars-loader"

                }]
            }
        ]
    }
});

// per ottenere la mappatura dei files
// mix.webpackConfig({ devtool: "inline-source-map" });

// frontend
mix.js('resources/assets/js/main-frontend.js', 'public/js')
    .sass('resources/assets/sass/main-frontend.scss', 'public/css/')
    // funzione per pulire, ottimizzare e rendere cross-broser CSS
    // .postCss('public/css/main-frontend.css', 'public/css/', [
    //     require('autoprefixer')({
    //         browsers: ['last 40 versions'],
    //         grid: true
    //     }),
    //     require('postcss-css-variables')()
    // ])
    // .copy('resources/assets/js/vendors/map-dependencies.js', 'public/js')
    .copy('resources/assets/js/vendors/imagesloaded.pkgd.min.js', 'public/js')
    .copy('resources/assets/js/vendors/isotope.pkgd.min.js', 'public/js')
    .copy('resources/assets/js/vendors/checkout.js', 'public/js')
    .copy('resources/assets/js/vendors/user.js', 'public/js')
    .copy('resources/assets/js/vendors/collections.js', 'public/js')
    .copy('resources/assets/js/vendors/artworks.js', 'public/js')
    .copy('resources/assets/js/vendors/pdf.js', 'public/js')
    .copy('resources/assets/js/vendors/pdf.worker.js', 'public/js')
    .copy('resources/assets/js/vendors/modernizr.js', 'public/js')
    .copy('resources/assets/js/vendors/timeline-main.js', 'public/js')
    // .copy('resources/assets/js/vendors/jquery.mobile.custom.min.js', 'public/js');
    .copy('resources/assets/js/vendors/jquery.mobile-1.5.0-alpha.1.min.js', 'public/js');


    
// backend
mix.js('resources/assets/js/main-admin.js', 'public/js')
    .sass('resources/assets/sass/main-admin.scss', 'public/css')
    .sass('resources/assets/sass/vendors/ckeditor_content.scss', 'public/css')
    .copy('node_modules/ckeditor/config.js', 'public/js/ckeditor/config.js')
    .copy('node_modules/ckeditor/styles.js', 'public/js/ckeditor/styles.js')
    .copy('node_modules/ckeditor/contents.css', 'public/js/ckeditor/contents.css')
    .copy('resources/assets/js/vendors/jquery.twism.min.js', 'public/js')
    .copyDirectory('node_modules/ckeditor/skins', 'public/js/ckeditor/skins')
    .copyDirectory('node_modules/ckeditor/lang', 'public/js/ckeditor/lang')
    .copyDirectory('node_modules/ckeditor/plugins', 'public/js/ckeditor/plugins');

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
mix.browserSync('http://kooness.local/');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps (mapping dei file scss e js)
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// \