@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : "Kooness | {$page->title}")

@section('body-class', 'page-gallery-membership')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
    {{--GALLERY--}}

    <section id="page-header" class="container-vertical-padding-top container-vertical-padding-bottom">
        <div class="container boxed-container">
            <h1 class="big-h1">{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : $page->title }}</h1>
        </div>
    </section>

    <section id="featured-section" class="container-vertical-padding-bottom">
        <div class="container boxed-container">
            <div class="col-container-with-offset featured-section-item">
                <div class="one-half-col featured-section-item-img">
                    <div class="slider-item-img wide-box">
                        <div class="background-cover" style="background-image: url(/images/gallery-application-cover.jpg)"></div>
                    </div>
                </div>
                <div class="one-half-col featured-section-item-txt">
                    <div class="slider-title-box-txt">
                        <h2>
                            Reach younger,<br>
                            <span class="main-color-txt slider-title-span">more digitally-savvy clients worldwide</span>
                        </h2>
                        <p>{{ $page->content }}</p>
                        <p>Attract new collectors and reach international clients in a very easy way. Kooness provides galleries the ability to showcase and present their artworks and artists to reach a global audience of collectors and to go beyond the white walls.</p>
                        <a class="black-button" href="#become-member-form">Become a member</a>
                    </div>
                    <div class="slider-title-drop-caps">
                        <h6 style="background-image: url(/images/opera-3.jpg)">k</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="page-content" class="container-vertical-padding-top container-vertical-padding-bottom">
        <div class="container boxed-container">
                <h3 class="app-box-title">
                        More than <span class="main-color-txt">300</span> international galleries<br>
                        More than <span class="main-color-txt">25</span> countries<br>
                        More than <span class="main-color-txt">800</span> artists<br><br><br>
                        And a thrilled <span class="main-color-txt">new generation</span> of art buyers<br><br><br><br>
                </h3>
            <span class="h1 app-box-title">How we work</span>
            <div class="container col-container-with-margin horizontal-icon-box-container">
                <div class="col one-half-col-with-margin center-col">
                    <div class="page-content-txt">

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>Artwork selection</h2>
                                <p>Our curatorial team selects artists and artworks to be featured online together with the gallery staff</p>
                            </div>
                        </div>

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>Sale</h2>
                                <p>Once a work is sold, we auto-generate shipping labels that get sent to your gallery</p>
                            </div>
                        </div>

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-rocket"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>Shipping</h2>
                                <p>Shipping and insurance costs are covered by buyers</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col one-half-col-with-margin center-col">
                    <div class="page-content-txt">

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-cog"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>Operations</h2>
                                <p>All promotional operations and inquiries related to artworks are managed by Kooness</p>
                            </div>
                        </div>

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-archive"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>Package</h2>
                                <p>The gallery is simply responsible for packing the sold artwork and consign it to the shipper.</p>
                            </div>
                        </div>

                        <div class="icon-box-horizontal-item">
                            <div class="icon-box-horizontal-item-img">
                                <div class="icon-round-container">
                                    <i class="fa fa-smile-o"></i>
                                </div>
                            </div>
                            <div class="icon-box-horizontal-item-txt">
                                <h2>After sale</h2>
                                <p>Kooness takes care of all the after sales activities and customer service</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="page-content" class="container-vertical-padding-bottom">

        <div class="container boxed-container container-vertical-padding-bottom">
            <span class="h1 app-box-title">We support you</span>
            <div>
                <p>
                    We are here to support you and all your gallery staff to upload new contents, improve your online presence, choose the best strategy and discover what collectors love most from your works. Kooness team is pleased to provide you with direct assistance service 24- hours a day, 7- days a week.
                </p>
            </div>
        </div>

        <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
            <span class="h1 app-box-title">Want to join?</span>
            <div>
                <p>
                    Are you a gallery and would like to exhibit on Kooness? Please fill in the subscription form and we will come back to you as soon as possible. We only work with art galleries so if you are an artist please do not fill in this form.
                </p>
            </div>
        </div>
        
        <div class="container boxed-container">
            <a id="become-member-form"></a>
            <hr>
            <p>Fields marked with * are required</p><br>
            <form class="col-container-with-offset-and-margin k-form" id="gallerist-form" role="form" method="POST" action="{{ route('galleries.request.subscription') }}">
                {!! Honeypot::generate('honey_name', 'honey_time') !!}
                @csrf
                <div class="one-third-col-with-margin">
                    <input id="name" class="full-col" type="text" placeholder="Gallery Name*" name="name" required/>
                </div>
                <div class="one-third-col-with-margin">
                    <input id="first_name" class="full-col" type="text" placeholder="Gallery owner first name*" name="first_name" required/>
                </div>
                <div class="one-third-col-with-margin">
                    <input id="last_name" class="full-col" type="text" placeholder="Gallery owner last name*" name="last_name" required/>
                </div>
                <div class="one-third-col-with-margin">
                    <input id="country" class="full-col" type="text" placeholder="Country" name="country"/>
                </div>
                <div class="one-third-col-with-margin">
                    <input id="state" class="full-col" type="text" placeholder="State / Province / Region" name="state"/>
                </div>
                <div class="one-third-col-with-margin">
                    <input id="city" class="full-col" type="text" placeholder="City" name="city"/>
                </div>
                <div class="full-col-with-margin">
                    <input id="street" class="full-col" type="text" placeholder="Street address*" name="street" required/>
                </div>
                <div class="one-half-col-with-margin">
                    <input id="zip" class="full-col" type="text" placeholder="Postal / Zip Code" name="zip" />
                </div>
                <div class="one-half-col-with-margin">
                    <input id="email" class="full-col" type="email" placeholder="E-mail address*" name="email" required/>
                </div>
                <div class="one-half-col-with-margin">
                    <input id="phone" class="full-col" type="text" placeholder="Phone number*" name="phone"/>
                </div>
                <div class="one-half-col-with-margin">
                    <input id="website" class="full-col" type="text" placeholder="Website address" name="website"/>
                </div>
                <div class="full-col-with-margin form-privacy">
                    <p><strong>Major art world events that your organization participates in, like art fairs or biennials (if any):</strong></p>
                    <textarea class="full-col" name="events" rows="5" data-type="textarea"></textarea>
                </div>
                <div class="full-col-with-margin form-privacy">
                    <p>
                        <input id="privacy_policy" type="checkbox" name="privacy_policy" value="privacy_policy" required> I read the
                        <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data</p>
                </div>
                <div class="full-col-with-margin">
                    <input class="default-input-button" type="submit" value="Become a member"/>
                </div>
            </form>
        </div>

        {{-- <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
            <span class="h1 app-box-title">HOW WE WORK</span>
            <div>
                <p>
                    1. Our curatorial team selects artists and artworks to be featured online together with gallery staff<br>
                    2. All promotional operations and inquiries are managed by Kooness<br>
                    3. Once a work is sold, we auto-generate shipping labels that get sent to your gallery<br>
                    4. Shipping and insurance costs are covered by buyers<br>
                    5. The gallery is simply responsible for packing<br>
                    6. Kooness takes care of all the after sales activities and customer service<br>
                </p>
            </div>
        </div> --}}
    
        {{-- <div class="container boxed-container">
            <span class="h1 app-box-title">WE SUPPORT YOU</span>
            <div>
                <p>
                    We are here to support you and all your gallery staff to upload new contents, improve your online presence, choose the best strategy and discover what collectors love most from your works. Kooness team is pleased to provide you with direct assistance service 24-hours a day, 7- days a week.
                </p>
            </div>
        </div> --}}

    </section>

@endsection
