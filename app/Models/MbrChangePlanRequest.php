<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MbrChangePlanRequest extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function plan()
    {
        return $this->hasOne('App\Models\MbrPlan', 'id', 'plan_id');
    }

}
