<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\FrontendExtendedController;
use App\Models\Page;
use Request;
use Validator;

class FrontendController extends FrontendExtendedController
{
    /**
     *
     * @return \Illuminate\Http\Response
     */

    public function pages($slug_name)
    {
        $page = Page::whereSlug($slug_name)->first();
        if(!$page){
            abort(404);
        }
        if($slug_name == 'about' || $slug_name == 'contact') {
            return view('pages.frontend.about-us', compact('slug_name', 'page'));
        }
        if($slug_name == 'our-services') {
            // Disattivato in attesa dei contenuti
            // return view('pages.frontend.our-services', compact('slug_name', 'page'));
        }
        if($slug_name == 'gallery-membership') {
            return view('pages.frontend.gallery-membership', compact('slug_name', 'page'));
        }
        if($slug_name == 'support-center') {
            return view('pages.frontend.support-center', compact('slug_name', 'page'));
        }
        else {
            return view('pages.frontend.pages', compact('slug_name', 'page'));
        }
    }

}



