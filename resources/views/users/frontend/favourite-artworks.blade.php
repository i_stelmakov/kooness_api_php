{{-- User - Favourite Artworks --}}

@extends('layouts.app')

@section('title')
    User - Favourite Artworks
@endsection

@section('title', 'Favourite Artworks')

@section('body-class')
    user-favourite-artworks
@endsection

@section('content')
    <section id="product-slider" class="default-slider-section slider-border">
        <div id="saved-artworks" class="container boxed-container">
            <div class="default-slider-header">
                <h1>Saved Artworks</h1>
            </div>
            <div class="container col-container-with-offset-and-margin">
                @if(Auth::user()->collectionArtworks()->count())
                    @foreach(Auth::user()->collectionArtworks()->get() as $artwork)
                        @include('users.frontend.partials.artwork', ['artwork' => $artwork])
                    @endforeach
                @else
                    No favourite
                @endif
            </div>
        </div>
    </section>
@endsection