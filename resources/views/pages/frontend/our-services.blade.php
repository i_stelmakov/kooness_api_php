@extends('layouts.app')

@section('title', ($seo_data && $seo_data->meta_title) ? $seo_data->meta_title : "Kooness | {$page->title}")

@section('body-class', 'page-our-services')

@section('header-og-abstract', ($seo_data && $seo_data->meta_description) ? $seo_data->meta_description : null)

@section('canonical', ($seo_data && $seo_data->canonical) ? $seo_data->canonical : null)

@section('seo-keywords', ($seo_data && $seo_data->meta_keywords) ? $seo_data->meta_keywords : null)

@section('index_opt', ($seo_data && $seo_data->no_index) ? 'noindex' : null)

@section('follow_opt', ($seo_data && $seo_data->no_follow) ? 'nofollow' : null)

@section('content')
{{--SERVICES--}}
<section id="page-header">
    <div class="container boxed-container">
        <div class="col-container-with-offset">
            <div class="default-sheet-row">
                <div class="default-sheet-row-cell">
                    <h1>{{ ($seo_data && $seo_data->h1) ? $seo_data->h1 : $page->title }}</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="user-tabs" class="tab-section">
    <div class="container boxed-container">
        <div class="container col-container-with-offset-and-margin container-vertical-padding-bottom">
            <div class="tab-menu one-fifths-col-with-margin container-vertical-padding-bottom">
                <ul>
                    <li class="tab-button current-tab-item" data-id="1">Art Advisor</li>
                    <li class="tab-button" data-id="2">Art for business</li>
                </ul>
            </div>
            <!-- Tab 1 !-->
            <div class="tab-container four-fifths-col-with-margin tab tab-display" data-id="1">
                <h2>Art Advisor</h2>
                <p>Kooness provides a tailor-made service for a young generation of collectors as well as established ones.</p><br>
                <section class="img-banner-title background-cover" style="background-image: url(/images/placeholder.png)">
                    <div class="img-banner-title-txt">
                        <h2>Bring art into your office</h2>
                        <a class="black-button" href="#self">CONTACT</a>
                    </div>
                </section>
                <section id="icon-box-container" class="default-slider-section">
                    <div class="container boxed-container">
                        <h2 class="text-align-center">The advantages of buying art for your business</h2>
                        <div class="container col-container-with-offset-and-margin">
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <div class="icon-round-container">
                                    <i class="fa fa-paint-brush"></i>
                                </div>
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <div class="icon-round-container">
                                    <i class="fa fa-camera-retro"></i>
                                </div>
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <div class="icon-round-container">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="services-list">
                    <h2>A tailored solution</h2>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>1</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>2</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>3</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>4</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>5</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <div class="services-list-row">
                        <div class="services-list-cell"><h3>6</h3></div>
                        <div class="services-list-cell"><h3>First step</h3></div>
                        <div class="services-list-cell"><p>Our head of sales will get in touch with you by phone or by email and take note of your requirements and budget.</p></div>
                    </div>
                    <em><p class="text-align-center default-margin-top">To benefit from this personalised service and advice from Artsper’s team,<br>a commission of 10% of the total cost of the works purchased will be requested</p></em>
                </div>
            </div>
            <!-- Tab 2 !-->
            <div class="tab-container four-fifths-col-with-margin tab" data-id="2">
                <section>
                    <h2>Art Advisor</h2>
                    <p>Kooness provides a tailor-made service for a young generation of collectors as well as established ones.</p><br>
                </section>

                {{-- Sezione Testimonials oscurata --}}
                {{-- <section id="icon-box-container" class="default-slider-section">
                    <div class="container boxed-container">
                        <h2 class="text-align-center no-margin">Testimonials</h2>
                        <div class="container col-container-with-offset-and-margin">
                            <div class="wide-box iframe-container">
                                <iframe class="center-block" src="https://www.youtube.com/embed/VpiWGffAV8s" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <img class="full-width-img" src="img/logo-placeholder.png">
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <img class="full-width-img" src="img/logo-placeholder.png">
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                            <div class="slider-item icon-box-item one-third-col-with-margin icon-box-item">
                                <img class="full-width-img" src="img/logo-placeholder.png">
                                <div class="slider-item-txt">
                                    <h2><a class="dark-text" href="artist.php">Lorem ipsum dolor</a></h2>
                                    <p>Works of art help to stimulate your team's creativity and create a pleasant and welcoming working environment</p>
                                    <a class="read-more" href="#self">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> --}}

            </div>
        </div>


        <section id="info">
                <div class="container boxed-container container-vertical-padding-top container-vertical-padding-bottom">
                    <div class="container col-container-with-offset-and-margin">
                        <div class="full-col-with-margin">
                            @if(Session::get('send') && Session::get('send') == 'success')
                                <div class="in-page-warning success">
                                    Message successful sent! We will reply as soon as possible.
                                </div>
                            @endif
                            <h2>Send a message</h2>
                            <form class="contact-form col-container-with-offset-and-margin k-form" method="POST" action="{{ route('send.contact.message') }}">
                                {!! Honeypot::generate('honey_name', 'honey_time') !!}
                                @csrf
                                <div class="one-half-col-with-margin">
                                    <input type="text" name="name" placeholder="Name" required>
                                </div>
                                <div class="one-half-col-with-margin">
                                    <input type="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="full-col-with-margin">
                                    <input type="text" name="subject" placeholder="Subject" required>
                                </div>
                                <div class="full-col-with-margin">
                                    <textarea name="body" placeholder="Message" rows="6" required></textarea>
                                </div>
                                <div class="full-col-with-margin form-privacy">
                                    <p>
                                        <input name="privacy" type="checkbox" required> I read the <a href="{{ route('pages.intro', ['privacy-policy']) }}">Privacy Policy</a> and I consent to the processing of my personal data
                                        <p class="errorTxt"></p>
                                    </p>
                                </div>
                                <div class="full-col-with-margin">
                                    <input class="default-input-button" type="submit" value="Submit">
                                </div>
                            </form>
            
                        </div>
                    </div>
                </div>
            </section>
            

        
    </div>
</section>
@endsection
